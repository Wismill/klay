# Klay

__[Klay]__ is an advanced cross-platform command-line utility
to design __[keyboard layouts](https://en.wikipedia.org/wiki/Keyboard_layout)__.

__Warning:__ this software is in alpha stage!

1. [Features](#features)
2. [Installation](#installation)
3. [Usage](#usage)

# Features

Klay provides the following features:

* __[Import](#import-formats):__ import layouts from files
* __[Export](#export-formats):__ create files in various formats to use the layout
* __[Check](#check):__ check issues in a layout
<!--[TODO] * __Evaluate:__ evalute the efficiency of a layout -->

## Import formats

Klay supports the following _input_ formats:

* __[YAML]__
* __[JSON]__
* __[MSKLC]__
* ~~__XCompose__ for dead keys definitions~~ (planned)

## Export formats

Klay supports the following _output_ formats:

* Operating system keyboard layout
  
    * __Linux:__ [XKB] and [XCompose]
    * __Windows:__
  
        * [MSKLC]: Microsoft Keyboard Layout Creator
        * C sources to build with [MSKLC] or [WDK]
        * ~~Autohotkey~~ (work in progress)
  
    * ~~__Mac OS__~~ (planned)
    * ~~__Android__~~ (planned)
  
* __[YAML]__
* __[JSON]__
* __Text:__ ASCII art for documentation
* __CSV:__ list of characters by key combination
* __Pictures:__

    * SVG
    * PNG
    * ~~keyboard-layout-editor.com~~ (planned)


* __[Kmonad]:__ brings advanced customization features that are usually available at the hardware level on the QMK-firmware enable keyboards.
* ~~Typing tutors~~ (planned)

[Klay]: https://gitlab.com/Wismill/klay/
[YAML]: https://yaml.org/
[JSON]: https://www.json.org/
[XKB]: https://wiki.archlinux.org/index.php/X_keyboard_extension
[XCompose]: https://en.wikipedia.org/wiki/Compose_key
[MSKLC]: https://microsoft-keyboard-layout-creator.software.informer.com/1.4/
[WDK]: https://docs.microsoft.com/en-us/windows-hardware/drivers/download-the-wdk
[Kmonad]: https://github.com/david-janssen/kmonad/

## Check

Check if there is any issue with the layout, such as:

* Locale characters coverage
* Incorrect dead key definition
<!-- [TODO] more useful info about the tests -->

# Installation

## Build from source

Klay is written in [Haskell](https://www.haskell.org) and can be compiled using the [Haskell platform](https://www.haskell.org/platform/).

1. [Get the sources](#get-the-sources)
2. [Prepare your build environment](#prepare-the-build-environnement)
3. [Build](#build)

### Get the sources

Clone this repository:

```bash
git clone https://gitlab.com/Wismill/klay
```

### Prepare the build environnement

#### Linux

Install the Haskell toolchain using the tool [`ghcup`](https://www.haskell.org/ghcup/). Ensure you have `GHC` ≥ 8.10 and `cabal-install` ≥ 3.2 installed and added to your `$PATH` environnent variable.

#### Windows

Install the Haskell toolchain using [Chocolatey](https://www.haskell.org/platform/#windows).

#### Mac OS X

Currently not tested.

### Build

Execute the following command at the root of your local repository.

```bash
cabal install
```

This will create a binary `klay` in `$HOME/.cabal/bin/`. Ensure to have this path added to your `$PATH`.

# Usage

Layouts are written in text files in the [YAML] format. Then are used as input for the `klay` command line program.

## File format

```yaml
metadata:
  name: Name of the layout
  description: Description of the layout
  systemName: system name (OS-dependent)
  version: semantic version
  author: author of the layout
  publicationDate: ISO date
  license: license of the layout
  locales:
    Primary:
    - optional
    - lists
    Other: 
    - of
    Secondary:
    - locales
dual functions: {}
options:
  os independent:
    ActionLabels: {} # Custom labels (OS-independent)
  linux:
    ActionLabels: {} # Custom labels for Linux
  windows:
    ShiftDeactivatesCaps: false
    KeyboardType: 4
    ActionLabels: {} # Custom labels
    VirtualKeyDefinition: # Custom mapping to virtual keys
      Minus: OEM_4
    IsoLevel3IsAltGr: true
    Altgr: true
    LrmRlm: false
    VirtualKeysNames: {}
groups: # ISO groups (a list of groups or a single group, as hereinafter)
  name: Group 1
  levels:
    default+windows+linux:
      base: Lower Alphanumeric
      shift: Upper Alphanumeric
      level3: ISO Level 3
      shift+level3: ISO Level 4
  keyMap: # Mapping
  - key: RAlternate # A singleton key (same at all levels)
    action: mod:level3 
    os:
    - default
    - linux
    - windows
  - key: Space # A single key, with some levels undefined
    actions: # Note the plural
      base: space
      shift: space
    os: 
    - default
    - linux
    - windows
  - keys: |- # A group of keys sharing same options.
      Grave N0 N1 N2 N3 N4 N5 N6 N7 N8 N9 Minus Equals
      A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
    layers:
    - layer: base # Modifiers used
      actions: |- # The symbols mapped to the keys above, in the same order
        dk:circumflex 0 1 2 3 4 5 6 7 8 9 ß dk:acute
        a b c d e f g h i j k l m n o p q r s t u v w x z y
      os:
      - default
      - linux
      - windows
    - layer: shift
      actions: |-
        ° = ! " § $ % & / ( ) ? dk:grave
        A B C D E F G H I J K L M N O P Q R S T U V W X Z Y
      os:
      - default
      - linux
      - windows
dead keys:
- dead key: Grave Accent
  base char: '`'
  label: '◌̀'
  combos:
  - .: U0300
  - nobreakspace: '`'
  - space: ˋ
  - A: À
```

## Overview of the command line program

```
Usage: klay COMMAND
  klay ─ Keyboard layout designer

Available options:
  -h,--help                Show this help text

Available commands:
  check                    Check the keyboard layout
  symbols                  Output the reachable character of the layout
  export                   Export files
```

## Export

### XKB

```bash
klay export -i LAYOUT_FILE -o OUTPUT_DIR --xkb
```

### Windows

```bash
# MSKLC
klay export -i LAYOUT_FILE -o OUTPUT_DIR --klc
# MSKLC + C source files
klay export -i LAYOUT_FILE -o OUTPUT_DIR --klc-c
# WDK C source files
klay export -i LAYOUT_FILE -o OUTPUT_DIR --wdk
```

### Pictures

__Note:__ PNG export is done by converting SVG export files. In order to use it, you need [inkscape](https://inkscape.org) ≥ 1.0 installed and available in your `$PATH`.

You may choose a specific [geometry] (ISO, ANSI, etc.) and a typing method
using `--geometry` and `--typing-method` options respectively.

[geometry]: https://en.wikipedia.org/wiki/Keyboard_layout#Physical_layouts

```bash
# Export in SVG
klay export -i LAYOUT_FILE -o OUTPUT_DIR --geometry GEOMETRY --typing-method TYPING_METHOD --svg
# Export in PNG
klay export -i LAYOUT_FILE -o OUTPUT_DIR --geometry GEOMETRY --typing-method TYPING_METHOD --png
```

# Related projects

* [KLFC](https://github.com/39aldo39/klfc)
* [kbdgen](https://github.com/divvun/kbdgen)
