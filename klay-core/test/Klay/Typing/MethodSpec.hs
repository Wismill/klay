module Klay.Typing.MethodSpec
  ( spec
  ) where

import Data.Foldable (traverse_)
import Data.Maybe (isJust)

import Test.Hspec

import Klay.Keyboard.Hardware
import Klay.Keyboard.Hardware.Divisions.ANSI qualified as ANSI
import Klay.Keyboard.Hardware.Divisions.ISO qualified as ISO
import Klay.Keyboard.Hardware.Divisions.TypeMatrix qualified as TypeMatrix
import Klay.Typing.Method
import Klay.Typing.Method.ISO.O0 qualified as O0
import Klay.Typing.Method.TypeMatrix.Default qualified as TMDefault

{- |
Ensure the keys used in evaluators have mapping to hand and finger
-}
spec :: Spec
spec = do
  describe "O0" do
    describe "ANSI" do
      it "Alphanumeric section" do
        let keys = _alphanumericSection ANSI.keyboardSections
        traverse_ (hasHFingerDefined O0.typingMethod) keys
    describe "ISO" do
      it "Alphanumeric section" do
        let keys = _alphanumericSection ISO.keyboardSections
        traverse_ (hasHFingerDefined O0.typingMethod) keys
  describe "TypeMatrix default" do
    it "TypeMatrix" do
      let keys = _alphanumericSection TypeMatrix.keyboardSections
      traverse_ (hasHFingerDefined TMDefault.typingMethod) keys
  where
    hasHFingerDefined tm key = hfinger tm key `shouldSatisfy` isJust
