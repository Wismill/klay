{-# OPTIONS_GHC -Wno-orphans #-}

module Klay.Import.Windows.MSKLCSpec
  ( spec
  ) where

import Data.Maybe (fromMaybe)
import Data.Either (isRight)
import Data.Text.Lazy qualified as TL
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Map.Merge.Strict qualified as Map
import Data.Functor ((<&>), void)
-- import Control.DeepSeq (force)

import Test.Hspec
import Data.These
-- import Data.Align
-- import Data.Witherable

-- import Klay.Export.Windows.Common.Layout
import Klay.Export.Windows.MSKLC (generateOnlyKlcFile, prepareLayout_)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action hiding (SpecialAction)
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Import.Windows.MSKLC
import Klay.OS (MultiOs(..), windows)
import Data.Alterable

spec :: Spec
spec =
  describe "Direct from layouts" do
    checkLayout "Bépo" Bépo.layout
    checkLayout "DE" DE.layout
    checkLayout "ES" ES.layout
    checkLayout "US" US.layout
  where
    get_msklcContent l = runIO $ generateOnlyKlcFile l mempty
      <&> \(mfileContent, _, _, _) -> fromMaybe undefined mfileContent
    get_win_opts = _windows . _options

    checkLayout name l_ref = describe name do
      klc_bépo <- TL.toStrict <$> get_msklcContent l_ref
      let klc = parseMsklc name klc_bépo
      it "Parse" do
        klc `shouldSatisfy` isRight
      let l = msklcToLayout . either error id $ klc
      let l_win = prepareLayout_ l
      let l_win_ref = prepareLayout_ l_ref
      describe "Metadata" do
        let metadata = _metadata l_win
        let metadata_ref = _metadata l_win_ref
        it "Name" do
          _name metadata `shouldBe` _name metadata_ref
        it "System Name" do
          _systemName metadata `shouldBe` _systemName metadata_ref
        it "Description" do
          _description metadata `shouldBe` _description metadata_ref
        -- [TODO] locales
        it "Version" do
          _version metadata `shouldBe` _version metadata_ref
        it "Author" do
          _author metadata `shouldBe` _author metadata_ref
        it "License" do
          _license metadata `shouldBe` _license metadata_ref
      it "Windows options" do
        get_win_opts l_win `shouldBe` get_win_opts l_win_ref

      describe "Dead Keys" do
        void $ check_dkd (_deadKeys l_win) (_deadKeys l_win_ref)

      describe "Groups" do
        -- _groups l `shouldBe` _groups l_ref
        check_groups (_groups l) (_groups l_ref)
        -- [TODO] dead keys

    -- We can only check that MSKLC's definitions are included in original layout
    check_dkd dkd dkd_ref =
      let dkd' = _dkMergedDefinitions dkd
          dkd_ref' = _dkMergedDefinitions dkd_ref
      in ialignWithA check_dk dkd' dkd_ref'

    ialignWithA :: (Applicative f, Ord k) => (k -> These a b -> f c) -> Map.Map k a -> Map.Map k b -> f (Map.Map k c)
    ialignWithA f = Map.mergeA
      (Map.traverseMissing \k -> f k . This)
      (Map.traverseMissing \k -> f k . That)
      (Map.zipWithAMatched \k a b -> f k (These a b))

    check_dk dk = it (show dk) . \case
      This _      -> expectationFailure $ "Unexpected dead key: " <> show dk
      -- That _      -> expectationFailure $ "Missing dead key: " <> show dk
      That _      -> pure () -- skip
      These c1 c2 ->
        -- [TODO] compare using unflattenDeadKeyDefinitions and add combo with space
        -- c1 Set.\\ c2 `shouldBe` mempty
        Set.intersection c1 c2 `shouldSatisfy` not. null

    check_groups gs gs_ref = do
      describe "Actions mappings" do
        case (gs, gs_ref) of
          (OneGroup g, OneGroup g_ref) -> checkGroup g g_ref
          _ -> error "Boo"

    checkGroup g g_ref = describe "Check group" do
      let g'     = resolveGroup windows g
      let g_ref' = resolveGroup windows g_ref
      -- it "Name" do
      --   _groupName g' `shouldBe` _groupName g_ref'
      it "Levels" do
        -- _groupLevels g' `shouldBe` _groupLevels g_ref'
        Map.keys (_groupLevels g') `shouldBe` Map.keys (_groupLevels g_ref')
      it "Mapping" do
        let m = _groupMapping g'
        let m_ref = _groupMapping g_ref'
        -- _groupMapping g' `shouldBe` _groupMapping g_ref'
        -- m `shouldBe` m_ref
        diff m m_ref `shouldBe` (Nothing :: Maybe ResolvedActionsKeyMapDelta)
      -- where go (These a b) | a == b = Nothing
      --       go x = Just x
