module Klay.Import.Linux.XComposeSpec
  ( spec
  ) where

import Data.Either (isRight)
import Data.Text.Lazy qualified as TL
import Control.DeepSeq (force)

import Test.Hspec

import Klay.Export.Linux.XKB qualified as XKB
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Import.Linux.XCompose (parseCompose)
import Klay.OS (linux)

spec :: Spec
spec =
  describe "Direct from layouts" do
    xkb_bépo <- runIO $ XKB.getXkbContent . resolveLayout linux $ Bépo.layout
    it "Bépo" do
      let xcompose = TL.toStrict . XKB.xkbCompose . force $ xkb_bépo
      parseCompose "Bépo" xcompose `shouldSatisfy` isRight
    xkb_es <- runIO $ XKB.getXkbContent . resolveLayout linux $ ES.layout
    it "ES" do
      let xcompose = TL.toStrict . XKB.xkbCompose . force $ xkb_es
      parseCompose "ES" xcompose `shouldSatisfy` isRight
    xkb_us <- runIO $ XKB.getXkbContent . resolveLayout linux $ US.layout
    it "US" do
      let xcompose = TL.toStrict . XKB.xkbCompose . force $ xkb_us
      parseCompose "US" xcompose `shouldSatisfy` isRight
