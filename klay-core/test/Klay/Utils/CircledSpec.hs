{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Utils.CircledSpec
  ( spec
  ) where

import Data.Maybe (mapMaybe)

import Test.Hspec

import Klay.Utils.Unicode.Circled


spec :: Spec
spec = do
  describe "circled number" do
    it "circledNumber" do
      mapMaybe circledNumber [0..20] `shouldBe` "⓪①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳"
    it "negativeCircledNumber" do
      mapMaybe negativeCircledNumber [0..20] `shouldBe` "⓿❶❷❸❹❺❻❼❽❾❿⓫⓬⓭⓮⓯⓰⓱⓲⓳⓴"
  describe "circled character" do
    it "circled" do
      let cs = mconcat [['A'..'Z'], ['a'..'z'], ['0'..'9']]
      mapMaybe circled cs `shouldBe` "ⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏⓐⓑⓒⓓⓔⓕⓖⓗⓘⓙⓚⓛⓜⓝⓞⓟⓠⓡⓢⓣⓤⓥⓦⓧⓨⓩ⓪①②③④⑤⑥⑦⑧⑨"

