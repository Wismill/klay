{-# OPTIONS_GHC -fno-warn-orphans #-}

module Klay.Utils.AlterableSpec
  ( spec
  ) where


import Test.Hspec
import Test.QuickCheck

import Data.Alterable

spec :: Spec
spec = do
  -- describe "Alteration" do
  --   describe "Magma instance" do
  --     it "associativity" do
  --       property $ \(a :: Alteration String String) (b :: Alteration String String) (c :: Alteration String String)
  --         -> (a +> b) +> c == a +> (b +> c)
  --     it "neutral element" do
  --       property $ \(a ::  Alteration String String) -> Update mempty +> a == a && a +> Update mempty == a
    -- describe "Semigroup instance" do
    --   it "associativity" do
    --     property $ \(a :: Alteration String String) (b :: Alteration String String) (c :: Alteration String String)
    --       -> (a <> b) <> c == a <> (b <> c)
    --   it "neutral element" do
    --     property $ \(a ::  Alteration String String) -> mempty <> a == a && a <> mempty == a
  describe "Upgrade" do
    describe "Semigroup instance" do
      it "associativity" do
        property $ \(a :: Upgrade String) (b :: Upgrade String) (c :: Upgrade String)
          -> (a <> b) <> c == a <> (b <> c)
      -- it "neutral element" do
      --   property $ \(a :: Upgrade String) -> mempty <> a == a && a <> mempty == a

instance Magma String where
  (+>) = (<>)
