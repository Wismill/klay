{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.GroupSpec
  ( spec
  ) where


-- import Data.Map.Strict qualified as Map

import Test.Hspec

import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.OS

spec :: Spec
spec = do
  let us_resolved_group = resolveGroup OsIndependent US.group
  describe "groupModifiers" do
    it "US layout" do
      groupModifiers us_resolved_group `shouldBe` [Shift]
  describe "groupIndexedLevelsNames" do
    it "US layout" do
      groupIndexedLevelsNames us_resolved_group `shouldBe` [(1, "Lower Alphanumeric"), (2, "Upper Alphanumeric")]
  -- describe "groupBaseVkActionMap" do
  --   it "US layout" do
  --     groupBaseVkActionMap US.group `shouldBe` usBaseActionMap
  -- where
  --   usBaseActionMap = Map.foldrWithKey discard_all_but_first_level mempty US.actionMap
  --   discard_all_but_first_level _  NoActions am = am
  --   discard_all_but_first_level vk (SingleAction a) am = Map.insert vk a am
  --   discard_all_but_first_level vk (Actions _ as) am =
  --     maybe am (\a -> Map.insert vk a am) (Map.lookup mempty as)
