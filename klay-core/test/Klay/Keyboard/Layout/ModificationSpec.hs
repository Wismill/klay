{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.ModificationSpec
  ( spec
  ) where

import Data.Maybe
import Data.Map.Strict qualified as Map

import Test.Hspec

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.ButtonAction
import Klay.Keyboard.Layout (Layout(..), Key)
import Klay.Keyboard.Layout.Action hiding (Delete, Left, Right)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier
import Klay.Keyboard.Layout.Modification
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.OS
import Data.Alterable (Alteration(..), insertOrUpdate1)
import Klay.Utils ((⇢))

spec :: Spec
spec = do
  describe "Permute map" do
    let m = Map.fromList $ zip K.keyGroupLetters K.keyGroupLetters
    it "permuteMapKeys" do
      permuteMapKeys [] m `shouldBe` Just m
      permuteMapKeys [K.A] m `shouldBe` Just m
      permuteMapKeys [K.A, K.B] m `shouldBe` Just ([(K.A, K.B), (K.B, K.A)] <> m)
      permuteMapKeys [K.A, K.B, K.C] m `shouldBe` Just ([(K.A, K.B), (K.B, K.C), (K.C, K.A)] <> m)
      permuteMapKeys [K.A, K.B, K.C, K.D] m `shouldBe` Just ([(K.A, K.B), (K.B, K.C), (K.C, K.D), (K.D, K.A)] <> m)
      -- Missing keys
      permuteMapKeys [K.A, K.Escape] m `shouldBe` Just (Map.delete K.A $ Map.insert K.Escape  K.A m)
      -- Erroneous permutation
      permuteMapKeys [K.A, K.A, K.B] m `shouldBe` Nothing
      permuteMapKeys [K.A, K.B, K.A] m `shouldBe` Nothing
    it "permuteMapValues" do
      permuteMapValues [] m `shouldBe` Just m
      permuteMapValues [ K.A] m `shouldBe` Just m
      permuteMapValues [ K.A, K.B] m `shouldBe` Just ([(K.A, K.B), (K.B, K.A)] <> m)
      permuteMapValues [ K.A, K.B, K.C] m `shouldBe` Just ([(K.A, K.B), (K.B, K.C), (K.C, K.A)] <> m)
      permuteMapValues [ K.A, K.B, K.C, K.D] m `shouldBe` Just ([(K.A, K.B), (K.B, K.C), (K.C, K.D), (K.D, K.A)] <> m)
      -- Missing vk
      permuteMapValues [ K.A, K.Escape] m `shouldBe` Just (Map.insert K.A  K.Escape m)
      -- Erroneous permutation
      permuteMapValues [ K.A, K.A, K.B] m `shouldBe` Nothing
      permuteMapValues [ K.A, K.B, K.A] m `shouldBe` Nothing
    describe "relativePermutationMap" do
      let m0 = Map.fromList $ zip K.keyGroupLetters  K.keyGroupLetters
      it "[K.A, K.B, K.C]; [K.A, K.Q, K.B]" do
        let m1 = fromMaybe undefined $ permuteMapKeys [K.A, K.B, K.C] m
        let m2 = fromMaybe undefined $ permuteMapKeys [K.A, K.Q, K.B] m
        relativePermutationMap m1 m2 `shouldBe` [( K.A, K.C), ( K.B, K.Q), ( K.C, K.A), ( K.Q, K.B)] <> m0
      it "[K.A, K.B, K.C, K.D]; [K.A, K.Q, K.W, K.B]" do
        let m1 = fromMaybe undefined $ permuteMapKeys [K.A, K.B, K.C, K.D] m
        let m2 = fromMaybe undefined $ permuteMapKeys [K.A, K.Q, K.W, K.B] m
        relativePermutationMap m1 m2 `shouldBe` [( K.A, K.D), ( K.B, K.Q), ( K.C, K.A), ( K.D, K.C), ( K.Q, K.W), ( K.W, K.B)] <> m0
      -- [TODO] more tests


  -- describe "GroupsModification" do
  --   it "adapt_group_layout" do
  --     let am = [K.A ⇉ [noModifier .= 'a'], K.B ⇉ [noModifier .= 'b'], K.C ⇉ [noModifier .= 'c'], K.D ⇉ [noModifier .= 'd']]
  --     let g = Group { _groupName="Test"
  --                   , _groupLevels=[ TOsIndependent [noModifier ⇢ "base"] ]
  --                   , _groupMapping=am
  --                   } :: KeyMultiOsRawGroup
  --     let gl = make_key_mapping g
  --     let am' = amodify (KeyPermutation [K.A, K.B, K.C] :: MultiOsRawActionMapModification) am
  --     adapt_group_layout [(K.A, K.C), (K.B, K.A), (K.C, K.B), (K.D, K.D)] gl `shouldBe` g{_groupMapping=am'}
      -- let gl' = kmModify (KeysPermutation [K.A, K.C, K.B] :: RawKeyMapModification K.Key) gl
      -- let am'' = amodify (KeyPermutation [K.A, K.C, K.B] :: MultiOsRawActionMapModification) am
      -- adapt_group_layout [(K.A, K.A), (K.B, K.B), (K.C, K.C), (K.D, K.D)] gl' `shouldBe` g{_groupMapping=am''}
      -- let am''' = amodify (KeyPermutation [K.A, K.B, K.C, K.D] :: MultiOsRawActionMapModification) am
      -- adapt_group_layout [(K.A, K.B), (K.B, K.D), (K.C, K.A), (K.D, K.C)] gl' `shouldBe` g{_groupMapping=am'''}

      -- [TODO]
      -- adapt_group_layout defaultVirtualKeysDefinition (get_key_mapping_first_group_layout ._groups $ DE.layout) `shouldBe` DE.group


  describe "GroupModification" do
    describe "modifyActionMapLevels" do
      let g = Group { _groupName="Test"
                    , _groupLevels=[noModifier ⇢ "base", shift ⇢ "shifted", isoLevel3 ⇢ "altGr", (isoLevel3 <> shift) ⇢ "shifted altGr"]
                    , _groupMapping=[K.A ⇢ [noModifier .= 'a', shift .= 'A', isoLevel3 .= 'à', (isoLevel3 <> shift) .= 'À']]
                    } :: ResolvedGroup K.Key
      it "isoLevel3 -> invalid" do
        let gl = [noModifier ⇢ "base", shift ⇢ "shifted"]
        let m = [K.A ⇢ [noModifier .= 'a', shift .= 'A']]
        let supported_modifiers = shift <> capitals <> control <> alt
        let f l
              | l == mempty = Right mempty
              | otherwise = case splitMask supported_modifiers l of
                Right ls -> if ls == mempty then Right l else Left (InvalidModifiers . maskToModifiers $ ls)
                Left ls -> Left (InvalidModifiers . maskToModifiers $ ls)
        let errors = mempty{_discardedLevels=[(isoLevel3, "altGr") ⇢ Invalid [IsoLevel3], (isoLevel3 <> shift, "shifted altGr") ⇢ Invalid [IsoLevel3]]}
        mapGroupLevelsResolved f Just KeepPrevious g `shouldBe` (g{_groupLevels=gl, _groupMapping=m}, Just errors)
      it "isoLevel3 -> altGr" do
        let gl = [noModifier ⇢ "base", shift ⇢ "shifted", altGr ⇢ "altGr", (altGr <> shift) ⇢ "shifted altGr"]
        let m = [K.A ⇢ [noModifier .= 'a', shift .= 'A', altGr .= 'à', (altGr <> shift) .= 'À']]
        let supported_modifiers = shift <> capitals <> control <> alt
        let f l
              | l == mempty = Right mempty
              | otherwise = case splitMask supported_modifiers l' of
                Right ls -> if ls == mempty then Right l' else Left (InvalidModifiers . maskToModifiers $ ls)
                Left ls -> Left (InvalidModifiers . maskToModifiers $ ls)
              where
                l' = either id (<> altGr) (splitMask isoLevel3 l)
        mapGroupLevelsResolved f Just KeepPrevious g `shouldBe` (g{_groupLevels=gl, _groupMapping=m}, Nothing)


  describe "MultiOsRawActionMapModification" do
    let am_os_independent = [ K.N1 ⇒ [ TOsIndependent [noModifier .= '1', shift .= '!'] ]
                            , K.N2 ⇒ [ TOsIndependent [noModifier .= '2', shift .= '"'] ]
                            , K.N3 ⇒ [ TOsIndependent [noModifier .= '3', shift .= '§'] ]
                            ] :: MultiOsRawActionsKeyMap
    let am_linux = [ K.N1 ⇒ [ TLinux [isoLevel3 .= '¹'] ]
                   , K.N2 ⇒ [ TLinux [isoLevel3 .= '²'] ]
                   ] :: MultiOsRawActionsKeyMap
    let am = am_os_independent +> am_linux
    it "init" do
      am `shouldBe`
        [ K.N1 ⇒ [ TOsIndependent [noModifier .= '1', shift .= '!']
                 , TLinux [isoLevel3 .= '¹' ]
                 ]
        , K.N2 ⇒ [ TOsIndependent [noModifier .= '2', shift .= '"']
                 , TLinux [isoLevel3 .= '²']
                 ]
        , K.N3 ⇒ [ TOsIndependent [noModifier .= '3', shift .= '§'] ]
        ]
    describe "KeyMapUpdate" do
      it "update OsIndependent" do
        let m1 = KeyMapUpdate
                [ K.N1 ⇢ [ TOsIndependent [isoLevel3 ⏵ '¡'] ]
                , K.N2 ⇢ [ TOsIndependent [noModifier ⏵ 'ⅱ', shift ⏵ 'Ⅱ'] ]
                ] :: MultiOsRawActionMapModification
        -- m1 `shouldBe`
        amodify m1 am `shouldBe`
          [ K.N1 ⇒ [ TOsIndependent [noModifier .= '1', shift .= '!', isoLevel3 .= '¡']
                   , TLinux   [isoLevel3 .= '¡' ]
                   , TWindows [noModifier .= '1', shift .= '!', isoLevel3 .= '¡' ] ]
          , K.N2 ⇒ [ TOsIndependent [noModifier .= 'ⅱ', shift .= 'Ⅱ']
                   , TLinux   [noModifier .= 'ⅱ', shift .= 'Ⅱ', isoLevel3 .= '²' ]
                   , TWindows [noModifier .= 'ⅱ', shift .= 'Ⅱ'] ]
          , K.N3 ⇒ [ TOsIndependent [noModifier .= '3', shift .= '§'] ]
          ]
      it "update Linux" do
        let m2 = KeyMapUpdate
                [ K.N1 ⇢ [ TLinux [isoLevel3 ⏵ '¡'] ]
                , K.N2 ⇢ [ TLinux [noModifier ⏵ 'ⅱ', shift ⏵ 'Ⅱ'] ]
                ] :: MultiOsRawActionMapModification
        amodify m2 am `shouldBe`
          [ K.N1 ⇒ [ TOsIndependent [noModifier .= '1', shift .= '!']
                   , TLinux         [isoLevel3 .= '¡']
                   ]
          , K.N2 ⇒ [ TOsIndependent [noModifier .= '2', shift .= '"']
                   , TLinux         [noModifier .= 'ⅱ', shift .= 'Ⅱ', isoLevel3 .= '²']
                   ]
          , K.N3 ⇒ [ TOsIndependent [noModifier .= '3', shift .= '§'] ]
          ]
      it "Remove N1" do
        let m1 = KeyMapUpdate [ K.N1 ⇢ Delete ] :: MultiOsRawActionMapModification
        amodify m1 am `shouldBe`
          [ K.N2 ⇒ [ TOsIndependent [noModifier .= '2', shift .= '"']
                   , TLinux         [isoLevel3 .= '²']
                   ]
          , K.N3 ⇒ [ TOsIndependent [noModifier .= '3', shift .= '§'] ]
          ]
      it "Remove Linux" do
        let m1 = KeyMapUpdate [ K.N1 ⇢ [TLinux Delete] ] :: MultiOsRawActionMapModification
        amodify m1 am `shouldBe`
          [ K.N1 ⇒ [ TOsIndependent [noModifier .= '1', shift .= '!']
                   ]
          , K.N2 ⇒ [ TOsIndependent [noModifier .= '2', shift .= '"']
                   , TLinux         [isoLevel3 .= '²']
                   ]
          , K.N3 ⇒ [ TOsIndependent [noModifier .= '3', shift .= '§'] ]
          ]

        let m2 = KeyMapUpdate [ K.N1 ⇢ [TLinux Delete], K.N2 ⇢ [TLinux Delete]] :: MultiOsRawActionMapModification
        amodify m2 am `shouldBe` am_os_independent


  describe "ActionMapModification" do
    let usActionMap = fmap untagCustomisableActions . _groupMapping . resolveGroup OsIndependent $ US.group
    let get_actions vk = fromMaybe UndefinedActions $ Map.lookup vk usActionMap
    -- [TODO] KeySwap
    describe "KeyPermutation" do
      describe "US layout" do
        it "[]" do
          let m = KeyPermutation [] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
        it "[A]" do
          let m = KeyPermutation [K.A] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
        it "[A, B]" do
          let m = KeyPermutation [K.A, K.B] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.A] <> usActionMap
        it "[A, B, C]" do
          let m = KeyPermutation [K.A, K.B, K.C] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.C, K.C ⇢ get_actions K.A] <> usActionMap
        it "[A, B, C, D]" do
          let m = KeyPermutation [K.A, K.B, K.C, K.D] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.C, K.C ⇢ get_actions K.D, K.D ⇢ get_actions K.A] <> usActionMap
        it "incorrect permutation [A, B, A]" do
          let m = KeyPermutation [K.A, K.B, K.A] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
    describe "KeyPermutations" do
      describe "US layout" do
        it "[]" do
          let m = KeyPermutations [] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
        it "[[]]" do
          let m = KeyPermutations [[]] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
        it "[[A]]" do
          let m = KeyPermutations [[K.A]] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
        it "[[A, B]]" do
          let m = KeyPermutations [[K.A, K.B]] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.A] <> usActionMap
        it "[[A, B], [B, C]]" do
          let m = KeyPermutations [[K.A, K.B], [K.B, K.C]] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.C, K.C ⇢ get_actions K.A] <> usActionMap
        it "[[A, B], [C, D, E], [D, K]]" do
          let m = KeyPermutations [[K.A, K.B], [K.C, K.D, K.E], [K.D, K.K]] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.A, K.C ⇢ get_actions K.D, K.D ⇢ get_actions K.K, K.E ⇢ get_actions K.C, K.K ⇢ get_actions K.E] <> usActionMap
        it "incorrect permutation [[A, B], [C, D, C], [D, K]]" do
          let m = KeyPermutations [[K.A, K.B], [K.C, K.D, K.C], [K.D, K.K]] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
    describe "KeyRemap" do
      describe "US layout" do
        it "[]" do
          let m = KeyRemap [] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` usActionMap
        it "[A->B]" do
          let m = KeyRemap [(K.A, K.B)] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B] <> usActionMap
        it "[A->B, B->A]" do
          let m = KeyRemap [(K.A, K.B), (K.B, K.A)] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.A] <> usActionMap
        it "[A->C, B->C]" do
          let m = KeyRemap [(K.A, K.C), (K.B, K.C)] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.C, K.B ⇢ get_actions K.C] <> usActionMap
        it "[A->B, B->C, C->A]" do
          let m = KeyRemap [(K.A, K.B), (K.B, K.C), (K.C, K.A)] :: ActionMapModification Key Actions Actions
          amodify m usActionMap `shouldBe` [K.A ⇢ get_actions K.B, K.B ⇢ get_actions K.C, K.C ⇢ get_actions K.A] <> usActionMap
    -- [TODO] KeyReplace
    describe "KeyClone" do
      describe "US layout" do
        it "A clones B" do
          amodify (KeyClone K.B K.A :: ActionMapModification Key Actions Actions) usActionMap `shouldBe` [K.A ⇢ get_actions K.B] <> usActionMap
        it "A clones Iso102" do
          amodify (KeyClone K.Iso102 K.A :: ActionMapModification Key Actions Actions) usActionMap `shouldBe` [K.A ⇢ UndefinedActions] <> usActionMap
        it "A clones Yen" do
          amodify (KeyClone K.Yen K.A :: ActionMapModification Key Actions Actions) usActionMap `shouldBe` Map.delete K.A usActionMap
    -- [TODO] KeyRemove
    describe "KeyMapUpdate" do
      describe "US layout" do
        it "Update A" do
          let as  = mkActions mempty     mempty [(noModifier, AChar 'à'), (shift, AChar 'À')]
          let as' = mkActions alphabetic mempty [(noModifier, AChar 'à'), (shift, AChar 'À')]
          let m = KeyMapUpdate [(K.A, insertOrUpdate1 as)] :: SimpleActionMapModification
          amodify m usActionMap `shouldBe` Map.insert K.A as' usActionMap
        it "Delete A" do
          let m = KeyMapUpdate [(K.A, Delete)] :: SimpleActionMapModification
          amodify m usActionMap `shouldBe` Map.delete K.A usActionMap


  describe "DuaFunctionModification" do
    describe "DualAddHold" do
      describe "US layout" do
        it "AddHold A Ctrl" do
          lmodify'' (DualAddHoldH K.A K.LControl defaultTapHoldDuration) US.layout `shouldBe` Map.insert K.A (TapHoldKey K.A K.LControl defaultTapHoldDuration) us_ds
        it "AddHold A Ctrl then Shift" do
          lmodify'' (
            [ DualAddHoldH K.A K.LControl defaultTapHoldDuration
            , DualAddHoldH K.A K.LShift defaultTapHoldDuration ] :: [DuaFunctionModification])
            US.layout `shouldBe`
            Map.insert K.A (TapHoldKey K.A K.LShift defaultTapHoldDuration) us_ds
    describe "DualAddTapH" do
      describe "US layout" do
        it "DualAddTapH A to Ctrl" do
          lmodify'' (DualAddTapH K.LControl K.A defaultTapHoldDuration) US.layout `shouldBe` Map.insert K.LControl (TapHoldKey K.A K.LControl defaultTapHoldDuration) us_ds
        it "DualAddTapH A to Ctrl then B" do
          lmodify'' (
            [ DualAddTapH K.LControl K.A defaultTapHoldDuration
            , DualAddTapH K.LControl K.B defaultTapHoldDuration ] :: [DuaFunctionModification])
            US.layout `shouldBe`
            Map.insert K.LControl (TapHoldKey K.B K.LControl defaultTapHoldDuration) us_ds
    describe "DualAddTapHold" do
      describe "US layout" do
        it "DualAddTapHold A (B, Ctrl)" do
          lmodify'' (DualAddTapHold K.A K.B K.LControl defaultTapHoldDuration) US.layout `shouldBe` Map.insert K.A (TapHoldKey K.B K.LControl defaultTapHoldDuration) us_ds
  where
    -- us_vkd = on_key_map id . _groups $ US.layout
    us_ds = _dualFunctionKeys US.layout
    -- es_vkd = on_key_map id . _groups $ ES.layout
    -- lmodify' :: (IsKeyModification (RawKeyMapModification a) VirtualKey) => RawKeyMapModification a -> MultiOsRawLayout -> VirtualKeysDefinition
    -- lmodify' m l = on_key_map id . _groups $ lmodify (Mod m :: Modification MultiOsRawLayout) l
    lmodify'' m l = _dualFunctionKeys $ lmodify m l
    -- normal vk = NormalKey (VK.ACustom vk)
    --tap_hold vk1 vk2 = TapHoldKey (VK.ACustom vk1) (VK.osDefault vk2) 300
