module Klay.Keyboard.Layout.ExamplesSpec
  ( spec
  ) where

import Data.Text qualified as T
-- import Data.Map.Strict qualified as Map
import Data.Set qualified as Set
-- import Data.Functor (void)
import Data.Foldable (traverse_)
import Data.List (intersperse)

import Test.Hspec
import Data.BCP47 qualified as BCP47

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Symbols (reachableCharacters)
import Klay.Check (checkLayout)
-- import Klay.Check.Locales (checkMissingCharacters)
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Carpalx qualified as Carpalx
import Klay.Keyboard.Layout.Examples.Colemak qualified as Colemak
import Klay.Keyboard.Layout.Examples.Neo2 qualified as Neo2
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Workman qualified as Workman
-- import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.OS (OsSpecific(OsIndependent))
import Klay.Utils.Unicode (showUnicode)
import Klay.Utils.CLDR qualified as CLDR

{- |
Ensure predefined layout have no errors
-}
spec :: Spec
spec =
  describe "ISO" do
    describe "Bépo" do
      it "Normal Check" do
        checkLayout Bépo.layout `shouldBe` mempty
      -- it "Unsupported characters in locales" do
      --   checkMissingCharacters Bépo.layout `shouldBe` mempty
      describe "Check supported characters" checkBépo
      let expected_failures = Set.fromList "ΆΈΉΊΌΎΏΐΪΫάέήίΰϊϋόύώ⁒"
      checkLocale Bépo.layout expected_failures
    describe "Carpalx" do
      it "Check" do
        checkLayout Carpalx.layout `shouldBe` mempty
      let expected_failures = Set.fromList us_expected_failures
      checkLocale Carpalx.layout expected_failures
    describe "Colemak" do
      it "Check" do
        checkLayout Colemak.layout `shouldBe` mempty
      let expected_failures = Set.fromList us_expected_failures
      checkLocale Colemak.layout expected_failures
    describe "Neo2" do
      it "Check" do
        checkLayout Neo2.layout `shouldBe` mempty
      let expected_failures = Set.fromList "†‡"
      checkLocale Neo2.layout expected_failures
    describe "Qwertz_DE" do
      it "Normal check" $ checkLayout DE.layout `shouldBe` mempty
      let expected_failures = Set.fromList "«»‐‑–—‘‚“„‑‰…ÃÅÆØãåæøĀāĂăçëĒēĔĕĞğïĪīĬĭİıŌōŎŏŒœşŪūŬŭÿ"
      checkLocale DE.layout expected_failures
    describe "Qwerty_ES" do
      it "Normal check" $ checkLayout ES.layout `shouldBe` mempty
      let expected_failures = Set.fromList "§«»‐‑–—‘’“”†‡…′″‑‰ÅÆØåæøĀāĂăĒēĔĕĪīĬĭŌōŎŏŒœŪūŬŭ"
      checkLocale ES.layout expected_failures
      -- it "Check missing characters" do
      --   checkMissingCharacters ES.layout `shouldBe` Nothing
    describe "Qwerty_US" do
      it "Check" do
        checkLayout US.layout `shouldBe` mempty
      -- [FIXME] enable this test
      -- it "Only remap alphanumeric, else system default" do
      --   let kas = _virtual_keys_def US.layout
      --   void $ Map.traverseWithKey check_virtual_keys_def kas
      let expected_failures = Set.fromList us_expected_failures
      checkLocale US.layout expected_failures
    describe "Workman" do
      it "Check" do
        checkLayout Workman.layout `shouldBe` mempty
      let expected_failures = Set.fromList us_expected_failures
      checkLocale Workman.layout expected_failures
  where
    us_expected_failures = "§‐‑–—‘’“”†‡…′″‑‰ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÑÒÓÔÖØÙÚÛÜàáâãäåæçèéêëìíîïñòóôöøùúûüÿĀāĂăĒēĔĕĪīĬĭŌōŎŏŒœŪūŬŭŸ"
    -- check_virtual_keys_def key ka
    --   | US.is_custom_key key = ka `shouldBe` VK.ACustom (VK.VirtualKey key)
    --   | otherwise            = ka `shouldBe` VK.osDefault (VK.VirtualKey key)

-- [TODO] Check all locales
checkLocale :: MultiOsRawLayout -> Set.Set Char -> Spec
checkLocale layout expected_failures = do
  describe "Check locales coverage" $ traverse_ checkLocale' mainLoc
  where
    layout_chars = reachableCharacters OsIndependent layout
    mainLoc = _locPrimary locale
    locale = _locales . _metadata $ layout
    check_chars :: Set.Set Char -> Expectation
    check_chars chars
      | not (null unexpected_invalid) = expectationFailure $ "Unexpected unsupported characters: " <> mkChars unexpected_invalid
      | not (null unexpected_valid) = expectationFailure $ "Unexpected supported characters: " <> mkChars unexpected_valid
      | otherwise = pure ()
      where
        supported_chars = Set.intersection chars layout_chars
        unsupported_chars = chars Set.\\ supported_chars
        unexpected_valid = Set.intersection supported_chars expected_failures
        unexpected_invalid = unsupported_chars Set.\\ expected_failures
    mkChars :: Set.Set Char -> String
    mkChars = mconcat . intersperse ", " . fmap (\c -> c: " (" <> showUnicode c <> ")") . Set.toList
    checkLocale' l = do
      let l' = BCP47.toText l
      describe ("Locale: " <> T.unpack l') do
        case CLDR.getExemplars l' of
          Nothing -> error "[ERROR] checkLocale: Locale not found"
          Just cs -> do
            maybe (pure ()) (it "main characters" . check_chars) $ CLDR._exMain cs
            maybe (pure ()) (it "auxiliary characters" . check_chars) $ CLDR._exAuxiliary cs
            maybe (pure ()) (it "punctuation characters" . check_chars) $ CLDR._exPunctuation cs
            maybe (pure ()) (it "numbers characters" . check_chars) $ CLDR._exAuxiliary cs

checkBépo :: Spec
checkBépo = traverse_ check_category bépoSupportedChars
  where
    bépo_chars :: Set.Set Char
    bépo_chars = reachableCharacters OsIndependent Bépo.layout
    check_category (category, sections) = describe category $ traverse_ check_section sections
    check_section (section, chars) = it section $
      let unsupported_chars = filter (`Set.notMember` bépo_chars) chars
      in if null unsupported_chars
        then pure ()
        else expectationFailure $ "Unsupported characters: " <> mkChars unsupported_chars
        --chars `shouldSatisfy` are_supported
    mkChars = mconcat . intersperse ", " . fmap (\c -> c: " (" <> showUnicode c <> ")")

-- | Characters available in Bépo
-- [Source](https://bepo.fr/wiki/Caract%C3%A8res_pris_en_charge)
bépoSupportedChars :: [(String, [(String, String)])]
bépoSupportedChars =
  [ ( "les 142 caractères présents sur les claviers azerty français"
    , [ ( "44 majuscules et 47 minuscules", "AaÀàÂâÄäÃãBbCcçDdEeéÈèÊêËëFfGgHhIiÌìÎîÏïJjKkLlMmNnÑñOoÒòÔôÖöÕõPpQqRrSsTtUuÙùÛûÜüVvWwXxYyÿZz")
      , ( "30 symboles typographiques", "_-'.,;:!?@&§~^`¨°|(){}[]/\\<>\"#")
      , ( "16 chiffres, opérations", "0123456789²*+=%;µ") -- ReplacedBy µ (U+00B5) with μ (U+03BC)
      , ( "4 unités monétaires", "€$¤£")
      , ("les 13 caractères supplémentaires du clavier azerty belge", "´ÁáÉÍíÓóÚúÝý³")
      ]
    )
  , ( "les 15 caractères nécessaires à une composition soignée en français, sans terme étranger"
    , [ ( "7 lettres oubliées", "ÉŸÇÆæŒœ")
      , ( "6 symboles de mise en forme", "«»…–—’")
      , ( "l’espace insécable et l’espace fine insécable", "\xA0\x202F")
      ]
    )
  , ( "les 84 lettres et 6 symboles supplémentaires des 21 autres langues officielles de l’Union européenne utilisant l’alphabet latin"
    , [ ("allemand", "ßẞ„")
      , ("anglais", "“”‘")
      , ("danois", "å Å ø Ø")
      , ("espagnol", "¡ ¿")
      , ("estonien", "š Š ž Ž")
      , ("finnois et suédois", "ä Ä ö Ö å Å")
      , ("hongrois", "ő Ő ű Ű")
      , ("irlandais", "ḃ Ḃ ċ Ċ ḋ Ḋ ḟ Ḟ ġ Ġ ḣ Ḣ ṁ Ṁ ṗ Ṗ ṡ Ṡ ṫ Ṫ")
      , ("italien", "aucun")
      , ("letton", "ā Ā č Č ē Ē ģ Ģ ī Ī ķ Ķ ļ Ļ ņ Ņ ŗ Ŗ š Š ū Ū ž Ž")
      , ("lituanien", "ą Ą č Č ę Ę ė Ė į Į š Š ų Ų ū Ū ž Ž")
      , ("maltais", "ċ Ċ ġ Ġ ħ Ħ ż Ż")
      , ("néerlandais", "ĳ Ĳ")
      , ("polonais", "ą Ą ć Ć ę Ę ł Ł ń Ń ó Ó ś Ś ź Ź ż Ż")
      , ("portugais", "aucun")
      , ("roumain", "ă Ă ș Ș ț Ț")
      , ("slovaque", "č Č ď Ď ĺ Ĺ ľ Ľ ň Ň ŕ Ŕ š Š ť Ť ž Ž")
      , ("slovène", "č Č š Š ž Ž")
      , ("tchèque", "č Č ď Ď ě ň Ň ó Ó ř Ř š Š ť Ť ů Ů ž Ž")
      , ("croate", "č Č ć Ć đ Đ š Š ž Ž") -- [TODO] ǆ ǅ Ǆ ǉ ǈ Ǉ ǌ ǋ Ǌ
      ]
    )
  , ("les caractères suivants, utilisables pour une composition très soignée"
    , [ ("Ponctuation", "· ± × ÷ † ‡ ′ ″ ` • ‹ › ‿ ⁂")
      , ("les exposants", "⁰ ¹ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ ⁺ ⁽ ⁾ ⁼ ⁻")
      , ("les indices", "₀ ₁ ₂ ₃ ₄ ₅ ₆ ₇ ₈ ₉ ₍ ₎ ₊ ₋ ₌ ₐ ₑ ₒ ₓ ₔ")
      ]
    )
  , ( "d’autres caractères pouvant être utiles"
    , [ ("Autres", "¶ ¬ © ® ™ ª º ♯ ♮ ♭")]
    )
  , ( "les lettres des langues suivantes non mentionnées ci-dessus"
    , [ ("asturien", "ḷḷ ḥ")
      , ("gallois", "ŵ ŷ Ŵ Ŷ")
      , ("turc & azéri", "ğ Ğ ı İ Ə ə")
      , ("islandais", "ð Ð þ Þ")
      , ("espéranto", "ĉ ĝ ĥ ĵ ŝ ŭ Ĉ Ĝ Ĥ Ĵ Ŝ Ŭ")
      , ("langues sames", "ŋ Ŋ ŧ Ŧ ʒ Ʒ ǯ Ǯ ǥ Ǥ")
      , ("romani, alphabet romani standard", "ć Ć ç Ç ë Ë ś Ś ź Ź ʒ Ʒ θ Θ ǎ Ǎ ě Ě ǐ Ǐ ǒ Ǒ ǔ Ǔ à À è È ì Ì ò Ò ù Ù ä Ä ö Ö ü Ü")
      ]
    )
  , ( "les lettres du grec monotonique"
    , [ ("Grec", "Α Β Δ Ε Φ Γ Η Ι Θ Κ Λ Μ Ν Ο Π Χ Ρ Σ Τ Υ Ω Ξ Ψ Ζ α β δ ε φ γ η ι θ κ λ μ ν ο π χ ρ σ τ υ ω ξ ψ ζ")
      , ("Variantes", "ϴ ς")
      ]
    )
  , ("les symboles monétaires"
    , [ ("Symboles", "฿ ₵ ¢ ₡ ₫ € ƒ ₲ ₴ ₭ £ ₤ ₥ ₦ ₱ ₨ ৲ ৳ 元 圓 $ ₪ ₮ ₩ ¥ ₳ ₢ ₰ ₯ ₠ ₣ ℳ ₧")]
    )
  -- [TODO] les caractères de l’alphabet phonétique international utilisés pour les langues ci-dessus
  , ("les caractères désuets des langues à script roman en Europe"
    , [ ("Script roman", "ĸ ſ ƶ Ƶ")]
    )
  , ("les lettres des langues et transcriptions suivantes"
    , [ ("vietnamien", "ơ ư Ơ Ư Ạạ Ặặ Ậậ Ẹẹ Ệệ Ịị Ọọ Ộộ Ợợ Ụụ Ựự Ỵỵ Ãã Ẵẵ Ẫẫ Ẽẽ Ễễ Ĩĩ Õõ Ỗỗ Ỡỡ Ũũ Ữữ Ỹỹ etc.")
      , ("hanyu pinyin", "ǎ Ǎ ǐ Ǐ ǒ Ǒ ǔ Ǔ ǚ Ǚ ǘ Ǘ ǜ Ǜ ǖ Ǖ")
      , ("cyrillique ISO 9", "ű Ű ḩ Ḩ ḥ Ḥ ǫ Ǫ") -- [TODO] ʹ ʺ
      , ("arabe ISO 233", "ḍ Ḍ ḏ Ḏ ǧ Ǧ ġ Ġ ḥ Ḥ ẖ š Š ṡ Ṡ ṯ Ṯ ṭ Ṭ ỳ Ỳ ẗ ʿ ʾ")
      , ("grec ISO 843", "á Á É ī Ī í Í ḯ ó Ó ō Ō ṓ Ṓ ý Ý Ÿ")
      , ("hébreu ISO 259-3", "å ă č ḃ ȩ ḝ ǧ ḣ ḥ k̇ ṗ ṭ ŏ ṣ s̀ ś š ŵ ẇ ž ʾ ʿ ˀ ṯ") -- [TODO] ˁ
      ]
    )
    -- [TODO] Add test for les symboles scientifiques primordiaux when implemented
  -- , ("les symboles scientifiques primordiaux"
  --   , [ ("Symboles", "‰ ≃ ≠ ≮ ≯ ≤ ≥ ≰ ≱ ≲ ≳ Ω ¼ ½ ¾ ƒ ℓ ⅓ ⅔ ⅛ ⅜ ⅝ ⅞ ⩽ ⩾ ← ↑ → ↓ ↔ ↦ ⇒ ⇔ ∂ ∙ ∏ ∑ ∆ ∇ √ ∞ ∫ ≈ ≡ ∀ ∃ ∈ ∉ ∪ ∩ ⊂ ⊃ ♀ ♂ ℝ ℂ ℚ ℕ ℤ ℍ ⊥ ‖ ∧ ∨ ⟦ ⟧ ⟨ ⟩ ∘ ")]
  --   )
  , ("autres caractères présents sur certaines dispositions de clavier"
    , [ ("Autres", "‚") ]
    )
  , ("les lettres supplémentaires de l’alphabet latin des langues (officielles ou nationales) des pays où le français est soit officiel, soit largement répandu"
    , [ ("afar (Djibouti)", mempty)
      , ("bariba (Bénin, alphabet national béninois)", "ɔ Ɔ ɛ Ɛ ã Ã ɛ̃ Ɛ̃ ĩ Ĩ ɔ̃ Ɔ̃ ũ Ũ")
      , ("berbère (Maroc, Algérie, écriture tifinagh ou latine)", "č Č ḍ Ḍ ɛ Ɛ ǧ Ǧ ɣ Ɣ ḥ Ḥ ṛ Ṛ ṣ Ṣ ṭ Ṭ ẓ Ẓ")
      , ("bichelamar (Vanuatu)", mempty)
      , ("kikongo (République du Congo, République démocratique du Congo)", mempty)
      , ("langues de Côte d’Ivoire", "ä Ä ë Ë ï Ï ö Ö ü Ü ẅ Ẅ ŋ Ŋ ʔ ɔ Ɔ ɛ Ɛ ɩ Ɩ ʊ Ʊ")
      , ("lingala (République du Congo, République démocratique du Congo, orthographe officielle — pas la plus utilisée)", "á Á â Â ǎ Ǎ é É ê Ê ě Ě ɛ Ɛ ɛ́ Ɛ́ ɛ̂ Ɛ̂ ɛ̌ Ɛ̌ í Í î Î ǐ Ǐ ó Ó ô Ô ǒ Ǒ ɔ Ɔ ɔ́ Ɔ́ ɔ̂ Ɔ̂ ɔ̌ Ɔ̌ ú Ú û Û ǔ Ǔ ")
      , ("tshiluba (orthographe traditionnelle) ou cilubà (nouvelle orthographe) (République démocratique du Congo)", "à À â Â ǎ Ǎ è È ê Ê ě Ě ì Ì î Î ǐ Ǐ ò Ò ô Ô ǒ Ǒ ù Ù û Û ǔ Ǔ ǹ Ǹ m̀ M̀")
      , ("diola (Sénégal)", "à À ì Ì ó Ó ë Ë ñ Ñ ŋ Ŋ")
      , ("éwé (Togo)", "ɖ Ɖ ɛ Ɛ ƒ Ƒ ɣ Ɣ ŋ Ŋ ɔ Ɔ ʋ Ʋ")
      , ("kabiyé (Togo)", "ɖ Ɖ ɛ Ɛ ɣ Ɣ ɩ Ɩ ñ Ñ ŋ Ŋ ɔ Ɔ ʋ Ʋ ʊ Ʊ")
      , ("mahorais (Mayotte, France)", "ɓ Ɓ ɗ Ɗ v̄ V̄")
      , ("manden (Sénégal)", "à À ì Ì ó Ó ë Ë ñ Ñ ǹ Ǹ ŋ Ŋ")
      , ("manjaque (Sénégal)", "ț Ț")
      , ("peul (pulaar) (Sénégal)", "ɓ Ɓ ɗ Ɗ ƙ Ƙ ƥ Ƥ ƭ Ƭ ƈ Ƈ ƴ Ƴ")
      , ("sérère (Sénégal)", "ɓ Ɓ ɗ Ɗ ƙ Ƙ ƥ Ƥ ƭ Ƭ ƈ Ƈ ƴ Ƴ")
      , ("soninké (Sénégal)", "Ñ ñ Ŋ ŋ ’")
      , ("wolof (Sénégal)", "à À ó Ó ë Ë ñ Ñ ŋ Ŋ")
      , ("shimaoré (Mayotte, France)", "ɓ Ɓ ɗ Ɗ v̄ V̄")
      , ("shibushi (Mayotte, France)", "ɓ Ɓ ɗ Ɗ n̈ N̈")
      ]
    )
  ]
