{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.ModifierSpec
  ( spec
  ) where


import Test.Hspec
import Test.QuickCheck

import Data.Alterable
import Klay.Keyboard.Layout.Modifier

spec :: Spec
spec = do
  describe "Modifier" do
    it "maskToInt" do
      -- Base modifiers
      maskToInt noModifier `shouldBe` 0b00000000
      maskToInt shift       `shouldBe` 0b00000001
      maskToInt capitals    `shouldBe` 0b00000010
      maskToInt control     `shouldBe` 0b00000100
      maskToInt alt         `shouldBe` 0b00001000
      maskToInt super       `shouldBe` 0b00010000
      maskToInt numeric     `shouldBe` 0b00100000
      maskToInt isoLevel3   `shouldBe` 0b01000000
      maskToInt isoLevel5   `shouldBe` 0b10000000
      -- Combos
      maskToInt altGr                                 `shouldBe` 0b00001100
      maskToInt (isoLevel3 <> shift)                `shouldBe` 0b01000001
      maskToInt (isoLevel3 <> isoLevel5 <> shift) `shouldBe` 0b11000001
    it "testMask" do
      testMask mempty mempty `shouldBe` True
      testMask mempty shift `shouldBe` True
      testMask shift mempty `shouldBe` False
      testMask shift shift `shouldBe` True
      testMask shift (shift <> isoLevel3) `shouldBe` True
      testMask shift isoLevel3 `shouldBe` False
    it "splitMask" do
      splitMask mempty mempty `shouldBe` Right mempty
      splitMask mempty shift `shouldBe` Right shift
      splitMask shift mempty `shouldBe` Left mempty
      splitMask shift shift `shouldBe` Right mempty
      splitMask shift (shift <> isoLevel3) `shouldBe` Right isoLevel3
      splitMask (shift <> alt) (shift <> isoLevel3) `shouldBe` Right isoLevel3
      splitMask shift isoLevel3 `shouldBe` Left isoLevel3
  describe "ModifiersOptions" do
    describe "Magma instance" do
      it "NoModifiersOptions is the neutral element" do
        property $ \(mo :: ModifiersOptions) -> NoModifiersOptions +> mo == mo && mo +> NoModifiersOptions == mo
      it "Right-biased" do
        forAll (suchThat arbitrary (/= mempty)) $ \(b :: ModifiersOptions) (a :: ModifiersOptions) -> a +> b == b
    describe "Semigroup instance" do
      it "associativity" do
        property $ \(a :: ModifiersOptions) (b :: ModifiersOptions) (c :: ModifiersOptions) -> (a <> b) <> c == a <> (b <> c)
      it "NoModifiersOptions is the neutral element" do
        property $ \(a :: ModifiersOptions) -> NoModifiersOptions <> a == a && a <> NoModifiersOptions == a
    describe "Predefined ModifiersOptions" do
      it "alphabeticNoCancel" do
        alphabeticNoCancel `shouldBe`
          [ (capitals, MTarget shift)
          , (isoLevel2ShiftCaps, MTarget shift) ]
      it "alphabetic" do
        alphabetic `shouldBe`
          [ (capitals, MTarget shift)
          , (isoLevel2ShiftCaps, MTarget mempty) ]
      it "fourLevelSemiAlphabeticNoCancel" do
        fourLevelSemiAlphabeticNoCancel `shouldBe`
          [ (isoLevel1 <> capitals, ModifiersOption isoLevel2 mempty)
          , (isoLevel2 <> capitals, ModifiersOption isoLevel2 mempty)
          , (isoLevel3 <> capitals, ModifiersOption isoLevel3 capitals)
          , (isoLevel4 <> capitals, ModifiersOption isoLevel4 capitals)
          ]
      it "fourLevelSemiAlphabetic" do
        fourLevelSemiAlphabetic `shouldBe`
          [ (isoLevel1 <> capitals, ModifiersOption isoLevel2 mempty)
          , (isoLevel2 <> capitals, ModifiersOption isoLevel1 mempty)
          , (isoLevel3 <> capitals, ModifiersOption isoLevel3 capitals)
          , (isoLevel4 <> capitals, ModifiersOption isoLevel4 capitals)
          ]
      it "fourLevelAlphabeticNoCancel" do
        fourLevelAlphabeticNoCancel `shouldBe`
          [ (isoLevel2Caps, MTarget isoLevel2), (isoLevel2ShiftCaps, MTarget isoLevel2)
          , (isoLevel4Caps, MTarget isoLevel4), (isoLevel4ShiftCaps, MTarget isoLevel4)]
      it "fourLevelAlphabetic" do
        fourLevelAlphabetic `shouldBe`
          [ (isoLevel2Caps, MTarget isoLevel2), (isoLevel2ShiftCaps, MTarget isoLevel1)
          , (isoLevel4Caps, MTarget isoLevel4), (isoLevel4ShiftCaps, MTarget isoLevel3)]
      -- [TODO] other predefined options
    describe "processCapsBehaviour" do
      it "mempty" do
        processCapsBehaviour mempty CapsIsNotShift `shouldBe` [mempty]
        processCapsBehaviour mempty CapsIsShiftCancel `shouldBe` [mempty]
        processCapsBehaviour mempty CapsIsShiftNoCancel `shouldBe` [mempty]
      it "shift" do
        processCapsBehaviour shift CapsIsNotShift `shouldBe` [shift]
        processCapsBehaviour shift CapsIsShiftCancel `shouldBe` [shift, capitals]
        processCapsBehaviour shift CapsIsShiftNoCancel `shouldBe` [shift, capitals, isoLevel2ShiftCaps]
      it "capitals" do
        processCapsBehaviour capitals CapsIsNotShift `shouldBe` [capitals]
        processCapsBehaviour capitals CapsIsShiftCancel `shouldBe` [shift, capitals]
        processCapsBehaviour capitals CapsIsShiftNoCancel `shouldBe` [shift, capitals, isoLevel2ShiftCaps]
      it "shift + capitals" do
        processCapsBehaviour isoLevel2ShiftCaps CapsIsNotShift `shouldBe` [isoLevel2ShiftCaps]
        processCapsBehaviour isoLevel2ShiftCaps CapsIsShiftCancel `shouldBe` [mempty, isoLevel2ShiftCaps]
        processCapsBehaviour isoLevel2ShiftCaps CapsIsShiftNoCancel `shouldBe` [shift, capitals, isoLevel2ShiftCaps]
      it "isoLevel3" do
        processCapsBehaviour isoLevel3 CapsIsNotShift `shouldBe` [isoLevel3]
        processCapsBehaviour isoLevel3 CapsIsShiftCancel `shouldBe` [isoLevel3]
        processCapsBehaviour isoLevel3 CapsIsShiftNoCancel `shouldBe` [isoLevel3]
      it "isoLevel3 + Shift" do
        processCapsBehaviour isoLevel4 CapsIsNotShift `shouldBe` [isoLevel4]
        processCapsBehaviour isoLevel4 CapsIsShiftCancel `shouldBe` [isoLevel4, isoLevel4Caps]
        processCapsBehaviour isoLevel4 CapsIsShiftNoCancel `shouldBe` [isoLevel4, isoLevel4Caps, isoLevel4 <> capitals]
      it "isoLevel3 + Caps" do
        processCapsBehaviour isoLevel4Caps CapsIsNotShift `shouldBe` [isoLevel4Caps]
        processCapsBehaviour isoLevel4Caps CapsIsShiftCancel `shouldBe` [isoLevel4, isoLevel4Caps]
        processCapsBehaviour isoLevel4Caps CapsIsShiftNoCancel `shouldBe` [isoLevel4, isoLevel4Caps, isoLevel4 <> capitals]
      it "isoLevel3 + Shift + Caps" do
        processCapsBehaviour isoLevel4ShiftCaps CapsIsNotShift `shouldBe` [isoLevel4ShiftCaps]
        processCapsBehaviour isoLevel4ShiftCaps CapsIsShiftCancel `shouldBe` [isoLevel3, isoLevel4ShiftCaps]
        processCapsBehaviour isoLevel4ShiftCaps CapsIsShiftNoCancel `shouldBe` [isoLevel4, isoLevel4Caps, isoLevel4 <> capitals]
    describe "capsBehaviourToOptions" do
      it "[(mempty, XXX)]" do
        capsBehaviourToOptions [(mempty, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(mempty, CapsIsShiftCancel)] `shouldBe` [(capitals, MTarget shift), (isoLevel2ShiftCaps, MTarget mempty)]
        capsBehaviourToOptions [(mempty, CapsIsShiftNoCancel)] `shouldBe` [(capitals, MTarget shift), (isoLevel2ShiftCaps, MTarget shift)]
      it "[(shift, XXX)]" do
        capsBehaviourToOptions [(shift, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(shift, CapsIsShiftCancel)] `shouldBe` [(capitals, MTarget shift), (isoLevel2ShiftCaps, MTarget mempty)]
        capsBehaviourToOptions [(shift, CapsIsShiftNoCancel)] `shouldBe` [(capitals, MTarget shift), (isoLevel2ShiftCaps, MTarget shift)]
      it "[(capitals, XXX)]" do
        capsBehaviourToOptions [(capitals, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(capitals, CapsIsShiftCancel)] `shouldBe` [(capitals, MTarget shift), (isoLevel2ShiftCaps, MTarget mempty)]
        capsBehaviourToOptions [(capitals, CapsIsShiftNoCancel)] `shouldBe` [(capitals, MTarget shift), (isoLevel2ShiftCaps, MTarget shift)]
      it "[(isoLevel3, XXX)]" do
        capsBehaviourToOptions [(isoLevel3, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(isoLevel3, CapsIsShiftCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel3)]
        capsBehaviourToOptions [(isoLevel3, CapsIsShiftNoCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel4)]
      it "[(isoLevel3 <> shift, XXX)]" do
        capsBehaviourToOptions [(isoLevel4, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(isoLevel4, CapsIsShiftCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel3)]
        capsBehaviourToOptions [(isoLevel4, CapsIsShiftNoCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel4)]
      it "[(isoLevel3 <> caps, XXX)]" do
        capsBehaviourToOptions [(isoLevel4Caps, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(isoLevel4Caps, CapsIsShiftCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel3)]
        capsBehaviourToOptions [(isoLevel4Caps, CapsIsShiftNoCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel4)]
      it "[(isoLevel3 <> shift <> caps, XXX)]" do
        capsBehaviourToOptions [(isoLevel4 <> caps, CapsIsNotShift)] `shouldBe` []
        capsBehaviourToOptions [(isoLevel4 <> caps, CapsIsShiftCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel3)]
        capsBehaviourToOptions [(isoLevel4 <> caps, CapsIsShiftNoCancel)] `shouldBe` [(isoLevel4Caps, MTarget isoLevel4), (isoLevel4Caps <> shift, MTarget isoLevel4)]
    describe "toCanonicalMask" do
      it "[]" do
        toCanonicalMask mempty mempty           `shouldBe` mempty
        toCanonicalMask mempty shift            `shouldBe` shift
        toCanonicalMask mempty caps             `shouldBe` caps
        toCanonicalMask mempty isoLevel2ShiftCaps       `shouldBe` isoLevel2ShiftCaps
        toCanonicalMask mempty isoLevel3      `shouldBe` isoLevel3
        toCanonicalMask mempty isoLevel4      `shouldBe` isoLevel4
        toCanonicalMask mempty isoLevel4Caps `shouldBe` isoLevel4Caps
        toCanonicalMask mempty isoLevel4ShiftCaps `shouldBe` isoLevel4ShiftCaps
        toCanonicalMask mempty isoLevel7      `shouldBe` isoLevel7
        toCanonicalMask mempty isoLevel8      `shouldBe` isoLevel8
        toCanonicalMask mempty isoLevel8ShiftCaps `shouldBe` isoLevel8ShiftCaps
      it "fourLevelSemiAlphabeticNoCancel" do
        toCanonicalMask fourLevelSemiAlphabeticNoCancel mempty           `shouldBe` mempty
        toCanonicalMask fourLevelSemiAlphabeticNoCancel shift            `shouldBe` shift
        toCanonicalMask fourLevelSemiAlphabeticNoCancel caps             `shouldBe` shift
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel2ShiftCaps       `shouldBe` shift
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel3      `shouldBe` isoLevel3
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel4      `shouldBe` isoLevel4
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel4Caps `shouldBe` isoLevel3
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel4ShiftCaps `shouldBe` isoLevel4
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel7      `shouldBe` isoLevel7
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel8      `shouldBe` isoLevel8
        toCanonicalMask fourLevelSemiAlphabeticNoCancel isoLevel8ShiftCaps `shouldBe` isoLevel8ShiftCaps
      it "fourLevelSemiAlphabetic" do
        toCanonicalMask fourLevelSemiAlphabetic mempty           `shouldBe` mempty
        toCanonicalMask fourLevelSemiAlphabetic shift            `shouldBe` shift
        toCanonicalMask fourLevelSemiAlphabetic caps             `shouldBe` shift
        toCanonicalMask fourLevelSemiAlphabetic isoLevel2ShiftCaps       `shouldBe` mempty
        toCanonicalMask fourLevelSemiAlphabetic isoLevel3      `shouldBe` isoLevel3
        toCanonicalMask fourLevelSemiAlphabetic isoLevel4      `shouldBe` isoLevel4
        toCanonicalMask fourLevelSemiAlphabetic isoLevel4Caps `shouldBe` isoLevel3
        toCanonicalMask fourLevelSemiAlphabetic isoLevel4ShiftCaps `shouldBe` isoLevel4
        toCanonicalMask fourLevelSemiAlphabetic isoLevel7      `shouldBe` isoLevel7
        toCanonicalMask fourLevelSemiAlphabetic isoLevel8      `shouldBe` isoLevel8
        toCanonicalMask fourLevelSemiAlphabetic isoLevel8ShiftCaps `shouldBe` isoLevel8ShiftCaps
      it "fourLevelAlphabeticNoCancel" do
        toCanonicalMask fourLevelAlphabeticNoCancel mempty           `shouldBe` mempty
        toCanonicalMask fourLevelAlphabeticNoCancel shift            `shouldBe` shift
        toCanonicalMask fourLevelAlphabeticNoCancel caps             `shouldBe` shift
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel2ShiftCaps `shouldBe` shift
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel3      `shouldBe` isoLevel3
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel4      `shouldBe` isoLevel4
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel4Caps `shouldBe` isoLevel4
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel4ShiftCaps `shouldBe` isoLevel4
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel7      `shouldBe` isoLevel7
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel8      `shouldBe` isoLevel8
        toCanonicalMask fourLevelAlphabeticNoCancel isoLevel8ShiftCaps `shouldBe` isoLevel8ShiftCaps
      it "fourLevelAlphabetic" do
        toCanonicalMask fourLevelAlphabetic mempty         `shouldBe` mempty
        toCanonicalMask fourLevelAlphabetic shift          `shouldBe` shift
        toCanonicalMask fourLevelAlphabetic caps           `shouldBe` shift
        toCanonicalMask fourLevelAlphabetic isoLevel2ShiftCaps `shouldBe` mempty
        toCanonicalMask fourLevelAlphabetic isoLevel3      `shouldBe` isoLevel3
        toCanonicalMask fourLevelAlphabetic isoLevel4      `shouldBe` isoLevel4
        toCanonicalMask fourLevelAlphabetic isoLevel4Caps  `shouldBe` isoLevel4
        toCanonicalMask fourLevelAlphabetic isoLevel4ShiftCaps `shouldBe` isoLevel3
        toCanonicalMask fourLevelAlphabetic isoLevel7      `shouldBe` isoLevel7
        toCanonicalMask fourLevelAlphabetic isoLevel8      `shouldBe` isoLevel8
        toCanonicalMask fourLevelAlphabetic isoLevel8ShiftCaps `shouldBe` isoLevel8ShiftCaps
    -- let caps_isShift_no_cancel_level3 = capsBehaviourToOptions [(isoLevel4, CapsIsShiftNoCancel)]
    --     caps_isShift_cancel_level3 = capsBehaviourToOptions [(isoLevel4, CapsIsShiftCancel)]
    --     caps_is_notShift_level3 = capsBehaviourToOptions [(isoLevel4, CapsIsNotShift)]
    let processModifiersOptions' = processModifiersOptions . mconcat
    describe "processModifiersOptions" do
      let capitals_is_notShift = mempty
      describe "[]" do
        it "CapsIsNotShift" do
          processModifiersOptions' [capitals_is_notShift] mempty `shouldBe` [mempty]
        it "CapsIsShiftNoCancel" do
          processModifiersOptions' [fourLevelSemiAlphabeticNoCancel] mempty `shouldBe` [mempty]
        it "CapsIsShiftCancel" do
          processModifiersOptions' [fourLevelSemiAlphabeticNoCancel] mempty `shouldBe` [mempty]
      describe "[Shift]" do
        let r1 = [[Shift]]
            r2 = [[Shift], [Capitals], [Shift, Capitals]]
            r3 = [[Shift], [Capitals]]
        it "CapsIsNotShift" do
          processModifiersOptions' [capitals_is_notShift] [Shift] `shouldBe` r1
        it "CapsIsShiftNoCancel" do
          processModifiersOptions' [fourLevelSemiAlphabeticNoCancel] [Shift] `shouldBe` r2
        it "CapsIsShiftCancel" do
          processModifiersOptions' [fourLevelSemiAlphabetic] [Shift] `shouldBe` r3
      describe "[Capitals]" do
        let ms = [Capitals]
            r1 = [[Capitals]]
            r2 = [[Shift], [Capitals], [Shift, Capitals]]
            r3 = [[Shift], [Capitals]]
        it "CapsIsNotShift" do
          processModifiersOptions' [capitals_is_notShift] ms `shouldBe` r1
        it "CapsIsShiftNoCancel" do
          processModifiersOptions' [fourLevelSemiAlphabeticNoCancel] ms `shouldBe` r2
        it "CapsIsShiftCancel" do
          processModifiersOptions' [fourLevelSemiAlphabetic] ms `shouldBe` r3
      describe "[Shift, Capitals]" do
        let ms = [Shift, Capitals]
            r1 = [[Shift, Capitals]]
            r2 = [[Shift], [Capitals], [Shift, Capitals]]
            r3 = [[], [Shift, Capitals]]
        it "CapsIsNotShift" do
          processModifiersOptions' [capitals_is_notShift] ms `shouldBe` r1
        it "CapsIsShiftNoCancel" do
          processModifiersOptions' [fourLevelSemiAlphabeticNoCancel] ms `shouldBe` r2
        it "CapsIsShiftCancel" do
          processModifiersOptions' [fourLevelSemiAlphabetic] ms `shouldBe` r3
      describe "[IsoLevel3, Shift]" do
        let ms = [IsoLevel3, Shift]
        it "capitals_is_notShift" do
          processModifiersOptions' [capitals_is_notShift] ms `shouldBe` [ms]
        it "fourLevelSemiAlphabeticNoCancel" do
          processModifiersOptions' [fourLevelSemiAlphabeticNoCancel] ms `shouldBe` [ms, ms <> capitals]
        it "fourLevelSemiAlphabetic" do
          processModifiersOptions' [fourLevelSemiAlphabetic] ms `shouldBe` [ms, ms <> capitals]
        -- it "CapsIsNotShift (Base, Level3)" do
        --   processModifiersOptions' [capitals_is_notShift, caps_is_notShift_level3] ms `shouldBe` [ms]
        -- it "CapsIsShiftNoCancel (Base, Level3)" do
        --   processModifiersOptions' [fourLevelSemiAlphabeticNoCancel, caps_isShift_no_cancel_level3] ms `shouldBe` [[IsoLevel3, Shift], [IsoLevel3, Capitals], [IsoLevel3, Shift, Capitals]]
        -- it "CapsIsShiftCancel (Base, Level3)" do
        --   processModifiersOptions' [fourLevelSemiAlphabeticNoCancel, caps_isShift_cancel_level3] ms `shouldBe` [[IsoLevel3, Shift], [IsoLevel3, Capitals]]
      describe "isoLevel6" do
        it "eightLevelLevelFiveLock" do
          processModifiersOptions eightLevelLevelFiveLock isoLevel6 `shouldBe`
            [isoLevel6, isoLevel6ShiftCaps, numeric <> isoLevel2, numeric <> isoLevel2ShiftCaps]


