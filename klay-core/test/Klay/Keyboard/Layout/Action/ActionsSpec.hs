{-# LANGUAGE OverloadedLists  #-}

module Klay.Keyboard.Layout.Action.ActionsSpec
  ( spec
  , mkActions'
  , aa, aà, aα, aA, ab, aB
  , as1A, as1B, asX1, asX1', asX2, asX2', asX3, asX3'
  , asX1'_opts, asX2'_opts, asX3'_opts
  ) where


import Test.Hspec
import Test.QuickCheck

import Data.IntMap.NonEmpty qualified as NEIntMap

import Klay.Keyboard.Layout.Action hiding (Insert, Delete)
import Klay.Keyboard.Layout.Action.Actions
import Klay.Keyboard.Layout.Modifier
import Data.Alterable

import Klay.Keyboard.Layout.Action.ActionSpec hiding (spec)

as1A, as1B, asX1, asX1', asX2, asX2', asX3, asX3' :: Actions
asX1'_opts, asX2'_opts, asX3'_opts :: ModifiersOptions
as1A = SingleAction aA
as1B = SingleAction aB
asX1 = mkActions' mempty [(noModifier, aa), (shift, aA)]
asX1'_opts = fourLevelSemiAlphabeticNoCancel
asX1' = mkActions' asX1'_opts [(noModifier, aa), (shift, aA)]
asX2 = mkActions' mempty [(noModifier, aα)]
asX2'_opts = fourLevelSemiAlphabetic
asX2' = mkActions' asX2'_opts [(noModifier, aα)]
asX3 = mkActions' mempty [(isoLevel3, aà)]
asX3' = mkActions' asX3'_opts [(isoLevel3, aà)]
asX3'_opts = [(isoLevel4ShiftCaps, MTarget isoLevel4)]


spec :: Spec
spec = do
  describe "Actions" do
    describe "Magma instance" do
      -- it "associativity" do
      --   property $ \(a :: Actions) (b :: Actions) (c :: Actions) -> (a +> b) +> c == a +> (b +> c)
      it "UndefinedActions is the neutral element" do
        property $ \(a :: Actions) -> UndefinedActions +> a == a && a +> UndefinedActions == a
      it "NoActions absorbs when on the right side" do
        property $ \(a :: Actions) -> a +> NoActions == NoActions
      it "SingleAction absorbs when on the right hand side" do
        forAll arbitraryDefinedSingleAction $ \s a -> a +> s == s
    describe "Semigroup instance" do
      it "associativity" do
        property $ \(a :: Actions) (b :: Actions) (c :: Actions) -> (a <> b) <> c == a <> (b <> c)
      it "UndefinedActions is the neutral element" do
        property $ \(a :: Actions) -> UndefinedActions <> a == a && a <> UndefinedActions == a
      -- it "NoActions absorbs when on the right side, else it is overrided" do
      --   property $ \(a :: Actions) -> a <> NoActions == NoActions
      -- it "MultiActions" do
      --   asX1 <> asX1' `shouldBe` asX1'
      --   asX1 <> asX2 `shouldBe` mkActions' mempty [(noModifier, aα), (shift, aA)]
      --   asX2 <> asX1 `shouldBe` asX1
      --   asX1' <> asX2' `shouldBe` mkActions' asX2'_opts [(noModifier, aα), (shift, aA)]
      --   asX2' <> asX1' `shouldBe` asX1'
      --   asX1 <> asX3 `shouldBe` mkActions' mempty [(noModifier, aa), (shift, aA), (isoLevel3, aà)]
      --   asX3 <> asX1 `shouldBe` mkActions' mempty [(noModifier, aa), (shift, aA), (isoLevel3, aà)]
      --   asX1' <> asX3' `shouldBe` mkActions' (Just [(noModifier, CapsIsShiftNoCancel), (isoLevel3, CapsIsShiftNoCancel)]) [(noModifier, aa), (shift, aA), (isoLevel3, aà)]
      -- it "SingleActions" do
      --   as1A <> as1B `shouldBe` as1B
      --   as1B <> as1A `shouldBe` as1A
      --   --
      --   asX1 <> as1B `shouldBe` as1B
      --   asX1' <> as1B `shouldBe` as1B
      --   --
      --   as1B <> asX1 `shouldBe` mkActions Nothing (Just aB) [(noModifier, aa), (shift, aA)]
      --   as1B <> asX1' `shouldBe` mkActions asX1'_opts (Just aB) [(noModifier, aa), (shift, aA)]
    describe "IsList instance" do
      it "Empty" do
        [] `shouldBe` NoActions
        [[]] `shouldBe` UndefinedActions
        [[], []] `shouldBe` UndefinedActions
        [[(noModifier, UndefinedAction)], [(isoLevel3, UndefinedAction)]] `shouldBe` UndefinedActions
      it "One element" do
        [[(noModifier, NoAction)]] `shouldBe` mkActions' mempty [(noModifier, NoAction)]
        [[(noModifier, aa), (noModifier, ab)]] `shouldBe` mkActions' mempty [(noModifier, ab)]
        [[(noModifier, aa)], [(noModifier, ab)]] `shouldBe` mkActions' mempty [(noModifier, ab)]
      it "Two elements" do
        [[(noModifier, aa), (shift, aA)]] `shouldBe` mkActions' mempty [(noModifier, aa), (shift, aA)]
        [[(noModifier, aa)], [(shift, aA)]] `shouldBe` mkActions' mempty [(noModifier, aa), (shift, aA)]
        [[(noModifier, aa), (noModifier, ab)], [(shift, aB)]] `shouldBe` mkActions' mempty [(noModifier, ab), (shift, aB)]
        [[(noModifier, aa)], [(noModifier, ab), (shift, aB)]] `shouldBe` mkActions' mempty [(noModifier, ab), (shift, aB)]

    it "mkActions" do
      mkActions [(caps, ModifiersOption mempty mempty)] NoAction [(caps, AChar 'a')] `shouldBe` Actions [(caps, ModifiersOption mempty mempty)] NoAction (NEIntMap.fromList [(0, AChar 'a')])
      -- [TODO]

  describe "instance Differentiable Actions ActionsAlterations" do
    it "Single actions" do
      forAll arbitrarySingleAction $ \as -> diff as as == (Nothing :: Maybe ActionsAlterations)
    describe "Actions" do
      it "identical" do
        forAll arbitraryMultiActions $ \as -> diff as as == (Nothing :: Maybe ActionsAlterations)
      it "non identical" do
        diff (mkActions mempty NoAction [(mempty, AChar 'a')]) (mkActions [(caps, ModifiersOption mempty mempty)] NoAction [(caps, AChar 'a')]) `shouldBe`
          Just (ActionsAlterations {_aOptions = Just (InsertOrUpdate [(caps, ModifiersOption mempty mempty)] [(2,InsertOrReplace (ModifiersOption mempty mempty))]), _aDefault = Nothing, _aLevels = Nothing})
        diff (mkActions [(caps, ModifiersOption mempty mempty)] NoAction [(caps, AChar 'a')]) (mkActions mempty NoAction [(mempty, AChar 'a')]) `shouldBe`
          Just (ActionsAlterations {_aOptions = Just (Delete :: ModifiersOptionsAlteration), _aDefault = Nothing, _aLevels = Nothing})
    it "Mix" do
      diff (SingleAction NoAction) (mkActions mempty NoAction [(mempty, AChar 'a')]) `shouldBe` Just (mkActionsAlterations Nothing Nothing [(mempty, Replace $ AChar 'a')])
      diff (mkActions mempty NoAction [(mempty, AChar 'a')]) (SingleAction NoAction) `shouldBe` Just (ActionsAlterations Nothing Nothing (Just Reset))
  describe "instance Alterable Actions ActionsAlterations" do
    it "malter (diff a1 a2) a1 == a2" do
      property $ \(a :: Actions) (b :: Actions) -> malter (diff a b :: Maybe ActionsAlterations) a == b

  describe "ActionsAlterations" do
    describe "IsList instance" do
      it "Empty" do
        [] `shouldBe` ResetActionsAlt
        [[]] `shouldBe` EmptyActionsAlt
    describe "Magma instance" do
      it "EmptyActionsAlt is the neutral element" do
        property $ \(a :: ActionsAlterations) -> EmptyActionsAlt +> a == a && a +> EmptyActionsAlt == a
    -- describe "Semigroup instance" do
    --   it "associativity" do
    --     property $ \(a :: ActionsAlterations) (b :: ActionsAlterations) (c :: ActionsAlterations) -> (a <> b) <> c == a <> (b <> c)
    --   it "EmptyActionsAlt is the neutral element" do
    --     property $ \(a :: ActionsAlterations) -> EmptyActionsAlt <> a == a && a <> EmptyActionsAlt == a


mkActions' :: ModifiersOptions -> [(Level, Action)] -> Actions
mkActions' opts = mkActions opts mempty
