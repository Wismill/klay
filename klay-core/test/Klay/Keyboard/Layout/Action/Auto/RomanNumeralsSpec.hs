module Klay.Keyboard.Layout.Action.Auto.RomanNumeralsSpec
  ( spec
  ) where


import Data.List ((\\))
import Test.Hspec

import Klay.Keyboard.Hardware.Key (Key(..))
import Klay.Keyboard.Layout.Action.Auto.RomanNumerals

keyDecimalNumbers, keyNumpad, keyOther, keyNotANumber :: [Key]
keyDecimalNumbers = enumFromTo N1 N9
keyNumpad          = enumFromTo KP1 KP9
keyOther           = [I, V, X, N0, KP0, L, C, D, M]
keyNotANumber    = enumFrom minBound \\ (keyDecimalNumbers <> keyNumpad <> keyOther)


spec :: Spec
spec = do
  describe "keyToRomanNumeralUpperCase" do
    it "Number row: 1-9" do
      mkTest' keyToRomanNumeralUpperCase keyDecimalNumbers "ⅠⅡⅢⅣⅤⅥⅦⅧⅨ"
    it "Numpad: 1-9" do
      mkTest' keyToRomanNumeralUpperCase keyNumpad "ⅠⅡⅢⅣⅤⅥⅦⅧⅨ"
    it "Other" do
      mkTest' keyToRomanNumeralUpperCase keyOther "ⅠⅤⅩⅩⅩⅬⅭⅮⅯ"
    it "Not a roman number" do
      mkTest keyToRomanNumeralUpperCase keyNotANumber (fmap (const Nothing) keyNotANumber)
  describe "keyToRomanNumeralLowerCase" do
    it "Number row: 1-9" do
      mkTest' keyToRomanNumeralLowerCase keyDecimalNumbers "ⅰⅱⅲⅳⅴⅵⅶⅷⅸ"
    it "Numpad: 1-9" do
      mkTest' keyToRomanNumeralLowerCase keyNumpad "ⅰⅱⅲⅳⅴⅵⅶⅷⅸ"
    it "Other" do
      mkTest' keyToRomanNumeralLowerCase keyOther "ⅰⅴⅹⅹⅹⅼⅽⅾⅿ"
    it "Not a roman number" do
      mkTest keyToRomanNumeralLowerCase keyNotANumber (fmap (const Nothing) keyNotANumber)
  where
    mkTest convert vks expected = fmap convert vks `shouldBe` expected
    mkTest' convert vks expected = mkTest convert vks (fmap Just expected)
