module Klay.Keyboard.Layout.Action.Auto.GreekAlphabetSpec
  ( spec
  ) where


import Data.List ((\\))
import Test.Hspec

import Klay.Keyboard.Hardware.Key (Key(..))
import Klay.Keyboard.Layout.Action.Auto.GreekAlphabet

vkGreekLetters :: [Key]
vkGreekLetters = [A, B, G, D, E, Z, H, U, I, K, L, M, N, J, O, P, R, S, T, Y, F, X, C, V]

spec :: Spec
spec = do
  describe "greek" do
    mkTests greek vkGreekLetters vkGreekLetters "αβγδεζηθικλμνξοπρστυφχψω" "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ"
  describe "greekMathematicalItalic" do
    mkTests greekMathematicalItalic vkGreekLetters vkGreekLetters "𝛼𝛽𝛾𝛿𝜀𝜁𝜂𝜃𝜄𝜅𝜆𝜇𝜈𝜉𝜊𝜋𝜌𝜎𝜏𝜐𝜑𝜒𝜓𝜔" "𝛢𝛣𝛤𝛥𝛦𝛧𝛨𝛩𝛪𝛫𝛬𝛭𝛮𝛯𝛰𝛱𝛲𝛴𝛵𝛶𝛷𝛸𝛹𝛺"
  describe "greekMathematicalBold" do
    mkTests greekMathematicalBold vkGreekLetters vkGreekLetters "𝛂𝛃𝛄𝛅𝛆𝛇𝛈𝛉𝛊𝛋𝛌𝛍𝛎𝛏𝛐𝛑𝛒𝛔𝛕𝛖𝛗𝛘𝛙𝛚" "𝚨𝚩𝚪𝚫𝚬𝚭𝚮𝚯𝚰𝚱𝚲𝚳𝚴𝚵𝚶𝚷𝚸𝚺𝚻𝚼𝚽𝚾𝚿𝛀"
  describe "greekMathematicalBoldItalic" do
    mkTests greekMathematicalBoldItalic vkGreekLetters vkGreekLetters "𝜶𝜷𝜸𝜹𝜺𝜻𝜼𝜽𝜾𝜿𝝀𝝁𝝂𝝃𝝄𝝅𝝆𝝈𝝉𝝊𝝋𝝌𝝍𝝎" "𝜜𝜝𝜞𝜟𝜠𝜡𝜢𝜣𝜤𝜥𝜦𝜧𝜨𝜩𝜪𝜫𝜬𝜮𝜯𝜰𝜱𝜲𝜳𝜴"
  describe "greekMathematicalDoubleStruck" do
    mkTests greekMathematicalDoubleStruck [G, P] [G, P, S] "ℽℼ" "ℾℿ⅀"
  where
    mkTests :: (Key -> (Maybe Char, Maybe Char)) -> [Key] -> [Key] -> String -> String -> Spec
    mkTests convert vksLower vksUpper expectedLower expectedUpper = do
      describe "Letters" do
        mkTest' convert vksLower vksUpper expectedLower expectedUpper
      describe "Other keys" do
        let vksLower_not = enumFrom minBound \\ vksLower
        let vksUpper_not = enumFrom minBound \\ vksUpper
        mkTest convert
          vksLower_not vksUpper_not
          (fmap (const Nothing) vksLower_not)
          (fmap (const Nothing) vksUpper_not)
    mkTest convert vksLower vksUpper expectedLower expectedUpper = do
      let resultLower = fmap (fst . convert) vksLower
      let resultUpper = fmap (snd . convert) vksUpper
      it "Lower case" $ resultLower `shouldBe` expectedLower
      it "Upper case" $ resultUpper `shouldBe` expectedUpper
    mkTest' convert vksLower vksUpper expectedLower expectedUpper =
      mkTest convert vksLower vksUpper (fmap Just expectedLower) (fmap Just expectedUpper)
