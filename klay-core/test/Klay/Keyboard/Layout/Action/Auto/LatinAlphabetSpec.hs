module Klay.Keyboard.Layout.Action.Auto.LatinAlphabetSpec
  ( spec
  , keyNotLetters
  , notLetters
  ) where


import Data.List ((\\))
import Test.Hspec

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.KeySpec hiding (spec)
import Klay.Keyboard.Layout.Action.Auto.LatinAlphabet

keyNotLetters :: [Key]
keyNotLetters = enumFromTo minBound maxBound \\ keyLetters

notLetters :: [Maybe Char]
notLetters = fmap (const Nothing) keyNotLetters


spec :: Spec
spec = do
  describe "latin" do
    describe "Letters" do
      mkTest' latin keyLetters ['a'..'z'] ['A'..'Z']
    describe "Other keys" do
      mkTest latin keyNotLetters notLetters notLetters
  describe "latinMathematicalItalic" do
    describe "Letters" do
      mkTest' latinMathematicalItalic keyLetters "𝑎𝑏𝑐𝑑𝑒𝑓𝑔ℎ𝑖𝑗𝑘𝑙𝑚𝑛𝑜𝑝𝑞𝑟𝑠𝑡𝑢𝑣𝑤𝑥𝑦𝑧" "𝐴𝐵𝐶𝐷𝐸𝐹𝐺𝐻𝐼𝐽𝐾𝐿𝑀𝑁𝑂𝑃𝑄𝑅𝑆𝑇𝑈𝑉𝑊𝑋𝑌𝑍"
    describe "Other keys" do
      mkTest latinMathematicalItalic keyNotLetters notLetters notLetters
  describe "latinMathematicalBold" do
    describe "Letters" do
      mkTest' latinMathematicalBold keyLetters "𝐚𝐛𝐜𝐝𝐞𝐟𝐠𝐡𝐢𝐣𝐤𝐥𝐦𝐧𝐨𝐩𝐪𝐫𝐬𝐭𝐮𝐯𝐰𝐱𝐲𝐳" "𝐀𝐁𝐂𝐃𝐄𝐅𝐆𝐇𝐈𝐉𝐊𝐋𝐌𝐍𝐎𝐏𝐐𝐑𝐒𝐓𝐔𝐕𝐖𝐗𝐘𝐙"
    describe "Other keys" do
      mkTest latinMathematicalBold keyNotLetters notLetters notLetters
  describe "latinMathematicalBoldItalic" do
    describe "Letters" do
      mkTest' latinMathematicalBoldItalic keyLetters "𝒂𝒃𝒄𝒅𝒆𝒇𝒈𝒉𝒊𝒋𝒌𝒍𝒎𝒏𝒐𝒑𝒒𝒓𝒔𝒕𝒖𝒗𝒘𝒙𝒚𝒛" "𝑨𝑩𝑪𝑫𝑬𝑭𝑮𝑯𝑰𝑱𝑲𝑳𝑴𝑵𝑶𝑷𝑸𝑹𝑺𝑻𝑼𝑽𝑾𝑿𝒀𝒁"
    describe "Other keys" do
      mkTest latinMathematicalBoldItalic keyNotLetters notLetters notLetters
  describe "latinMathematicalScript" do
    describe "Letters" do
      mkTest' latinMathematicalScript keyLetters "𝒶𝒷𝒸𝒹ℯ𝒻ℊ𝒽𝒾𝒿𝓀𝓁𝓂𝓃ℴ𝓅𝓆𝓇𝓈𝓉𝓊𝓋𝓌𝓍𝓎𝓏" "𝒜ℬ𝒞𝒟ℰℱ𝒢ℋℐ𝒥𝒦ℒℳ𝒩𝒪𝒫𝒬ℛ𝒮𝒯𝒰𝒱𝒲𝒳𝒴𝒵"
    describe "Other keys" do
      mkTest latinMathematicalScript keyNotLetters notLetters notLetters
  describe "latinMathematicalScriptBold" do
    describe "Letters" do
      mkTest' latinMathematicalScriptBold keyLetters "𝓪𝓫𝓬𝓭𝓮𝓯𝓰𝓱𝓲𝓳𝓴𝓵𝓶𝓷𝓸𝓹𝓺𝓻𝓼𝓽𝓾𝓿𝔀𝔁𝔂𝔃" "𝓐𝓑𝓒𝓓𝓔𝓕𝓖𝓗𝓘𝓙𝓚𝓛𝓜𝓝𝓞𝓟𝓠𝓡𝓢𝓣𝓤𝓥𝓦𝓧𝓨𝓩"
    describe "Other keys" do
      mkTest latinMathematicalScriptBold keyNotLetters notLetters notLetters
  describe "latinMathematicalFraktur" do
    describe "Letters" do
      mkTest' latinMathematicalFraktur keyLetters "𝔞𝔟𝔠𝔡𝔢𝔣𝔤𝔥𝔦𝔧𝔨𝔩𝔪𝔫𝔬𝔭𝔮𝔯𝔰𝔱𝔲𝔳𝔴𝔵𝔶𝔷" "𝔄𝔅ℭ𝔇𝔈𝔉𝔊ℌℑ𝔍𝔎𝔏𝔐𝔑𝔒𝔓𝔔ℜ𝔖𝔗𝔘𝔙𝔚𝔛𝔜ℨ"
    describe "Other keys" do
      mkTest latinMathematicalFraktur keyNotLetters notLetters notLetters
  describe "latinMathematicalFrakturBold" do
    describe "Letters" do
      mkTest' latinMathematicalFrakturBold keyLetters "𝖆𝖇𝖈𝖉𝖊𝖋𝖌𝖍𝖎𝖏𝖐𝖑𝖒𝖓𝖔𝖕𝖖𝖗𝖘𝖙𝖚𝖛𝖜𝖝𝖞𝖟" "𝕬𝕭𝕮𝕯𝕰𝕱𝕲𝕳𝕴𝕵𝕶𝕷𝕸𝕹𝕺𝕻𝕼𝕽𝕾𝕿𝖀𝖁𝖂𝖃𝖄𝖅"
    describe "Other keys" do
      mkTest latinMathematicalFrakturBold keyNotLetters notLetters notLetters
  describe "latinMathematicalDoubleStruck" do
    describe "Letters" do
      mkTest' latinMathematicalDoubleStruck keyLetters "𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫" "𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ"
    describe "Other keys" do
      mkTest latinMathematicalDoubleStruck keyNotLetters notLetters notLetters
  where
    f (Just (a, b)) = (Just a, Just b)
    f Nothing       = (Nothing, Nothing)
    mkTest convert vks expectedLower expectedUpper = do
      let (resultLower, resultUpper) = unzip . fmap (f . convert) $ vks
      it "Lower case" $ resultLower `shouldBe` expectedLower
      it "Upper case" $ resultUpper `shouldBe` expectedUpper
    mkTest' convert vks expectedLower expectedUpper =
      mkTest convert vks (fmap Just expectedLower) (fmap Just expectedUpper)
