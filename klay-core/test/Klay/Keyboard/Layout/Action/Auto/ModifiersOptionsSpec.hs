module Klay.Keyboard.Layout.Action.Auto.ModifiersOptionsSpec
  ( spec
  ) where


import Test.Hspec
import Test.QuickCheck

import Klay.Keyboard.Layout.Action hiding (Delete)
import Data.Alterable


spec :: Spec
spec = do
  describe "AutoActions" do
    describe "Magma instance" do
      it "Neutral element: AutoUndefinedActions" do
        property $ \a -> AutoUndefinedActions +> a == a && a +> AutoUndefinedActions == a
    describe "Semigroup instance" do
      it "associativity" do
        property $ \(a :: AutoActions) b c -> (a <> b) <> c == a <> (b <> c)
      it "Neutral element: AutoUndefinedActions" do
        property $ \a -> AutoUndefinedActions <> a == a && a <> AutoUndefinedActions == a
    describe "instance Alterable AutoUndefinedActions AutoActionsAlterations" do
      it "malter (diff a1 a2) a1 == a2" do
        property $ \(a :: AutoActions) (b :: AutoActions) -> malter (diff a b :: Maybe AutoActionsAlterations) a == b
