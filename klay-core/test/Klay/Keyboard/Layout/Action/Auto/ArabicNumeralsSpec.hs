module Klay.Keyboard.Layout.Action.Auto.ArabicNumeralsSpec
  ( spec
  ) where


import Control.Monad ((>=>))
import Test.Hspec

import Klay.Keyboard.Layout.Action (Action(..))
import Klay.Keyboard.Layout.Action.Auto.ArabicNumerals
import Klay.Keyboard.Hardware.KeySpec hiding (spec)

spec :: Spec
spec = do
  describe "toWesternArabicNumeral" do
    mkTests toWesternArabicNumeral decimalNumbersChar
  describe "toWesternArabicNumeralSubscript" do
    mkTests toWesternArabicNumeralSubscript (fmap Just "₀₁₂₃₄₅₆₇₈₉")
  describe "toWesternArabicNumeralSuperscript" do
    mkTests toWesternArabicNumeralSuperscript (fmap Just "⁰¹²³⁴⁵⁶⁷⁸⁹")
  describe "toWesternArabicNumeralDoubleStruck" do
    mkTests toWesternArabicNumeralDoubleStruck (fmap Just "𝟘𝟙𝟚𝟛𝟜𝟝𝟞𝟟𝟠𝟡")
  describe "toWesternArabicNumeralBold" do
    mkTests toWesternArabicNumeralBold (fmap Just "𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗")
  where
    number_from_vk f = f >=> \case
      AChar c -> Just c
      _       -> Nothing
    mkTests convert expected = do
      let number_from_vk' = number_from_vk convert
      it "Number row" do
        fmap number_from_vk' keyDecimalNumbers `shouldBe` expected
      it "Numpad" do
        fmap number_from_vk' keyNumpad `shouldBe` expected
      it "Other keys" do
        fmap number_from_vk' keyNotADecimalNumber `shouldBe` notADecimalNumberChar
