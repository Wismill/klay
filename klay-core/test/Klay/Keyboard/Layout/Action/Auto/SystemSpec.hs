
-- {-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Action.Auto.SystemSpec
  ( spec
  ) where

import Data.Maybe
import Data.Foldable (traverse_)
-- import Control.Monad ((>=>))
import Test.Hspec

-- import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.Key qualified as K
-- import Klay.Keyboard.Layout.Action (Action(..))
import Klay.Keyboard.Layout.Action.Auto.System (defaultSystemAction)
-- import Klay.Keyboard.Layout.Modifier qualified as M

{- |
Ensure that the synchronisation of virtual keys and actions
-}
spec :: Spec
spec = do
  it "System keys" do
    let vks = enumFromTo minBound (pred K.LShift)
    traverse_ (\key -> key `shouldSatisfy` (isJust . defaultSystemAction)) vks
  -- describe "Special actions" do
  --   let vks = enumFromTo minBound (pred K.LShift)
  --   let actions = actions_from_vks vks
  --   it "Actions" do
  --     fmap defaultSystemAction vks `shouldBe` actions
  -- describe "Modifiers" do
  --   let vks = K.keyGroupModifiers
  --   let modifiers = Just <$> [[M.Shift], [M.Shift], [M.Capitals], [M.Control], [M.Control], [M.Alternate], [M.Alternate], [M.Super], [M.Super], [M.Numeric]]
  --   it "Actions" do
  --     fmap modifier_from_vk vks `shouldBe` modifiers
  describe "Other keys" do
    let vks = enumFrom (succ . last $ K.keyGroupModifiers)
    it "Actions" do
      fmap defaultSystemAction vks `shouldBe` replicate (length vks) Nothing
  -- where
    -- actions_from_vks :: [Key] -> [Maybe Action]
    -- actions_from_vks ks = Just . ASpecial . toEnum . fromEnum <$> ks
    --modifier_from_action :: [Action] -> [Maybe ModifierBit]
    -- modifier_from_vk = defaultSystemAction >=> \case
    --   AModifier m -> Just . M._mBit $ m
    --   _           -> Nothing
