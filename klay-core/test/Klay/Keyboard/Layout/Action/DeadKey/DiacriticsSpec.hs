module Klay.Keyboard.Layout.Action.DeadKey.DiacriticsSpec
  ( spec
  ) where

-- import Data.Set qualified as Set
-- import Data.Map.Strict qualified as Map
-- import Data.Foldable (traverse_)

import Test.Hspec

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
-- import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All qualified as Bépo
-- import Klay.Utils.UserInput (raw_text_to_list)

spec :: Spec
spec =
  describe "No conflict" do
    it "No duplicate key" do
      _dkOverlappingBaseChars errors `shouldBe` mempty
    it "No duplicate combo" do
      _dkOverlappingCombos errors `shouldBe` mempty
    -- describe "Comparison with Bépo" do
    --   traverse_ compare_with_bépo Bépo.bépoDeadKeys
  where
    (_diacriticsDef, errors) = mkDeadKeyDefinitionsWithDetails DK.diacriticsDef
    -- (diacriticsDef, errors) = mkDeadKeyDefinitionsWithDetails DK.diacriticsDef
    -- bépoDefs = fst $ mkDeadKeyDefinitionsWithDetails Bépo.bépoDef
    -- compare_with_bépo dk = case Map.lookup dk diacriticsDef of
    --   Nothing -> pure ()
    --   Just def -> it (raw_text_to_list . _dkName $ dk) do
    --     Set.difference bépoDef def `shouldBe` mempty
    --   where
    --     bépoDef = bépoDefs Map.! dk
