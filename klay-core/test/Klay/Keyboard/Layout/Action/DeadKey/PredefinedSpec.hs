{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Action.DeadKey.PredefinedSpec
  ( spec
  ) where

import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
-- import Data.Bifunctor (first, second)
-- import Data.Foldable (traverse_)

import Test.Hspec

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Predefined (predefinedDeadKeys)

-- [TODO] Better check using mkDeadKeyDefinitionsWithDetails
spec :: Spec
spec =
  describe "No conflict" do
    it "No duplicate base char" do
      check_duplicate_base_char predefinedDeadKeys `shouldBe` mempty
      -- _dkOverlappingBaseChars errors `shouldBe` mempty
    -- it "No duplicate combo" do
    --   _dkOverlappingCombos errors `shouldBe` mempty
  where
    check_duplicate_base_char = fst . foldr go mempty
      where
        go dk@DeadKey{_dkBaseChar=c} (duplicates, dks) =
          case Map.lookup c dks >>= check_alias dk of
            Nothing  -> (duplicates, Map.insert c dk dks)
            Just dk' -> (Map.insertWith Set.union c [dk', dk] duplicates, dks)
        check_alias dk dk' = if dk == dk' then Nothing else Just dk'
    -- (_diacriticsDef, errors) = mkDeadKeyDefinitionsWithDetails DK.diacriticsDef
