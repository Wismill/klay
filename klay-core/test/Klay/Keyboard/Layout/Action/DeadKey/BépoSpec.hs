module Klay.Keyboard.Layout.Action.DeadKey.BépoSpec
  ( spec
  ) where


import Test.Hspec

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Bépo qualified as DK


spec :: Spec
spec = do
  -- [TODO] Add check that the combining and the base char share name
  {- [TODO]
  describe "Base char is not a combining character" do
    void $ Map.traverseWithKey check_combining diacriticsDef
  -}
  -- [TODO] How to obtain the combining char from the dead key? double it except for some single accents with chained dk
  describe "Latin" do
    it "No duplicate key" do
      _dkOverlappingBaseChars latin_errors `shouldBe` mempty
    it "No duplicate combo" do
      _dkOverlappingCombos latin_errors `shouldBe` mempty
  describe "Science" do
    it "No duplicate key" do
      _dkOverlappingBaseChars science_errors `shouldBe` mempty
    it "No duplicate combo" do
      _dkOverlappingCombos science_errors `shouldBe` mempty
  where
    (_latinDef, latin_errors) = mkDeadKeyDefinitionsWithDetails DK.bépoLatinDef
    (_scienceDef, science_errors) = mkDeadKeyDefinitionsWithDetails DK.bépoScienceDef
