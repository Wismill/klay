module Klay.Keyboard.Layout.Action.DeadKey.ComposeSpec
  ( spec
  ) where


import Test.Hspec

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Compose.English qualified as EN
import Klay.Keyboard.Layout.Action.DeadKey.Compose.French qualified as FR


spec :: Spec
spec = do
  -- [TODO] Add check that the combining and the base char share name
  {- [TODO]
  describe "Base char is not a combining character" do
    void $ Map.traverseWithKey check_combining diacriticsDef
  -}
  -- [TODO] How to obtain the combining char from the dead key? double it except for some single accents with chained dk
  describe "English" do
    it "No duplicate key" do
      _dkOverlappingBaseChars en_errors `shouldBe` mempty
    it "No duplicate combo" do
      _dkOverlappingCombos en_errors `shouldBe` mempty
  describe "French" do
    it "No duplicate key" do
      _dkOverlappingBaseChars fr_errors `shouldBe` mempty
    it "No duplicate combo" do
      _dkOverlappingCombos fr_errors `shouldBe` mempty
  where
    (_enDef, en_errors) = mkDeadKeyDefinitionsWithDetails EN.composeDef
    (_frDef, fr_errors) = mkDeadKeyDefinitionsWithDetails FR.composeDef
