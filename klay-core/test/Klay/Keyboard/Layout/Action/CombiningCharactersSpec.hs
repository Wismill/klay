module Klay.Keyboard.Layout.Action.CombiningCharactersSpec
  ( spec
  ) where


import Data.Char (isMark)
import Data.Foldable

import Test.Hspec

import Klay.Keyboard.Layout.Action.CombiningCharacters qualified as CC

spec :: Spec
spec =
    it "All diacritics" do
      traverse_ (`shouldSatisfy` isMark) CC.allCombiningChars
