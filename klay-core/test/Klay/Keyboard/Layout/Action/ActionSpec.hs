module Klay.Keyboard.Layout.Action.ActionSpec
  ( spec
  , aa, aà, aα, aA, ab, aB
  ) where


import Test.Hspec
import Test.QuickCheck

import Klay.Keyboard.Layout.Action

aa, aà, aα, aA, ab, aB :: Action
aa = AChar 'a'
aα = AChar 'α'
aà = AChar 'à'
aA = AChar 'A'
ab = AChar 'b'
aB = AChar 'B'

spec :: Spec
spec = do
  describe "Action" do
    describe "instance Semigroup Action" do
      it "associativity" do
        property $ \(a :: Action) (b :: Action) (c :: Action) -> (a <> b) <> c == a <> (b <> c)
      it "UndefinedAction is the neutral element" do
        property $ \(a :: Action) -> UndefinedAction <> a == a && a <> UndefinedAction == a
