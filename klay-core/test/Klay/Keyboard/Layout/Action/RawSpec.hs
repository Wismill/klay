module Klay.Keyboard.Layout.Action.RawSpec
  ( spec
  ) where


import Test.Hspec
import Test.QuickCheck

import Klay.Keyboard.Layout.Action hiding (Delete)
import Klay.Keyboard.Layout.Modifier
import Data.Alterable

import Klay.Keyboard.Layout.Action.ActionsSpec hiding (spec)


spec :: Spec
spec = do
  let d1Aa = ADefault (AutoActions Manual as1A ) :: RawActions
  -- let d1B  = ADefault (AutoActions Manual as1B ) :: RawActions
  -- let dX1  = ADefault (AutoActions Manual asX1 ) :: RawActions
  -- let dX1' = ADefault (AutoActions Manual asX1') :: RawActions
  -- let dX2' = ADefault (AutoActions Manual asX2') :: RawActions
  let cX3a = ACustom (AutoActions Auto asX3) :: RawActions
  let cX3m = ACustom (AutoActions Manual asX3) :: RawActions
  let cX3'm = ACustom (AutoActions Manual asX3') :: RawActions
  -- let c1A  = customManual as1A :: RawActions
  -- let c1B  = customManual as1B :: RawActions
  -- let cX1  = customManual asX1 :: RawActions
  -- let cX2  = customManual asX2 :: RawActions
  -- let cX3 = ACustom asX3 :: RawActions
  describe "RawActions" do
    describe "Magma instance" do
      it "ADefault +> ADefault" do
        property $ \a b -> ADefault a +> ADefault b == (ADefault b :: RawActions)
    describe "Semigroup instance" do
      it "associativity" do
        property $ \(a :: RawActions) b c -> (a <> b) <> c == a <> (b <> c)
      it "Neutral element" do
        property $ \(a :: RawActions) -> mempty <> a == a && a <> mempty == a
    describe "instance Alterable RawActions ActionsAlterations" do
      -- it "(malter (diff a1 (ADefault a2)) a1) == ADefault a2" do
      --   property $ \(a :: RawActions) (b :: AutoActions) -> let b' = ADefault b in malter (diff a b' :: Maybe RawActionsAlterations) a == b'
      it "(malter (diff a1 (ACustom a2)) a1) == ACustom a2" do
        property $ \(a :: RawActions) (b :: AutoActions) -> let b' = ACustom b in malter (diff a b' :: Maybe RawActionsAlterations) a == b'
      it "untagCustomisableActions (malter (diff a1 a2) a1) == untagCustomisableActions a2" do
        property $ \(a :: RawActions) (b :: RawActions) -> untagCustomisableActions (malter (diff a b :: Maybe RawActionsAlterations) a) == untagCustomisableActions b
    describe "instance Alterable RawActions RawActionsChanges" do
      it "malter (diff a1 a2) a1 == a2" do
        property $ \(a :: RawActions) (b :: RawActions) -> malter (diff a b :: Maybe RawActionsChanges) a == b

  describe "RawActionsAlteration" do
    it "instance Alterable RawActionsAlteration ModifiersOptions" do
      alter NoModifiersOptions (InsertOrReplace d1Aa :: RawActionsAlteration) `shouldBe` InsertOrReplace d1Aa
      alter asX3'_opts         (InsertOrReplace d1Aa :: RawActionsAlteration) `shouldBe` InsertOrReplace d1Aa
      alter NoModifiersOptions (InsertOrReplace cX3a :: RawActionsAlteration) `shouldBe` InsertOrReplace cX3m
      alter asX3'_opts         (InsertOrReplace cX3a :: RawActionsAlteration) `shouldBe` InsertOrReplace cX3'm
