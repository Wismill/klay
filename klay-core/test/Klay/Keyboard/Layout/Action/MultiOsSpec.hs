module Klay.Keyboard.Layout.Action.MultiOsSpec
  ( spec
  ) where


import Test.Hspec
import Test.QuickCheck

-- import Klay.Keyboard.Hardware.Key qualified as K
-- import Klay.Keyboard.Layout.Action hiding (Delete)
-- import Klay.Keyboard.Layout.Action.Default
import Klay.Keyboard.Layout.Action
-- import Klay.Keyboard.Layout.Action.Special qualified as A
-- import Klay.Keyboard.Layout.Modifier
import Klay.OS
-- import Klay.Utils
import Data.Alterable

-- import Klay.Keyboard.Layout.Action.ActionsSpec hiding (spec)


spec :: Spec
spec = do
  -- let d1Aa = ADefault (AutoActions Manual as1A ) :: MultiOsRawActions
  -- let cX3a = ACustom (AutoActions Auto asX3) :: MultiOsRawActions
  -- let cX3m = ACustom (AutoActions Manual asX3) :: MultiOsRawActions
  -- let cX3'm = ACustom (AutoActions Manual asX3') :: MultiOsRawActions
  describe "MultiOsRawActions" do
    describe "Magma instance" do
      it "Neutral element" do
        property $ \(a :: MultiOsRawActions) -> mempty +> a == a && a +> mempty == a
      it "Linux & Windows #1" do
        property $ \(ai :: RawActions) al aw bl bw
          -> MultiOsActions (MultiOs ai al aw)
          +> MultiOsActions (MultiOs mempty bl bw)
          == MultiOsActions (MultiOs ai (al +> bl) (aw +> bw))
      it "Linux & Windows #2" do
        property $ \(bi :: RawActions) al aw bl bw
          -> MultiOsActions (MultiOs mempty al aw)
          +> MultiOsActions (MultiOs bi bl bw)
          == MultiOsActions (MultiOs bi (al +> bl) (aw +> bw))
    --   it "ADefault +> ADefault" do
    --     property $ \a b -> ADefault a +> ADefault b == (ADefault b :: MultiOsRawActions)
    describe "Semigroup instance" do
      it "associativity" do
        property $ \(a :: MultiOsRawActions) b c -> (a <> b) <> c == a <> (b <> c)
      it "Neutral element" do
        property $ \(a :: MultiOsRawActions) -> mempty <> a == a && a <> mempty == a
    -- describe "instance Alterable MultiOsRawActions ActionsAlterations" do
    --   it "(malter (diff a1 (ACustom a2)) a1) == ACustom a2" do
    --     property $ \(a :: MultiOsRawActions) (b :: AutoActions) -> let b' = ACustom b in malter (diff a b' :: Maybe RawActionsAlterations) a == b'
    --   it "untagCustomisableActions (malter (diff a1 a2) a1) == untagCustomisableActions a2" do
    --     property $ \(a :: MultiOsRawActions) (b :: MultiOsRawActions) -> untagCustomisableActions (malter (diff a b :: Maybe RawActionsAlterations) a) == untagCustomisableActions b
    -- describe "instance Alterable MultiOsRawActions RawActionsChanges" do
    --   it "malter (diff a1 a2) a1 == a2" do
    --     property $ \(a :: MultiOsRawActions) (b :: MultiOsRawActions) -> malter (diff a b :: Maybe RawActionsChanges) a == b

  describe "MultiOsRawActionsAlterations" do
    describe "instance Alterable MultiOsRawActions MultiOsRawActionsAlterations" do
      it "Windows & Linux" do
        property $ \(ai :: RawActions) al aw (ml :: Maybe RawActionsAlteration) mw ->
          alter (MultiOsActions (MultiOs Nothing ml mw)) (MultiOsActions (MultiOs ai al aw))
          == MultiOsActions (MultiOs ai (malter ml al) (malter mw aw))
  --     alter NoModifiersOptions (InsertOrReplace d1Aa :: MultiOsActionsAlteration) `shouldBe` InsertOrReplace d1Aa
  --     alter asX3'_opts         (InsertOrReplace d1Aa :: MultiOsActionsAlteration) `shouldBe` InsertOrReplace d1Aa
  --     alter NoModifiersOptions (InsertOrReplace cX3a :: MultiOsActionsAlteration) `shouldBe` InsertOrReplace cX3m
  --     alter asX3'_opts         (InsertOrReplace cX3a :: MultiOsActionsAlteration) `shouldBe` InsertOrReplace cX3'm
