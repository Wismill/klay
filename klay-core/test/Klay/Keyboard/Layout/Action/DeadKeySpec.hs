{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Action.DeadKeySpec
  ( spec
  ) where

import Numeric.Natural
import Data.Set (Set)
import Data.Map.Strict qualified as Map

import Test.Hspec

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Compose qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK


safeDeadKeyChars :: [Natural]
safeDeadKeyChars = [1..]

spec :: Spec
spec = do
  describe "replace" do
    let old1 = DK.acuteAbove
    let old2 = DK.doubledAcute
    let new1 = old1{_dkBaseChar='ó'}
    let new2 = old2{_dkBaseChar='ő'}
    let combo1   = char 'e' 'é'
    let combo2   = chainedDeadKey old1 old2
    let combo2'  = chainedDeadKey new1 old2
    let combo2'' = chainedDeadKey new1 new2
    let combo3   = chainedDeadKey old1 old1
    let combo3'  = chainedDeadKey new1 new1
    it "updateDeadKeyCombo1" do
      updateDeadKeyCombo1 old1 new1 combo1 `shouldBe` combo1
      updateDeadKeyCombo1 old1 new1 combo2 `shouldBe` combo2'
      updateDeadKeyCombo1 old1 new1 combo3 `shouldBe` combo3'
    it "updateDeadKeyCombo" do
      let s = [(old1, new1), (old2, new2)]
      updateDeadKeyCombo s combo1 `shouldBe` combo1
      updateDeadKeyCombo s combo2 `shouldBe` combo2''
    it "updateRawDeadKeyDefinitions" do
      let s = [(old1, new1), (old2, new2)]
      let dkd = [(old1, [combo1, combo2, combo3])]
      let dkd' = [(new1, [combo1, combo2'', combo3'])]
      updateRawDeadKeyDefinitions s dkd `shouldBe` dkd'
    it "updateDeadKeyDefinitions" do
      let s = [(old1, new1), (old2, new2)]
      let m = [(old1, [combo1, combo2]), (old2, [combo3])]
      let r = [(old1, [combo1, combo2]), (old2, [combo3])]
      let m' = [(new1, [combo1, combo2'']), (new2, [combo3'])]
      let r' = [(new1, [combo1, combo2'']), (new2, [combo3'])]
      let dkd = DeadKeyDefinitions m r mempty
      let dkd' = DeadKeyDefinitions m' r' mempty
      updateDeadKeyDefinitions s dkd `shouldBe` dkd'
  describe "resolveMergedDeadKeyDefinitions" do
    describe "dks=[], dksDefs=[]" $
      let dks = []
          dksDefs = [] :: MergedDeadKeyDefinitions
          cs = []
          m = mempty
          errors = mempty
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose], dksDefs=[]" $
      let dks = [DK.compose]
          dksDefs = [] :: MergedDeadKeyDefinitions
          cs = take 1 safeDeadKeyChars
          m = [DK.compose]
          errors = mempty{_dkUndefined=dks}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose], Undecidable" $
      let dks = [DK.compose]
          combo1 = mkCombo [DK.compose] DK.compose
          dksDefs = [(DK.compose, [combo1])] :: MergedDeadKeyDefinitions
          cs = take 1 safeDeadKeyChars
          m = [DK.compose]
          combos_errors = [(DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.compose] (Just . RDeadKey $ DK.compose))])]
          errors = mempty
            { _dkUndecidable=dks
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose], Decidable" $
      let dks = [DK.compose]
          combo1 = mkCombo [DK.compose] DK.compose
          dksDefs =
            [ (DK.compose,
                [ combo1
                , chars ['1', '2'] '½'
                ])
            ] :: MergedDeadKeyDefinitions
          cs = take 1 safeDeadKeyChars
          m = [DK.compose]
          combos_errors = [(DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.compose] (Just . RDeadKey $ DK.compose))])]
          errors = mempty{_dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove], Invalid" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove] '_'
          dksDefs =
            [ (DK.compose, [combo1])
            ] :: MergedDeadKeyDefinitions
          cs = take 2 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose]
          combos_errors = [(DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.graveAbove] Nothing)])]
          errors = mempty
            { _dkUndefined=[DK.graveAbove]
            , _dkEmptyDefinition=[DK.compose]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove], Undecidable #1" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove] DK.compose
          dksDefs =
            [ (DK.compose, [combo1])
            ] :: MergedDeadKeyDefinitions
          cs = take 2 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose]
          combos_errors = [(DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.graveAbove] (Just . RDeadKey $ DK.compose))])]
          errors = mempty
            { _dkUndefined=[DK.graveAbove]
            , _dkUndecidable=[DK.compose]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove], Undecidable #2" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove, DK.graveAbove] DK.compose
          dksDefs =
            [ (DK.compose,     [combo1])
            , (DK.graveAbove, [char ' ' 'ˋ'])
            ] :: MergedDeadKeyDefinitions
          cs = take 2 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose]
          combos_errors = [(DK.compose, [(combo1, DeadKeyComboError mempty (Just . RDeadKey $ DK.compose))])]
          errors = mempty
            { _dkUndecidable=[DK.compose]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove], Undecidable #3" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = chainedDeadKey DK.compose DK.graveAbove
          combo2 = chainedDeadKey DK.graveAbove DK.compose
          dksDefs =
            [ (DK.compose,     [combo1])
            , (DK.graveAbove, [combo2])
            ] :: MergedDeadKeyDefinitions
          cs = take 2 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose]
          combos_errors =
            [ (DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.compose] (Just . RDeadKey $ DK.graveAbove))])
            , (DK.graveAbove, [(combo2, DeadKeyComboError [SDeadKey DK.graveAbove] (Just . RDeadKey $ DK.compose))])]
          errors = mempty
            { _dkUndecidable=[DK.compose, DK.graveAbove]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.graveAbove, DK.doubleGrave], Valid" $
      let dks = [DK.graveAbove, DK.doubleGrave]
          dksDefs =
            [ (DK.graveAbove,  [chainedDeadKey DK.graveAbove DK.doubleGrave])
            , (DK.doubleGrave, [char ' ' '‶'])
            ] :: MergedDeadKeyDefinitions
          cs = take 2 safeDeadKeyChars
          m = [DK.graveAbove, DK.doubleGrave]
          errors = mempty
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove, DK.doubleGrave], Invalid #1" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove, DK.graveAbove] DK.doubleGrave
          dksDefs =
            [ (DK.compose,     [combo1])
            , (DK.graveAbove, [char ' ' 'ˋ'])
            ] :: MergedDeadKeyDefinitions
          cs = take 3 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose, DK.doubleGrave]
          combos_errors = [ (DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.doubleGrave] Nothing)])]
          errors = mempty
            { _dkUndefined=[DK.doubleGrave]
            , _dkEmptyDefinition=[DK.compose]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove, DK.doubleGrave], Invalid #2" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove, DK.graveAbove] DK.doubleGrave
          dksDefs =
            [ (DK.compose,      [combo1])
            , (DK.doubleGrave, [char ' ' '‶'])
            ] :: MergedDeadKeyDefinitions
          cs = take 3 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose, DK.doubleGrave]
          combos_errors = [ (DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.graveAbove] Nothing)])]
          errors = mempty
            { _dkUndefined=[DK.graveAbove]
            , _dkEmptyDefinition=[DK.compose]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove, DK.doubleGrave], Undecidable #1" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove, DK.graveAbove] DK.doubleGrave
          combo2 = chainedDeadKey DK.graveAbove DK.graveAbove
          dksDefs =
            [ (DK.compose,      [combo1])
            , (DK.graveAbove,  [combo2])
            , (DK.doubleGrave, [char ' ' '‶'])
            ] :: MergedDeadKeyDefinitions
          cs = take 3 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose, DK.doubleGrave]
          combos_errors =
            [ (DK.compose, [(combo1, DeadKeyComboError [SDeadKey DK.graveAbove] Nothing)])
            , (DK.graveAbove, [(combo2, DeadKeyComboError [SDeadKey DK.graveAbove] (Just . RDeadKey $ DK.graveAbove))])]
          errors = mempty
            { _dkUndecidable=[DK.compose, DK.graveAbove]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove, DK.doubleGrave], Undecidable #2" $
      let dks = [DK.compose, DK.graveAbove]
          combo1 = mkCombo [DK.graveAbove, DK.graveAbove] DK.doubleGrave
          combo2 = chainedDeadKey DK.doubleGrave DK.doubleGrave
          dksDefs =
            [ (DK.compose,      [combo1])
            , (DK.graveAbove,  [char ' ' 'ˋ'])
            , (DK.doubleGrave, [combo2])
            ] :: MergedDeadKeyDefinitions
          cs = take 3 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose, DK.doubleGrave]
          combos_errors =
            [ (DK.compose, [(combo1, DeadKeyComboError mempty (Just . RDeadKey $ DK.doubleGrave))])
            , (DK.doubleGrave, [(combo2, DeadKeyComboError [SDeadKey DK.doubleGrave] (Just . RDeadKey $ DK.doubleGrave))])]
          errors = mempty
            { _dkUndecidable=[DK.compose, DK.doubleGrave]
            , _dkCombosErrors=combos_errors}
      in check_resolve dks dksDefs cs m errors
    describe "dks=[DK.compose, DK.graveAbove, DK.doubleGrave], Valid #1" $
      let dks = [DK.compose, DK.graveAbove]
          dksDefs =
            [ (DK.compose,      [mkCombo [DK.graveAbove, DK.graveAbove] DK.doubleGrave])
            , (DK.graveAbove,  [char ' ' 'ˋ'])
            , (DK.doubleGrave, [char ' ' '‶'])
            ] :: MergedDeadKeyDefinitions
          cs = take 3 safeDeadKeyChars
          m = [DK.graveAbove, DK.compose, DK.doubleGrave]
          errors = mempty
      in check_resolve dks dksDefs cs m errors
  describe "flatten_descriptions (REMOVE chained dead keys)" do
    describe "No chained dead keys" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo ['A'] 'À'
                ] )
            ] :: MergedDeadKeyDefinitions
          errors = mempty
      in check_flatten FlattenThenRemoveChainedDK dksDefs dksDefs errors
    describe "Recursive chained dead keys" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo [' '] DK.graveAbove
                , mkCombo ['A'] 'À'
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' =
            [ ( DK.graveAbove,
                [ mkCombo ['A'] 'À'
                ] )
            ] :: MergedDeadKeyDefinitions
          errors = mempty
      in check_flatten FlattenThenRemoveChainedDK dksDefs dksDefs' errors
    describe "1 level chained dead keys" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo [DK.graveAbove] DK.doubleGrave
                , mkCombo ['A'] 'À'
                ] )
            , ( DK.doubleGrave,
                [ mkCombo ['a'] 'ȁ'
                , mkCombo ['A'] 'Ȁ'
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' = dksDefs ><
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo ['A'] 'À'
                , mkCombo [SDeadKey DK.graveAbove, SChar 'a'] 'ȁ'
                , mkCombo [SDeadKey DK.graveAbove, SChar 'A'] 'Ȁ'
                ])
            ]
          errors = mempty
      in check_flatten FlattenThenRemoveChainedDK dksDefs dksDefs' errors
    describe "1 level chained dead keys with recursive dks" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo [DK.doubleGrave] DK.graveAbove
                , mkCombo [DK.graveAbove] DK.doubleGrave
                , mkCombo ['A'] 'À'
                ] )
            , ( DK.doubleGrave,
                [ mkCombo ['a'] 'ȁ'
                , mkCombo ['A'] 'Ȁ'
                , mkCombo [DK.doubleGrave] DK.graveAbove
                , mkCombo [DK.doubleGrave] DK.doubleGrave
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' = dksDefs ><
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo ['A'] 'À'
                , mkCombo [SDeadKey DK.graveAbove, SChar 'a'] 'ȁ'
                , mkCombo [SDeadKey DK.graveAbove, SChar 'A'] 'Ȁ'
                ])
            , ( DK.doubleGrave,
                [ mkCombo ['a'] 'ȁ'
                , mkCombo ['A'] 'Ȁ'
                , mkCombo [SDeadKey DK.doubleGrave, SChar 'a'] 'à'
                , mkCombo [SDeadKey DK.doubleGrave, SChar 'A'] 'À'
                ] )
            ]
          errors = mempty
      in check_flatten FlattenThenRemoveChainedDK dksDefs dksDefs' errors
    describe "2 levels chained dead keys" $
      let dksDefs =
            [ ( DK.dotAbove,
                [ mkCombo ['a'] 'ȧ'
                , mkCombo [DK.dotAbove] DK.diaeresisAbove
                ] )
            , ( DK.diaeresisAbove,
                [ mkCombo ['a'] 'ä'
                , mkCombo ['u'] 'ü'
                , mkCombo [DK.dotAbove] DK.diaeresisBelow
                ] )
            , ( DK.diaeresisBelow,
                [ mkCombo ['u'] 'ṳ'
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' = dksDefs ><
            [ ( DK.dotAbove,
                [ mkCombo ['a'] 'ȧ'
                , mkCombo [SDeadKey DK.dotAbove, SChar 'a'] 'ä'
                , mkCombo [SDeadKey DK.dotAbove, SChar 'u'] 'ü'
                , mkCombo [SDeadKey DK.dotAbove, SDeadKey DK.dotAbove, SChar 'u'] 'ṳ'
                ])
            , ( DK.diaeresisAbove,
                [ mkCombo ['a'] 'ä'
                , mkCombo ['u'] 'ü'
                , mkCombo [SDeadKey DK.dotAbove, SChar 'u'] 'ṳ'
                ] )
            ]
          errors = mempty
      in check_flatten FlattenThenRemoveChainedDK dksDefs dksDefs' errors
  describe "flatten_descriptions (KEEP chained dead keys)" do
    describe "No chained dead keys" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo ['A'] 'À'
                ] )
            ] :: MergedDeadKeyDefinitions
          errors = mempty
      in check_flatten FlattenThenKeepChainedDK dksDefs dksDefs errors
    describe "Recursive chained dead keys" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo [' '] DK.graveAbove
                , mkCombo ['A'] 'À'
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' =
            [ ( DK.graveAbove,
                [ mkCombo ['A'] 'À'
                ] )
            ] :: MergedDeadKeyDefinitions
          errors = mempty
      in check_flatten FlattenThenKeepChainedDK dksDefs dksDefs' errors
    describe "1 level chained dead keys" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo [DK.graveAbove] DK.doubleGrave
                , mkCombo ['A'] 'À'
                ] )
            , ( DK.doubleGrave,
                [ mkCombo ['a'] 'ȁ'
                , mkCombo ['A'] 'Ȁ'
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' = dksDefs ><
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo ['A'] 'À'
                , mkCombo [DK.graveAbove] DK.doubleGrave
                , mkCombo [SDeadKey DK.graveAbove, SChar 'a'] 'ȁ'
                , mkCombo [SDeadKey DK.graveAbove, SChar 'A'] 'Ȁ'
                ])
            ]
          errors = mempty
      in check_flatten FlattenThenKeepChainedDK dksDefs dksDefs' errors
    describe "1 level chained dead keys with recursive dks" $
      let dksDefs =
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo [DK.doubleGrave] DK.graveAbove
                , mkCombo [DK.graveAbove] DK.doubleGrave
                , mkCombo ['A'] 'À'
                ] )
            , ( DK.doubleGrave,
                [ mkCombo ['a'] 'ȁ'
                , mkCombo ['A'] 'Ȁ'
                , mkCombo [DK.doubleGrave] DK.graveAbove
                , mkCombo [DK.doubleGrave] DK.doubleGrave
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' = dksDefs ><
            [ ( DK.graveAbove,
                [ mkCombo ['a'] 'à'
                , mkCombo ['A'] 'À'
                , mkCombo [DK.graveAbove] DK.doubleGrave
                , mkCombo [SDeadKey DK.graveAbove, SChar 'a'] 'ȁ'
                , mkCombo [SDeadKey DK.graveAbove, SChar 'A'] 'Ȁ'
                ])
            , ( DK.doubleGrave,
                [ mkCombo ['a'] 'ȁ'
                , mkCombo ['A'] 'Ȁ'
                , mkCombo [DK.doubleGrave] DK.graveAbove
                , mkCombo [SDeadKey DK.doubleGrave, SChar 'a'] 'à'
                , mkCombo [SDeadKey DK.doubleGrave, SChar 'A'] 'À'
                ] )
            ]
          errors = mempty
      in check_flatten FlattenThenKeepChainedDK dksDefs dksDefs' errors
    describe "2 levels chained dead keys" $
      let dksDefs =
            [ ( DK.dotAbove,
                [ mkCombo ['a'] 'ȧ'
                , mkCombo [DK.dotAbove] DK.diaeresisAbove
                ] )
            , ( DK.diaeresisAbove,
                [ mkCombo ['a'] 'ä'
                , mkCombo ['u'] 'ü'
                , mkCombo [DK.dotAbove] DK.diaeresisBelow
                ] )
            , ( DK.diaeresisBelow,
                [ mkCombo ['u'] 'ṳ'
                ] )
            ] :: MergedDeadKeyDefinitions
          dksDefs' = dksDefs ><
            [ ( DK.dotAbove,
                [ mkCombo ['a'] 'ȧ'
                , mkCombo [DK.dotAbove] DK.diaeresisAbove
                , mkCombo [DK.dotAbove, DK.dotAbove] DK.diaeresisBelow
                , mkCombo [SDeadKey DK.dotAbove, SChar 'a'] 'ä'
                , mkCombo [SDeadKey DK.dotAbove, SChar 'u'] 'ü'
                , mkCombo [SDeadKey DK.dotAbove, SDeadKey DK.dotAbove, SChar 'u'] 'ṳ'
                ])
            , ( DK.diaeresisAbove,
                [ mkCombo ['a'] 'ä'
                , mkCombo ['u'] 'ü'
                , mkCombo [DK.dotAbove] DK.diaeresisBelow
                , mkCombo [SDeadKey DK.dotAbove, SChar 'u'] 'ṳ'
                ] )
            ]
          errors = mempty
      in check_flatten FlattenThenKeepChainedDK dksDefs dksDefs' errors

  where
    check_resolve :: Set DeadKey -> MergedDeadKeyDefinitions -> [Natural] -> [DeadKey] -> DeadKeyError -> Spec
    check_resolve dks dksDefs cs m errors = do
      let (m', cs', _, errors') = resolveMergedDeadKeyDefinitions' dks dksDefs cs
      it "Dead key mapping" $ m' `shouldBe` Map.fromList (zip m cs)
      it "Remaining chars" $ cs' `shouldBe` mempty
      it "Errors" $ errors' `shouldBe` errors

    check_flatten :: DeadKeyFlattenMode -> MergedDeadKeyDefinitions -> MergedDeadKeyDefinitions -> DeadKeyError -> Spec
    check_flatten mode merged expected_merged expected_errors = do
      let defs = mempty{_dkMergedDefinitions=merged}
      let DeadKeyDefinitions{_dkMergedDefinitions=merged', _dkErrors=errors} = flattenDeadKeyDefinitions mode defs
      it "Flattened" $ merged' `shouldBe` expected_merged
      it "Errors" $ errors `shouldBe` expected_errors

(><) :: (Semigroup a) => a -> a -> a
(><) = flip (<>)
