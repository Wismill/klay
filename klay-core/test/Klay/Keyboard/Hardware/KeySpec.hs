module Klay.Keyboard.Hardware.KeySpec
  ( spec
  , keyLetters, keyPunctuation, keyDecimalNumbers, keyHexadecimalNumbers, keyNumpad
  , keyNotAChar, keyNotADecimalNumber, keyNotAnHexadecimalNumber
  , upperLetters, lowerLetters, punctuation, decimalNumbersChar, decimalNumbersInt, hexadecimalNumbers
  , notAChar, notADecimalNumberChar, notADecimalNumberInt, notAnHexadecimalNumber
  ) where

import Data.List ((\\))
import Data.Map.Strict qualified as Map
import Data.Foldable (traverse_)
import Data.Functor (void)

import Test.Hspec

import Klay.Keyboard.Hardware.Key

{- |
Ensure that the naming convention of key and actions are sync
and that the expected order is kept synchronised.
-}
spec :: Spec
spec = do
  describe "Order" do
    it "Numeric keypad" do
      let keys = [KP0, KP1, KP2, KP3, KP4, KP5, KP6, KP7, KP8, KP9]
      indexes keys `shouldBe` mkIndexes (fromEnum KP0) keys
    it "Number row" do
      let keys = [N0, N1, N2, N3, N4, N5, N6, N7, N8, N9]
      indexes keys `shouldBe` mkIndexes (fromEnum N0) keys
    it "Letters" do
      let keys = [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z]
      indexes keys `shouldBe` mkIndexes (fromEnum A) keys
    it "Modifiers" do
      let keys = [LShift, RShift, CapsLock, LControl, RControl, LAlternate, RAlternate, LSuper, RSuper]
      indexes keys `shouldBe` mkIndexes (fromEnum LShift) keys
    it "Function key" do
      let keys = [F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24]
      indexes keys `shouldBe` mkIndexes (fromEnum F1) keys

  describe "keyToChar" do
    it "Letters" do
      fmap keyToChar keyLetters `shouldBe` upperLetters
    it "Punctuation" do
      fmap keyToChar keyPunctuation `shouldBe` punctuation
    it "Number row" do
      fmap keyToChar keyDecimalNumbers `shouldBe` decimalNumbersChar
    it "Numpad" do
      fmap keyToChar keyNumpad `shouldBe` decimalNumbersChar
    it "Other keys" do
      fmap keyToChar keyNotAChar `shouldBe` notAChar

  describe "keyDecimalToInt" do
    it "Hexadecimal" do
      fmap keyDecimalToInt keyDecimalNumbers `shouldBe` decimalNumbersInt
    it "Numpad" do
      fmap keyDecimalToInt keyNumpad `shouldBe` decimalNumbersInt
    it "Other keys" do
      fmap keyDecimalToInt keyNotADecimalNumber `shouldBe` notADecimalNumberInt

  describe "keyHexadecimalToInt" do
    it "Hexadecimal" do
      fmap keyHexadecimalToInt keyHexadecimalNumbers `shouldBe` hexadecimalNumbers
    it "Numpad" do
      fmap keyHexadecimalToInt keyNumpad `shouldBe` decimalNumbersInt
    it "Other keys" do
      fmap keyHexadecimalToInt keyNotAnHexadecimalNumber `shouldBe` notAnHexadecimalNumber

  describe "keyToKeycodeAssoc" do
    it "All key mapped" do
      traverse_ checkKey $ enumFromTo minBound maxBound
    describe "Exactly one key per keycode" do
      void $ Map.traverseWithKey check_unique reversedMap
  where
    checkKey key = key `shouldSatisfy` (`Map.member` keyToKeycodeMap)
    check_unique keycode keys =
      it (show keycode) $ (length keys, keys) `shouldBe` (1, keys)
    reversedMap
      = Map.fromListWith (<>)
      . fmap (\(key, keycode, _) -> (keycode, [key]))
      $ keyToKeycodeAssoc

    indexes :: (Enum x) => [x] -> [Int]
    indexes = fmap fromEnum
    mkIndexes :: Int -> [x] -> [Int]
    mkIndexes offset xs = take (length xs) [offset..]

keyLetters, keyPunctuation, keyDecimalNumbers, keyHexadecimalNumbers, keyNumpad :: [Key]
upperLetters, lowerLetters, punctuation, decimalNumbersChar, notAChar :: [Maybe Char]
decimalNumbersInt, hexadecimalNumbers :: [Maybe Int]
keyLetters = enumFromTo A Z
upperLetters = fmap Just ['A'..'Z']
lowerLetters = fmap Just ['a'..'z']

keyPunctuation = [Space, Minus, Comma, Semicolon, Period, Quote, LBracket, RBracket, Slash, Backslash]
punctuation = fmap Just [' ', '-', ',', ';', '.', '\'', '[', ']', '/', '\\']

keyDecimalNumbers = enumFromTo N0 N9
decimalNumbersChar = fmap Just ['0'..'9']
decimalNumbersInt = fmap Just [0..9]

keyHexadecimalNumbers = keyDecimalNumbers <> enumFromTo A F
hexadecimalNumbers = decimalNumbersInt <> fmap Just [0xA..0xF]

keyNumpad = enumFromTo KP0 KP9

keyNotAChar, keyNotADecimalNumber, keyNotAnHexadecimalNumber :: [Key]
notADecimalNumberChar :: [Maybe Char]
notADecimalNumberInt, notAnHexadecimalNumber :: [Maybe Int]
keyNotAChar = enumFrom minBound \\ (keyLetters <> keyPunctuation <> keyDecimalNumbers <> keyNumpad)
notAChar = fmap (const Nothing) keyNotAChar

keyNotADecimalNumber = enumFrom minBound \\ (keyDecimalNumbers <> keyNumpad)
notADecimalNumberChar = fmap (const Nothing) keyNotADecimalNumber
notADecimalNumberInt = fmap (const Nothing) keyNotADecimalNumber

keyNotAnHexadecimalNumber = enumFrom minBound \\ (keyHexadecimalNumbers <> keyNumpad)
notAnHexadecimalNumber = fmap (const Nothing) keyNotAnHexadecimalNumber
