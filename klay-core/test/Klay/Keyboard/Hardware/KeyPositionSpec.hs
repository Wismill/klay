module Klay.Keyboard.Hardware.KeyPositionSpec
  ( spec
  ) where

import Data.Foldable (traverse_)
import Data.Maybe (isJust)

import Test.Hspec

import Klay.Keyboard.Hardware
import Klay.Keyboard.Hardware.Divisions.ANSI qualified as ANSI
import Klay.Keyboard.Hardware.Divisions.ISO qualified as ISO
import Klay.Keyboard.Hardware.KeyPosition.ANSI qualified as ANSI
import Klay.Keyboard.Hardware.KeyPosition.ISO qualified as ISO

{- |
Ensure the keys used in evaluators have a key position
-}
spec :: Spec
spec = do
  describe "ANSI" do
    it "Alphanumeric section" do
      let keys = _alphanumericSection ANSI.keyboardSections
      traverse_ (hasPositionDefined ANSI.toKeyPosition) keys
  describe "ISO" do
    it "Alphanumeric section" do
      let keys = _alphanumericSection ISO.keyboardSections
      traverse_ (hasPositionDefined ISO.toKeyPosition) keys
  -- [TODO] TypeMatrix
  where
    hasPositionDefined toPosition key = toPosition key `shouldSatisfy` isJust
