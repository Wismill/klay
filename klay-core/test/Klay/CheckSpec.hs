{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Klay.CheckSpec
  ( spec
  ) where


import Test.Hspec

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
import Klay.Keyboard.Layout.Action.Auto.LatinAlphabet
import Klay.Check
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Modifier
import Klay.OS


{- |
Ensure predefined layout have no errors
-}
spec :: Spec
spec = do
  describe "Dummy layout" do
      -- it "No mapping" do
      --   let layout = mkLayout mempty mempty
      --   checkLayout layout `shouldBe` (LayoutErrors [("Group 1", K.A), ("Group 1", K.B)] mempty)
      it "Simple mapping, no dead keys" do
        let layout = mkLayout defaultActionMap mempty
        checkLayout layout `shouldBe` LayoutErrors mempty mempty
      it "Simple mapping, dead keys with no definitions" do
        let layout = mkLayout actionMapDk mempty
        checkLayout layout `shouldBe` LayoutErrors mempty mempty{_dkUndefined=[DK.graveAbove, DK.doubleGrave]}
      it "Simple mapping, dead keys with invalid definitions #1" do
        let layout = mkLayout actionMapDk [(DK.graveAbove, [])]
        checkLayout layout `shouldBe`
          LayoutErrors mempty mempty{_dkUndefined=[DK.graveAbove, DK.doubleGrave]}
      it "Simple mapping, dead keys with invalid definitions #2" do
        let combo1 = chainedDeadKey DK.graveAbove DK.doubleGrave
        let layout = mkLayout actionMapDk [(DK.graveAbove, [combo1])]
        checkLayout layout `shouldBe` LayoutErrors mempty mempty
          { _dkUndefined=[DK.doubleGrave]
          , _dkEmptyDefinition=[DK.graveAbove]
          , _dkCombosErrors=[ (DK.graveAbove, [(combo1, DeadKeyComboError [SDeadKey DK.doubleGrave] Nothing)]) ]
          }
      it "Simple mapping, dead keys with valid definitions" do
        let layout = mkLayout actionMapDk
              [ (DK.graveAbove, [chainedDeadKey DK.graveAbove DK.doubleGrave])
              , (DK.doubleGrave, [char ' ' '‶'])]
        checkLayout layout `shouldBe` mempty
  where
    actionMapDk = defaultActionMap +> [ (K.A, [noModifier .= DK.graveAbove, shift .= DK.doubleGrave]) ]

mkLayout :: RawActionsKeyMap -> RawDeadKeyDefinitions -> MultiOsRawLayout
mkLayout actionMap dksDefs = US.layout
  { _deadKeys = mkDeadKeyDefinitions dksDefs
  , _groups = OneGroup group
  }
  where
    group :: KeyMultiOsRawGroup
    group = mkRawGroup "Group 1" levels (toMultiOsRawActionsMap actionMap)
    levels = toMultiOs [(mempty, "Level 1"), (shift, "Level 2") ]

defaultActionMap :: RawActionsKeyMap
defaultActionMap = latinActionMap (Just DefaultCapsLockBehaviour) mempty shift
