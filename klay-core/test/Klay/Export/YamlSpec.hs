{-# LANGUAGE OverloadedStrings #-}

module Klay.Export.YamlSpec
  ( spec
  ) where

import Data.ByteString.Lazy qualified as BS
import Test.Hspec

import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Bépo      qualified as Bépo
import Klay.Keyboard.Layout.Examples.Neo2      qualified as Neo2
import Klay.Export.Json (generateJson)
import Klay.Export.Yaml (generateYaml)
import Klay.Import.Yaml (importYaml)

spec :: Spec
spec = do
  describe "US" do
    roundtrip US.layout
  describe "DE" do
    roundtrip DE.layout
  describe "ES" do
    roundtrip ES.layout
  describe "Bépo" do
    roundtrip Bépo.layout
  describe "Neo2" do
    roundtrip Neo2.layout
  where
    roundtrip l = do
      it "Roundtrip export (YAML) -> import" do
        importYaml (generateYaml l) `shouldBe` Right l
      it "Roundtrip export (JSON) -> import" do
        importYaml (BS.toStrict $ generateJson l) `shouldBe` Right l

