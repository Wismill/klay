{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Lookup.VirtualKeysSpec
  ( spec
  ) where

import Data.Maybe
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Foldable (fold)
-- import Control.Arrow ((&&&))

import Test.Hspec

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.VirtualKey qualified as VK

{-|
Ensure all virtual keys are mapped
-}
spec :: Spec
spec =
  describe "Virtual keys" do
    it "Native virtual keys" do
      mapped `Set.intersection` expected_failures `shouldBe` mempty
    -- describe "Unique virtual keys" do
    --   void $ Map.traverseWithKey check_win_vk reversedMap
  where
    all_keys :: [K.Key]
    all_keys = enumFromTo minBound maxBound
    mapped :: Set.Set K.Key
    mapped = fold reversedMap
    reversedMap = Map.fromListWith (<>) . mapMaybe (\k -> (, Set.singleton k) <$> VK.keyNativeVirtualKey4 k) $ all_keys
    -- check_win_vk vk keys = it (show vk) $ (Set.size keys, keys) `shouldBe` (1, keys)
    expected_failures =
      [ K.ScreenLock
      -- ,  K.Stop
      ,  K.Cancel
      ,  K.Save
      -- ,  K.MediaPlayCD -- [TODO] check
      -- ,  K.MediaPlay -- [TODO] check
      ,  K.MediaPause -- [TODO] check
      ,  K.MediaEject -- [TODO] check
      ,  K.MediaRewind -- [TODO] check
      ,  K.MediaForward -- [TODO] check
      ,  K.Find -- [TODO] check
      ,  K.Explorer -- [TODO] check
      ,  K.Browser -- [TODO] check
      ,  K.MyComputer -- [TODO] check
      ,  K.Documents -- [TODO] check
      ,  K.Calculator -- [TODO] check
      -- ,  K.Email2
      ,  K.Launch3 -- [TODO] check
      ,  K.Launch4 -- [TODO] check
      ,  K.Power -- [TODO] check
      ,  K.Suspend -- [TODO] check
      ,  K.WakeUp -- [TODO] check
      ,  K.BrightnessDown -- [TODO] check
      ,  K.BrightnessUp -- [TODO] check
      ,  K.KPPlusMinus
      ,  K.Undo -- [TODO] check
      ,  K.Redo -- [TODO] check
      ,  K.Cut -- [TODO] check
      ,  K.Paste -- [TODO] check
      ,  K.Hiragana -- [TODO] check
      -- ,  K.HiraganaKatakana -- [TODO] check
      ,  K.KPEnter
      -- ,  K.KPPlusMinus
      ]
