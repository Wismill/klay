{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Lookup.KeycodesSpec
  ( spec
  ) where

import Data.Bits (xor)
import Data.Tuple (swap)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Functor (void)

import Test.Hspec

import Klay.Export.Windows.Lookup.Keycodes (winKeyCodesMap, winKeyCodes)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Utils.Unicode (showHeX)

{-|
Ensure all keys are mapped to a keycode
-}
spec :: Spec
spec =
  describe "Keycodes" do
    it "Check keys" do
      checkKey <$> keys `shouldBe` (,True) <$> keys
    describe "Unique keys" do
      let reversedMap = Map.fromListWith (<>) . fmap (fmap Set.singleton . swap) $ winKeyCodes
      void $ Map.traverseWithKey check_unique_key reversedMap
    it "Check template" do
      check_template keys `shouldBe` template_keys
  where
    keys = enumFromTo minBound maxBound
    checkKey key = (key, (key `Set.member` expected_failures) `xor` (key `elem` keysMapped))
    check_unique_key win_key ks = it win_key $ (Set.size ks, ks) `shouldBe` (1, ks)
    keysMapped = Map.keysSet winKeyCodesMap
    expected_failures =
      [ K.ScreenLock
      -- , K.Stop
      , K.Cancel
      , K.Save
      , K.MediaPlayCD -- [TODO] check
      , K.MediaPlay -- [TODO] check
      , K.MediaPause -- [TODO] check
      , K.MediaEject -- [TODO] check
      -- , K.MediaPrevious -- [TODO] check
      -- , K.MediaNext -- [TODO] check
      , K.MediaRewind -- [TODO] check
      , K.MediaForward -- [TODO] check
      -- , K.BrowserHomePage -- [TODO] check
      -- , K.Calculator -- [TODO] check
      -- , K.MediaPlayer -- [TODO] check
      , K.Browser -- [TODO] check
      -- , K.Email1 -- [TODO] check
      , K.Email2 -- [TODO] check
      , K.Help -- [TODO] check
      , K.Find -- [TODO] check
      , K.Explorer -- [TODO] check
      -- , K.MyComputer -- [TODO] check
      , K.Documents -- [TODO] check
      , K.Launch1 -- [TODO] check
      , K.Launch2 -- [TODO] check
      , K.Launch3 -- [TODO] check
      , K.Launch4 -- [TODO] check
      -- , K.Power -- [TODO] check
      -- , K.Sleep -- [TODO] check
      , K.Suspend -- [TODO] check
      -- , K.WakeUp -- [TODO] check
      , K.BrightnessDown -- [TODO] check
      , K.BrightnessUp -- [TODO] check
      , K.Undo -- [TODO] check
      , K.Redo -- [TODO] check
      , K.Copy -- [TODO] check
      , K.Cut -- [TODO] check
      , K.Paste -- [TODO] check
      , K.KPPlusMinus -- [TODO] check
      -- , K.Katakana -- [TODO] check
      -- , K.Hiragana -- [TODO] check
      -- , K.HiraganaKatakana -- [TODO] check
      -- , K.Hangul -- [TODO] check
      -- , K.Hanja -- [TODO] check
      ]
    check_template = foldr go mempty
      where
        go key acc = case Map.lookup key winKeyCodesMap of
          Nothing -> acc
          Just keycode -> Set.insert keycode acc
    template_keys
      =  (Set.fromList ["T" <> showHeX 2 k | k <- [1..0x7F]]
      <> [ "X10", "X19", "X1D", "X20", "X21", "X22", "X24", "X2E"
         , "X30", "X32", "X35", "X37", "X38", "X47", "X48", "X49"
         , "X4B", "X4D", "X4F", "X50", "X51", "X52", "X53", "X5B"
         , "X5C", "X5D", "X5F", "X65", "X66", "X67", "X68", "X69"
         , "X6A", "X6B", "X6C", "X6D", "X1C", "X46", "XF1", "XF2"
         , "Y1D"
         , "X5E", "X63"
         ])
     -- Not mapped
     Set.\\ [ "T54", "T55", "T5A", "T5B", "T5D", "T5E", "T5F"
            , "T60", "T61", "T62", "T63", "T6F"
            , "T71", "T72", "T74", "T75"
            , "T7A", "T7C", "T7F"
            , "X46" ]
