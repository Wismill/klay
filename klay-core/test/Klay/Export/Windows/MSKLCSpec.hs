{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Export.Windows.MSKLCSpec
  ( spec
  ) where

import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Foldable (traverse_)
import Control.Monad ((>=>))

import Test.Hspec
import Data.Align (align)
import Data.These

import Paths_klay_core (getDataFileName)
import Klay.Export.Windows.IO (read_utf_16_le)
import Klay.Export.Windows.MSKLC (generateOnlyKlcFile)
import Klay.Export.Windows.Common.Modifiers (ALevel(..), ModifierErrors(..))
import Klay.Export.Windows.Common.Symbols (SymbolErrors(..))
import Klay.Keyboard.Hardware.Key qualified as K
-- import Klay.Keyboard.Layout.Action
-- import Klay.Keyboard.Layout.Action.Special qualified as A
import Klay.Keyboard.Layout.Modification (DiscardedReason(..))
import Klay.Keyboard.Layout.Modifier
-- import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Neo2 qualified as Neo2
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All qualified as Bépo

spec :: Spec
spec =
  describe "generateKlcFile" do
    describe "US layout" do
      checkLayout US.layout "us_klay.klc" Nothing Nothing Nothing
    describe "ES layout" do
      checkLayout ES.layout "es_klay.klc" Nothing Nothing Nothing
    describe "DE layout" do
      checkLayout DE.layout "de_klay.klc" Nothing Nothing Nothing
    describe "Bépo layout" do
      let dkErrors = mempty{_dkCombosErrors=bépo_dkErrors}
      checkLayout Bépo.layout "bepo.klc" Nothing Nothing (Just dkErrors)
    describe "Neo2 layout" do
      checkLayout Neo2.layout "neo2.klc" (Just neo2_mErrors) (Just neo2_sErrors) Nothing
  where
    checkLayout l klc_file mErrors sErrors dkErrors = do
      klc <- runIO $ getDataFileName >=> read_utf_16_le $ "test/windows/msklc/" <> klc_file
      (mfileContent, mErrors', symbolsErrors', dkErrors') <- runIO $ generateOnlyKlcFile l mempty
      it "Modifiers" do
        mErrors' `shouldBe` mErrors
      it "Symbols" do
        symbolsErrors' `shouldBe` sErrors
      it "Dead keys" do
        dkErrors' `shouldBe` dkErrors
      it "Klc file" do
        case mfileContent of
          Nothing -> expectationFailure "Empty file"
          Just fileContent -> do
            -- TL.toStrict fileContent `shouldBe` klc_es
            compareLines (T.lines . TL.toStrict $ fileContent) (T.lines klc)

    compareLines :: [T.Text] -> [T.Text] -> IO ()
    compareLines xs xs' = traverse_ f $ align (zip [(1::Int)..] xs) (zip [(1::Int)..] xs')
      where
        f (These a b) = a `shouldBe` b
        f (This (n, a)) = expectationFailure $ mconcat ["Unexpected data at line ", show n, ": " <> show a]
        f (That (n, b)) = expectationFailure $ mconcat ["Expected data at line ", show n, ": " <> show b]

    bépo_dkErrors =
      [ ( Bépo.horn
        , [ ( mkCombo [Bépo.horn] '\x1F918'
            , mempty{_dkcomboInvalidResult=Just (RChar '\129304')}
            )
          ]
        )
      ]

    neo2_mErrors = mempty
      { _mUnsupportedModifierBits=[IsoLevel3,IsoLevel5]
      , _mUnsupportedModifiersCombos=[[IsoLevel3], [IsoLevel3, Shift], [IsoLevel5], [IsoLevel5, Shift]]
      , _mUnsupportedLevels =
          [ (ALevel "Neo2" isoLevel3 "Sonder Zeichen", Invalid [IsoLevel3])
          , (ALevel "Neo2" (isoLevel3 <> shift) "Griechisch", Invalid [IsoLevel3, Shift])
          , (ALevel "Neo2" isoLevel5 "Tabellen Kalkulation", Invalid [IsoLevel5])
          , (ALevel "Neo2" (isoLevel5 <> shift) "Wissenschaft", Invalid [IsoLevel5, Shift])
          ]
      }
    neo2_sErrors = mempty
      { _sUnsupportedKeys = [K.Tabulator, K.CapsLock]
      -- , _sUnsupportedVks = [VK.BaseVirtualKey VK.Tabulator]
      , _sUnsupportedActions =
        [ Left (Modifier {_mBit = [IsoLevel3], _mVariant = N, _mEffect = Set})
        , Left (Modifier {_mBit = [IsoLevel5], _mVariant = N, _mEffect = Set})
        ]
      -- , _sDiscardedActions =
      --   [ ( K.Tabulator
      --     , ( mkActions mempty
      --         Nothing --(Just $ ASpecial A.Tabulator)
      --         [(noModifier, ASpecial A.Tabulator), (shift, ASpecial A.Tabulator)]
      --       , Invalid K.Tabulator
      --       )
      --     )
      --   ]
      }
