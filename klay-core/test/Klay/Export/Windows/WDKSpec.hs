{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Export.Windows.WDKSpec
  ( spec
  ) where

import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Foldable (traverse_)
import Control.Monad ((>=>))

import Test.Hspec
import Data.Align (align)
import Data.These

import Paths_klay_core (getDataFileName)
import Klay.Export.Windows.Common.Symbols (SymbolErrors(..))
import Klay.Export.Windows.IO (read_utf_8)
import Klay.Export.Windows.WDK.Header (generateHeader)
import Klay.Export.Windows.WDK.C (generateC)
import Klay.Export.Windows.WDK (prepareLayout)
import Klay.Keyboard.Layout.Action.Special qualified as A
import Klay.Keyboard.Layout.Action.DeadKey (DeadKeyError)
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Neo2 qualified as Neo2

spec :: Spec
spec = do
  describe "US layout" do
    header_ref <- load_file "test/windows/wdk/us_klay.h"
    c_ref <- load_file "test/windows/wdk/us_klay.c"
    checkLayout US.layout header_ref c_ref
  describe "ES layout" do
    header_ref <- load_file "test/windows/wdk/es_klay.h"
    c_ref <- load_file "test/windows/wdk/es_klay.c"
    checkLayout ES.layout header_ref c_ref
  describe "DE layout" do
    header_ref <- load_file "test/windows/wdk/de_klay.h"
    c_ref <- load_file "test/windows/wdk/de_klay.c"
    checkLayout DE.layout header_ref c_ref
  describe "Bépo layout" do
    let l = Bépo.layout
    header_ref <- load_file "test/windows/wdk/bepo.h"
    c_ref <- load_file "test/windows/wdk/bepo.c"
    let (l', pErrors) = prepareLayout l
    (vks, headerContent, hErrors) <- runIO $ generateHeader True mempty l'
    let dkErrors = Nothing :: Maybe DeadKeyError-- [FIXME]
    (cContent, mErrors3, sErrors2, _dkErrors) <- runIO $ generateC mempty l' vks
    it "prepareLayout" do
      pErrors `shouldBe` (Nothing, Nothing)
    it "generateHeader" do
      check_header headerContent hErrors header_ref
    it "generateC" do
      check_c cContent mErrors3 Nothing sErrors2 Nothing dkErrors Nothing c_ref
  describe "Neo2 layout" do
    let l = Neo2.layout
    header_ref <- load_file "test/windows/wdk/neo2.h"
    c_ref <- load_file "test/windows/wdk/neo2.c"
    let (l', pErrors) = prepareLayout l
    (vks, headerContent, hErrors) <- runIO $ generateHeader True mempty l'
    let sErrors2_ref = Just mempty
                      { _sUnsupportedActions =
                        [ Right A.Home, Right A.End, Right A.Page_Up
                        , Right A.Page_Down, Right A.CursorLeft, Right A.CursorRight
                        , Right A.CursorUp, Right A.CursorDown, Right A.Delete
                        , Right A.Insert, Right A.Undo]
                      }
    let dkErrors = Nothing :: Maybe DeadKeyError-- [FIXME]
    (cContent, mErrors3, sErrors2, _dkErrors) <- runIO $ generateC mempty l' vks
    it "prepareLayout" do
      pErrors `shouldBe` (Nothing, Nothing)
    it "generateHeader" do
      check_header headerContent hErrors header_ref
    it "generateC" do
      check_c cContent mErrors3 Nothing sErrors2 sErrors2_ref dkErrors Nothing c_ref
  where
    checkLayout l h_ref c_ref = do
      let (l', pErrors) = prepareLayout l
      (vks, headerContent, hErrors) <- runIO $ generateHeader True mempty l'
      (cContent, mErrors3, sErrors2, dkErrors) <- runIO $ generateC mempty l' vks
      it "prepareLayout" do
        pErrors `shouldBe` (Nothing, Nothing)
      it "generateHeader" do
        check_header headerContent hErrors h_ref
      it "generateC" do
        check_c cContent mErrors3 Nothing sErrors2 Nothing dkErrors Nothing c_ref
    check_header mh hErrors h_ref = do
      hErrors `shouldBe` Nothing
      case mh of
        Nothing -> expectationFailure "Empty file"
        Just h ->
          compareLines (T.lines . TL.toStrict $ h) (T.lines h_ref)
    check_c mc mErrors mErrors_ref sErrors sErrors_ref dkErrors dkErrors_ref c_ref = do
      mErrors `shouldBe` mErrors_ref
      sErrors `shouldBe` sErrors_ref
      dkErrors `shouldBe` dkErrors_ref
      case mc of
        Nothing -> expectationFailure "Empty file"
        Just c ->
          compareLines (T.lines . TL.toStrict $ c) (T.lines c_ref)

    load_file = runIO . (getDataFileName >=> read_utf_8)

    compareLines :: [T.Text] -> [T.Text] -> IO ()
    compareLines xs xs' = traverse_ f $ align (zip [(1::Int)..] xs) (zip [(1::Int)..] xs')
      where
        f (These a b) = a `shouldBe` b
        f (This (n, a)) = expectationFailure $ mconcat ["Unexpected data at line ", show n, ": " <> show a]
        f (That (n, b)) = expectationFailure $ mconcat ["Expected data at line ", show n, ": " <> show b]
