{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Export.CSVSpec
  ( spec
  ) where


import Data.Either (fromRight)
import Control.Monad ((>=>))
import Data.List.NonEmpty qualified as NE
import Data.Vector qualified as V
import System.IO.Unsafe (unsafePerformIO)
import Data.ByteString.Lazy qualified as BL
import Data.Foldable

import Test.Hspec
import Data.Csv (decodeByName, FromNamedRecord)
import Data.Align (align)
import Data.These

import Paths_klay_core (getDataFileName)
import Klay.Export.CSV
import Klay.Keyboard.Layout (Layout(..), MultiOsRawLayout, finalGroupsLayouts)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.OS (OsSpecific(OsIndependent))


spec :: Spec
spec =
  describe "generateGroupCsvContent" do
    describe "US layout" do
      checkLayout US.layout usSummary usDetailed
    describe "ES layout" do
      checkLayout ES.layout esSummary esDetailed
    describe "DE layout" do
      checkLayout DE.layout deSummary deDetailed
  where
    checkLayout l summary detailed = do
      let filesContents = generateGroupsCsvContent l
      describe "Simple" do
        let summary_files = fst <$> filesContents
        compareGroups summary_files [summary]
      describe "Detailed" do
        let files = snd <$> filesContents
        compareGroups files [detailed]

    generateGroupsCsvContent :: MultiOsRawLayout -> [([SimpleCharacterEntry], [DetailedCharacterEntry])]
    generateGroupsCsvContent layout = fmap (generateGroupCsvContent dkd) . NE.toList . groupsToList . finalGroupsLayouts OsIndependent $ layout
      where dkd = _deadKeys layout

    compareGroups xs xs' = traverse_ compare_group $ zip3 [(1::Int)..] xs xs'
    compare_group (i, xs, xs') = it ("Group: " <> show i) $ compareLines xs xs'
    compareLines xs xs' = traverse_ f $ align (zip [(1::Int)..] xs) (zip [(1::Int)..] xs')
      where
        f (These a b) = a `shouldBe` b
        f (This (n, a)) = expectationFailure $ mconcat ["Unexpected data at line ", show n, ": " <> show a]
        f (That (n, b)) = expectationFailure $ mconcat ["Expected data at line ", show n, ": " <> show b]

-- [NOTE] Cassava assumes UTF-8 encoding for 'Char'.
--        See: https://hackage.haskell.org/package/cassava-0.5.2.0/docs/Data-Csv.html#t:FromField
usSummary :: [SimpleCharacterEntry]
usSummary = unsafeLoad "test/csv/US/1 – English – summary.csv"
usDetailed :: [DetailedCharacterEntry]
usDetailed = unsafeLoad "test/csv/US/1 – English – detailed.csv"

deSummary :: [SimpleCharacterEntry]
deSummary = unsafeLoad "test/csv/DE/1 – Deutsch – summary.csv"
deDetailed :: [DetailedCharacterEntry]
deDetailed = unsafeLoad "test/csv/DE/1 – Deutsch – detailed.csv"

esSummary :: [SimpleCharacterEntry]
esSummary = unsafeLoad "test/csv/ES/1 – Español – summary.csv"
esDetailed :: [DetailedCharacterEntry]
esDetailed = unsafeLoad "test/csv/ES/1 – Español – detailed.csv"

unsafeLoad :: (FromNamedRecord a) => FilePath -> [a]
unsafeLoad
  = V.toList
  . snd
  . fromRight mempty
  . decodeByName
  . unsafePerformIO
  . (getDataFileName >=> BL.readFile)
