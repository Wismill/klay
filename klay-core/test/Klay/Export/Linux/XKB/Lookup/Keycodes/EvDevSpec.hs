module Klay.Export.Linux.XKB.Lookup.Keycodes.EvDevSpec
  ( spec
  ) where

import Data.Bits (xor)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map

import Test.Hspec

import Klay.Export.Linux.XKB.Lookup.Keycodes.EvDev (xkbKeycodeMapping)

{-|
Ensure all keys are mapped to a keycode
-}
spec :: Spec
spec =
  describe "Keycodes" do
    it "Check keys" do
      checkKey <$> keys `shouldBe` (,True) <$> keys
  where
    keys = enumFromTo minBound maxBound
    checkKey key = (key, (key `Set.member` expected_failures) `xor` (key `elem` keysMapped))
    keysMapped = Map.keysSet xkbKeycodeMapping
    expected_failures = mempty
