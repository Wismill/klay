module Klay.Export.Linux.XKB.Lookup.ActionsSpec
  ( spec
  ) where

import Data.Maybe

import Test.Hspec

import Klay.Export.Linux.XKB.Lookup (toXkbAction)

{-|
Ensure all keys are mapped to a keycode
-}
spec :: Spec
spec =
  describe "Actions" do
    it "Check actions" do
      check <$> actions `shouldBe` (,True) <$> actions
  where
    actions = enumFromTo minBound maxBound
    check action = (action, isJust . toXkbAction $ action)
