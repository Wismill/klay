module Klay.Export.Linux.XKBSpec
  ( spec
  ) where

import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Foldable (traverse_)
import Control.Monad ((>=>), when)

import Test.Hspec
import Data.Text.Lazy.IO.Utf8 qualified as Utf8
import Data.Align (align)
import Data.These

import Paths_klay_core (getDataFileName)
import Klay.Export.Linux.XKB
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Carpalx qualified as Carpalx
import Klay.Keyboard.Layout.Examples.Colemak qualified as Colemak
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Neo2 qualified as Neo2
import Klay.Keyboard.Layout.Examples.Workman qualified as Workman
import Klay.OS (linux)
import Klay.Utils.UserInput


{- |
Ensure predefined layout have no errors
-}
spec :: Spec
spec =
  describe "ISO" do
    describe "Bépo" do
      checkLayout Bépo.layout True
    describe "Carpalx" do
      checkLayout Carpalx.layout False
    describe "Colemak" do
      checkLayout Colemak.layout False
    describe "Qwertz_DE" do
      checkLayout DE.layout True
    describe "Qwerty_ES" do
      checkLayout ES.layout True
    describe "Neo2" do
      checkLayout Neo2.layout True
    describe "Qwerty_US" do
      checkLayout US.layout True
    describe "Workman" do
      checkLayout Workman.layout False
  where
    checkLayout layout check_files = do
      let lname = escapeSafeFilename' . _systemName . _metadata $ layout
      let l = resolveLayout linux layout
      xkb <- runIO $ getXkbContent l
      xkbSymbols_ref <- if check_files then runIO $ load_file ("test/linux/xkb/symbols/" <> lname <> ".xkb") else pure mempty
      xkbTypes_ref <- if check_files then runIO $ load_file ("test/linux/xkb/types/" <> lname <> ".xkb") else pure mempty
      xkbCompose_ref <- if check_files then runIO $ load_file ("test/linux/xkb/compose/" <> lname) else pure mempty
      it "Symbols" do
        xkbSymbolsErrors xkb `shouldBe` Nothing
        when check_files $ compareLines' (xkbSymbols xkb) xkbSymbols_ref
      it "Types" do
        when check_files $ compareLines' (xkbTypes xkb) xkbTypes_ref
      it "Compose" do
        xkbComposeErrors xkb `shouldBe` Nothing
        when check_files $ compareLines' (xkbCompose xkb) xkbCompose_ref
    load_file = getDataFileName >=> Utf8.readFile

    compareLines' t1 t2 = compareLines (T.lines . TL.toStrict $ t1) (T.lines . TL.toStrict $ t2)

    compareLines :: [T.Text] -> [T.Text] -> IO ()
    compareLines xs xs' = traverse_ f $ align (zip [(1::Int)..] xs) (zip [(1::Int)..] xs')
      where
        f (These a b) = a `shouldBe` b
        f (This (n, a)) = expectationFailure $ mconcat ["Unexpected data at line ", show n, ": " <> show a]
        f (That (n, b)) = expectationFailure $ mconcat ["Expected data at line ", show n, ": " <> show b]
