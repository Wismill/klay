{-# LANGUAGE OverloadedStrings #-}

module Klay.Export.JsonSpec
  ( spec
  ) where


import Test.Hspec

import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Bépo      qualified as Bépo
import Klay.Keyboard.Layout.Examples.Neo2      qualified as Neo2
import Klay.Export.Json (generateJson)
import Klay.Import.Json (importJson)

spec :: Spec
spec = do
  describe "US" do
    roundtrip US.layout
  describe "DE" do
    roundtrip DE.layout
  describe "ES" do
    roundtrip ES.layout
  describe "Bépo" do
    roundtrip Bépo.layout
  describe "Neo2" do
    roundtrip Neo2.layout
  where
    roundtrip l = it "Roundtrip export -> import" do
      importJson (generateJson l) `shouldBe` Right l
