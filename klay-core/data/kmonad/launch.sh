#!/bin/sh
set -u

# Get the directory path of this script
kmonad_dir=$(dirname "$0")
kmonad_dir=$(realpath "$kmonad_dir")

# Pause to avoid last key pressed to be stuck
sleep 1

# Launch Kmonad in background
echo "[INFO] Launching kmonad in background…"
kmonad $@ "$kmonad_dir/{{ layout }}.kbd" &
kmonad_pid=$!
echo "[INFO] Kmonad launched. Subprocess PID: $kmonad_pid"

# Switch to the new XKB layout after saving current configuration
sleep 1
(cd "$kmonad_dir/../linux/xkb" && ./switch.sh -r -s)

# Check if we are running in interactive mode
if [ -t 0 ]; then 
    # Wait for user input to stop Kmonad
    echo -e "Ready. Press enter to \e[3mrestore\e[0m keyboard layout…"
    read -p ""
    # Stop Kmonad
    echo "[INFO] Stopping Kmonad…"
    kill $kmonad_pid || echo "[WARNING] Kmonad was not running"
else
    echo "[INFO] Ready. To switch back to the previous configuration, terminate Kmonad."
    # Wait for Kmonad to finish
    wait $kmonad_pid
fi

echo "[INFO] Kmonad stopped."

# Switch back to the previous layout
(cd "$kmonad_dir/../linux/xkb" && ./switch.sh -r)
