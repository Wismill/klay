#!/bin/sh
set -eu

# Get the directory path of this script
kmonad_dir=$(dirname "$0")
kmonad_dir=$(realpath "$kmonad_dir")

# Switch kmonad on if not running, else terminate it
# Check if we are running in interactive mode
if [ -t 0 ]; then 
    ps -C kmonad >/dev/null && killall kmonad || "$kmonad_dir/launch"
else
    ps -C kmonad >/dev/null && killall kmonad || ("$kmonad_dir/launch"&)
fi