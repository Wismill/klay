{{{ header }}}
$CURRENT_LOCATION = Get-Location
Set-Location $PSScriptRoot

# Configuration

$MSKLC_DIR = "C:/Program Files (x86)/Microsoft Keyboard Layout Creator 1.4"
$KBDUTOOL = "$MSKLC_DIR/bin/i386/kbdutool.exe"

$PACKAGE_DIRECTORY = "$PSScriptRoot/{{{ base_file }}}"
$I386_DIRECTORY = "$PACKAGE_DIRECTORY/i386"
$AMD64_DIRECTORY = "$PACKAGE_DIRECTORY/amd64"
$IA64_DIRECTORY = "$PACKAGE_DIRECTORY/ia64"
$WOW64_DIRECTORY = "$PACKAGE_DIRECTORY/wow64"

$KLC_FILE = "$PSScriptRoot/{{{ base_file }}}.klc"
$C_FILE = "$PSScriptRoot/{{{ base_file }}}.c"
$H_FILE = "$PSScriptRoot/{{{ base_file }}}.h"
$DEF_FILE = "$PSScriptRoot/{{{ base_file }}}.def"
$RC_FILE = "$PSScriptRoot/{{{ base_file }}}.rc"
$DLL_FILE = "$PSScriptRoot/{{{ base_file }}}.dll"

# Protect the source file to avoid them to be erased by kbdutool

Set-ItemProperty $C_FILE -name IsReadOnly -value $true
Set-ItemProperty $H_FILE -name IsReadOnly -value $true
Set-ItemProperty $DEF_FILE -name IsReadOnly -value $true
Set-ItemProperty $RC_FILE -name IsReadOnly -value $true

# Create directories
New-Item -ItemType directory -Force -Path $PACKAGE_DIRECTORY
New-Item -ItemType directory -Force -Path $I386_DIRECTORY
New-Item -ItemType directory -Force -Path $AMD64_DIRECTORY
New-Item -ItemType directory -Force -Path $IA64_DIRECTORY
New-Item -ItemType directory -Force -Path $WOW64_DIRECTORY

# Compile for the various architectures

&"$KBDUTOOL" -u -v -w -x $KLC_FILE
Move-Item -Path $DLL_FILE -Destination $I386_DIRECTORY -Force

&"$KBDUTOOL" -u -v -w -m $KLC_FILE
Move-Item -Path $DLL_FILE -Destination $AMD64_DIRECTORY -Force

&"$KBDUTOOL" -u -v -w -i $KLC_FILE
Move-Item -Path $DLL_FILE -Destination $IA64_DIRECTORY -Force

&"$KBDUTOOL" -u -v -w -o $KLC_FILE
Move-Item -Path $DLL_FILE -Destination $WOW64_DIRECTORY -Force

# Remove the protection of the files

Set-ItemProperty $C_FILE -name IsReadOnly -value $false
Set-ItemProperty $H_FILE -name IsReadOnly -value $false
Set-ItemProperty $DEF_FILE -name IsReadOnly -value $false
Set-ItemProperty $RC_FILE -name IsReadOnly -value $false

Set-Location $CURRENT_LOCATION
