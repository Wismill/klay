{{{ header }}}
#define KBD_TYPE {{{ kbd_type }}}
#include "kbd.h"
{{#is_wdk}}
#include <dontuse.h>
{{/is_wdk}}

// Extra definitions for FE keyboards [FIXME]
#define VK_DBE_ALPHANUMERIC              0x0f0
#define VK_DBE_KATAKANA                  0x0f1
#define VK_DBE_HIRAGANA                  0x0f2
#define VK_DBE_ROMAN                     0x0f5
#define VK_DBE_NOROMAN                   0x0f6

// Modifiers bits
// Windows predefined values:
// * KBDSHIFT       1<<0
// * KBDCTRL        1<<1
// * KBDALT         1<<2
// * KBDKANA        1<<3
// * KBDROYA        1<<4
// * KBDLOYA        1<<5
// * KBDGRPSELTAP   1<<7
#define KBD_NUMERIC     1<<{{ numeric_shift }}
#define KBD_ISO_LEVEL_3 1<<{{ iso_level_3_shift }}
#define KBD_ISO_LEVEL_5 1<<{{ iso_level_5_shift }}

// Extra definitions of TYPEDEF_VK_TO_WCHARS
{{{ type_def_vk_to_wchars }}}

// Swap first arguments of the macro of the Windows API.
#undef DEADTRANS
#define DEADTRANS(dk, ch, comp, flags) { MAKELONG(ch, dk), comp, flags}

// Flag for the result of dead key combo
#define NORMAL_CHARACTER 0x0000
#define CHAINED_DEAD_KEY 0x0001 // DKF_DEAD

// Extra definitions of TYPEDEF_LIGATURE
{{{ type_def_ligature }}}

// Mapping scancode -> virtual key
{{{ virtual_key_map }}}
