{{{ header }}}
#include <windows.h>
#include "kbd.h"
#include "{{{ kbd_file }}}.h"

#if defined(_M_IA64)
#pragma section(".data")
#define ALLOC_SECTION_LDATA __declspec(allocate(".data"))
#else
#pragma data_seg(".data")
#define ALLOC_SECTION_LDATA
#endif

// ausVK[] - Virtual Scan Code to Virtual Key conversion table
static ALLOC_SECTION_LDATA USHORT ausVK[] = {
  T00, T01, T02, T03, T04, T05, T06, T07,
  T08, T09, T0A, T0B, T0C, T0D, T0E, T0F,
  T10, T11, T12, T13, T14, T15, T16, T17,
  T18, T19, T1A, T1B, T1C, T1D, T1E, T1F,
  T20, T21, T22, T23, T24, T25, T26, T27,
  T28, T29, T2A, T2B, T2C, T2D, T2E, T2F,
  T30, T31, T32, T33, T34, T35,

  /*
   * Right-hand Shift key must have KBDEXT bit set.
   */
  T36 | KBDEXT,
  T37 | KBDMULTIVK,               // numpad_* + Shift/Alt -> SnapShot

  T38, T39, T3A, T3B, T3C, T3D, T3E,
  T3F, T40, T41, T42, T43, T44,

  /*
   * NumLock Key:
   *     KBDEXT     - VK_NUMLOCK is an Extended key
   *     KBDMULTIVK - VK_NUMLOCK or VK_PAUSE (without or with CTRL)
   */
  T45 | KBDEXT | KBDMULTIVK,
  T46 | KBDMULTIVK,

  /*
   * Number Pad keys:
   *     KBDNUMPAD  - digits 0-9 and decimal point.
   *     KBDSPECIAL - require special processing by Windows
   */
  T47 | KBDNUMPAD | KBDSPECIAL,   // Numpad 7 (Home)
  T48 | KBDNUMPAD | KBDSPECIAL,   // Numpad 8 (Up),
  T49 | KBDNUMPAD | KBDSPECIAL,   // Numpad 9 (PgUp),
  T4A,
  T4B | KBDNUMPAD | KBDSPECIAL,   // Numpad 4 (Left),
  T4C | KBDNUMPAD | KBDSPECIAL,   // Numpad 5 (Clear),
  T4D | KBDNUMPAD | KBDSPECIAL,   // Numpad 6 (Right),
  T4E,
  T4F | KBDNUMPAD | KBDSPECIAL,   // Numpad 1 (End),
  T50 | KBDNUMPAD | KBDSPECIAL,   // Numpad 2 (Down),
  T51 | KBDNUMPAD | KBDSPECIAL,   // Numpad 3 (PgDn),
  T52 | KBDNUMPAD | KBDSPECIAL,   // Numpad 0 (Ins),
  T53 | KBDNUMPAD | KBDSPECIAL,   // Numpad . (Del),

  T54, T55, T56, T57, T58, T59, T5A, T5B,
  T5C, T5D, T5E, T5F, T60, T61, T62, T63,
  T64, T65, T66, T67, T68, T69, T6A, T6B,
  T6C, T6D, T6E, T6F, T70, T71, T72, T73,
  T74, T75, T76, T77, T78, T79, T7A, T7B,
  T7C, T7D, T7E, T7F
};

static ALLOC_SECTION_LDATA VSC_VK aE0VscToVk[] = {
  { 0x10, X10 | KBDEXT              },  // Speedracer: Previous Track
  { 0x19, X19 | KBDEXT              },  // Speedracer: Next Track
  { 0x1D, X1D | KBDEXT              },  // RControl
  { 0x20, X20 | KBDEXT              },  // Speedracer: Volume Mute
  { 0x21, X21 | KBDEXT              },  // Speedracer: Launch App 2
  { 0x22, X22 | KBDEXT              },  // Speedracer: Media Play/Pause
  { 0x24, X24 | KBDEXT              },  // Speedracer: Media Stop
  { 0x2E, X2E | KBDEXT              },  // Speedracer: Volume Down
  { 0x30, X30 | KBDEXT              },  // Speedracer: Volume Up
  { 0x32, X32 | KBDEXT              },  // Speedracer: Browser Home
  { 0x35, X35 | KBDEXT              },  // Numpad Divide
  { 0x37, X37 | KBDEXT              },  // Snapshot
  { 0x38, X38 | KBDEXT              },  // RMenu
  { 0x47, X47 | KBDEXT              },  // Home
  { 0x48, X48 | KBDEXT              },  // Up
  { 0x49, X49 | KBDEXT              },  // Prior
  { 0x4B, X4B | KBDEXT              },  // Left
  { 0x4D, X4D | KBDEXT              },  // Right
  { 0x4F, X4F | KBDEXT              },  // End
  { 0x50, X50 | KBDEXT              },  // Down
  { 0x51, X51 | KBDEXT              },  // Next
  { 0x52, X52 | KBDEXT              },  // Insert
  { 0x53, X53 | KBDEXT              },  // Delete
  { 0x5B, X5B | KBDEXT              },  // Left Win
  { 0x5C, X5C | KBDEXT              },  // Right Win
  { 0x5D, X5D | KBDEXT              },  // Application
  { 0x5F, X5F | KBDEXT              },  // Speedracer: Sleep
  { 0x65, X65 | KBDEXT              },  // Speedracer: Browser Search
  { 0x66, X66 | KBDEXT              },  // Speedracer: Browser Favorites
  { 0x67, X67 | KBDEXT              },  // Speedracer: Browser Refresh
  { 0x68, X68 | KBDEXT              },  // Speedracer: Browser Stop
  { 0x69, X69 | KBDEXT              },  // Speedracer: Browser Forward
  { 0x6A, X6A | KBDEXT              },  // Speedracer: Browser Back
  { 0x6B, X6B | KBDEXT              },  // Speedracer: Launch App 1
  { 0x6C, X6C | KBDEXT              },  // Speedracer: Launch Mail
  { 0x6D, X6D | KBDEXT              },  // Speedracer: Launch Media Selector
  { 0x1C, X1C | KBDEXT              },  // Numpad Enter
  { 0x46, X46 | KBDEXT              },  // Break (Ctrl + Pause)
  { 0,      0                       }
};

static ALLOC_SECTION_LDATA VSC_VK aE1VscToVk[] = {
  { 0x1D, Y1D                       },  // Pause
  { 0   ,   0                       }
};

// aVkToBits[] - map Virtual Keys to Modifier Bits
static ALLOC_SECTION_LDATA VK_TO_BIT aVkToBits[] = {
{{{ aVkToBits }}}  { 0, 0 }
};

// aModification[] - map character modifier bits to modification number
static ALLOC_SECTION_LDATA MODIFIERS CharModifiers = {
  &aVkToBits[0],
  {{{ level_total_number }}},
  {
{{{ level_definitions }}}
  }
};

/***************************************************************************\\
*
* aVkToWch<n>[]  - Virtual Key to WCHAR translation for <n> shift states
*
* Table attributes: Unordered Scan, null-terminated
*
* Search this table for an entry with a matching Virtual Key to find the
* corresponding unshifted and shifted WCHAR characters.
*
* Special values for VirtualKey (column 1)
*     0xff          - dead chars for the previous entry
*     0             - terminate the list
*
* Special values for Attributes (column 2)
*     CAPLOK bit    - CAPS-LOCK affect this key like SHIFT
*
* Special values for wch[*] (column 3 & 4)
*     WCH_NONE      - No character
*     WCH_DEAD      - Dead Key (diaresis) or invalid (US keyboard has none)
*     WCH_LGTR      - Ligature (generates multiple characters)
*
\\***************************************************************************/

{{{ aVkToWchs }}}

// aVkToWcharTable - List the various aVkToWchN tables in use
static ALLOC_SECTION_LDATA VK_TO_WCHAR_TABLE aVkToWcharTable[] = {
{{{ aVkToWcharTable }}}
  {                      NULL , 0 , 0                     },
};

/***************************************************************************\\
* aKeyNames[], aKeyNamesExt[] - Virtual Scancode to Key Name tables
*
* Table attributes: Ordered Scan (by scancode), null-terminated
*
* Only the names of Extended, NumPad, Dead and Non-Printable keys are here.
* (Keys producing printable characters are named by that character)
\\***************************************************************************/
static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNames[] = {
{{{ aKeyNames }}}  0, NULL
};

static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNamesExt[] = {
{{{ aKeyNamesExt }}}  0, NULL
};

/***************************************************************************\\
 * Dead Keys
\\***************************************************************************/
static ALLOC_SECTION_LDATA DEADKEY_LPWSTR aKeyNamesDead[] = {
{{{ aKeyNamesDead }}}
  NULL
};

static ALLOC_SECTION_LDATA DEADKEY aDeadKey[] = {
{{{ aDeadKey }}}
  0, 0
};

/***************************************************************************\\
 * Ligatures
\\***************************************************************************/
{{{ aLigature }}}

static ALLOC_SECTION_LDATA KBDTABLES KbdTables = {
  // Modifier keys
  &CharModifiers,

  // Characters tables
  aVkToWcharTable,

  // Diacritics
  aDeadKey,

  // Names of Keys
  aKeyNames,
  aKeyNamesExt,
  aKeyNamesDead,

  // Scan codes to Virtual Keys
  ausVK,
  sizeof(ausVK) / sizeof(ausVK[0]),
  aE0VscToVk,
  aE1VscToVk,

  // Locale-specific special processing
  MAKELONG({{{ locale_options }}}, KBD_VERSION),

  // Ligatures
{{{ KbdTables_ligatures }}}
};

PKBDTABLES KbdLayerDescriptor(VOID)
{
  return &KbdTables;
};
