
#include <windows.h>
#include "kbd.h"
#include "bepo.h"

#if defined(_M_IA64)
#pragma section(".data")
#define ALLOC_SECTION_LDATA __declspec(allocate(".data"))
#else
#pragma data_seg(".data")
#define ALLOC_SECTION_LDATA
#endif

// ausVK[] - Virtual Scan Code to Virtual Key conversion table
static ALLOC_SECTION_LDATA USHORT ausVK[] = {
  T00, T01, T02, T03, T04, T05, T06, T07,
  T08, T09, T0A, T0B, T0C, T0D, T0E, T0F,
  T10, T11, T12, T13, T14, T15, T16, T17,
  T18, T19, T1A, T1B, T1C, T1D, T1E, T1F,
  T20, T21, T22, T23, T24, T25, T26, T27,
  T28, T29, T2A, T2B, T2C, T2D, T2E, T2F,
  T30, T31, T32, T33, T34, T35,

  /*
   * Right-hand Shift key must have KBDEXT bit set.
   */
  T36 | KBDEXT,
  T37 | KBDMULTIVK,               // numpad_* + Shift/Alt -> SnapShot

  T38, T39, T3A, T3B, T3C, T3D, T3E,
  T3F, T40, T41, T42, T43, T44,

  /*
   * NumLock Key:
   *     KBDEXT     - VK_NUMLOCK is an Extended key
   *     KBDMULTIVK - VK_NUMLOCK or VK_PAUSE (without or with CTRL)
   */
  T45 | KBDEXT | KBDMULTIVK,
  T46 | KBDMULTIVK,

  /*
   * Number Pad keys:
   *     KBDNUMPAD  - digits 0-9 and decimal point.
   *     KBDSPECIAL - require special processing by Windows
   */
  T47 | KBDNUMPAD | KBDSPECIAL,   // Numpad 7 (Home)
  T48 | KBDNUMPAD | KBDSPECIAL,   // Numpad 8 (Up),
  T49 | KBDNUMPAD | KBDSPECIAL,   // Numpad 9 (PgUp),
  T4A,
  T4B | KBDNUMPAD | KBDSPECIAL,   // Numpad 4 (Left),
  T4C | KBDNUMPAD | KBDSPECIAL,   // Numpad 5 (Clear),
  T4D | KBDNUMPAD | KBDSPECIAL,   // Numpad 6 (Right),
  T4E,
  T4F | KBDNUMPAD | KBDSPECIAL,   // Numpad 1 (End),
  T50 | KBDNUMPAD | KBDSPECIAL,   // Numpad 2 (Down),
  T51 | KBDNUMPAD | KBDSPECIAL,   // Numpad 3 (PgDn),
  T52 | KBDNUMPAD | KBDSPECIAL,   // Numpad 0 (Ins),
  T53 | KBDNUMPAD | KBDSPECIAL,   // Numpad . (Del),

  T54, T55, T56, T57, T58, T59, T5A, T5B,
  T5C, T5D, T5E, T5F, T60, T61, T62, T63,
  T64, T65, T66, T67, T68, T69, T6A, T6B,
  T6C, T6D, T6E, T6F, T70, T71, T72, T73,
  T74, T75, T76, T77, T78, T79, T7A, T7B,
  T7C, T7D, T7E, T7F
};

static ALLOC_SECTION_LDATA VSC_VK aE0VscToVk[] = {
  { 0x10, X10 | KBDEXT              },  // Speedracer: Previous Track
  { 0x19, X19 | KBDEXT              },  // Speedracer: Next Track
  { 0x1D, X1D | KBDEXT              },  // RControl
  { 0x20, X20 | KBDEXT              },  // Speedracer: Volume Mute
  { 0x21, X21 | KBDEXT              },  // Speedracer: Launch App 2
  { 0x22, X22 | KBDEXT              },  // Speedracer: Media Play/Pause
  { 0x24, X24 | KBDEXT              },  // Speedracer: Media Stop
  { 0x2E, X2E | KBDEXT              },  // Speedracer: Volume Down
  { 0x30, X30 | KBDEXT              },  // Speedracer: Volume Up
  { 0x32, X32 | KBDEXT              },  // Speedracer: Browser Home
  { 0x35, X35 | KBDEXT              },  // Numpad Divide
  { 0x37, X37 | KBDEXT              },  // Snapshot
  { 0x38, X38 | KBDEXT              },  // RMenu
  { 0x47, X47 | KBDEXT              },  // Home
  { 0x48, X48 | KBDEXT              },  // Up
  { 0x49, X49 | KBDEXT              },  // Prior
  { 0x4B, X4B | KBDEXT              },  // Left
  { 0x4D, X4D | KBDEXT              },  // Right
  { 0x4F, X4F | KBDEXT              },  // End
  { 0x50, X50 | KBDEXT              },  // Down
  { 0x51, X51 | KBDEXT              },  // Next
  { 0x52, X52 | KBDEXT              },  // Insert
  { 0x53, X53 | KBDEXT              },  // Delete
  { 0x5B, X5B | KBDEXT              },  // Left Win
  { 0x5C, X5C | KBDEXT              },  // Right Win
  { 0x5D, X5D | KBDEXT              },  // Application
  { 0x5F, X5F | KBDEXT              },  // Speedracer: Sleep
  { 0x65, X65 | KBDEXT              },  // Speedracer: Browser Search
  { 0x66, X66 | KBDEXT              },  // Speedracer: Browser Favorites
  { 0x67, X67 | KBDEXT              },  // Speedracer: Browser Refresh
  { 0x68, X68 | KBDEXT              },  // Speedracer: Browser Stop
  { 0x69, X69 | KBDEXT              },  // Speedracer: Browser Forward
  { 0x6A, X6A | KBDEXT              },  // Speedracer: Browser Back
  { 0x6B, X6B | KBDEXT              },  // Speedracer: Launch App 1
  { 0x6C, X6C | KBDEXT              },  // Speedracer: Launch Mail
  { 0x6D, X6D | KBDEXT              },  // Speedracer: Launch Media Selector
  { 0x1C, X1C | KBDEXT              },  // Numpad Enter
  { 0x46, X46 | KBDEXT              },  // Break (Ctrl + Pause)
  { 0,      0                       }
};

static ALLOC_SECTION_LDATA VSC_VK aE1VscToVk[] = {
  { 0x1D, Y1D                       },  // Pause
  { 0   ,   0                       }
};

// aVkToBits[] - map Virtual Keys to Modifier Bits
static ALLOC_SECTION_LDATA VK_TO_BIT aVkToBits[] = {
  { VK_CONTROL   , KBDCTRL                        },
  { VK_MENU      , KBDALT                         },
  { VK_SHIFT     , KBDSHIFT                       },
  { 0, 0 }
};

// aModification[] - map character modifier bits to modification number
static ALLOC_SECTION_LDATA MODIFIERS CharModifiers = {
  &aVkToBits[0],
  8,
  {
  // Level index -> Modifiers pressed       -> Level name
    0            /*                         -> ISO Level 1              */,
    1            /* Shift                   -> ISO Level 2              */,
    2            /*       Control           -> Control                  */,
    SHFT_INVALID /* Shift Control           -> (No valid level)         */,
    SHFT_INVALID /*               Alternate -> (No valid level)         */,
    SHFT_INVALID /* Shift         Alternate -> (No valid level)         */,
    3            /*       Control Alternate -> ISO Level 3              */,
    4            /* Shift Control Alternate -> ISO Level 4              */
  }
};

/***************************************************************************\\
*
* aVkToWch<n>[]  - Virtual Key to WCHAR translation for <n> shift states
*
* Table attributes: Unordered Scan, null-terminated
*
* Search this table for an entry with a matching Virtual Key to find the
* corresponding unshifted and shifted WCHAR characters.
*
* Special values for VirtualKey (column 1)
*     0xff          - dead chars for the previous entry
*     0             - terminate the list
*
* Special values for Attributes (column 2)
*     CAPLOK bit    - CAPS-LOCK affect this key like SHIFT
*
* Special values for wch[*] (column 3 & 4)
*     WCH_NONE      - No character
*     WCH_DEAD      - Dead Key (diaresis) or invalid (US keyboard has none)
*     WCH_LGTR      - Ligature (generates multiple characters)
*
\\***************************************************************************/

// Errors encountered while creating aVkToWch tables:

static ALLOC_SECTION_LDATA VK_TO_WCHARS2 aVkToWchOther2[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 
  /*                                KP_Decimal, KP_Decimal */
  {VK_ABNT_C2   , 0               , L'.'    , L'.'    },
  /*                                KP_Add  , KP_Add   */
  {VK_ADD       , 0               , L'+'    , L'+'    },
  /*                                KP_Equal, KP_Equal */
  {VK_CLEAR     , 0               , L'='    , L'='    },
  /*                                KP_Separator, KP_Separator */
  {VK_DECIMAL   , 0               , L','    , L','    },
  /*                                KP_Divide, KP_Divide */
  {VK_DIVIDE    , 0               , L'/'    , L'/'    },
  /*                                KP_Multiply, KP_Multiply */
  {VK_MULTIPLY  , 0               , L'*'    , L'*'    },
  /*                                KP_Subtract, KP_Subtract */
  {VK_SUBTRACT  , 0               , L'-'    , L'-'    },
  /*                                Tab     , Tab      */
  {VK_TAB       , 0               , L'\t'   , L'\t'   },
  {0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS3 aVkToWchOther3[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 
  /*                                BackSpace, BackSpace, U+007F ⌦ */
  {VK_BACK      , 0               , 0x0008  , 0x0008  , 0x007f  },
  /*                                U+0003 ␃, U+0003 ␃, U+0003 ␃ */
  {VK_CANCEL    , 0               , 0x0003  , 0x0003  , 0x0003  },
  /*                                Escape  , Escape  , Escape   */
  {VK_ESCAPE    , 0               , 0x001b  , 0x001b  , 0x001b  },
  /*                                Return  , Return  , U+000A ␊ */
  {VK_RETURN    , 0               , L'\r'   , L'\r'   , L'\n'   },
  {0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS5 aVkToWchOther5[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 , Level 3 , Level 4 
  /*                                U+002A *, U+0030 0, �       , U+00D7 ×, U+00BE ¾ */
  {'0'          , CAPLOK          , L'*'    , L'0'    , WCH_NONE, 0x00d7  , 0x00be  },
  /*                                U+0022 ", U+0031 1, �       , U+2014 —, U+201E „ */
  {'1'          , CAPLOK          , L'"'    , L'1'    , WCH_NONE, 0x2014  , 0x201e  },
  /*                                U+00AB «, U+0032 2, �       , U+003C <, U+201C “ */
  {'2'          , CAPLOK          , 0x00ab  , L'2'    , WCH_NONE, L'<'    , 0x201c  },
  /*                                U+00BB », U+0033 3, �       , U+003E >, U+201D ” */
  {'3'          , CAPLOK          , 0x00bb  , L'3'    , WCH_NONE, L'>'    , 0x201d  },
  /*                                U+0028 (, U+0034 4, �       , U+005B [, U+2A7D ⩽ */
  {'4'          , CAPLOK          , L'('    , L'4'    , WCH_NONE, L'['    , 0x2a7d  },
  /*                                U+0029 ), U+0035 5, �       , U+005D ], U+2A7E ⩾ */
  {'5'          , CAPLOK          , L')'    , L'5'    , WCH_NONE, L']'    , 0x2a7e  },
  /*                                U+0040 @, U+0036 6, �       , U+005E ^, U+262D ☭ */
  {'6'          , CAPLOK          , L'@'    , L'6'    , WCH_NONE, L'^'    , 0x262d  },
  /*                                U+002B +, U+0037 7, �       , U+00B1 ±, U+00AC ¬ */
  {'7'          , CAPLOK          , L'+'    , L'7'    , WCH_NONE, 0x00b1  , 0x00ac  },
  /*                                U+002D -, U+0038 8, �       , U+2212 −, U+00BC ¼ */
  {'8'          , CAPLOK          , L'-'    , L'8'    , WCH_NONE, 0x2212  , 0x00bc  },
  /*                                U+002F /, U+0039 9, �       , U+00F7 ÷, U+00BD ½ */
  {'9'          , CAPLOK          , L'/'    , L'9'    , WCH_NONE, 0x00f7  , 0x00bd  },
  /*                                U+0061 a, U+0041 A, �       , U+00E6 æ, U+00C6 Æ */
  {'A'          , CAPLOK | CAPLOKALTGR, L'a'    , L'A'    , WCH_NONE, 0x00e6  , 0x00c6  },
  /*                                U+0062 b, U+0042 B, �       , U+007C |, U+005F _ */
  {'B'          , CAPLOK          , L'b'    , L'B'    , WCH_NONE, L'|'    , L'_'    },
  /*                                U+0063 c, U+0043 C, �       , ‹Cedilla›, U+00A9 © */
  {'C'          , CAPLOK          , L'c'    , L'C'    , WCH_NONE, WCH_DEAD, 0x00a9  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L','    , WCH_NONE},
  /*                                U+0064 d, U+0044 D, �       , ‹Bépo Science›, U+2623 ☣ */
  {'D'          , CAPLOK          , L'd'    , L'D'    , WCH_NONE, WCH_DEAD, 0x2623  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x221e  , WCH_NONE},
  /*                                U+0065 e, U+0045 E, �       , U+20AC €, ‹Bépo Currency› */
  {'E'          , CAPLOK          , L'e'    , L'E'    , WCH_NONE, 0x20ac  , WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, WCH_NONE, 0x00a4  },
  /*                                U+0066 f, U+0046 F, �       , ‹Ogonek›, U+26C4 ⛄ */
  {'F'          , CAPLOK          , L'f'    , L'F'    , WCH_NONE, WCH_DEAD, 0x26c4  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x02db  , WCH_NONE},
  /*                                U+0067 g, U+0047 G, �       , ‹Greek› , U+2020 † */
  {'G'          , CAPLOK          , L'g'    , L'G'    , WCH_NONE, WCH_DEAD, 0x2020  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x00b5  , WCH_NONE},
  /*                                U+0068 h, U+0048 H, �       , ‹Dot Below›, U+2021 ‡ */
  {'H'          , CAPLOK          , L'h'    , L'H'    , WCH_NONE, WCH_DEAD, 0x2021  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L'.'    , WCH_NONE},
  /*                                U+0069 i, U+0049 I, �       , ‹Diaeresis›, ‹Dot Above› */
  {'I'          , CAPLOK          , L'i'    , L'I'    , WCH_NONE, WCH_DEAD, WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L':'    , 0x02d9  },
  /*                                U+006A j, U+004A J, �       , U+262E ☮, U+262F ☯ */
  {'J'          , CAPLOK          , L'j'    , L'J'    , WCH_NONE, 0x262e  , 0x262f  },
  /*                                U+006B k, U+004B K, �       , U+007E ~, U+2011 -̺ */
  {'K'          , CAPLOK          , L'k'    , L'K'    , WCH_NONE, L'~'    , 0x2011  },
  /*                                U+006C l, U+004C L, �       , ‹Long Solidus Overlay›, U+00A3 £ */
  {'L'          , CAPLOK          , L'l'    , L'L'    , WCH_NONE, WCH_DEAD, 0x00a3  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L'/'    , WCH_NONE},
  /*                                U+006D m, U+004D M, �       , ‹Macron›, U+26FD ⛽ */
  {'M'          , CAPLOK          , L'm'    , L'M'    , WCH_NONE, WCH_DEAD, 0x26fd  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x00af  , WCH_NONE},
  /*                                U+006E n, U+004E N, �       , ‹Tilde› , U+2693 ⚓ */
  {'N'          , CAPLOK          , L'n'    , L'N'    , WCH_NONE, WCH_DEAD, 0x2693  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L'~'    , WCH_NONE},
  /*                                U+006F o, U+004F O, �       , U+0153 œ, U+0152 Œ */
  {'O'          , CAPLOK | CAPLOKALTGR, L'o'    , L'O'    , WCH_NONE, 0x0153  , 0x0152  },
  /*                                U+0070 p, U+0050 P, �       , U+0026 &, U+00A7 § */
  {'P'          , CAPLOK          , L'p'    , L'P'    , WCH_NONE, L'&'    , 0x00a7  },
  /*                                U+0071 q, U+0051 Q, �       , ‹Ring Above›, ‹Horn›   */
  {'Q'          , CAPLOK          , L'q'    , L'Q'    , WCH_NONE, WCH_DEAD, WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x02da  , 0x031b  },
  /*                                U+0072 r, U+0052 R, �       , ‹Breve› , U+00AE ® */
  {'R'          , CAPLOK          , L'r'    , L'R'    , WCH_NONE, WCH_DEAD, 0x00ae  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x02d8  , WCH_NONE},
  /*                                U+0073 s, U+0053 S, �       , ‹Bépo Latin›, U+017F ſ */
  {'S'          , CAPLOK          , L's'    , L'S'    , WCH_NONE, WCH_DEAD, 0x017f  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x00df  , WCH_NONE},
  /*                                U+0074 t, U+0054 T, �       , ‹Superscript›, U+2122 ™ */
  {'T'          , CAPLOK          , L't'    , L'T'    , WCH_NONE, WCH_DEAD, 0x2122  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x1d49  , WCH_NONE},
  /*                                U+0075 u, U+0055 U, �       , U+00F9 ù, U+00D9 Ù */
  {'U'          , CAPLOK | CAPLOKALTGR, L'u'    , L'U'    , WCH_NONE, 0x00f9  , 0x00d9  },
  /*                                U+0076 v, U+0056 V, �       , ‹Caron› , U+2622 ☢ */
  {'V'          , CAPLOK          , L'v'    , L'V'    , WCH_NONE, WCH_DEAD, 0x2622  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x02c7  , WCH_NONE},
  /*                                U+0077 w, U+0057 W, �       , U+269C ⚜, U+267F ♿ */
  {'W'          , CAPLOK          , L'w'    , L'W'    , WCH_NONE, 0x269c  , 0x267f  },
  /*                                U+0078 x, U+0058 X, �       , U+007D }, U+2019 ’ */
  {'X'          , CAPLOK          , L'x'    , L'X'    , WCH_NONE, L'}'    , 0x2019  },
  /*                                U+0079 y, U+0059 Y, �       , U+007B {, U+2018 ‘ */
  {'Y'          , CAPLOK          , L'y'    , L'Y'    , WCH_NONE, L'{'    , 0x2018  },
  /*                                U+007A z, U+005A Z, �       , ‹Long Stroke Overlay›, U+2619 ☙ */
  {'Z'          , CAPLOK          , L'z'    , L'Z'    , WCH_NONE, WCH_DEAD, 0x2619  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x0336  , WCH_NONE},
  /*                                U+00E9 é, U+00C9 É, �       , ‹Acute Accent›, U+2665 ♥ */
  {VK_OEM_1     , CAPLOK          , 0x00e9  , 0x00c9  , WCH_NONE, WCH_DEAD, 0x2665  },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x00b4  , WCH_NONE},
  /*                                U+00EA ê, U+00CA Ê, �       , U+002F /, U+005E ^ */
  {VK_OEM_102   , CAPLOK          , 0x00ea  , 0x00ca  , WCH_NONE, L'/'    , L'^'    },
  /*                                U+00E8 è, U+00C8 È, �       , ‹Grave Accent›, U+0060 ` */
  {VK_OEM_2     , CAPLOK          , 0x00e8  , 0x00c8  , WCH_NONE, WCH_DEAD, L'`'    },
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L'`'    , WCH_NONE},
  /*                                U+2019 ’, U+003F ?, �       , U+00BF ¿, ‹Crochet en chef› */
  {VK_OEM_3     , 0               , 0x2019  , L'?'    , WCH_NONE, 0x00bf  , WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, WCH_NONE, 0x1ebb  },
  /*                                ‹Circumflex Accent›, U+0021 !, �       , U+00A1 ¡, U+2620 ☠ */
  {VK_OEM_4     , 0               , WCH_DEAD, L'!'    , WCH_NONE, 0x00a1  , 0x2620  },
  {0xff         , 0               , 0x02c6  , WCH_NONE, WCH_NONE, WCH_NONE, WCH_NONE},
  /*                                U+00E7 ç, U+00C7 Ç, �       , U+2708 ✈, U+1F12F 🄯 */
  {VK_OEM_5     , CAPLOK          , 0x00e7  , 0x00c7  , WCH_NONE, 0x2708  , WCH_LGTR},
  /*                                U+00E0 à, U+00C0 À, �       , U+005C \, U+201A ‚ */
  {VK_OEM_6     , CAPLOK          , 0x00e0  , 0x00c0  , WCH_NONE, L'\\'   , 0x201a  },
  /*                                U+0024 $, U+0023 #, �       , U+2013 –, U+00B6 ¶ */
  {VK_OEM_7     , 0               , L'$'    , L'#'    , WCH_NONE, 0x2013  , 0x00b6  },
  /*                                U+002C ,, U+003B ;, �       , U+0027 ', ‹Comma Below› */
  {VK_OEM_COMMA , 0               , L','    , L';'    , WCH_NONE, L'\''   , WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, WCH_NONE, L';'    },
  /*                                U+003D =, U+00B0 °, �       , U+2260 ≠, U+2032 ′ */
  {VK_OEM_MINUS , 0               , L'='    , 0x00b0  , WCH_NONE, 0x2260  , 0x2032  },
  /*                                U+002E ., U+003A :, �       , U+2026 …, U+00B7 · */
  {VK_OEM_PERIOD, 0               , L'.'    , L':'    , WCH_NONE, 0x2026  , 0x00b7  },
  /*                                U+0025 %, U+0060 `, �       , U+2030 ‰, U+2033 ″ */
  {VK_OEM_PLUS  , 0               , L'%'    , L'`'    , WCH_NONE, 0x2030  , 0x2033  },
  /*                                U+0020 ␣, U+202F ⍽ ‹narrow›, �       , U+005F _, U+00A0 ⍽ */
  {VK_SPACE     , 0               , L' '    , 0x202f  , WCH_NONE, L'_'    , 0x00a0  },
  {0, 0, 0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS1 aVkToWchNumpad1[] = {
  // Virtual key, Modifier options, Level 0 
  /*                                KP_0     */
  {VK_NUMPAD0   , 0               , L'0'    },
  /*                                KP_1     */
  {VK_NUMPAD1   , 0               , L'1'    },
  /*                                KP_2     */
  {VK_NUMPAD2   , 0               , L'2'    },
  /*                                KP_3     */
  {VK_NUMPAD3   , 0               , L'3'    },
  /*                                KP_4     */
  {VK_NUMPAD4   , 0               , L'4'    },
  /*                                KP_5     */
  {VK_NUMPAD5   , 0               , L'5'    },
  /*                                KP_6     */
  {VK_NUMPAD6   , 0               , L'6'    },
  /*                                KP_7     */
  {VK_NUMPAD7   , 0               , L'7'    },
  /*                                KP_8     */
  {VK_NUMPAD8   , 0               , L'8'    },
  /*                                KP_9     */
  {VK_NUMPAD9   , 0               , L'9'    },
  {0, 0, 0}
};



// aVkToWcharTable - List the various aVkToWchN tables in use
static ALLOC_SECTION_LDATA VK_TO_WCHAR_TABLE aVkToWcharTable[] = {
  { (PVK_TO_WCHARS1)aVkToWchOther2 , 2 , sizeof(aVkToWchOther2[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther3 , 3 , sizeof(aVkToWchOther3[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther5 , 5 , sizeof(aVkToWchOther5[0]) },
  // The numpad keys must come last so that VkKeyScan interprets number characters
  // as coming from the main section of the kbd before considering the numpad.
  { (PVK_TO_WCHARS1)aVkToWchNumpad1 , 1 , sizeof(aVkToWchNumpad1[0]) },
  {                      NULL , 0 , 0                     },
};

/***************************************************************************\\
* aKeyNames[], aKeyNamesExt[] - Virtual Scancode to Key Name tables
*
* Table attributes: Ordered Scan (by scancode), null-terminated
*
* Only the names of Extended, NumPad, Dead and Non-Printable keys are here.
* (Keys producing printable characters are named by that character)
\\***************************************************************************/
static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNames[] = {
  0x01, L"Esc",
  0x0e, L"Backspace",
  0x0f, L"Tab",
  0x1c, L"Enter",
  0x1d, L"Left Ctrl",
  0x2a, L"Left Shift",
  0x36, L"Right Shift",
  0x37, L"Num *",
  0x38, L"Left Alt",
  0x39, L"Space",
  0x3a, L"Caps Lock",
  0x3b, L"F1",
  0x3c, L"F2",
  0x3d, L"F3",
  0x3e, L"F4",
  0x3f, L"F5",
  0x40, L"F6",
  0x41, L"F7",
  0x42, L"F8",
  0x43, L"F9",
  0x44, L"F10",
  0x45, L"Pause",
  0x46, L"Scroll Lock",
  0x47, L"Num 7",
  0x48, L"Num 8",
  0x49, L"Num 9",
  0x4a, L"Num -",
  0x4b, L"Num 4",
  0x4c, L"Num 5",
  0x4d, L"Num 6",
  0x4e, L"Num +",
  0x4f, L"Num 1",
  0x50, L"Num 2",
  0x51, L"Num 3",
  0x52, L"Num 0",
  0x53, L"Num Del",
  0x54, L"Sys Req",
  0x57, L"F11",
  0x58, L"F12",
  0x64, L"F13",
  0x65, L"F14",
  0x66, L"F15",
  0x67, L"F16",
  0x68, L"F17",
  0x69, L"F18",
  0x6a, L"F19",
  0x6b, L"F20",
  0x6c, L"F21",
  0x6d, L"F22",
  0x6e, L"F23",
  0x76, L"F24",
  0, NULL
};

static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNamesExt[] = {
  0x1c, L"Num Enter",
  0x1d, L"Right Ctrl",
  0x35, L"Num /",
  0x37, L"Prnt Scrn",
  0x38, L"Right Alt",
  0x45, L"Num Lock",
  0x46, L"Break",
  0x47, L"Home",
  0x48, L"Up",
  0x49, L"Page Up",
  0x4b, L"Left",
  0x4d, L"Right",
  0x4f, L"End",
  0x50, L"Down",
  0x51, L"Page Down",
  0x52, L"Insert",
  0x53, L"Delete",
  0x54, L"<00>",
  0x56, L"Help",
  0x5b, L"Left Windows",
  0x5c, L"Right Windows",
  0x5d, L"Application",
  0, NULL
};

/***************************************************************************\\
 * Dead Keys
\\***************************************************************************/
static ALLOC_SECTION_LDATA DEADKEY_LPWSTR aKeyNamesDead[] = {
  L","     L"Cedilla",
  L"."     L"Dot Below",
  L"/"     L"Long Solidus Overlay",
  L":"     L"Diaeresis",
  L";"     L"Comma Below",
  L"`"     L"Grave Accent",
  L"~"     L"Tilde",
  L"\u00a4"   L"Bépo Currency",
  L"\u00af"   L"Macron",
  L"\u00b4"   L"Acute Accent",
  L"\u00b5"   L"Greek",
  L"\u00df"   L"Bépo Latin",
  L"\u0181"   L"Crosse",
  L"\u02c6"   L"Circumflex Accent",
  L"\u02c7"   L"Caron",
  L"\u02cd"   L"Macron Below",
  L"\u02d8"   L"Breve",
  L"\u02d9"   L"Dot Above",
  L"\u02da"   L"Ring Above",
  L"\u02db"   L"Ogonek",
  L"\u02dd"   L"Double Acute Accent",
  L"\u02ec"   L"Caron Below",
  L"\u02f3"   L"Ring Below",
  L"\u030f"   L"Double Grave Accent",
  L"\u0311"   L"Inverted Breve",
  L"\u031b"   L"Horn",
  L"\u0336"   L"Long Stroke Overlay",
  L"\u0378"   L"Cedilla + Acute Accent",
  L"\u0379"   L"Cedilla + Breve",
  L"\u0380"   L"Dot Below + Macron",
  L"\u0381"   L"Dot Below + Circumflex Accent",
  L"\u0382"   L"Dot Below + Breve",
  L"\u0383"   L"Dot Below + Dot Above",
  L"\u038b"   L"Dot Below + Horn",
  L"\u038d"   L"Long Solidus Overlay + Acute Accent",
  L"\u03a2"   L"Long Solidus Overlay + Greek",
  L"\u0530"   L"Long Solidus Overlay + Bépo Latin",
  L"\u0557"   L"Long Solidus Overlay + Dot Above",
  L"\u0558"   L"Long Solidus Overlay + Long Stroke Overlay",
  L"\u058b"   L"Long Solidus Overlay + Subscript",
  L"\u058c"   L"Long Solidus Overlay + Bépo Science",
  L"\u0590"   L"Diaeresis + Grave Accent",
  L"\u05c8"   L"Diaeresis + Tilde",
  L"\u05c9"   L"Diaeresis + Macron",
  L"\u05ca"   L"Diaeresis + Acute Accent",
  L"\u05cb"   L"Diaeresis + Caron",
  L"\u05cc"   L"Grave Accent + Diaeresis",
  L"\u05cd"   L"Grave Accent + Macron",
  L"\u05ce"   L"Grave Accent + Circumflex Accent",
  L"\u05cf"   L"Grave Accent + Breve",
  L"\u05eb"   L"Grave Accent + Horn",
  L"\u05ec"   L"Tilde + Diaeresis",
  L"\u05ed"   L"Tilde + Macron",
  L"\u05ee"   L"Tilde + Acute Accent",
  L"\u05f5"   L"Tilde + Circumflex Accent",
  L"\u05f6"   L"Tilde + Breve",
  L"\u05f7"   L"Tilde + Horn",
  L"\u05f8"   L"Macron + Dot Below",
  L"\u05f9"   L"Macron + Diaeresis",
  L"\u05fa"   L"Macron + Grave Accent",
  L"\u05fb"   L"Macron + Tilde",
  L"\u05fc"   L"Macron + Acute Accent",
  L"\u05fd"   L"Macron + Greek",
  L"\u05fe"   L"Macron + Dot Above",
  L"\u05ff"   L"Macron + Ogonek",
  L"\u061d"   L"Macron + Bépo dot below",
  L"\u070e"   L"Macron + Bépo Science",
  L"\u074b"   L"Acute Accent + Cedilla",
  L"\u074c"   L"Acute Accent + Long Solidus Overlay",
  L"\u07b2"   L"Acute Accent + Diaeresis",
  L"\u07b3"   L"Acute Accent + Tilde",
  L"\u07b4"   L"Acute Accent + Macron",
  L"\u07b5"   L"Acute Accent + Circumflex Accent",
  L"\u07b6"   L"Acute Accent + Breve",
  L"\u07b7"   L"Acute Accent + Dot Above",
  L"\u07b8"   L"Acute Accent + Ring Above",
  L"\u07b9"   L"Acute Accent + Horn",
  L"\u07ba"   L"Greek + Long Solidus Overlay",
  L"\u07bb"   L"Greek + Macron",
  L"\u07bc"   L"Greek + Crochet en chef",
  L"\u07bd"   L"Bépo Latin + Long Solidus Overlay",
  L"\u07be"   L"Bépo Latin + Long Stroke Overlay",
  L"\u07bf"   L"Circumflex Accent + Dot Below",
  L"\u07fb"   L"Circumflex Accent + Grave Accent",
  L"\u07fc"   L"Circumflex Accent + Tilde",
  L"\u082e"   L"Circumflex Accent + Acute Accent",
  L"\u082f"   L"Circumflex Accent + Bépo dot below",
  L"\u083f"   L"Caron + Diaeresis",
  L"\u085c"   L"Caron + Bépo Latin",
  L"\u085d"   L"Caron + Dot Above",
  L"\u085f"   L"Breve + Cedilla",
  L"\u086b"   L"Breve + Dot Below",
  L"\u086c"   L"Breve + Grave Accent",
  L"\u086d"   L"Breve + Tilde",
  L"\u086e"   L"Breve + Acute Accent",
  L"\u086f"   L"Breve + Bépo dot below",
  L"\u0870"   L"Dot Above + Dot Below",
  L"\u0871"   L"Dot Above + Long Solidus Overlay",
  L"\u0872"   L"Dot Above + Macron",
  L"\u0873"   L"Dot Above + Acute Accent",
  L"\u0874"   L"Dot Above + Caron",
  L"\u0875"   L"Dot Above + Long Stroke Overlay",
  L"\u0876"   L"Dot Above + Bépo dot below",
  L"\u0877"   L"Dot Above + Long Stroke Overlay + Crochet en chef",
  L"\u0878"   L"Ring Above + Acute Accent",
  L"\u0879"   L"Ogonek + Macron",
  L"\u087a"   L"Horn + Dot Below",
  L"\u087b"   L"Horn + Grave Accent",
  L"\u087c"   L"Horn + Tilde",
  L"\u087d"   L"Horn + Acute Accent",
  L"\u087e"   L"Horn + Bépo dot below",
  L"\u087f"   L"Long Stroke Overlay + Long Solidus Overlay",
  L"\u0880"   L"Long Stroke Overlay + Bépo Latin",
  L"\u0881"   L"Long Stroke Overlay + Dot Above",
  L"\u0882"   L"Long Stroke Overlay + Subscript",
  L"\u0883"   L"Long Stroke Overlay + Dot Above + Crochet en chef",
  L"\u0884"   L"Superscript + Bépo Latin",
  L"\u0885"   L"Subscript + Long Solidus Overlay",
  L"\u0886"   L"Subscript + Bépo Latin",
  L"\u0887"   L"Subscript + Long Stroke Overlay",
  L"\u0888"   L"Bépo dot below + Macron",
  L"\u0889"   L"Bépo dot below + Circumflex Accent",
  L"\u088a"   L"Bépo dot below + Breve",
  L"\u088b"   L"Bépo dot below + Horn",
  L"\u088c"   L"Crochet en chef + Greek",
  L"\u088d"   L"Crochet en chef + Bépo Latin",
  L"\u088e"   L"Crochet en chef + Circumflex Accent",
  L"\u088f"   L"Crochet en chef + Breve",
  L"\u0890"   L"Crochet en chef + Dot Above",
  L"\u0891"   L"Crochet en chef + Horn",
  L"\u0892"   L"Crochet en chef + Long Stroke Overlay",
  L"\u0893"   L"Crochet en chef + Dot Above + Long Stroke Overlay",
  L"\u0894"   L"Crochet en chef + Long Stroke Overlay + Dot Above",
  L"\u0895"   L"Bépo Science + F",
  L"\u0896"   L"Bépo Science + I",
  L"\u0897"   L"Bépo Science + b",
  L"\u0898"   L"Bépo Science + c",
  L"\u0899"   L"Bépo Science + f",
  L"\u089a"   L"Bépo Science + g",
  L"\u089b"   L"Bépo Science + i",
  L"\u089c"   L"Bépo Science + r",
  L"\u089d"   L"Bépo Science + Long Solidus Overlay",
  L"\u089e"   L"Bépo Science + Macron",
  L"\u089f"   L"Bépo Science + I + Greek",
  L"\u08b5"   L"Bépo Science + b + Greek",
  L"\u08be"   L"Bépo Science + g + Greek",
  L"\u08bf"   L"Bépo Science + i + Greek",
  L"\u08c0"   L"Tilde Overlay + Tilde",
  L"\u1d49"   L"Superscript",
  L"\u1d62"   L"Subscript",
  L"\u1e33"   L"Bépo dot below",
  L"\u1ebb"   L"Crochet en chef",
  L"\u2025"   L"Diaeresis Below",
  L"\u2038"   L"Circumflex Accent Below",
  L"\u221e"   L"Bépo Science",
  L"\u2550"   L"Doubled Long Stroke Overlay",
  L"\u301c"   L"Tilde Overlay",

  NULL
};

static ALLOC_SECTION_LDATA DEADKEY aDeadKey[] = {

// Dead key: Cedilla
  DEADTRANS(L','  , L'.'  , 0x0327, NORMAL_CHARACTER),	// U+002E -> U+0327
  DEADTRANS(L','  , L':'  , 0x0327, NORMAL_CHARACTER),	// U+003A -> U+0327
  DEADTRANS(L','  , L'?'  , 0x0327, NORMAL_CHARACTER),	// U+003F -> U+0327
  DEADTRANS(L','  , L'C'  , 0x00c7, NORMAL_CHARACTER),	// ‘C’ -> ‘Ç’
  DEADTRANS(L','  , L'D'  , 0x1e10, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḑ’
  DEADTRANS(L','  , L'E'  , 0x0228, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȩ’
  DEADTRANS(L','  , L'G'  , 0x0122, NORMAL_CHARACTER),	// ‘G’ -> ‘Ģ’
  DEADTRANS(L','  , L'H'  , 0x1e28, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḩ’
  DEADTRANS(L','  , L'K'  , 0x0136, NORMAL_CHARACTER),	// ‘K’ -> ‘Ķ’
  DEADTRANS(L','  , L'L'  , 0x013b, NORMAL_CHARACTER),	// ‘L’ -> ‘Ļ’
  DEADTRANS(L','  , L'N'  , 0x0145, NORMAL_CHARACTER),	// ‘N’ -> ‘Ņ’
  DEADTRANS(L','  , L'R'  , 0x0156, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŗ’
  DEADTRANS(L','  , L'S'  , 0x015e, NORMAL_CHARACTER),	// ‘S’ -> ‘Ş’
  DEADTRANS(L','  , L'T'  , 0x0162, NORMAL_CHARACTER),	// ‘T’ -> ‘Ţ’
  DEADTRANS(L','  , L'_'  , 0x00b8, NORMAL_CHARACTER),	// U+005F -> U+00B8
  DEADTRANS(L','  , L'c'  , 0x00e7, NORMAL_CHARACTER),	// ‘c’ -> ‘ç’
  DEADTRANS(L','  , L'd'  , 0x1e11, NORMAL_CHARACTER),	// ‘d’ -> ‘ḑ’
  DEADTRANS(L','  , L'e'  , 0x0229, NORMAL_CHARACTER),	// ‘e’ -> ‘ȩ’
  DEADTRANS(L','  , L'g'  , 0x0123, NORMAL_CHARACTER),	// ‘g’ -> ‘ģ’
  DEADTRANS(L','  , L'h'  , 0x1e29, NORMAL_CHARACTER),	// ‘h’ -> ‘ḩ’
  DEADTRANS(L','  , L'k'  , 0x0137, NORMAL_CHARACTER),	// ‘k’ -> ‘ķ’
  DEADTRANS(L','  , L'l'  , 0x013c, NORMAL_CHARACTER),	// ‘l’ -> ‘ļ’
  DEADTRANS(L','  , L'n'  , 0x0146, NORMAL_CHARACTER),	// ‘n’ -> ‘ņ’
  DEADTRANS(L','  , L'r'  , 0x0157, NORMAL_CHARACTER),	// ‘r’ -> ‘ŗ’
  DEADTRANS(L','  , L's'  , 0x015f, NORMAL_CHARACTER),	// ‘s’ -> ‘ş’
  DEADTRANS(L','  , L't'  , 0x0163, NORMAL_CHARACTER),	// ‘t’ -> ‘ţ’
  DEADTRANS(L','  , 0x00a0, 0x00b8, NORMAL_CHARACTER),	// U+00A0 -> U+00B8
  DEADTRANS(L','  , 0x00b7, 0x0327, NORMAL_CHARACTER),	// U+00B7 -> U+0327
  DEADTRANS(L','  , 0x00bf, 0x0327, NORMAL_CHARACTER),	// U+00BF -> U+0327
  DEADTRANS(L','  , 0x2019, 0x0327, NORMAL_CHARACTER),	// U+2019 -> U+0327
  DEADTRANS(L','  , 0x2026, 0x0327, NORMAL_CHARACTER),	// U+2026 -> U+0327
  DEADTRANS(L','  , 0x202f, 0x00b8, NORMAL_CHARACTER),	// U+202F -> U+00B8
  DEADTRANS(L','  , 0x00b4, 0x0378, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Cedilla + Acute Accent›
  DEADTRANS(L','  , 0x02d8, 0x0379, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Cedilla + Breve›
  DEADTRANS(L','  , 0x1ebb, 0x0327, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0327
  DEADTRANS(L','  , L' '  , 0x00b8, NORMAL_CHARACTER),	// U+0020 -> U+00B8

// Dead key: Dot Below
  DEADTRANS(L'.'  , L'.'  , 0x0323, NORMAL_CHARACTER),	// U+002E -> U+0323
  DEADTRANS(L'.'  , L':'  , 0x0323, NORMAL_CHARACTER),	// U+003A -> U+0323
  DEADTRANS(L'.'  , L'?'  , 0x0323, NORMAL_CHARACTER),	// U+003F -> U+0323
  DEADTRANS(L'.'  , L'A'  , 0x1ea0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ạ’
  DEADTRANS(L'.'  , L'B'  , 0x1e04, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḅ’
  DEADTRANS(L'.'  , L'D'  , 0x1e0c, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḍ’
  DEADTRANS(L'.'  , L'E'  , 0x1eb8, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẹ’
  DEADTRANS(L'.'  , L'H'  , 0x1e24, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḥ’
  DEADTRANS(L'.'  , L'I'  , 0x1eca, NORMAL_CHARACTER),	// ‘I’ -> ‘Ị’
  DEADTRANS(L'.'  , L'K'  , 0x1e32, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḳ’
  DEADTRANS(L'.'  , L'L'  , 0x1e36, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḷ’
  DEADTRANS(L'.'  , L'M'  , 0x1e42, NORMAL_CHARACTER),	// ‘M’ -> ‘Ṃ’
  DEADTRANS(L'.'  , L'N'  , 0x1e46, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṇ’
  DEADTRANS(L'.'  , L'O'  , 0x1ecc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ọ’
  DEADTRANS(L'.'  , L'R'  , 0x1e5a, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṛ’
  DEADTRANS(L'.'  , L'S'  , 0x1e62, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṣ’
  DEADTRANS(L'.'  , L'T'  , 0x1e6c, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṭ’
  DEADTRANS(L'.'  , L'U'  , 0x1ee4, NORMAL_CHARACTER),	// ‘U’ -> ‘Ụ’
  DEADTRANS(L'.'  , L'V'  , 0x1e7e, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṿ’
  DEADTRANS(L'.'  , L'W'  , 0x1e88, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẉ’
  DEADTRANS(L'.'  , L'Y'  , 0x1ef4, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỵ’
  DEADTRANS(L'.'  , L'Z'  , 0x1e92, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẓ’
  DEADTRANS(L'.'  , L'_'  , L'.'  , NORMAL_CHARACTER),	// U+005F -> U+002E
  DEADTRANS(L'.'  , L'a'  , 0x1ea1, NORMAL_CHARACTER),	// ‘a’ -> ‘ạ’
  DEADTRANS(L'.'  , L'b'  , 0x1e05, NORMAL_CHARACTER),	// ‘b’ -> ‘ḅ’
  DEADTRANS(L'.'  , L'd'  , 0x1e0d, NORMAL_CHARACTER),	// ‘d’ -> ‘ḍ’
  DEADTRANS(L'.'  , L'e'  , 0x1eb9, NORMAL_CHARACTER),	// ‘e’ -> ‘ẹ’
  DEADTRANS(L'.'  , L'h'  , 0x1e25, NORMAL_CHARACTER),	// ‘h’ -> ‘ḥ’
  DEADTRANS(L'.'  , L'i'  , 0x1ecb, NORMAL_CHARACTER),	// ‘i’ -> ‘ị’
  DEADTRANS(L'.'  , L'k'  , 0x1e33, NORMAL_CHARACTER),	// ‘k’ -> ‘ḳ’
  DEADTRANS(L'.'  , L'l'  , 0x1e37, NORMAL_CHARACTER),	// ‘l’ -> ‘ḷ’
  DEADTRANS(L'.'  , L'm'  , 0x1e43, NORMAL_CHARACTER),	// ‘m’ -> ‘ṃ’
  DEADTRANS(L'.'  , L'n'  , 0x1e47, NORMAL_CHARACTER),	// ‘n’ -> ‘ṇ’
  DEADTRANS(L'.'  , L'o'  , 0x1ecd, NORMAL_CHARACTER),	// ‘o’ -> ‘ọ’
  DEADTRANS(L'.'  , L'r'  , 0x1e5b, NORMAL_CHARACTER),	// ‘r’ -> ‘ṛ’
  DEADTRANS(L'.'  , L's'  , 0x1e63, NORMAL_CHARACTER),	// ‘s’ -> ‘ṣ’
  DEADTRANS(L'.'  , L't'  , 0x1e6d, NORMAL_CHARACTER),	// ‘t’ -> ‘ṭ’
  DEADTRANS(L'.'  , L'u'  , 0x1ee5, NORMAL_CHARACTER),	// ‘u’ -> ‘ụ’
  DEADTRANS(L'.'  , L'v'  , 0x1e7f, NORMAL_CHARACTER),	// ‘v’ -> ‘ṿ’
  DEADTRANS(L'.'  , L'w'  , 0x1e89, NORMAL_CHARACTER),	// ‘w’ -> ‘ẉ’
  DEADTRANS(L'.'  , L'y'  , 0x1ef5, NORMAL_CHARACTER),	// ‘y’ -> ‘ỵ’
  DEADTRANS(L'.'  , L'z'  , 0x1e93, NORMAL_CHARACTER),	// ‘z’ -> ‘ẓ’
  DEADTRANS(L'.'  , 0x00a0, L'.'  , NORMAL_CHARACTER),	// U+00A0 -> U+002E
  DEADTRANS(L'.'  , 0x00b7, 0x0323, NORMAL_CHARACTER),	// U+00B7 -> U+0323
  DEADTRANS(L'.'  , 0x00bf, 0x0323, NORMAL_CHARACTER),	// U+00BF -> U+0323
  DEADTRANS(L'.'  , 0x00ca, 0x1ec6, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ệ’
  DEADTRANS(L'.'  , 0x00ea, 0x1ec7, NORMAL_CHARACTER),	// ‘ê’ -> ‘ệ’
  DEADTRANS(L'.'  , 0x2019, 0x0323, NORMAL_CHARACTER),	// U+2019 -> U+0323
  DEADTRANS(L'.'  , 0x2026, 0x0323, NORMAL_CHARACTER),	// U+2026 -> U+0323
  DEADTRANS(L'.'  , 0x202f, L'.'  , NORMAL_CHARACTER),	// U+202F -> U+002E
  DEADTRANS(L'.'  , 0x00af, 0x0380, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Dot Below + Macron›
  DEADTRANS(L'.'  , 0x02c6, 0x0381, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Dot Below + Circumflex Accent›
  DEADTRANS(L'.'  , 0x02d8, 0x0382, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Dot Below + Breve›
  DEADTRANS(L'.'  , 0x02d9, 0x0383, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Dot Below + Dot Above›
  DEADTRANS(L'.'  , 0x031b, 0x038b, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Dot Below + Horn›
  DEADTRANS(L'.'  , 0x1ebb, 0x0323, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0323
  DEADTRANS(L'.'  , L' '  , L'.'  , NORMAL_CHARACTER),	// U+0020 -> U+002E

// Dead key: Long Solidus Overlay
  DEADTRANS(L'/'  , L'.'  , 0x0338, NORMAL_CHARACTER),	// U+002E -> U+0338
  DEADTRANS(L'/'  , L':'  , 0x0338, NORMAL_CHARACTER),	// U+003A -> U+0338
  DEADTRANS(L'/'  , L'<'  , 0x226e, NORMAL_CHARACTER),	// U+003C -> U+226E
  DEADTRANS(L'/'  , L'='  , 0x2260, NORMAL_CHARACTER),	// U+003D -> U+2260
  DEADTRANS(L'/'  , L'>'  , 0x226f, NORMAL_CHARACTER),	// U+003E -> U+226F
  DEADTRANS(L'/'  , L'?'  , 0x0338, NORMAL_CHARACTER),	// U+003F -> U+0338
  DEADTRANS(L'/'  , L'A'  , 0x023a, NORMAL_CHARACTER),	// ‘A’ -> ‘Ⱥ’
  DEADTRANS(L'/'  , L'C'  , 0x023b, NORMAL_CHARACTER),	// ‘C’ -> ‘Ȼ’
  DEADTRANS(L'/'  , L'E'  , 0x0246, NORMAL_CHARACTER),	// ‘E’ -> ‘Ɇ’
  DEADTRANS(L'/'  , L'G'  , 0xa7a0, NORMAL_CHARACTER),	// ‘G’ -> ‘Ꞡ’
  DEADTRANS(L'/'  , L'K'  , 0xa7a2, NORMAL_CHARACTER),	// ‘K’ -> ‘Ꞣ’
  DEADTRANS(L'/'  , L'L'  , 0x0141, NORMAL_CHARACTER),	// ‘L’ -> ‘Ł’
  DEADTRANS(L'/'  , L'N'  , 0xa7a4, NORMAL_CHARACTER),	// ‘N’ -> ‘Ꞥ’
  DEADTRANS(L'/'  , L'O'  , 0x00d8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ø’
  DEADTRANS(L'/'  , L'Q'  , 0xa758, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ꝙ’
  DEADTRANS(L'/'  , L'R'  , 0xa7a6, NORMAL_CHARACTER),	// ‘R’ -> ‘Ꞧ’
  DEADTRANS(L'/'  , L'S'  , 0xa7a8, NORMAL_CHARACTER),	// ‘S’ -> ‘Ꞩ’
  DEADTRANS(L'/'  , L'T'  , 0x023e, NORMAL_CHARACTER),	// ‘T’ -> ‘Ⱦ’
  DEADTRANS(L'/'  , L'V'  , 0xa75e, NORMAL_CHARACTER),	// ‘V’ -> ‘Ꝟ’
  DEADTRANS(L'/'  , L'_'  , L'/'  , NORMAL_CHARACTER),	// U+005F -> U+002F
  DEADTRANS(L'/'  , L'a'  , 0x2c65, NORMAL_CHARACTER),	// ‘a’ -> ‘ⱥ’
  DEADTRANS(L'/'  , L'b'  , 0x2422, NORMAL_CHARACTER),	// ‘b’ -> U+2422
  DEADTRANS(L'/'  , L'c'  , 0x023c, NORMAL_CHARACTER),	// ‘c’ -> ‘ȼ’
  DEADTRANS(L'/'  , L'e'  , 0x0247, NORMAL_CHARACTER),	// ‘e’ -> ‘ɇ’
  DEADTRANS(L'/'  , L'g'  , 0xa7a1, NORMAL_CHARACTER),	// ‘g’ -> ‘ꞡ’
  DEADTRANS(L'/'  , L'k'  , 0xa7a3, NORMAL_CHARACTER),	// ‘k’ -> ‘ꞣ’
  DEADTRANS(L'/'  , L'l'  , 0x0142, NORMAL_CHARACTER),	// ‘l’ -> ‘ł’
  DEADTRANS(L'/'  , L'n'  , 0xa7a5, NORMAL_CHARACTER),	// ‘n’ -> ‘ꞥ’
  DEADTRANS(L'/'  , L'o'  , 0x00f8, NORMAL_CHARACTER),	// ‘o’ -> ‘ø’
  DEADTRANS(L'/'  , L'q'  , 0xa759, NORMAL_CHARACTER),	// ‘q’ -> ‘ꝙ’
  DEADTRANS(L'/'  , L'r'  , 0xa7a7, NORMAL_CHARACTER),	// ‘r’ -> ‘ꞧ’
  DEADTRANS(L'/'  , L's'  , 0xa7a9, NORMAL_CHARACTER),	// ‘s’ -> ‘ꞩ’
  DEADTRANS(L'/'  , L't'  , 0x2c66, NORMAL_CHARACTER),	// ‘t’ -> ‘ⱦ’
  DEADTRANS(L'/'  , L'v'  , 0xa75f, NORMAL_CHARACTER),	// ‘v’ -> ‘ꝟ’
  DEADTRANS(L'/'  , L'~'  , 0xa743, NORMAL_CHARACTER),	// U+007E -> ‘ꝃ’
  DEADTRANS(L'/'  , 0x00a0, L'/'  , NORMAL_CHARACTER),	// U+00A0 -> U+002F
  DEADTRANS(L'/'  , 0x00b7, 0x0338, NORMAL_CHARACTER),	// U+00B7 -> U+0338
  DEADTRANS(L'/'  , 0x00bf, 0x0338, NORMAL_CHARACTER),	// U+00BF -> U+0338
  DEADTRANS(L'/'  , 0x017f, 0x1e9c, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẜ’
  DEADTRANS(L'/'  , 0x2011, 0xa742, NORMAL_CHARACTER),	// U+2011 -> ‘Ꝃ’
  DEADTRANS(L'/'  , 0x2019, 0x0338, NORMAL_CHARACTER),	// U+2019 -> U+0338
  DEADTRANS(L'/'  , 0x2026, 0x0338, NORMAL_CHARACTER),	// U+2026 -> U+0338
  DEADTRANS(L'/'  , 0x202f, L'/'  , NORMAL_CHARACTER),	// U+202F -> U+002F
  DEADTRANS(L'/'  , 0x00b4, 0x038d, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Long Solidus Overlay + Acute Accent›
  DEADTRANS(L'/'  , 0x00b5, 0x03a2, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Long Solidus Overlay + Greek›
  DEADTRANS(L'/'  , 0x00df, 0x0530, CHAINED_DEAD_KEY),	// ‹Bépo Latin› -> ‹Long Solidus Overlay + Bépo Latin›
  DEADTRANS(L'/'  , 0x02d9, 0x0557, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Long Solidus Overlay + Dot Above›
  DEADTRANS(L'/'  , 0x0336, 0x0558, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Long Solidus Overlay + Long Stroke Overlay›
  DEADTRANS(L'/'  , 0x1d62, 0x058b, CHAINED_DEAD_KEY),	// ‹Subscript› -> ‹Long Solidus Overlay + Subscript›
  DEADTRANS(L'/'  , 0x1ebb, 0x0338, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0338
  DEADTRANS(L'/'  , 0x221e, 0x058c, CHAINED_DEAD_KEY),	// ‹Bépo Science› -> ‹Long Solidus Overlay + Bépo Science›
  DEADTRANS(L'/'  , L' '  , L'/'  , NORMAL_CHARACTER),	// U+0020 -> U+002F

// Dead key: Diaeresis
  DEADTRANS(L':'  , L'.'  , 0x0308, NORMAL_CHARACTER),	// U+002E -> U+0308
  DEADTRANS(L':'  , L':'  , 0x0308, NORMAL_CHARACTER),	// U+003A -> U+0308
  DEADTRANS(L':'  , L'?'  , 0x0308, NORMAL_CHARACTER),	// U+003F -> U+0308
  DEADTRANS(L':'  , L'A'  , 0x00c4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ä’
  DEADTRANS(L':'  , L'E'  , 0x00cb, NORMAL_CHARACTER),	// ‘E’ -> ‘Ë’
  DEADTRANS(L':'  , L'H'  , 0x1e26, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḧ’
  DEADTRANS(L':'  , L'I'  , 0x00cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ï’
  DEADTRANS(L':'  , L'O'  , 0x00d6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ö’
  DEADTRANS(L':'  , L'U'  , 0x00dc, NORMAL_CHARACTER),	// ‘U’ -> ‘Ü’
  DEADTRANS(L':'  , L'W'  , 0x1e84, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẅ’
  DEADTRANS(L':'  , L'X'  , 0x1e8c, NORMAL_CHARACTER),	// ‘X’ -> ‘Ẍ’
  DEADTRANS(L':'  , L'Y'  , 0x0178, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ÿ’
  DEADTRANS(L':'  , L'_'  , 0x00a8, NORMAL_CHARACTER),	// U+005F -> U+00A8
  DEADTRANS(L':'  , L'a'  , 0x00e4, NORMAL_CHARACTER),	// ‘a’ -> ‘ä’
  DEADTRANS(L':'  , L'e'  , 0x00eb, NORMAL_CHARACTER),	// ‘e’ -> ‘ë’
  DEADTRANS(L':'  , L'h'  , 0x1e27, NORMAL_CHARACTER),	// ‘h’ -> ‘ḧ’
  DEADTRANS(L':'  , L'i'  , 0x00ef, NORMAL_CHARACTER),	// ‘i’ -> ‘ï’
  DEADTRANS(L':'  , L'o'  , 0x00f6, NORMAL_CHARACTER),	// ‘o’ -> ‘ö’
  DEADTRANS(L':'  , L't'  , 0x1e97, NORMAL_CHARACTER),	// ‘t’ -> ‘ẗ’
  DEADTRANS(L':'  , L'u'  , 0x00fc, NORMAL_CHARACTER),	// ‘u’ -> ‘ü’
  DEADTRANS(L':'  , L'w'  , 0x1e85, NORMAL_CHARACTER),	// ‘w’ -> ‘ẅ’
  DEADTRANS(L':'  , L'x'  , 0x1e8d, NORMAL_CHARACTER),	// ‘x’ -> ‘ẍ’
  DEADTRANS(L':'  , L'y'  , 0x00ff, NORMAL_CHARACTER),	// ‘y’ -> ‘ÿ’
  DEADTRANS(L':'  , 0x00a0, 0x00a8, NORMAL_CHARACTER),	// U+00A0 -> U+00A8
  DEADTRANS(L':'  , 0x00b7, 0x0308, NORMAL_CHARACTER),	// U+00B7 -> U+0308
  DEADTRANS(L':'  , 0x00bf, 0x0308, NORMAL_CHARACTER),	// U+00BF -> U+0308
  DEADTRANS(L':'  , 0x00d9, 0x01db, NORMAL_CHARACTER),	// ‘Ù’ -> ‘Ǜ’
  DEADTRANS(L':'  , 0x00f9, 0x01dc, NORMAL_CHARACTER),	// ‘ù’ -> ‘ǜ’
  DEADTRANS(L':'  , 0x2019, 0x0308, NORMAL_CHARACTER),	// U+2019 -> U+0308
  DEADTRANS(L':'  , 0x2026, 0x0308, NORMAL_CHARACTER),	// U+2026 -> U+0308
  DEADTRANS(L':'  , 0x202f, 0x00a8, NORMAL_CHARACTER),	// U+202F -> U+00A8
  DEADTRANS(L':'  , L':'  , 0x2025, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Diaeresis Below›
  DEADTRANS(L':'  , L'`'  , 0x0590, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Diaeresis + Grave Accent›
  DEADTRANS(L':'  , L'~'  , 0x05c8, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Diaeresis + Tilde›
  DEADTRANS(L':'  , 0x00af, 0x05c9, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Diaeresis + Macron›
  DEADTRANS(L':'  , 0x00b4, 0x05ca, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Diaeresis + Acute Accent›
  DEADTRANS(L':'  , 0x02c7, 0x05cb, CHAINED_DEAD_KEY),	// ‹Caron› -> ‹Diaeresis + Caron›
  DEADTRANS(L':'  , 0x1ebb, 0x0308, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0308
  DEADTRANS(L':'  , L' '  , 0x00a8, NORMAL_CHARACTER),	// U+0020 -> U+00A8

// Dead key: Comma Below
  DEADTRANS(L';'  , L'.'  , 0x0326, NORMAL_CHARACTER),	// U+002E -> U+0326
  DEADTRANS(L';'  , L':'  , 0x0326, NORMAL_CHARACTER),	// U+003A -> U+0326
  DEADTRANS(L';'  , L'?'  , 0x0326, NORMAL_CHARACTER),	// U+003F -> U+0326
  DEADTRANS(L';'  , L'S'  , 0x0218, NORMAL_CHARACTER),	// ‘S’ -> ‘Ș’
  DEADTRANS(L';'  , L'T'  , 0x021a, NORMAL_CHARACTER),	// ‘T’ -> ‘Ț’
  DEADTRANS(L';'  , L'_'  , L','  , NORMAL_CHARACTER),	// U+005F -> U+002C
  DEADTRANS(L';'  , L's'  , 0x0219, NORMAL_CHARACTER),	// ‘s’ -> ‘ș’
  DEADTRANS(L';'  , L't'  , 0x021b, NORMAL_CHARACTER),	// ‘t’ -> ‘ț’
  DEADTRANS(L';'  , 0x00a0, L','  , NORMAL_CHARACTER),	// U+00A0 -> U+002C
  DEADTRANS(L';'  , 0x00b7, 0x0326, NORMAL_CHARACTER),	// U+00B7 -> U+0326
  DEADTRANS(L';'  , 0x00bf, 0x0326, NORMAL_CHARACTER),	// U+00BF -> U+0326
  DEADTRANS(L';'  , 0x2019, 0x0326, NORMAL_CHARACTER),	// U+2019 -> U+0326
  DEADTRANS(L';'  , 0x2026, 0x0326, NORMAL_CHARACTER),	// U+2026 -> U+0326
  DEADTRANS(L';'  , 0x202f, L','  , NORMAL_CHARACTER),	// U+202F -> U+002C
  DEADTRANS(L';'  , 0x1ebb, 0x0326, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0326
  DEADTRANS(L';'  , L' '  , L','  , NORMAL_CHARACTER),	// U+0020 -> U+002C

// Dead key: Grave Accent
  DEADTRANS(L'`'  , L'.'  , 0x0300, NORMAL_CHARACTER),	// U+002E -> U+0300
  DEADTRANS(L'`'  , L':'  , 0x0300, NORMAL_CHARACTER),	// U+003A -> U+0300
  DEADTRANS(L'`'  , L'?'  , 0x0300, NORMAL_CHARACTER),	// U+003F -> U+0300
  DEADTRANS(L'`'  , L'A'  , 0x00c0, NORMAL_CHARACTER),	// ‘A’ -> ‘À’
  DEADTRANS(L'`'  , L'E'  , 0x00c8, NORMAL_CHARACTER),	// ‘E’ -> ‘È’
  DEADTRANS(L'`'  , L'I'  , 0x00cc, NORMAL_CHARACTER),	// ‘I’ -> ‘Ì’
  DEADTRANS(L'`'  , L'N'  , 0x01f8, NORMAL_CHARACTER),	// ‘N’ -> ‘Ǹ’
  DEADTRANS(L'`'  , L'O'  , 0x00d2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ò’
  DEADTRANS(L'`'  , L'U'  , 0x00d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ù’
  DEADTRANS(L'`'  , L'V'  , 0x01db, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǜ’
  DEADTRANS(L'`'  , L'W'  , 0x1e80, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẁ’
  DEADTRANS(L'`'  , L'Y'  , 0x1ef2, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỳ’
  DEADTRANS(L'`'  , L'_'  , L'`'  , NORMAL_CHARACTER),	// U+005F -> U+0060
  DEADTRANS(L'`'  , L'a'  , 0x00e0, NORMAL_CHARACTER),	// ‘a’ -> ‘à’
  DEADTRANS(L'`'  , L'e'  , 0x00e8, NORMAL_CHARACTER),	// ‘e’ -> ‘è’
  DEADTRANS(L'`'  , L'i'  , 0x00ec, NORMAL_CHARACTER),	// ‘i’ -> ‘ì’
  DEADTRANS(L'`'  , L'n'  , 0x01f9, NORMAL_CHARACTER),	// ‘n’ -> ‘ǹ’
  DEADTRANS(L'`'  , L'o'  , 0x00f2, NORMAL_CHARACTER),	// ‘o’ -> ‘ò’
  DEADTRANS(L'`'  , L'u'  , 0x00f9, NORMAL_CHARACTER),	// ‘u’ -> ‘ù’
  DEADTRANS(L'`'  , L'v'  , 0x01dc, NORMAL_CHARACTER),	// ‘v’ -> ‘ǜ’
  DEADTRANS(L'`'  , L'w'  , 0x1e81, NORMAL_CHARACTER),	// ‘w’ -> ‘ẁ’
  DEADTRANS(L'`'  , L'y'  , 0x1ef3, NORMAL_CHARACTER),	// ‘y’ -> ‘ỳ’
  DEADTRANS(L'`'  , 0x00a0, L'`'  , NORMAL_CHARACTER),	// U+00A0 -> U+0060
  DEADTRANS(L'`'  , 0x00b7, 0x0300, NORMAL_CHARACTER),	// U+00B7 -> U+0300
  DEADTRANS(L'`'  , 0x00bf, 0x0300, NORMAL_CHARACTER),	// U+00BF -> U+0300
  DEADTRANS(L'`'  , 0x00ca, 0x1ec0, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ề’
  DEADTRANS(L'`'  , 0x00ea, 0x1ec1, NORMAL_CHARACTER),	// ‘ê’ -> ‘ề’
  DEADTRANS(L'`'  , 0x2019, 0x0300, NORMAL_CHARACTER),	// U+2019 -> U+0300
  DEADTRANS(L'`'  , 0x2026, 0x0300, NORMAL_CHARACTER),	// U+2026 -> U+0300
  DEADTRANS(L'`'  , 0x202f, L'`'  , NORMAL_CHARACTER),	// U+202F -> U+0060
  DEADTRANS(L'`'  , L':'  , 0x05cc, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Grave Accent + Diaeresis›
  DEADTRANS(L'`'  , L'`'  , 0x030f, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Double Grave Accent›
  DEADTRANS(L'`'  , 0x00af, 0x05cd, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Grave Accent + Macron›
  DEADTRANS(L'`'  , 0x02c6, 0x05ce, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Grave Accent + Circumflex Accent›
  DEADTRANS(L'`'  , 0x02d8, 0x05cf, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Grave Accent + Breve›
  DEADTRANS(L'`'  , 0x031b, 0x05eb, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Grave Accent + Horn›
  DEADTRANS(L'`'  , 0x1ebb, 0x0300, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0300
  DEADTRANS(L'`'  , L' '  , L'`'  , NORMAL_CHARACTER),	// U+0020 -> U+0060

// Dead key: Tilde
  DEADTRANS(L'~'  , L'-'  , 0x2243, NORMAL_CHARACTER),	// U+002D -> U+2243
  DEADTRANS(L'~'  , L'.'  , 0x0303, NORMAL_CHARACTER),	// U+002E -> U+0303
  DEADTRANS(L'~'  , L':'  , 0x0303, NORMAL_CHARACTER),	// U+003A -> U+0303
  DEADTRANS(L'~'  , L'<'  , 0x2272, NORMAL_CHARACTER),	// U+003C -> U+2272
  DEADTRANS(L'~'  , L'>'  , 0x2273, NORMAL_CHARACTER),	// U+003E -> U+2273
  DEADTRANS(L'~'  , L'?'  , 0x0303, NORMAL_CHARACTER),	// U+003F -> U+0303
  DEADTRANS(L'~'  , L'A'  , 0x00c3, NORMAL_CHARACTER),	// ‘A’ -> ‘Ã’
  DEADTRANS(L'~'  , L'E'  , 0x1ebc, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẽ’
  DEADTRANS(L'~'  , L'I'  , 0x0128, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĩ’
  DEADTRANS(L'~'  , L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(L'~'  , L'O'  , 0x00d5, NORMAL_CHARACTER),	// ‘O’ -> ‘Õ’
  DEADTRANS(L'~'  , L'U'  , 0x0168, NORMAL_CHARACTER),	// ‘U’ -> ‘Ũ’
  DEADTRANS(L'~'  , L'V'  , 0x1e7c, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṽ’
  DEADTRANS(L'~'  , L'Y'  , 0x1ef8, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỹ’
  DEADTRANS(L'~'  , L'_'  , L'~'  , NORMAL_CHARACTER),	// U+005F -> U+007E
  DEADTRANS(L'~'  , L'a'  , 0x00e3, NORMAL_CHARACTER),	// ‘a’ -> ‘ã’
  DEADTRANS(L'~'  , L'e'  , 0x1ebd, NORMAL_CHARACTER),	// ‘e’ -> ‘ẽ’
  DEADTRANS(L'~'  , L'i'  , 0x0129, NORMAL_CHARACTER),	// ‘i’ -> ‘ĩ’
  DEADTRANS(L'~'  , L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(L'~'  , L'o'  , 0x00f5, NORMAL_CHARACTER),	// ‘o’ -> ‘õ’
  DEADTRANS(L'~'  , L'u'  , 0x0169, NORMAL_CHARACTER),	// ‘u’ -> ‘ũ’
  DEADTRANS(L'~'  , L'v'  , 0x1e7d, NORMAL_CHARACTER),	// ‘v’ -> ‘ṽ’
  DEADTRANS(L'~'  , L'y'  , 0x1ef9, NORMAL_CHARACTER),	// ‘y’ -> ‘ỹ’
  DEADTRANS(L'~'  , 0x00a0, L'~'  , NORMAL_CHARACTER),	// U+00A0 -> U+007E
  DEADTRANS(L'~'  , 0x00b7, 0x0303, NORMAL_CHARACTER),	// U+00B7 -> U+0303
  DEADTRANS(L'~'  , 0x00bf, 0x0303, NORMAL_CHARACTER),	// U+00BF -> U+0303
  DEADTRANS(L'~'  , 0x00ca, 0x1ec4, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ễ’
  DEADTRANS(L'~'  , 0x00ea, 0x1ec5, NORMAL_CHARACTER),	// ‘ê’ -> ‘ễ’
  DEADTRANS(L'~'  , 0x2019, 0x0303, NORMAL_CHARACTER),	// U+2019 -> U+0303
  DEADTRANS(L'~'  , 0x2026, 0x0303, NORMAL_CHARACTER),	// U+2026 -> U+0303
  DEADTRANS(L'~'  , 0x202f, L'~'  , NORMAL_CHARACTER),	// U+202F -> U+007E
  DEADTRANS(L'~'  , L':'  , 0x05ec, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Tilde + Diaeresis›
  DEADTRANS(L'~'  , L'~'  , 0x301c, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Tilde Overlay›
  DEADTRANS(L'~'  , 0x00af, 0x05ed, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Tilde + Macron›
  DEADTRANS(L'~'  , 0x00b4, 0x05ee, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Tilde + Acute Accent›
  DEADTRANS(L'~'  , 0x02c6, 0x05f5, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Tilde + Circumflex Accent›
  DEADTRANS(L'~'  , 0x02d8, 0x05f6, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Tilde + Breve›
  DEADTRANS(L'~'  , 0x031b, 0x05f7, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Tilde + Horn›
  DEADTRANS(L'~'  , 0x1ebb, 0x0303, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0303
  DEADTRANS(L'~'  , L' '  , L'~'  , NORMAL_CHARACTER),	// U+0020 -> U+007E

// Dead key: Bépo Currency
  DEADTRANS(0x00a4, L'&'  , 0x20b0, NORMAL_CHARACTER),	// U+0026 -> U+20B0
  DEADTRANS(0x00a4, L'A'  , 0x20b3, NORMAL_CHARACTER),	// ‘A’ -> U+20B3
  DEADTRANS(0x00a4, L'B'  , 0x0e3f, NORMAL_CHARACTER),	// ‘B’ -> U+0E3F
  DEADTRANS(0x00a4, L'C'  , 0x20a1, NORMAL_CHARACTER),	// ‘C’ -> U+20A1
  DEADTRANS(0x00a4, L'D'  , 0x20af, NORMAL_CHARACTER),	// ‘D’ -> U+20AF
  DEADTRANS(0x00a4, L'F'  , 0x20a3, NORMAL_CHARACTER),	// ‘F’ -> U+20A3
  DEADTRANS(0x00a4, L'I'  , 0x0294, NORMAL_CHARACTER),	// ‘I’ -> ‘ʔ’
  DEADTRANS(0x00a4, L'L'  , 0x20a4, NORMAL_CHARACTER),	// ‘L’ -> U+20A4
  DEADTRANS(0x00a4, L'M'  , 0x2133, NORMAL_CHARACTER),	// ‘M’ -> ‘ℳ’
  DEADTRANS(0x00a4, L'O'  , 0x0af1, NORMAL_CHARACTER),	// ‘O’ -> U+0AF1
  DEADTRANS(0x00a4, L'P'  , 0x20a7, NORMAL_CHARACTER),	// ‘P’ -> U+20A7
  DEADTRANS(0x00a4, L'R'  , 0x20b9, NORMAL_CHARACTER),	// ‘R’ -> U+20B9
  DEADTRANS(0x00a4, L'S'  , 0x20b7, NORMAL_CHARACTER),	// ‘S’ -> U+20B7
  DEADTRANS(0x00a4, L'T'  , 0x20ae, NORMAL_CHARACTER),	// ‘T’ -> U+20AE
  DEADTRANS(0x00a4, L'U'  , 0x5713, NORMAL_CHARACTER),	// ‘U’ -> ‘圓’
  DEADTRANS(0x00a4, L'Y'  , 0x5186, NORMAL_CHARACTER),	// ‘Y’ -> ‘円’
  DEADTRANS(0x00a4, L'a'  , 0x060b, NORMAL_CHARACTER),	// ‘a’ -> U+060B
  DEADTRANS(0x00a4, L'b'  , 0x20bf, NORMAL_CHARACTER),	// ‘b’ -> U+20BF
  DEADTRANS(0x00a4, L'c'  , 0x00a2, NORMAL_CHARACTER),	// ‘c’ -> U+00A2
  DEADTRANS(0x00a4, L'd'  , 0x20ab, NORMAL_CHARACTER),	// ‘d’ -> U+20AB
  DEADTRANS(0x00a4, L'e'  , 0x20a0, NORMAL_CHARACTER),	// ‘e’ -> U+20A0
  DEADTRANS(0x00a4, L'f'  , 0x0192, NORMAL_CHARACTER),	// ‘f’ -> ‘ƒ’
  DEADTRANS(0x00a4, L'g'  , 0x20b2, NORMAL_CHARACTER),	// ‘g’ -> U+20B2
  DEADTRANS(0x00a4, L'h'  , 0x20b4, NORMAL_CHARACTER),	// ‘h’ -> U+20B4
  DEADTRANS(0x00a4, L'i'  , 0xfdfc, NORMAL_CHARACTER),	// ‘i’ -> U+FDFC
  DEADTRANS(0x00a4, L'k'  , 0x20ad, NORMAL_CHARACTER),	// ‘k’ -> U+20AD
  DEADTRANS(0x00a4, L'l'  , 0x20ba, NORMAL_CHARACTER),	// ‘l’ -> U+20BA
  DEADTRANS(0x00a4, L'm'  , 0x20a5, NORMAL_CHARACTER),	// ‘m’ -> U+20A5
  DEADTRANS(0x00a4, L'n'  , 0x20a6, NORMAL_CHARACTER),	// ‘n’ -> U+20A6
  DEADTRANS(0x00a4, L'o'  , 0x0bf9, NORMAL_CHARACTER),	// ‘o’ -> U+0BF9
  DEADTRANS(0x00a4, L'p'  , 0x20b1, NORMAL_CHARACTER),	// ‘p’ -> U+20B1
  DEADTRANS(0x00a4, L'r'  , 0x20bd, NORMAL_CHARACTER),	// ‘r’ -> U+20BD
  DEADTRANS(0x00a4, L's'  , 0x20aa, NORMAL_CHARACTER),	// ‘s’ -> U+20AA
  DEADTRANS(0x00a4, L't'  , 0x20b8, NORMAL_CHARACTER),	// ‘t’ -> U+20B8
  DEADTRANS(0x00a4, L'u'  , 0x5143, NORMAL_CHARACTER),	// ‘u’ -> ‘元’
  DEADTRANS(0x00a4, L'w'  , 0x20a9, NORMAL_CHARACTER),	// ‘w’ -> U+20A9
  DEADTRANS(0x00a4, L'y'  , 0x00a5, NORMAL_CHARACTER),	// ‘y’ -> U+00A5
  DEADTRANS(0x00a4, 0x00a3, 0x20b6, NORMAL_CHARACTER),	// U+00A3 -> U+20B6
  DEADTRANS(0x00a4, 0x00e7, 0x20b5, NORMAL_CHARACTER),	// ‘ç’ -> U+20B5
  DEADTRANS(0x00a4, 0x2122, 0x09f2, NORMAL_CHARACTER),	// U+2122 -> U+09F2
  DEADTRANS(0x00a4, 0x26fd, 0x20bb, NORMAL_CHARACTER),	// U+26FD -> U+20BB
  DEADTRANS(0x00a4, L','  , 0x20a2, NORMAL_CHARACTER),	// ‹Cedilla› -> U+20A2
  DEADTRANS(0x00a4, L'/'  , 0x20be, NORMAL_CHARACTER),	// ‹Long Solidus Overlay› -> U+20BE
  DEADTRANS(0x00a4, 0x00a4, 0x00a4, NORMAL_CHARACTER),	// ‹Bépo Currency› -> U+00A4
  DEADTRANS(0x00a4, 0x00af, 0x20bc, NORMAL_CHARACTER),	// ‹Macron› -> U+20BC
  DEADTRANS(0x00a4, 0x02d8, 0x20a8, NORMAL_CHARACTER),	// ‹Breve› -> U+20A8
  DEADTRANS(0x00a4, 0x1d49, 0x09f3, NORMAL_CHARACTER),	// ‹Superscript› -> U+09F3
  DEADTRANS(0x00a4, L' '  , 0x00a4, NORMAL_CHARACTER),	// U+0020 -> U+00A4

// Dead key: Macron
  DEADTRANS(0x00af, L'.'  , 0x0304, NORMAL_CHARACTER),	// U+002E -> U+0304
  DEADTRANS(0x00af, L':'  , 0x0304, NORMAL_CHARACTER),	// U+003A -> U+0304
  DEADTRANS(0x00af, L'?'  , 0x0304, NORMAL_CHARACTER),	// U+003F -> U+0304
  DEADTRANS(0x00af, L'A'  , 0x0100, NORMAL_CHARACTER),	// ‘A’ -> ‘Ā’
  DEADTRANS(0x00af, L'E'  , 0x0112, NORMAL_CHARACTER),	// ‘E’ -> ‘Ē’
  DEADTRANS(0x00af, L'G'  , 0x1e20, NORMAL_CHARACTER),	// ‘G’ -> ‘Ḡ’
  DEADTRANS(0x00af, L'I'  , 0x012a, NORMAL_CHARACTER),	// ‘I’ -> ‘Ī’
  DEADTRANS(0x00af, L'O'  , 0x014c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ō’
  DEADTRANS(0x00af, L'U'  , 0x016a, NORMAL_CHARACTER),	// ‘U’ -> ‘Ū’
  DEADTRANS(0x00af, L'V'  , 0x01d5, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǖ’
  DEADTRANS(0x00af, L'Y'  , 0x0232, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ȳ’
  DEADTRANS(0x00af, L'_'  , 0x00af, NORMAL_CHARACTER),	// U+005F -> U+00AF
  DEADTRANS(0x00af, L'a'  , 0x0101, NORMAL_CHARACTER),	// ‘a’ -> ‘ā’
  DEADTRANS(0x00af, L'e'  , 0x0113, NORMAL_CHARACTER),	// ‘e’ -> ‘ē’
  DEADTRANS(0x00af, L'g'  , 0x1e21, NORMAL_CHARACTER),	// ‘g’ -> ‘ḡ’
  DEADTRANS(0x00af, L'i'  , 0x012b, NORMAL_CHARACTER),	// ‘i’ -> ‘ī’
  DEADTRANS(0x00af, L'o'  , 0x014d, NORMAL_CHARACTER),	// ‘o’ -> ‘ō’
  DEADTRANS(0x00af, L'u'  , 0x016b, NORMAL_CHARACTER),	// ‘u’ -> ‘ū’
  DEADTRANS(0x00af, L'v'  , 0x01d6, NORMAL_CHARACTER),	// ‘v’ -> ‘ǖ’
  DEADTRANS(0x00af, L'y'  , 0x0233, NORMAL_CHARACTER),	// ‘y’ -> ‘ȳ’
  DEADTRANS(0x00af, 0x00a0, 0x00af, NORMAL_CHARACTER),	// U+00A0 -> U+00AF
  DEADTRANS(0x00af, 0x00b7, 0x0304, NORMAL_CHARACTER),	// U+00B7 -> U+0304
  DEADTRANS(0x00af, 0x00bf, 0x0304, NORMAL_CHARACTER),	// U+00BF -> U+0304
  DEADTRANS(0x00af, 0x00c6, 0x01e2, NORMAL_CHARACTER),	// ‘Æ’ -> ‘Ǣ’
  DEADTRANS(0x00af, 0x00c8, 0x1e14, NORMAL_CHARACTER),	// ‘È’ -> ‘Ḕ’
  DEADTRANS(0x00af, 0x00c9, 0x1e16, NORMAL_CHARACTER),	// ‘É’ -> ‘Ḗ’
  DEADTRANS(0x00af, 0x00e6, 0x01e3, NORMAL_CHARACTER),	// ‘æ’ -> ‘ǣ’
  DEADTRANS(0x00af, 0x00e8, 0x1e15, NORMAL_CHARACTER),	// ‘è’ -> ‘ḕ’
  DEADTRANS(0x00af, 0x00e9, 0x1e17, NORMAL_CHARACTER),	// ‘é’ -> ‘ḗ’
  DEADTRANS(0x00af, 0x2019, 0x0304, NORMAL_CHARACTER),	// U+2019 -> U+0304
  DEADTRANS(0x00af, 0x2026, 0x0304, NORMAL_CHARACTER),	// U+2026 -> U+0304
  DEADTRANS(0x00af, 0x202f, 0x00af, NORMAL_CHARACTER),	// U+202F -> U+00AF
  DEADTRANS(0x00af, L'.'  , 0x05f8, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Macron + Dot Below›
  DEADTRANS(0x00af, L':'  , 0x05f9, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Macron + Diaeresis›
  DEADTRANS(0x00af, L'`'  , 0x05fa, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Macron + Grave Accent›
  DEADTRANS(0x00af, L'~'  , 0x05fb, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Macron + Tilde›
  DEADTRANS(0x00af, 0x00af, 0x02cd, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Macron Below›
  DEADTRANS(0x00af, 0x00b4, 0x05fc, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Macron + Acute Accent›
  DEADTRANS(0x00af, 0x00b5, 0x05fd, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Macron + Greek›
  DEADTRANS(0x00af, 0x02d9, 0x05fe, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Macron + Dot Above›
  DEADTRANS(0x00af, 0x02db, 0x05ff, CHAINED_DEAD_KEY),	// ‹Ogonek› -> ‹Macron + Ogonek›
  DEADTRANS(0x00af, 0x1e33, 0x061d, CHAINED_DEAD_KEY),	// ‹Bépo dot below› -> ‹Macron + Bépo dot below›
  DEADTRANS(0x00af, 0x1ebb, 0x0304, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0304
  DEADTRANS(0x00af, 0x221e, 0x070e, CHAINED_DEAD_KEY),	// ‹Bépo Science› -> ‹Macron + Bépo Science›
  DEADTRANS(0x00af, L' '  , 0x00af, NORMAL_CHARACTER),	// U+0020 -> U+00AF

// Dead key: Acute Accent
  DEADTRANS(0x00b4, L'.'  , 0x0301, NORMAL_CHARACTER),	// U+002E -> U+0301
  DEADTRANS(0x00b4, L':'  , 0x0301, NORMAL_CHARACTER),	// U+003A -> U+0301
  DEADTRANS(0x00b4, L'?'  , 0x0301, NORMAL_CHARACTER),	// U+003F -> U+0301
  DEADTRANS(0x00b4, L'A'  , 0x00c1, NORMAL_CHARACTER),	// ‘A’ -> ‘Á’
  DEADTRANS(0x00b4, L'C'  , 0x0106, NORMAL_CHARACTER),	// ‘C’ -> ‘Ć’
  DEADTRANS(0x00b4, L'E'  , 0x00c9, NORMAL_CHARACTER),	// ‘E’ -> ‘É’
  DEADTRANS(0x00b4, L'G'  , 0x01f4, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǵ’
  DEADTRANS(0x00b4, L'I'  , 0x00cd, NORMAL_CHARACTER),	// ‘I’ -> ‘Í’
  DEADTRANS(0x00b4, L'K'  , 0x1e30, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḱ’
  DEADTRANS(0x00b4, L'L'  , 0x0139, NORMAL_CHARACTER),	// ‘L’ -> ‘Ĺ’
  DEADTRANS(0x00b4, L'M'  , 0x1e3e, NORMAL_CHARACTER),	// ‘M’ -> ‘Ḿ’
  DEADTRANS(0x00b4, L'N'  , 0x0143, NORMAL_CHARACTER),	// ‘N’ -> ‘Ń’
  DEADTRANS(0x00b4, L'O'  , 0x00d3, NORMAL_CHARACTER),	// ‘O’ -> ‘Ó’
  DEADTRANS(0x00b4, L'P'  , 0x1e54, NORMAL_CHARACTER),	// ‘P’ -> ‘Ṕ’
  DEADTRANS(0x00b4, L'R'  , 0x0154, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŕ’
  DEADTRANS(0x00b4, L'S'  , 0x015a, NORMAL_CHARACTER),	// ‘S’ -> ‘Ś’
  DEADTRANS(0x00b4, L'U'  , 0x00da, NORMAL_CHARACTER),	// ‘U’ -> ‘Ú’
  DEADTRANS(0x00b4, L'V'  , 0x01d7, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǘ’
  DEADTRANS(0x00b4, L'W'  , 0x1e82, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẃ’
  DEADTRANS(0x00b4, L'Y'  , 0x00dd, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ý’
  DEADTRANS(0x00b4, L'Z'  , 0x0179, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ź’
  DEADTRANS(0x00b4, L'_'  , 0x00b4, NORMAL_CHARACTER),	// U+005F -> U+00B4
  DEADTRANS(0x00b4, L'a'  , 0x00e1, NORMAL_CHARACTER),	// ‘a’ -> ‘á’
  DEADTRANS(0x00b4, L'c'  , 0x0107, NORMAL_CHARACTER),	// ‘c’ -> ‘ć’
  DEADTRANS(0x00b4, L'e'  , 0x00e9, NORMAL_CHARACTER),	// ‘e’ -> ‘é’
  DEADTRANS(0x00b4, L'g'  , 0x01f5, NORMAL_CHARACTER),	// ‘g’ -> ‘ǵ’
  DEADTRANS(0x00b4, L'i'  , 0x00ed, NORMAL_CHARACTER),	// ‘i’ -> ‘í’
  DEADTRANS(0x00b4, L'k'  , 0x1e31, NORMAL_CHARACTER),	// ‘k’ -> ‘ḱ’
  DEADTRANS(0x00b4, L'l'  , 0x013a, NORMAL_CHARACTER),	// ‘l’ -> ‘ĺ’
  DEADTRANS(0x00b4, L'm'  , 0x1e3f, NORMAL_CHARACTER),	// ‘m’ -> ‘ḿ’
  DEADTRANS(0x00b4, L'n'  , 0x0144, NORMAL_CHARACTER),	// ‘n’ -> ‘ń’
  DEADTRANS(0x00b4, L'o'  , 0x00f3, NORMAL_CHARACTER),	// ‘o’ -> ‘ó’
  DEADTRANS(0x00b4, L'p'  , 0x1e55, NORMAL_CHARACTER),	// ‘p’ -> ‘ṕ’
  DEADTRANS(0x00b4, L'r'  , 0x0155, NORMAL_CHARACTER),	// ‘r’ -> ‘ŕ’
  DEADTRANS(0x00b4, L's'  , 0x015b, NORMAL_CHARACTER),	// ‘s’ -> ‘ś’
  DEADTRANS(0x00b4, L'u'  , 0x00fa, NORMAL_CHARACTER),	// ‘u’ -> ‘ú’
  DEADTRANS(0x00b4, L'v'  , 0x01d8, NORMAL_CHARACTER),	// ‘v’ -> ‘ǘ’
  DEADTRANS(0x00b4, L'w'  , 0x1e83, NORMAL_CHARACTER),	// ‘w’ -> ‘ẃ’
  DEADTRANS(0x00b4, L'y'  , 0x00fd, NORMAL_CHARACTER),	// ‘y’ -> ‘ý’
  DEADTRANS(0x00b4, L'z'  , 0x017a, NORMAL_CHARACTER),	// ‘z’ -> ‘ź’
  DEADTRANS(0x00b4, 0x00a0, 0x00b4, NORMAL_CHARACTER),	// U+00A0 -> U+00B4
  DEADTRANS(0x00b4, 0x00b7, 0x0301, NORMAL_CHARACTER),	// U+00B7 -> U+0301
  DEADTRANS(0x00b4, 0x00bf, 0x0301, NORMAL_CHARACTER),	// U+00BF -> U+0301
  DEADTRANS(0x00b4, 0x00c6, 0x01fc, NORMAL_CHARACTER),	// ‘Æ’ -> ‘Ǽ’
  DEADTRANS(0x00b4, 0x00c7, 0x1e08, NORMAL_CHARACTER),	// ‘Ç’ -> ‘Ḉ’
  DEADTRANS(0x00b4, 0x00ca, 0x1ebe, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ế’
  DEADTRANS(0x00b4, 0x00e6, 0x01fd, NORMAL_CHARACTER),	// ‘æ’ -> ‘ǽ’
  DEADTRANS(0x00b4, 0x00e7, 0x1e09, NORMAL_CHARACTER),	// ‘ç’ -> ‘ḉ’
  DEADTRANS(0x00b4, 0x00ea, 0x1ebf, NORMAL_CHARACTER),	// ‘ê’ -> ‘ế’
  DEADTRANS(0x00b4, 0x2019, 0x0301, NORMAL_CHARACTER),	// U+2019 -> U+0301
  DEADTRANS(0x00b4, 0x2026, 0x0301, NORMAL_CHARACTER),	// U+2026 -> U+0301
  DEADTRANS(0x00b4, 0x202f, 0x00b4, NORMAL_CHARACTER),	// U+202F -> U+00B4
  DEADTRANS(0x00b4, L','  , 0x074b, CHAINED_DEAD_KEY),	// ‹Cedilla› -> ‹Acute Accent + Cedilla›
  DEADTRANS(0x00b4, L'/'  , 0x074c, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Acute Accent + Long Solidus Overlay›
  DEADTRANS(0x00b4, L':'  , 0x07b2, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Acute Accent + Diaeresis›
  DEADTRANS(0x00b4, L'~'  , 0x07b3, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Acute Accent + Tilde›
  DEADTRANS(0x00b4, 0x00af, 0x07b4, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Acute Accent + Macron›
  DEADTRANS(0x00b4, 0x00b4, 0x02dd, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Double Acute Accent›
  DEADTRANS(0x00b4, 0x02c6, 0x07b5, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Acute Accent + Circumflex Accent›
  DEADTRANS(0x00b4, 0x02d8, 0x07b6, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Acute Accent + Breve›
  DEADTRANS(0x00b4, 0x02d9, 0x07b7, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Acute Accent + Dot Above›
  DEADTRANS(0x00b4, 0x02da, 0x07b8, CHAINED_DEAD_KEY),	// ‹Ring Above› -> ‹Acute Accent + Ring Above›
  DEADTRANS(0x00b4, 0x031b, 0x07b9, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Acute Accent + Horn›
  DEADTRANS(0x00b4, 0x1ebb, 0x0301, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0301
  DEADTRANS(0x00b4, L' '  , 0x00b4, NORMAL_CHARACTER),	// U+0020 -> U+00B4

// Dead key: Greek
  DEADTRANS(0x00b5, L'A'  , 0x0391, NORMAL_CHARACTER),	// ‘A’ -> ‘Α’
  DEADTRANS(0x00b5, L'B'  , 0x0392, NORMAL_CHARACTER),	// ‘B’ -> ‘Β’
  DEADTRANS(0x00b5, L'C'  , 0x03a8, NORMAL_CHARACTER),	// ‘C’ -> ‘Ψ’
  DEADTRANS(0x00b5, L'D'  , 0x0394, NORMAL_CHARACTER),	// ‘D’ -> ‘Δ’
  DEADTRANS(0x00b5, L'E'  , 0x0395, NORMAL_CHARACTER),	// ‘E’ -> ‘Ε’
  DEADTRANS(0x00b5, L'F'  , 0x03a6, NORMAL_CHARACTER),	// ‘F’ -> ‘Φ’
  DEADTRANS(0x00b5, L'G'  , 0x0393, NORMAL_CHARACTER),	// ‘G’ -> ‘Γ’
  DEADTRANS(0x00b5, L'H'  , 0x0397, NORMAL_CHARACTER),	// ‘H’ -> ‘Η’
  DEADTRANS(0x00b5, L'I'  , 0x0399, NORMAL_CHARACTER),	// ‘I’ -> ‘Ι’
  DEADTRANS(0x00b5, L'J'  , 0x039e, NORMAL_CHARACTER),	// ‘J’ -> ‘Ξ’
  DEADTRANS(0x00b5, L'K'  , 0x039a, NORMAL_CHARACTER),	// ‘K’ -> ‘Κ’
  DEADTRANS(0x00b5, L'L'  , 0x039b, NORMAL_CHARACTER),	// ‘L’ -> ‘Λ’
  DEADTRANS(0x00b5, L'M'  , 0x039c, NORMAL_CHARACTER),	// ‘M’ -> ‘Μ’
  DEADTRANS(0x00b5, L'N'  , 0x039d, NORMAL_CHARACTER),	// ‘N’ -> ‘Ν’
  DEADTRANS(0x00b5, L'O'  , 0x039f, NORMAL_CHARACTER),	// ‘O’ -> ‘Ο’
  DEADTRANS(0x00b5, L'P'  , 0x03a0, NORMAL_CHARACTER),	// ‘P’ -> ‘Π’
  DEADTRANS(0x00b5, L'R'  , 0x03a1, NORMAL_CHARACTER),	// ‘R’ -> ‘Ρ’
  DEADTRANS(0x00b5, L'S'  , 0x03a3, NORMAL_CHARACTER),	// ‘S’ -> ‘Σ’
  DEADTRANS(0x00b5, L'T'  , 0x03a4, NORMAL_CHARACTER),	// ‘T’ -> ‘Τ’
  DEADTRANS(0x00b5, L'U'  , 0x0398, NORMAL_CHARACTER),	// ‘U’ -> ‘Θ’
  DEADTRANS(0x00b5, L'V'  , 0x03a9, NORMAL_CHARACTER),	// ‘V’ -> ‘Ω’
  DEADTRANS(0x00b5, L'X'  , 0x03a7, NORMAL_CHARACTER),	// ‘X’ -> ‘Χ’
  DEADTRANS(0x00b5, L'Y'  , 0x03a5, NORMAL_CHARACTER),	// ‘Y’ -> ‘Υ’
  DEADTRANS(0x00b5, L'Z'  , 0x0396, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ζ’
  DEADTRANS(0x00b5, L'a'  , 0x03b1, NORMAL_CHARACTER),	// ‘a’ -> ‘α’
  DEADTRANS(0x00b5, L'b'  , 0x03b2, NORMAL_CHARACTER),	// ‘b’ -> ‘β’
  DEADTRANS(0x00b5, L'c'  , 0x03c8, NORMAL_CHARACTER),	// ‘c’ -> ‘ψ’
  DEADTRANS(0x00b5, L'd'  , 0x03b4, NORMAL_CHARACTER),	// ‘d’ -> ‘δ’
  DEADTRANS(0x00b5, L'e'  , 0x03b5, NORMAL_CHARACTER),	// ‘e’ -> ‘ε’
  DEADTRANS(0x00b5, L'f'  , 0x03c6, NORMAL_CHARACTER),	// ‘f’ -> ‘φ’
  DEADTRANS(0x00b5, L'g'  , 0x03b3, NORMAL_CHARACTER),	// ‘g’ -> ‘γ’
  DEADTRANS(0x00b5, L'h'  , 0x03b7, NORMAL_CHARACTER),	// ‘h’ -> ‘η’
  DEADTRANS(0x00b5, L'i'  , 0x03b9, NORMAL_CHARACTER),	// ‘i’ -> ‘ι’
  DEADTRANS(0x00b5, L'j'  , 0x03be, NORMAL_CHARACTER),	// ‘j’ -> ‘ξ’
  DEADTRANS(0x00b5, L'k'  , 0x03ba, NORMAL_CHARACTER),	// ‘k’ -> ‘κ’
  DEADTRANS(0x00b5, L'l'  , 0x03bb, NORMAL_CHARACTER),	// ‘l’ -> ‘λ’
  DEADTRANS(0x00b5, L'm'  , 0x03bc, NORMAL_CHARACTER),	// ‘m’ -> ‘μ’
  DEADTRANS(0x00b5, L'n'  , 0x03bd, NORMAL_CHARACTER),	// ‘n’ -> ‘ν’
  DEADTRANS(0x00b5, L'o'  , 0x03bf, NORMAL_CHARACTER),	// ‘o’ -> ‘ο’
  DEADTRANS(0x00b5, L'p'  , 0x03c0, NORMAL_CHARACTER),	// ‘p’ -> ‘π’
  DEADTRANS(0x00b5, L'r'  , 0x03c1, NORMAL_CHARACTER),	// ‘r’ -> ‘ρ’
  DEADTRANS(0x00b5, L's'  , 0x03c3, NORMAL_CHARACTER),	// ‘s’ -> ‘σ’
  DEADTRANS(0x00b5, L't'  , 0x03c4, NORMAL_CHARACTER),	// ‘t’ -> ‘τ’
  DEADTRANS(0x00b5, L'u'  , 0x03b8, NORMAL_CHARACTER),	// ‘u’ -> ‘θ’
  DEADTRANS(0x00b5, L'v'  , 0x03c9, NORMAL_CHARACTER),	// ‘v’ -> ‘ω’
  DEADTRANS(0x00b5, L'w'  , 0x03c2, NORMAL_CHARACTER),	// ‘w’ -> ‘ς’
  DEADTRANS(0x00b5, L'x'  , 0x03c7, NORMAL_CHARACTER),	// ‘x’ -> ‘χ’
  DEADTRANS(0x00b5, L'y'  , 0x03c5, NORMAL_CHARACTER),	// ‘y’ -> ‘υ’
  DEADTRANS(0x00b5, L'z'  , 0x03b6, NORMAL_CHARACTER),	// ‘z’ -> ‘ζ’
  DEADTRANS(0x00b5, L'/'  , 0x07ba, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Greek + Long Solidus Overlay›
  DEADTRANS(0x00b5, 0x00af, 0x07bb, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Greek + Macron›
  DEADTRANS(0x00b5, 0x00b5, 0x00b5, NORMAL_CHARACTER),	// ‹Greek› -> ‘µ’
  DEADTRANS(0x00b5, 0x1ebb, 0x07bc, CHAINED_DEAD_KEY),	// ‹Crochet en chef› -> ‹Greek + Crochet en chef›
  DEADTRANS(0x00b5, L' '  , 0x00b5, NORMAL_CHARACTER),	// U+0020 -> ‘µ’

// Dead key: Bépo Latin
  DEADTRANS(0x00df, L'!'  , 0x203d, NORMAL_CHARACTER),	// U+0021 -> U+203D
  DEADTRANS(0x00df, L'#'  , 0x266f, NORMAL_CHARACTER),	// U+0023 -> U+266F
  DEADTRANS(0x00df, L'$'  , 0x266d, NORMAL_CHARACTER),	// U+0024 -> U+266D
  DEADTRANS(0x00df, L'%'  , 0x2031, NORMAL_CHARACTER),	// U+0025 -> U+2031
  DEADTRANS(0x00df, L'&'  , 0x204a, NORMAL_CHARACTER),	// U+0026 -> U+204A
  DEADTRANS(0x00df, L'*'  , 0x2042, NORMAL_CHARACTER),	// U+002A -> U+2042
  DEADTRANS(0x00df, L'+'  , 0x205c, NORMAL_CHARACTER),	// U+002B -> U+205C
  DEADTRANS(0x00df, L'-'  , 0x00ad, NORMAL_CHARACTER),	// U+002D -> U+00AD
  DEADTRANS(0x00df, L'.'  , 0x2022, NORMAL_CHARACTER),	// U+002E -> U+2022
  DEADTRANS(0x00df, L'0'  , 0x2182, NORMAL_CHARACTER),	// ‘0’ -> ‘ↂ’
  DEADTRANS(0x00df, L'1'  , 0x2180, NORMAL_CHARACTER),	// ‘1’ -> ‘ↀ’
  DEADTRANS(0x00df, L'5'  , 0x2181, NORMAL_CHARACTER),	// ‘5’ -> ‘ↁ’
  DEADTRANS(0x00df, L':'  , 0x2043, NORMAL_CHARACTER),	// U+003A -> U+2043
  DEADTRANS(0x00df, L'?'  , 0x2e2e, NORMAL_CHARACTER),	// U+003F -> U+2E2E
  DEADTRANS(0x00df, L'A'  , 0x2c6d, NORMAL_CHARACTER),	// ‘A’ -> ‘Ɑ’
  DEADTRANS(0x00df, L'B'  , 0xa7b4, NORMAL_CHARACTER),	// ‘B’ -> ‘Ꞵ’
  DEADTRANS(0x00df, L'C'  , 0x03f4, NORMAL_CHARACTER),	// ‘C’ -> ‘ϴ’
  DEADTRANS(0x00df, L'D'  , 0x00d0, NORMAL_CHARACTER),	// ‘D’ -> ‘Ð’
  DEADTRANS(0x00df, L'E'  , 0x018f, NORMAL_CHARACTER),	// ‘E’ -> ‘Ə’
  DEADTRANS(0x00df, L'G'  , 0x0194, NORMAL_CHARACTER),	// ‘G’ -> ‘Ɣ’
  DEADTRANS(0x00df, L'H'  , 0x0241, NORMAL_CHARACTER),	// ‘H’ -> ‘Ɂ’
  DEADTRANS(0x00df, L'I'  , 0x0196, NORMAL_CHARACTER),	// ‘I’ -> ‘Ɩ’
  DEADTRANS(0x00df, L'J'  , 0x0132, NORMAL_CHARACTER),	// ‘J’ -> ‘Ĳ’
  DEADTRANS(0x00df, L'N'  , 0x014a, NORMAL_CHARACTER),	// ‘N’ -> ‘Ŋ’
  DEADTRANS(0x00df, L'O'  , 0x0186, NORMAL_CHARACTER),	// ‘O’ -> ‘Ɔ’
  DEADTRANS(0x00df, L'Q'  , 0xa7b3, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ꭓ’
  DEADTRANS(0x00df, L'S'  , 0x1e9e, NORMAL_CHARACTER),	// ‘S’ -> ‘ẞ’
  DEADTRANS(0x00df, L'T'  , 0x00de, NORMAL_CHARACTER),	// ‘T’ -> ‘Þ’
  DEADTRANS(0x00df, L'U'  , 0x01b1, NORMAL_CHARACTER),	// ‘U’ -> ‘Ʊ’
  DEADTRANS(0x00df, L'V'  , 0x0245, NORMAL_CHARACTER),	// ‘V’ -> ‘Ʌ’
  DEADTRANS(0x00df, L'W'  , 0x01f7, NORMAL_CHARACTER),	// ‘W’ -> ‘Ƿ’
  DEADTRANS(0x00df, L'X'  , 0xa78b, NORMAL_CHARACTER),	// ‘X’ -> ‘Ꞌ’
  DEADTRANS(0x00df, L'Y'  , 0x021c, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ȝ’
  DEADTRANS(0x00df, L'Z'  , 0x01b7, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ʒ’
  DEADTRANS(0x00df, L'_'  , 0x203e, NORMAL_CHARACTER),	// U+005F -> U+203E
  DEADTRANS(0x00df, L'`'  , 0x25cc, NORMAL_CHARACTER),	// U+0060 -> U+25CC
  DEADTRANS(0x00df, L'a'  , 0x0251, NORMAL_CHARACTER),	// ‘a’ -> ‘ɑ’
  DEADTRANS(0x00df, L'b'  , 0xa7b5, NORMAL_CHARACTER),	// ‘b’ -> ‘ꞵ’
  DEADTRANS(0x00df, L'c'  , 0x03b8, NORMAL_CHARACTER),	// ‘c’ -> ‘θ’
  DEADTRANS(0x00df, L'd'  , 0x00f0, NORMAL_CHARACTER),	// ‘d’ -> ‘ð’
  DEADTRANS(0x00df, L'e'  , 0x0259, NORMAL_CHARACTER),	// ‘e’ -> ‘ə’
  DEADTRANS(0x00df, L'f'  , 0x00aa, NORMAL_CHARACTER),	// ‘f’ -> ‘ª’
  DEADTRANS(0x00df, L'g'  , 0x0263, NORMAL_CHARACTER),	// ‘g’ -> ‘ɣ’
  DEADTRANS(0x00df, L'h'  , 0x0242, NORMAL_CHARACTER),	// ‘h’ -> ‘ɂ’
  DEADTRANS(0x00df, L'i'  , 0x0269, NORMAL_CHARACTER),	// ‘i’ -> ‘ɩ’
  DEADTRANS(0x00df, L'j'  , 0x0133, NORMAL_CHARACTER),	// ‘j’ -> ‘ĳ’
  DEADTRANS(0x00df, L'k'  , 0x0138, NORMAL_CHARACTER),	// ‘k’ -> ‘ĸ’
  DEADTRANS(0x00df, L'm'  , 0x00ba, NORMAL_CHARACTER),	// ‘m’ -> ‘º’
  DEADTRANS(0x00df, L'n'  , 0x014b, NORMAL_CHARACTER),	// ‘n’ -> ‘ŋ’
  DEADTRANS(0x00df, L'o'  , 0x0254, NORMAL_CHARACTER),	// ‘o’ -> ‘ɔ’
  DEADTRANS(0x00df, L'q'  , 0xab53, NORMAL_CHARACTER),	// ‘q’ -> ‘ꭓ’
  DEADTRANS(0x00df, L's'  , 0x00df, NORMAL_CHARACTER),	// ‘s’ -> ‘ß’
  DEADTRANS(0x00df, L't'  , 0x00fe, NORMAL_CHARACTER),	// ‘t’ -> ‘þ’
  DEADTRANS(0x00df, L'u'  , 0x028a, NORMAL_CHARACTER),	// ‘u’ -> ‘ʊ’
  DEADTRANS(0x00df, L'v'  , 0x028c, NORMAL_CHARACTER),	// ‘v’ -> ‘ʌ’
  DEADTRANS(0x00df, L'w'  , 0x01bf, NORMAL_CHARACTER),	// ‘w’ -> ‘ƿ’
  DEADTRANS(0x00df, L'x'  , 0xa78c, NORMAL_CHARACTER),	// ‘x’ -> ‘ꞌ’
  DEADTRANS(0x00df, L'y'  , 0x021d, NORMAL_CHARACTER),	// ‘y’ -> ‘ȝ’
  DEADTRANS(0x00df, L'z'  , 0x0292, NORMAL_CHARACTER),	// ‘z’ -> ‘ʒ’
  DEADTRANS(0x00df, L'{'  , 0x02bf, NORMAL_CHARACTER),	// U+007B -> ‘ʿ’
  DEADTRANS(0x00df, L'}'  , 0x02be, NORMAL_CHARACTER),	// U+007D -> ‘ʾ’
  DEADTRANS(0x00df, 0x00a0, 0x203f, NORMAL_CHARACTER),	// U+00A0 -> U+203F
  DEADTRANS(0x00df, 0x00a7, 0x2117, NORMAL_CHARACTER),	// U+00A7 -> U+2117
  DEADTRANS(0x00df, 0x00ab, 0x2039, NORMAL_CHARACTER),	// U+00AB -> U+2039
  DEADTRANS(0x00df, 0x00ac, 0x261e, NORMAL_CHARACTER),	// U+00AC -> U+261E
  DEADTRANS(0x00df, 0x00b6, 0x266e, NORMAL_CHARACTER),	// U+00B6 -> U+266E
  DEADTRANS(0x00df, 0x00b7, 0x25aa, NORMAL_CHARACTER),	// U+00B7 -> U+25AA
  DEADTRANS(0x00df, 0x00bb, 0x203a, NORMAL_CHARACTER),	// U+00BB -> U+203A
  DEADTRANS(0x00df, 0x00bc, 0x2153, NORMAL_CHARACTER),	// ‘¼’ -> ‘⅓’
  DEADTRANS(0x00df, 0x00be, 0x2154, NORMAL_CHARACTER),	// ‘¾’ -> ‘⅔’
  DEADTRANS(0x00df, 0x00c0, 0xa722, NORMAL_CHARACTER),	// ‘À’ -> ‘Ꜣ’
  DEADTRANS(0x00df, 0x00c6, 0xa724, NORMAL_CHARACTER),	// ‘Æ’ -> ‘Ꜥ’
  DEADTRANS(0x00df, 0x00c7, 0x01a9, NORMAL_CHARACTER),	// ‘Ç’ -> ‘Ʃ’
  DEADTRANS(0x00df, 0x00c8, 0x018e, NORMAL_CHARACTER),	// ‘È’ -> ‘Ǝ’
  DEADTRANS(0x00df, 0x00c9, 0x0190, NORMAL_CHARACTER),	// ‘É’ -> ‘Ɛ’
  DEADTRANS(0x00df, 0x00ca, 0xa7ae, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ɪ’
  DEADTRANS(0x00df, 0x00d7, 0x203b, NORMAL_CHARACTER),	// U+00D7 -> U+203B
  DEADTRANS(0x00df, 0x00d9, 0xa78d, NORMAL_CHARACTER),	// ‘Ù’ -> ‘Ɥ’
  DEADTRANS(0x00df, 0x00e0, 0xa723, NORMAL_CHARACTER),	// ‘à’ -> ‘ꜣ’
  DEADTRANS(0x00df, 0x00e6, 0xa725, NORMAL_CHARACTER),	// ‘æ’ -> ‘ꜥ’
  DEADTRANS(0x00df, 0x00e7, 0x0283, NORMAL_CHARACTER),	// ‘ç’ -> ‘ʃ’
  DEADTRANS(0x00df, 0x00e8, 0x01dd, NORMAL_CHARACTER),	// ‘è’ -> ‘ǝ’
  DEADTRANS(0x00df, 0x00e9, 0x025b, NORMAL_CHARACTER),	// ‘é’ -> ‘ɛ’
  DEADTRANS(0x00df, 0x00ea, 0x026a, NORMAL_CHARACTER),	// ‘ê’ -> ‘ɪ’
  DEADTRANS(0x00df, 0x00f9, 0x0265, NORMAL_CHARACTER),	// ‘ù’ -> ‘ɥ’
  DEADTRANS(0x00df, 0x0152, 0xa7b6, NORMAL_CHARACTER),	// ‘Œ’ -> ‘Ꞷ’
  DEADTRANS(0x00df, 0x0153, 0xa7b7, NORMAL_CHARACTER),	// ‘œ’ -> ‘ꞷ’
  DEADTRANS(0x00df, 0x017f, 0x1e9e, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẞ’
  DEADTRANS(0x00df, 0x2013, 0x2012, NORMAL_CHARACTER),	// U+2013 -> U+2012
  DEADTRANS(0x00df, 0x2014, 0x2015, NORMAL_CHARACTER),	// U+2014 -> U+2015
  DEADTRANS(0x00df, 0x2018, 0x02bb, NORMAL_CHARACTER),	// U+2018 -> ‘ʻ’
  DEADTRANS(0x00df, 0x2019, 0x02bc, NORMAL_CHARACTER),	// U+2019 -> ‘ʼ’
  DEADTRANS(0x00df, 0x202f, 0x2003, NORMAL_CHARACTER),	// U+202F -> U+2003
  DEADTRANS(0x00df, 0x2032, 0x2034, NORMAL_CHARACTER),	// U+2032 -> U+2034
  DEADTRANS(0x00df, 0x2033, 0x2057, NORMAL_CHARACTER),	// U+2033 -> U+2057
  DEADTRANS(0x00df, 0x2212, 0x2010, NORMAL_CHARACTER),	// U+2212 -> U+2010
  DEADTRANS(0x00df, L'/'  , 0x07bd, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Bépo Latin + Long Solidus Overlay›
  DEADTRANS(0x00df, 0x00df, 0x00df, NORMAL_CHARACTER),	// ‹Bépo Latin› -> ‘ß’
  DEADTRANS(0x00df, 0x0336, 0x07be, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Bépo Latin + Long Stroke Overlay›
  DEADTRANS(0x00df, 0x1ebb, 0x02bb, NORMAL_CHARACTER),	// ‹Crochet en chef› -> ‘ʻ’
  DEADTRANS(0x00df, L' '  , 0x2423, NORMAL_CHARACTER),	// U+0020 -> U+2423

// Dead key: Crosse
  DEADTRANS(0x0181, L'B'  , 0x0181, NORMAL_CHARACTER),	// ‘B’ -> ‘Ɓ’
  DEADTRANS(0x0181, L'C'  , 0x0187, NORMAL_CHARACTER),	// ‘C’ -> ‘Ƈ’
  DEADTRANS(0x0181, L'D'  , 0x018a, NORMAL_CHARACTER),	// ‘D’ -> ‘Ɗ’
  DEADTRANS(0x0181, L'G'  , 0x0193, NORMAL_CHARACTER),	// ‘G’ -> ‘Ɠ’
  DEADTRANS(0x0181, L'H'  , 0xa7aa, NORMAL_CHARACTER),	// ‘H’ -> ‘Ɦ’
  DEADTRANS(0x0181, L'K'  , 0x0198, NORMAL_CHARACTER),	// ‘K’ -> ‘Ƙ’
  DEADTRANS(0x0181, L'P'  , 0x01a4, NORMAL_CHARACTER),	// ‘P’ -> ‘Ƥ’
  DEADTRANS(0x0181, L'T'  , 0x01ac, NORMAL_CHARACTER),	// ‘T’ -> ‘Ƭ’
  DEADTRANS(0x0181, L'V'  , 0x01b2, NORMAL_CHARACTER),	// ‘V’ -> ‘Ʋ’
  DEADTRANS(0x0181, L'W'  , 0x2c72, NORMAL_CHARACTER),	// ‘W’ -> ‘Ⱳ’
  DEADTRANS(0x0181, L'Y'  , 0x01b3, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ƴ’
  DEADTRANS(0x0181, L'b'  , 0x0253, NORMAL_CHARACTER),	// ‘b’ -> ‘ɓ’
  DEADTRANS(0x0181, L'c'  , 0x0188, NORMAL_CHARACTER),	// ‘c’ -> ‘ƈ’
  DEADTRANS(0x0181, L'd'  , 0x0257, NORMAL_CHARACTER),	// ‘d’ -> ‘ɗ’
  DEADTRANS(0x0181, L'g'  , 0x0260, NORMAL_CHARACTER),	// ‘g’ -> ‘ɠ’
  DEADTRANS(0x0181, L'h'  , 0x0266, NORMAL_CHARACTER),	// ‘h’ -> ‘ɦ’
  DEADTRANS(0x0181, L'k'  , 0x0199, NORMAL_CHARACTER),	// ‘k’ -> ‘ƙ’
  DEADTRANS(0x0181, L'p'  , 0x01a5, NORMAL_CHARACTER),	// ‘p’ -> ‘ƥ’
  DEADTRANS(0x0181, L'q'  , 0x02a0, NORMAL_CHARACTER),	// ‘q’ -> ‘ʠ’
  DEADTRANS(0x0181, L't'  , 0x01ad, NORMAL_CHARACTER),	// ‘t’ -> ‘ƭ’
  DEADTRANS(0x0181, L'u'  , 0xab52, NORMAL_CHARACTER),	// ‘u’ -> ‘ꭒ’
  DEADTRANS(0x0181, L'v'  , 0x028b, NORMAL_CHARACTER),	// ‘v’ -> ‘ʋ’
  DEADTRANS(0x0181, L'w'  , 0x2c73, NORMAL_CHARACTER),	// ‘w’ -> ‘ⱳ’
  DEADTRANS(0x0181, L'y'  , 0x01b4, NORMAL_CHARACTER),	// ‘y’ -> ‘ƴ’
  DEADTRANS(0x0181, L' '  , 0x0181, NORMAL_CHARACTER),	// U+0020 -> ‘Ɓ’

// Dead key: Circumflex Accent
  DEADTRANS(0x02c6, L'('  , 0x207d, NORMAL_CHARACTER),	// U+0028 -> U+207D
  DEADTRANS(0x02c6, L')'  , 0x207e, NORMAL_CHARACTER),	// U+0029 -> U+207E
  DEADTRANS(0x02c6, L'+'  , 0x207a, NORMAL_CHARACTER),	// U+002B -> U+207A
  DEADTRANS(0x02c6, L'-'  , 0x207b, NORMAL_CHARACTER),	// U+002D -> U+207B
  DEADTRANS(0x02c6, L'.'  , 0x0302, NORMAL_CHARACTER),	// U+002E -> U+0302
  DEADTRANS(0x02c6, L'0'  , 0x2070, NORMAL_CHARACTER),	// ‘0’ -> ‘⁰’
  DEADTRANS(0x02c6, L'1'  , 0x00b9, NORMAL_CHARACTER),	// ‘1’ -> ‘¹’
  DEADTRANS(0x02c6, L'2'  , 0x00b2, NORMAL_CHARACTER),	// ‘2’ -> ‘²’
  DEADTRANS(0x02c6, L'3'  , 0x00b3, NORMAL_CHARACTER),	// ‘3’ -> ‘³’
  DEADTRANS(0x02c6, L'4'  , 0x2074, NORMAL_CHARACTER),	// ‘4’ -> ‘⁴’
  DEADTRANS(0x02c6, L'5'  , 0x2075, NORMAL_CHARACTER),	// ‘5’ -> ‘⁵’
  DEADTRANS(0x02c6, L'6'  , 0x2076, NORMAL_CHARACTER),	// ‘6’ -> ‘⁶’
  DEADTRANS(0x02c6, L'7'  , 0x2077, NORMAL_CHARACTER),	// ‘7’ -> ‘⁷’
  DEADTRANS(0x02c6, L'8'  , 0x2078, NORMAL_CHARACTER),	// ‘8’ -> ‘⁸’
  DEADTRANS(0x02c6, L'9'  , 0x2079, NORMAL_CHARACTER),	// ‘9’ -> ‘⁹’
  DEADTRANS(0x02c6, L':'  , 0x0302, NORMAL_CHARACTER),	// U+003A -> U+0302
  DEADTRANS(0x02c6, L'='  , 0x207c, NORMAL_CHARACTER),	// U+003D -> U+207C
  DEADTRANS(0x02c6, L'?'  , 0x0302, NORMAL_CHARACTER),	// U+003F -> U+0302
  DEADTRANS(0x02c6, L'A'  , 0x00c2, NORMAL_CHARACTER),	// ‘A’ -> ‘Â’
  DEADTRANS(0x02c6, L'C'  , 0x0108, NORMAL_CHARACTER),	// ‘C’ -> ‘Ĉ’
  DEADTRANS(0x02c6, L'E'  , 0x00ca, NORMAL_CHARACTER),	// ‘E’ -> ‘Ê’
  DEADTRANS(0x02c6, L'G'  , 0x011c, NORMAL_CHARACTER),	// ‘G’ -> ‘Ĝ’
  DEADTRANS(0x02c6, L'H'  , 0x0124, NORMAL_CHARACTER),	// ‘H’ -> ‘Ĥ’
  DEADTRANS(0x02c6, L'I'  , 0x00ce, NORMAL_CHARACTER),	// ‘I’ -> ‘Î’
  DEADTRANS(0x02c6, L'J'  , 0x0134, NORMAL_CHARACTER),	// ‘J’ -> ‘Ĵ’
  DEADTRANS(0x02c6, L'O'  , 0x00d4, NORMAL_CHARACTER),	// ‘O’ -> ‘Ô’
  DEADTRANS(0x02c6, L'S'  , 0x015c, NORMAL_CHARACTER),	// ‘S’ -> ‘Ŝ’
  DEADTRANS(0x02c6, L'U'  , 0x00db, NORMAL_CHARACTER),	// ‘U’ -> ‘Û’
  DEADTRANS(0x02c6, L'W'  , 0x0174, NORMAL_CHARACTER),	// ‘W’ -> ‘Ŵ’
  DEADTRANS(0x02c6, L'Y'  , 0x0176, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ŷ’
  DEADTRANS(0x02c6, L'Z'  , 0x1e90, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẑ’
  DEADTRANS(0x02c6, L'_'  , L'^'  , NORMAL_CHARACTER),	// U+005F -> U+005E
  DEADTRANS(0x02c6, L'a'  , 0x00e2, NORMAL_CHARACTER),	// ‘a’ -> ‘â’
  DEADTRANS(0x02c6, L'c'  , 0x0109, NORMAL_CHARACTER),	// ‘c’ -> ‘ĉ’
  DEADTRANS(0x02c6, L'e'  , 0x00ea, NORMAL_CHARACTER),	// ‘e’ -> ‘ê’
  DEADTRANS(0x02c6, L'g'  , 0x011d, NORMAL_CHARACTER),	// ‘g’ -> ‘ĝ’
  DEADTRANS(0x02c6, L'h'  , 0x0125, NORMAL_CHARACTER),	// ‘h’ -> ‘ĥ’
  DEADTRANS(0x02c6, L'i'  , 0x00ee, NORMAL_CHARACTER),	// ‘i’ -> ‘î’
  DEADTRANS(0x02c6, L'j'  , 0x0135, NORMAL_CHARACTER),	// ‘j’ -> ‘ĵ’
  DEADTRANS(0x02c6, L'o'  , 0x00f4, NORMAL_CHARACTER),	// ‘o’ -> ‘ô’
  DEADTRANS(0x02c6, L's'  , 0x015d, NORMAL_CHARACTER),	// ‘s’ -> ‘ŝ’
  DEADTRANS(0x02c6, L'u'  , 0x00fb, NORMAL_CHARACTER),	// ‘u’ -> ‘û’
  DEADTRANS(0x02c6, L'w'  , 0x0175, NORMAL_CHARACTER),	// ‘w’ -> ‘ŵ’
  DEADTRANS(0x02c6, L'y'  , 0x0177, NORMAL_CHARACTER),	// ‘y’ -> ‘ŷ’
  DEADTRANS(0x02c6, L'z'  , 0x1e91, NORMAL_CHARACTER),	// ‘z’ -> ‘ẑ’
  DEADTRANS(0x02c6, 0x00a0, L'^'  , NORMAL_CHARACTER),	// U+00A0 -> U+005E
  DEADTRANS(0x02c6, 0x00b7, 0x0302, NORMAL_CHARACTER),	// U+00B7 -> U+0302
  DEADTRANS(0x02c6, 0x00bf, 0x0302, NORMAL_CHARACTER),	// U+00BF -> U+0302
  DEADTRANS(0x02c6, 0x00c0, 0x1ea6, NORMAL_CHARACTER),	// ‘À’ -> ‘Ầ’
  DEADTRANS(0x02c6, 0x00c8, 0x1ec0, NORMAL_CHARACTER),	// ‘È’ -> ‘Ề’
  DEADTRANS(0x02c6, 0x00c9, 0x1ebe, NORMAL_CHARACTER),	// ‘É’ -> ‘Ế’
  DEADTRANS(0x02c6, 0x00e0, 0x1ea7, NORMAL_CHARACTER),	// ‘à’ -> ‘ầ’
  DEADTRANS(0x02c6, 0x00e8, 0x1ec1, NORMAL_CHARACTER),	// ‘è’ -> ‘ề’
  DEADTRANS(0x02c6, 0x00e9, 0x1ebf, NORMAL_CHARACTER),	// ‘é’ -> ‘ế’
  DEADTRANS(0x02c6, 0x2019, 0x0302, NORMAL_CHARACTER),	// U+2019 -> U+0302
  DEADTRANS(0x02c6, 0x2026, 0x0302, NORMAL_CHARACTER),	// U+2026 -> U+0302
  DEADTRANS(0x02c6, 0x202f, L'^'  , NORMAL_CHARACTER),	// U+202F -> U+005E
  DEADTRANS(0x02c6, L'.'  , 0x07bf, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Circumflex Accent + Dot Below›
  DEADTRANS(0x02c6, L'`'  , 0x07fb, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Circumflex Accent + Grave Accent›
  DEADTRANS(0x02c6, L'~'  , 0x07fc, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Circumflex Accent + Tilde›
  DEADTRANS(0x02c6, 0x00b4, 0x082e, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Circumflex Accent + Acute Accent›
  DEADTRANS(0x02c6, 0x02c6, 0x2038, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Circumflex Accent Below›
  DEADTRANS(0x02c6, 0x1e33, 0x082f, CHAINED_DEAD_KEY),	// ‹Bépo dot below› -> ‹Circumflex Accent + Bépo dot below›
  DEADTRANS(0x02c6, 0x1ebb, 0x0302, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0302
  DEADTRANS(0x02c6, L' '  , L'^'  , NORMAL_CHARACTER),	// U+0020 -> U+005E

// Dead key: Caron
  DEADTRANS(0x02c7, L'('  , 0x208d, NORMAL_CHARACTER),	// U+0028 -> U+208D
  DEADTRANS(0x02c7, L')'  , 0x208e, NORMAL_CHARACTER),	// U+0029 -> U+208E
  DEADTRANS(0x02c7, L'+'  , 0x208a, NORMAL_CHARACTER),	// U+002B -> U+208A
  DEADTRANS(0x02c7, L'-'  , 0x208b, NORMAL_CHARACTER),	// U+002D -> U+208B
  DEADTRANS(0x02c7, L'.'  , 0x030c, NORMAL_CHARACTER),	// U+002E -> U+030C
  DEADTRANS(0x02c7, L'0'  , 0x2080, NORMAL_CHARACTER),	// ‘0’ -> ‘₀’
  DEADTRANS(0x02c7, L'1'  , 0x2081, NORMAL_CHARACTER),	// ‘1’ -> ‘₁’
  DEADTRANS(0x02c7, L'2'  , 0x2082, NORMAL_CHARACTER),	// ‘2’ -> ‘₂’
  DEADTRANS(0x02c7, L'3'  , 0x2083, NORMAL_CHARACTER),	// ‘3’ -> ‘₃’
  DEADTRANS(0x02c7, L'4'  , 0x2084, NORMAL_CHARACTER),	// ‘4’ -> ‘₄’
  DEADTRANS(0x02c7, L'5'  , 0x2085, NORMAL_CHARACTER),	// ‘5’ -> ‘₅’
  DEADTRANS(0x02c7, L'6'  , 0x2086, NORMAL_CHARACTER),	// ‘6’ -> ‘₆’
  DEADTRANS(0x02c7, L'7'  , 0x2087, NORMAL_CHARACTER),	// ‘7’ -> ‘₇’
  DEADTRANS(0x02c7, L'8'  , 0x2088, NORMAL_CHARACTER),	// ‘8’ -> ‘₈’
  DEADTRANS(0x02c7, L'9'  , 0x2089, NORMAL_CHARACTER),	// ‘9’ -> ‘₉’
  DEADTRANS(0x02c7, L':'  , 0x030c, NORMAL_CHARACTER),	// U+003A -> U+030C
  DEADTRANS(0x02c7, L'='  , 0x208c, NORMAL_CHARACTER),	// U+003D -> U+208C
  DEADTRANS(0x02c7, L'?'  , 0x030c, NORMAL_CHARACTER),	// U+003F -> U+030C
  DEADTRANS(0x02c7, L'A'  , 0x01cd, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǎ’
  DEADTRANS(0x02c7, L'C'  , 0x010c, NORMAL_CHARACTER),	// ‘C’ -> ‘Č’
  DEADTRANS(0x02c7, L'D'  , 0x010e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ď’
  DEADTRANS(0x02c7, L'E'  , 0x011a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ě’
  DEADTRANS(0x02c7, L'G'  , 0x01e6, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǧ’
  DEADTRANS(0x02c7, L'H'  , 0x021e, NORMAL_CHARACTER),	// ‘H’ -> ‘Ȟ’
  DEADTRANS(0x02c7, L'I'  , 0x01cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ǐ’
  DEADTRANS(0x02c7, L'K'  , 0x01e8, NORMAL_CHARACTER),	// ‘K’ -> ‘Ǩ’
  DEADTRANS(0x02c7, L'L'  , 0x013d, NORMAL_CHARACTER),	// ‘L’ -> ‘Ľ’
  DEADTRANS(0x02c7, L'N'  , 0x0147, NORMAL_CHARACTER),	// ‘N’ -> ‘Ň’
  DEADTRANS(0x02c7, L'O'  , 0x01d1, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǒ’
  DEADTRANS(0x02c7, L'R'  , 0x0158, NORMAL_CHARACTER),	// ‘R’ -> ‘Ř’
  DEADTRANS(0x02c7, L'S'  , 0x0160, NORMAL_CHARACTER),	// ‘S’ -> ‘Š’
  DEADTRANS(0x02c7, L'T'  , 0x0164, NORMAL_CHARACTER),	// ‘T’ -> ‘Ť’
  DEADTRANS(0x02c7, L'U'  , 0x01d3, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǔ’
  DEADTRANS(0x02c7, L'V'  , 0x01d9, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǚ’
  DEADTRANS(0x02c7, L'Z'  , 0x017d, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ž’
  DEADTRANS(0x02c7, L'_'  , 0x02c7, NORMAL_CHARACTER),	// U+005F -> ‘ˇ’
  DEADTRANS(0x02c7, L'a'  , 0x01ce, NORMAL_CHARACTER),	// ‘a’ -> ‘ǎ’
  DEADTRANS(0x02c7, L'c'  , 0x010d, NORMAL_CHARACTER),	// ‘c’ -> ‘č’
  DEADTRANS(0x02c7, L'd'  , 0x010f, NORMAL_CHARACTER),	// ‘d’ -> ‘ď’
  DEADTRANS(0x02c7, L'e'  , 0x011b, NORMAL_CHARACTER),	// ‘e’ -> ‘ě’
  DEADTRANS(0x02c7, L'g'  , 0x01e7, NORMAL_CHARACTER),	// ‘g’ -> ‘ǧ’
  DEADTRANS(0x02c7, L'h'  , 0x021f, NORMAL_CHARACTER),	// ‘h’ -> ‘ȟ’
  DEADTRANS(0x02c7, L'i'  , 0x01d0, NORMAL_CHARACTER),	// ‘i’ -> ‘ǐ’
  DEADTRANS(0x02c7, L'j'  , 0x01f0, NORMAL_CHARACTER),	// ‘j’ -> ‘ǰ’
  DEADTRANS(0x02c7, L'k'  , 0x01e9, NORMAL_CHARACTER),	// ‘k’ -> ‘ǩ’
  DEADTRANS(0x02c7, L'l'  , 0x013e, NORMAL_CHARACTER),	// ‘l’ -> ‘ľ’
  DEADTRANS(0x02c7, L'n'  , 0x0148, NORMAL_CHARACTER),	// ‘n’ -> ‘ň’
  DEADTRANS(0x02c7, L'o'  , 0x01d2, NORMAL_CHARACTER),	// ‘o’ -> ‘ǒ’
  DEADTRANS(0x02c7, L'r'  , 0x0159, NORMAL_CHARACTER),	// ‘r’ -> ‘ř’
  DEADTRANS(0x02c7, L's'  , 0x0161, NORMAL_CHARACTER),	// ‘s’ -> ‘š’
  DEADTRANS(0x02c7, L't'  , 0x0165, NORMAL_CHARACTER),	// ‘t’ -> ‘ť’
  DEADTRANS(0x02c7, L'u'  , 0x01d4, NORMAL_CHARACTER),	// ‘u’ -> ‘ǔ’
  DEADTRANS(0x02c7, L'v'  , 0x01da, NORMAL_CHARACTER),	// ‘v’ -> ‘ǚ’
  DEADTRANS(0x02c7, L'z'  , 0x017e, NORMAL_CHARACTER),	// ‘z’ -> ‘ž’
  DEADTRANS(0x02c7, 0x00a0, 0x02c7, NORMAL_CHARACTER),	// U+00A0 -> ‘ˇ’
  DEADTRANS(0x02c7, 0x00b7, 0x030c, NORMAL_CHARACTER),	// U+00B7 -> U+030C
  DEADTRANS(0x02c7, 0x00bf, 0x030c, NORMAL_CHARACTER),	// U+00BF -> U+030C
  DEADTRANS(0x02c7, 0x2019, 0x030c, NORMAL_CHARACTER),	// U+2019 -> U+030C
  DEADTRANS(0x02c7, 0x2026, 0x030c, NORMAL_CHARACTER),	// U+2026 -> U+030C
  DEADTRANS(0x02c7, 0x202f, 0x02c7, NORMAL_CHARACTER),	// U+202F -> ‘ˇ’
  DEADTRANS(0x02c7, L':'  , 0x083f, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Caron + Diaeresis›
  DEADTRANS(0x02c7, 0x00df, 0x085c, CHAINED_DEAD_KEY),	// ‹Bépo Latin› -> ‹Caron + Bépo Latin›
  DEADTRANS(0x02c7, 0x02c7, 0x02ec, CHAINED_DEAD_KEY),	// ‹Caron› -> ‹Caron Below›
  DEADTRANS(0x02c7, 0x02d9, 0x085d, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Caron + Dot Above›
  DEADTRANS(0x02c7, 0x1ebb, 0x030c, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+030C
  DEADTRANS(0x02c7, L' '  , 0x02c7, NORMAL_CHARACTER),	// U+0020 -> ‘ˇ’

// Dead key: Macron Below
  DEADTRANS(0x02cd, L'.'  , 0x0331, NORMAL_CHARACTER),	// U+002E -> U+0331
  DEADTRANS(0x02cd, L':'  , 0x0331, NORMAL_CHARACTER),	// U+003A -> U+0331
  DEADTRANS(0x02cd, L'?'  , 0x0331, NORMAL_CHARACTER),	// U+003F -> U+0331
  DEADTRANS(0x02cd, L'B'  , 0x1e06, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḇ’
  DEADTRANS(0x02cd, L'D'  , 0x1e0e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḏ’
  DEADTRANS(0x02cd, L'K'  , 0x1e34, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḵ’
  DEADTRANS(0x02cd, L'L'  , 0x1e3a, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḻ’
  DEADTRANS(0x02cd, L'N'  , 0x1e48, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṉ’
  DEADTRANS(0x02cd, L'R'  , 0x1e5e, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṟ’
  DEADTRANS(0x02cd, L'T'  , 0x1e6e, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṯ’
  DEADTRANS(0x02cd, L'Z'  , 0x1e94, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẕ’
  DEADTRANS(0x02cd, L'_'  , L'_'  , NORMAL_CHARACTER),	// U+005F -> U+005F
  DEADTRANS(0x02cd, L'b'  , 0x1e07, NORMAL_CHARACTER),	// ‘b’ -> ‘ḇ’
  DEADTRANS(0x02cd, L'd'  , 0x1e0f, NORMAL_CHARACTER),	// ‘d’ -> ‘ḏ’
  DEADTRANS(0x02cd, L'h'  , 0x1e96, NORMAL_CHARACTER),	// ‘h’ -> ‘ẖ’
  DEADTRANS(0x02cd, L'k'  , 0x1e35, NORMAL_CHARACTER),	// ‘k’ -> ‘ḵ’
  DEADTRANS(0x02cd, L'l'  , 0x1e3b, NORMAL_CHARACTER),	// ‘l’ -> ‘ḻ’
  DEADTRANS(0x02cd, L'n'  , 0x1e49, NORMAL_CHARACTER),	// ‘n’ -> ‘ṉ’
  DEADTRANS(0x02cd, L'r'  , 0x1e5f, NORMAL_CHARACTER),	// ‘r’ -> ‘ṟ’
  DEADTRANS(0x02cd, L't'  , 0x1e6f, NORMAL_CHARACTER),	// ‘t’ -> ‘ṯ’
  DEADTRANS(0x02cd, L'z'  , 0x1e95, NORMAL_CHARACTER),	// ‘z’ -> ‘ẕ’
  DEADTRANS(0x02cd, 0x00a0, L'_'  , NORMAL_CHARACTER),	// U+00A0 -> U+005F
  DEADTRANS(0x02cd, 0x00b7, 0x0331, NORMAL_CHARACTER),	// U+00B7 -> U+0331
  DEADTRANS(0x02cd, 0x00bf, 0x0331, NORMAL_CHARACTER),	// U+00BF -> U+0331
  DEADTRANS(0x02cd, 0x2019, 0x0331, NORMAL_CHARACTER),	// U+2019 -> U+0331
  DEADTRANS(0x02cd, 0x2026, 0x0331, NORMAL_CHARACTER),	// U+2026 -> U+0331
  DEADTRANS(0x02cd, 0x202f, L'_'  , NORMAL_CHARACTER),	// U+202F -> U+005F
  DEADTRANS(0x02cd, 0x1ebb, 0x0331, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0331
  DEADTRANS(0x02cd, L' '  , L'_'  , NORMAL_CHARACTER),	// U+0020 -> U+005F

// Dead key: Breve
  DEADTRANS(0x02d8, L'.'  , 0x0306, NORMAL_CHARACTER),	// U+002E -> U+0306
  DEADTRANS(0x02d8, L':'  , 0x0306, NORMAL_CHARACTER),	// U+003A -> U+0306
  DEADTRANS(0x02d8, L'?'  , 0x0306, NORMAL_CHARACTER),	// U+003F -> U+0306
  DEADTRANS(0x02d8, L'A'  , 0x0102, NORMAL_CHARACTER),	// ‘A’ -> ‘Ă’
  DEADTRANS(0x02d8, L'E'  , 0x0114, NORMAL_CHARACTER),	// ‘E’ -> ‘Ĕ’
  DEADTRANS(0x02d8, L'G'  , 0x011e, NORMAL_CHARACTER),	// ‘G’ -> ‘Ğ’
  DEADTRANS(0x02d8, L'I'  , 0x012c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĭ’
  DEADTRANS(0x02d8, L'O'  , 0x014e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ŏ’
  DEADTRANS(0x02d8, L'U'  , 0x016c, NORMAL_CHARACTER),	// ‘U’ -> ‘Ŭ’
  DEADTRANS(0x02d8, L'_'  , 0x02d8, NORMAL_CHARACTER),	// U+005F -> U+02D8
  DEADTRANS(0x02d8, L'a'  , 0x0103, NORMAL_CHARACTER),	// ‘a’ -> ‘ă’
  DEADTRANS(0x02d8, L'e'  , 0x0115, NORMAL_CHARACTER),	// ‘e’ -> ‘ĕ’
  DEADTRANS(0x02d8, L'g'  , 0x011f, NORMAL_CHARACTER),	// ‘g’ -> ‘ğ’
  DEADTRANS(0x02d8, L'i'  , 0x012d, NORMAL_CHARACTER),	// ‘i’ -> ‘ĭ’
  DEADTRANS(0x02d8, L'o'  , 0x014f, NORMAL_CHARACTER),	// ‘o’ -> ‘ŏ’
  DEADTRANS(0x02d8, L'u'  , 0x016d, NORMAL_CHARACTER),	// ‘u’ -> ‘ŭ’
  DEADTRANS(0x02d8, 0x00a0, 0x02d8, NORMAL_CHARACTER),	// U+00A0 -> U+02D8
  DEADTRANS(0x02d8, 0x00b7, 0x0306, NORMAL_CHARACTER),	// U+00B7 -> U+0306
  DEADTRANS(0x02d8, 0x00bf, 0x0306, NORMAL_CHARACTER),	// U+00BF -> U+0306
  DEADTRANS(0x02d8, 0x00c0, 0x1eb0, NORMAL_CHARACTER),	// ‘À’ -> ‘Ằ’
  DEADTRANS(0x02d8, 0x00e0, 0x1eb1, NORMAL_CHARACTER),	// ‘à’ -> ‘ằ’
  DEADTRANS(0x02d8, 0x2019, 0x0306, NORMAL_CHARACTER),	// U+2019 -> U+0306
  DEADTRANS(0x02d8, 0x2026, 0x0306, NORMAL_CHARACTER),	// U+2026 -> U+0306
  DEADTRANS(0x02d8, 0x202f, 0x02d8, NORMAL_CHARACTER),	// U+202F -> U+02D8
  DEADTRANS(0x02d8, L','  , 0x085f, CHAINED_DEAD_KEY),	// ‹Cedilla› -> ‹Breve + Cedilla›
  DEADTRANS(0x02d8, L'.'  , 0x086b, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Breve + Dot Below›
  DEADTRANS(0x02d8, L'`'  , 0x086c, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Breve + Grave Accent›
  DEADTRANS(0x02d8, L'~'  , 0x086d, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Breve + Tilde›
  DEADTRANS(0x02d8, 0x00b4, 0x086e, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Breve + Acute Accent›
  DEADTRANS(0x02d8, 0x02d8, 0x0311, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Inverted Breve›
  DEADTRANS(0x02d8, 0x1e33, 0x086f, CHAINED_DEAD_KEY),	// ‹Bépo dot below› -> ‹Breve + Bépo dot below›
  DEADTRANS(0x02d8, 0x1ebb, 0x0306, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0306
  DEADTRANS(0x02d8, L' '  , 0x02d8, NORMAL_CHARACTER),	// U+0020 -> U+02D8

// Dead key: Dot Above
  DEADTRANS(0x02d9, L'.'  , 0x0307, NORMAL_CHARACTER),	// U+002E -> U+0307
  DEADTRANS(0x02d9, L':'  , 0x0307, NORMAL_CHARACTER),	// U+003A -> U+0307
  DEADTRANS(0x02d9, L'?'  , 0x0307, NORMAL_CHARACTER),	// U+003F -> U+0307
  DEADTRANS(0x02d9, L'A'  , 0x0226, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȧ’
  DEADTRANS(0x02d9, L'B'  , 0x1e02, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḃ’
  DEADTRANS(0x02d9, L'C'  , 0x010a, NORMAL_CHARACTER),	// ‘C’ -> ‘Ċ’
  DEADTRANS(0x02d9, L'D'  , 0x1e0a, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḋ’
  DEADTRANS(0x02d9, L'E'  , 0x0116, NORMAL_CHARACTER),	// ‘E’ -> ‘Ė’
  DEADTRANS(0x02d9, L'F'  , 0x1e1e, NORMAL_CHARACTER),	// ‘F’ -> ‘Ḟ’
  DEADTRANS(0x02d9, L'G'  , 0x0120, NORMAL_CHARACTER),	// ‘G’ -> ‘Ġ’
  DEADTRANS(0x02d9, L'H'  , 0x1e22, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḣ’
  DEADTRANS(0x02d9, L'I'  , 0x0130, NORMAL_CHARACTER),	// ‘I’ -> ‘İ’
  DEADTRANS(0x02d9, L'L'  , 0x013f, NORMAL_CHARACTER),	// ‘L’ -> ‘Ŀ’
  DEADTRANS(0x02d9, L'M'  , 0x1e40, NORMAL_CHARACTER),	// ‘M’ -> ‘Ṁ’
  DEADTRANS(0x02d9, L'N'  , 0x1e44, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṅ’
  DEADTRANS(0x02d9, L'O'  , 0x022e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȯ’
  DEADTRANS(0x02d9, L'P'  , 0x1e56, NORMAL_CHARACTER),	// ‘P’ -> ‘Ṗ’
  DEADTRANS(0x02d9, L'R'  , 0x1e58, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṙ’
  DEADTRANS(0x02d9, L'S'  , 0x1e60, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṡ’
  DEADTRANS(0x02d9, L'T'  , 0x1e6a, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṫ’
  DEADTRANS(0x02d9, L'W'  , 0x1e86, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẇ’
  DEADTRANS(0x02d9, L'X'  , 0x1e8a, NORMAL_CHARACTER),	// ‘X’ -> ‘Ẋ’
  DEADTRANS(0x02d9, L'Y'  , 0x1e8e, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ẏ’
  DEADTRANS(0x02d9, L'Z'  , 0x017b, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ż’
  DEADTRANS(0x02d9, L'_'  , 0x02d9, NORMAL_CHARACTER),	// U+005F -> U+02D9
  DEADTRANS(0x02d9, L'a'  , 0x0227, NORMAL_CHARACTER),	// ‘a’ -> ‘ȧ’
  DEADTRANS(0x02d9, L'b'  , 0x1e03, NORMAL_CHARACTER),	// ‘b’ -> ‘ḃ’
  DEADTRANS(0x02d9, L'c'  , 0x010b, NORMAL_CHARACTER),	// ‘c’ -> ‘ċ’
  DEADTRANS(0x02d9, L'd'  , 0x1e0b, NORMAL_CHARACTER),	// ‘d’ -> ‘ḋ’
  DEADTRANS(0x02d9, L'e'  , 0x0117, NORMAL_CHARACTER),	// ‘e’ -> ‘ė’
  DEADTRANS(0x02d9, L'f'  , 0x1e1f, NORMAL_CHARACTER),	// ‘f’ -> ‘ḟ’
  DEADTRANS(0x02d9, L'g'  , 0x0121, NORMAL_CHARACTER),	// ‘g’ -> ‘ġ’
  DEADTRANS(0x02d9, L'h'  , 0x1e23, NORMAL_CHARACTER),	// ‘h’ -> ‘ḣ’
  DEADTRANS(0x02d9, L'i'  , 0x0131, NORMAL_CHARACTER),	// ‘i’ -> ‘ı’
  DEADTRANS(0x02d9, L'j'  , 0x0237, NORMAL_CHARACTER),	// ‘j’ -> ‘ȷ’
  DEADTRANS(0x02d9, L'l'  , 0x0140, NORMAL_CHARACTER),	// ‘l’ -> ‘ŀ’
  DEADTRANS(0x02d9, L'm'  , 0x1e41, NORMAL_CHARACTER),	// ‘m’ -> ‘ṁ’
  DEADTRANS(0x02d9, L'n'  , 0x1e45, NORMAL_CHARACTER),	// ‘n’ -> ‘ṅ’
  DEADTRANS(0x02d9, L'o'  , 0x022f, NORMAL_CHARACTER),	// ‘o’ -> ‘ȯ’
  DEADTRANS(0x02d9, L'p'  , 0x1e57, NORMAL_CHARACTER),	// ‘p’ -> ‘ṗ’
  DEADTRANS(0x02d9, L'r'  , 0x1e59, NORMAL_CHARACTER),	// ‘r’ -> ‘ṙ’
  DEADTRANS(0x02d9, L's'  , 0x1e61, NORMAL_CHARACTER),	// ‘s’ -> ‘ṡ’
  DEADTRANS(0x02d9, L't'  , 0x1e6b, NORMAL_CHARACTER),	// ‘t’ -> ‘ṫ’
  DEADTRANS(0x02d9, L'w'  , 0x1e87, NORMAL_CHARACTER),	// ‘w’ -> ‘ẇ’
  DEADTRANS(0x02d9, L'x'  , 0x1e8b, NORMAL_CHARACTER),	// ‘x’ -> ‘ẋ’
  DEADTRANS(0x02d9, L'y'  , 0x1e8f, NORMAL_CHARACTER),	// ‘y’ -> ‘ẏ’
  DEADTRANS(0x02d9, L'z'  , 0x017c, NORMAL_CHARACTER),	// ‘z’ -> ‘ż’
  DEADTRANS(0x02d9, 0x00a0, 0x02d9, NORMAL_CHARACTER),	// U+00A0 -> U+02D9
  DEADTRANS(0x02d9, 0x00b7, 0x0307, NORMAL_CHARACTER),	// U+00B7 -> U+0307
  DEADTRANS(0x02d9, 0x00bf, 0x0307, NORMAL_CHARACTER),	// U+00BF -> U+0307
  DEADTRANS(0x02d9, 0x017f, 0x1e9b, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẛ’
  DEADTRANS(0x02d9, 0x2019, 0x0307, NORMAL_CHARACTER),	// U+2019 -> U+0307
  DEADTRANS(0x02d9, 0x2026, 0x0307, NORMAL_CHARACTER),	// U+2026 -> U+0307
  DEADTRANS(0x02d9, 0x202f, 0x02d9, NORMAL_CHARACTER),	// U+202F -> U+02D9
  DEADTRANS(0x02d9, L'.'  , 0x0870, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Dot Above + Dot Below›
  DEADTRANS(0x02d9, L'/'  , 0x0871, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Dot Above + Long Solidus Overlay›
  DEADTRANS(0x02d9, 0x00af, 0x0872, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Dot Above + Macron›
  DEADTRANS(0x02d9, 0x00b4, 0x0873, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Dot Above + Acute Accent›
  DEADTRANS(0x02d9, 0x02c7, 0x0874, CHAINED_DEAD_KEY),	// ‹Caron› -> ‹Dot Above + Caron›
  DEADTRANS(0x02d9, 0x02d9, 0x1e33, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Bépo dot below›
  DEADTRANS(0x02d9, 0x0336, 0x0875, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Dot Above + Long Stroke Overlay›
  DEADTRANS(0x02d9, 0x1e33, 0x0876, CHAINED_DEAD_KEY),	// ‹Bépo dot below› -> ‹Dot Above + Bépo dot below›
  DEADTRANS(0x02d9, 0x1ebb, 0x0307, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0307
  DEADTRANS(0x02d9, L' '  , 0x02d9, NORMAL_CHARACTER),	// U+0020 -> U+02D9

// Dead key: Ring Above
  DEADTRANS(0x02da, L'.'  , 0x030a, NORMAL_CHARACTER),	// U+002E -> U+030A
  DEADTRANS(0x02da, L':'  , 0x030a, NORMAL_CHARACTER),	// U+003A -> U+030A
  DEADTRANS(0x02da, L'?'  , 0x030a, NORMAL_CHARACTER),	// U+003F -> U+030A
  DEADTRANS(0x02da, L'A'  , 0x00c5, NORMAL_CHARACTER),	// ‘A’ -> ‘Å’
  DEADTRANS(0x02da, L'U'  , 0x016e, NORMAL_CHARACTER),	// ‘U’ -> ‘Ů’
  DEADTRANS(0x02da, L'_'  , 0x00b0, NORMAL_CHARACTER),	// U+005F -> U+00B0
  DEADTRANS(0x02da, L'a'  , 0x00e5, NORMAL_CHARACTER),	// ‘a’ -> ‘å’
  DEADTRANS(0x02da, L'u'  , 0x016f, NORMAL_CHARACTER),	// ‘u’ -> ‘ů’
  DEADTRANS(0x02da, L'w'  , 0x1e98, NORMAL_CHARACTER),	// ‘w’ -> ‘ẘ’
  DEADTRANS(0x02da, L'y'  , 0x1e99, NORMAL_CHARACTER),	// ‘y’ -> ‘ẙ’
  DEADTRANS(0x02da, 0x00a0, 0x00b0, NORMAL_CHARACTER),	// U+00A0 -> U+00B0
  DEADTRANS(0x02da, 0x00b7, 0x030a, NORMAL_CHARACTER),	// U+00B7 -> U+030A
  DEADTRANS(0x02da, 0x00bf, 0x030a, NORMAL_CHARACTER),	// U+00BF -> U+030A
  DEADTRANS(0x02da, 0x2019, 0x030a, NORMAL_CHARACTER),	// U+2019 -> U+030A
  DEADTRANS(0x02da, 0x2026, 0x030a, NORMAL_CHARACTER),	// U+2026 -> U+030A
  DEADTRANS(0x02da, 0x202f, 0x00b0, NORMAL_CHARACTER),	// U+202F -> U+00B0
  DEADTRANS(0x02da, 0x00b4, 0x0878, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Ring Above + Acute Accent›
  DEADTRANS(0x02da, 0x02da, 0x02f3, CHAINED_DEAD_KEY),	// ‹Ring Above› -> ‹Ring Below›
  DEADTRANS(0x02da, 0x1ebb, 0x030a, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+030A
  DEADTRANS(0x02da, L' '  , 0x00b0, NORMAL_CHARACTER),	// U+0020 -> U+00B0

// Dead key: Ogonek
  DEADTRANS(0x02db, L'.'  , 0x0328, NORMAL_CHARACTER),	// U+002E -> U+0328
  DEADTRANS(0x02db, L':'  , 0x0328, NORMAL_CHARACTER),	// U+003A -> U+0328
  DEADTRANS(0x02db, L'?'  , 0x0328, NORMAL_CHARACTER),	// U+003F -> U+0328
  DEADTRANS(0x02db, L'A'  , 0x0104, NORMAL_CHARACTER),	// ‘A’ -> ‘Ą’
  DEADTRANS(0x02db, L'E'  , 0x0118, NORMAL_CHARACTER),	// ‘E’ -> ‘Ę’
  DEADTRANS(0x02db, L'I'  , 0x012e, NORMAL_CHARACTER),	// ‘I’ -> ‘Į’
  DEADTRANS(0x02db, L'O'  , 0x01ea, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǫ’
  DEADTRANS(0x02db, L'U'  , 0x0172, NORMAL_CHARACTER),	// ‘U’ -> ‘Ų’
  DEADTRANS(0x02db, L'_'  , 0x02db, NORMAL_CHARACTER),	// U+005F -> U+02DB
  DEADTRANS(0x02db, L'a'  , 0x0105, NORMAL_CHARACTER),	// ‘a’ -> ‘ą’
  DEADTRANS(0x02db, L'e'  , 0x0119, NORMAL_CHARACTER),	// ‘e’ -> ‘ę’
  DEADTRANS(0x02db, L'i'  , 0x012f, NORMAL_CHARACTER),	// ‘i’ -> ‘į’
  DEADTRANS(0x02db, L'o'  , 0x01eb, NORMAL_CHARACTER),	// ‘o’ -> ‘ǫ’
  DEADTRANS(0x02db, L'u'  , 0x0173, NORMAL_CHARACTER),	// ‘u’ -> ‘ų’
  DEADTRANS(0x02db, 0x00a0, 0x02db, NORMAL_CHARACTER),	// U+00A0 -> U+02DB
  DEADTRANS(0x02db, 0x00b7, 0x0328, NORMAL_CHARACTER),	// U+00B7 -> U+0328
  DEADTRANS(0x02db, 0x00bf, 0x0328, NORMAL_CHARACTER),	// U+00BF -> U+0328
  DEADTRANS(0x02db, 0x2019, 0x0328, NORMAL_CHARACTER),	// U+2019 -> U+0328
  DEADTRANS(0x02db, 0x2026, 0x0328, NORMAL_CHARACTER),	// U+2026 -> U+0328
  DEADTRANS(0x02db, 0x202f, 0x02db, NORMAL_CHARACTER),	// U+202F -> U+02DB
  DEADTRANS(0x02db, 0x00af, 0x0879, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Ogonek + Macron›
  DEADTRANS(0x02db, 0x1ebb, 0x0328, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0328
  DEADTRANS(0x02db, L' '  , 0x02db, NORMAL_CHARACTER),	// U+0020 -> U+02DB

// Dead key: Double Acute Accent
  DEADTRANS(0x02dd, L'.'  , 0x030b, NORMAL_CHARACTER),	// U+002E -> U+030B
  DEADTRANS(0x02dd, L':'  , 0x030b, NORMAL_CHARACTER),	// U+003A -> U+030B
  DEADTRANS(0x02dd, L'?'  , 0x030b, NORMAL_CHARACTER),	// U+003F -> U+030B
  DEADTRANS(0x02dd, L'O'  , 0x0150, NORMAL_CHARACTER),	// ‘O’ -> ‘Ő’
  DEADTRANS(0x02dd, L'U'  , 0x0170, NORMAL_CHARACTER),	// ‘U’ -> ‘Ű’
  DEADTRANS(0x02dd, L'_'  , 0x02dd, NORMAL_CHARACTER),	// U+005F -> U+02DD
  DEADTRANS(0x02dd, L'o'  , 0x0151, NORMAL_CHARACTER),	// ‘o’ -> ‘ő’
  DEADTRANS(0x02dd, L'u'  , 0x0171, NORMAL_CHARACTER),	// ‘u’ -> ‘ű’
  DEADTRANS(0x02dd, 0x00a0, 0x02dd, NORMAL_CHARACTER),	// U+00A0 -> U+02DD
  DEADTRANS(0x02dd, 0x00b7, 0x030b, NORMAL_CHARACTER),	// U+00B7 -> U+030B
  DEADTRANS(0x02dd, 0x00bf, 0x030b, NORMAL_CHARACTER),	// U+00BF -> U+030B
  DEADTRANS(0x02dd, 0x2019, 0x030b, NORMAL_CHARACTER),	// U+2019 -> U+030B
  DEADTRANS(0x02dd, 0x2026, 0x030b, NORMAL_CHARACTER),	// U+2026 -> U+030B
  DEADTRANS(0x02dd, 0x202f, 0x02dd, NORMAL_CHARACTER),	// U+202F -> U+02DD
  DEADTRANS(0x02dd, 0x1ebb, 0x030b, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+030B
  DEADTRANS(0x02dd, L' '  , 0x02dd, NORMAL_CHARACTER),	// U+0020 -> U+02DD

// Dead key: Caron Below
  DEADTRANS(0x02ec, L'.'  , 0x032c, NORMAL_CHARACTER),	// U+002E -> U+032C
  DEADTRANS(0x02ec, L':'  , 0x032c, NORMAL_CHARACTER),	// U+003A -> U+032C
  DEADTRANS(0x02ec, L'?'  , 0x032c, NORMAL_CHARACTER),	// U+003F -> U+032C
  DEADTRANS(0x02ec, 0x00b7, 0x032c, NORMAL_CHARACTER),	// U+00B7 -> U+032C
  DEADTRANS(0x02ec, 0x00bf, 0x032c, NORMAL_CHARACTER),	// U+00BF -> U+032C
  DEADTRANS(0x02ec, 0x2019, 0x032c, NORMAL_CHARACTER),	// U+2019 -> U+032C
  DEADTRANS(0x02ec, 0x2026, 0x032c, NORMAL_CHARACTER),	// U+2026 -> U+032C
  DEADTRANS(0x02ec, 0x1ebb, 0x032c, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+032C
  DEADTRANS(0x02ec, L' '  , 0x02ec, NORMAL_CHARACTER),	// U+0020 -> ‘ˬ’

// Dead key: Ring Below
  DEADTRANS(0x02f3, L'.'  , 0x0325, NORMAL_CHARACTER),	// U+002E -> U+0325
  DEADTRANS(0x02f3, L':'  , 0x0325, NORMAL_CHARACTER),	// U+003A -> U+0325
  DEADTRANS(0x02f3, L'?'  , 0x0325, NORMAL_CHARACTER),	// U+003F -> U+0325
  DEADTRANS(0x02f3, L'A'  , 0x1e00, NORMAL_CHARACTER),	// ‘A’ -> ‘Ḁ’
  DEADTRANS(0x02f3, L'a'  , 0x1e01, NORMAL_CHARACTER),	// ‘a’ -> ‘ḁ’
  DEADTRANS(0x02f3, 0x00b7, 0x0325, NORMAL_CHARACTER),	// U+00B7 -> U+0325
  DEADTRANS(0x02f3, 0x00bf, 0x0325, NORMAL_CHARACTER),	// U+00BF -> U+0325
  DEADTRANS(0x02f3, 0x2019, 0x0325, NORMAL_CHARACTER),	// U+2019 -> U+0325
  DEADTRANS(0x02f3, 0x2026, 0x0325, NORMAL_CHARACTER),	// U+2026 -> U+0325
  DEADTRANS(0x02f3, 0x1ebb, 0x0325, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0325
  DEADTRANS(0x02f3, L' '  , 0x02f3, NORMAL_CHARACTER),	// U+0020 -> U+02F3

// Dead key: Double Grave Accent
  DEADTRANS(0x030f, L'.'  , 0x030f, NORMAL_CHARACTER),	// U+002E -> U+030F
  DEADTRANS(0x030f, L':'  , 0x030f, NORMAL_CHARACTER),	// U+003A -> U+030F
  DEADTRANS(0x030f, L'?'  , 0x030f, NORMAL_CHARACTER),	// U+003F -> U+030F
  DEADTRANS(0x030f, L'A'  , 0x0200, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȁ’
  DEADTRANS(0x030f, L'E'  , 0x0204, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȅ’
  DEADTRANS(0x030f, L'I'  , 0x0208, NORMAL_CHARACTER),	// ‘I’ -> ‘Ȉ’
  DEADTRANS(0x030f, L'O'  , 0x020c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȍ’
  DEADTRANS(0x030f, L'R'  , 0x0210, NORMAL_CHARACTER),	// ‘R’ -> ‘Ȑ’
  DEADTRANS(0x030f, L'U'  , 0x0214, NORMAL_CHARACTER),	// ‘U’ -> ‘Ȕ’
  DEADTRANS(0x030f, L'a'  , 0x0201, NORMAL_CHARACTER),	// ‘a’ -> ‘ȁ’
  DEADTRANS(0x030f, L'e'  , 0x0205, NORMAL_CHARACTER),	// ‘e’ -> ‘ȅ’
  DEADTRANS(0x030f, L'i'  , 0x0209, NORMAL_CHARACTER),	// ‘i’ -> ‘ȉ’
  DEADTRANS(0x030f, L'o'  , 0x020d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȍ’
  DEADTRANS(0x030f, L'r'  , 0x0211, NORMAL_CHARACTER),	// ‘r’ -> ‘ȑ’
  DEADTRANS(0x030f, L'u'  , 0x0215, NORMAL_CHARACTER),	// ‘u’ -> ‘ȕ’
  DEADTRANS(0x030f, 0x00b7, 0x030f, NORMAL_CHARACTER),	// U+00B7 -> U+030F
  DEADTRANS(0x030f, 0x00bf, 0x030f, NORMAL_CHARACTER),	// U+00BF -> U+030F
  DEADTRANS(0x030f, 0x2019, 0x030f, NORMAL_CHARACTER),	// U+2019 -> U+030F
  DEADTRANS(0x030f, 0x2026, 0x030f, NORMAL_CHARACTER),	// U+2026 -> U+030F
  DEADTRANS(0x030f, 0x1ebb, 0x030f, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+030F
  DEADTRANS(0x030f, L' '  , 0x030f, NORMAL_CHARACTER),	// U+0020 -> U+030F

// Dead key: Inverted Breve
  DEADTRANS(0x0311, L'.'  , 0x0311, NORMAL_CHARACTER),	// U+002E -> U+0311
  DEADTRANS(0x0311, L':'  , 0x0311, NORMAL_CHARACTER),	// U+003A -> U+0311
  DEADTRANS(0x0311, L'?'  , 0x032e, NORMAL_CHARACTER),	// U+003F -> U+032E
  DEADTRANS(0x0311, L'A'  , 0x0202, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȃ’
  DEADTRANS(0x0311, L'E'  , 0x0206, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȇ’
  DEADTRANS(0x0311, L'H'  , 0x1e2a, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḫ’
  DEADTRANS(0x0311, L'I'  , 0x020a, NORMAL_CHARACTER),	// ‘I’ -> ‘Ȋ’
  DEADTRANS(0x0311, L'O'  , 0x020e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȏ’
  DEADTRANS(0x0311, L'R'  , 0x0212, NORMAL_CHARACTER),	// ‘R’ -> ‘Ȓ’
  DEADTRANS(0x0311, L'U'  , 0x0216, NORMAL_CHARACTER),	// ‘U’ -> ‘Ȗ’
  DEADTRANS(0x0311, L'a'  , 0x0203, NORMAL_CHARACTER),	// ‘a’ -> ‘ȃ’
  DEADTRANS(0x0311, L'e'  , 0x0207, NORMAL_CHARACTER),	// ‘e’ -> ‘ȇ’
  DEADTRANS(0x0311, L'h'  , 0x1e2b, NORMAL_CHARACTER),	// ‘h’ -> ‘ḫ’
  DEADTRANS(0x0311, L'i'  , 0x020b, NORMAL_CHARACTER),	// ‘i’ -> ‘ȋ’
  DEADTRANS(0x0311, L'o'  , 0x020f, NORMAL_CHARACTER),	// ‘o’ -> ‘ȏ’
  DEADTRANS(0x0311, L'r'  , 0x0213, NORMAL_CHARACTER),	// ‘r’ -> ‘ȓ’
  DEADTRANS(0x0311, L'u'  , 0x0217, NORMAL_CHARACTER),	// ‘u’ -> ‘ȗ’
  DEADTRANS(0x0311, 0x00b7, 0x0311, NORMAL_CHARACTER),	// U+00B7 -> U+0311
  DEADTRANS(0x0311, 0x00bf, 0x032e, NORMAL_CHARACTER),	// U+00BF -> U+032E
  DEADTRANS(0x0311, 0x2019, 0x032e, NORMAL_CHARACTER),	// U+2019 -> U+032E
  DEADTRANS(0x0311, 0x2026, 0x0311, NORMAL_CHARACTER),	// U+2026 -> U+0311
  DEADTRANS(0x0311, 0x1ebb, 0x032e, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+032E
  DEADTRANS(0x0311, L' '  , 0x0311, NORMAL_CHARACTER),	// U+0020 -> U+0311

// Dead key: Horn
  DEADTRANS(0x031b, L'.'  , 0x031b, NORMAL_CHARACTER),	// U+002E -> U+031B
  DEADTRANS(0x031b, L':'  , 0x031b, NORMAL_CHARACTER),	// U+003A -> U+031B
  DEADTRANS(0x031b, L'?'  , 0x031b, NORMAL_CHARACTER),	// U+003F -> U+031B
  DEADTRANS(0x031b, L'O'  , 0x01a0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ơ’
  DEADTRANS(0x031b, L'U'  , 0x01af, NORMAL_CHARACTER),	// ‘U’ -> ‘Ư’
  DEADTRANS(0x031b, L'o'  , 0x01a1, NORMAL_CHARACTER),	// ‘o’ -> ‘ơ’
  DEADTRANS(0x031b, L'u'  , 0x01b0, NORMAL_CHARACTER),	// ‘u’ -> ‘ư’
  DEADTRANS(0x031b, 0x00b7, 0x031b, NORMAL_CHARACTER),	// U+00B7 -> U+031B
  DEADTRANS(0x031b, 0x00bf, 0x031b, NORMAL_CHARACTER),	// U+00BF -> U+031B
  DEADTRANS(0x031b, 0x00d9, 0x1eea, NORMAL_CHARACTER),	// ‘Ù’ -> ‘Ừ’
  DEADTRANS(0x031b, 0x00f9, 0x1eeb, NORMAL_CHARACTER),	// ‘ù’ -> ‘ừ’
  DEADTRANS(0x031b, 0x2019, 0x031b, NORMAL_CHARACTER),	// U+2019 -> U+031B
  DEADTRANS(0x031b, 0x2026, 0x031b, NORMAL_CHARACTER),	// U+2026 -> U+031B
  DEADTRANS(0x031b, L'.'  , 0x087a, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Horn + Dot Below›
  DEADTRANS(0x031b, L'`'  , 0x087b, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Horn + Grave Accent›
  DEADTRANS(0x031b, L'~'  , 0x087c, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Horn + Tilde›
  DEADTRANS(0x031b, 0x00b4, 0x087d, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Horn + Acute Accent›
  DEADTRANS(0x031b, 0x1e33, 0x087e, CHAINED_DEAD_KEY),	// ‹Bépo dot below› -> ‹Horn + Bépo dot below›
  DEADTRANS(0x031b, 0x1ebb, 0x031b, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+031B
  DEADTRANS(0x031b, L' '  , 0x031b, NORMAL_CHARACTER),	// U+0020 -> U+031B

// Dead key: Long Stroke Overlay
  DEADTRANS(0x0336, L'&'  , 0xa751, NORMAL_CHARACTER),	// U+0026 -> ‘ꝑ’
  DEADTRANS(0x0336, L'.'  , 0x0335, NORMAL_CHARACTER),	// U+002E -> U+0335
  DEADTRANS(0x0336, L'2'  , 0x01bb, NORMAL_CHARACTER),	// ‘2’ -> ‘ƻ’
  DEADTRANS(0x0336, L':'  , 0x0335, NORMAL_CHARACTER),	// U+003A -> U+0335
  DEADTRANS(0x0336, L'?'  , 0x0336, NORMAL_CHARACTER),	// U+003F -> U+0336
  DEADTRANS(0x0336, L'B'  , 0x0243, NORMAL_CHARACTER),	// ‘B’ -> ‘Ƀ’
  DEADTRANS(0x0336, L'C'  , 0xa792, NORMAL_CHARACTER),	// ‘C’ -> ‘Ꞓ’
  DEADTRANS(0x0336, L'D'  , 0x0110, NORMAL_CHARACTER),	// ‘D’ -> ‘Đ’
  DEADTRANS(0x0336, L'F'  , 0xa798, NORMAL_CHARACTER),	// ‘F’ -> ‘Ꞙ’
  DEADTRANS(0x0336, L'G'  , 0x01e4, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǥ’
  DEADTRANS(0x0336, L'H'  , 0x0126, NORMAL_CHARACTER),	// ‘H’ -> ‘Ħ’
  DEADTRANS(0x0336, L'I'  , 0x0197, NORMAL_CHARACTER),	// ‘I’ -> ‘Ɨ’
  DEADTRANS(0x0336, L'J'  , 0x0248, NORMAL_CHARACTER),	// ‘J’ -> ‘Ɉ’
  DEADTRANS(0x0336, L'K'  , 0xa740, NORMAL_CHARACTER),	// ‘K’ -> ‘Ꝁ’
  DEADTRANS(0x0336, L'L'  , 0x023d, NORMAL_CHARACTER),	// ‘L’ -> ‘Ƚ’
  DEADTRANS(0x0336, L'O'  , 0xa74a, NORMAL_CHARACTER),	// ‘O’ -> ‘Ꝋ’
  DEADTRANS(0x0336, L'P'  , 0x2c63, NORMAL_CHARACTER),	// ‘P’ -> ‘Ᵽ’
  DEADTRANS(0x0336, L'Q'  , 0xa756, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ꝗ’
  DEADTRANS(0x0336, L'R'  , 0x024c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ɍ’
  DEADTRANS(0x0336, L'T'  , 0x0166, NORMAL_CHARACTER),	// ‘T’ -> ‘Ŧ’
  DEADTRANS(0x0336, L'U'  , 0x0244, NORMAL_CHARACTER),	// ‘U’ -> ‘Ʉ’
  DEADTRANS(0x0336, L'Y'  , 0x024e, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ɏ’
  DEADTRANS(0x0336, L'Z'  , 0x01b5, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ƶ’
  DEADTRANS(0x0336, L'_'  , 0x2013, NORMAL_CHARACTER),	// U+005F -> U+2013
  DEADTRANS(0x0336, L'b'  , 0x0180, NORMAL_CHARACTER),	// ‘b’ -> ‘ƀ’
  DEADTRANS(0x0336, L'c'  , 0xa793, NORMAL_CHARACTER),	// ‘c’ -> ‘ꞓ’
  DEADTRANS(0x0336, L'd'  , 0x0111, NORMAL_CHARACTER),	// ‘d’ -> ‘đ’
  DEADTRANS(0x0336, L'f'  , 0xa799, NORMAL_CHARACTER),	// ‘f’ -> ‘ꞙ’
  DEADTRANS(0x0336, L'g'  , 0x01e5, NORMAL_CHARACTER),	// ‘g’ -> ‘ǥ’
  DEADTRANS(0x0336, L'h'  , 0x0127, NORMAL_CHARACTER),	// ‘h’ -> ‘ħ’
  DEADTRANS(0x0336, L'i'  , 0x0268, NORMAL_CHARACTER),	// ‘i’ -> ‘ɨ’
  DEADTRANS(0x0336, L'j'  , 0x0249, NORMAL_CHARACTER),	// ‘j’ -> ‘ɉ’
  DEADTRANS(0x0336, L'k'  , 0xa741, NORMAL_CHARACTER),	// ‘k’ -> ‘ꝁ’
  DEADTRANS(0x0336, L'l'  , 0x019a, NORMAL_CHARACTER),	// ‘l’ -> ‘ƚ’
  DEADTRANS(0x0336, L'o'  , 0xa74b, NORMAL_CHARACTER),	// ‘o’ -> ‘ꝋ’
  DEADTRANS(0x0336, L'p'  , 0x1d7d, NORMAL_CHARACTER),	// ‘p’ -> ‘ᵽ’
  DEADTRANS(0x0336, L'q'  , 0xa757, NORMAL_CHARACTER),	// ‘q’ -> ‘ꝗ’
  DEADTRANS(0x0336, L'r'  , 0x024d, NORMAL_CHARACTER),	// ‘r’ -> ‘ɍ’
  DEADTRANS(0x0336, L't'  , 0x0167, NORMAL_CHARACTER),	// ‘t’ -> ‘ŧ’
  DEADTRANS(0x0336, L'u'  , 0x0289, NORMAL_CHARACTER),	// ‘u’ -> ‘ʉ’
  DEADTRANS(0x0336, L'y'  , 0x024f, NORMAL_CHARACTER),	// ‘y’ -> ‘ɏ’
  DEADTRANS(0x0336, L'z'  , 0x01b6, NORMAL_CHARACTER),	// ‘z’ -> ‘ƶ’
  DEADTRANS(0x0336, 0x00a0, 0x2013, NORMAL_CHARACTER),	// U+00A0 -> U+2013
  DEADTRANS(0x0336, 0x00a3, 0xa748, NORMAL_CHARACTER),	// U+00A3 -> ‘Ꝉ’
  DEADTRANS(0x0336, 0x00a7, 0xa750, NORMAL_CHARACTER),	// U+00A7 -> ‘Ꝑ’
  DEADTRANS(0x0336, 0x00b7, 0x0335, NORMAL_CHARACTER),	// U+00B7 -> U+0335
  DEADTRANS(0x0336, 0x00bf, 0x0336, NORMAL_CHARACTER),	// U+00BF -> U+0336
  DEADTRANS(0x0336, 0x0152, 0x019f, NORMAL_CHARACTER),	// ‘Œ’ -> ‘Ɵ’
  DEADTRANS(0x0336, 0x0153, 0x0275, NORMAL_CHARACTER),	// ‘œ’ -> ‘ɵ’
  DEADTRANS(0x0336, 0x017f, 0x1e9d, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẝ’
  DEADTRANS(0x0336, 0x2019, 0x0336, NORMAL_CHARACTER),	// U+2019 -> U+0336
  DEADTRANS(0x0336, 0x2026, 0x0335, NORMAL_CHARACTER),	// U+2026 -> U+0335
  DEADTRANS(0x0336, 0x202f, 0x2013, NORMAL_CHARACTER),	// U+202F -> U+2013
  DEADTRANS(0x0336, L'/'  , 0x087f, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Long Stroke Overlay + Long Solidus Overlay›
  DEADTRANS(0x0336, 0x00df, 0x0880, CHAINED_DEAD_KEY),	// ‹Bépo Latin› -> ‹Long Stroke Overlay + Bépo Latin›
  DEADTRANS(0x0336, 0x02d9, 0x0881, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Long Stroke Overlay + Dot Above›
  DEADTRANS(0x0336, 0x0336, 0x2550, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Doubled Long Stroke Overlay›
  DEADTRANS(0x0336, 0x1d62, 0x0882, CHAINED_DEAD_KEY),	// ‹Subscript› -> ‹Long Stroke Overlay + Subscript›
  DEADTRANS(0x0336, 0x1ebb, 0x0336, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0336
  DEADTRANS(0x0336, L' '  , 0x2013, NORMAL_CHARACTER),	// U+0020 -> U+2013

// Dead key: Cedilla + Acute Accent
  DEADTRANS(0x0378, L'C'  , 0x1e08, NORMAL_CHARACTER),	// ‘C’ -> ‘Ḉ’
  DEADTRANS(0x0378, L'c'  , 0x1e09, NORMAL_CHARACTER),	// ‘c’ -> ‘ḉ’
  DEADTRANS(0x0378, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Cedilla + Breve
  DEADTRANS(0x0379, L'E'  , 0x1e1c, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḝ’
  DEADTRANS(0x0379, L'e'  , 0x1e1d, NORMAL_CHARACTER),	// ‘e’ -> ‘ḝ’
  DEADTRANS(0x0379, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Macron
  DEADTRANS(0x0380, L'L'  , 0x1e38, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḹ’
  DEADTRANS(0x0380, L'R'  , 0x1e5c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṝ’
  DEADTRANS(0x0380, L'l'  , 0x1e39, NORMAL_CHARACTER),	// ‘l’ -> ‘ḹ’
  DEADTRANS(0x0380, L'r'  , 0x1e5d, NORMAL_CHARACTER),	// ‘r’ -> ‘ṝ’
  DEADTRANS(0x0380, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Circumflex Accent
  DEADTRANS(0x0381, L'A'  , 0x1eac, NORMAL_CHARACTER),	// ‘A’ -> ‘Ậ’
  DEADTRANS(0x0381, L'E'  , 0x1ec6, NORMAL_CHARACTER),	// ‘E’ -> ‘Ệ’
  DEADTRANS(0x0381, L'O'  , 0x1ed8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ộ’
  DEADTRANS(0x0381, L'a'  , 0x1ead, NORMAL_CHARACTER),	// ‘a’ -> ‘ậ’
  DEADTRANS(0x0381, L'e'  , 0x1ec7, NORMAL_CHARACTER),	// ‘e’ -> ‘ệ’
  DEADTRANS(0x0381, L'o'  , 0x1ed9, NORMAL_CHARACTER),	// ‘o’ -> ‘ộ’
  DEADTRANS(0x0381, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Breve
  DEADTRANS(0x0382, L'A'  , 0x1eb6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ặ’
  DEADTRANS(0x0382, L'a'  , 0x1eb7, NORMAL_CHARACTER),	// ‘a’ -> ‘ặ’
  DEADTRANS(0x0382, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Dot Above
  DEADTRANS(0x0383, L'S'  , 0x1e68, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṩ’
  DEADTRANS(0x0383, L's'  , 0x1e69, NORMAL_CHARACTER),	// ‘s’ -> ‘ṩ’
  DEADTRANS(0x0383, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Horn
  DEADTRANS(0x038b, L'O'  , 0x1ee2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ợ’
  DEADTRANS(0x038b, L'U'  , 0x1ef0, NORMAL_CHARACTER),	// ‘U’ -> ‘Ự’
  DEADTRANS(0x038b, L'o'  , 0x1ee3, NORMAL_CHARACTER),	// ‘o’ -> ‘ợ’
  DEADTRANS(0x038b, L'u'  , 0x1ef1, NORMAL_CHARACTER),	// ‘u’ -> ‘ự’
  DEADTRANS(0x038b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Acute Accent
  DEADTRANS(0x038d, L'O'  , 0x01fe, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǿ’
  DEADTRANS(0x038d, L'o'  , 0x01ff, NORMAL_CHARACTER),	// ‘o’ -> ‘ǿ’
  DEADTRANS(0x038d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Greek
  DEADTRANS(0x03a2, L'r'  , 0x03fc, NORMAL_CHARACTER),	// ‘r’ -> ‘ϼ’
  DEADTRANS(0x03a2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Bépo Latin
  DEADTRANS(0x0530, L'o'  , 0xab3f, NORMAL_CHARACTER),	// ‘o’ -> ‘ꬿ’
  DEADTRANS(0x0530, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Dot Above
  DEADTRANS(0x0557, L'j'  , 0x025f, NORMAL_CHARACTER),	// ‘j’ -> ‘ɟ’
  DEADTRANS(0x0557, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Long Stroke Overlay
  DEADTRANS(0x0558, L'K'  , 0xa744, NORMAL_CHARACTER),	// ‘K’ -> ‘Ꝅ’
  DEADTRANS(0x0558, L'k'  , 0xa745, NORMAL_CHARACTER),	// ‘k’ -> ‘ꝅ’
  DEADTRANS(0x0558, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Subscript
  DEADTRANS(0x058b, L'L'  , 0x1d0c, NORMAL_CHARACTER),	// ‘L’ -> ‘ᴌ’
  DEADTRANS(0x058b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Bépo Science
  DEADTRANS(0x058c, L'\'' , 0x2285, NORMAL_CHARACTER),	// U+0027 -> U+2285
  DEADTRANS(0x058c, L','  , 0x220c, NORMAL_CHARACTER),	// U+002C -> U+220C
  DEADTRANS(0x058c, L'<'  , 0x2280, NORMAL_CHARACTER),	// U+003C -> U+2280
  DEADTRANS(0x058c, L'>'  , 0x2281, NORMAL_CHARACTER),	// U+003E -> U+2281
  DEADTRANS(0x058c, L'E'  , 0x2204, NORMAL_CHARACTER),	// ‘E’ -> U+2204
  DEADTRANS(0x058c, L'Q'  , 0x2226, NORMAL_CHARACTER),	// ‘Q’ -> U+2226
  DEADTRANS(0x058c, L'e'  , 0x2209, NORMAL_CHARACTER),	// ‘e’ -> U+2209
  DEADTRANS(0x058c, L'k'  , 0x2241, NORMAL_CHARACTER),	// ‘k’ -> U+2241
  DEADTRANS(0x058c, L'v'  , 0x22bd, NORMAL_CHARACTER),	// ‘v’ -> U+22BD
  DEADTRANS(0x058c, L'|'  , 0x2224, NORMAL_CHARACTER),	// U+007C -> U+2224
  DEADTRANS(0x058c, 0x00e9, 0x22bc, NORMAL_CHARACTER),	// ‘é’ -> U+22BC
  DEADTRANS(0x058c, 0x20ac, 0x2284, NORMAL_CHARACTER),	// U+20AC -> U+2284
  DEADTRANS(0x058c, 0x2122, 0x22ad, NORMAL_CHARACTER),	// U+2122 -> U+22AD
  DEADTRANS(0x058c, 0x2260, 0x2262, NORMAL_CHARACTER),	// U+2260 -> U+2262
  DEADTRANS(0x058c, 0x2623, 0x22ea, NORMAL_CHARACTER),	// U+2623 -> U+22EA
  DEADTRANS(0x058c, L';'  , 0x228b, NORMAL_CHARACTER),	// ‹Comma Below› -> U+228B
  DEADTRANS(0x058c, 0x00a4, 0x228a, NORMAL_CHARACTER),	// ‹Bépo Currency› -> U+228A
  DEADTRANS(0x058c, 0x1d49, 0x22ac, NORMAL_CHARACTER),	// ‹Superscript› -> U+22AC
  DEADTRANS(0x058c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Grave Accent
  DEADTRANS(0x0590, L'U'  , 0x01db, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǜ’
  DEADTRANS(0x0590, L'u'  , 0x01dc, NORMAL_CHARACTER),	// ‘u’ -> ‘ǜ’
  DEADTRANS(0x0590, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Tilde
  DEADTRANS(0x05c8, L'O'  , 0x1e4e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṏ’
  DEADTRANS(0x05c8, L'o'  , 0x1e4f, NORMAL_CHARACTER),	// ‘o’ -> ‘ṏ’
  DEADTRANS(0x05c8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Macron
  DEADTRANS(0x05c9, L'A'  , 0x01de, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǟ’
  DEADTRANS(0x05c9, L'O'  , 0x022a, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȫ’
  DEADTRANS(0x05c9, L'U'  , 0x1e7a, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṻ’
  DEADTRANS(0x05c9, L'a'  , 0x01df, NORMAL_CHARACTER),	// ‘a’ -> ‘ǟ’
  DEADTRANS(0x05c9, L'o'  , 0x022b, NORMAL_CHARACTER),	// ‘o’ -> ‘ȫ’
  DEADTRANS(0x05c9, L'u'  , 0x1e7b, NORMAL_CHARACTER),	// ‘u’ -> ‘ṻ’
  DEADTRANS(0x05c9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Acute Accent
  DEADTRANS(0x05ca, L'I'  , 0x1e2e, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḯ’
  DEADTRANS(0x05ca, L'U'  , 0x01d7, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǘ’
  DEADTRANS(0x05ca, L'i'  , 0x1e2f, NORMAL_CHARACTER),	// ‘i’ -> ‘ḯ’
  DEADTRANS(0x05ca, L'u'  , 0x01d8, NORMAL_CHARACTER),	// ‘u’ -> ‘ǘ’
  DEADTRANS(0x05ca, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Caron
  DEADTRANS(0x05cb, L'U'  , 0x01d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǚ’
  DEADTRANS(0x05cb, L'u'  , 0x01da, NORMAL_CHARACTER),	// ‘u’ -> ‘ǚ’
  DEADTRANS(0x05cb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Diaeresis
  DEADTRANS(0x05cc, L'U'  , 0x01db, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǜ’
  DEADTRANS(0x05cc, L'u'  , 0x01dc, NORMAL_CHARACTER),	// ‘u’ -> ‘ǜ’
  DEADTRANS(0x05cc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Macron
  DEADTRANS(0x05cd, L'E'  , 0x1e14, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḕ’
  DEADTRANS(0x05cd, L'O'  , 0x1e50, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṑ’
  DEADTRANS(0x05cd, L'e'  , 0x1e15, NORMAL_CHARACTER),	// ‘e’ -> ‘ḕ’
  DEADTRANS(0x05cd, L'o'  , 0x1e51, NORMAL_CHARACTER),	// ‘o’ -> ‘ṑ’
  DEADTRANS(0x05cd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Circumflex Accent
  DEADTRANS(0x05ce, L'A'  , 0x1ea6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ầ’
  DEADTRANS(0x05ce, L'E'  , 0x1ec0, NORMAL_CHARACTER),	// ‘E’ -> ‘Ề’
  DEADTRANS(0x05ce, L'O'  , 0x1ed2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ồ’
  DEADTRANS(0x05ce, L'a'  , 0x1ea7, NORMAL_CHARACTER),	// ‘a’ -> ‘ầ’
  DEADTRANS(0x05ce, L'e'  , 0x1ec1, NORMAL_CHARACTER),	// ‘e’ -> ‘ề’
  DEADTRANS(0x05ce, L'o'  , 0x1ed3, NORMAL_CHARACTER),	// ‘o’ -> ‘ồ’
  DEADTRANS(0x05ce, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Breve
  DEADTRANS(0x05cf, L'A'  , 0x1eb0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ằ’
  DEADTRANS(0x05cf, L'a'  , 0x1eb1, NORMAL_CHARACTER),	// ‘a’ -> ‘ằ’
  DEADTRANS(0x05cf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Horn
  DEADTRANS(0x05eb, L'O'  , 0x1edc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ờ’
  DEADTRANS(0x05eb, L'U'  , 0x1eea, NORMAL_CHARACTER),	// ‘U’ -> ‘Ừ’
  DEADTRANS(0x05eb, L'o'  , 0x1edd, NORMAL_CHARACTER),	// ‘o’ -> ‘ờ’
  DEADTRANS(0x05eb, L'u'  , 0x1eeb, NORMAL_CHARACTER),	// ‘u’ -> ‘ừ’
  DEADTRANS(0x05eb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Diaeresis
  DEADTRANS(0x05ec, L'O'  , 0x1e4e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṏ’
  DEADTRANS(0x05ec, L'o'  , 0x1e4f, NORMAL_CHARACTER),	// ‘o’ -> ‘ṏ’
  DEADTRANS(0x05ec, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Macron
  DEADTRANS(0x05ed, L'O'  , 0x022c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȭ’
  DEADTRANS(0x05ed, L'o'  , 0x022d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȭ’
  DEADTRANS(0x05ed, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Acute Accent
  DEADTRANS(0x05ee, L'O'  , 0x1e4c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṍ’
  DEADTRANS(0x05ee, L'U'  , 0x1e78, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṹ’
  DEADTRANS(0x05ee, L'o'  , 0x1e4d, NORMAL_CHARACTER),	// ‘o’ -> ‘ṍ’
  DEADTRANS(0x05ee, L'u'  , 0x1e79, NORMAL_CHARACTER),	// ‘u’ -> ‘ṹ’
  DEADTRANS(0x05ee, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Circumflex Accent
  DEADTRANS(0x05f5, L'A'  , 0x1eaa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẫ’
  DEADTRANS(0x05f5, L'E'  , 0x1ec4, NORMAL_CHARACTER),	// ‘E’ -> ‘Ễ’
  DEADTRANS(0x05f5, L'O'  , 0x1ed6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỗ’
  DEADTRANS(0x05f5, L'a'  , 0x1eab, NORMAL_CHARACTER),	// ‘a’ -> ‘ẫ’
  DEADTRANS(0x05f5, L'e'  , 0x1ec5, NORMAL_CHARACTER),	// ‘e’ -> ‘ễ’
  DEADTRANS(0x05f5, L'o'  , 0x1ed7, NORMAL_CHARACTER),	// ‘o’ -> ‘ỗ’
  DEADTRANS(0x05f5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Breve
  DEADTRANS(0x05f6, L'A'  , 0x1eb4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẵ’
  DEADTRANS(0x05f6, L'a'  , 0x1eb5, NORMAL_CHARACTER),	// ‘a’ -> ‘ẵ’
  DEADTRANS(0x05f6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Horn
  DEADTRANS(0x05f7, L'O'  , 0x1ee0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỡ’
  DEADTRANS(0x05f7, L'U'  , 0x1eee, NORMAL_CHARACTER),	// ‘U’ -> ‘Ữ’
  DEADTRANS(0x05f7, L'o'  , 0x1ee1, NORMAL_CHARACTER),	// ‘o’ -> ‘ỡ’
  DEADTRANS(0x05f7, L'u'  , 0x1eef, NORMAL_CHARACTER),	// ‘u’ -> ‘ữ’
  DEADTRANS(0x05f7, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Dot Below
  DEADTRANS(0x05f8, L'L'  , 0x1e38, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḹ’
  DEADTRANS(0x05f8, L'R'  , 0x1e5c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṝ’
  DEADTRANS(0x05f8, L'l'  , 0x1e39, NORMAL_CHARACTER),	// ‘l’ -> ‘ḹ’
  DEADTRANS(0x05f8, L'r'  , 0x1e5d, NORMAL_CHARACTER),	// ‘r’ -> ‘ṝ’
  DEADTRANS(0x05f8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Diaeresis
  DEADTRANS(0x05f9, L'A'  , 0x01de, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǟ’
  DEADTRANS(0x05f9, L'O'  , 0x022a, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȫ’
  DEADTRANS(0x05f9, L'U'  , 0x01d5, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǖ’
  DEADTRANS(0x05f9, L'a'  , 0x01df, NORMAL_CHARACTER),	// ‘a’ -> ‘ǟ’
  DEADTRANS(0x05f9, L'o'  , 0x022b, NORMAL_CHARACTER),	// ‘o’ -> ‘ȫ’
  DEADTRANS(0x05f9, L'u'  , 0x01d6, NORMAL_CHARACTER),	// ‘u’ -> ‘ǖ’
  DEADTRANS(0x05f9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Grave Accent
  DEADTRANS(0x05fa, L'E'  , 0x1e14, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḕ’
  DEADTRANS(0x05fa, L'O'  , 0x1e50, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṑ’
  DEADTRANS(0x05fa, L'e'  , 0x1e15, NORMAL_CHARACTER),	// ‘e’ -> ‘ḕ’
  DEADTRANS(0x05fa, L'o'  , 0x1e51, NORMAL_CHARACTER),	// ‘o’ -> ‘ṑ’
  DEADTRANS(0x05fa, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Tilde
  DEADTRANS(0x05fb, L'O'  , 0x022c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȭ’
  DEADTRANS(0x05fb, L'o'  , 0x022d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȭ’
  DEADTRANS(0x05fb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Acute Accent
  DEADTRANS(0x05fc, L'E'  , 0x1e16, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḗ’
  DEADTRANS(0x05fc, L'O'  , 0x1e52, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṓ’
  DEADTRANS(0x05fc, L'e'  , 0x1e17, NORMAL_CHARACTER),	// ‘e’ -> ‘ḗ’
  DEADTRANS(0x05fc, L'o'  , 0x1e53, NORMAL_CHARACTER),	// ‘o’ -> ‘ṓ’
  DEADTRANS(0x05fc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Greek
  DEADTRANS(0x05fd, L'A'  , 0x1fb9, NORMAL_CHARACTER),	// ‘A’ -> ‘Ᾱ’
  DEADTRANS(0x05fd, L'I'  , 0x1fd9, NORMAL_CHARACTER),	// ‘I’ -> ‘Ῑ’
  DEADTRANS(0x05fd, L'U'  , 0x1fe9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ῡ’
  DEADTRANS(0x05fd, L'a'  , 0x1fb1, NORMAL_CHARACTER),	// ‘a’ -> ‘ᾱ’
  DEADTRANS(0x05fd, L'i'  , 0x1fd1, NORMAL_CHARACTER),	// ‘i’ -> ‘ῑ’
  DEADTRANS(0x05fd, L'u'  , 0x1fe1, NORMAL_CHARACTER),	// ‘u’ -> ‘ῡ’
  DEADTRANS(0x05fd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Dot Above
  DEADTRANS(0x05fe, L'A'  , 0x01e0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǡ’
  DEADTRANS(0x05fe, L'O'  , 0x0230, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȱ’
  DEADTRANS(0x05fe, L'a'  , 0x01e1, NORMAL_CHARACTER),	// ‘a’ -> ‘ǡ’
  DEADTRANS(0x05fe, L'o'  , 0x0231, NORMAL_CHARACTER),	// ‘o’ -> ‘ȱ’
  DEADTRANS(0x05fe, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Ogonek
  DEADTRANS(0x05ff, L'O'  , 0x01ec, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǭ’
  DEADTRANS(0x05ff, L'o'  , 0x01ed, NORMAL_CHARACTER),	// ‘o’ -> ‘ǭ’
  DEADTRANS(0x05ff, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Bépo dot below
  DEADTRANS(0x061d, L'L'  , 0x1e38, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḹ’
  DEADTRANS(0x061d, L'R'  , 0x1e5c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṝ’
  DEADTRANS(0x061d, L'l'  , 0x1e39, NORMAL_CHARACTER),	// ‘l’ -> ‘ḹ’
  DEADTRANS(0x061d, L'r'  , 0x1e5d, NORMAL_CHARACTER),	// ‘r’ -> ‘ṝ’
  DEADTRANS(0x061d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Bépo Science
  DEADTRANS(0x070e, L'v'  , 0x22bd, NORMAL_CHARACTER),	// ‘v’ -> U+22BD
  DEADTRANS(0x070e, 0x00e9, 0x22bc, NORMAL_CHARACTER),	// ‘é’ -> U+22BC
  DEADTRANS(0x070e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Cedilla
  DEADTRANS(0x074b, L'C'  , 0x1e08, NORMAL_CHARACTER),	// ‘C’ -> ‘Ḉ’
  DEADTRANS(0x074b, L'c'  , 0x1e09, NORMAL_CHARACTER),	// ‘c’ -> ‘ḉ’
  DEADTRANS(0x074b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Long Solidus Overlay
  DEADTRANS(0x074c, L'O'  , 0x01fe, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǿ’
  DEADTRANS(0x074c, L'o'  , 0x01ff, NORMAL_CHARACTER),	// ‘o’ -> ‘ǿ’
  DEADTRANS(0x074c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Diaeresis
  DEADTRANS(0x07b2, L'I'  , 0x1e2e, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḯ’
  DEADTRANS(0x07b2, L'U'  , 0x01d7, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǘ’
  DEADTRANS(0x07b2, L'i'  , 0x1e2f, NORMAL_CHARACTER),	// ‘i’ -> ‘ḯ’
  DEADTRANS(0x07b2, L'u'  , 0x01d8, NORMAL_CHARACTER),	// ‘u’ -> ‘ǘ’
  DEADTRANS(0x07b2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Tilde
  DEADTRANS(0x07b3, L'O'  , 0x1e4c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṍ’
  DEADTRANS(0x07b3, L'U'  , 0x1e78, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṹ’
  DEADTRANS(0x07b3, L'o'  , 0x1e4d, NORMAL_CHARACTER),	// ‘o’ -> ‘ṍ’
  DEADTRANS(0x07b3, L'u'  , 0x1e79, NORMAL_CHARACTER),	// ‘u’ -> ‘ṹ’
  DEADTRANS(0x07b3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Macron
  DEADTRANS(0x07b4, L'E'  , 0x1e16, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḗ’
  DEADTRANS(0x07b4, L'O'  , 0x1e52, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṓ’
  DEADTRANS(0x07b4, L'e'  , 0x1e17, NORMAL_CHARACTER),	// ‘e’ -> ‘ḗ’
  DEADTRANS(0x07b4, L'o'  , 0x1e53, NORMAL_CHARACTER),	// ‘o’ -> ‘ṓ’
  DEADTRANS(0x07b4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Circumflex Accent
  DEADTRANS(0x07b5, L'A'  , 0x1ea4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ấ’
  DEADTRANS(0x07b5, L'E'  , 0x1ebe, NORMAL_CHARACTER),	// ‘E’ -> ‘Ế’
  DEADTRANS(0x07b5, L'O'  , 0x1ed0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ố’
  DEADTRANS(0x07b5, L'a'  , 0x1ea5, NORMAL_CHARACTER),	// ‘a’ -> ‘ấ’
  DEADTRANS(0x07b5, L'e'  , 0x1ebf, NORMAL_CHARACTER),	// ‘e’ -> ‘ế’
  DEADTRANS(0x07b5, L'o'  , 0x1ed1, NORMAL_CHARACTER),	// ‘o’ -> ‘ố’
  DEADTRANS(0x07b5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Breve
  DEADTRANS(0x07b6, L'A'  , 0x1eae, NORMAL_CHARACTER),	// ‘A’ -> ‘Ắ’
  DEADTRANS(0x07b6, L'a'  , 0x1eaf, NORMAL_CHARACTER),	// ‘a’ -> ‘ắ’
  DEADTRANS(0x07b6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Dot Above
  DEADTRANS(0x07b7, L'S'  , 0x1e64, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṥ’
  DEADTRANS(0x07b7, L's'  , 0x1e65, NORMAL_CHARACTER),	// ‘s’ -> ‘ṥ’
  DEADTRANS(0x07b7, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Ring Above
  DEADTRANS(0x07b8, L'A'  , 0x01fa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǻ’
  DEADTRANS(0x07b8, L'a'  , 0x01fb, NORMAL_CHARACTER),	// ‘a’ -> ‘ǻ’
  DEADTRANS(0x07b8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Horn
  DEADTRANS(0x07b9, L'O'  , 0x1eda, NORMAL_CHARACTER),	// ‘O’ -> ‘Ớ’
  DEADTRANS(0x07b9, L'U'  , 0x1ee8, NORMAL_CHARACTER),	// ‘U’ -> ‘Ứ’
  DEADTRANS(0x07b9, L'o'  , 0x1edb, NORMAL_CHARACTER),	// ‘o’ -> ‘ớ’
  DEADTRANS(0x07b9, L'u'  , 0x1ee9, NORMAL_CHARACTER),	// ‘u’ -> ‘ứ’
  DEADTRANS(0x07b9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Greek + Long Solidus Overlay
  DEADTRANS(0x07ba, L'r'  , 0x03fc, NORMAL_CHARACTER),	// ‘r’ -> ‘ϼ’
  DEADTRANS(0x07ba, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Greek + Macron
  DEADTRANS(0x07bb, L'A'  , 0x1fb9, NORMAL_CHARACTER),	// ‘A’ -> ‘Ᾱ’
  DEADTRANS(0x07bb, L'I'  , 0x1fd9, NORMAL_CHARACTER),	// ‘I’ -> ‘Ῑ’
  DEADTRANS(0x07bb, L'U'  , 0x1fe9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ῡ’
  DEADTRANS(0x07bb, L'a'  , 0x1fb1, NORMAL_CHARACTER),	// ‘a’ -> ‘ᾱ’
  DEADTRANS(0x07bb, L'i'  , 0x1fd1, NORMAL_CHARACTER),	// ‘i’ -> ‘ῑ’
  DEADTRANS(0x07bb, L'u'  , 0x1fe1, NORMAL_CHARACTER),	// ‘u’ -> ‘ῡ’
  DEADTRANS(0x07bb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Greek + Crochet en chef
  DEADTRANS(0x07bc, L'U'  , 0x03d2, NORMAL_CHARACTER),	// ‘U’ -> ‘ϒ’
  DEADTRANS(0x07bc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Latin + Long Solidus Overlay
  DEADTRANS(0x07bd, L'o'  , 0xab3f, NORMAL_CHARACTER),	// ‘o’ -> ‘ꬿ’
  DEADTRANS(0x07bd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Latin + Long Stroke Overlay
  DEADTRANS(0x07be, L'T'  , 0xa764, NORMAL_CHARACTER),	// ‘T’ -> ‘Ꝥ’
  DEADTRANS(0x07be, L'a'  , 0xab30, NORMAL_CHARACTER),	// ‘a’ -> ‘ꬰ’
  DEADTRANS(0x07be, L'i'  , 0x1d7c, NORMAL_CHARACTER),	// ‘i’ -> ‘ᵼ’
  DEADTRANS(0x07be, L't'  , 0xa765, NORMAL_CHARACTER),	// ‘t’ -> ‘ꝥ’
  DEADTRANS(0x07be, L'u'  , 0x1d7f, NORMAL_CHARACTER),	// ‘u’ -> ‘ᵿ’
  DEADTRANS(0x07be, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Dot Below
  DEADTRANS(0x07bf, L'A'  , 0x1eac, NORMAL_CHARACTER),	// ‘A’ -> ‘Ậ’
  DEADTRANS(0x07bf, L'E'  , 0x1ec6, NORMAL_CHARACTER),	// ‘E’ -> ‘Ệ’
  DEADTRANS(0x07bf, L'O'  , 0x1ed8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ộ’
  DEADTRANS(0x07bf, L'a'  , 0x1ead, NORMAL_CHARACTER),	// ‘a’ -> ‘ậ’
  DEADTRANS(0x07bf, L'e'  , 0x1ec7, NORMAL_CHARACTER),	// ‘e’ -> ‘ệ’
  DEADTRANS(0x07bf, L'o'  , 0x1ed9, NORMAL_CHARACTER),	// ‘o’ -> ‘ộ’
  DEADTRANS(0x07bf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Grave Accent
  DEADTRANS(0x07fb, L'A'  , 0x1ea6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ầ’
  DEADTRANS(0x07fb, L'E'  , 0x1ec0, NORMAL_CHARACTER),	// ‘E’ -> ‘Ề’
  DEADTRANS(0x07fb, L'O'  , 0x1ed2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ồ’
  DEADTRANS(0x07fb, L'a'  , 0x1ea7, NORMAL_CHARACTER),	// ‘a’ -> ‘ầ’
  DEADTRANS(0x07fb, L'e'  , 0x1ec1, NORMAL_CHARACTER),	// ‘e’ -> ‘ề’
  DEADTRANS(0x07fb, L'o'  , 0x1ed3, NORMAL_CHARACTER),	// ‘o’ -> ‘ồ’
  DEADTRANS(0x07fb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Tilde
  DEADTRANS(0x07fc, L'A'  , 0x1eaa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẫ’
  DEADTRANS(0x07fc, L'E'  , 0x1ec4, NORMAL_CHARACTER),	// ‘E’ -> ‘Ễ’
  DEADTRANS(0x07fc, L'O'  , 0x1ed6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỗ’
  DEADTRANS(0x07fc, L'a'  , 0x1eab, NORMAL_CHARACTER),	// ‘a’ -> ‘ẫ’
  DEADTRANS(0x07fc, L'e'  , 0x1ec5, NORMAL_CHARACTER),	// ‘e’ -> ‘ễ’
  DEADTRANS(0x07fc, L'o'  , 0x1ed7, NORMAL_CHARACTER),	// ‘o’ -> ‘ỗ’
  DEADTRANS(0x07fc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Acute Accent
  DEADTRANS(0x082e, L'A'  , 0x1ea4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ấ’
  DEADTRANS(0x082e, L'E'  , 0x1ebe, NORMAL_CHARACTER),	// ‘E’ -> ‘Ế’
  DEADTRANS(0x082e, L'O'  , 0x1ed0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ố’
  DEADTRANS(0x082e, L'a'  , 0x1ea5, NORMAL_CHARACTER),	// ‘a’ -> ‘ấ’
  DEADTRANS(0x082e, L'e'  , 0x1ebf, NORMAL_CHARACTER),	// ‘e’ -> ‘ế’
  DEADTRANS(0x082e, L'o'  , 0x1ed1, NORMAL_CHARACTER),	// ‘o’ -> ‘ố’
  DEADTRANS(0x082e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Bépo dot below
  DEADTRANS(0x082f, L'A'  , 0x1eac, NORMAL_CHARACTER),	// ‘A’ -> ‘Ậ’
  DEADTRANS(0x082f, L'E'  , 0x1ec6, NORMAL_CHARACTER),	// ‘E’ -> ‘Ệ’
  DEADTRANS(0x082f, L'O'  , 0x1ed8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ộ’
  DEADTRANS(0x082f, L'a'  , 0x1ead, NORMAL_CHARACTER),	// ‘a’ -> ‘ậ’
  DEADTRANS(0x082f, L'e'  , 0x1ec7, NORMAL_CHARACTER),	// ‘e’ -> ‘ệ’
  DEADTRANS(0x082f, L'o'  , 0x1ed9, NORMAL_CHARACTER),	// ‘o’ -> ‘ộ’
  DEADTRANS(0x082f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Caron + Diaeresis
  DEADTRANS(0x083f, L'U'  , 0x01d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǚ’
  DEADTRANS(0x083f, L'u'  , 0x01da, NORMAL_CHARACTER),	// ‘u’ -> ‘ǚ’
  DEADTRANS(0x083f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Caron + Bépo Latin
  DEADTRANS(0x085c, L'Z'  , 0x01ee, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ǯ’
  DEADTRANS(0x085c, L'z'  , 0x01ef, NORMAL_CHARACTER),	// ‘z’ -> ‘ǯ’
  DEADTRANS(0x085c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Caron + Dot Above
  DEADTRANS(0x085d, L'S'  , 0x1e66, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṧ’
  DEADTRANS(0x085d, L's'  , 0x1e67, NORMAL_CHARACTER),	// ‘s’ -> ‘ṧ’
  DEADTRANS(0x085d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Cedilla
  DEADTRANS(0x085f, L'E'  , 0x1e1c, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḝ’
  DEADTRANS(0x085f, L'e'  , 0x1e1d, NORMAL_CHARACTER),	// ‘e’ -> ‘ḝ’
  DEADTRANS(0x085f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Dot Below
  DEADTRANS(0x086b, L'A'  , 0x1eb6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ặ’
  DEADTRANS(0x086b, L'a'  , 0x1eb7, NORMAL_CHARACTER),	// ‘a’ -> ‘ặ’
  DEADTRANS(0x086b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Grave Accent
  DEADTRANS(0x086c, L'A'  , 0x1eb0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ằ’
  DEADTRANS(0x086c, L'a'  , 0x1eb1, NORMAL_CHARACTER),	// ‘a’ -> ‘ằ’
  DEADTRANS(0x086c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Tilde
  DEADTRANS(0x086d, L'A'  , 0x1eb4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẵ’
  DEADTRANS(0x086d, L'a'  , 0x1eb5, NORMAL_CHARACTER),	// ‘a’ -> ‘ẵ’
  DEADTRANS(0x086d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Acute Accent
  DEADTRANS(0x086e, L'A'  , 0x1eae, NORMAL_CHARACTER),	// ‘A’ -> ‘Ắ’
  DEADTRANS(0x086e, L'a'  , 0x1eaf, NORMAL_CHARACTER),	// ‘a’ -> ‘ắ’
  DEADTRANS(0x086e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Bépo dot below
  DEADTRANS(0x086f, L'A'  , 0x1eb6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ặ’
  DEADTRANS(0x086f, L'a'  , 0x1eb7, NORMAL_CHARACTER),	// ‘a’ -> ‘ặ’
  DEADTRANS(0x086f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Dot Below
  DEADTRANS(0x0870, L'S'  , 0x1e68, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṩ’
  DEADTRANS(0x0870, L's'  , 0x1e69, NORMAL_CHARACTER),	// ‘s’ -> ‘ṩ’
  DEADTRANS(0x0870, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Long Solidus Overlay
  DEADTRANS(0x0871, L'j'  , 0x025f, NORMAL_CHARACTER),	// ‘j’ -> ‘ɟ’
  DEADTRANS(0x0871, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Macron
  DEADTRANS(0x0872, L'A'  , 0x01e0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǡ’
  DEADTRANS(0x0872, L'O'  , 0x0230, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȱ’
  DEADTRANS(0x0872, L'a'  , 0x01e1, NORMAL_CHARACTER),	// ‘a’ -> ‘ǡ’
  DEADTRANS(0x0872, L'o'  , 0x0231, NORMAL_CHARACTER),	// ‘o’ -> ‘ȱ’
  DEADTRANS(0x0872, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Acute Accent
  DEADTRANS(0x0873, L'S'  , 0x1e64, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṥ’
  DEADTRANS(0x0873, L's'  , 0x1e65, NORMAL_CHARACTER),	// ‘s’ -> ‘ṥ’
  DEADTRANS(0x0873, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Caron
  DEADTRANS(0x0874, L'S'  , 0x1e66, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṧ’
  DEADTRANS(0x0874, L's'  , 0x1e67, NORMAL_CHARACTER),	// ‘s’ -> ‘ṧ’
  DEADTRANS(0x0874, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Long Stroke Overlay
  DEADTRANS(0x0875, 0x1ebb, 0x0877, CHAINED_DEAD_KEY),	// ‹Crochet en chef› -> ‹Dot Above + Long Stroke Overlay + Crochet en chef›
  DEADTRANS(0x0875, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Bépo dot below
  DEADTRANS(0x0876, L'S'  , 0x1e68, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṩ’
  DEADTRANS(0x0876, L's'  , 0x1e69, NORMAL_CHARACTER),	// ‘s’ -> ‘ṩ’
  DEADTRANS(0x0876, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Long Stroke Overlay + Crochet en chef
  DEADTRANS(0x0877, L'j'  , 0x0284, NORMAL_CHARACTER),	// ‘j’ -> ‘ʄ’
  DEADTRANS(0x0877, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Ring Above + Acute Accent
  DEADTRANS(0x0878, L'A'  , 0x01fa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǻ’
  DEADTRANS(0x0878, L'a'  , 0x01fb, NORMAL_CHARACTER),	// ‘a’ -> ‘ǻ’
  DEADTRANS(0x0878, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Ogonek + Macron
  DEADTRANS(0x0879, L'O'  , 0x01ec, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǭ’
  DEADTRANS(0x0879, L'o'  , 0x01ed, NORMAL_CHARACTER),	// ‘o’ -> ‘ǭ’
  DEADTRANS(0x0879, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Dot Below
  DEADTRANS(0x087a, L'O'  , 0x1ee2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ợ’
  DEADTRANS(0x087a, L'U'  , 0x1ef0, NORMAL_CHARACTER),	// ‘U’ -> ‘Ự’
  DEADTRANS(0x087a, L'o'  , 0x1ee3, NORMAL_CHARACTER),	// ‘o’ -> ‘ợ’
  DEADTRANS(0x087a, L'u'  , 0x1ef1, NORMAL_CHARACTER),	// ‘u’ -> ‘ự’
  DEADTRANS(0x087a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Grave Accent
  DEADTRANS(0x087b, L'O'  , 0x1edc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ờ’
  DEADTRANS(0x087b, L'U'  , 0x1eea, NORMAL_CHARACTER),	// ‘U’ -> ‘Ừ’
  DEADTRANS(0x087b, L'o'  , 0x1edd, NORMAL_CHARACTER),	// ‘o’ -> ‘ờ’
  DEADTRANS(0x087b, L'u'  , 0x1eeb, NORMAL_CHARACTER),	// ‘u’ -> ‘ừ’
  DEADTRANS(0x087b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Tilde
  DEADTRANS(0x087c, L'O'  , 0x1ee0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỡ’
  DEADTRANS(0x087c, L'U'  , 0x1eee, NORMAL_CHARACTER),	// ‘U’ -> ‘Ữ’
  DEADTRANS(0x087c, L'o'  , 0x1ee1, NORMAL_CHARACTER),	// ‘o’ -> ‘ỡ’
  DEADTRANS(0x087c, L'u'  , 0x1eef, NORMAL_CHARACTER),	// ‘u’ -> ‘ữ’
  DEADTRANS(0x087c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Acute Accent
  DEADTRANS(0x087d, L'O'  , 0x1eda, NORMAL_CHARACTER),	// ‘O’ -> ‘Ớ’
  DEADTRANS(0x087d, L'U'  , 0x1ee8, NORMAL_CHARACTER),	// ‘U’ -> ‘Ứ’
  DEADTRANS(0x087d, L'o'  , 0x1edb, NORMAL_CHARACTER),	// ‘o’ -> ‘ớ’
  DEADTRANS(0x087d, L'u'  , 0x1ee9, NORMAL_CHARACTER),	// ‘u’ -> ‘ứ’
  DEADTRANS(0x087d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Bépo dot below
  DEADTRANS(0x087e, L'O'  , 0x1ee2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ợ’
  DEADTRANS(0x087e, L'U'  , 0x1ef0, NORMAL_CHARACTER),	// ‘U’ -> ‘Ự’
  DEADTRANS(0x087e, L'o'  , 0x1ee3, NORMAL_CHARACTER),	// ‘o’ -> ‘ợ’
  DEADTRANS(0x087e, L'u'  , 0x1ef1, NORMAL_CHARACTER),	// ‘u’ -> ‘ự’
  DEADTRANS(0x087e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Stroke Overlay + Long Solidus Overlay
  DEADTRANS(0x087f, L'K'  , 0xa744, NORMAL_CHARACTER),	// ‘K’ -> ‘Ꝅ’
  DEADTRANS(0x087f, L'k'  , 0xa745, NORMAL_CHARACTER),	// ‘k’ -> ‘ꝅ’
  DEADTRANS(0x087f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Stroke Overlay + Bépo Latin
  DEADTRANS(0x0880, L'T'  , 0xa764, NORMAL_CHARACTER),	// ‘T’ -> ‘Ꝥ’
  DEADTRANS(0x0880, L'a'  , 0xab30, NORMAL_CHARACTER),	// ‘a’ -> ‘ꬰ’
  DEADTRANS(0x0880, L'i'  , 0x1d7c, NORMAL_CHARACTER),	// ‘i’ -> ‘ᵼ’
  DEADTRANS(0x0880, L't'  , 0xa765, NORMAL_CHARACTER),	// ‘t’ -> ‘ꝥ’
  DEADTRANS(0x0880, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Stroke Overlay + Dot Above
  DEADTRANS(0x0881, 0x1ebb, 0x0883, CHAINED_DEAD_KEY),	// ‹Crochet en chef› -> ‹Long Stroke Overlay + Dot Above + Crochet en chef›
  DEADTRANS(0x0881, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Stroke Overlay + Subscript
  DEADTRANS(0x0882, L'B'  , 0x1d03, NORMAL_CHARACTER),	// ‘B’ -> ‘ᴃ’
  DEADTRANS(0x0882, L'I'  , 0x1d7b, NORMAL_CHARACTER),	// ‘I’ -> ‘ᵻ’
  DEADTRANS(0x0882, L'U'  , 0x1d7e, NORMAL_CHARACTER),	// ‘U’ -> ‘ᵾ’
  DEADTRANS(0x0882, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Stroke Overlay + Dot Above + Crochet en chef
  DEADTRANS(0x0883, L'j'  , 0x0284, NORMAL_CHARACTER),	// ‘j’ -> ‘ʄ’
  DEADTRANS(0x0883, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Superscript + Bépo Latin
  DEADTRANS(0x0884, L'h'  , 0x02c0, NORMAL_CHARACTER),	// ‘h’ -> ‘ˀ’
  DEADTRANS(0x0884, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Subscript + Long Solidus Overlay
  DEADTRANS(0x0885, L'L'  , 0x1d0c, NORMAL_CHARACTER),	// ‘L’ -> ‘ᴌ’
  DEADTRANS(0x0885, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Subscript + Bépo Latin
  DEADTRANS(0x0886, L'e'  , 0x2094, NORMAL_CHARACTER),	// ‘e’ -> ‘ₔ’
  DEADTRANS(0x0886, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Subscript + Long Stroke Overlay
  DEADTRANS(0x0887, L'B'  , 0x1d03, NORMAL_CHARACTER),	// ‘B’ -> ‘ᴃ’
  DEADTRANS(0x0887, L'I'  , 0x1d7b, NORMAL_CHARACTER),	// ‘I’ -> ‘ᵻ’
  DEADTRANS(0x0887, L'U'  , 0x1d7e, NORMAL_CHARACTER),	// ‘U’ -> ‘ᵾ’
  DEADTRANS(0x0887, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo dot below + Macron
  DEADTRANS(0x0888, L'L'  , 0x1e38, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḹ’
  DEADTRANS(0x0888, L'R'  , 0x1e5c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṝ’
  DEADTRANS(0x0888, L'l'  , 0x1e39, NORMAL_CHARACTER),	// ‘l’ -> ‘ḹ’
  DEADTRANS(0x0888, L'r'  , 0x1e5d, NORMAL_CHARACTER),	// ‘r’ -> ‘ṝ’
  DEADTRANS(0x0888, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo dot below + Circumflex Accent
  DEADTRANS(0x0889, L'A'  , 0x1eac, NORMAL_CHARACTER),	// ‘A’ -> ‘Ậ’
  DEADTRANS(0x0889, L'E'  , 0x1ec6, NORMAL_CHARACTER),	// ‘E’ -> ‘Ệ’
  DEADTRANS(0x0889, L'O'  , 0x1ed8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ộ’
  DEADTRANS(0x0889, L'a'  , 0x1ead, NORMAL_CHARACTER),	// ‘a’ -> ‘ậ’
  DEADTRANS(0x0889, L'e'  , 0x1ec7, NORMAL_CHARACTER),	// ‘e’ -> ‘ệ’
  DEADTRANS(0x0889, L'o'  , 0x1ed9, NORMAL_CHARACTER),	// ‘o’ -> ‘ộ’
  DEADTRANS(0x0889, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo dot below + Breve
  DEADTRANS(0x088a, L'A'  , 0x1eb6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ặ’
  DEADTRANS(0x088a, L'a'  , 0x1eb7, NORMAL_CHARACTER),	// ‘a’ -> ‘ặ’
  DEADTRANS(0x088a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo dot below + Horn
  DEADTRANS(0x088b, L'O'  , 0x1ee2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ợ’
  DEADTRANS(0x088b, L'U'  , 0x1ef0, NORMAL_CHARACTER),	// ‘U’ -> ‘Ự’
  DEADTRANS(0x088b, L'o'  , 0x1ee3, NORMAL_CHARACTER),	// ‘o’ -> ‘ợ’
  DEADTRANS(0x088b, L'u'  , 0x1ef1, NORMAL_CHARACTER),	// ‘u’ -> ‘ự’
  DEADTRANS(0x088b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Greek
  DEADTRANS(0x088c, L'U'  , 0x03d2, NORMAL_CHARACTER),	// ‘U’ -> ‘ϒ’
  DEADTRANS(0x088c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Bépo Latin
  DEADTRANS(0x088d, L'e'  , 0x025a, NORMAL_CHARACTER),	// ‘e’ -> ‘ɚ’
  DEADTRANS(0x088d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Circumflex Accent
  DEADTRANS(0x088e, L'A'  , 0x1ea8, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẩ’
  DEADTRANS(0x088e, L'E'  , 0x1ec2, NORMAL_CHARACTER),	// ‘E’ -> ‘Ể’
  DEADTRANS(0x088e, L'O'  , 0x1ed4, NORMAL_CHARACTER),	// ‘O’ -> ‘Ổ’
  DEADTRANS(0x088e, L'a'  , 0x1ea9, NORMAL_CHARACTER),	// ‘a’ -> ‘ẩ’
  DEADTRANS(0x088e, L'e'  , 0x1ec3, NORMAL_CHARACTER),	// ‘e’ -> ‘ể’
  DEADTRANS(0x088e, L'o'  , 0x1ed5, NORMAL_CHARACTER),	// ‘o’ -> ‘ổ’
  DEADTRANS(0x088e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Breve
  DEADTRANS(0x088f, L'A'  , 0x1eb2, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẳ’
  DEADTRANS(0x088f, L'a'  , 0x1eb3, NORMAL_CHARACTER),	// ‘a’ -> ‘ẳ’
  DEADTRANS(0x088f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Dot Above
  DEADTRANS(0x0890, 0x0336, 0x0893, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Crochet en chef + Dot Above + Long Stroke Overlay›
  DEADTRANS(0x0890, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Horn
  DEADTRANS(0x0891, L'O'  , 0x1ede, NORMAL_CHARACTER),	// ‘O’ -> ‘Ở’
  DEADTRANS(0x0891, L'U'  , 0x1eec, NORMAL_CHARACTER),	// ‘U’ -> ‘Ử’
  DEADTRANS(0x0891, L'o'  , 0x1edf, NORMAL_CHARACTER),	// ‘o’ -> ‘ở’
  DEADTRANS(0x0891, L'u'  , 0x1eed, NORMAL_CHARACTER),	// ‘u’ -> ‘ử’
  DEADTRANS(0x0891, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Long Stroke Overlay
  DEADTRANS(0x0892, 0x02d9, 0x0894, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Crochet en chef + Long Stroke Overlay + Dot Above›
  DEADTRANS(0x0892, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Dot Above + Long Stroke Overlay
  DEADTRANS(0x0893, L'j'  , 0x0284, NORMAL_CHARACTER),	// ‘j’ -> ‘ʄ’
  DEADTRANS(0x0893, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Crochet en chef + Long Stroke Overlay + Dot Above
  DEADTRANS(0x0894, L'j'  , 0x0284, NORMAL_CHARACTER),	// ‘j’ -> ‘ʄ’
  DEADTRANS(0x0894, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + I
  DEADTRANS(0x0896, 0x00b5, 0x089f, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Bépo Science + I + Greek›
  DEADTRANS(0x0896, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + b
  DEADTRANS(0x0897, L'('  , 0x27ec, NORMAL_CHARACTER),	// U+0028 -> U+27EC
  DEADTRANS(0x0897, L')'  , 0x27ed, NORMAL_CHARACTER),	// U+0029 -> U+27ED
  DEADTRANS(0x0897, L'C'  , 0x2102, NORMAL_CHARACTER),	// ‘C’ -> ‘ℂ’
  DEADTRANS(0x0897, L'H'  , 0x210d, NORMAL_CHARACTER),	// ‘H’ -> ‘ℍ’
  DEADTRANS(0x0897, L'N'  , 0x2115, NORMAL_CHARACTER),	// ‘N’ -> ‘ℕ’
  DEADTRANS(0x0897, L'P'  , 0x2119, NORMAL_CHARACTER),	// ‘P’ -> ‘ℙ’
  DEADTRANS(0x0897, L'Q'  , 0x211a, NORMAL_CHARACTER),	// ‘Q’ -> ‘ℚ’
  DEADTRANS(0x0897, L'R'  , 0x211d, NORMAL_CHARACTER),	// ‘R’ -> ‘ℝ’
  DEADTRANS(0x0897, L'Z'  , 0x2124, NORMAL_CHARACTER),	// ‘Z’ -> ‘ℤ’
  DEADTRANS(0x0897, L'['  , 0x301a, NORMAL_CHARACTER),	// U+005B -> U+301A
  DEADTRANS(0x0897, L']'  , 0x301b, NORMAL_CHARACTER),	// U+005D -> U+301B
  DEADTRANS(0x0897, L'{'  , 0x2983, NORMAL_CHARACTER),	// U+007B -> U+2983
  DEADTRANS(0x0897, L'}'  , 0x2984, NORMAL_CHARACTER),	// U+007D -> U+2984
  DEADTRANS(0x0897, 0x00b5, 0x08b5, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Bépo Science + b + Greek›
  DEADTRANS(0x0897, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + f
  DEADTRANS(0x0899, L'C'  , 0x212d, NORMAL_CHARACTER),	// ‘C’ -> ‘ℭ’
  DEADTRANS(0x0899, L'H'  , 0x210c, NORMAL_CHARACTER),	// ‘H’ -> ‘ℌ’
  DEADTRANS(0x0899, L'I'  , 0x2111, NORMAL_CHARACTER),	// ‘I’ -> ‘ℑ’
  DEADTRANS(0x0899, L'R'  , 0x211c, NORMAL_CHARACTER),	// ‘R’ -> ‘ℜ’
  DEADTRANS(0x0899, L'Z'  , 0x2128, NORMAL_CHARACTER),	// ‘Z’ -> ‘ℨ’
  DEADTRANS(0x0899, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + g
  DEADTRANS(0x089a, L'['  , 0x2997, NORMAL_CHARACTER),	// U+005B -> U+2997
  DEADTRANS(0x089a, L']'  , 0x2998, NORMAL_CHARACTER),	// U+005D -> U+2998
  DEADTRANS(0x089a, 0x00b5, 0x08be, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Bépo Science + g + Greek›
  DEADTRANS(0x089a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + i
  DEADTRANS(0x089b, L'h'  , 0x210e, NORMAL_CHARACTER),	// ‘h’ -> ‘ℎ’
  DEADTRANS(0x089b, 0x00b5, 0x08bf, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Bépo Science + i + Greek›
  DEADTRANS(0x089b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + r
  DEADTRANS(0x089c, L'B'  , 0x212c, NORMAL_CHARACTER),	// ‘B’ -> ‘ℬ’
  DEADTRANS(0x089c, L'E'  , 0x2130, NORMAL_CHARACTER),	// ‘E’ -> ‘ℰ’
  DEADTRANS(0x089c, L'F'  , 0x2131, NORMAL_CHARACTER),	// ‘F’ -> ‘ℱ’
  DEADTRANS(0x089c, L'H'  , 0x210b, NORMAL_CHARACTER),	// ‘H’ -> ‘ℋ’
  DEADTRANS(0x089c, L'I'  , 0x2110, NORMAL_CHARACTER),	// ‘I’ -> ‘ℐ’
  DEADTRANS(0x089c, L'L'  , 0x2112, NORMAL_CHARACTER),	// ‘L’ -> ‘ℒ’
  DEADTRANS(0x089c, L'M'  , 0x2133, NORMAL_CHARACTER),	// ‘M’ -> ‘ℳ’
  DEADTRANS(0x089c, L'R'  , 0x211b, NORMAL_CHARACTER),	// ‘R’ -> ‘ℛ’
  DEADTRANS(0x089c, L'e'  , 0x212f, NORMAL_CHARACTER),	// ‘e’ -> ‘ℯ’
  DEADTRANS(0x089c, L'g'  , 0x210a, NORMAL_CHARACTER),	// ‘g’ -> ‘ℊ’
  DEADTRANS(0x089c, L'o'  , 0x2134, NORMAL_CHARACTER),	// ‘o’ -> ‘ℴ’
  DEADTRANS(0x089c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + Long Solidus Overlay
  DEADTRANS(0x089d, L'\'' , 0x2285, NORMAL_CHARACTER),	// U+0027 -> U+2285
  DEADTRANS(0x089d, L','  , 0x220c, NORMAL_CHARACTER),	// U+002C -> U+220C
  DEADTRANS(0x089d, L'<'  , 0x2280, NORMAL_CHARACTER),	// U+003C -> U+2280
  DEADTRANS(0x089d, L'>'  , 0x2281, NORMAL_CHARACTER),	// U+003E -> U+2281
  DEADTRANS(0x089d, L'E'  , 0x2204, NORMAL_CHARACTER),	// ‘E’ -> U+2204
  DEADTRANS(0x089d, L'Q'  , 0x2226, NORMAL_CHARACTER),	// ‘Q’ -> U+2226
  DEADTRANS(0x089d, L'e'  , 0x2209, NORMAL_CHARACTER),	// ‘e’ -> U+2209
  DEADTRANS(0x089d, L'k'  , 0x2241, NORMAL_CHARACTER),	// ‘k’ -> U+2241
  DEADTRANS(0x089d, L'v'  , 0x22bd, NORMAL_CHARACTER),	// ‘v’ -> U+22BD
  DEADTRANS(0x089d, L'|'  , 0x2224, NORMAL_CHARACTER),	// U+007C -> U+2224
  DEADTRANS(0x089d, 0x00e9, 0x22bc, NORMAL_CHARACTER),	// ‘é’ -> U+22BC
  DEADTRANS(0x089d, 0x20ac, 0x2284, NORMAL_CHARACTER),	// U+20AC -> U+2284
  DEADTRANS(0x089d, 0x2122, 0x22ad, NORMAL_CHARACTER),	// U+2122 -> U+22AD
  DEADTRANS(0x089d, 0x2260, 0x2262, NORMAL_CHARACTER),	// U+2260 -> U+2262
  DEADTRANS(0x089d, 0x2623, 0x22ea, NORMAL_CHARACTER),	// U+2623 -> U+22EA
  DEADTRANS(0x089d, L';'  , 0x228b, NORMAL_CHARACTER),	// ‹Comma Below› -> U+228B
  DEADTRANS(0x089d, 0x00a4, 0x228a, NORMAL_CHARACTER),	// ‹Bépo Currency› -> U+228A
  DEADTRANS(0x089d, 0x1d49, 0x22ac, NORMAL_CHARACTER),	// ‹Superscript› -> U+22AC
  DEADTRANS(0x089d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + Macron
  DEADTRANS(0x089e, L'v'  , 0x22bd, NORMAL_CHARACTER),	// ‘v’ -> U+22BD
  DEADTRANS(0x089e, 0x00e9, 0x22bc, NORMAL_CHARACTER),	// ‘é’ -> U+22BC
  DEADTRANS(0x089e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Bépo Science + b + Greek
  DEADTRANS(0x08b5, L'G'  , 0x213e, NORMAL_CHARACTER),	// ‘G’ -> ‘ℾ’
  DEADTRANS(0x08b5, L'P'  , 0x213f, NORMAL_CHARACTER),	// ‘P’ -> ‘ℿ’
  DEADTRANS(0x08b5, L'S'  , 0x2140, NORMAL_CHARACTER),	// ‘S’ -> U+2140
  DEADTRANS(0x08b5, L'g'  , 0x213d, NORMAL_CHARACTER),	// ‘g’ -> ‘ℽ’
  DEADTRANS(0x08b5, L'p'  , 0x213c, NORMAL_CHARACTER),	// ‘p’ -> ‘ℼ’
  DEADTRANS(0x08b5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde Overlay + Tilde
  DEADTRANS(0x08c0, L'l'  , 0xab38, NORMAL_CHARACTER),	// ‘l’ -> ‘ꬸ’
  DEADTRANS(0x08c0, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Superscript
  DEADTRANS(0x1d49, L'('  , 0x207d, NORMAL_CHARACTER),	// U+0028 -> U+207D
  DEADTRANS(0x1d49, L')'  , 0x207e, NORMAL_CHARACTER),	// U+0029 -> U+207E
  DEADTRANS(0x1d49, L'+'  , 0x207a, NORMAL_CHARACTER),	// U+002B -> U+207A
  DEADTRANS(0x1d49, L'-'  , 0x207b, NORMAL_CHARACTER),	// U+002D -> U+207B
  DEADTRANS(0x1d49, L'0'  , 0x2070, NORMAL_CHARACTER),	// ‘0’ -> ‘⁰’
  DEADTRANS(0x1d49, L'1'  , 0x00b9, NORMAL_CHARACTER),	// ‘1’ -> ‘¹’
  DEADTRANS(0x1d49, L'2'  , 0x00b2, NORMAL_CHARACTER),	// ‘2’ -> ‘²’
  DEADTRANS(0x1d49, L'3'  , 0x00b3, NORMAL_CHARACTER),	// ‘3’ -> ‘³’
  DEADTRANS(0x1d49, L'4'  , 0x2074, NORMAL_CHARACTER),	// ‘4’ -> ‘⁴’
  DEADTRANS(0x1d49, L'5'  , 0x2075, NORMAL_CHARACTER),	// ‘5’ -> ‘⁵’
  DEADTRANS(0x1d49, L'6'  , 0x2076, NORMAL_CHARACTER),	// ‘6’ -> ‘⁶’
  DEADTRANS(0x1d49, L'7'  , 0x2077, NORMAL_CHARACTER),	// ‘7’ -> ‘⁷’
  DEADTRANS(0x1d49, L'8'  , 0x2078, NORMAL_CHARACTER),	// ‘8’ -> ‘⁸’
  DEADTRANS(0x1d49, L'9'  , 0x2079, NORMAL_CHARACTER),	// ‘9’ -> ‘⁹’
  DEADTRANS(0x1d49, L'='  , 0x207c, NORMAL_CHARACTER),	// U+003D -> U+207C
  DEADTRANS(0x1d49, L'A'  , 0x1d2c, NORMAL_CHARACTER),	// ‘A’ -> ‘ᴬ’
  DEADTRANS(0x1d49, L'B'  , 0x1d2e, NORMAL_CHARACTER),	// ‘B’ -> ‘ᴮ’
  DEADTRANS(0x1d49, L'D'  , 0x1d30, NORMAL_CHARACTER),	// ‘D’ -> ‘ᴰ’
  DEADTRANS(0x1d49, L'E'  , 0x1d31, NORMAL_CHARACTER),	// ‘E’ -> ‘ᴱ’
  DEADTRANS(0x1d49, L'G'  , 0x1d33, NORMAL_CHARACTER),	// ‘G’ -> ‘ᴳ’
  DEADTRANS(0x1d49, L'H'  , 0x1d34, NORMAL_CHARACTER),	// ‘H’ -> ‘ᴴ’
  DEADTRANS(0x1d49, L'I'  , 0x1d35, NORMAL_CHARACTER),	// ‘I’ -> ‘ᴵ’
  DEADTRANS(0x1d49, L'J'  , 0x1d36, NORMAL_CHARACTER),	// ‘J’ -> ‘ᴶ’
  DEADTRANS(0x1d49, L'K'  , 0x1d37, NORMAL_CHARACTER),	// ‘K’ -> ‘ᴷ’
  DEADTRANS(0x1d49, L'L'  , 0x1d38, NORMAL_CHARACTER),	// ‘L’ -> ‘ᴸ’
  DEADTRANS(0x1d49, L'M'  , 0x1d39, NORMAL_CHARACTER),	// ‘M’ -> ‘ᴹ’
  DEADTRANS(0x1d49, L'N'  , 0x1d3a, NORMAL_CHARACTER),	// ‘N’ -> ‘ᴺ’
  DEADTRANS(0x1d49, L'O'  , 0x1d3c, NORMAL_CHARACTER),	// ‘O’ -> ‘ᴼ’
  DEADTRANS(0x1d49, L'P'  , 0x1d3e, NORMAL_CHARACTER),	// ‘P’ -> ‘ᴾ’
  DEADTRANS(0x1d49, L'R'  , 0x1d3f, NORMAL_CHARACTER),	// ‘R’ -> ‘ᴿ’
  DEADTRANS(0x1d49, L'T'  , 0x1d40, NORMAL_CHARACTER),	// ‘T’ -> ‘ᵀ’
  DEADTRANS(0x1d49, L'U'  , 0x1d41, NORMAL_CHARACTER),	// ‘U’ -> ‘ᵁ’
  DEADTRANS(0x1d49, L'V'  , 0x2c7d, NORMAL_CHARACTER),	// ‘V’ -> ‘ⱽ’
  DEADTRANS(0x1d49, L'W'  , 0x1d42, NORMAL_CHARACTER),	// ‘W’ -> ‘ᵂ’
  DEADTRANS(0x1d49, L'a'  , 0x1d43, NORMAL_CHARACTER),	// ‘a’ -> ‘ᵃ’
  DEADTRANS(0x1d49, L'b'  , 0x1d47, NORMAL_CHARACTER),	// ‘b’ -> ‘ᵇ’
  DEADTRANS(0x1d49, L'c'  , 0x1d9c, NORMAL_CHARACTER),	// ‘c’ -> ‘ᶜ’
  DEADTRANS(0x1d49, L'd'  , 0x1d48, NORMAL_CHARACTER),	// ‘d’ -> ‘ᵈ’
  DEADTRANS(0x1d49, L'e'  , 0x1d49, NORMAL_CHARACTER),	// ‘e’ -> ‘ᵉ’
  DEADTRANS(0x1d49, L'f'  , 0x1da0, NORMAL_CHARACTER),	// ‘f’ -> ‘ᶠ’
  DEADTRANS(0x1d49, L'g'  , 0x1d4d, NORMAL_CHARACTER),	// ‘g’ -> ‘ᵍ’
  DEADTRANS(0x1d49, L'h'  , 0x02b0, NORMAL_CHARACTER),	// ‘h’ -> ‘ʰ’
  DEADTRANS(0x1d49, L'i'  , 0x2071, NORMAL_CHARACTER),	// ‘i’ -> ‘ⁱ’
  DEADTRANS(0x1d49, L'j'  , 0x02b2, NORMAL_CHARACTER),	// ‘j’ -> ‘ʲ’
  DEADTRANS(0x1d49, L'k'  , 0x1d4f, NORMAL_CHARACTER),	// ‘k’ -> ‘ᵏ’
  DEADTRANS(0x1d49, L'l'  , 0x02e1, NORMAL_CHARACTER),	// ‘l’ -> ‘ˡ’
  DEADTRANS(0x1d49, L'm'  , 0x1d50, NORMAL_CHARACTER),	// ‘m’ -> ‘ᵐ’
  DEADTRANS(0x1d49, L'n'  , 0x207f, NORMAL_CHARACTER),	// ‘n’ -> ‘ⁿ’
  DEADTRANS(0x1d49, L'o'  , 0x1d52, NORMAL_CHARACTER),	// ‘o’ -> ‘ᵒ’
  DEADTRANS(0x1d49, L'p'  , 0x1d56, NORMAL_CHARACTER),	// ‘p’ -> ‘ᵖ’
  DEADTRANS(0x1d49, L'r'  , 0x02b3, NORMAL_CHARACTER),	// ‘r’ -> ‘ʳ’
  DEADTRANS(0x1d49, L's'  , 0x02e2, NORMAL_CHARACTER),	// ‘s’ -> ‘ˢ’
  DEADTRANS(0x1d49, L't'  , 0x1d57, NORMAL_CHARACTER),	// ‘t’ -> ‘ᵗ’
  DEADTRANS(0x1d49, L'u'  , 0x1d58, NORMAL_CHARACTER),	// ‘u’ -> ‘ᵘ’
  DEADTRANS(0x1d49, L'v'  , 0x1d5b, NORMAL_CHARACTER),	// ‘v’ -> ‘ᵛ’
  DEADTRANS(0x1d49, L'w'  , 0x02b7, NORMAL_CHARACTER),	// ‘w’ -> ‘ʷ’
  DEADTRANS(0x1d49, L'x'  , 0x02e3, NORMAL_CHARACTER),	// ‘x’ -> ‘ˣ’
  DEADTRANS(0x1d49, L'y'  , 0x02b8, NORMAL_CHARACTER),	// ‘y’ -> ‘ʸ’
  DEADTRANS(0x1d49, L'z'  , 0x1dbb, NORMAL_CHARACTER),	// ‘z’ -> ‘ᶻ’
  DEADTRANS(0x1d49, 0x00c6, 0x1d2d, NORMAL_CHARACTER),	// ‘Æ’ -> ‘ᴭ’
  DEADTRANS(0x1d49, 0x0153, 0xa7f9, NORMAL_CHARACTER),	// ‘œ’ -> ‘ꟹ’
  DEADTRANS(0x1d49, 0x00df, 0x0884, CHAINED_DEAD_KEY),	// ‹Bépo Latin› -> ‹Superscript + Bépo Latin›
  DEADTRANS(0x1d49, 0x1d49, 0x1d62, CHAINED_DEAD_KEY),	// ‹Superscript› -> ‹Subscript›
  DEADTRANS(0x1d49, L' '  , 0x1d49, NORMAL_CHARACTER),	// U+0020 -> ‘ᵉ’

// Dead key: Subscript
  DEADTRANS(0x1d62, L'('  , 0x208d, NORMAL_CHARACTER),	// U+0028 -> U+208D
  DEADTRANS(0x1d62, L')'  , 0x208e, NORMAL_CHARACTER),	// U+0029 -> U+208E
  DEADTRANS(0x1d62, L'+'  , 0x208a, NORMAL_CHARACTER),	// U+002B -> U+208A
  DEADTRANS(0x1d62, L'-'  , 0x208b, NORMAL_CHARACTER),	// U+002D -> U+208B
  DEADTRANS(0x1d62, L'0'  , 0x2080, NORMAL_CHARACTER),	// ‘0’ -> ‘₀’
  DEADTRANS(0x1d62, L'1'  , 0x2081, NORMAL_CHARACTER),	// ‘1’ -> ‘₁’
  DEADTRANS(0x1d62, L'2'  , 0x2082, NORMAL_CHARACTER),	// ‘2’ -> ‘₂’
  DEADTRANS(0x1d62, L'3'  , 0x2083, NORMAL_CHARACTER),	// ‘3’ -> ‘₃’
  DEADTRANS(0x1d62, L'4'  , 0x2084, NORMAL_CHARACTER),	// ‘4’ -> ‘₄’
  DEADTRANS(0x1d62, L'5'  , 0x2085, NORMAL_CHARACTER),	// ‘5’ -> ‘₅’
  DEADTRANS(0x1d62, L'6'  , 0x2086, NORMAL_CHARACTER),	// ‘6’ -> ‘₆’
  DEADTRANS(0x1d62, L'7'  , 0x2087, NORMAL_CHARACTER),	// ‘7’ -> ‘₇’
  DEADTRANS(0x1d62, L'8'  , 0x2088, NORMAL_CHARACTER),	// ‘8’ -> ‘₈’
  DEADTRANS(0x1d62, L'9'  , 0x2089, NORMAL_CHARACTER),	// ‘9’ -> ‘₉’
  DEADTRANS(0x1d62, L'='  , 0x208c, NORMAL_CHARACTER),	// U+003D -> U+208C
  DEADTRANS(0x1d62, L'A'  , 0x1d00, NORMAL_CHARACTER),	// ‘A’ -> ‘ᴀ’
  DEADTRANS(0x1d62, L'B'  , 0x0299, NORMAL_CHARACTER),	// ‘B’ -> ‘ʙ’
  DEADTRANS(0x1d62, L'C'  , 0x1d04, NORMAL_CHARACTER),	// ‘C’ -> ‘ᴄ’
  DEADTRANS(0x1d62, L'D'  , 0x1d05, NORMAL_CHARACTER),	// ‘D’ -> ‘ᴅ’
  DEADTRANS(0x1d62, L'E'  , 0x1d07, NORMAL_CHARACTER),	// ‘E’ -> ‘ᴇ’
  DEADTRANS(0x1d62, L'F'  , 0xa730, NORMAL_CHARACTER),	// ‘F’ -> ‘ꜰ’
  DEADTRANS(0x1d62, L'G'  , 0x0262, NORMAL_CHARACTER),	// ‘G’ -> ‘ɢ’
  DEADTRANS(0x1d62, L'H'  , 0x029c, NORMAL_CHARACTER),	// ‘H’ -> ‘ʜ’
  DEADTRANS(0x1d62, L'I'  , 0x026a, NORMAL_CHARACTER),	// ‘I’ -> ‘ɪ’
  DEADTRANS(0x1d62, L'J'  , 0x1d0a, NORMAL_CHARACTER),	// ‘J’ -> ‘ᴊ’
  DEADTRANS(0x1d62, L'K'  , 0x1d0b, NORMAL_CHARACTER),	// ‘K’ -> ‘ᴋ’
  DEADTRANS(0x1d62, L'L'  , 0x029f, NORMAL_CHARACTER),	// ‘L’ -> ‘ʟ’
  DEADTRANS(0x1d62, L'M'  , 0x1d0d, NORMAL_CHARACTER),	// ‘M’ -> ‘ᴍ’
  DEADTRANS(0x1d62, L'N'  , 0x0274, NORMAL_CHARACTER),	// ‘N’ -> ‘ɴ’
  DEADTRANS(0x1d62, L'O'  , 0x1d0f, NORMAL_CHARACTER),	// ‘O’ -> ‘ᴏ’
  DEADTRANS(0x1d62, L'P'  , 0x1d18, NORMAL_CHARACTER),	// ‘P’ -> ‘ᴘ’
  DEADTRANS(0x1d62, L'R'  , 0x0280, NORMAL_CHARACTER),	// ‘R’ -> ‘ʀ’
  DEADTRANS(0x1d62, L'S'  , 0xa731, NORMAL_CHARACTER),	// ‘S’ -> ‘ꜱ’
  DEADTRANS(0x1d62, L'T'  , 0x1d1b, NORMAL_CHARACTER),	// ‘T’ -> ‘ᴛ’
  DEADTRANS(0x1d62, L'U'  , 0x1d1c, NORMAL_CHARACTER),	// ‘U’ -> ‘ᴜ’
  DEADTRANS(0x1d62, L'V'  , 0x1d20, NORMAL_CHARACTER),	// ‘V’ -> ‘ᴠ’
  DEADTRANS(0x1d62, L'W'  , 0x1d21, NORMAL_CHARACTER),	// ‘W’ -> ‘ᴡ’
  DEADTRANS(0x1d62, L'Y'  , 0x028f, NORMAL_CHARACTER),	// ‘Y’ -> ‘ʏ’
  DEADTRANS(0x1d62, L'Z'  , 0x1d22, NORMAL_CHARACTER),	// ‘Z’ -> ‘ᴢ’
  DEADTRANS(0x1d62, L'a'  , 0x2090, NORMAL_CHARACTER),	// ‘a’ -> ‘ₐ’
  DEADTRANS(0x1d62, L'e'  , 0x2091, NORMAL_CHARACTER),	// ‘e’ -> ‘ₑ’
  DEADTRANS(0x1d62, L'h'  , 0x2095, NORMAL_CHARACTER),	// ‘h’ -> ‘ₕ’
  DEADTRANS(0x1d62, L'i'  , 0x1d62, NORMAL_CHARACTER),	// ‘i’ -> ‘ᵢ’
  DEADTRANS(0x1d62, L'j'  , 0x2c7c, NORMAL_CHARACTER),	// ‘j’ -> ‘ⱼ’
  DEADTRANS(0x1d62, L'k'  , 0x2096, NORMAL_CHARACTER),	// ‘k’ -> ‘ₖ’
  DEADTRANS(0x1d62, L'l'  , 0x2097, NORMAL_CHARACTER),	// ‘l’ -> ‘ₗ’
  DEADTRANS(0x1d62, L'm'  , 0x2098, NORMAL_CHARACTER),	// ‘m’ -> ‘ₘ’
  DEADTRANS(0x1d62, L'n'  , 0x2099, NORMAL_CHARACTER),	// ‘n’ -> ‘ₙ’
  DEADTRANS(0x1d62, L'o'  , 0x2092, NORMAL_CHARACTER),	// ‘o’ -> ‘ₒ’
  DEADTRANS(0x1d62, L'p'  , 0x209a, NORMAL_CHARACTER),	// ‘p’ -> ‘ₚ’
  DEADTRANS(0x1d62, L'r'  , 0x1d63, NORMAL_CHARACTER),	// ‘r’ -> ‘ᵣ’
  DEADTRANS(0x1d62, L's'  , 0x209b, NORMAL_CHARACTER),	// ‘s’ -> ‘ₛ’
  DEADTRANS(0x1d62, L't'  , 0x209c, NORMAL_CHARACTER),	// ‘t’ -> ‘ₜ’
  DEADTRANS(0x1d62, L'u'  , 0x1d64, NORMAL_CHARACTER),	// ‘u’ -> ‘ᵤ’
  DEADTRANS(0x1d62, L'v'  , 0x1d65, NORMAL_CHARACTER),	// ‘v’ -> ‘ᵥ’
  DEADTRANS(0x1d62, L'x'  , 0x2093, NORMAL_CHARACTER),	// ‘x’ -> ‘ₓ’
  DEADTRANS(0x1d62, 0x00c6, 0x1d01, NORMAL_CHARACTER),	// ‘Æ’ -> ‘ᴁ’
  DEADTRANS(0x1d62, 0x0152, 0x0276, NORMAL_CHARACTER),	// ‘Œ’ -> ‘ɶ’
  DEADTRANS(0x1d62, L'/'  , 0x0885, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Subscript + Long Solidus Overlay›
  DEADTRANS(0x1d62, 0x00df, 0x0886, CHAINED_DEAD_KEY),	// ‹Bépo Latin› -> ‹Subscript + Bépo Latin›
  DEADTRANS(0x1d62, 0x0336, 0x0887, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Subscript + Long Stroke Overlay›
  DEADTRANS(0x1d62, L' '  , 0x1d62, NORMAL_CHARACTER),	// U+0020 -> ‘ᵢ’

// Dead key: Bépo dot below
  DEADTRANS(0x1e33, L'.'  , 0x0323, NORMAL_CHARACTER),	// U+002E -> U+0323
  DEADTRANS(0x1e33, L':'  , 0x0323, NORMAL_CHARACTER),	// U+003A -> U+0323
  DEADTRANS(0x1e33, L'?'  , 0x0323, NORMAL_CHARACTER),	// U+003F -> U+0323
  DEADTRANS(0x1e33, L'A'  , 0x1ea0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ạ’
  DEADTRANS(0x1e33, L'B'  , 0x1e04, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḅ’
  DEADTRANS(0x1e33, L'D'  , 0x1e0c, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḍ’
  DEADTRANS(0x1e33, L'E'  , 0x1eb8, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẹ’
  DEADTRANS(0x1e33, L'H'  , 0x1e24, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḥ’
  DEADTRANS(0x1e33, L'I'  , 0x1eca, NORMAL_CHARACTER),	// ‘I’ -> ‘Ị’
  DEADTRANS(0x1e33, L'K'  , 0x1e32, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḳ’
  DEADTRANS(0x1e33, L'L'  , 0x1e36, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḷ’
  DEADTRANS(0x1e33, L'M'  , 0x1e42, NORMAL_CHARACTER),	// ‘M’ -> ‘Ṃ’
  DEADTRANS(0x1e33, L'N'  , 0x1e46, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṇ’
  DEADTRANS(0x1e33, L'O'  , 0x1ecc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ọ’
  DEADTRANS(0x1e33, L'R'  , 0x1e5a, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṛ’
  DEADTRANS(0x1e33, L'S'  , 0x1e62, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṣ’
  DEADTRANS(0x1e33, L'T'  , 0x1e6c, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṭ’
  DEADTRANS(0x1e33, L'U'  , 0x1ee4, NORMAL_CHARACTER),	// ‘U’ -> ‘Ụ’
  DEADTRANS(0x1e33, L'V'  , 0x1e7e, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṿ’
  DEADTRANS(0x1e33, L'W'  , 0x1e88, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẉ’
  DEADTRANS(0x1e33, L'Y'  , 0x1ef4, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỵ’
  DEADTRANS(0x1e33, L'Z'  , 0x1e92, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẓ’
  DEADTRANS(0x1e33, L'_'  , L'.'  , NORMAL_CHARACTER),	// U+005F -> U+002E
  DEADTRANS(0x1e33, L'a'  , 0x1ea1, NORMAL_CHARACTER),	// ‘a’ -> ‘ạ’
  DEADTRANS(0x1e33, L'b'  , 0x1e05, NORMAL_CHARACTER),	// ‘b’ -> ‘ḅ’
  DEADTRANS(0x1e33, L'd'  , 0x1e0d, NORMAL_CHARACTER),	// ‘d’ -> ‘ḍ’
  DEADTRANS(0x1e33, L'e'  , 0x1eb9, NORMAL_CHARACTER),	// ‘e’ -> ‘ẹ’
  DEADTRANS(0x1e33, L'h'  , 0x1e25, NORMAL_CHARACTER),	// ‘h’ -> ‘ḥ’
  DEADTRANS(0x1e33, L'i'  , 0x1ecb, NORMAL_CHARACTER),	// ‘i’ -> ‘ị’
  DEADTRANS(0x1e33, L'k'  , 0x1e33, NORMAL_CHARACTER),	// ‘k’ -> ‘ḳ’
  DEADTRANS(0x1e33, L'l'  , 0x1e37, NORMAL_CHARACTER),	// ‘l’ -> ‘ḷ’
  DEADTRANS(0x1e33, L'm'  , 0x1e43, NORMAL_CHARACTER),	// ‘m’ -> ‘ṃ’
  DEADTRANS(0x1e33, L'n'  , 0x1e47, NORMAL_CHARACTER),	// ‘n’ -> ‘ṇ’
  DEADTRANS(0x1e33, L'o'  , 0x1ecd, NORMAL_CHARACTER),	// ‘o’ -> ‘ọ’
  DEADTRANS(0x1e33, L'r'  , 0x1e5b, NORMAL_CHARACTER),	// ‘r’ -> ‘ṛ’
  DEADTRANS(0x1e33, L's'  , 0x1e63, NORMAL_CHARACTER),	// ‘s’ -> ‘ṣ’
  DEADTRANS(0x1e33, L't'  , 0x1e6d, NORMAL_CHARACTER),	// ‘t’ -> ‘ṭ’
  DEADTRANS(0x1e33, L'u'  , 0x1ee5, NORMAL_CHARACTER),	// ‘u’ -> ‘ụ’
  DEADTRANS(0x1e33, L'v'  , 0x1e7f, NORMAL_CHARACTER),	// ‘v’ -> ‘ṿ’
  DEADTRANS(0x1e33, L'w'  , 0x1e89, NORMAL_CHARACTER),	// ‘w’ -> ‘ẉ’
  DEADTRANS(0x1e33, L'y'  , 0x1ef5, NORMAL_CHARACTER),	// ‘y’ -> ‘ỵ’
  DEADTRANS(0x1e33, L'z'  , 0x1e93, NORMAL_CHARACTER),	// ‘z’ -> ‘ẓ’
  DEADTRANS(0x1e33, 0x00a0, L'.'  , NORMAL_CHARACTER),	// U+00A0 -> U+002E
  DEADTRANS(0x1e33, 0x00b7, 0x0323, NORMAL_CHARACTER),	// U+00B7 -> U+0323
  DEADTRANS(0x1e33, 0x00bf, 0x0323, NORMAL_CHARACTER),	// U+00BF -> U+0323
  DEADTRANS(0x1e33, 0x00ca, 0x1ec6, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ệ’
  DEADTRANS(0x1e33, 0x00ea, 0x1ec7, NORMAL_CHARACTER),	// ‘ê’ -> ‘ệ’
  DEADTRANS(0x1e33, 0x2019, 0x0323, NORMAL_CHARACTER),	// U+2019 -> U+0323
  DEADTRANS(0x1e33, 0x2026, 0x0323, NORMAL_CHARACTER),	// U+2026 -> U+0323
  DEADTRANS(0x1e33, 0x202f, L'.'  , NORMAL_CHARACTER),	// U+202F -> U+002E
  DEADTRANS(0x1e33, 0x00af, 0x0888, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Bépo dot below + Macron›
  DEADTRANS(0x1e33, 0x02c6, 0x0889, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Bépo dot below + Circumflex Accent›
  DEADTRANS(0x1e33, 0x02d8, 0x088a, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Bépo dot below + Breve›
  DEADTRANS(0x1e33, 0x031b, 0x088b, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Bépo dot below + Horn›
  DEADTRANS(0x1e33, 0x1ebb, 0x0323, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0323
  DEADTRANS(0x1e33, L' '  , L'.'  , NORMAL_CHARACTER),	// U+0020 -> U+002E

// Dead key: Crochet en chef
  DEADTRANS(0x1ebb, L'.'  , 0x0309, NORMAL_CHARACTER),	// U+002E -> U+0309
  DEADTRANS(0x1ebb, L':'  , 0x0309, NORMAL_CHARACTER),	// U+003A -> U+0309
  DEADTRANS(0x1ebb, L'?'  , 0x0309, NORMAL_CHARACTER),	// U+003F -> U+0309
  DEADTRANS(0x1ebb, L'A'  , 0x1ea2, NORMAL_CHARACTER),	// ‘A’ -> ‘Ả’
  DEADTRANS(0x1ebb, L'D'  , 0x0189, NORMAL_CHARACTER),	// ‘D’ -> ‘Ɖ’
  DEADTRANS(0x1ebb, L'E'  , 0x1eba, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẻ’
  DEADTRANS(0x1ebb, L'F'  , 0x0191, NORMAL_CHARACTER),	// ‘F’ -> ‘Ƒ’
  DEADTRANS(0x1ebb, L'H'  , 0xa726, NORMAL_CHARACTER),	// ‘H’ -> ‘Ꜧ’
  DEADTRANS(0x1ebb, L'I'  , 0x1ec8, NORMAL_CHARACTER),	// ‘I’ -> ‘Ỉ’
  DEADTRANS(0x1ebb, L'M'  , 0x2c6e, NORMAL_CHARACTER),	// ‘M’ -> ‘Ɱ’
  DEADTRANS(0x1ebb, L'N'  , 0x019d, NORMAL_CHARACTER),	// ‘N’ -> ‘Ɲ’
  DEADTRANS(0x1ebb, L'O'  , 0x1ece, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỏ’
  DEADTRANS(0x1ebb, L'Q'  , 0x024a, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ɋ’
  DEADTRANS(0x1ebb, L'R'  , 0x2c64, NORMAL_CHARACTER),	// ‘R’ -> ‘Ɽ’
  DEADTRANS(0x1ebb, L'T'  , 0x01ae, NORMAL_CHARACTER),	// ‘T’ -> ‘Ʈ’
  DEADTRANS(0x1ebb, L'U'  , 0x1ee6, NORMAL_CHARACTER),	// ‘U’ -> ‘Ủ’
  DEADTRANS(0x1ebb, L'Y'  , 0x1ef6, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỷ’
  DEADTRANS(0x1ebb, L'Z'  , 0x0224, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ȥ’
  DEADTRANS(0x1ebb, L'a'  , 0x1ea3, NORMAL_CHARACTER),	// ‘a’ -> ‘ả’
  DEADTRANS(0x1ebb, L'd'  , 0x0256, NORMAL_CHARACTER),	// ‘d’ -> ‘ɖ’
  DEADTRANS(0x1ebb, L'e'  , 0x1ebb, NORMAL_CHARACTER),	// ‘e’ -> ‘ẻ’
  DEADTRANS(0x1ebb, L'f'  , 0x0192, NORMAL_CHARACTER),	// ‘f’ -> ‘ƒ’
  DEADTRANS(0x1ebb, L'h'  , 0xa727, NORMAL_CHARACTER),	// ‘h’ -> ‘ꜧ’
  DEADTRANS(0x1ebb, L'i'  , 0x1ec9, NORMAL_CHARACTER),	// ‘i’ -> ‘ỉ’
  DEADTRANS(0x1ebb, L'm'  , 0x0271, NORMAL_CHARACTER),	// ‘m’ -> ‘ɱ’
  DEADTRANS(0x1ebb, L'n'  , 0x0272, NORMAL_CHARACTER),	// ‘n’ -> ‘ɲ’
  DEADTRANS(0x1ebb, L'o'  , 0x1ecf, NORMAL_CHARACTER),	// ‘o’ -> ‘ỏ’
  DEADTRANS(0x1ebb, L'q'  , 0x024b, NORMAL_CHARACTER),	// ‘q’ -> ‘ɋ’
  DEADTRANS(0x1ebb, L'r'  , 0x027d, NORMAL_CHARACTER),	// ‘r’ -> ‘ɽ’
  DEADTRANS(0x1ebb, L's'  , 0x0282, NORMAL_CHARACTER),	// ‘s’ -> ‘ʂ’
  DEADTRANS(0x1ebb, L't'  , 0x0288, NORMAL_CHARACTER),	// ‘t’ -> ‘ʈ’
  DEADTRANS(0x1ebb, L'u'  , 0x1ee7, NORMAL_CHARACTER),	// ‘u’ -> ‘ủ’
  DEADTRANS(0x1ebb, L'y'  , 0x1ef7, NORMAL_CHARACTER),	// ‘y’ -> ‘ỷ’
  DEADTRANS(0x1ebb, L'z'  , 0x0225, NORMAL_CHARACTER),	// ‘z’ -> ‘ȥ’
  DEADTRANS(0x1ebb, 0x00b7, 0x0309, NORMAL_CHARACTER),	// U+00B7 -> U+0309
  DEADTRANS(0x1ebb, 0x00bf, 0x0309, NORMAL_CHARACTER),	// U+00BF -> U+0309
  DEADTRANS(0x1ebb, 0x00ca, 0x1ec2, NORMAL_CHARACTER),	// ‘Ê’ -> ‘Ể’
  DEADTRANS(0x1ebb, 0x00ea, 0x1ec3, NORMAL_CHARACTER),	// ‘ê’ -> ‘ể’
  DEADTRANS(0x1ebb, 0x2019, 0x0309, NORMAL_CHARACTER),	// U+2019 -> U+0309
  DEADTRANS(0x1ebb, 0x2026, 0x0309, NORMAL_CHARACTER),	// U+2026 -> U+0309
  DEADTRANS(0x1ebb, 0x00b5, 0x088c, CHAINED_DEAD_KEY),	// ‹Greek› -> ‹Crochet en chef + Greek›
  DEADTRANS(0x1ebb, 0x00df, 0x088d, CHAINED_DEAD_KEY),	// ‹Bépo Latin› -> ‹Crochet en chef + Bépo Latin›
  DEADTRANS(0x1ebb, 0x02c6, 0x088e, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Crochet en chef + Circumflex Accent›
  DEADTRANS(0x1ebb, 0x02d8, 0x088f, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Crochet en chef + Breve›
  DEADTRANS(0x1ebb, 0x02d9, 0x0890, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Crochet en chef + Dot Above›
  DEADTRANS(0x1ebb, 0x031b, 0x0891, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Crochet en chef + Horn›
  DEADTRANS(0x1ebb, 0x0336, 0x0892, CHAINED_DEAD_KEY),	// ‹Long Stroke Overlay› -> ‹Crochet en chef + Long Stroke Overlay›
  DEADTRANS(0x1ebb, 0x1ebb, 0x0181, CHAINED_DEAD_KEY),	// ‹Crochet en chef› -> ‹Crosse›
  DEADTRANS(0x1ebb, L' '  , 0x1ebb, NORMAL_CHARACTER),	// U+0020 -> ‘ẻ’

// Dead key: Diaeresis Below
  DEADTRANS(0x2025, L'.'  , 0x0324, NORMAL_CHARACTER),	// U+002E -> U+0324
  DEADTRANS(0x2025, L':'  , 0x0324, NORMAL_CHARACTER),	// U+003A -> U+0324
  DEADTRANS(0x2025, L'?'  , 0x0324, NORMAL_CHARACTER),	// U+003F -> U+0324
  DEADTRANS(0x2025, L'U'  , 0x1e72, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṳ’
  DEADTRANS(0x2025, L'u'  , 0x1e73, NORMAL_CHARACTER),	// ‘u’ -> ‘ṳ’
  DEADTRANS(0x2025, 0x00b7, 0x0324, NORMAL_CHARACTER),	// U+00B7 -> U+0324
  DEADTRANS(0x2025, 0x00bf, 0x0324, NORMAL_CHARACTER),	// U+00BF -> U+0324
  DEADTRANS(0x2025, 0x2019, 0x0324, NORMAL_CHARACTER),	// U+2019 -> U+0324
  DEADTRANS(0x2025, 0x2026, 0x0324, NORMAL_CHARACTER),	// U+2026 -> U+0324
  DEADTRANS(0x2025, 0x1ebb, 0x0324, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0324
  DEADTRANS(0x2025, L' '  , 0x2025, NORMAL_CHARACTER),	// U+0020 -> U+2025

// Dead key: Circumflex Accent Below
  DEADTRANS(0x2038, L'.'  , 0x032d, NORMAL_CHARACTER),	// U+002E -> U+032D
  DEADTRANS(0x2038, L':'  , 0x032d, NORMAL_CHARACTER),	// U+003A -> U+032D
  DEADTRANS(0x2038, L'?'  , 0x032d, NORMAL_CHARACTER),	// U+003F -> U+032D
  DEADTRANS(0x2038, L'D'  , 0x1e12, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḓ’
  DEADTRANS(0x2038, L'E'  , 0x1e18, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḙ’
  DEADTRANS(0x2038, L'L'  , 0x1e3c, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḽ’
  DEADTRANS(0x2038, L'N'  , 0x1e4a, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṋ’
  DEADTRANS(0x2038, L'T'  , 0x1e70, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṱ’
  DEADTRANS(0x2038, L'U'  , 0x1e76, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṷ’
  DEADTRANS(0x2038, L'd'  , 0x1e13, NORMAL_CHARACTER),	// ‘d’ -> ‘ḓ’
  DEADTRANS(0x2038, L'e'  , 0x1e19, NORMAL_CHARACTER),	// ‘e’ -> ‘ḙ’
  DEADTRANS(0x2038, L'l'  , 0x1e3d, NORMAL_CHARACTER),	// ‘l’ -> ‘ḽ’
  DEADTRANS(0x2038, L'n'  , 0x1e4b, NORMAL_CHARACTER),	// ‘n’ -> ‘ṋ’
  DEADTRANS(0x2038, L't'  , 0x1e71, NORMAL_CHARACTER),	// ‘t’ -> ‘ṱ’
  DEADTRANS(0x2038, L'u'  , 0x1e77, NORMAL_CHARACTER),	// ‘u’ -> ‘ṷ’
  DEADTRANS(0x2038, 0x00b7, 0x032d, NORMAL_CHARACTER),	// U+00B7 -> U+032D
  DEADTRANS(0x2038, 0x00bf, 0x032d, NORMAL_CHARACTER),	// U+00BF -> U+032D
  DEADTRANS(0x2038, 0x2019, 0x032d, NORMAL_CHARACTER),	// U+2019 -> U+032D
  DEADTRANS(0x2038, 0x2026, 0x032d, NORMAL_CHARACTER),	// U+2026 -> U+032D
  DEADTRANS(0x2038, 0x1ebb, 0x032d, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+032D
  DEADTRANS(0x2038, L' '  , 0x2038, NORMAL_CHARACTER),	// U+0020 -> U+2038

// Dead key: Bépo Science
  DEADTRANS(0x221e, L'!'  , 0x21d2, NORMAL_CHARACTER),	// U+0021 -> U+21D2
  DEADTRANS(0x221e, L'"'  , 0x2644, NORMAL_CHARACTER),	// U+0022 -> U+2644
  DEADTRANS(0x221e, L'#'  , 0x263f, NORMAL_CHARACTER),	// U+0023 -> U+263F
  DEADTRANS(0x221e, L'$'  , 0x2640, NORMAL_CHARACTER),	// U+0024 -> U+2640
  DEADTRANS(0x221e, L'\'' , 0x2283, NORMAL_CHARACTER),	// U+0027 -> U+2283
  DEADTRANS(0x221e, L'('  , 0x230a, NORMAL_CHARACTER),	// U+0028 -> U+230A
  DEADTRANS(0x221e, L')'  , 0x230b, NORMAL_CHARACTER),	// U+0029 -> U+230B
  DEADTRANS(0x221e, L'*'  , 0x2297, NORMAL_CHARACTER),	// U+002A -> U+2297
  DEADTRANS(0x221e, L'+'  , 0x2295, NORMAL_CHARACTER),	// U+002B -> U+2295
  DEADTRANS(0x221e, L','  , 0x220b, NORMAL_CHARACTER),	// U+002C -> U+220B
  DEADTRANS(0x221e, L'-'  , 0x221e, NORMAL_CHARACTER),	// U+002D -> U+221E
  DEADTRANS(0x221e, L'.'  , 0x2219, NORMAL_CHARACTER),	// U+002E -> U+2219
  DEADTRANS(0x221e, L'0'  , 0x2217, NORMAL_CHARACTER),	// ‘0’ -> U+2217
  DEADTRANS(0x221e, L'1'  , 0x2643, NORMAL_CHARACTER),	// ‘1’ -> U+2643
  DEADTRANS(0x221e, L'2'  , 0x226a, NORMAL_CHARACTER),	// ‘2’ -> U+226A
  DEADTRANS(0x221e, L'3'  , 0x226b, NORMAL_CHARACTER),	// ‘3’ -> U+226B
  DEADTRANS(0x221e, L'4'  , 0x2308, NORMAL_CHARACTER),	// ‘4’ -> U+2308
  DEADTRANS(0x221e, L'5'  , 0x2309, NORMAL_CHARACTER),	// ‘5’ -> U+2309
  DEADTRANS(0x221e, L'6'  , 0x27fc, NORMAL_CHARACTER),	// ‘6’ -> U+27FC
  DEADTRANS(0x221e, L'7'  , 0x229e, NORMAL_CHARACTER),	// ‘7’ -> U+229E
  DEADTRANS(0x221e, L'8'  , 0x221d, NORMAL_CHARACTER),	// ‘8’ -> U+221D
  DEADTRANS(0x221e, L':'  , 0x220e, NORMAL_CHARACTER),	// U+003A -> U+220E
  DEADTRANS(0x221e, L'<'  , 0x227a, NORMAL_CHARACTER),	// U+003C -> U+227A
  DEADTRANS(0x221e, L'='  , 0x2194, NORMAL_CHARACTER),	// U+003D -> U+2194
  DEADTRANS(0x221e, L'>'  , 0x227b, NORMAL_CHARACTER),	// U+003E -> U+227B
  DEADTRANS(0x221e, L'?'  , 0x22ee, NORMAL_CHARACTER),	// U+003F -> U+22EE
  DEADTRANS(0x221e, L'@'  , 0x27f6, NORMAL_CHARACTER),	// U+0040 -> U+27F6
  DEADTRANS(0x221e, L'A'  , 0x2200, NORMAL_CHARACTER),	// ‘A’ -> U+2200
  DEADTRANS(0x221e, L'B'  , 0x2136, NORMAL_CHARACTER),	// ‘B’ -> ‘ℶ’
  DEADTRANS(0x221e, L'C'  , 0x2201, NORMAL_CHARACTER),	// ‘C’ -> U+2201
  DEADTRANS(0x221e, L'D'  , 0x2206, NORMAL_CHARACTER),	// ‘D’ -> U+2206
  DEADTRANS(0x221e, L'E'  , 0x2203, NORMAL_CHARACTER),	// ‘E’ -> U+2203
  DEADTRANS(0x221e, L'F'  , 0x0895, CHAINED_DEAD_KEY),	// ‘F’ -> ‹Bépo Science + F›
  DEADTRANS(0x221e, L'I'  , 0x0896, CHAINED_DEAD_KEY),	// ‘I’ -> ‹Bépo Science + I›
  DEADTRANS(0x221e, L'J'  , 0x2193, NORMAL_CHARACTER),	// ‘J’ -> U+2193
  DEADTRANS(0x221e, L'K'  , 0x2972, NORMAL_CHARACTER),	// ‘K’ -> U+2972
  DEADTRANS(0x221e, L'L'  , 0x2114, NORMAL_CHARACTER),	// ‘L’ -> U+2114
  DEADTRANS(0x221e, L'N'  , 0x2135, NORMAL_CHARACTER),	// ‘N’ -> ‘ℵ’
  DEADTRANS(0x221e, L'O'  , 0x2205, NORMAL_CHARACTER),	// ‘O’ -> U+2205
  DEADTRANS(0x221e, L'P'  , 0x220f, NORMAL_CHARACTER),	// ‘P’ -> U+220F
  DEADTRANS(0x221e, L'Q'  , 0x2225, NORMAL_CHARACTER),	// ‘Q’ -> U+2225
  DEADTRANS(0x221e, L'S'  , 0x2211, NORMAL_CHARACTER),	// ‘S’ -> U+2211
  DEADTRANS(0x221e, L'T'  , 0x22a4, NORMAL_CHARACTER),	// ‘T’ -> U+22A4
  DEADTRANS(0x221e, L'U'  , 0x2229, NORMAL_CHARACTER),	// ‘U’ -> U+2229
  DEADTRANS(0x221e, L'V'  , 0x221a, NORMAL_CHARACTER),	// ‘V’ -> U+221A
  DEADTRANS(0x221e, L'W'  , 0x223f, NORMAL_CHARACTER),	// ‘W’ -> U+223F
  DEADTRANS(0x221e, L'Z'  , 0x2125, NORMAL_CHARACTER),	// ‘Z’ -> U+2125
  DEADTRANS(0x221e, L'['  , 0x228f, NORMAL_CHARACTER),	// U+005B -> U+228F
  DEADTRANS(0x221e, L'\\' , 0x2216, NORMAL_CHARACTER),	// U+005C -> U+2216
  DEADTRANS(0x221e, L']'  , 0x2290, NORMAL_CHARACTER),	// U+005D -> U+2290
  DEADTRANS(0x221e, L'^'  , 0x202d, NORMAL_CHARACTER),	// U+005E -> U+202D
  DEADTRANS(0x221e, L'b'  , 0x0897, CHAINED_DEAD_KEY),	// ‘b’ -> ‹Bépo Science + b›
  DEADTRANS(0x221e, L'c'  , 0x0898, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Bépo Science + c›
  DEADTRANS(0x221e, L'd'  , 0x2202, NORMAL_CHARACTER),	// ‘d’ -> U+2202
  DEADTRANS(0x221e, L'e'  , 0x2208, NORMAL_CHARACTER),	// ‘e’ -> U+2208
  DEADTRANS(0x221e, L'f'  , 0x0899, CHAINED_DEAD_KEY),	// ‘f’ -> ‹Bépo Science + f›
  DEADTRANS(0x221e, L'g'  , 0x089a, CHAINED_DEAD_KEY),	// ‘g’ -> ‹Bépo Science + g›
  DEADTRANS(0x221e, L'h'  , 0x210e, NORMAL_CHARACTER),	// ‘h’ -> ‘ℎ’
  DEADTRANS(0x221e, L'i'  , 0x089b, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Bépo Science + i›
  DEADTRANS(0x221e, L'j'  , 0x2191, NORMAL_CHARACTER),	// ‘j’ -> U+2191
  DEADTRANS(0x221e, L'k'  , 0x223c, NORMAL_CHARACTER),	// ‘k’ -> U+223C
  DEADTRANS(0x221e, L'l'  , 0x2113, NORMAL_CHARACTER),	// ‘l’ -> ‘ℓ’
  DEADTRANS(0x221e, L'n'  , 0x2016, NORMAL_CHARACTER),	// ‘n’ -> U+2016
  DEADTRANS(0x221e, L'o'  , 0x2218, NORMAL_CHARACTER),	// ‘o’ -> U+2218
  DEADTRANS(0x221e, L'p'  , 0x2210, NORMAL_CHARACTER),	// ‘p’ -> U+2210
  DEADTRANS(0x221e, L'q'  , 0x27c2, NORMAL_CHARACTER),	// ‘q’ -> U+27C2
  DEADTRANS(0x221e, L'r'  , 0x089c, CHAINED_DEAD_KEY),	// ‘r’ -> ‹Bépo Science + r›
  DEADTRANS(0x221e, L's'  , 0x222b, NORMAL_CHARACTER),	// ‘s’ -> U+222B
  DEADTRANS(0x221e, L't'  , 0x22a5, NORMAL_CHARACTER),	// ‘t’ -> U+22A5
  DEADTRANS(0x221e, L'u'  , 0x222a, NORMAL_CHARACTER),	// ‘u’ -> U+222A
  DEADTRANS(0x221e, L'v'  , 0x2228, NORMAL_CHARACTER),	// ‘v’ -> U+2228
  DEADTRANS(0x221e, L'w'  , 0x2248, NORMAL_CHARACTER),	// ‘w’ -> U+2248
  DEADTRANS(0x221e, L'x'  , 0x22ca, NORMAL_CHARACTER),	// ‘x’ -> U+22CA
  DEADTRANS(0x221e, L'y'  , 0x22c9, NORMAL_CHARACTER),	// ‘y’ -> U+22C9
  DEADTRANS(0x221e, L'z'  , 0x200b, NORMAL_CHARACTER),	// ‘z’ -> U+200B
  DEADTRANS(0x221e, L'|'  , 0x2223, NORMAL_CHARACTER),	// U+007C -> U+2223
  DEADTRANS(0x221e, L'~'  , 0x21aa, NORMAL_CHARACTER),	// U+007E -> U+21AA
  DEADTRANS(0x221e, 0x00a1, 0x21a6, NORMAL_CHARACTER),	// U+00A1 -> U+21A6
  DEADTRANS(0x221e, 0x00ab, 0x27e8, NORMAL_CHARACTER),	// U+00AB -> U+27E8
  DEADTRANS(0x221e, 0x00b0, 0x21d4, NORMAL_CHARACTER),	// U+00B0 -> U+21D4
  DEADTRANS(0x221e, 0x00b1, 0x228e, NORMAL_CHARACTER),	// U+00B1 -> U+228E
  DEADTRANS(0x221e, 0x00b6, 0x2641, NORMAL_CHARACTER),	// U+00B6 -> U+2641
  DEADTRANS(0x221e, 0x00b7, 0x20dc, NORMAL_CHARACTER),	// U+00B7 -> U+20DC
  DEADTRANS(0x221e, 0x00bb, 0x27e9, NORMAL_CHARACTER),	// U+00BB -> U+27E9
  DEADTRANS(0x221e, 0x00bf, 0x22f1, NORMAL_CHARACTER),	// U+00BF -> U+22F1
  DEADTRANS(0x221e, 0x00c8, 0x21d0, NORMAL_CHARACTER),	// ‘È’ -> U+21D0
  DEADTRANS(0x221e, 0x00c9, 0x214b, NORMAL_CHARACTER),	// ‘É’ -> U+214B
  DEADTRANS(0x221e, 0x00d7, 0x22c6, NORMAL_CHARACTER),	// U+00D7 -> U+22C6
  DEADTRANS(0x221e, 0x00e8, 0x2190, NORMAL_CHARACTER),	// ‘è’ -> U+2190
  DEADTRANS(0x221e, 0x00e9, 0x2227, NORMAL_CHARACTER),	// ‘é’ -> U+2227
  DEADTRANS(0x221e, 0x0153, 0x2300, NORMAL_CHARACTER),	// ‘œ’ -> U+2300
  DEADTRANS(0x221e, 0x017f, 0x222c, NORMAL_CHARACTER),	// ‘ſ’ -> U+222C
  DEADTRANS(0x221e, 0x2011, 0x21a0, NORMAL_CHARACTER),	// U+2011 -> U+21A0
  DEADTRANS(0x221e, 0x2013, 0x2642, NORMAL_CHARACTER),	// U+2013 -> U+2642
  DEADTRANS(0x221e, 0x2014, 0x2646, NORMAL_CHARACTER),	// U+2014 -> U+2646
  DEADTRANS(0x221e, 0x2019, 0x22ef, NORMAL_CHARACTER),	// U+2019 -> U+22EF
  DEADTRANS(0x221e, 0x201e, 0x2645, NORMAL_CHARACTER),	// U+201E -> U+2645
  DEADTRANS(0x221e, 0x2026, 0x20db, NORMAL_CHARACTER),	// U+2026 -> U+20DB
  DEADTRANS(0x221e, 0x2032, 0x21c4, NORMAL_CHARACTER),	// U+2032 -> U+21C4
  DEADTRANS(0x221e, 0x20ac, 0x2282, NORMAL_CHARACTER),	// U+20AC -> U+2282
  DEADTRANS(0x221e, 0x2122, 0x22a7, NORMAL_CHARACTER),	// U+2122 -> U+22A7
  DEADTRANS(0x221e, 0x2260, 0x2261, NORMAL_CHARACTER),	// U+2260 -> U+2261
  DEADTRANS(0x221e, 0x2620, 0x21c0, NORMAL_CHARACTER),	// U+2620 -> U+21C0
  DEADTRANS(0x221e, 0x2623, 0x22b2, NORMAL_CHARACTER),	// U+2623 -> U+22B2
  DEADTRANS(0x221e, 0x262d, 0x20d5, NORMAL_CHARACTER),	// U+262D -> U+20D5
  DEADTRANS(0x221e, 0x262e, 0x2a21, NORMAL_CHARACTER),	// U+262E -> U+2A21
  DEADTRANS(0x221e, 0x269c, 0x2393, NORMAL_CHARACTER),	// U+269C -> U+2393
  DEADTRANS(0x221e, 0x2a7d, 0x2264, NORMAL_CHARACTER),	// U+2A7D -> U+2264
  DEADTRANS(0x221e, 0x2a7e, 0x2265, NORMAL_CHARACTER),	// U+2A7E -> U+2265
  DEADTRANS(0x221e, L','  , 0x2103, NORMAL_CHARACTER),	// ‹Cedilla› -> U+2103
  DEADTRANS(0x221e, L'.'  , 0x210f, NORMAL_CHARACTER),	// ‹Dot Below› -> ‘ℏ’
  DEADTRANS(0x221e, L'/'  , 0x089d, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Bépo Science + Long Solidus Overlay›
  DEADTRANS(0x221e, L':'  , 0x2129, NORMAL_CHARACTER),	// ‹Diaeresis› -> U+2129
  DEADTRANS(0x221e, L';'  , 0x2287, NORMAL_CHARACTER),	// ‹Comma Below› -> U+2287
  DEADTRANS(0x221e, 0x00a4, 0x2286, NORMAL_CHARACTER),	// ‹Bépo Currency› -> U+2286
  DEADTRANS(0x221e, 0x00af, 0x089e, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Bépo Science + Macron›
  DEADTRANS(0x221e, 0x00b4, 0x21ba, NORMAL_CHARACTER),	// ‹Acute Accent› -> U+21BA
  DEADTRANS(0x221e, 0x00df, 0x222e, NORMAL_CHARACTER),	// ‹Bépo Latin› -> U+222E
  DEADTRANS(0x221e, 0x02c6, 0x2192, NORMAL_CHARACTER),	// ‹Circumflex Accent› -> U+2192
  DEADTRANS(0x221e, 0x02c7, 0x22bb, NORMAL_CHARACTER),	// ‹Caron› -> U+22BB
  DEADTRANS(0x221e, 0x02db, 0x2109, NORMAL_CHARACTER),	// ‹Ogonek› -> U+2109
  DEADTRANS(0x221e, 0x02dd, 0x21bb, NORMAL_CHARACTER),	// ‹Double Acute Accent› -> U+21BB
  DEADTRANS(0x221e, 0x1d49, 0x22a2, NORMAL_CHARACTER),	// ‹Superscript› -> U+22A2
  DEADTRANS(0x221e, 0x1e33, 0x210f, NORMAL_CHARACTER),	// ‹Bépo dot below› -> ‘ℏ’
  DEADTRANS(0x221e, 0x1ebb, 0x22f0, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+22F0
  DEADTRANS(0x221e, 0x221e, 0x2207, NORMAL_CHARACTER),	// ‹Bépo Science› -> U+2207
  DEADTRANS(0x221e, L' '  , 0x221e, NORMAL_CHARACTER),	// U+0020 -> U+221E

// Dead key: Doubled Long Stroke Overlay
  DEADTRANS(0x2550, L'L'  , 0x2c60, NORMAL_CHARACTER),	// ‘L’ -> ‘Ⱡ’
  DEADTRANS(0x2550, L'l'  , 0x2c61, NORMAL_CHARACTER),	// ‘l’ -> ‘ⱡ’
  DEADTRANS(0x2550, L' '  , 0x2550, NORMAL_CHARACTER),	// U+0020 -> U+2550

// Dead key: Tilde Overlay
  DEADTRANS(0x301c, L'+'  , 0x2a26, NORMAL_CHARACTER),	// U+002B -> U+2A26
  DEADTRANS(0x301c, L'.'  , 0x0330, NORMAL_CHARACTER),	// U+002E -> U+0330
  DEADTRANS(0x301c, L':'  , 0x0330, NORMAL_CHARACTER),	// U+003A -> U+0330
  DEADTRANS(0x301c, L'?'  , 0x0330, NORMAL_CHARACTER),	// U+003F -> U+0330
  DEADTRANS(0x301c, L'E'  , 0x1e1a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḛ’
  DEADTRANS(0x301c, L'I'  , 0x1e2c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḭ’
  DEADTRANS(0x301c, L'L'  , 0x2c62, NORMAL_CHARACTER),	// ‘L’ -> ‘Ɫ’
  DEADTRANS(0x301c, L'O'  , 0x019f, NORMAL_CHARACTER),	// ‘O’ -> ‘Ɵ’
  DEADTRANS(0x301c, L'U'  , 0x1e74, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṵ’
  DEADTRANS(0x301c, L'b'  , 0x1d6c, NORMAL_CHARACTER),	// ‘b’ -> ‘ᵬ’
  DEADTRANS(0x301c, L'd'  , 0x1d6d, NORMAL_CHARACTER),	// ‘d’ -> ‘ᵭ’
  DEADTRANS(0x301c, L'e'  , 0x1e1b, NORMAL_CHARACTER),	// ‘e’ -> ‘ḛ’
  DEADTRANS(0x301c, L'f'  , 0x1d6e, NORMAL_CHARACTER),	// ‘f’ -> ‘ᵮ’
  DEADTRANS(0x301c, L'i'  , 0x1e2d, NORMAL_CHARACTER),	// ‘i’ -> ‘ḭ’
  DEADTRANS(0x301c, L'l'  , 0x026b, NORMAL_CHARACTER),	// ‘l’ -> ‘ɫ’
  DEADTRANS(0x301c, L'm'  , 0x1d6f, NORMAL_CHARACTER),	// ‘m’ -> ‘ᵯ’
  DEADTRANS(0x301c, L'n'  , 0x1d70, NORMAL_CHARACTER),	// ‘n’ -> ‘ᵰ’
  DEADTRANS(0x301c, L'p'  , 0x1d71, NORMAL_CHARACTER),	// ‘p’ -> ‘ᵱ’
  DEADTRANS(0x301c, L'r'  , 0x1d72, NORMAL_CHARACTER),	// ‘r’ -> ‘ᵲ’
  DEADTRANS(0x301c, L's'  , 0x1d74, NORMAL_CHARACTER),	// ‘s’ -> ‘ᵴ’
  DEADTRANS(0x301c, L't'  , 0x1d75, NORMAL_CHARACTER),	// ‘t’ -> ‘ᵵ’
  DEADTRANS(0x301c, L'u'  , 0x1e75, NORMAL_CHARACTER),	// ‘u’ -> ‘ṵ’
  DEADTRANS(0x301c, L'z'  , 0x1d76, NORMAL_CHARACTER),	// ‘z’ -> ‘ᵶ’
  DEADTRANS(0x301c, 0x00b7, 0x0330, NORMAL_CHARACTER),	// U+00B7 -> U+0330
  DEADTRANS(0x301c, 0x00bf, 0x0330, NORMAL_CHARACTER),	// U+00BF -> U+0330
  DEADTRANS(0x301c, 0x2019, 0x0330, NORMAL_CHARACTER),	// U+2019 -> U+0330
  DEADTRANS(0x301c, 0x2026, 0x0330, NORMAL_CHARACTER),	// U+2026 -> U+0330
  DEADTRANS(0x301c, L'~'  , 0x08c0, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Tilde Overlay + Tilde›
  DEADTRANS(0x301c, 0x1ebb, 0x0330, NORMAL_CHARACTER),	// ‹Crochet en chef› -> U+0330
  DEADTRANS(0x301c, L' '  , 0x301c, NORMAL_CHARACTER),	// U+0020 -> U+301C

  0, 0
};

/***************************************************************************\\
 * Ligatures
\\***************************************************************************/
static ALLOC_SECTION_LDATA LIGATURE4 aLigature[] = {
  // Virtual key, Level index, Ligature sequence
  { VK_OEM_5     , 4 , 0x003c  , 0x00d8  , 0x002f  , 0x00dd   },
  { 0, 0, 0, 0, 0, 0 }
};


static ALLOC_SECTION_LDATA KBDTABLES KbdTables = {
  // Modifier keys
  &CharModifiers,

  // Characters tables
  aVkToWcharTable,

  // Diacritics
  aDeadKey,

  // Names of Keys
  aKeyNames,
  aKeyNamesExt,
  aKeyNamesDead,

  // Scan codes to Virtual Keys
  ausVK,
  sizeof(ausVK) / sizeof(ausVK[0]),
  aE0VscToVk,
  aE1VscToVk,

  // Locale-specific special processing
  MAKELONG(KLLF_ALTGR, KBD_VERSION),

  // Ligatures
  4, // Size of the longuest sequence
  sizeof(aLigature[0]), // Size of one entry in `aLigature`
  (PLIGATURE1)aLigature, // Pointer to `aLigature` table
};

PKBDTABLES KbdLayerDescriptor(VOID)
{
  return &KbdTables;
};
