
#define KBD_TYPE 4
#include "kbd.h"
#include <dontuse.h>

// Extra definitions for FE keyboards [FIXME]
#define VK_DBE_ALPHANUMERIC              0x0f0
#define VK_DBE_KATAKANA                  0x0f1
#define VK_DBE_HIRAGANA                  0x0f2
#define VK_DBE_ROMAN                     0x0f5
#define VK_DBE_NOROMAN                   0x0f6

// Modifiers bits
// Windows predefined values:
// * KBDSHIFT       1<<0
// * KBDCTRL        1<<1
// * KBDALT         1<<2
// * KBDKANA        1<<3
// * KBDROYA        1<<4
// * KBDLOYA        1<<5
// * KBDGRPSELTAP   1<<7
#define KBD_NUMERIC     1<<3
#define KBD_ISO_LEVEL_3 1<<4
#define KBD_ISO_LEVEL_5 1<<5

// Extra definitions of TYPEDEF_VK_TO_WCHARS
TYPEDEF_VK_TO_WCHARS(11) //  VK_TO_WCHARS11, *PVK_TO_WCHARS11;
TYPEDEF_VK_TO_WCHARS(12) //  VK_TO_WCHARS12, *PVK_TO_WCHARS12;
TYPEDEF_VK_TO_WCHARS(13) //  VK_TO_WCHARS13, *PVK_TO_WCHARS13;
TYPEDEF_VK_TO_WCHARS(14) //  VK_TO_WCHARS14, *PVK_TO_WCHARS14;
TYPEDEF_VK_TO_WCHARS(15) //  VK_TO_WCHARS15, *PVK_TO_WCHARS15;
TYPEDEF_VK_TO_WCHARS(16) //  VK_TO_WCHARS16, *PVK_TO_WCHARS16;
TYPEDEF_VK_TO_WCHARS(17) //  VK_TO_WCHARS17, *PVK_TO_WCHARS17;
TYPEDEF_VK_TO_WCHARS(18) //  VK_TO_WCHARS18, *PVK_TO_WCHARS18;
TYPEDEF_VK_TO_WCHARS(19) //  VK_TO_WCHARS19, *PVK_TO_WCHARS19;
TYPEDEF_VK_TO_WCHARS(20) //  VK_TO_WCHARS20, *PVK_TO_WCHARS20;
TYPEDEF_VK_TO_WCHARS(21) //  VK_TO_WCHARS21, *PVK_TO_WCHARS21;
TYPEDEF_VK_TO_WCHARS(22) //  VK_TO_WCHARS22, *PVK_TO_WCHARS22;
TYPEDEF_VK_TO_WCHARS(23) //  VK_TO_WCHARS23, *PVK_TO_WCHARS23;
TYPEDEF_VK_TO_WCHARS(24) //  VK_TO_WCHARS24, *PVK_TO_WCHARS24;
TYPEDEF_VK_TO_WCHARS(25) //  VK_TO_WCHARS25, *PVK_TO_WCHARS25;
TYPEDEF_VK_TO_WCHARS(26) //  VK_TO_WCHARS26, *PVK_TO_WCHARS26;
TYPEDEF_VK_TO_WCHARS(27) //  VK_TO_WCHARS27, *PVK_TO_WCHARS27;
TYPEDEF_VK_TO_WCHARS(28) //  VK_TO_WCHARS28, *PVK_TO_WCHARS28;
TYPEDEF_VK_TO_WCHARS(29) //  VK_TO_WCHARS29, *PVK_TO_WCHARS29;
TYPEDEF_VK_TO_WCHARS(30) //  VK_TO_WCHARS30, *PVK_TO_WCHARS30;
TYPEDEF_VK_TO_WCHARS(31) //  VK_TO_WCHARS31, *PVK_TO_WCHARS31;
TYPEDEF_VK_TO_WCHARS(32) //  VK_TO_WCHARS32, *PVK_TO_WCHARS32;

// Swap first arguments of the macro of the Windows API.
#undef DEADTRANS
#define DEADTRANS(dk, ch, comp, flags) { MAKELONG(ch, dk), comp, flags}

// Flag for the result of dead key combo
#define NORMAL_CHARACTER 0x0000
#define CHAINED_DEAD_KEY 0x0001 // DKF_DEAD

// Extra definitions of TYPEDEF_LIGATURE
TYPEDEF_LIGATURE(6)
TYPEDEF_LIGATURE(7)
TYPEDEF_LIGATURE(8)
TYPEDEF_LIGATURE(9)
TYPEDEF_LIGATURE(10)
TYPEDEF_LIGATURE(11)
TYPEDEF_LIGATURE(12)
TYPEDEF_LIGATURE(13)
TYPEDEF_LIGATURE(14)
TYPEDEF_LIGATURE(15)
TYPEDEF_LIGATURE(16)

// Mapping scancode -> virtual key
// Key Equals -> VK_OEM_2
#undef  T0D
#define T0D _EQ(OEM_2)
// Key Q -> VK_X
#undef  T10
#define T10 _EQ('X')
// Key W -> VK_V
#undef  T11
#define T11 _EQ('V')
// Key E -> VK_L
#undef  T12
#define T12 _EQ('L')
// Key R -> VK_C
#undef  T13
#define T13 _EQ('C')
// Key T -> VK_W
#undef  T14
#define T14 _EQ('W')
// Key Y -> VK_K
#undef  T15
#define T15 _EQ('K')
// Key U -> VK_H
#undef  T16
#define T16 _EQ('H')
// Key I -> VK_G
#undef  T17
#define T17 _EQ('G')
// Key O -> VK_F
#undef  T18
#define T18 _EQ('F')
// Key P -> VK_Q
#undef  T19
#define T19 _EQ('Q')
// Key LBracket -> VK_OEM_3
#undef  T1A
#define T1A _EQ(OEM_3)
// Key RBracket -> VK_OEM_4
#undef  T1B
#define T1B _EQ(OEM_4)
// Key A -> VK_U
#undef  T1E
#define T1E _EQ('U')
// Key S -> VK_I
#undef  T1F
#define T1F _EQ('I')
// Key D -> VK_A
#undef  T20
#define T20 _EQ('A')
// Key F -> VK_E
#undef  T21
#define T21 _EQ('E')
// Key G -> VK_O
#undef  T22
#define T22 _EQ('O')
// Key H -> VK_S
#undef  T23
#define T23 _EQ('S')
// Key J -> VK_N
#undef  T24
#define T24 _EQ('N')
// Key K -> VK_R
#undef  T25
#define T25 _EQ('R')
// Key L -> VK_T
#undef  T26
#define T26 _EQ('T')
// Key Semicolon -> VK_D
#undef  T27
#define T27 _EQ('D')
// Key Quote -> VK_Y
#undef  T28
#define T28 _EQ('Y')
// Key Grave -> VK_OEM_1
#undef  T29
#define T29 _EQ(OEM_1)
// Key Backslash -> VK_OEM_102
#undef  T2B
#define T2B _EQ(OEM_102)
// Key Z -> VK_OEM_5
#undef  T2C
#define T2C _EQ(OEM_5)
// Key X -> VK_OEM_6
#undef  T2D
#define T2D _EQ(OEM_6)
// Key C -> VK_OEM_7
#undef  T2E
#define T2E _EQ(OEM_7)
// Key V -> VK_P
#undef  T2F
#define T2F _EQ('P')
// Key B -> VK_Z
#undef  T30
#define T30 _EQ('Z')
// Key N -> VK_B
#undef  T31
#define T31 _EQ('B')
// Key Slash -> VK_J
#undef  T35
#define T35 _EQ('J')
// Key RAlternate -> VK_OEM_8
#undef  X38
#define X38 _EQ(OEM_8)
// Key CapsLock -> VK_OEM_102
#undef  T3A
#define T3A _EQ(OEM_102)
// Key Iso102 -> VK_OEM_8
#undef  T56
#define T56 _EQ(OEM_8)

