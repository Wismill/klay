
#include <windows.h>
#include "kbd.h"
#include "neo2.h"

#if defined(_M_IA64)
#pragma section(".data")
#define ALLOC_SECTION_LDATA __declspec(allocate(".data"))
#else
#pragma data_seg(".data")
#define ALLOC_SECTION_LDATA
#endif

// ausVK[] - Virtual Scan Code to Virtual Key conversion table
static ALLOC_SECTION_LDATA USHORT ausVK[] = {
  T00, T01, T02, T03, T04, T05, T06, T07,
  T08, T09, T0A, T0B, T0C, T0D, T0E, T0F,
  T10, T11, T12, T13, T14, T15, T16, T17,
  T18, T19, T1A, T1B, T1C, T1D, T1E, T1F,
  T20, T21, T22, T23, T24, T25, T26, T27,
  T28, T29, T2A, T2B, T2C, T2D, T2E, T2F,
  T30, T31, T32, T33, T34, T35,

  /*
   * Right-hand Shift key must have KBDEXT bit set.
   */
  T36 | KBDEXT,
  T37 | KBDMULTIVK,               // numpad_* + Shift/Alt -> SnapShot

  T38, T39, T3A, T3B, T3C, T3D, T3E,
  T3F, T40, T41, T42, T43, T44,

  /*
   * NumLock Key:
   *     KBDEXT     - VK_NUMLOCK is an Extended key
   *     KBDMULTIVK - VK_NUMLOCK or VK_PAUSE (without or with CTRL)
   */
  T45 | KBDEXT | KBDMULTIVK,
  T46 | KBDMULTIVK,

  /*
   * Number Pad keys:
   *     KBDNUMPAD  - digits 0-9 and decimal point.
   *     KBDSPECIAL - require special processing by Windows
   */
  T47 | KBDNUMPAD | KBDSPECIAL,   // Numpad 7 (Home)
  T48 | KBDNUMPAD | KBDSPECIAL,   // Numpad 8 (Up),
  T49 | KBDNUMPAD | KBDSPECIAL,   // Numpad 9 (PgUp),
  T4A,
  T4B | KBDNUMPAD | KBDSPECIAL,   // Numpad 4 (Left),
  T4C | KBDNUMPAD | KBDSPECIAL,   // Numpad 5 (Clear),
  T4D | KBDNUMPAD | KBDSPECIAL,   // Numpad 6 (Right),
  T4E,
  T4F | KBDNUMPAD | KBDSPECIAL,   // Numpad 1 (End),
  T50 | KBDNUMPAD | KBDSPECIAL,   // Numpad 2 (Down),
  T51 | KBDNUMPAD | KBDSPECIAL,   // Numpad 3 (PgDn),
  T52 | KBDNUMPAD | KBDSPECIAL,   // Numpad 0 (Ins),
  T53 | KBDNUMPAD | KBDSPECIAL,   // Numpad . (Del),

  T54, T55, T56, T57, T58, T59, T5A, T5B,
  T5C, T5D, T5E, T5F, T60, T61, T62, T63,
  T64, T65, T66, T67, T68, T69, T6A, T6B,
  T6C, T6D, T6E, T6F, T70, T71, T72, T73,
  T74, T75, T76, T77, T78, T79, T7A, T7B,
  T7C, T7D, T7E, T7F
};

static ALLOC_SECTION_LDATA VSC_VK aE0VscToVk[] = {
  { 0x10, X10 | KBDEXT              },  // Speedracer: Previous Track
  { 0x19, X19 | KBDEXT              },  // Speedracer: Next Track
  { 0x1D, X1D | KBDEXT              },  // RControl
  { 0x20, X20 | KBDEXT              },  // Speedracer: Volume Mute
  { 0x21, X21 | KBDEXT              },  // Speedracer: Launch App 2
  { 0x22, X22 | KBDEXT              },  // Speedracer: Media Play/Pause
  { 0x24, X24 | KBDEXT              },  // Speedracer: Media Stop
  { 0x2E, X2E | KBDEXT              },  // Speedracer: Volume Down
  { 0x30, X30 | KBDEXT              },  // Speedracer: Volume Up
  { 0x32, X32 | KBDEXT              },  // Speedracer: Browser Home
  { 0x35, X35 | KBDEXT              },  // Numpad Divide
  { 0x37, X37 | KBDEXT              },  // Snapshot
  { 0x38, X38 | KBDEXT              },  // RMenu
  { 0x47, X47 | KBDEXT              },  // Home
  { 0x48, X48 | KBDEXT              },  // Up
  { 0x49, X49 | KBDEXT              },  // Prior
  { 0x4B, X4B | KBDEXT              },  // Left
  { 0x4D, X4D | KBDEXT              },  // Right
  { 0x4F, X4F | KBDEXT              },  // End
  { 0x50, X50 | KBDEXT              },  // Down
  { 0x51, X51 | KBDEXT              },  // Next
  { 0x52, X52 | KBDEXT              },  // Insert
  { 0x53, X53 | KBDEXT              },  // Delete
  { 0x5B, X5B | KBDEXT              },  // Left Win
  { 0x5C, X5C | KBDEXT              },  // Right Win
  { 0x5D, X5D | KBDEXT              },  // Application
  { 0x5F, X5F | KBDEXT              },  // Speedracer: Sleep
  { 0x65, X65 | KBDEXT              },  // Speedracer: Browser Search
  { 0x66, X66 | KBDEXT              },  // Speedracer: Browser Favorites
  { 0x67, X67 | KBDEXT              },  // Speedracer: Browser Refresh
  { 0x68, X68 | KBDEXT              },  // Speedracer: Browser Stop
  { 0x69, X69 | KBDEXT              },  // Speedracer: Browser Forward
  { 0x6A, X6A | KBDEXT              },  // Speedracer: Browser Back
  { 0x6B, X6B | KBDEXT              },  // Speedracer: Launch App 1
  { 0x6C, X6C | KBDEXT              },  // Speedracer: Launch Mail
  { 0x6D, X6D | KBDEXT              },  // Speedracer: Launch Media Selector
  { 0x1C, X1C | KBDEXT              },  // Numpad Enter
  { 0x46, X46 | KBDEXT              },  // Break (Ctrl + Pause)
  { 0,      0                       }
};

static ALLOC_SECTION_LDATA VSC_VK aE1VscToVk[] = {
  { 0x1D, Y1D                       },  // Pause
  { 0   ,   0                       }
};

// aVkToBits[] - map Virtual Keys to Modifier Bits
static ALLOC_SECTION_LDATA VK_TO_BIT aVkToBits[] = {
  { VK_CONTROL   , KBDCTRL                        },
  { VK_MENU      , KBDALT                         },
  { VK_OEM_102   , KBD_ISO_LEVEL_3                },
  { VK_OEM_8     , KBD_ISO_LEVEL_5                },
  { VK_SHIFT     , KBDSHIFT                       },
  { 0, 0 }
};

// aModification[] - map character modifier bits to modification number
static ALLOC_SECTION_LDATA MODIFIERS CharModifiers = {
  &aVkToBits[0],
  34,
  {
  // Level index -> Modifiers pressed                                   -> Level name
    0            /*                                                     -> Klein Buchstaben         */,
    1            /* Shift                                               -> Groß Buchstaben          */,
    2            /*       Control                                       -> Control                  */,
    SHFT_INVALID /* Shift Control                                       -> (No valid level)         */,
    SHFT_INVALID /*               Alternate                             -> (No valid level)         */,
    SHFT_INVALID /* Shift         Alternate                             -> (No valid level)         */,
    SHFT_INVALID /*       Control Alternate                             -> (No valid level)         */,
    SHFT_INVALID /* Shift Control Alternate                             -> (No valid level)         */,
    SHFT_INVALID /*                         Numeric                     -> (No valid level)         */,
    SHFT_INVALID /* Shift                   Numeric                     -> (No valid level)         */,
    SHFT_INVALID /*       Control           Numeric                     -> (No valid level)         */,
    SHFT_INVALID /* Shift Control           Numeric                     -> (No valid level)         */,
    SHFT_INVALID /*               Alternate Numeric                     -> (No valid level)         */,
    SHFT_INVALID /* Shift         Alternate Numeric                     -> (No valid level)         */,
    SHFT_INVALID /*       Control Alternate Numeric                     -> (No valid level)         */,
    SHFT_INVALID /* Shift Control Alternate Numeric                     -> (No valid level)         */,
    3            /*                                 IsoLevel3           -> Sonder Zeichen           */,
    4            /* Shift                           IsoLevel3           -> Griechisch               */,
    SHFT_INVALID /*       Control                   IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift Control                   IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /*               Alternate         IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift         Alternate         IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /*       Control Alternate         IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift Control Alternate         IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /*                         Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift                   Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /*       Control           Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift Control           Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /*               Alternate Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift         Alternate Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /*       Control Alternate Numeric IsoLevel3           -> (No valid level)         */,
    SHFT_INVALID /* Shift Control Alternate Numeric IsoLevel3           -> (No valid level)         */,
    5            /*                                           IsoLevel5 -> Tabellen Kalkulation     */,
    6            /* Shift                                     IsoLevel5 -> Wissenschaft             */
  }
};

/***************************************************************************\\
*
* aVkToWch<n>[]  - Virtual Key to WCHAR translation for <n> shift states
*
* Table attributes: Unordered Scan, null-terminated
*
* Search this table for an entry with a matching Virtual Key to find the
* corresponding unshifted and shifted WCHAR characters.
*
* Special values for VirtualKey (column 1)
*     0xff          - dead chars for the previous entry
*     0             - terminate the list
*
* Special values for Attributes (column 2)
*     CAPLOK bit    - CAPS-LOCK affect this key like SHIFT
*
* Special values for wch[*] (column 3 & 4)
*     WCH_NONE      - No character
*     WCH_DEAD      - Dead Key (diaresis) or invalid (US keyboard has none)
*     WCH_LGTR      - Ligature (generates multiple characters)
*
\\***************************************************************************/

// Errors encountered while creating aVkToWch tables:

static ALLOC_SECTION_LDATA VK_TO_WCHARS2 aVkToWchOther2[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 
  /*                                KP_Decimal, KP_Decimal */
  {VK_ABNT_C2   , 0               , L'.'    , L'.'    },
  /*                                KP_Add  , KP_Add   */
  {VK_ADD       , 0               , L'+'    , L'+'    },
  /*                                KP_Equal, KP_Equal */
  {VK_CLEAR     , 0               , L'='    , L'='    },
  /*                                KP_Separator, KP_Separator */
  {VK_DECIMAL   , 0               , L','    , L','    },
  /*                                KP_Divide, KP_Divide */
  {VK_DIVIDE    , 0               , L'/'    , L'/'    },
  /*                                KP_Multiply, KP_Multiply */
  {VK_MULTIPLY  , 0               , L'*'    , L'*'    },
  /*                                KP_Subtract, KP_Subtract */
  {VK_SUBTRACT  , 0               , L'-'    , L'-'    },
  {0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS3 aVkToWchOther3[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 
  /*                                BackSpace, BackSpace, U+007F ⌦ */
  {VK_BACK      , 0               , 0x0008  , 0x0008  , 0x007f  },
  /*                                U+0003 ␃, U+0003 ␃, U+0003 ␃ */
  {VK_CANCEL    , 0               , 0x0003  , 0x0003  , 0x0003  },
  /*                                Escape  , Escape  , Escape   */
  {VK_ESCAPE    , 0               , 0x001b  , 0x001b  , 0x001b  },
  /*                                Return  , Return  , U+000A ␊ */
  {VK_RETURN    , 0               , L'\r'   , L'\r'   , L'\n'   },
  {0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS4 aVkToWchOther4[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 , Level 3 
  /*                                Tab     , Tab     , �       , ‹Compose› */
  {VK_TAB       , 0               , L'\t'   , L'\t'   , WCH_NONE, WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, 0x266b  },
  {0, 0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS7 aVkToWchOther7[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 , Level 3 , Level 4 , Level 5 , Level 6 
  /*                                U+0030 0, U+201D ”, �       , U+2019 ’, U+2080 ₀, KP_Multiply, U+2205 ∅ */
  {'0'          , 0               , L'0'    , 0x201d  , WCH_NONE, 0x2019  , 0x2080  , L'*'    , 0x2205  },
  /*                                U+0031 1, U+00B0 °, �       , U+00B9 ¹, U+2081 ₁, U+00AA ª, U+00AC ¬ */
  {'1'          , 0               , L'1'    , 0x00b0  , WCH_NONE, 0x00b9  , 0x2081  , 0x00aa  , 0x00ac  },
  /*                                U+0032 2, U+00A7 §, �       , U+00B2 ², U+2082 ₂, U+00BA º, U+2228 ∨ */
  {'2'          , 0               , L'2'    , 0x00a7  , WCH_NONE, 0x00b2  , 0x2082  , 0x00ba  , 0x2228  },
  /*                                U+0033 3, U+2113 ℓ, �       , U+00B3 ³, U+2083 ₃, U+2116 №, U+2227 ∧ */
  {'3'          , 0               , L'3'    , 0x2113  , WCH_NONE, 0x00b3  , 0x2083  , 0x2116  , 0x2227  },
  /*                                U+0034 4, U+00BB », �       , U+203A ›, U+2640 ♀, �       , U+22A5 ⊥ */
  {'4'          , 0               , L'4'    , 0x00bb  , WCH_NONE, 0x203a  , 0x2640  , WCH_NONE, 0x22a5  },
  /*                                U+0035 5, U+00AB «, �       , U+2039 ‹, U+2642 ♂, U+00B7 ·, U+2221 ∡ */
  {'5'          , 0               , L'5'    , 0x00ab  , WCH_NONE, 0x2039  , 0x2642  , 0x00b7  , 0x2221  },
  /*                                U+0036 6, U+0024 $, �       , U+00A2 ¢, U+26A5 ⚥, U+00A3 £, U+2225 ∥ */
  {'6'          , 0               , L'6'    , L'$'    , WCH_NONE, 0x00a2  , 0x26a5  , 0x00a3  , 0x2225  },
  /*                                U+0037 7, U+20AC €, �       , U+00A5 ¥, U+03F0 ϰ, U+00A4 ¤, U+2192 → */
  {'7'          , 0               , L'7'    , 0x20ac  , WCH_NONE, 0x00a5  , 0x03f0  , 0x00a4  , 0x2192  },
  /*                                U+0038 8, U+201E „, �       , U+201A ‚, U+27E8 ⟨, Tab     , U+221E ∞ */
  {'8'          , 0               , L'8'    , 0x201e  , WCH_NONE, 0x201a  , 0x27e8  , L'\t'   , 0x221e  },
  /*                                U+0039 9, U+201C “, �       , U+2018 ‘, U+27E9 ⟩, KP_Divide, U+221D ∝ */
  {'9'          , 0               , L'9'    , 0x201c  , WCH_NONE, 0x2018  , 0x27e9  , L'/'    , 0x221d  },
  /*                                U+0061 a, U+0041 A, �       , U+007B {, U+03B1 α, �       , U+2200 ∀ */
  {'A'          , CAPLOK          , L'a'    , L'A'    , WCH_NONE, L'{'    , 0x03b1  , WCH_NONE, 0x2200  },
  /*                                U+0062 b, U+0042 B, �       , U+002B +, U+03B2 β, U+003A :, U+21D0 ⇐ */
  {'B'          , CAPLOK          , L'b'    , L'B'    , WCH_NONE, L'+'    , 0x03b2  , L':'    , 0x21d0  },
  /*                                U+0063 c, U+0043 C, �       , U+005D ], U+03C7 χ, �       , U+2102 ℂ */
  {'C'          , CAPLOK          , L'c'    , L'C'    , WCH_NONE, L']'    , 0x03c7  , WCH_NONE, 0x2102  },
  /*                                U+0064 d, U+0044 D, �       , U+003A :, U+03B4 δ, KP_Decimal, U+0394 Δ */
  {'D'          , CAPLOK          , L'd'    , L'D'    , WCH_NONE, L':'    , 0x03b4  , L'.'    , 0x0394  },
  /*                                U+0065 e, U+0045 E, �       , U+007D }, U+03B5 ε, �       , U+2203 ∃ */
  {'E'          , CAPLOK          , L'e'    , L'E'    , WCH_NONE, L'}'    , 0x03b5  , WCH_NONE, 0x2203  },
  /*                                U+0066 f, U+0046 F, �       , U+003D =, U+03C6 φ, KP_9    , U+03A6 Φ */
  {'F'          , CAPLOK          , L'f'    , L'F'    , WCH_NONE, L'='    , 0x03c6  , L'9'    , 0x03a6  },
  /*                                U+0067 g, U+0047 G, �       , U+003E >, U+03B3 γ, KP_8    , U+0393 Γ */
  {'G'          , CAPLOK          , L'g'    , L'G'    , WCH_NONE, L'>'    , 0x03b3  , L'8'    , 0x0393  },
  /*                                U+0068 h, U+0048 H, �       , U+003C <, U+03C8 ψ, KP_7    , U+03A8 Ψ */
  {'H'          , CAPLOK          , L'h'    , L'H'    , WCH_NONE, L'<'    , 0x03c8  , L'7'    , 0x03a8  },
  /*                                U+0069 i, U+0049 I, �       , U+002F /, U+03B9 ι, �       , U+222B ∫ */
  {'I'          , CAPLOK          , L'i'    , L'I'    , WCH_NONE, L'/'    , 0x03b9  , WCH_NONE, 0x222b  },
  /*                                U+006A j, U+004A J, �       , U+003B ;, U+03B8 θ, U+003B ;, U+0398 Θ */
  {'J'          , CAPLOK          , L'j'    , L'J'    , WCH_NONE, L';'    , 0x03b8  , L';'    , 0x0398  },
  /*                                U+006B k, U+004B K, �       , U+0021 !, U+03BA κ, U+00A1 ¡, U+00D7 × */
  {'K'          , CAPLOK          , L'k'    , L'K'    , WCH_NONE, L'!'    , 0x03ba  , 0x00a1  , 0x00d7  },
  /*                                U+006C l, U+004C L, �       , U+005B [, U+03BB λ, �       , U+039B Λ */
  {'L'          , CAPLOK          , L'l'    , L'L'    , WCH_NONE, L'['    , 0x03bb  , WCH_NONE, 0x039b  },
  /*                                U+006D m, U+004D M, �       , U+0025 %, U+03BC μ, KP_1    , U+21D4 ⇔ */
  {'M'          , CAPLOK          , L'm'    , L'M'    , WCH_NONE, L'%'    , 0x03bc  , L'1'    , 0x21d4  },
  /*                                U+006E n, U+004E N, �       , U+0028 (, U+03BD ν, KP_4    , U+2115 ℕ */
  {'N'          , CAPLOK          , L'n'    , L'N'    , WCH_NONE, L'('    , 0x03bd  , L'4'    , 0x2115  },
  /*                                U+006F o, U+004F O, �       , U+002A *, U+03BF ο, �       , U+2208 ∈ */
  {'O'          , CAPLOK          , L'o'    , L'O'    , WCH_NONE, L'*'    , 0x03bf  , WCH_NONE, 0x2208  },
  /*                                U+0070 p, U+0050 P, �       , U+007E ~, U+03C0 π, Return  , U+03A0 Π */
  {'P'          , CAPLOK          , L'p'    , L'P'    , WCH_NONE, L'~'    , 0x03c0  , L'\r'   , 0x03a0  },
  /*                                U+0071 q, U+0051 Q, �       , U+0026 &, U+03D5 ϕ, KP_Add  , U+211A ℚ */
  {'Q'          , CAPLOK          , L'q'    , L'Q'    , WCH_NONE, L'&'    , 0x03d5  , L'+'    , 0x211a  },
  /*                                U+0072 r, U+0052 R, �       , U+0029 ), U+03C1 ρ, KP_5    , U+211D ℝ */
  {'R'          , CAPLOK          , L'r'    , L'R'    , WCH_NONE, L')'    , 0x03c1  , L'5'    , 0x211d  },
  /*                                U+0073 s, U+0053 S, �       , U+003F ?, U+03C3 σ, U+00BF ¿, U+03A3 Σ */
  {'S'          , CAPLOK          , L's'    , L'S'    , WCH_NONE, L'?'    , 0x03c3  , 0x00bf  , 0x03a3  },
  /*                                U+0074 t, U+0054 T, �       , U+002D -, U+03C4 τ, KP_6    , U+2202 ∂ */
  {'T'          , CAPLOK          , L't'    , L'T'    , WCH_NONE, L'-'    , 0x03c4  , L'6'    , 0x2202  },
  /*                                U+0075 u, U+0055 U, �       , U+005C \, �       , �       , U+2282 ⊂ */
  {'U'          , CAPLOK          , L'u'    , L'U'    , WCH_NONE, L'\\'   , WCH_NONE, WCH_NONE, 0x2282  },
  /*                                U+0076 v, U+0056 V, �       , U+005F _, �       , BackSpace, U+221A √ */
  {'V'          , CAPLOK          , L'v'    , L'V'    , WCH_NONE, L'_'    , WCH_NONE, 0x0008  , 0x221a  },
  /*                                U+0077 w, U+0057 W, �       , U+005E ^, U+03C9 ω, �       , U+03A9 Ω */
  {'W'          , CAPLOK          , L'w'    , L'W'    , WCH_NONE, L'^'    , 0x03c9  , WCH_NONE, 0x03a9  },
  /*                                U+0078 x, U+0058 X, �       , U+2026 …, U+03BE ξ, �       , U+039E Ξ */
  {'X'          , CAPLOK          , L'x'    , L'X'    , WCH_NONE, 0x2026  , 0x03be  , WCH_NONE, 0x039e  },
  /*                                U+0079 y, U+0059 Y, �       , U+0040 @, U+03C5 υ, U+002E ., U+2207 ∇ */
  {'Y'          , CAPLOK          , L'y'    , L'Y'    , WCH_NONE, L'@'    , 0x03c5  , L'.'    , 0x2207  },
  /*                                U+007A z, U+005A Z, �       , U+0060 `, U+03B6 ζ, �       , U+2124 ℤ */
  {'Z'          , CAPLOK          , L'z'    , L'Z'    , WCH_NONE, L'`'    , 0x03b6  , WCH_NONE, 0x2124  },
  /*                                ‹Circumflex Accent›, ‹Caron› , �       , ‹Drehen›, �       , ‹Dot Above›, ‹Dot Below› */
  {VK_OEM_1     , 0               , WCH_DEAD, WCH_DEAD, WCH_NONE, WCH_DEAD, WCH_NONE, WCH_DEAD, WCH_DEAD},
  {0xff         , 0               , 0x02c6  , 0x02c7  , WCH_NONE, 0x21bb  , WCH_NONE, 0x02d9  , L'.'    },
  /*                                ‹Grave Accent›, ‹Cedilla›, �       , ‹Ring Above›, ‹Reversed Comma Above›, ‹Diaeresis›, ‹Macron› */
  {VK_OEM_2     , 0               , WCH_DEAD, WCH_DEAD, WCH_NONE, WCH_DEAD, WCH_DEAD, WCH_DEAD, WCH_DEAD},
  {0xff         , 0               , L'`'    , L','    , WCH_NONE, 0x02da  , 0x201b  , L':'    , 0x00af  },
  /*                                U+00DF ß, U+1E9E ẞ, �       , U+017F ſ, U+03C2 ς, U+2212 −, U+2218 ∘ */
  {VK_OEM_3     , CAPLOK          , 0x00df  , 0x1e9e  , WCH_NONE, 0x017f  , 0x03c2  , 0x2212  , 0x2218  },
  /*                                ‹Acute Accent›, ‹Tilde› , �       , ‹Long Solidus Overlay›, ‹Horn›  , ‹Double Acute Accent›, ‹Breve›  */
  {VK_OEM_4     , 0               , WCH_DEAD, WCH_DEAD, WCH_NONE, WCH_DEAD, WCH_DEAD, WCH_DEAD, WCH_DEAD},
  {0xff         , 0               , 0x00b4  , L'~'    , WCH_NONE, L'/'    , 0x031b  , 0x02dd  , 0x02d8  },
  /*                                U+00FC ü, U+00DC Ü, �       , U+0023 #, �       , Escape  , U+222A ∪ */
  {VK_OEM_5     , CAPLOK          , 0x00fc  , 0x00dc  , WCH_NONE, L'#'    , WCH_NONE, 0x001b  , 0x222a  },
  /*                                U+00F6 ö, U+00D6 Ö, �       , U+0024 $, U+03F5 ϵ, Tab     , U+2229 ∩ */
  {VK_OEM_6     , CAPLOK          , 0x00f6  , 0x00d6  , WCH_NONE, L'$'    , 0x03f5  , L'\t'   , 0x2229  },
  /*                                U+00E4 ä, U+00C4 Ä, �       , U+007C |, U+03B7 η, �       , U+2135 ℵ */
  {VK_OEM_7     , CAPLOK          , 0x00e4  , 0x00c4  , WCH_NONE, L'|'    , 0x03b7  , WCH_NONE, 0x2135  },
  /*                                U+002C ,, U+2013 –, �       , U+0022 ", U+03F1 ϱ, KP_2    , U+21D2 ⇒ */
  {VK_OEM_COMMA , 0               , L','    , 0x2013  , WCH_NONE, L'"'    , 0x03f1  , L'2'    , 0x21d2  },
  /*                                U+002D -, U+2014 —, �       , �       , U+2011 -̺, KP_Subtract, U+254C ╌ */
  {VK_OEM_MINUS , 0               , L'-'    , 0x2014  , WCH_NONE, WCH_NONE, 0x2011  , L'-'    , 0x254c  },
  /*                                U+002E ., U+2022 •, �       , U+0027 ', U+03D1 ϑ, KP_3    , U+21A6 ↦ */
  {VK_OEM_PERIOD, 0               , L'.'    , 0x2022  , WCH_NONE, L'\''   , 0x03d1  , L'3'    , 0x21a6  },
  /*                                U+0020 ␣, U+0020 ␣, U+0020 ␣, U+0020 ␣, U+00A0 ⍽, KP_0    , U+202F ⍽ ‹narrow› */
  {VK_SPACE     , 0               , L' '    , L' '    , L' '    , L' '    , 0x00a0  , L'0'    , 0x202f  },
  {0, 0, 0, 0, 0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS1 aVkToWchNumpad1[] = {
  // Virtual key, Modifier options, Level 0 
  /*                                KP_0     */
  {VK_NUMPAD0   , 0               , L'0'    },
  /*                                KP_1     */
  {VK_NUMPAD1   , 0               , L'1'    },
  /*                                KP_2     */
  {VK_NUMPAD2   , 0               , L'2'    },
  /*                                KP_3     */
  {VK_NUMPAD3   , 0               , L'3'    },
  /*                                KP_4     */
  {VK_NUMPAD4   , 0               , L'4'    },
  /*                                KP_5     */
  {VK_NUMPAD5   , 0               , L'5'    },
  /*                                KP_6     */
  {VK_NUMPAD6   , 0               , L'6'    },
  /*                                KP_7     */
  {VK_NUMPAD7   , 0               , L'7'    },
  /*                                KP_8     */
  {VK_NUMPAD8   , 0               , L'8'    },
  /*                                KP_9     */
  {VK_NUMPAD9   , 0               , L'9'    },
  {0, 0, 0}
};



// aVkToWcharTable - List the various aVkToWchN tables in use
static ALLOC_SECTION_LDATA VK_TO_WCHAR_TABLE aVkToWcharTable[] = {
  { (PVK_TO_WCHARS1)aVkToWchOther2 , 2 , sizeof(aVkToWchOther2[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther3 , 3 , sizeof(aVkToWchOther3[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther4 , 4 , sizeof(aVkToWchOther4[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther7 , 7 , sizeof(aVkToWchOther7[0]) },
  // The numpad keys must come last so that VkKeyScan interprets number characters
  // as coming from the main section of the kbd before considering the numpad.
  { (PVK_TO_WCHARS1)aVkToWchNumpad1 , 1 , sizeof(aVkToWchNumpad1[0]) },
  {                      NULL , 0 , 0                     },
};

/***************************************************************************\\
* aKeyNames[], aKeyNamesExt[] - Virtual Scancode to Key Name tables
*
* Table attributes: Ordered Scan (by scancode), null-terminated
*
* Only the names of Extended, NumPad, Dead and Non-Printable keys are here.
* (Keys producing printable characters are named by that character)
\\***************************************************************************/
static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNames[] = {
  0x01, L"Esc",
  0x0e, L"Backspace",
  0x0f, L"Tab",
  0x1c, L"Enter",
  0x1d, L"Left Ctrl",
  0x2a, L"Left Shift",
  0x36, L"Right Shift",
  0x37, L"Num *",
  0x38, L"Left Alt",
  0x39, L"Space",
  0x3a, L"Caps Lock",
  0x3b, L"F1",
  0x3c, L"F2",
  0x3d, L"F3",
  0x3e, L"F4",
  0x3f, L"F5",
  0x40, L"F6",
  0x41, L"F7",
  0x42, L"F8",
  0x43, L"F9",
  0x44, L"F10",
  0x45, L"Pause",
  0x46, L"Scroll Lock",
  0x47, L"Num 7",
  0x48, L"Num 8",
  0x49, L"Num 9",
  0x4a, L"Num -",
  0x4b, L"Num 4",
  0x4c, L"Num 5",
  0x4d, L"Num 6",
  0x4e, L"Num +",
  0x4f, L"Num 1",
  0x50, L"Num 2",
  0x51, L"Num 3",
  0x52, L"Num 0",
  0x53, L"Num Del",
  0x54, L"Sys Req",
  0x57, L"F11",
  0x58, L"F12",
  0x64, L"F13",
  0x65, L"F14",
  0x66, L"F15",
  0x67, L"F16",
  0x68, L"F17",
  0x69, L"F18",
  0x6a, L"F19",
  0x6b, L"F20",
  0x6c, L"F21",
  0x6d, L"F22",
  0x6e, L"F23",
  0x76, L"F24",
  0, NULL
};

static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNamesExt[] = {
  0x1c, L"Num Enter",
  0x1d, L"Right Ctrl",
  0x35, L"Num /",
  0x37, L"Prnt Scrn",
  0x38, L"Right Alt",
  0x45, L"Num Lock",
  0x46, L"Break",
  0x47, L"Home",
  0x48, L"Up",
  0x49, L"Page Up",
  0x4b, L"Left",
  0x4d, L"Right",
  0x4f, L"End",
  0x50, L"Down",
  0x51, L"Page Down",
  0x52, L"Insert",
  0x53, L"Delete",
  0x54, L"<00>",
  0x56, L"Help",
  0x5b, L"Left Windows",
  0x5c, L"Right Windows",
  0x5d, L"Application",
  0, NULL
};

/***************************************************************************\\
 * Dead Keys
\\***************************************************************************/
static ALLOC_SECTION_LDATA DEADKEY_LPWSTR aKeyNamesDead[] = {
  L","     L"Cedilla",
  L"."     L"Dot Below",
  L"/"     L"Long Solidus Overlay",
  L":"     L"Diaeresis",
  L"`"     L"Grave Accent",
  L"~"     L"Tilde",
  L"\u00af"   L"Macron",
  L"\u00b4"   L"Acute Accent",
  L"\u02c6"   L"Circumflex Accent",
  L"\u02c7"   L"Caron",
  L"\u02d8"   L"Breve",
  L"\u02d9"   L"Dot Above",
  L"\u02da"   L"Ring Above",
  L"\u02dd"   L"Double Acute Accent",
  L"\u02f7"   L"Tilde Below",
  L"\u030f"   L"Double Grave Accent",
  L"\u031b"   L"Horn",
  L"\u0378"   L"Cedilla + Acute Accent",
  L"\u0379"   L"Cedilla + Breve",
  L"\u0380"   L"Dot Below + Macron",
  L"\u0381"   L"Dot Below + Circumflex Accent",
  L"\u0382"   L"Dot Below + Breve",
  L"\u0383"   L"Dot Below + Dot Above",
  L"\u038b"   L"Dot Below + Horn",
  L"\u038d"   L"Long Solidus Overlay + Acute Accent",
  L"\u03a2"   L"Diaeresis + Grave Accent",
  L"\u0530"   L"Diaeresis + Tilde",
  L"\u0557"   L"Diaeresis + Macron",
  L"\u0558"   L"Diaeresis + Acute Accent",
  L"\u058b"   L"Diaeresis + Caron",
  L"\u058c"   L"Grave Accent + Diaeresis",
  L"\u0590"   L"Grave Accent + Macron",
  L"\u05c8"   L"Grave Accent + Circumflex Accent",
  L"\u05c9"   L"Grave Accent + Breve",
  L"\u05ca"   L"Grave Accent + Horn",
  L"\u05cb"   L"Grave Accent + Reversed Comma Above",
  L"\u05cc"   L"Tilde + Diaeresis",
  L"\u05cd"   L"Tilde + Macron",
  L"\u05ce"   L"Tilde + Acute Accent",
  L"\u05cf"   L"Tilde + Circumflex Accent",
  L"\u05eb"   L"Tilde + Breve",
  L"\u05ec"   L"Tilde + Horn",
  L"\u05ed"   L"Macron + Dot Below",
  L"\u05ee"   L"Macron + Diaeresis",
  L"\u05f5"   L"Macron + Grave Accent",
  L"\u05f6"   L"Macron + Tilde",
  L"\u05f7"   L"Macron + Acute Accent",
  L"\u05f8"   L"Macron + Dot Above",
  L"\u05f9"   L"Acute Accent + Cedilla",
  L"\u05fa"   L"Acute Accent + Long Solidus Overlay",
  L"\u05fb"   L"Acute Accent + Diaeresis",
  L"\u05fc"   L"Acute Accent + Tilde",
  L"\u05fd"   L"Acute Accent + Macron",
  L"\u05fe"   L"Acute Accent + Circumflex Accent",
  L"\u05ff"   L"Acute Accent + Breve",
  L"\u061d"   L"Acute Accent + Dot Above",
  L"\u070e"   L"Acute Accent + Ring Above",
  L"\u074b"   L"Acute Accent + Horn",
  L"\u074c"   L"Acute Accent + Reversed Comma Above",
  L"\u07b2"   L"Circumflex Accent + Dot Below",
  L"\u07b3"   L"Circumflex Accent + Grave Accent",
  L"\u07b4"   L"Circumflex Accent + Tilde",
  L"\u07b5"   L"Circumflex Accent + Acute Accent",
  L"\u07b6"   L"Caron + Diaeresis",
  L"\u07b7"   L"Caron + Dot Above",
  L"\u07b8"   L"Breve + Cedilla",
  L"\u07b9"   L"Breve + Dot Below",
  L"\u07ba"   L"Breve + Grave Accent",
  L"\u07bb"   L"Breve + Tilde",
  L"\u07bc"   L"Breve + Acute Accent",
  L"\u07bd"   L"Dot Above + Dot Below",
  L"\u07be"   L"Dot Above + Macron",
  L"\u07bf"   L"Dot Above + Acute Accent",
  L"\u07fb"   L"Dot Above + Caron",
  L"\u07fc"   L"Ring Above + Acute Accent",
  L"\u082e"   L"Horn + Dot Below",
  L"\u082f"   L"Horn + Grave Accent",
  L"\u083f"   L"Horn + Tilde",
  L"\u085c"   L"Horn + Acute Accent",
  L"\u085d"   L"Reversed Comma Above + Grave Accent",
  L"\u085f"   L"Reversed Comma Above + Acute Accent",
  L"\u086b"   L"Compose +  ",
  L"\u086c"   L"Compose + !",
  L"\u086d"   L"Compose + _",
  L"\u086e"   L"Compose + #",
  L"\u086f"   L"Compose + $",
  L"\u0870"   L"Compose + %",
  L"\u0871"   L"Compose + &",
  L"\u0872"   L"Compose + ’",
  L"\u0873"   L"Compose + (",
  L"\u0874"   L"Compose + )",
  L"\u0875"   L"Compose + *",
  L"\u0876"   L"Compose + +",
  L"\u0877"   L"Compose + ,",
  L"\u0878"   L"Compose + -",
  L"\u0879"   L"Compose + .",
  L"\u087a"   L"Compose + /",
  L"\u087b"   L"Compose + 0",
  L"\u087c"   L"Compose + 1",
  L"\u087d"   L"Compose + 2",
  L"\u087e"   L"Compose + 3",
  L"\u087f"   L"Compose + 4",
  L"\u0880"   L"Compose + 5",
  L"\u0881"   L"Compose + 7",
  L"\u0882"   L"Compose + 8",
  L"\u0883"   L"Compose + 9",
  L"\u0884"   L"Compose + :",
  L"\u0885"   L"Compose + ;",
  L"\u0886"   L"Compose + <",
  L"\u0887"   L"Compose + =",
  L"\u0888"   L"Compose + >",
  L"\u0889"   L"Compose + ?",
  L"\u088a"   L"Compose + @",
  L"\u088b"   L"Compose + A",
  L"\u088c"   L"Compose + C",
  L"\u088d"   L"Compose + D",
  L"\u088e"   L"Compose + E",
  L"\u088f"   L"Compose + F",
  L"\u0890"   L"Compose + G",
  L"\u0891"   L"Compose + H",
  L"\u0892"   L"Compose + I",
  L"\u0893"   L"Compose + L",
  L"\u0894"   L"Compose + M",
  L"\u0895"   L"Compose + N",
  L"\u0896"   L"Compose + O",
  L"\u0897"   L"Compose + R",
  L"\u0898"   L"Compose + S",
  L"\u0899"   L"Compose + T",
  L"\u089a"   L"Compose + V",
  L"\u089b"   L"Compose + W",
  L"\u089c"   L"Compose + X",
  L"\u089d"   L"Compose + Y",
  L"\u089e"   L"Compose + Z",
  L"\u089f"   L"Compose + [",
  L"\u08b5"   L"Compose + _",
  L"\u08be"   L"Compose + ]",
  L"\u08bf"   L"Compose + ^",
  L"\u08c0"   L"Compose + _",
  L"\u08c1"   L"Compose + `",
  L"\u08c2"   L"Compose + a",
  L"\u08c3"   L"Compose + b",
  L"\u08c4"   L"Compose + c",
  L"\u08c5"   L"Compose + d",
  L"\u08c6"   L"Compose + e",
  L"\u08c7"   L"Compose + f",
  L"\u08c8"   L"Compose + g",
  L"\u08c9"   L"Compose + h",
  L"\u08ca"   L"Compose + i",
  L"\u08cb"   L"Compose + j",
  L"\u08cc"   L"Compose + k",
  L"\u08cd"   L"Compose + l",
  L"\u08ce"   L"Compose + m",
  L"\u08cf"   L"Compose + n",
  L"\u08d0"   L"Compose + o",
  L"\u08d1"   L"Compose + p",
  L"\u08d2"   L"Compose + q",
  L"\u0984"   L"Compose + r",
  L"\u098d"   L"Compose + s",
  L"\u098e"   L"Compose + t",
  L"\u0991"   L"Compose + u",
  L"\u0992"   L"Compose + v",
  L"\u09a9"   L"Compose + w",
  L"\u09b1"   L"Compose + x",
  L"\u09b3"   L"Compose + y",
  L"\u09b4"   L"Compose + z",
  L"\u09b5"   L"Compose + {",
  L"\u09ba"   L"Compose + |",
  L"\u09bb"   L"Compose + }",
  L"\u09c5"   L"Compose + ~",
  L"\u09c6"   L"Compose + °",
  L"\u09c9"   L"Compose + ·",
  L"\u09ca"   L"Compose + ‘",
  L"\u09cf"   L"Compose + ’",
  L"\u09d0"   L"Compose + €",
  L"\u09d1"   L"Compose + ⟨",
  L"\u09d2"   L"Compose + ⟩",
  L"\u09d3"   L"Compose + Compose",
  L"\u09d4"   L"Compose +   + <",
  L"\u09d5"   L"Compose +   + >",
  L"\u09d6"   L"Compose +   + c",
  L"\u09d8"   L"Compose +   + h",
  L"\u09d9"   L"Compose +   + l",
  L"\u09da"   L"Compose +   + s",
  L"\u09db"   L"Compose + ! + -",
  L"\u09de"   L"Compose + ! + - + *",
  L"\u09e4"   L"Compose + ! + - + * + <",
  L"\u09e5"   L"Compose + ! + <",
  L"\u09ff"   L"Compose + ! + >",
  L"\u0a00"   L"Compose + ! + E",
  L"\u0a04"   L"Compose + ! + ~",
  L"\u0a0b"   L"Compose + % + <",
  L"\u0a0c"   L"Compose + & + !",
  L"\u0a0d"   L"Compose + & + ! + c",
  L"\u0a0e"   L"Compose + & + ! + c + o",
  L"\u0a11"   L"Compose + & + ! + c + o + n",
  L"\u0a12"   L"Compose + & + ! + c + o + n + t",
  L"\u0a29"   L"Compose + & + ! + c + o + n + t + a",
  L"\u0a31"   L"Compose + & + ! + c + o + n + t + a + i",
  L"\u0a34"   L"Compose + & + ! + c + o + n + t + a + i + n",
  L"\u0a37"   L"Compose + & + ! + e",
  L"\u0a3a"   L"Compose + & + ! + e + l",
  L"\u0a3b"   L"Compose + & + ! + e + l + e",
  L"\u0a3d"   L"Compose + & + ! + e + x",
  L"\u0a43"   L"Compose + & + ! + e + x + i",
  L"\u0a44"   L"Compose + & + ! + e + x + i + s",
  L"\u0a45"   L"Compose + & + ! + e + x + i + s + t",
  L"\u0a46"   L"Compose + & + ! + i",
  L"\u0a49"   L"Compose + & + ! + n",
  L"\u0a4a"   L"Compose + & + O",
  L"\u0a4e"   L"Compose + & + O + h",
  L"\u0a4f"   L"Compose + & + a",
  L"\u0a50"   L"Compose + & + a + n",
  L"\u0a52"   L"Compose + & + c",
  L"\u0a53"   L"Compose + & + c + o",
  L"\u0a54"   L"Compose + & + c + o + n",
  L"\u0a55"   L"Compose + & + c + o + n + t",
  L"\u0a56"   L"Compose + & + c + o + n + t + a",
  L"\u0a57"   L"Compose + & + c + o + n + t + a + i",
  L"\u0a58"   L"Compose + & + c + o + n + t + a + i + n",
  L"\u0a5d"   L"Compose + & + d",
  L"\u0a5f"   L"Compose + & + d + e",
  L"\u0a60"   L"Compose + & + e",
  L"\u0a61"   L"Compose + & + e + l",
  L"\u0a62"   L"Compose + & + e + l + e",
  L"\u0a63"   L"Compose + & + e + s",
  L"\u0a64"   L"Compose + & + e + s + t",
  L"\u0a65"   L"Compose + & + e + s + t + i",
  L"\u0a77"   L"Compose + & + e + x",
  L"\u0a78"   L"Compose + & + e + x + i",
  L"\u0a79"   L"Compose + & + e + x + i + s",
  L"\u0a7a"   L"Compose + & + e + x + i + s + t",
  L"\u0a7b"   L"Compose + & + f",
  L"\u0a7c"   L"Compose + & + f + o",
  L"\u0a7d"   L"Compose + & + f + o + r",
  L"\u0a7e"   L"Compose + & + f + o + r + a",
  L"\u0a7f"   L"Compose + & + f + o + r + a + l",
  L"\u0a80"   L"Compose + & + i",
  L"\u0a84"   L"Compose + & + n",
  L"\u0a8e"   L"Compose + & + n + a",
  L"\u0a92"   L"Compose + & + n + a + n",
  L"\u0aa9"   L"Compose + & + n + o",
  L"\u0ab1"   L"Compose + & + o",
  L"\u0ab4"   L"Compose + & + o + h",
  L"\u0aba"   L"Compose + & + x",
  L"\u0abb"   L"Compose + & + x + o",
  L"\u0ac6"   L"Compose + ’ + _",
  L"\u0aca"   L"Compose + ( + 2",
  L"\u0ace"   L"Compose + ( + v",
  L"\u0acf"   L"Compose + ( + v + 2",
  L"\u0ad1"   L"Compose + ) + 2",
  L"\u0ad2"   L"Compose + ) + v",
  L"\u0ad3"   L"Compose + ) + v + 2",
  L"\u0ad4"   L"Compose + * + %",
  L"\u0ad5"   L"Compose + - + *",
  L"\u0ad6"   L"Compose + - + * + <",
  L"\u0ad7"   L"Compose + - + * + ^",
  L"\u0ad8"   L"Compose + . + 3",
  L"\u0ad9"   L"Compose + / + 2",
  L"\u0ada"   L"Compose + 0 + /",
  L"\u0adb"   L"Compose + 1 + 1",
  L"\u0adc"   L"Compose + 1 + @",
  L"\u0add"   L"Compose + 2 + @",
  L"\u0ade"   L"Compose + 2 + `",
  L"\u0adf"   L"Compose + 2 + c",
  L"\u0ae4"   L"Compose + 2 + d",
  L"\u0ae5"   L"Compose + 2 + k",
  L"\u0af2"   L"Compose + 2 + m",
  L"\u0af3"   L"Compose + 3 + @",
  L"\u0af4"   L"Compose + 3 + c",
  L"\u0af5"   L"Compose + 3 + d",
  L"\u0af6"   L"Compose + 3 + k",
  L"\u0af7"   L"Compose + 3 + m",
  L"\u0af8"   L"Compose + = + !",
  L"\u0b00"   L"Compose + = + ! + *",
  L"\u0b04"   L"Compose + = + ! + * + <",
  L"\u0b0d"   L"Compose + = + *",
  L"\u0b0e"   L"Compose + = + * + <",
  L"\u0b11"   L"Compose + = + * + ^",
  L"\u0b12"   L"Compose + = + /",
  L"\u0b29"   L"Compose + = + d",
  L"\u0b31"   L"Compose + = + d + e",
  L"\u0b34"   L"Compose + = + |",
  L"\u0b3a"   L"Compose + @ + %",
  L"\u0b3b"   L"Compose + @ + % + Compose",
  L"\u0b45"   L"Compose + @ + % + Compose + Compose",
  L"\u0b46"   L"Compose + @ + n",
  L"\u0b49"   L"Compose + @ + Compose",
  L"\u0b4a"   L"Compose + @ + Compose + Compose",
  L"\u0b4e"   L"Compose + M + !",
  L"\u0b4f"   L"Compose + M + =",
  L"\u0b50"   L"Compose + M + = + !",
  L"\u0b51"   L"Compose + M + F",
  L"\u0b52"   L"Compose + M + I",
  L"\u0b53"   L"Compose + M + S",
  L"\u0b54"   L"Compose + M + b",
  L"\u0b55"   L"Compose + M + d",
  L"\u0b58"   L"Compose + M + f",
  L"\u0b59"   L"Compose + M + i",
  L"\u0b5a"   L"Compose + M + s",
  L"\u0b5b"   L"Compose + M + {",
  L"\u0b5e"   L"Compose + M + { + !",
  L"\u0b64"   L"Compose + [ + n",
  L"\u0b65"   L"Compose + ^ + _",
  L"\u0b78"   L"Compose + ^ + |",
  L"\u0b79"   L"Compose + _ + :",
  L"\u0b7a"   L"Compose + _ + <",
  L"\u0b7b"   L"Compose + _ + >",
  L"\u0b7c"   L"Compose + _ + `",
  L"\u0b7d"   L"Compose + _ + |",
  L"\u0b7e"   L"Compose + _ + ~",
  L"\u0b7f"   L"Compose + f + a",
  L"\u0b80"   L"Compose + p + e",
  L"\u0b81"   L"Compose + p + p",
  L"\u0b84"   L"Compose + q + e",
  L"\u0b8b"   L"Compose + t + e",
  L"\u0b8c"   L"Compose + w + *",
  L"\u0b8d"   L"Compose + w + * + <",
  L"\u0b91"   L"Compose + { + n",
  L"\u0b96"   L"Compose + ’ + _",
  L"\u0b97"   L"Compose + Compose + 2",
  L"\u0b98"   L"Compose + Compose + 9",
  L"\u0b9b"   L"Compose + Compose + @",
  L"\u0b9d"   L"Compose + Compose + @ + %",
  L"\u0ba0"   L"Compose + Compose + @ + % + Compose",
  L"\u0ba1"   L"Compose + Compose + @ + % + Compose + Compose",
  L"\u0ba2"   L"Compose + Compose + @ + Compose",
  L"\u0ba5"   L"Compose + Compose + @ + Compose + Compose",
  L"\u0ba6"   L"Compose + Compose + Compose",
  L"\u201b"   L"Reversed Comma Above",
  L"\u2025"   L"Diaeresis Below",
  L"\u20db"   L"Three Dots Above",
  L"\u20e8"   L"Triple Underdot",
  L"\u21bb"   L"Drehen",
  L"\u266b"   L"Compose",

  NULL
};

static ALLOC_SECTION_LDATA DEADKEY aDeadKey[] = {

// Dead key: Cedilla
  DEADTRANS(L','  , L'.'  , 0x0327, NORMAL_CHARACTER),	// U+002E -> U+0327
  DEADTRANS(L','  , L'C'  , 0x00c7, NORMAL_CHARACTER),	// ‘C’ -> ‘Ç’
  DEADTRANS(L','  , L'D'  , 0x1e10, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḑ’
  DEADTRANS(L','  , L'E'  , 0x0228, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȩ’
  DEADTRANS(L','  , L'G'  , 0x0122, NORMAL_CHARACTER),	// ‘G’ -> ‘Ģ’
  DEADTRANS(L','  , L'H'  , 0x1e28, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḩ’
  DEADTRANS(L','  , L'K'  , 0x0136, NORMAL_CHARACTER),	// ‘K’ -> ‘Ķ’
  DEADTRANS(L','  , L'L'  , 0x013b, NORMAL_CHARACTER),	// ‘L’ -> ‘Ļ’
  DEADTRANS(L','  , L'N'  , 0x0145, NORMAL_CHARACTER),	// ‘N’ -> ‘Ņ’
  DEADTRANS(L','  , L'R'  , 0x0156, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŗ’
  DEADTRANS(L','  , L'S'  , 0x015e, NORMAL_CHARACTER),	// ‘S’ -> ‘Ş’
  DEADTRANS(L','  , L'T'  , 0x0162, NORMAL_CHARACTER),	// ‘T’ -> ‘Ţ’
  DEADTRANS(L','  , L'c'  , 0x00e7, NORMAL_CHARACTER),	// ‘c’ -> ‘ç’
  DEADTRANS(L','  , L'd'  , 0x1e11, NORMAL_CHARACTER),	// ‘d’ -> ‘ḑ’
  DEADTRANS(L','  , L'e'  , 0x0229, NORMAL_CHARACTER),	// ‘e’ -> ‘ȩ’
  DEADTRANS(L','  , L'g'  , 0x0123, NORMAL_CHARACTER),	// ‘g’ -> ‘ģ’
  DEADTRANS(L','  , L'h'  , 0x1e29, NORMAL_CHARACTER),	// ‘h’ -> ‘ḩ’
  DEADTRANS(L','  , L'k'  , 0x0137, NORMAL_CHARACTER),	// ‘k’ -> ‘ķ’
  DEADTRANS(L','  , L'l'  , 0x013c, NORMAL_CHARACTER),	// ‘l’ -> ‘ļ’
  DEADTRANS(L','  , L'n'  , 0x0146, NORMAL_CHARACTER),	// ‘n’ -> ‘ņ’
  DEADTRANS(L','  , L'r'  , 0x0157, NORMAL_CHARACTER),	// ‘r’ -> ‘ŗ’
  DEADTRANS(L','  , L's'  , 0x015f, NORMAL_CHARACTER),	// ‘s’ -> ‘ş’
  DEADTRANS(L','  , L't'  , 0x0163, NORMAL_CHARACTER),	// ‘t’ -> ‘ţ’
  DEADTRANS(L','  , 0x00a0, 0x00b8, NORMAL_CHARACTER),	// U+00A0 -> U+00B8
  DEADTRANS(L','  , 0x00b4, 0x0378, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Cedilla + Acute Accent›
  DEADTRANS(L','  , 0x02d8, 0x0379, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Cedilla + Breve›
  DEADTRANS(L','  , L' '  , L','  , NORMAL_CHARACTER),	// U+0020 -> U+002C

// Dead key: Dot Below
  DEADTRANS(L'.'  , L'.'  , 0x0323, NORMAL_CHARACTER),	// U+002E -> U+0323
  DEADTRANS(L'.'  , L'A'  , 0x1ea0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ạ’
  DEADTRANS(L'.'  , L'B'  , 0x1e04, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḅ’
  DEADTRANS(L'.'  , L'D'  , 0x1e0c, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḍ’
  DEADTRANS(L'.'  , L'E'  , 0x1eb8, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẹ’
  DEADTRANS(L'.'  , L'H'  , 0x1e24, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḥ’
  DEADTRANS(L'.'  , L'I'  , 0x1eca, NORMAL_CHARACTER),	// ‘I’ -> ‘Ị’
  DEADTRANS(L'.'  , L'K'  , 0x1e32, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḳ’
  DEADTRANS(L'.'  , L'L'  , 0x1e36, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḷ’
  DEADTRANS(L'.'  , L'M'  , 0x1e42, NORMAL_CHARACTER),	// ‘M’ -> ‘Ṃ’
  DEADTRANS(L'.'  , L'N'  , 0x1e46, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṇ’
  DEADTRANS(L'.'  , L'O'  , 0x1ecc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ọ’
  DEADTRANS(L'.'  , L'R'  , 0x1e5a, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṛ’
  DEADTRANS(L'.'  , L'S'  , 0x1e62, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṣ’
  DEADTRANS(L'.'  , L'T'  , 0x1e6c, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṭ’
  DEADTRANS(L'.'  , L'U'  , 0x1ee4, NORMAL_CHARACTER),	// ‘U’ -> ‘Ụ’
  DEADTRANS(L'.'  , L'V'  , 0x1e7e, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṿ’
  DEADTRANS(L'.'  , L'W'  , 0x1e88, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẉ’
  DEADTRANS(L'.'  , L'Y'  , 0x1ef4, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỵ’
  DEADTRANS(L'.'  , L'Z'  , 0x1e92, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẓ’
  DEADTRANS(L'.'  , L'a'  , 0x1ea1, NORMAL_CHARACTER),	// ‘a’ -> ‘ạ’
  DEADTRANS(L'.'  , L'b'  , 0x1e05, NORMAL_CHARACTER),	// ‘b’ -> ‘ḅ’
  DEADTRANS(L'.'  , L'd'  , 0x1e0d, NORMAL_CHARACTER),	// ‘d’ -> ‘ḍ’
  DEADTRANS(L'.'  , L'e'  , 0x1eb9, NORMAL_CHARACTER),	// ‘e’ -> ‘ẹ’
  DEADTRANS(L'.'  , L'h'  , 0x1e25, NORMAL_CHARACTER),	// ‘h’ -> ‘ḥ’
  DEADTRANS(L'.'  , L'i'  , 0x1ecb, NORMAL_CHARACTER),	// ‘i’ -> ‘ị’
  DEADTRANS(L'.'  , L'k'  , 0x1e33, NORMAL_CHARACTER),	// ‘k’ -> ‘ḳ’
  DEADTRANS(L'.'  , L'l'  , 0x1e37, NORMAL_CHARACTER),	// ‘l’ -> ‘ḷ’
  DEADTRANS(L'.'  , L'm'  , 0x1e43, NORMAL_CHARACTER),	// ‘m’ -> ‘ṃ’
  DEADTRANS(L'.'  , L'n'  , 0x1e47, NORMAL_CHARACTER),	// ‘n’ -> ‘ṇ’
  DEADTRANS(L'.'  , L'o'  , 0x1ecd, NORMAL_CHARACTER),	// ‘o’ -> ‘ọ’
  DEADTRANS(L'.'  , L'r'  , 0x1e5b, NORMAL_CHARACTER),	// ‘r’ -> ‘ṛ’
  DEADTRANS(L'.'  , L's'  , 0x1e63, NORMAL_CHARACTER),	// ‘s’ -> ‘ṣ’
  DEADTRANS(L'.'  , L't'  , 0x1e6d, NORMAL_CHARACTER),	// ‘t’ -> ‘ṭ’
  DEADTRANS(L'.'  , L'u'  , 0x1ee5, NORMAL_CHARACTER),	// ‘u’ -> ‘ụ’
  DEADTRANS(L'.'  , L'v'  , 0x1e7f, NORMAL_CHARACTER),	// ‘v’ -> ‘ṿ’
  DEADTRANS(L'.'  , L'w'  , 0x1e89, NORMAL_CHARACTER),	// ‘w’ -> ‘ẉ’
  DEADTRANS(L'.'  , L'y'  , 0x1ef5, NORMAL_CHARACTER),	// ‘y’ -> ‘ỵ’
  DEADTRANS(L'.'  , L'z'  , 0x1e93, NORMAL_CHARACTER),	// ‘z’ -> ‘ẓ’
  DEADTRANS(L'.'  , 0x00a0, L'.'  , NORMAL_CHARACTER),	// U+00A0 -> U+002E
  DEADTRANS(L'.'  , 0x00af, 0x0380, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Dot Below + Macron›
  DEADTRANS(L'.'  , 0x02c6, 0x0381, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Dot Below + Circumflex Accent›
  DEADTRANS(L'.'  , 0x02d8, 0x0382, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Dot Below + Breve›
  DEADTRANS(L'.'  , 0x02d9, 0x0383, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Dot Below + Dot Above›
  DEADTRANS(L'.'  , 0x031b, 0x038b, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Dot Below + Horn›
  DEADTRANS(L'.'  , L' '  , 0xfbb3, NORMAL_CHARACTER),	// U+0020 -> U+FBB3

// Dead key: Long Solidus Overlay
  DEADTRANS(L'/'  , L'.'  , 0x0338, NORMAL_CHARACTER),	// U+002E -> U+0338
  DEADTRANS(L'/'  , L'0'  , 0x2205, NORMAL_CHARACTER),	// ‘0’ -> U+2205
  DEADTRANS(L'/'  , L'<'  , 0x226e, NORMAL_CHARACTER),	// U+003C -> U+226E
  DEADTRANS(L'/'  , L'='  , 0x2260, NORMAL_CHARACTER),	// U+003D -> U+2260
  DEADTRANS(L'/'  , L'>'  , 0x226f, NORMAL_CHARACTER),	// U+003E -> U+226F
  DEADTRANS(L'/'  , L'A'  , 0x023a, NORMAL_CHARACTER),	// ‘A’ -> ‘Ⱥ’
  DEADTRANS(L'/'  , L'C'  , 0x023b, NORMAL_CHARACTER),	// ‘C’ -> ‘Ȼ’
  DEADTRANS(L'/'  , L'E'  , 0x0246, NORMAL_CHARACTER),	// ‘E’ -> ‘Ɇ’
  DEADTRANS(L'/'  , L'G'  , 0xa7a0, NORMAL_CHARACTER),	// ‘G’ -> ‘Ꞡ’
  DEADTRANS(L'/'  , L'K'  , 0xa7a2, NORMAL_CHARACTER),	// ‘K’ -> ‘Ꞣ’
  DEADTRANS(L'/'  , L'L'  , 0x0141, NORMAL_CHARACTER),	// ‘L’ -> ‘Ł’
  DEADTRANS(L'/'  , L'N'  , 0xa7a4, NORMAL_CHARACTER),	// ‘N’ -> ‘Ꞥ’
  DEADTRANS(L'/'  , L'O'  , 0x00d8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ø’
  DEADTRANS(L'/'  , L'Q'  , 0xa758, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ꝙ’
  DEADTRANS(L'/'  , L'R'  , 0xa7a6, NORMAL_CHARACTER),	// ‘R’ -> ‘Ꞧ’
  DEADTRANS(L'/'  , L'S'  , 0xa7a8, NORMAL_CHARACTER),	// ‘S’ -> ‘Ꞩ’
  DEADTRANS(L'/'  , L'T'  , 0x023e, NORMAL_CHARACTER),	// ‘T’ -> ‘Ⱦ’
  DEADTRANS(L'/'  , L'V'  , 0xa75e, NORMAL_CHARACTER),	// ‘V’ -> ‘Ꝟ’
  DEADTRANS(L'/'  , L'a'  , 0x2c65, NORMAL_CHARACTER),	// ‘a’ -> ‘ⱥ’
  DEADTRANS(L'/'  , L'b'  , 0x2422, NORMAL_CHARACTER),	// ‘b’ -> U+2422
  DEADTRANS(L'/'  , L'c'  , 0x023c, NORMAL_CHARACTER),	// ‘c’ -> ‘ȼ’
  DEADTRANS(L'/'  , L'e'  , 0x0247, NORMAL_CHARACTER),	// ‘e’ -> ‘ɇ’
  DEADTRANS(L'/'  , L'f'  , 0x1e9c, NORMAL_CHARACTER),	// ‘f’ -> ‘ẜ’
  DEADTRANS(L'/'  , L'g'  , 0xa7a1, NORMAL_CHARACTER),	// ‘g’ -> ‘ꞡ’
  DEADTRANS(L'/'  , L'k'  , 0xa7a3, NORMAL_CHARACTER),	// ‘k’ -> ‘ꞣ’
  DEADTRANS(L'/'  , L'l'  , 0x0142, NORMAL_CHARACTER),	// ‘l’ -> ‘ł’
  DEADTRANS(L'/'  , L'n'  , 0xa7a5, NORMAL_CHARACTER),	// ‘n’ -> ‘ꞥ’
  DEADTRANS(L'/'  , L'o'  , 0x00f8, NORMAL_CHARACTER),	// ‘o’ -> ‘ø’
  DEADTRANS(L'/'  , L'p'  , 0x1d7d, NORMAL_CHARACTER),	// ‘p’ -> ‘ᵽ’
  DEADTRANS(L'/'  , L'q'  , 0xa759, NORMAL_CHARACTER),	// ‘q’ -> ‘ꝙ’
  DEADTRANS(L'/'  , L'r'  , 0xa7a7, NORMAL_CHARACTER),	// ‘r’ -> ‘ꞧ’
  DEADTRANS(L'/'  , L's'  , 0xa7a9, NORMAL_CHARACTER),	// ‘s’ -> ‘ꞩ’
  DEADTRANS(L'/'  , L't'  , 0x2c66, NORMAL_CHARACTER),	// ‘t’ -> ‘ⱦ’
  DEADTRANS(L'/'  , L'u'  , 0xa7b8, NORMAL_CHARACTER),	// ‘u’ -> ‘Ꞹ’
  DEADTRANS(L'/'  , L'v'  , 0xa75f, NORMAL_CHARACTER),	// ‘v’ -> ‘ꝟ’
  DEADTRANS(L'/'  , 0x00a0, L'/'  , NORMAL_CHARACTER),	// U+00A0 -> U+002F
  DEADTRANS(L'/'  , 0x017f, 0x1e9c, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẜ’
  DEADTRANS(L'/'  , 0x2192, 0x219b, NORMAL_CHARACTER),	// U+2192 -> U+219B
  DEADTRANS(L'/'  , 0x21d0, 0x21cd, NORMAL_CHARACTER),	// U+21D0 -> U+21CD
  DEADTRANS(L'/'  , 0x21d2, 0x21cf, NORMAL_CHARACTER),	// U+21D2 -> U+21CF
  DEADTRANS(L'/'  , 0x21d4, 0x21ce, NORMAL_CHARACTER),	// U+21D4 -> U+21CE
  DEADTRANS(L'/'  , 0x2203, 0x2204, NORMAL_CHARACTER),	// U+2203 -> U+2204
  DEADTRANS(L'/'  , 0x2208, 0x2209, NORMAL_CHARACTER),	// U+2208 -> U+2209
  DEADTRANS(L'/'  , 0x2225, 0x2226, NORMAL_CHARACTER),	// U+2225 -> U+2226
  DEADTRANS(L'/'  , 0x2282, 0x2284, NORMAL_CHARACTER),	// U+2282 -> U+2284
  DEADTRANS(L'/'  , 0x00b4, 0x038d, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Long Solidus Overlay + Acute Accent›
  DEADTRANS(L'/'  , L' '  , L'/'  , NORMAL_CHARACTER),	// U+0020 -> U+002F

// Dead key: Diaeresis
  DEADTRANS(L':'  , L'.'  , 0x0308, NORMAL_CHARACTER),	// U+002E -> U+0308
  DEADTRANS(L':'  , L'A'  , 0x00c4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ä’
  DEADTRANS(L':'  , L'E'  , 0x00cb, NORMAL_CHARACTER),	// ‘E’ -> ‘Ë’
  DEADTRANS(L':'  , L'H'  , 0x1e26, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḧ’
  DEADTRANS(L':'  , L'I'  , 0x00cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ï’
  DEADTRANS(L':'  , L'O'  , 0x00d6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ö’
  DEADTRANS(L':'  , L'U'  , 0x00dc, NORMAL_CHARACTER),	// ‘U’ -> ‘Ü’
  DEADTRANS(L':'  , L'W'  , 0x1e84, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẅ’
  DEADTRANS(L':'  , L'X'  , 0x1e8c, NORMAL_CHARACTER),	// ‘X’ -> ‘Ẍ’
  DEADTRANS(L':'  , L'Y'  , 0x0178, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ÿ’
  DEADTRANS(L':'  , L'a'  , 0x00e4, NORMAL_CHARACTER),	// ‘a’ -> ‘ä’
  DEADTRANS(L':'  , L'e'  , 0x00eb, NORMAL_CHARACTER),	// ‘e’ -> ‘ë’
  DEADTRANS(L':'  , L'h'  , 0x1e27, NORMAL_CHARACTER),	// ‘h’ -> ‘ḧ’
  DEADTRANS(L':'  , L'i'  , 0x00ef, NORMAL_CHARACTER),	// ‘i’ -> ‘ï’
  DEADTRANS(L':'  , L'o'  , 0x00f6, NORMAL_CHARACTER),	// ‘o’ -> ‘ö’
  DEADTRANS(L':'  , L't'  , 0x1e97, NORMAL_CHARACTER),	// ‘t’ -> ‘ẗ’
  DEADTRANS(L':'  , L'u'  , 0x00fc, NORMAL_CHARACTER),	// ‘u’ -> ‘ü’
  DEADTRANS(L':'  , L'w'  , 0x1e85, NORMAL_CHARACTER),	// ‘w’ -> ‘ẅ’
  DEADTRANS(L':'  , L'x'  , 0x1e8d, NORMAL_CHARACTER),	// ‘x’ -> ‘ẍ’
  DEADTRANS(L':'  , L'y'  , 0x00ff, NORMAL_CHARACTER),	// ‘y’ -> ‘ÿ’
  DEADTRANS(L':'  , 0x00a0, 0x00a8, NORMAL_CHARACTER),	// U+00A0 -> U+00A8
  DEADTRANS(L':'  , 0x03b9, 0x03ca, NORMAL_CHARACTER),	// ‘ι’ -> ‘ϊ’
  DEADTRANS(L':'  , 0x03c5, 0x03cb, NORMAL_CHARACTER),	// ‘υ’ -> ‘ϋ’
  DEADTRANS(L':'  , L'`'  , 0x03a2, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Diaeresis + Grave Accent›
  DEADTRANS(L':'  , L'~'  , 0x0530, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Diaeresis + Tilde›
  DEADTRANS(L':'  , 0x00af, 0x0557, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Diaeresis + Macron›
  DEADTRANS(L':'  , 0x00b4, 0x0558, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Diaeresis + Acute Accent›
  DEADTRANS(L':'  , 0x02c7, 0x058b, CHAINED_DEAD_KEY),	// ‹Caron› -> ‹Diaeresis + Caron›
  DEADTRANS(L':'  , L' '  , L':'  , NORMAL_CHARACTER),	// U+0020 -> U+003A

// Dead key: Grave Accent
  DEADTRANS(L'`'  , L'.'  , 0x0300, NORMAL_CHARACTER),	// U+002E -> U+0300
  DEADTRANS(L'`'  , L'A'  , 0x00c0, NORMAL_CHARACTER),	// ‘A’ -> ‘À’
  DEADTRANS(L'`'  , L'E'  , 0x00c8, NORMAL_CHARACTER),	// ‘E’ -> ‘È’
  DEADTRANS(L'`'  , L'I'  , 0x00cc, NORMAL_CHARACTER),	// ‘I’ -> ‘Ì’
  DEADTRANS(L'`'  , L'N'  , 0x01f8, NORMAL_CHARACTER),	// ‘N’ -> ‘Ǹ’
  DEADTRANS(L'`'  , L'O'  , 0x00d2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ò’
  DEADTRANS(L'`'  , L'U'  , 0x00d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ù’
  DEADTRANS(L'`'  , L'V'  , 0x01db, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǜ’
  DEADTRANS(L'`'  , L'W'  , 0x1e80, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẁ’
  DEADTRANS(L'`'  , L'Y'  , 0x1ef2, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỳ’
  DEADTRANS(L'`'  , L'a'  , 0x00e0, NORMAL_CHARACTER),	// ‘a’ -> ‘à’
  DEADTRANS(L'`'  , L'e'  , 0x00e8, NORMAL_CHARACTER),	// ‘e’ -> ‘è’
  DEADTRANS(L'`'  , L'i'  , 0x00ec, NORMAL_CHARACTER),	// ‘i’ -> ‘ì’
  DEADTRANS(L'`'  , L'n'  , 0x01f9, NORMAL_CHARACTER),	// ‘n’ -> ‘ǹ’
  DEADTRANS(L'`'  , L'o'  , 0x00f2, NORMAL_CHARACTER),	// ‘o’ -> ‘ò’
  DEADTRANS(L'`'  , L'u'  , 0x00f9, NORMAL_CHARACTER),	// ‘u’ -> ‘ù’
  DEADTRANS(L'`'  , L'v'  , 0x01dc, NORMAL_CHARACTER),	// ‘v’ -> ‘ǜ’
  DEADTRANS(L'`'  , L'w'  , 0x1e81, NORMAL_CHARACTER),	// ‘w’ -> ‘ẁ’
  DEADTRANS(L'`'  , L'y'  , 0x1ef3, NORMAL_CHARACTER),	// ‘y’ -> ‘ỳ’
  DEADTRANS(L'`'  , 0x00a0, L'`'  , NORMAL_CHARACTER),	// U+00A0 -> U+0060
  DEADTRANS(L'`'  , 0x00dc, 0x01db, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǜ’
  DEADTRANS(L'`'  , 0x00fc, 0x01dc, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǜ’
  DEADTRANS(L'`'  , 0x03a9, 0x1ffa, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὼ’
  DEADTRANS(L'`'  , 0x03b1, 0x1f70, NORMAL_CHARACTER),	// ‘α’ -> ‘ὰ’
  DEADTRANS(L'`'  , 0x03b5, 0x1f72, NORMAL_CHARACTER),	// ‘ε’ -> ‘ὲ’
  DEADTRANS(L'`'  , 0x03b7, 0x1f74, NORMAL_CHARACTER),	// ‘η’ -> ‘ὴ’
  DEADTRANS(L'`'  , 0x03b9, 0x1f76, NORMAL_CHARACTER),	// ‘ι’ -> ‘ὶ’
  DEADTRANS(L'`'  , 0x03bf, 0x1f78, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὸ’
  DEADTRANS(L'`'  , 0x03c5, 0x1f7a, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὺ’
  DEADTRANS(L'`'  , 0x03c9, 0x1f7c, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὼ’
  DEADTRANS(L'`'  , L':'  , 0x058c, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Grave Accent + Diaeresis›
  DEADTRANS(L'`'  , 0x00af, 0x0590, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Grave Accent + Macron›
  DEADTRANS(L'`'  , 0x02c6, 0x05c8, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Grave Accent + Circumflex Accent›
  DEADTRANS(L'`'  , 0x02d8, 0x05c9, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Grave Accent + Breve›
  DEADTRANS(L'`'  , 0x031b, 0x05ca, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Grave Accent + Horn›
  DEADTRANS(L'`'  , 0x201b, 0x05cb, CHAINED_DEAD_KEY),	// ‹Reversed Comma Above› -> ‹Grave Accent + Reversed Comma Above›
  DEADTRANS(L'`'  , L' '  , 0x02cb, NORMAL_CHARACTER),	// U+0020 -> ‘ˋ’

// Dead key: Tilde
  DEADTRANS(L'~'  , L'-'  , 0x2243, NORMAL_CHARACTER),	// U+002D -> U+2243
  DEADTRANS(L'~'  , L'.'  , 0x0303, NORMAL_CHARACTER),	// U+002E -> U+0303
  DEADTRANS(L'~'  , L'A'  , 0x00c3, NORMAL_CHARACTER),	// ‘A’ -> ‘Ã’
  DEADTRANS(L'~'  , L'E'  , 0x1ebc, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẽ’
  DEADTRANS(L'~'  , L'I'  , 0x0128, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĩ’
  DEADTRANS(L'~'  , L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(L'~'  , L'O'  , 0x00d5, NORMAL_CHARACTER),	// ‘O’ -> ‘Õ’
  DEADTRANS(L'~'  , L'U'  , 0x0168, NORMAL_CHARACTER),	// ‘U’ -> ‘Ũ’
  DEADTRANS(L'~'  , L'V'  , 0x1e7c, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṽ’
  DEADTRANS(L'~'  , L'Y'  , 0x1ef8, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỹ’
  DEADTRANS(L'~'  , L'a'  , 0x00e3, NORMAL_CHARACTER),	// ‘a’ -> ‘ã’
  DEADTRANS(L'~'  , L'e'  , 0x1ebd, NORMAL_CHARACTER),	// ‘e’ -> ‘ẽ’
  DEADTRANS(L'~'  , L'i'  , 0x0129, NORMAL_CHARACTER),	// ‘i’ -> ‘ĩ’
  DEADTRANS(L'~'  , L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(L'~'  , L'o'  , 0x00f5, NORMAL_CHARACTER),	// ‘o’ -> ‘õ’
  DEADTRANS(L'~'  , L'u'  , 0x0169, NORMAL_CHARACTER),	// ‘u’ -> ‘ũ’
  DEADTRANS(L'~'  , L'v'  , 0x1e7d, NORMAL_CHARACTER),	// ‘v’ -> ‘ṽ’
  DEADTRANS(L'~'  , L'y'  , 0x1ef9, NORMAL_CHARACTER),	// ‘y’ -> ‘ỹ’
  DEADTRANS(L'~'  , 0x00a0, L'~'  , NORMAL_CHARACTER),	// U+00A0 -> U+007E
  DEADTRANS(L'~'  , 0x00d6, 0x1e4e, NORMAL_CHARACTER),	// ‘Ö’ -> ‘Ṏ’
  DEADTRANS(L'~'  , 0x00f6, 0x1e4f, NORMAL_CHARACTER),	// ‘ö’ -> ‘ṏ’
  DEADTRANS(L'~'  , L':'  , 0x05cc, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Tilde + Diaeresis›
  DEADTRANS(L'~'  , 0x00af, 0x05cd, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Tilde + Macron›
  DEADTRANS(L'~'  , 0x00b4, 0x05ce, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Tilde + Acute Accent›
  DEADTRANS(L'~'  , 0x02c6, 0x05cf, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Tilde + Circumflex Accent›
  DEADTRANS(L'~'  , 0x02d8, 0x05eb, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Tilde + Breve›
  DEADTRANS(L'~'  , 0x031b, 0x05ec, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Tilde + Horn›
  DEADTRANS(L'~'  , L' '  , 0x02dc, NORMAL_CHARACTER),	// U+0020 -> U+02DC

// Dead key: Macron
  DEADTRANS(0x00af, L'.'  , 0x0304, NORMAL_CHARACTER),	// U+002E -> U+0304
  DEADTRANS(0x00af, L'A'  , 0x0100, NORMAL_CHARACTER),	// ‘A’ -> ‘Ā’
  DEADTRANS(0x00af, L'E'  , 0x0112, NORMAL_CHARACTER),	// ‘E’ -> ‘Ē’
  DEADTRANS(0x00af, L'G'  , 0x1e20, NORMAL_CHARACTER),	// ‘G’ -> ‘Ḡ’
  DEADTRANS(0x00af, L'I'  , 0x012a, NORMAL_CHARACTER),	// ‘I’ -> ‘Ī’
  DEADTRANS(0x00af, L'O'  , 0x014c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ō’
  DEADTRANS(0x00af, L'U'  , 0x016a, NORMAL_CHARACTER),	// ‘U’ -> ‘Ū’
  DEADTRANS(0x00af, L'V'  , 0x01d5, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǖ’
  DEADTRANS(0x00af, L'Y'  , 0x0232, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ȳ’
  DEADTRANS(0x00af, L'a'  , 0x0101, NORMAL_CHARACTER),	// ‘a’ -> ‘ā’
  DEADTRANS(0x00af, L'e'  , 0x0113, NORMAL_CHARACTER),	// ‘e’ -> ‘ē’
  DEADTRANS(0x00af, L'g'  , 0x1e21, NORMAL_CHARACTER),	// ‘g’ -> ‘ḡ’
  DEADTRANS(0x00af, L'i'  , 0x012b, NORMAL_CHARACTER),	// ‘i’ -> ‘ī’
  DEADTRANS(0x00af, L'o'  , 0x014d, NORMAL_CHARACTER),	// ‘o’ -> ‘ō’
  DEADTRANS(0x00af, L'u'  , 0x016b, NORMAL_CHARACTER),	// ‘u’ -> ‘ū’
  DEADTRANS(0x00af, L'v'  , 0x01d6, NORMAL_CHARACTER),	// ‘v’ -> ‘ǖ’
  DEADTRANS(0x00af, L'y'  , 0x0233, NORMAL_CHARACTER),	// ‘y’ -> ‘ȳ’
  DEADTRANS(0x00af, 0x00a0, 0x00af, NORMAL_CHARACTER),	// U+00A0 -> U+00AF
  DEADTRANS(0x00af, 0x00c4, 0x01de, NORMAL_CHARACTER),	// ‘Ä’ -> ‘Ǟ’
  DEADTRANS(0x00af, 0x00d6, 0x022a, NORMAL_CHARACTER),	// ‘Ö’ -> ‘Ȫ’
  DEADTRANS(0x00af, 0x00dc, 0x01d5, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǖ’
  DEADTRANS(0x00af, 0x00e4, 0x01df, NORMAL_CHARACTER),	// ‘ä’ -> ‘ǟ’
  DEADTRANS(0x00af, 0x00f6, 0x022b, NORMAL_CHARACTER),	// ‘ö’ -> ‘ȫ’
  DEADTRANS(0x00af, 0x00fc, 0x01d6, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǖ’
  DEADTRANS(0x00af, 0x03b1, 0x1fb1, NORMAL_CHARACTER),	// ‘α’ -> ‘ᾱ’
  DEADTRANS(0x00af, 0x03b9, 0x1fd1, NORMAL_CHARACTER),	// ‘ι’ -> ‘ῑ’
  DEADTRANS(0x00af, 0x03c5, 0x1fe1, NORMAL_CHARACTER),	// ‘υ’ -> ‘ῡ’
  DEADTRANS(0x00af, L'.'  , 0x05ed, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Macron + Dot Below›
  DEADTRANS(0x00af, L':'  , 0x05ee, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Macron + Diaeresis›
  DEADTRANS(0x00af, L'`'  , 0x05f5, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Macron + Grave Accent›
  DEADTRANS(0x00af, L'~'  , 0x05f6, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Macron + Tilde›
  DEADTRANS(0x00af, 0x00b4, 0x05f7, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Macron + Acute Accent›
  DEADTRANS(0x00af, 0x02d9, 0x05f8, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Macron + Dot Above›
  DEADTRANS(0x00af, L' '  , 0x02c9, NORMAL_CHARACTER),	// U+0020 -> ‘ˉ’

// Dead key: Acute Accent
  DEADTRANS(0x00b4, L'.'  , 0x0301, NORMAL_CHARACTER),	// U+002E -> U+0301
  DEADTRANS(0x00b4, L'A'  , 0x00c1, NORMAL_CHARACTER),	// ‘A’ -> ‘Á’
  DEADTRANS(0x00b4, L'C'  , 0x0106, NORMAL_CHARACTER),	// ‘C’ -> ‘Ć’
  DEADTRANS(0x00b4, L'E'  , 0x00c9, NORMAL_CHARACTER),	// ‘E’ -> ‘É’
  DEADTRANS(0x00b4, L'G'  , 0x01f4, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǵ’
  DEADTRANS(0x00b4, L'I'  , 0x00cd, NORMAL_CHARACTER),	// ‘I’ -> ‘Í’
  DEADTRANS(0x00b4, L'K'  , 0x1e30, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḱ’
  DEADTRANS(0x00b4, L'L'  , 0x0139, NORMAL_CHARACTER),	// ‘L’ -> ‘Ĺ’
  DEADTRANS(0x00b4, L'M'  , 0x1e3e, NORMAL_CHARACTER),	// ‘M’ -> ‘Ḿ’
  DEADTRANS(0x00b4, L'N'  , 0x0143, NORMAL_CHARACTER),	// ‘N’ -> ‘Ń’
  DEADTRANS(0x00b4, L'O'  , 0x00d3, NORMAL_CHARACTER),	// ‘O’ -> ‘Ó’
  DEADTRANS(0x00b4, L'P'  , 0x1e54, NORMAL_CHARACTER),	// ‘P’ -> ‘Ṕ’
  DEADTRANS(0x00b4, L'R'  , 0x0154, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŕ’
  DEADTRANS(0x00b4, L'S'  , 0x015a, NORMAL_CHARACTER),	// ‘S’ -> ‘Ś’
  DEADTRANS(0x00b4, L'U'  , 0x00da, NORMAL_CHARACTER),	// ‘U’ -> ‘Ú’
  DEADTRANS(0x00b4, L'V'  , 0x01d7, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǘ’
  DEADTRANS(0x00b4, L'W'  , 0x1e82, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẃ’
  DEADTRANS(0x00b4, L'Y'  , 0x00dd, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ý’
  DEADTRANS(0x00b4, L'Z'  , 0x0179, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ź’
  DEADTRANS(0x00b4, L'a'  , 0x00e1, NORMAL_CHARACTER),	// ‘a’ -> ‘á’
  DEADTRANS(0x00b4, L'c'  , 0x0107, NORMAL_CHARACTER),	// ‘c’ -> ‘ć’
  DEADTRANS(0x00b4, L'e'  , 0x00e9, NORMAL_CHARACTER),	// ‘e’ -> ‘é’
  DEADTRANS(0x00b4, L'g'  , 0x01f5, NORMAL_CHARACTER),	// ‘g’ -> ‘ǵ’
  DEADTRANS(0x00b4, L'i'  , 0x00ed, NORMAL_CHARACTER),	// ‘i’ -> ‘í’
  DEADTRANS(0x00b4, L'k'  , 0x1e31, NORMAL_CHARACTER),	// ‘k’ -> ‘ḱ’
  DEADTRANS(0x00b4, L'l'  , 0x013a, NORMAL_CHARACTER),	// ‘l’ -> ‘ĺ’
  DEADTRANS(0x00b4, L'm'  , 0x1e3f, NORMAL_CHARACTER),	// ‘m’ -> ‘ḿ’
  DEADTRANS(0x00b4, L'n'  , 0x0144, NORMAL_CHARACTER),	// ‘n’ -> ‘ń’
  DEADTRANS(0x00b4, L'o'  , 0x00f3, NORMAL_CHARACTER),	// ‘o’ -> ‘ó’
  DEADTRANS(0x00b4, L'p'  , 0x1e55, NORMAL_CHARACTER),	// ‘p’ -> ‘ṕ’
  DEADTRANS(0x00b4, L'r'  , 0x0155, NORMAL_CHARACTER),	// ‘r’ -> ‘ŕ’
  DEADTRANS(0x00b4, L's'  , 0x015b, NORMAL_CHARACTER),	// ‘s’ -> ‘ś’
  DEADTRANS(0x00b4, L'u'  , 0x00fa, NORMAL_CHARACTER),	// ‘u’ -> ‘ú’
  DEADTRANS(0x00b4, L'v'  , 0x01d8, NORMAL_CHARACTER),	// ‘v’ -> ‘ǘ’
  DEADTRANS(0x00b4, L'w'  , 0x1e83, NORMAL_CHARACTER),	// ‘w’ -> ‘ẃ’
  DEADTRANS(0x00b4, L'y'  , 0x00fd, NORMAL_CHARACTER),	// ‘y’ -> ‘ý’
  DEADTRANS(0x00b4, L'z'  , 0x017a, NORMAL_CHARACTER),	// ‘z’ -> ‘ź’
  DEADTRANS(0x00b4, 0x00a0, 0x00b4, NORMAL_CHARACTER),	// U+00A0 -> U+00B4
  DEADTRANS(0x00b4, 0x00dc, 0x01d7, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǘ’
  DEADTRANS(0x00b4, 0x00fc, 0x01d8, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǘ’
  DEADTRANS(0x00b4, 0x03a9, 0x038f, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ώ’
  DEADTRANS(0x00b4, 0x03b1, 0x03ac, NORMAL_CHARACTER),	// ‘α’ -> ‘ά’
  DEADTRANS(0x00b4, 0x03b5, 0x03ad, NORMAL_CHARACTER),	// ‘ε’ -> ‘έ’
  DEADTRANS(0x00b4, 0x03b7, 0x03ae, NORMAL_CHARACTER),	// ‘η’ -> ‘ή’
  DEADTRANS(0x00b4, 0x03b9, 0x03af, NORMAL_CHARACTER),	// ‘ι’ -> ‘ί’
  DEADTRANS(0x00b4, 0x03bf, 0x03cc, NORMAL_CHARACTER),	// ‘ο’ -> ‘ό’
  DEADTRANS(0x00b4, 0x03c5, 0x03cd, NORMAL_CHARACTER),	// ‘υ’ -> ‘ύ’
  DEADTRANS(0x00b4, 0x03c9, 0x03ce, NORMAL_CHARACTER),	// ‘ω’ -> ‘ώ’
  DEADTRANS(0x00b4, L','  , 0x05f9, CHAINED_DEAD_KEY),	// ‹Cedilla› -> ‹Acute Accent + Cedilla›
  DEADTRANS(0x00b4, L'/'  , 0x05fa, CHAINED_DEAD_KEY),	// ‹Long Solidus Overlay› -> ‹Acute Accent + Long Solidus Overlay›
  DEADTRANS(0x00b4, L':'  , 0x05fb, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Acute Accent + Diaeresis›
  DEADTRANS(0x00b4, L'~'  , 0x05fc, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Acute Accent + Tilde›
  DEADTRANS(0x00b4, 0x00af, 0x05fd, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Acute Accent + Macron›
  DEADTRANS(0x00b4, 0x02c6, 0x05fe, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Acute Accent + Circumflex Accent›
  DEADTRANS(0x00b4, 0x02d8, 0x05ff, CHAINED_DEAD_KEY),	// ‹Breve› -> ‹Acute Accent + Breve›
  DEADTRANS(0x00b4, 0x02d9, 0x061d, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Acute Accent + Dot Above›
  DEADTRANS(0x00b4, 0x02da, 0x070e, CHAINED_DEAD_KEY),	// ‹Ring Above› -> ‹Acute Accent + Ring Above›
  DEADTRANS(0x00b4, 0x031b, 0x074b, CHAINED_DEAD_KEY),	// ‹Horn› -> ‹Acute Accent + Horn›
  DEADTRANS(0x00b4, 0x201b, 0x074c, CHAINED_DEAD_KEY),	// ‹Reversed Comma Above› -> ‹Acute Accent + Reversed Comma Above›
  DEADTRANS(0x00b4, L' '  , 0x02ca, NORMAL_CHARACTER),	// U+0020 -> ‘ˊ’

// Dead key: Circumflex Accent
  DEADTRANS(0x02c6, L'('  , 0x207d, NORMAL_CHARACTER),	// U+0028 -> U+207D
  DEADTRANS(0x02c6, L')'  , 0x207e, NORMAL_CHARACTER),	// U+0029 -> U+207E
  DEADTRANS(0x02c6, L'+'  , 0x207a, NORMAL_CHARACTER),	// U+002B -> U+207A
  DEADTRANS(0x02c6, L'.'  , 0x0302, NORMAL_CHARACTER),	// U+002E -> U+0302
  DEADTRANS(0x02c6, L'0'  , 0x2070, NORMAL_CHARACTER),	// ‘0’ -> ‘⁰’
  DEADTRANS(0x02c6, L'1'  , 0x00b9, NORMAL_CHARACTER),	// ‘1’ -> ‘¹’
  DEADTRANS(0x02c6, L'2'  , 0x00b2, NORMAL_CHARACTER),	// ‘2’ -> ‘²’
  DEADTRANS(0x02c6, L'3'  , 0x00b3, NORMAL_CHARACTER),	// ‘3’ -> ‘³’
  DEADTRANS(0x02c6, L'4'  , 0x2074, NORMAL_CHARACTER),	// ‘4’ -> ‘⁴’
  DEADTRANS(0x02c6, L'5'  , 0x2075, NORMAL_CHARACTER),	// ‘5’ -> ‘⁵’
  DEADTRANS(0x02c6, L'6'  , 0x2076, NORMAL_CHARACTER),	// ‘6’ -> ‘⁶’
  DEADTRANS(0x02c6, L'7'  , 0x2077, NORMAL_CHARACTER),	// ‘7’ -> ‘⁷’
  DEADTRANS(0x02c6, L'8'  , 0x2078, NORMAL_CHARACTER),	// ‘8’ -> ‘⁸’
  DEADTRANS(0x02c6, L'9'  , 0x2079, NORMAL_CHARACTER),	// ‘9’ -> ‘⁹’
  DEADTRANS(0x02c6, L'A'  , 0x00c2, NORMAL_CHARACTER),	// ‘A’ -> ‘Â’
  DEADTRANS(0x02c6, L'C'  , 0x0108, NORMAL_CHARACTER),	// ‘C’ -> ‘Ĉ’
  DEADTRANS(0x02c6, L'E'  , 0x00ca, NORMAL_CHARACTER),	// ‘E’ -> ‘Ê’
  DEADTRANS(0x02c6, L'G'  , 0x011c, NORMAL_CHARACTER),	// ‘G’ -> ‘Ĝ’
  DEADTRANS(0x02c6, L'H'  , 0x0124, NORMAL_CHARACTER),	// ‘H’ -> ‘Ĥ’
  DEADTRANS(0x02c6, L'I'  , 0x00ce, NORMAL_CHARACTER),	// ‘I’ -> ‘Î’
  DEADTRANS(0x02c6, L'J'  , 0x0134, NORMAL_CHARACTER),	// ‘J’ -> ‘Ĵ’
  DEADTRANS(0x02c6, L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(0x02c6, L'O'  , 0x00d4, NORMAL_CHARACTER),	// ‘O’ -> ‘Ô’
  DEADTRANS(0x02c6, L'S'  , 0x015c, NORMAL_CHARACTER),	// ‘S’ -> ‘Ŝ’
  DEADTRANS(0x02c6, L'U'  , 0x00db, NORMAL_CHARACTER),	// ‘U’ -> ‘Û’
  DEADTRANS(0x02c6, L'W'  , 0x0174, NORMAL_CHARACTER),	// ‘W’ -> ‘Ŵ’
  DEADTRANS(0x02c6, L'Y'  , 0x0176, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ŷ’
  DEADTRANS(0x02c6, L'Z'  , 0x1e90, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẑ’
  DEADTRANS(0x02c6, L'a'  , 0x00e2, NORMAL_CHARACTER),	// ‘a’ -> ‘â’
  DEADTRANS(0x02c6, L'c'  , 0x0109, NORMAL_CHARACTER),	// ‘c’ -> ‘ĉ’
  DEADTRANS(0x02c6, L'e'  , 0x00ea, NORMAL_CHARACTER),	// ‘e’ -> ‘ê’
  DEADTRANS(0x02c6, L'g'  , 0x011d, NORMAL_CHARACTER),	// ‘g’ -> ‘ĝ’
  DEADTRANS(0x02c6, L'h'  , 0x0125, NORMAL_CHARACTER),	// ‘h’ -> ‘ĥ’
  DEADTRANS(0x02c6, L'i'  , 0x00ee, NORMAL_CHARACTER),	// ‘i’ -> ‘î’
  DEADTRANS(0x02c6, L'j'  , 0x0135, NORMAL_CHARACTER),	// ‘j’ -> ‘ĵ’
  DEADTRANS(0x02c6, L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(0x02c6, L'o'  , 0x00f4, NORMAL_CHARACTER),	// ‘o’ -> ‘ô’
  DEADTRANS(0x02c6, L's'  , 0x015d, NORMAL_CHARACTER),	// ‘s’ -> ‘ŝ’
  DEADTRANS(0x02c6, L'u'  , 0x00fb, NORMAL_CHARACTER),	// ‘u’ -> ‘û’
  DEADTRANS(0x02c6, L'w'  , 0x0175, NORMAL_CHARACTER),	// ‘w’ -> ‘ŵ’
  DEADTRANS(0x02c6, L'y'  , 0x0177, NORMAL_CHARACTER),	// ‘y’ -> ‘ŷ’
  DEADTRANS(0x02c6, L'z'  , 0x1e91, NORMAL_CHARACTER),	// ‘z’ -> ‘ẑ’
  DEADTRANS(0x02c6, 0x00a0, L'^'  , NORMAL_CHARACTER),	// U+00A0 -> U+005E
  DEADTRANS(0x02c6, L'.'  , 0x07b2, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Circumflex Accent + Dot Below›
  DEADTRANS(0x02c6, L'`'  , 0x07b3, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Circumflex Accent + Grave Accent›
  DEADTRANS(0x02c6, L'~'  , 0x07b4, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Circumflex Accent + Tilde›
  DEADTRANS(0x02c6, 0x00b4, 0x07b5, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Circumflex Accent + Acute Accent›
  DEADTRANS(0x02c6, L' '  , 0x02c6, NORMAL_CHARACTER),	// U+0020 -> ‘ˆ’

// Dead key: Caron
  DEADTRANS(0x02c7, L'('  , 0x208d, NORMAL_CHARACTER),	// U+0028 -> U+208D
  DEADTRANS(0x02c7, L')'  , 0x208e, NORMAL_CHARACTER),	// U+0029 -> U+208E
  DEADTRANS(0x02c7, L'+'  , 0x208a, NORMAL_CHARACTER),	// U+002B -> U+208A
  DEADTRANS(0x02c7, L'.'  , 0x030c, NORMAL_CHARACTER),	// U+002E -> U+030C
  DEADTRANS(0x02c7, L'0'  , 0x2080, NORMAL_CHARACTER),	// ‘0’ -> ‘₀’
  DEADTRANS(0x02c7, L'1'  , 0x2081, NORMAL_CHARACTER),	// ‘1’ -> ‘₁’
  DEADTRANS(0x02c7, L'2'  , 0x2082, NORMAL_CHARACTER),	// ‘2’ -> ‘₂’
  DEADTRANS(0x02c7, L'3'  , 0x2083, NORMAL_CHARACTER),	// ‘3’ -> ‘₃’
  DEADTRANS(0x02c7, L'4'  , 0x2084, NORMAL_CHARACTER),	// ‘4’ -> ‘₄’
  DEADTRANS(0x02c7, L'5'  , 0x2085, NORMAL_CHARACTER),	// ‘5’ -> ‘₅’
  DEADTRANS(0x02c7, L'6'  , 0x2086, NORMAL_CHARACTER),	// ‘6’ -> ‘₆’
  DEADTRANS(0x02c7, L'7'  , 0x2087, NORMAL_CHARACTER),	// ‘7’ -> ‘₇’
  DEADTRANS(0x02c7, L'8'  , 0x2088, NORMAL_CHARACTER),	// ‘8’ -> ‘₈’
  DEADTRANS(0x02c7, L'9'  , 0x2089, NORMAL_CHARACTER),	// ‘9’ -> ‘₉’
  DEADTRANS(0x02c7, L'A'  , 0x01cd, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǎ’
  DEADTRANS(0x02c7, L'C'  , 0x010c, NORMAL_CHARACTER),	// ‘C’ -> ‘Č’
  DEADTRANS(0x02c7, L'D'  , 0x010e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ď’
  DEADTRANS(0x02c7, L'E'  , 0x011a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ě’
  DEADTRANS(0x02c7, L'G'  , 0x01e6, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǧ’
  DEADTRANS(0x02c7, L'H'  , 0x021e, NORMAL_CHARACTER),	// ‘H’ -> ‘Ȟ’
  DEADTRANS(0x02c7, L'I'  , 0x01cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ǐ’
  DEADTRANS(0x02c7, L'K'  , 0x01e8, NORMAL_CHARACTER),	// ‘K’ -> ‘Ǩ’
  DEADTRANS(0x02c7, L'L'  , 0x013d, NORMAL_CHARACTER),	// ‘L’ -> ‘Ľ’
  DEADTRANS(0x02c7, L'N'  , 0x0147, NORMAL_CHARACTER),	// ‘N’ -> ‘Ň’
  DEADTRANS(0x02c7, L'O'  , 0x01d1, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǒ’
  DEADTRANS(0x02c7, L'R'  , 0x0158, NORMAL_CHARACTER),	// ‘R’ -> ‘Ř’
  DEADTRANS(0x02c7, L'S'  , 0x0160, NORMAL_CHARACTER),	// ‘S’ -> ‘Š’
  DEADTRANS(0x02c7, L'T'  , 0x0164, NORMAL_CHARACTER),	// ‘T’ -> ‘Ť’
  DEADTRANS(0x02c7, L'U'  , 0x01d3, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǔ’
  DEADTRANS(0x02c7, L'Z'  , 0x017d, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ž’
  DEADTRANS(0x02c7, L'a'  , 0x01ce, NORMAL_CHARACTER),	// ‘a’ -> ‘ǎ’
  DEADTRANS(0x02c7, L'c'  , 0x010d, NORMAL_CHARACTER),	// ‘c’ -> ‘č’
  DEADTRANS(0x02c7, L'd'  , 0x010f, NORMAL_CHARACTER),	// ‘d’ -> ‘ď’
  DEADTRANS(0x02c7, L'e'  , 0x011b, NORMAL_CHARACTER),	// ‘e’ -> ‘ě’
  DEADTRANS(0x02c7, L'g'  , 0x01e7, NORMAL_CHARACTER),	// ‘g’ -> ‘ǧ’
  DEADTRANS(0x02c7, L'h'  , 0x021f, NORMAL_CHARACTER),	// ‘h’ -> ‘ȟ’
  DEADTRANS(0x02c7, L'i'  , 0x01d0, NORMAL_CHARACTER),	// ‘i’ -> ‘ǐ’
  DEADTRANS(0x02c7, L'j'  , 0x01f0, NORMAL_CHARACTER),	// ‘j’ -> ‘ǰ’
  DEADTRANS(0x02c7, L'k'  , 0x01e9, NORMAL_CHARACTER),	// ‘k’ -> ‘ǩ’
  DEADTRANS(0x02c7, L'l'  , 0x013e, NORMAL_CHARACTER),	// ‘l’ -> ‘ľ’
  DEADTRANS(0x02c7, L'n'  , 0x0148, NORMAL_CHARACTER),	// ‘n’ -> ‘ň’
  DEADTRANS(0x02c7, L'o'  , 0x01d2, NORMAL_CHARACTER),	// ‘o’ -> ‘ǒ’
  DEADTRANS(0x02c7, L'r'  , 0x0159, NORMAL_CHARACTER),	// ‘r’ -> ‘ř’
  DEADTRANS(0x02c7, L's'  , 0x0161, NORMAL_CHARACTER),	// ‘s’ -> ‘š’
  DEADTRANS(0x02c7, L't'  , 0x0165, NORMAL_CHARACTER),	// ‘t’ -> ‘ť’
  DEADTRANS(0x02c7, L'u'  , 0x01d4, NORMAL_CHARACTER),	// ‘u’ -> ‘ǔ’
  DEADTRANS(0x02c7, L'z'  , 0x017e, NORMAL_CHARACTER),	// ‘z’ -> ‘ž’
  DEADTRANS(0x02c7, 0x00dc, 0x01d9, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǚ’
  DEADTRANS(0x02c7, 0x00fc, 0x01da, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǚ’
  DEADTRANS(0x02c7, L':'  , 0x07b6, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Caron + Diaeresis›
  DEADTRANS(0x02c7, 0x02d9, 0x07b7, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Caron + Dot Above›
  DEADTRANS(0x02c7, L' '  , 0x02c7, NORMAL_CHARACTER),	// U+0020 -> ‘ˇ’

// Dead key: Breve
  DEADTRANS(0x02d8, L'.'  , 0x0306, NORMAL_CHARACTER),	// U+002E -> U+0306
  DEADTRANS(0x02d8, L'A'  , 0x0102, NORMAL_CHARACTER),	// ‘A’ -> ‘Ă’
  DEADTRANS(0x02d8, L'E'  , 0x0114, NORMAL_CHARACTER),	// ‘E’ -> ‘Ĕ’
  DEADTRANS(0x02d8, L'G'  , 0x011e, NORMAL_CHARACTER),	// ‘G’ -> ‘Ğ’
  DEADTRANS(0x02d8, L'I'  , 0x012c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĭ’
  DEADTRANS(0x02d8, L'O'  , 0x014e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ŏ’
  DEADTRANS(0x02d8, L'U'  , 0x016c, NORMAL_CHARACTER),	// ‘U’ -> ‘Ŭ’
  DEADTRANS(0x02d8, L'a'  , 0x0103, NORMAL_CHARACTER),	// ‘a’ -> ‘ă’
  DEADTRANS(0x02d8, L'e'  , 0x0115, NORMAL_CHARACTER),	// ‘e’ -> ‘ĕ’
  DEADTRANS(0x02d8, L'g'  , 0x011f, NORMAL_CHARACTER),	// ‘g’ -> ‘ğ’
  DEADTRANS(0x02d8, L'i'  , 0x012d, NORMAL_CHARACTER),	// ‘i’ -> ‘ĭ’
  DEADTRANS(0x02d8, L'o'  , 0x014f, NORMAL_CHARACTER),	// ‘o’ -> ‘ŏ’
  DEADTRANS(0x02d8, L'u'  , 0x016d, NORMAL_CHARACTER),	// ‘u’ -> ‘ŭ’
  DEADTRANS(0x02d8, 0x03b1, 0x1fb0, NORMAL_CHARACTER),	// ‘α’ -> ‘ᾰ’
  DEADTRANS(0x02d8, 0x03b9, 0x1fd0, NORMAL_CHARACTER),	// ‘ι’ -> ‘ῐ’
  DEADTRANS(0x02d8, 0x03c5, 0x1fe0, NORMAL_CHARACTER),	// ‘υ’ -> ‘ῠ’
  DEADTRANS(0x02d8, L','  , 0x07b8, CHAINED_DEAD_KEY),	// ‹Cedilla› -> ‹Breve + Cedilla›
  DEADTRANS(0x02d8, L'.'  , 0x07b9, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Breve + Dot Below›
  DEADTRANS(0x02d8, L'`'  , 0x07ba, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Breve + Grave Accent›
  DEADTRANS(0x02d8, L'~'  , 0x07bb, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Breve + Tilde›
  DEADTRANS(0x02d8, 0x00b4, 0x07bc, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Breve + Acute Accent›
  DEADTRANS(0x02d8, L' '  , 0x02d8, NORMAL_CHARACTER),	// U+0020 -> U+02D8

// Dead key: Dot Above
  DEADTRANS(0x02d9, L'.'  , 0x0307, NORMAL_CHARACTER),	// U+002E -> U+0307
  DEADTRANS(0x02d9, L'A'  , 0x0226, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȧ’
  DEADTRANS(0x02d9, L'B'  , 0x1e02, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḃ’
  DEADTRANS(0x02d9, L'C'  , 0x010a, NORMAL_CHARACTER),	// ‘C’ -> ‘Ċ’
  DEADTRANS(0x02d9, L'D'  , 0x1e0a, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḋ’
  DEADTRANS(0x02d9, L'E'  , 0x0116, NORMAL_CHARACTER),	// ‘E’ -> ‘Ė’
  DEADTRANS(0x02d9, L'F'  , 0x1e1e, NORMAL_CHARACTER),	// ‘F’ -> ‘Ḟ’
  DEADTRANS(0x02d9, L'G'  , 0x0120, NORMAL_CHARACTER),	// ‘G’ -> ‘Ġ’
  DEADTRANS(0x02d9, L'H'  , 0x1e22, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḣ’
  DEADTRANS(0x02d9, L'I'  , 0x0130, NORMAL_CHARACTER),	// ‘I’ -> ‘İ’
  DEADTRANS(0x02d9, L'L'  , 0x013f, NORMAL_CHARACTER),	// ‘L’ -> ‘Ŀ’
  DEADTRANS(0x02d9, L'M'  , 0x1e40, NORMAL_CHARACTER),	// ‘M’ -> ‘Ṁ’
  DEADTRANS(0x02d9, L'N'  , 0x1e44, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṅ’
  DEADTRANS(0x02d9, L'O'  , 0x022e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȯ’
  DEADTRANS(0x02d9, L'P'  , 0x1e56, NORMAL_CHARACTER),	// ‘P’ -> ‘Ṗ’
  DEADTRANS(0x02d9, L'R'  , 0x1e58, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṙ’
  DEADTRANS(0x02d9, L'S'  , 0x1e60, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṡ’
  DEADTRANS(0x02d9, L'T'  , 0x1e6a, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṫ’
  DEADTRANS(0x02d9, L'W'  , 0x1e86, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẇ’
  DEADTRANS(0x02d9, L'X'  , 0x1e8a, NORMAL_CHARACTER),	// ‘X’ -> ‘Ẋ’
  DEADTRANS(0x02d9, L'Y'  , 0x1e8e, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ẏ’
  DEADTRANS(0x02d9, L'Z'  , 0x017b, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ż’
  DEADTRANS(0x02d9, L'a'  , 0x0227, NORMAL_CHARACTER),	// ‘a’ -> ‘ȧ’
  DEADTRANS(0x02d9, L'b'  , 0x1e03, NORMAL_CHARACTER),	// ‘b’ -> ‘ḃ’
  DEADTRANS(0x02d9, L'c'  , 0x010b, NORMAL_CHARACTER),	// ‘c’ -> ‘ċ’
  DEADTRANS(0x02d9, L'd'  , 0x1e0b, NORMAL_CHARACTER),	// ‘d’ -> ‘ḋ’
  DEADTRANS(0x02d9, L'e'  , 0x0117, NORMAL_CHARACTER),	// ‘e’ -> ‘ė’
  DEADTRANS(0x02d9, L'f'  , 0x1e1f, NORMAL_CHARACTER),	// ‘f’ -> ‘ḟ’
  DEADTRANS(0x02d9, L'g'  , 0x0121, NORMAL_CHARACTER),	// ‘g’ -> ‘ġ’
  DEADTRANS(0x02d9, L'h'  , 0x1e23, NORMAL_CHARACTER),	// ‘h’ -> ‘ḣ’
  DEADTRANS(0x02d9, L'i'  , 0x0131, NORMAL_CHARACTER),	// ‘i’ -> ‘ı’
  DEADTRANS(0x02d9, L'j'  , 0x0237, NORMAL_CHARACTER),	// ‘j’ -> ‘ȷ’
  DEADTRANS(0x02d9, L'l'  , 0x0140, NORMAL_CHARACTER),	// ‘l’ -> ‘ŀ’
  DEADTRANS(0x02d9, L'm'  , 0x1e41, NORMAL_CHARACTER),	// ‘m’ -> ‘ṁ’
  DEADTRANS(0x02d9, L'n'  , 0x1e45, NORMAL_CHARACTER),	// ‘n’ -> ‘ṅ’
  DEADTRANS(0x02d9, L'o'  , 0x022f, NORMAL_CHARACTER),	// ‘o’ -> ‘ȯ’
  DEADTRANS(0x02d9, L'p'  , 0x1e57, NORMAL_CHARACTER),	// ‘p’ -> ‘ṗ’
  DEADTRANS(0x02d9, L'r'  , 0x1e59, NORMAL_CHARACTER),	// ‘r’ -> ‘ṙ’
  DEADTRANS(0x02d9, L's'  , 0x1e61, NORMAL_CHARACTER),	// ‘s’ -> ‘ṡ’
  DEADTRANS(0x02d9, L't'  , 0x1e6b, NORMAL_CHARACTER),	// ‘t’ -> ‘ṫ’
  DEADTRANS(0x02d9, L'w'  , 0x1e87, NORMAL_CHARACTER),	// ‘w’ -> ‘ẇ’
  DEADTRANS(0x02d9, L'x'  , 0x1e8b, NORMAL_CHARACTER),	// ‘x’ -> ‘ẋ’
  DEADTRANS(0x02d9, L'y'  , 0x1e8f, NORMAL_CHARACTER),	// ‘y’ -> ‘ẏ’
  DEADTRANS(0x02d9, L'z'  , 0x017c, NORMAL_CHARACTER),	// ‘z’ -> ‘ż’
  DEADTRANS(0x02d9, 0x017f, 0x1e9b, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẛ’
  DEADTRANS(0x02d9, L'.'  , 0x07bd, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Dot Above + Dot Below›
  DEADTRANS(0x02d9, 0x00af, 0x07be, CHAINED_DEAD_KEY),	// ‹Macron› -> ‹Dot Above + Macron›
  DEADTRANS(0x02d9, 0x00b4, 0x07bf, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Dot Above + Acute Accent›
  DEADTRANS(0x02d9, 0x02c7, 0x07fb, CHAINED_DEAD_KEY),	// ‹Caron› -> ‹Dot Above + Caron›
  DEADTRANS(0x02d9, L' '  , 0x02d9, NORMAL_CHARACTER),	// U+0020 -> U+02D9

// Dead key: Ring Above
  DEADTRANS(0x02da, L'.'  , 0x030a, NORMAL_CHARACTER),	// U+002E -> U+030A
  DEADTRANS(0x02da, L'A'  , 0x00c5, NORMAL_CHARACTER),	// ‘A’ -> ‘Å’
  DEADTRANS(0x02da, L'U'  , 0x016e, NORMAL_CHARACTER),	// ‘U’ -> ‘Ů’
  DEADTRANS(0x02da, L'a'  , 0x00e5, NORMAL_CHARACTER),	// ‘a’ -> ‘å’
  DEADTRANS(0x02da, L'u'  , 0x016f, NORMAL_CHARACTER),	// ‘u’ -> ‘ů’
  DEADTRANS(0x02da, L'w'  , 0x1e98, NORMAL_CHARACTER),	// ‘w’ -> ‘ẘ’
  DEADTRANS(0x02da, L'y'  , 0x1e99, NORMAL_CHARACTER),	// ‘y’ -> ‘ẙ’
  DEADTRANS(0x02da, 0x00a0, 0x00b0, NORMAL_CHARACTER),	// U+00A0 -> U+00B0
  DEADTRANS(0x02da, 0x00b4, 0x07fc, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Ring Above + Acute Accent›
  DEADTRANS(0x02da, L' '  , 0x02da, NORMAL_CHARACTER),	// U+0020 -> U+02DA

// Dead key: Double Acute Accent
  DEADTRANS(0x02dd, L'.'  , 0x030b, NORMAL_CHARACTER),	// U+002E -> U+030B
  DEADTRANS(0x02dd, L'O'  , 0x0150, NORMAL_CHARACTER),	// ‘O’ -> ‘Ő’
  DEADTRANS(0x02dd, L'U'  , 0x0170, NORMAL_CHARACTER),	// ‘U’ -> ‘Ű’
  DEADTRANS(0x02dd, L'o'  , 0x0151, NORMAL_CHARACTER),	// ‘o’ -> ‘ő’
  DEADTRANS(0x02dd, L'u'  , 0x0171, NORMAL_CHARACTER),	// ‘u’ -> ‘ű’
  DEADTRANS(0x02dd, L' '  , 0x02dd, NORMAL_CHARACTER),	// U+0020 -> U+02DD

// Dead key: Tilde Below
  DEADTRANS(0x02f7, L'.'  , 0x0330, NORMAL_CHARACTER),	// U+002E -> U+0330
  DEADTRANS(0x02f7, L'E'  , 0x1e1a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḛ’
  DEADTRANS(0x02f7, L'I'  , 0x1e2c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḭ’
  DEADTRANS(0x02f7, L'U'  , 0x1e74, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṵ’
  DEADTRANS(0x02f7, L'e'  , 0x1e1b, NORMAL_CHARACTER),	// ‘e’ -> ‘ḛ’
  DEADTRANS(0x02f7, L'i'  , 0x1e2d, NORMAL_CHARACTER),	// ‘i’ -> ‘ḭ’
  DEADTRANS(0x02f7, L'u'  , 0x1e75, NORMAL_CHARACTER),	// ‘u’ -> ‘ṵ’
  DEADTRANS(0x02f7, L' '  , 0x02f7, NORMAL_CHARACTER),	// U+0020 -> U+02F7

// Dead key: Double Grave Accent
  DEADTRANS(0x030f, L'.'  , 0x030f, NORMAL_CHARACTER),	// U+002E -> U+030F
  DEADTRANS(0x030f, L'A'  , 0x0200, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȁ’
  DEADTRANS(0x030f, L'E'  , 0x0204, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȅ’
  DEADTRANS(0x030f, L'I'  , 0x0208, NORMAL_CHARACTER),	// ‘I’ -> ‘Ȉ’
  DEADTRANS(0x030f, L'O'  , 0x020c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȍ’
  DEADTRANS(0x030f, L'R'  , 0x0210, NORMAL_CHARACTER),	// ‘R’ -> ‘Ȑ’
  DEADTRANS(0x030f, L'U'  , 0x0214, NORMAL_CHARACTER),	// ‘U’ -> ‘Ȕ’
  DEADTRANS(0x030f, L'a'  , 0x0201, NORMAL_CHARACTER),	// ‘a’ -> ‘ȁ’
  DEADTRANS(0x030f, L'e'  , 0x0205, NORMAL_CHARACTER),	// ‘e’ -> ‘ȅ’
  DEADTRANS(0x030f, L'i'  , 0x0209, NORMAL_CHARACTER),	// ‘i’ -> ‘ȉ’
  DEADTRANS(0x030f, L'o'  , 0x020d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȍ’
  DEADTRANS(0x030f, L'r'  , 0x0211, NORMAL_CHARACTER),	// ‘r’ -> ‘ȑ’
  DEADTRANS(0x030f, L'u'  , 0x0215, NORMAL_CHARACTER),	// ‘u’ -> ‘ȕ’
  DEADTRANS(0x030f, L' '  , 0x030f, NORMAL_CHARACTER),	// U+0020 -> U+030F

// Dead key: Horn
  DEADTRANS(0x031b, L'.'  , 0x031b, NORMAL_CHARACTER),	// U+002E -> U+031B
  DEADTRANS(0x031b, L'O'  , 0x01a0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ơ’
  DEADTRANS(0x031b, L'U'  , 0x01af, NORMAL_CHARACTER),	// ‘U’ -> ‘Ư’
  DEADTRANS(0x031b, L'o'  , 0x01a1, NORMAL_CHARACTER),	// ‘o’ -> ‘ơ’
  DEADTRANS(0x031b, L'u'  , 0x01b0, NORMAL_CHARACTER),	// ‘u’ -> ‘ư’
  DEADTRANS(0x031b, L'.'  , 0x082e, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Horn + Dot Below›
  DEADTRANS(0x031b, L'`'  , 0x082f, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Horn + Grave Accent›
  DEADTRANS(0x031b, L'~'  , 0x083f, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Horn + Tilde›
  DEADTRANS(0x031b, 0x00b4, 0x085c, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Horn + Acute Accent›
  DEADTRANS(0x031b, L' '  , 0x031b, NORMAL_CHARACTER),	// U+0020 -> U+031B

// Dead key: Cedilla + Acute Accent
  DEADTRANS(0x0378, L'C'  , 0x1e08, NORMAL_CHARACTER),	// ‘C’ -> ‘Ḉ’
  DEADTRANS(0x0378, L'c'  , 0x1e09, NORMAL_CHARACTER),	// ‘c’ -> ‘ḉ’
  DEADTRANS(0x0378, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Cedilla + Breve
  DEADTRANS(0x0379, L'E'  , 0x1e1c, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḝ’
  DEADTRANS(0x0379, L'e'  , 0x1e1d, NORMAL_CHARACTER),	// ‘e’ -> ‘ḝ’
  DEADTRANS(0x0379, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Macron
  DEADTRANS(0x0380, L'L'  , 0x1e38, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḹ’
  DEADTRANS(0x0380, L'R'  , 0x1e5c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṝ’
  DEADTRANS(0x0380, L'l'  , 0x1e39, NORMAL_CHARACTER),	// ‘l’ -> ‘ḹ’
  DEADTRANS(0x0380, L'r'  , 0x1e5d, NORMAL_CHARACTER),	// ‘r’ -> ‘ṝ’
  DEADTRANS(0x0380, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Circumflex Accent
  DEADTRANS(0x0381, L'A'  , 0x1eac, NORMAL_CHARACTER),	// ‘A’ -> ‘Ậ’
  DEADTRANS(0x0381, L'E'  , 0x1ec6, NORMAL_CHARACTER),	// ‘E’ -> ‘Ệ’
  DEADTRANS(0x0381, L'O'  , 0x1ed8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ộ’
  DEADTRANS(0x0381, L'a'  , 0x1ead, NORMAL_CHARACTER),	// ‘a’ -> ‘ậ’
  DEADTRANS(0x0381, L'e'  , 0x1ec7, NORMAL_CHARACTER),	// ‘e’ -> ‘ệ’
  DEADTRANS(0x0381, L'o'  , 0x1ed9, NORMAL_CHARACTER),	// ‘o’ -> ‘ộ’
  DEADTRANS(0x0381, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Breve
  DEADTRANS(0x0382, L'A'  , 0x1eb6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ặ’
  DEADTRANS(0x0382, L'a'  , 0x1eb7, NORMAL_CHARACTER),	// ‘a’ -> ‘ặ’
  DEADTRANS(0x0382, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Dot Above
  DEADTRANS(0x0383, L'S'  , 0x1e68, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṩ’
  DEADTRANS(0x0383, L's'  , 0x1e69, NORMAL_CHARACTER),	// ‘s’ -> ‘ṩ’
  DEADTRANS(0x0383, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Below + Horn
  DEADTRANS(0x038b, L'O'  , 0x1ee2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ợ’
  DEADTRANS(0x038b, L'U'  , 0x1ef0, NORMAL_CHARACTER),	// ‘U’ -> ‘Ự’
  DEADTRANS(0x038b, L'o'  , 0x1ee3, NORMAL_CHARACTER),	// ‘o’ -> ‘ợ’
  DEADTRANS(0x038b, L'u'  , 0x1ef1, NORMAL_CHARACTER),	// ‘u’ -> ‘ự’
  DEADTRANS(0x038b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Long Solidus Overlay + Acute Accent
  DEADTRANS(0x038d, L'O'  , 0x01fe, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǿ’
  DEADTRANS(0x038d, L'o'  , 0x01ff, NORMAL_CHARACTER),	// ‘o’ -> ‘ǿ’
  DEADTRANS(0x038d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Grave Accent
  DEADTRANS(0x03a2, L'U'  , 0x01db, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǜ’
  DEADTRANS(0x03a2, L'u'  , 0x01dc, NORMAL_CHARACTER),	// ‘u’ -> ‘ǜ’
  DEADTRANS(0x03a2, 0x03b9, 0x1fd2, NORMAL_CHARACTER),	// ‘ι’ -> ‘ῒ’
  DEADTRANS(0x03a2, 0x03c5, 0x1fe2, NORMAL_CHARACTER),	// ‘υ’ -> ‘ῢ’
  DEADTRANS(0x03a2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Tilde
  DEADTRANS(0x0530, L'O'  , 0x1e4e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṏ’
  DEADTRANS(0x0530, L'o'  , 0x1e4f, NORMAL_CHARACTER),	// ‘o’ -> ‘ṏ’
  DEADTRANS(0x0530, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Macron
  DEADTRANS(0x0557, L'A'  , 0x01de, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǟ’
  DEADTRANS(0x0557, L'O'  , 0x022a, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȫ’
  DEADTRANS(0x0557, L'U'  , 0x01d5, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǖ’
  DEADTRANS(0x0557, L'a'  , 0x01df, NORMAL_CHARACTER),	// ‘a’ -> ‘ǟ’
  DEADTRANS(0x0557, L'o'  , 0x022b, NORMAL_CHARACTER),	// ‘o’ -> ‘ȫ’
  DEADTRANS(0x0557, L'u'  , 0x01d6, NORMAL_CHARACTER),	// ‘u’ -> ‘ǖ’
  DEADTRANS(0x0557, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Acute Accent
  DEADTRANS(0x0558, L'I'  , 0x1e2e, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḯ’
  DEADTRANS(0x0558, L'U'  , 0x01d7, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǘ’
  DEADTRANS(0x0558, L'i'  , 0x1e2f, NORMAL_CHARACTER),	// ‘i’ -> ‘ḯ’
  DEADTRANS(0x0558, L'u'  , 0x01d8, NORMAL_CHARACTER),	// ‘u’ -> ‘ǘ’
  DEADTRANS(0x0558, 0x03b9, 0x0390, NORMAL_CHARACTER),	// ‘ι’ -> ‘ΐ’
  DEADTRANS(0x0558, 0x03c5, 0x03b0, NORMAL_CHARACTER),	// ‘υ’ -> ‘ΰ’
  DEADTRANS(0x0558, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Caron
  DEADTRANS(0x058b, L'U'  , 0x01d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǚ’
  DEADTRANS(0x058b, L'u'  , 0x01da, NORMAL_CHARACTER),	// ‘u’ -> ‘ǚ’
  DEADTRANS(0x058b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Diaeresis
  DEADTRANS(0x058c, L'U'  , 0x01db, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǜ’
  DEADTRANS(0x058c, L'u'  , 0x01dc, NORMAL_CHARACTER),	// ‘u’ -> ‘ǜ’
  DEADTRANS(0x058c, 0x03b9, 0x1fd2, NORMAL_CHARACTER),	// ‘ι’ -> ‘ῒ’
  DEADTRANS(0x058c, 0x03c5, 0x1fe2, NORMAL_CHARACTER),	// ‘υ’ -> ‘ῢ’
  DEADTRANS(0x058c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Macron
  DEADTRANS(0x0590, L'E'  , 0x1e14, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḕ’
  DEADTRANS(0x0590, L'O'  , 0x1e50, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṑ’
  DEADTRANS(0x0590, L'e'  , 0x1e15, NORMAL_CHARACTER),	// ‘e’ -> ‘ḕ’
  DEADTRANS(0x0590, L'o'  , 0x1e51, NORMAL_CHARACTER),	// ‘o’ -> ‘ṑ’
  DEADTRANS(0x0590, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Circumflex Accent
  DEADTRANS(0x05c8, L'A'  , 0x1ea6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ầ’
  DEADTRANS(0x05c8, L'E'  , 0x1ec0, NORMAL_CHARACTER),	// ‘E’ -> ‘Ề’
  DEADTRANS(0x05c8, L'O'  , 0x1ed2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ồ’
  DEADTRANS(0x05c8, L'a'  , 0x1ea7, NORMAL_CHARACTER),	// ‘a’ -> ‘ầ’
  DEADTRANS(0x05c8, L'e'  , 0x1ec1, NORMAL_CHARACTER),	// ‘e’ -> ‘ề’
  DEADTRANS(0x05c8, L'o'  , 0x1ed3, NORMAL_CHARACTER),	// ‘o’ -> ‘ồ’
  DEADTRANS(0x05c8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Breve
  DEADTRANS(0x05c9, L'A'  , 0x1eb0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ằ’
  DEADTRANS(0x05c9, L'a'  , 0x1eb1, NORMAL_CHARACTER),	// ‘a’ -> ‘ằ’
  DEADTRANS(0x05c9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Horn
  DEADTRANS(0x05ca, L'O'  , 0x1edc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ờ’
  DEADTRANS(0x05ca, L'U'  , 0x1eea, NORMAL_CHARACTER),	// ‘U’ -> ‘Ừ’
  DEADTRANS(0x05ca, L'o'  , 0x1edd, NORMAL_CHARACTER),	// ‘o’ -> ‘ờ’
  DEADTRANS(0x05ca, L'u'  , 0x1eeb, NORMAL_CHARACTER),	// ‘u’ -> ‘ừ’
  DEADTRANS(0x05ca, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Reversed Comma Above
  DEADTRANS(0x05cb, 0x03a9, 0x1f6b, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὣ’
  DEADTRANS(0x05cb, 0x03b1, 0x1f03, NORMAL_CHARACTER),	// ‘α’ -> ‘ἃ’
  DEADTRANS(0x05cb, 0x03b5, 0x1f13, NORMAL_CHARACTER),	// ‘ε’ -> ‘ἓ’
  DEADTRANS(0x05cb, 0x03b7, 0x1f23, NORMAL_CHARACTER),	// ‘η’ -> ‘ἣ’
  DEADTRANS(0x05cb, 0x03b9, 0x1f33, NORMAL_CHARACTER),	// ‘ι’ -> ‘ἳ’
  DEADTRANS(0x05cb, 0x03bf, 0x1f43, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὃ’
  DEADTRANS(0x05cb, 0x03c5, 0x1f53, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὓ’
  DEADTRANS(0x05cb, 0x03c9, 0x1f63, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὣ’
  DEADTRANS(0x05cb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Diaeresis
  DEADTRANS(0x05cc, L'O'  , 0x1e4e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṏ’
  DEADTRANS(0x05cc, L'o'  , 0x1e4f, NORMAL_CHARACTER),	// ‘o’ -> ‘ṏ’
  DEADTRANS(0x05cc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Macron
  DEADTRANS(0x05cd, L'O'  , 0x022c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȭ’
  DEADTRANS(0x05cd, L'o'  , 0x022d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȭ’
  DEADTRANS(0x05cd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Acute Accent
  DEADTRANS(0x05ce, L'O'  , 0x1e4c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṍ’
  DEADTRANS(0x05ce, L'U'  , 0x1e78, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṹ’
  DEADTRANS(0x05ce, L'o'  , 0x1e4d, NORMAL_CHARACTER),	// ‘o’ -> ‘ṍ’
  DEADTRANS(0x05ce, L'u'  , 0x1e79, NORMAL_CHARACTER),	// ‘u’ -> ‘ṹ’
  DEADTRANS(0x05ce, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Circumflex Accent
  DEADTRANS(0x05cf, L'A'  , 0x1eaa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẫ’
  DEADTRANS(0x05cf, L'E'  , 0x1ec4, NORMAL_CHARACTER),	// ‘E’ -> ‘Ễ’
  DEADTRANS(0x05cf, L'O'  , 0x1ed6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỗ’
  DEADTRANS(0x05cf, L'a'  , 0x1eab, NORMAL_CHARACTER),	// ‘a’ -> ‘ẫ’
  DEADTRANS(0x05cf, L'e'  , 0x1ec5, NORMAL_CHARACTER),	// ‘e’ -> ‘ễ’
  DEADTRANS(0x05cf, L'o'  , 0x1ed7, NORMAL_CHARACTER),	// ‘o’ -> ‘ỗ’
  DEADTRANS(0x05cf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Breve
  DEADTRANS(0x05eb, L'A'  , 0x1eb4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẵ’
  DEADTRANS(0x05eb, L'a'  , 0x1eb5, NORMAL_CHARACTER),	// ‘a’ -> ‘ẵ’
  DEADTRANS(0x05eb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Horn
  DEADTRANS(0x05ec, L'O'  , 0x1ee0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỡ’
  DEADTRANS(0x05ec, L'U'  , 0x1eee, NORMAL_CHARACTER),	// ‘U’ -> ‘Ữ’
  DEADTRANS(0x05ec, L'o'  , 0x1ee1, NORMAL_CHARACTER),	// ‘o’ -> ‘ỡ’
  DEADTRANS(0x05ec, L'u'  , 0x1eef, NORMAL_CHARACTER),	// ‘u’ -> ‘ữ’
  DEADTRANS(0x05ec, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Dot Below
  DEADTRANS(0x05ed, L'L'  , 0x1e38, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḹ’
  DEADTRANS(0x05ed, L'R'  , 0x1e5c, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṝ’
  DEADTRANS(0x05ed, L'l'  , 0x1e39, NORMAL_CHARACTER),	// ‘l’ -> ‘ḹ’
  DEADTRANS(0x05ed, L'r'  , 0x1e5d, NORMAL_CHARACTER),	// ‘r’ -> ‘ṝ’
  DEADTRANS(0x05ed, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Diaeresis
  DEADTRANS(0x05ee, L'A'  , 0x01de, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǟ’
  DEADTRANS(0x05ee, L'O'  , 0x022a, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȫ’
  DEADTRANS(0x05ee, L'U'  , 0x1e7a, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṻ’
  DEADTRANS(0x05ee, L'a'  , 0x01df, NORMAL_CHARACTER),	// ‘a’ -> ‘ǟ’
  DEADTRANS(0x05ee, L'o'  , 0x022b, NORMAL_CHARACTER),	// ‘o’ -> ‘ȫ’
  DEADTRANS(0x05ee, L'u'  , 0x1e7b, NORMAL_CHARACTER),	// ‘u’ -> ‘ṻ’
  DEADTRANS(0x05ee, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Grave Accent
  DEADTRANS(0x05f5, L'E'  , 0x1e14, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḕ’
  DEADTRANS(0x05f5, L'O'  , 0x1e50, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṑ’
  DEADTRANS(0x05f5, L'e'  , 0x1e15, NORMAL_CHARACTER),	// ‘e’ -> ‘ḕ’
  DEADTRANS(0x05f5, L'o'  , 0x1e51, NORMAL_CHARACTER),	// ‘o’ -> ‘ṑ’
  DEADTRANS(0x05f5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Tilde
  DEADTRANS(0x05f6, L'O'  , 0x022c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȭ’
  DEADTRANS(0x05f6, L'o'  , 0x022d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȭ’
  DEADTRANS(0x05f6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Acute Accent
  DEADTRANS(0x05f7, L'E'  , 0x1e16, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḗ’
  DEADTRANS(0x05f7, L'O'  , 0x1e52, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṓ’
  DEADTRANS(0x05f7, L'e'  , 0x1e17, NORMAL_CHARACTER),	// ‘e’ -> ‘ḗ’
  DEADTRANS(0x05f7, L'o'  , 0x1e53, NORMAL_CHARACTER),	// ‘o’ -> ‘ṓ’
  DEADTRANS(0x05f7, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Macron + Dot Above
  DEADTRANS(0x05f8, L'A'  , 0x01e0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǡ’
  DEADTRANS(0x05f8, L'O'  , 0x0230, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȱ’
  DEADTRANS(0x05f8, L'a'  , 0x01e1, NORMAL_CHARACTER),	// ‘a’ -> ‘ǡ’
  DEADTRANS(0x05f8, L'o'  , 0x0231, NORMAL_CHARACTER),	// ‘o’ -> ‘ȱ’
  DEADTRANS(0x05f8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Cedilla
  DEADTRANS(0x05f9, L'C'  , 0x1e08, NORMAL_CHARACTER),	// ‘C’ -> ‘Ḉ’
  DEADTRANS(0x05f9, L'c'  , 0x1e09, NORMAL_CHARACTER),	// ‘c’ -> ‘ḉ’
  DEADTRANS(0x05f9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Long Solidus Overlay
  DEADTRANS(0x05fa, L'O'  , 0x01fe, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǿ’
  DEADTRANS(0x05fa, L'o'  , 0x01ff, NORMAL_CHARACTER),	// ‘o’ -> ‘ǿ’
  DEADTRANS(0x05fa, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Diaeresis
  DEADTRANS(0x05fb, L'I'  , 0x1e2e, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḯ’
  DEADTRANS(0x05fb, L'U'  , 0x01d7, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǘ’
  DEADTRANS(0x05fb, L'i'  , 0x1e2f, NORMAL_CHARACTER),	// ‘i’ -> ‘ḯ’
  DEADTRANS(0x05fb, L'u'  , 0x01d8, NORMAL_CHARACTER),	// ‘u’ -> ‘ǘ’
  DEADTRANS(0x05fb, 0x03b9, 0x0390, NORMAL_CHARACTER),	// ‘ι’ -> ‘ΐ’
  DEADTRANS(0x05fb, 0x03c5, 0x03b0, NORMAL_CHARACTER),	// ‘υ’ -> ‘ΰ’
  DEADTRANS(0x05fb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Tilde
  DEADTRANS(0x05fc, L'O'  , 0x1e4c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṍ’
  DEADTRANS(0x05fc, L'U'  , 0x1e78, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṹ’
  DEADTRANS(0x05fc, L'o'  , 0x1e4d, NORMAL_CHARACTER),	// ‘o’ -> ‘ṍ’
  DEADTRANS(0x05fc, L'u'  , 0x1e79, NORMAL_CHARACTER),	// ‘u’ -> ‘ṹ’
  DEADTRANS(0x05fc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Macron
  DEADTRANS(0x05fd, L'E'  , 0x1e16, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḗ’
  DEADTRANS(0x05fd, L'O'  , 0x1e52, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṓ’
  DEADTRANS(0x05fd, L'e'  , 0x1e17, NORMAL_CHARACTER),	// ‘e’ -> ‘ḗ’
  DEADTRANS(0x05fd, L'o'  , 0x1e53, NORMAL_CHARACTER),	// ‘o’ -> ‘ṓ’
  DEADTRANS(0x05fd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Circumflex Accent
  DEADTRANS(0x05fe, L'A'  , 0x1ea4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ấ’
  DEADTRANS(0x05fe, L'E'  , 0x1ebe, NORMAL_CHARACTER),	// ‘E’ -> ‘Ế’
  DEADTRANS(0x05fe, L'O'  , 0x1ed0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ố’
  DEADTRANS(0x05fe, L'a'  , 0x1ea5, NORMAL_CHARACTER),	// ‘a’ -> ‘ấ’
  DEADTRANS(0x05fe, L'e'  , 0x1ebf, NORMAL_CHARACTER),	// ‘e’ -> ‘ế’
  DEADTRANS(0x05fe, L'o'  , 0x1ed1, NORMAL_CHARACTER),	// ‘o’ -> ‘ố’
  DEADTRANS(0x05fe, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Breve
  DEADTRANS(0x05ff, L'A'  , 0x1eae, NORMAL_CHARACTER),	// ‘A’ -> ‘Ắ’
  DEADTRANS(0x05ff, L'a'  , 0x1eaf, NORMAL_CHARACTER),	// ‘a’ -> ‘ắ’
  DEADTRANS(0x05ff, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Dot Above
  DEADTRANS(0x061d, L'S'  , 0x1e64, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṥ’
  DEADTRANS(0x061d, L's'  , 0x1e65, NORMAL_CHARACTER),	// ‘s’ -> ‘ṥ’
  DEADTRANS(0x061d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Ring Above
  DEADTRANS(0x070e, L'A'  , 0x01fa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǻ’
  DEADTRANS(0x070e, L'a'  , 0x01fb, NORMAL_CHARACTER),	// ‘a’ -> ‘ǻ’
  DEADTRANS(0x070e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Horn
  DEADTRANS(0x074b, L'O'  , 0x1eda, NORMAL_CHARACTER),	// ‘O’ -> ‘Ớ’
  DEADTRANS(0x074b, L'U'  , 0x1ee8, NORMAL_CHARACTER),	// ‘U’ -> ‘Ứ’
  DEADTRANS(0x074b, L'o'  , 0x1edb, NORMAL_CHARACTER),	// ‘o’ -> ‘ớ’
  DEADTRANS(0x074b, L'u'  , 0x1ee9, NORMAL_CHARACTER),	// ‘u’ -> ‘ứ’
  DEADTRANS(0x074b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Reversed Comma Above
  DEADTRANS(0x074c, 0x03a9, 0x1f6d, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὥ’
  DEADTRANS(0x074c, 0x03b1, 0x1f05, NORMAL_CHARACTER),	// ‘α’ -> ‘ἅ’
  DEADTRANS(0x074c, 0x03b5, 0x1f15, NORMAL_CHARACTER),	// ‘ε’ -> ‘ἕ’
  DEADTRANS(0x074c, 0x03b7, 0x1f25, NORMAL_CHARACTER),	// ‘η’ -> ‘ἥ’
  DEADTRANS(0x074c, 0x03b9, 0x1f35, NORMAL_CHARACTER),	// ‘ι’ -> ‘ἵ’
  DEADTRANS(0x074c, 0x03bf, 0x1f45, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὅ’
  DEADTRANS(0x074c, 0x03c5, 0x1f55, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὕ’
  DEADTRANS(0x074c, 0x03c9, 0x1f65, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὥ’
  DEADTRANS(0x074c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Dot Below
  DEADTRANS(0x07b2, L'A'  , 0x1eac, NORMAL_CHARACTER),	// ‘A’ -> ‘Ậ’
  DEADTRANS(0x07b2, L'E'  , 0x1ec6, NORMAL_CHARACTER),	// ‘E’ -> ‘Ệ’
  DEADTRANS(0x07b2, L'O'  , 0x1ed8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ộ’
  DEADTRANS(0x07b2, L'a'  , 0x1ead, NORMAL_CHARACTER),	// ‘a’ -> ‘ậ’
  DEADTRANS(0x07b2, L'e'  , 0x1ec7, NORMAL_CHARACTER),	// ‘e’ -> ‘ệ’
  DEADTRANS(0x07b2, L'o'  , 0x1ed9, NORMAL_CHARACTER),	// ‘o’ -> ‘ộ’
  DEADTRANS(0x07b2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Grave Accent
  DEADTRANS(0x07b3, L'A'  , 0x1ea6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ầ’
  DEADTRANS(0x07b3, L'E'  , 0x1ec0, NORMAL_CHARACTER),	// ‘E’ -> ‘Ề’
  DEADTRANS(0x07b3, L'O'  , 0x1ed2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ồ’
  DEADTRANS(0x07b3, L'a'  , 0x1ea7, NORMAL_CHARACTER),	// ‘a’ -> ‘ầ’
  DEADTRANS(0x07b3, L'e'  , 0x1ec1, NORMAL_CHARACTER),	// ‘e’ -> ‘ề’
  DEADTRANS(0x07b3, L'o'  , 0x1ed3, NORMAL_CHARACTER),	// ‘o’ -> ‘ồ’
  DEADTRANS(0x07b3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Tilde
  DEADTRANS(0x07b4, L'A'  , 0x1eaa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẫ’
  DEADTRANS(0x07b4, L'E'  , 0x1ec4, NORMAL_CHARACTER),	// ‘E’ -> ‘Ễ’
  DEADTRANS(0x07b4, L'O'  , 0x1ed6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỗ’
  DEADTRANS(0x07b4, L'a'  , 0x1eab, NORMAL_CHARACTER),	// ‘a’ -> ‘ẫ’
  DEADTRANS(0x07b4, L'e'  , 0x1ec5, NORMAL_CHARACTER),	// ‘e’ -> ‘ễ’
  DEADTRANS(0x07b4, L'o'  , 0x1ed7, NORMAL_CHARACTER),	// ‘o’ -> ‘ỗ’
  DEADTRANS(0x07b4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Acute Accent
  DEADTRANS(0x07b5, L'A'  , 0x1ea4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ấ’
  DEADTRANS(0x07b5, L'E'  , 0x1ebe, NORMAL_CHARACTER),	// ‘E’ -> ‘Ế’
  DEADTRANS(0x07b5, L'O'  , 0x1ed0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ố’
  DEADTRANS(0x07b5, L'a'  , 0x1ea5, NORMAL_CHARACTER),	// ‘a’ -> ‘ấ’
  DEADTRANS(0x07b5, L'e'  , 0x1ebf, NORMAL_CHARACTER),	// ‘e’ -> ‘ế’
  DEADTRANS(0x07b5, L'o'  , 0x1ed1, NORMAL_CHARACTER),	// ‘o’ -> ‘ố’
  DEADTRANS(0x07b5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Caron + Diaeresis
  DEADTRANS(0x07b6, L'U'  , 0x01d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǚ’
  DEADTRANS(0x07b6, L'u'  , 0x01da, NORMAL_CHARACTER),	// ‘u’ -> ‘ǚ’
  DEADTRANS(0x07b6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Caron + Dot Above
  DEADTRANS(0x07b7, L'S'  , 0x1e66, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṧ’
  DEADTRANS(0x07b7, L's'  , 0x1e67, NORMAL_CHARACTER),	// ‘s’ -> ‘ṧ’
  DEADTRANS(0x07b7, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Cedilla
  DEADTRANS(0x07b8, L'E'  , 0x1e1c, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḝ’
  DEADTRANS(0x07b8, L'e'  , 0x1e1d, NORMAL_CHARACTER),	// ‘e’ -> ‘ḝ’
  DEADTRANS(0x07b8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Dot Below
  DEADTRANS(0x07b9, L'A'  , 0x1eb6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ặ’
  DEADTRANS(0x07b9, L'a'  , 0x1eb7, NORMAL_CHARACTER),	// ‘a’ -> ‘ặ’
  DEADTRANS(0x07b9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Grave Accent
  DEADTRANS(0x07ba, L'A'  , 0x1eb0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ằ’
  DEADTRANS(0x07ba, L'a'  , 0x1eb1, NORMAL_CHARACTER),	// ‘a’ -> ‘ằ’
  DEADTRANS(0x07ba, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Tilde
  DEADTRANS(0x07bb, L'A'  , 0x1eb4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẵ’
  DEADTRANS(0x07bb, L'a'  , 0x1eb5, NORMAL_CHARACTER),	// ‘a’ -> ‘ẵ’
  DEADTRANS(0x07bb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Breve + Acute Accent
  DEADTRANS(0x07bc, L'A'  , 0x1eae, NORMAL_CHARACTER),	// ‘A’ -> ‘Ắ’
  DEADTRANS(0x07bc, L'a'  , 0x1eaf, NORMAL_CHARACTER),	// ‘a’ -> ‘ắ’
  DEADTRANS(0x07bc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Dot Below
  DEADTRANS(0x07bd, L'S'  , 0x1e68, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṩ’
  DEADTRANS(0x07bd, L's'  , 0x1e69, NORMAL_CHARACTER),	// ‘s’ -> ‘ṩ’
  DEADTRANS(0x07bd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Macron
  DEADTRANS(0x07be, L'A'  , 0x01e0, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǡ’
  DEADTRANS(0x07be, L'O'  , 0x0230, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȱ’
  DEADTRANS(0x07be, L'a'  , 0x01e1, NORMAL_CHARACTER),	// ‘a’ -> ‘ǡ’
  DEADTRANS(0x07be, L'o'  , 0x0231, NORMAL_CHARACTER),	// ‘o’ -> ‘ȱ’
  DEADTRANS(0x07be, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Acute Accent
  DEADTRANS(0x07bf, L'S'  , 0x1e64, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṥ’
  DEADTRANS(0x07bf, L's'  , 0x1e65, NORMAL_CHARACTER),	// ‘s’ -> ‘ṥ’
  DEADTRANS(0x07bf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Dot Above + Caron
  DEADTRANS(0x07fb, L'S'  , 0x1e66, NORMAL_CHARACTER),	// ‘S’ -> ‘Ṧ’
  DEADTRANS(0x07fb, L's'  , 0x1e67, NORMAL_CHARACTER),	// ‘s’ -> ‘ṧ’
  DEADTRANS(0x07fb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Ring Above + Acute Accent
  DEADTRANS(0x07fc, L'A'  , 0x01fa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǻ’
  DEADTRANS(0x07fc, L'a'  , 0x01fb, NORMAL_CHARACTER),	// ‘a’ -> ‘ǻ’
  DEADTRANS(0x07fc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Dot Below
  DEADTRANS(0x082e, L'O'  , 0x1ee2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ợ’
  DEADTRANS(0x082e, L'U'  , 0x1ef0, NORMAL_CHARACTER),	// ‘U’ -> ‘Ự’
  DEADTRANS(0x082e, L'o'  , 0x1ee3, NORMAL_CHARACTER),	// ‘o’ -> ‘ợ’
  DEADTRANS(0x082e, L'u'  , 0x1ef1, NORMAL_CHARACTER),	// ‘u’ -> ‘ự’
  DEADTRANS(0x082e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Grave Accent
  DEADTRANS(0x082f, L'O'  , 0x1edc, NORMAL_CHARACTER),	// ‘O’ -> ‘Ờ’
  DEADTRANS(0x082f, L'U'  , 0x1eea, NORMAL_CHARACTER),	// ‘U’ -> ‘Ừ’
  DEADTRANS(0x082f, L'o'  , 0x1edd, NORMAL_CHARACTER),	// ‘o’ -> ‘ờ’
  DEADTRANS(0x082f, L'u'  , 0x1eeb, NORMAL_CHARACTER),	// ‘u’ -> ‘ừ’
  DEADTRANS(0x082f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Tilde
  DEADTRANS(0x083f, L'O'  , 0x1ee0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỡ’
  DEADTRANS(0x083f, L'U'  , 0x1eee, NORMAL_CHARACTER),	// ‘U’ -> ‘Ữ’
  DEADTRANS(0x083f, L'o'  , 0x1ee1, NORMAL_CHARACTER),	// ‘o’ -> ‘ỡ’
  DEADTRANS(0x083f, L'u'  , 0x1eef, NORMAL_CHARACTER),	// ‘u’ -> ‘ữ’
  DEADTRANS(0x083f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Horn + Acute Accent
  DEADTRANS(0x085c, L'O'  , 0x1eda, NORMAL_CHARACTER),	// ‘O’ -> ‘Ớ’
  DEADTRANS(0x085c, L'U'  , 0x1ee8, NORMAL_CHARACTER),	// ‘U’ -> ‘Ứ’
  DEADTRANS(0x085c, L'o'  , 0x1edb, NORMAL_CHARACTER),	// ‘o’ -> ‘ớ’
  DEADTRANS(0x085c, L'u'  , 0x1ee9, NORMAL_CHARACTER),	// ‘u’ -> ‘ứ’
  DEADTRANS(0x085c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Reversed Comma Above + Grave Accent
  DEADTRANS(0x085d, 0x03a9, 0x1f6b, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὣ’
  DEADTRANS(0x085d, 0x03b1, 0x1f03, NORMAL_CHARACTER),	// ‘α’ -> ‘ἃ’
  DEADTRANS(0x085d, 0x03b5, 0x1f13, NORMAL_CHARACTER),	// ‘ε’ -> ‘ἓ’
  DEADTRANS(0x085d, 0x03b7, 0x1f23, NORMAL_CHARACTER),	// ‘η’ -> ‘ἣ’
  DEADTRANS(0x085d, 0x03b9, 0x1f33, NORMAL_CHARACTER),	// ‘ι’ -> ‘ἳ’
  DEADTRANS(0x085d, 0x03bf, 0x1f43, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὃ’
  DEADTRANS(0x085d, 0x03c5, 0x1f53, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὓ’
  DEADTRANS(0x085d, 0x03c9, 0x1f63, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὣ’
  DEADTRANS(0x085d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Reversed Comma Above + Acute Accent
  DEADTRANS(0x085f, 0x03a9, 0x1f6d, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὥ’
  DEADTRANS(0x085f, 0x03b1, 0x1f05, NORMAL_CHARACTER),	// ‘α’ -> ‘ἅ’
  DEADTRANS(0x085f, 0x03b5, 0x1f15, NORMAL_CHARACTER),	// ‘ε’ -> ‘ἕ’
  DEADTRANS(0x085f, 0x03b7, 0x1f25, NORMAL_CHARACTER),	// ‘η’ -> ‘ἥ’
  DEADTRANS(0x085f, 0x03b9, 0x1f35, NORMAL_CHARACTER),	// ‘ι’ -> ‘ἵ’
  DEADTRANS(0x085f, 0x03bf, 0x1f45, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὅ’
  DEADTRANS(0x085f, 0x03c5, 0x1f55, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὕ’
  DEADTRANS(0x085f, 0x03c9, 0x1f65, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὥ’
  DEADTRANS(0x085f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +  
  DEADTRANS(0x086b, L'#'  , 0x2007, NORMAL_CHARACTER),	// U+0023 -> U+2007
  DEADTRANS(0x086b, L'%'  , 0x205f, NORMAL_CHARACTER),	// U+0025 -> U+205F
  DEADTRANS(0x086b, L'*'  , 0x2062, NORMAL_CHARACTER),	// U+002A -> U+2062
  DEADTRANS(0x086b, L'+'  , 0x2064, NORMAL_CHARACTER),	// U+002B -> U+2064
  DEADTRANS(0x086b, L','  , 0x202f, NORMAL_CHARACTER),	// U+002C -> U+202F
  DEADTRANS(0x086b, L'-'  , 0x00ad, NORMAL_CHARACTER),	// U+002D -> U+00AD
  DEADTRANS(0x086b, L'0'  , 0x200b, NORMAL_CHARACTER),	// ‘0’ -> U+200B
  DEADTRANS(0x086b, L'1'  , 0x200a, NORMAL_CHARACTER),	// ‘1’ -> U+200A
  DEADTRANS(0x086b, L'2'  , 0x2009, NORMAL_CHARACTER),	// ‘2’ -> U+2009
  DEADTRANS(0x086b, L'3'  , 0x2004, NORMAL_CHARACTER),	// ‘3’ -> U+2004
  DEADTRANS(0x086b, L'4'  , 0x2005, NORMAL_CHARACTER),	// ‘4’ -> U+2005
  DEADTRANS(0x086b, L'6'  , 0x2006, NORMAL_CHARACTER),	// ‘6’ -> U+2006
  DEADTRANS(0x086b, L':'  , 0x2008, NORMAL_CHARACTER),	// U+003A -> U+2008
  DEADTRANS(0x086b, L';'  , 0x2063, NORMAL_CHARACTER),	// U+003B -> U+2063
  DEADTRANS(0x086b, L'<'  , 0x09d4, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose +   + <›
  DEADTRANS(0x086b, L'>'  , 0x09d5, CHAINED_DEAD_KEY),	// U+003E -> ‹Compose +   + >›
  DEADTRANS(0x086b, L'?'  , 0xfffd, NORMAL_CHARACTER),	// U+003F -> U+FFFD
  DEADTRANS(0x086b, L'F'  , 0x2766, NORMAL_CHARACTER),	// ‘F’ -> U+2766
  DEADTRANS(0x086b, L'J'  , 0x200d, NORMAL_CHARACTER),	// ‘J’ -> U+200D
  DEADTRANS(0x086b, L'L'  , 0x2028, NORMAL_CHARACTER),	// ‘L’ -> U+2028
  DEADTRANS(0x086b, L'P'  , 0x2029, NORMAL_CHARACTER),	// ‘P’ -> U+2029
  DEADTRANS(0x086b, L'b'  , 0x2422, NORMAL_CHARACTER),	// ‘b’ -> U+2422
  DEADTRANS(0x086b, L'c'  , 0x09d6, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Compose +   + c›
  DEADTRANS(0x086b, L'f'  , 0x2061, NORMAL_CHARACTER),	// ‘f’ -> U+2061
  DEADTRANS(0x086b, L'h'  , 0x09d8, CHAINED_DEAD_KEY),	// ‘h’ -> ‹Compose +   + h›
  DEADTRANS(0x086b, L'j'  , 0x200c, NORMAL_CHARACTER),	// ‘j’ -> U+200C
  DEADTRANS(0x086b, L'l'  , 0x09d9, CHAINED_DEAD_KEY),	// ‘l’ -> ‹Compose +   + l›
  DEADTRANS(0x086b, L'm'  , 0x2003, NORMAL_CHARACTER),	// ‘m’ -> U+2003
  DEADTRANS(0x086b, L'n'  , 0x2002, NORMAL_CHARACTER),	// ‘n’ -> U+2002
  DEADTRANS(0x086b, L'o'  , 0x25cc, NORMAL_CHARACTER),	// ‘o’ -> U+25CC
  DEADTRANS(0x086b, L'r'  , L'\r' , NORMAL_CHARACTER),	// ‘r’ -> U+000D
  DEADTRANS(0x086b, L's'  , 0x09da, CHAINED_DEAD_KEY),	// ‘s’ -> ‹Compose +   + s›
  DEADTRANS(0x086b, L't'  , L'\t' , NORMAL_CHARACTER),	// ‘t’ -> U+0009
  DEADTRANS(0x086b, L'w'  , 0x2060, NORMAL_CHARACTER),	// ‘w’ -> U+2060
  DEADTRANS(0x086b, L'|'  , 0x034f, NORMAL_CHARACTER),	// U+007C -> U+034F
  DEADTRANS(0x086b, L'~'  , 0x00a0, NORMAL_CHARACTER),	// U+007E -> U+00A0
  DEADTRANS(0x086b, 0x00a0, 0x237d, NORMAL_CHARACTER),	// U+00A0 -> U+237D
  DEADTRANS(0x086b, 0x202f, 0x237d, NORMAL_CHARACTER),	// U+202F -> U+237D
  DEADTRANS(0x086b, L' '  , 0x2423, NORMAL_CHARACTER),	// U+0020 -> U+2423

// Dead key: Compose + !
  DEADTRANS(0x086c, L'!'  , 0x203c, NORMAL_CHARACTER),	// U+0021 -> U+203C
  DEADTRANS(0x086c, L'-'  , 0x09db, CHAINED_DEAD_KEY),	// U+002D -> ‹Compose + ! + -›
  DEADTRANS(0x086c, L'<'  , 0x09e5, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + ! + <›
  DEADTRANS(0x086c, L'='  , 0x2260, NORMAL_CHARACTER),	// U+003D -> U+2260
  DEADTRANS(0x086c, L'>'  , 0x09ff, CHAINED_DEAD_KEY),	// U+003E -> ‹Compose + ! + >›
  DEADTRANS(0x086c, L'?'  , 0x2049, NORMAL_CHARACTER),	// U+003F -> U+2049
  DEADTRANS(0x086c, L'E'  , 0x0a00, CHAINED_DEAD_KEY),	// ‘E’ -> ‹Compose + ! + E›
  DEADTRANS(0x086c, L'~'  , 0x0a04, CHAINED_DEAD_KEY),	// U+007E -> ‹Compose + ! + ~›
  DEADTRANS(0x086c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + "
  DEADTRANS(0x086d, L'"'  , 0x201d, NORMAL_CHARACTER),	// U+0022 -> U+201D
  DEADTRANS(0x086d, L'\'' , 0x201c, NORMAL_CHARACTER),	// U+0027 -> U+201C
  DEADTRANS(0x086d, L','  , 0x201e, NORMAL_CHARACTER),	// U+002C -> U+201E
  DEADTRANS(0x086d, L'.'  , 0x030b, NORMAL_CHARACTER),	// U+002E -> U+030B
  DEADTRANS(0x086d, L'<'  , 0x00ab, NORMAL_CHARACTER),	// U+003C -> U+00AB
  DEADTRANS(0x086d, L'>'  , 0x00bb, NORMAL_CHARACTER),	// U+003E -> U+00BB
  DEADTRANS(0x086d, L'O'  , 0x0150, NORMAL_CHARACTER),	// ‘O’ -> ‘Ő’
  DEADTRANS(0x086d, L'U'  , 0x0170, NORMAL_CHARACTER),	// ‘U’ -> ‘Ű’
  DEADTRANS(0x086d, L'o'  , 0x0151, NORMAL_CHARACTER),	// ‘o’ -> ‘ő’
  DEADTRANS(0x086d, L'u'  , 0x0171, NORMAL_CHARACTER),	// ‘u’ -> ‘ű’
  DEADTRANS(0x086d, L' '  , 0x02dd, NORMAL_CHARACTER),	// U+0020 -> U+02DD

// Dead key: Compose + #
  DEADTRANS(0x086e, L'#'  , 0x266f, NORMAL_CHARACTER),	// U+0023 -> U+266F
  DEADTRANS(0x086e, L'*'  , 0x29c6, NORMAL_CHARACTER),	// U+002A -> U+29C6
  DEADTRANS(0x086e, L'+'  , 0x229e, NORMAL_CHARACTER),	// U+002B -> U+229E
  DEADTRANS(0x086e, L'-'  , 0x229f, NORMAL_CHARACTER),	// U+002D -> U+229F
  DEADTRANS(0x086e, L'.'  , 0x22a1, NORMAL_CHARACTER),	// U+002E -> U+22A1
  DEADTRANS(0x086e, L'/'  , 0x29c4, NORMAL_CHARACTER),	// U+002F -> U+29C4
  DEADTRANS(0x086e, L'P'  , 0x204b, NORMAL_CHARACTER),	// ‘P’ -> U+204B
  DEADTRANS(0x086e, L'X'  , 0x2612, NORMAL_CHARACTER),	// ‘X’ -> U+2612
  DEADTRANS(0x086e, L'\\' , 0x29c5, NORMAL_CHARACTER),	// U+005C -> U+29C5
  DEADTRANS(0x086e, L'a'  , 0x2693, NORMAL_CHARACTER),	// ‘a’ -> U+2693
  DEADTRANS(0x086e, L'o'  , 0x29c7, NORMAL_CHARACTER),	// ‘o’ -> U+29C7
  DEADTRANS(0x086e, L'p'  , 0x00b6, NORMAL_CHARACTER),	// ‘p’ -> U+00B6
  DEADTRANS(0x086e, L's'  , 0x00a7, NORMAL_CHARACTER),	// ‘s’ -> U+00A7
  DEADTRANS(0x086e, L'v'  , 0x2611, NORMAL_CHARACTER),	// ‘v’ -> U+2611
  DEADTRANS(0x086e, L'x'  , 0x22a0, NORMAL_CHARACTER),	// ‘x’ -> U+22A0
  DEADTRANS(0x086e, L'~'  , 0x2318, NORMAL_CHARACTER),	// U+007E -> U+2318
  DEADTRANS(0x086e, 0x2218, 0x29c7, NORMAL_CHARACTER),	// U+2218 -> U+29C7
  DEADTRANS(0x086e, L' '  , 0x2610, NORMAL_CHARACTER),	// U+0020 -> U+2610

// Dead key: Compose + $
  DEADTRANS(0x086f, L'&'  , 0x20b0, NORMAL_CHARACTER),	// U+0026 -> U+20B0
  DEADTRANS(0x086f, L'A'  , 0x20b3, NORMAL_CHARACTER),	// ‘A’ -> U+20B3
  DEADTRANS(0x086f, L'B'  , 0x0e3f, NORMAL_CHARACTER),	// ‘B’ -> U+0E3F
  DEADTRANS(0x086f, L'C'  , 0x20a1, NORMAL_CHARACTER),	// ‘C’ -> U+20A1
  DEADTRANS(0x086f, L'D'  , 0x20af, NORMAL_CHARACTER),	// ‘D’ -> U+20AF
  DEADTRANS(0x086f, L'F'  , 0x20a3, NORMAL_CHARACTER),	// ‘F’ -> U+20A3
  DEADTRANS(0x086f, L'I'  , 0x0294, NORMAL_CHARACTER),	// ‘I’ -> ‘ʔ’
  DEADTRANS(0x086f, L'L'  , 0x20a4, NORMAL_CHARACTER),	// ‘L’ -> U+20A4
  DEADTRANS(0x086f, L'M'  , 0x2133, NORMAL_CHARACTER),	// ‘M’ -> ‘ℳ’
  DEADTRANS(0x086f, L'O'  , 0x0af1, NORMAL_CHARACTER),	// ‘O’ -> U+0AF1
  DEADTRANS(0x086f, L'P'  , 0x20a7, NORMAL_CHARACTER),	// ‘P’ -> U+20A7
  DEADTRANS(0x086f, L'R'  , 0x20b9, NORMAL_CHARACTER),	// ‘R’ -> U+20B9
  DEADTRANS(0x086f, L'S'  , 0x20b7, NORMAL_CHARACTER),	// ‘S’ -> U+20B7
  DEADTRANS(0x086f, L'T'  , 0x20ae, NORMAL_CHARACTER),	// ‘T’ -> U+20AE
  DEADTRANS(0x086f, L'U'  , 0x5713, NORMAL_CHARACTER),	// ‘U’ -> ‘圓’
  DEADTRANS(0x086f, L'Y'  , 0x5186, NORMAL_CHARACTER),	// ‘Y’ -> ‘円’
  DEADTRANS(0x086f, L'a'  , 0x060b, NORMAL_CHARACTER),	// ‘a’ -> U+060B
  DEADTRANS(0x086f, L'b'  , 0x20bf, NORMAL_CHARACTER),	// ‘b’ -> U+20BF
  DEADTRANS(0x086f, L'c'  , 0x00a2, NORMAL_CHARACTER),	// ‘c’ -> U+00A2
  DEADTRANS(0x086f, L'd'  , 0x20ab, NORMAL_CHARACTER),	// ‘d’ -> U+20AB
  DEADTRANS(0x086f, L'e'  , 0x20a0, NORMAL_CHARACTER),	// ‘e’ -> U+20A0
  DEADTRANS(0x086f, L'f'  , 0x0192, NORMAL_CHARACTER),	// ‘f’ -> ‘ƒ’
  DEADTRANS(0x086f, L'g'  , 0x20b2, NORMAL_CHARACTER),	// ‘g’ -> U+20B2
  DEADTRANS(0x086f, L'h'  , 0x20b4, NORMAL_CHARACTER),	// ‘h’ -> U+20B4
  DEADTRANS(0x086f, L'i'  , 0xfdfc, NORMAL_CHARACTER),	// ‘i’ -> U+FDFC
  DEADTRANS(0x086f, L'k'  , 0x20ad, NORMAL_CHARACTER),	// ‘k’ -> U+20AD
  DEADTRANS(0x086f, L'l'  , 0x20ba, NORMAL_CHARACTER),	// ‘l’ -> U+20BA
  DEADTRANS(0x086f, L'm'  , 0x20a5, NORMAL_CHARACTER),	// ‘m’ -> U+20A5
  DEADTRANS(0x086f, L'n'  , 0x20a6, NORMAL_CHARACTER),	// ‘n’ -> U+20A6
  DEADTRANS(0x086f, L'o'  , 0x0bf9, NORMAL_CHARACTER),	// ‘o’ -> U+0BF9
  DEADTRANS(0x086f, L'p'  , 0x20b1, NORMAL_CHARACTER),	// ‘p’ -> U+20B1
  DEADTRANS(0x086f, L'r'  , 0x20bd, NORMAL_CHARACTER),	// ‘r’ -> U+20BD
  DEADTRANS(0x086f, L's'  , 0x20aa, NORMAL_CHARACTER),	// ‘s’ -> U+20AA
  DEADTRANS(0x086f, L't'  , 0x20b8, NORMAL_CHARACTER),	// ‘t’ -> U+20B8
  DEADTRANS(0x086f, L'u'  , 0x5143, NORMAL_CHARACTER),	// ‘u’ -> ‘元’
  DEADTRANS(0x086f, L'w'  , 0x20a9, NORMAL_CHARACTER),	// ‘w’ -> U+20A9
  DEADTRANS(0x086f, L'y'  , 0x00a5, NORMAL_CHARACTER),	// ‘y’ -> U+00A5
  DEADTRANS(0x086f, 0x00a3, 0x20b6, NORMAL_CHARACTER),	// U+00A3 -> U+20B6
  DEADTRANS(0x086f, L','  , 0x20a2, NORMAL_CHARACTER),	// ‹Cedilla› -> U+20A2
  DEADTRANS(0x086f, L'/'  , 0x20be, NORMAL_CHARACTER),	// ‹Long Solidus Overlay› -> U+20BE
  DEADTRANS(0x086f, 0x00af, 0x20bc, NORMAL_CHARACTER),	// ‹Macron› -> U+20BC
  DEADTRANS(0x086f, 0x02d8, 0x20a8, NORMAL_CHARACTER),	// ‹Breve› -> U+20A8
  DEADTRANS(0x086f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + %
  DEADTRANS(0x0870, L'%'  , 0x2030, NORMAL_CHARACTER),	// U+0025 -> U+2030
  DEADTRANS(0x0870, L'-'  , 0x2052, NORMAL_CHARACTER),	// U+002D -> U+2052
  DEADTRANS(0x0870, L'<'  , 0x0a0b, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + % + <›
  DEADTRANS(0x0870, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + &
  DEADTRANS(0x0871, L'!'  , 0x0a0c, CHAINED_DEAD_KEY),	// U+0021 -> ‹Compose + & + !›
  DEADTRANS(0x0871, L'&'  , 0x214b, NORMAL_CHARACTER),	// U+0026 -> U+214B
  DEADTRANS(0x0871, L'O'  , 0x0a4a, CHAINED_DEAD_KEY),	// ‘O’ -> ‹Compose + & + O›
  DEADTRANS(0x0871, L'a'  , 0x0a4f, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + & + a›
  DEADTRANS(0x0871, L'c'  , 0x0a52, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Compose + & + c›
  DEADTRANS(0x0871, L'd'  , 0x0a5d, CHAINED_DEAD_KEY),	// ‘d’ -> ‹Compose + & + d›
  DEADTRANS(0x0871, L'e'  , 0x0a60, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + & + e›
  DEADTRANS(0x0871, L'f'  , 0x0a7b, CHAINED_DEAD_KEY),	// ‘f’ -> ‹Compose + & + f›
  DEADTRANS(0x0871, L'i'  , 0x0a80, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + i›
  DEADTRANS(0x0871, L'n'  , 0x0a84, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + n›
  DEADTRANS(0x0871, L'o'  , 0x0ab1, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + & + o›
  DEADTRANS(0x0871, L'x'  , 0x0aba, CHAINED_DEAD_KEY),	// ‘x’ -> ‹Compose + & + x›
  DEADTRANS(0x0871, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + '
  DEADTRANS(0x0872, L'"'  , 0x2018, NORMAL_CHARACTER),	// U+0022 -> U+2018
  DEADTRANS(0x0872, L'\'' , 0x2019, NORMAL_CHARACTER),	// U+0027 -> U+2019
  DEADTRANS(0x0872, L','  , 0x201a, NORMAL_CHARACTER),	// U+002C -> U+201A
  DEADTRANS(0x0872, L'.'  , 0x0301, NORMAL_CHARACTER),	// U+002E -> U+0301
  DEADTRANS(0x0872, L'0'  , 0x00b0, NORMAL_CHARACTER),	// ‘0’ -> U+00B0
  DEADTRANS(0x0872, L'1'  , 0x2035, NORMAL_CHARACTER),	// ‘1’ -> U+2035
  DEADTRANS(0x0872, L'2'  , 0x2036, NORMAL_CHARACTER),	// ‘2’ -> U+2036
  DEADTRANS(0x0872, L'3'  , 0x2037, NORMAL_CHARACTER),	// ‘3’ -> U+2037
  DEADTRANS(0x0872, L'<'  , 0x2039, NORMAL_CHARACTER),	// U+003C -> U+2039
  DEADTRANS(0x0872, L'>'  , 0x203a, NORMAL_CHARACTER),	// U+003E -> U+203A
  DEADTRANS(0x0872, L'A'  , 0x00c1, NORMAL_CHARACTER),	// ‘A’ -> ‘Á’
  DEADTRANS(0x0872, L'C'  , 0x0106, NORMAL_CHARACTER),	// ‘C’ -> ‘Ć’
  DEADTRANS(0x0872, L'E'  , 0x00c9, NORMAL_CHARACTER),	// ‘E’ -> ‘É’
  DEADTRANS(0x0872, L'G'  , 0x01f4, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǵ’
  DEADTRANS(0x0872, L'I'  , 0x00cd, NORMAL_CHARACTER),	// ‘I’ -> ‘Í’
  DEADTRANS(0x0872, L'K'  , 0x1e30, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḱ’
  DEADTRANS(0x0872, L'L'  , 0x0139, NORMAL_CHARACTER),	// ‘L’ -> ‘Ĺ’
  DEADTRANS(0x0872, L'M'  , 0x1e3e, NORMAL_CHARACTER),	// ‘M’ -> ‘Ḿ’
  DEADTRANS(0x0872, L'N'  , 0x0143, NORMAL_CHARACTER),	// ‘N’ -> ‘Ń’
  DEADTRANS(0x0872, L'O'  , 0x00d3, NORMAL_CHARACTER),	// ‘O’ -> ‘Ó’
  DEADTRANS(0x0872, L'P'  , 0x1e54, NORMAL_CHARACTER),	// ‘P’ -> ‘Ṕ’
  DEADTRANS(0x0872, L'R'  , 0x0154, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŕ’
  DEADTRANS(0x0872, L'S'  , 0x015a, NORMAL_CHARACTER),	// ‘S’ -> ‘Ś’
  DEADTRANS(0x0872, L'U'  , 0x00da, NORMAL_CHARACTER),	// ‘U’ -> ‘Ú’
  DEADTRANS(0x0872, L'V'  , 0x01d7, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǘ’
  DEADTRANS(0x0872, L'W'  , 0x1e82, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẃ’
  DEADTRANS(0x0872, L'Y'  , 0x00dd, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ý’
  DEADTRANS(0x0872, L'Z'  , 0x0179, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ź’
  DEADTRANS(0x0872, L'['  , 0x2e22, NORMAL_CHARACTER),	// U+005B -> U+2E22
  DEADTRANS(0x0872, L']'  , 0x2e23, NORMAL_CHARACTER),	// U+005D -> U+2E23
  DEADTRANS(0x0872, L'_'  , 0x0ac6, CHAINED_DEAD_KEY),	// U+005F -> ‹Compose + ' + _›
  DEADTRANS(0x0872, L'a'  , 0x00e1, NORMAL_CHARACTER),	// ‘a’ -> ‘á’
  DEADTRANS(0x0872, L'c'  , 0x0107, NORMAL_CHARACTER),	// ‘c’ -> ‘ć’
  DEADTRANS(0x0872, L'e'  , 0x00e9, NORMAL_CHARACTER),	// ‘e’ -> ‘é’
  DEADTRANS(0x0872, L'g'  , 0x01f5, NORMAL_CHARACTER),	// ‘g’ -> ‘ǵ’
  DEADTRANS(0x0872, L'i'  , 0x00ed, NORMAL_CHARACTER),	// ‘i’ -> ‘í’
  DEADTRANS(0x0872, L'k'  , 0x1e31, NORMAL_CHARACTER),	// ‘k’ -> ‘ḱ’
  DEADTRANS(0x0872, L'l'  , 0x013a, NORMAL_CHARACTER),	// ‘l’ -> ‘ĺ’
  DEADTRANS(0x0872, L'm'  , 0x1e3f, NORMAL_CHARACTER),	// ‘m’ -> ‘ḿ’
  DEADTRANS(0x0872, L'n'  , 0x0144, NORMAL_CHARACTER),	// ‘n’ -> ‘ń’
  DEADTRANS(0x0872, L'o'  , 0x00f3, NORMAL_CHARACTER),	// ‘o’ -> ‘ó’
  DEADTRANS(0x0872, L'p'  , 0x1e55, NORMAL_CHARACTER),	// ‘p’ -> ‘ṕ’
  DEADTRANS(0x0872, L'r'  , 0x0155, NORMAL_CHARACTER),	// ‘r’ -> ‘ŕ’
  DEADTRANS(0x0872, L's'  , 0x015b, NORMAL_CHARACTER),	// ‘s’ -> ‘ś’
  DEADTRANS(0x0872, L'u'  , 0x00fa, NORMAL_CHARACTER),	// ‘u’ -> ‘ú’
  DEADTRANS(0x0872, L'v'  , 0x01d8, NORMAL_CHARACTER),	// ‘v’ -> ‘ǘ’
  DEADTRANS(0x0872, L'w'  , 0x1e83, NORMAL_CHARACTER),	// ‘w’ -> ‘ẃ’
  DEADTRANS(0x0872, L'y'  , 0x00fd, NORMAL_CHARACTER),	// ‘y’ -> ‘ý’
  DEADTRANS(0x0872, L'z'  , 0x017a, NORMAL_CHARACTER),	// ‘z’ -> ‘ź’
  DEADTRANS(0x0872, 0x00a0, 0x00b4, NORMAL_CHARACTER),	// U+00A0 -> U+00B4
  DEADTRANS(0x0872, 0x00dc, 0x01d7, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǘ’
  DEADTRANS(0x0872, 0x00fc, 0x01d8, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǘ’
  DEADTRANS(0x0872, 0x03a9, 0x038f, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ώ’
  DEADTRANS(0x0872, 0x03b1, 0x03ac, NORMAL_CHARACTER),	// ‘α’ -> ‘ά’
  DEADTRANS(0x0872, 0x03b5, 0x03ad, NORMAL_CHARACTER),	// ‘ε’ -> ‘έ’
  DEADTRANS(0x0872, 0x03b7, 0x03ae, NORMAL_CHARACTER),	// ‘η’ -> ‘ή’
  DEADTRANS(0x0872, 0x03b9, 0x03af, NORMAL_CHARACTER),	// ‘ι’ -> ‘ί’
  DEADTRANS(0x0872, 0x03bf, 0x03cc, NORMAL_CHARACTER),	// ‘ο’ -> ‘ό’
  DEADTRANS(0x0872, 0x03c5, 0x03cd, NORMAL_CHARACTER),	// ‘υ’ -> ‘ύ’
  DEADTRANS(0x0872, 0x03c9, 0x03ce, NORMAL_CHARACTER),	// ‘ω’ -> ‘ώ’
  DEADTRANS(0x0872, L' '  , 0x02ca, NORMAL_CHARACTER),	// U+0020 -> ‘ˊ’

// Dead key: Compose + (
  DEADTRANS(0x0873, L'('  , 0x2985, NORMAL_CHARACTER),	// U+0028 -> U+2985
  DEADTRANS(0x0873, L'.'  , 0x0311, NORMAL_CHARACTER),	// U+002E -> U+0311
  DEADTRANS(0x0873, L'2'  , 0x0aca, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + ( + 2›
  DEADTRANS(0x0873, L'<'  , 0x2329, NORMAL_CHARACTER),	// U+003C -> U+2329
  DEADTRANS(0x0873, L'>'  , 0x232a, NORMAL_CHARACTER),	// U+003E -> U+232A
  DEADTRANS(0x0873, L'A'  , 0x0202, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȃ’
  DEADTRANS(0x0873, L'E'  , 0x0206, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȇ’
  DEADTRANS(0x0873, L'I'  , 0x020a, NORMAL_CHARACTER),	// ‘I’ -> ‘Ȋ’
  DEADTRANS(0x0873, L'O'  , 0x020e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȏ’
  DEADTRANS(0x0873, L'R'  , 0x0212, NORMAL_CHARACTER),	// ‘R’ -> ‘Ȓ’
  DEADTRANS(0x0873, L'U'  , 0x0216, NORMAL_CHARACTER),	// ‘U’ -> ‘Ȗ’
  DEADTRANS(0x0873, L'^'  , 0x23dc, NORMAL_CHARACTER),	// U+005E -> U+23DC
  DEADTRANS(0x0873, L'a'  , 0x0203, NORMAL_CHARACTER),	// ‘a’ -> ‘ȃ’
  DEADTRANS(0x0873, L'e'  , 0x0207, NORMAL_CHARACTER),	// ‘e’ -> ‘ȇ’
  DEADTRANS(0x0873, L'i'  , 0x020b, NORMAL_CHARACTER),	// ‘i’ -> ‘ȋ’
  DEADTRANS(0x0873, L'o'  , 0x020f, NORMAL_CHARACTER),	// ‘o’ -> ‘ȏ’
  DEADTRANS(0x0873, L'r'  , 0x0213, NORMAL_CHARACTER),	// ‘r’ -> ‘ȓ’
  DEADTRANS(0x0873, L's'  , 0x27c6, NORMAL_CHARACTER),	// ‘s’ -> U+27C6
  DEADTRANS(0x0873, L'u'  , 0x0217, NORMAL_CHARACTER),	// ‘u’ -> ‘ȗ’
  DEADTRANS(0x0873, L'v'  , 0x0ace, CHAINED_DEAD_KEY),	// ‘v’ -> ‹Compose + ( + v›
  DEADTRANS(0x0873, L'w'  , 0x29d8, NORMAL_CHARACTER),	// ‘w’ -> U+29D8
  DEADTRANS(0x0873, L'|'  , 0x2987, NORMAL_CHARACTER),	// U+007C -> U+2987
  DEADTRANS(0x0873, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + )
  DEADTRANS(0x0874, L'('  , 0x226c, NORMAL_CHARACTER),	// U+0028 -> U+226C
  DEADTRANS(0x0874, L')'  , 0x2986, NORMAL_CHARACTER),	// U+0029 -> U+2986
  DEADTRANS(0x0874, L'.'  , 0x0306, NORMAL_CHARACTER),	// U+002E -> U+0306
  DEADTRANS(0x0874, L'2'  , 0x0ad1, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + ) + 2›
  DEADTRANS(0x0874, L'<'  , 0x232a, NORMAL_CHARACTER),	// U+003C -> U+232A
  DEADTRANS(0x0874, L'A'  , 0x0102, NORMAL_CHARACTER),	// ‘A’ -> ‘Ă’
  DEADTRANS(0x0874, L'E'  , 0x0114, NORMAL_CHARACTER),	// ‘E’ -> ‘Ĕ’
  DEADTRANS(0x0874, L'G'  , 0x011e, NORMAL_CHARACTER),	// ‘G’ -> ‘Ğ’
  DEADTRANS(0x0874, L'I'  , 0x012c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĭ’
  DEADTRANS(0x0874, L'O'  , 0x014e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ŏ’
  DEADTRANS(0x0874, L'U'  , 0x016c, NORMAL_CHARACTER),	// ‘U’ -> ‘Ŭ’
  DEADTRANS(0x0874, L'^'  , 0x23dd, NORMAL_CHARACTER),	// U+005E -> U+23DD
  DEADTRANS(0x0874, L'a'  , 0x0103, NORMAL_CHARACTER),	// ‘a’ -> ‘ă’
  DEADTRANS(0x0874, L'e'  , 0x0115, NORMAL_CHARACTER),	// ‘e’ -> ‘ĕ’
  DEADTRANS(0x0874, L'g'  , 0x011f, NORMAL_CHARACTER),	// ‘g’ -> ‘ğ’
  DEADTRANS(0x0874, L'i'  , 0x012d, NORMAL_CHARACTER),	// ‘i’ -> ‘ĭ’
  DEADTRANS(0x0874, L'o'  , 0x014f, NORMAL_CHARACTER),	// ‘o’ -> ‘ŏ’
  DEADTRANS(0x0874, L's'  , 0x27c5, NORMAL_CHARACTER),	// ‘s’ -> U+27C5
  DEADTRANS(0x0874, L'u'  , 0x016d, NORMAL_CHARACTER),	// ‘u’ -> ‘ŭ’
  DEADTRANS(0x0874, L'v'  , 0x0ad2, CHAINED_DEAD_KEY),	// ‘v’ -> ‹Compose + ) + v›
  DEADTRANS(0x0874, L'w'  , 0x29d9, NORMAL_CHARACTER),	// ‘w’ -> U+29D9
  DEADTRANS(0x0874, L'|'  , 0x2988, NORMAL_CHARACTER),	// U+007C -> U+2988
  DEADTRANS(0x0874, 0x03b1, 0x1fb0, NORMAL_CHARACTER),	// ‘α’ -> ‘ᾰ’
  DEADTRANS(0x0874, 0x03b9, 0x1fd0, NORMAL_CHARACTER),	// ‘ι’ -> ‘ῐ’
  DEADTRANS(0x0874, 0x03c5, 0x1fe0, NORMAL_CHARACTER),	// ‘υ’ -> ‘ῠ’
  DEADTRANS(0x0874, L' '  , 0x02d8, NORMAL_CHARACTER),	// U+0020 -> U+02D8

// Dead key: Compose + *
  DEADTRANS(0x0875, L'#'  , 0x25aa, NORMAL_CHARACTER),	// U+0023 -> U+25AA
  DEADTRANS(0x0875, L'%'  , 0x0ad4, CHAINED_DEAD_KEY),	// U+0025 -> ‹Compose + * + %›
  DEADTRANS(0x0875, L'*'  , 0x2051, NORMAL_CHARACTER),	// U+002A -> U+2051
  DEADTRANS(0x0875, L'-'  , 0x2043, NORMAL_CHARACTER),	// U+002D -> U+2043
  DEADTRANS(0x0875, L'.'  , 0x2022, NORMAL_CHARACTER),	// U+002E -> U+2022
  DEADTRANS(0x0875, L'>'  , 0x2023, NORMAL_CHARACTER),	// U+003E -> U+2023
  DEADTRANS(0x0875, L'@'  , 0x25c9, NORMAL_CHARACTER),	// U+0040 -> U+25C9
  DEADTRANS(0x0875, L']'  , 0x25aa, NORMAL_CHARACTER),	// U+005D -> U+25AA
  DEADTRANS(0x0875, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + +
  DEADTRANS(0x0876, L'-'  , 0x00b1, NORMAL_CHARACTER),	// U+002D -> U+00B1
  DEADTRANS(0x0876, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ,
  DEADTRANS(0x0877, L','  , 0x201e, NORMAL_CHARACTER),	// U+002C -> U+201E
  DEADTRANS(0x0877, L'.'  , 0x0327, NORMAL_CHARACTER),	// U+002E -> U+0327
  DEADTRANS(0x0877, L'C'  , 0x00c7, NORMAL_CHARACTER),	// ‘C’ -> ‘Ç’
  DEADTRANS(0x0877, L'D'  , 0x1e10, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḑ’
  DEADTRANS(0x0877, L'E'  , 0x0228, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȩ’
  DEADTRANS(0x0877, L'G'  , 0x0122, NORMAL_CHARACTER),	// ‘G’ -> ‘Ģ’
  DEADTRANS(0x0877, L'H'  , 0x1e28, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḩ’
  DEADTRANS(0x0877, L'K'  , 0x0136, NORMAL_CHARACTER),	// ‘K’ -> ‘Ķ’
  DEADTRANS(0x0877, L'L'  , 0x013b, NORMAL_CHARACTER),	// ‘L’ -> ‘Ļ’
  DEADTRANS(0x0877, L'N'  , 0x0145, NORMAL_CHARACTER),	// ‘N’ -> ‘Ņ’
  DEADTRANS(0x0877, L'R'  , 0x0156, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŗ’
  DEADTRANS(0x0877, L'S'  , 0x015e, NORMAL_CHARACTER),	// ‘S’ -> ‘Ş’
  DEADTRANS(0x0877, L'T'  , 0x0162, NORMAL_CHARACTER),	// ‘T’ -> ‘Ţ’
  DEADTRANS(0x0877, L'['  , 0x2e24, NORMAL_CHARACTER),	// U+005B -> U+2E24
  DEADTRANS(0x0877, L']'  , 0x2e25, NORMAL_CHARACTER),	// U+005D -> U+2E25
  DEADTRANS(0x0877, L'c'  , 0x00e7, NORMAL_CHARACTER),	// ‘c’ -> ‘ç’
  DEADTRANS(0x0877, L'd'  , 0x1e11, NORMAL_CHARACTER),	// ‘d’ -> ‘ḑ’
  DEADTRANS(0x0877, L'e'  , 0x0229, NORMAL_CHARACTER),	// ‘e’ -> ‘ȩ’
  DEADTRANS(0x0877, L'g'  , 0x0123, NORMAL_CHARACTER),	// ‘g’ -> ‘ģ’
  DEADTRANS(0x0877, L'h'  , 0x1e29, NORMAL_CHARACTER),	// ‘h’ -> ‘ḩ’
  DEADTRANS(0x0877, L'k'  , 0x0137, NORMAL_CHARACTER),	// ‘k’ -> ‘ķ’
  DEADTRANS(0x0877, L'l'  , 0x013c, NORMAL_CHARACTER),	// ‘l’ -> ‘ļ’
  DEADTRANS(0x0877, L'n'  , 0x0146, NORMAL_CHARACTER),	// ‘n’ -> ‘ņ’
  DEADTRANS(0x0877, L'r'  , 0x0157, NORMAL_CHARACTER),	// ‘r’ -> ‘ŗ’
  DEADTRANS(0x0877, L's'  , 0x015f, NORMAL_CHARACTER),	// ‘s’ -> ‘ş’
  DEADTRANS(0x0877, L't'  , 0x0163, NORMAL_CHARACTER),	// ‘t’ -> ‘ţ’
  DEADTRANS(0x0877, 0x00a0, 0x00b8, NORMAL_CHARACTER),	// U+00A0 -> U+00B8
  DEADTRANS(0x0877, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + -
  DEADTRANS(0x0878, L'#'  , 0x2012, NORMAL_CHARACTER),	// U+0023 -> U+2012
  DEADTRANS(0x0878, L'$'  , 0x2052, NORMAL_CHARACTER),	// U+0024 -> U+2052
  DEADTRANS(0x0878, L'%'  , 0x2212, NORMAL_CHARACTER),	// U+0025 -> U+2212
  DEADTRANS(0x0878, L'*'  , 0x0ad5, CHAINED_DEAD_KEY),	// U+002A -> ‹Compose + - + *›
  DEADTRANS(0x0878, L'+'  , 0x2213, NORMAL_CHARACTER),	// U+002B -> U+2213
  DEADTRANS(0x0878, L','  , 0x00ac, NORMAL_CHARACTER),	// U+002C -> U+00AC
  DEADTRANS(0x0878, L'-'  , 0x2013, NORMAL_CHARACTER),	// U+002D -> U+2013
  DEADTRANS(0x0878, L'0'  , 0x00ad, NORMAL_CHARACTER),	// ‘0’ -> U+00AD
  DEADTRANS(0x0878, L'1'  , 0x2010, NORMAL_CHARACTER),	// ‘1’ -> U+2010
  DEADTRANS(0x0878, L'2'  , 0x2e3a, NORMAL_CHARACTER),	// ‘2’ -> U+2E3A
  DEADTRANS(0x0878, L'3'  , 0x2e3b, NORMAL_CHARACTER),	// ‘3’ -> U+2E3B
  DEADTRANS(0x0878, L':'  , 0x00f7, NORMAL_CHARACTER),	// U+003A -> U+00F7
  DEADTRANS(0x0878, L'<'  , 0x2190, NORMAL_CHARACTER),	// U+003C -> U+2190
  DEADTRANS(0x0878, L'>'  , 0x2192, NORMAL_CHARACTER),	// U+003E -> U+2192
  DEADTRANS(0x0878, L'^'  , 0x2191, NORMAL_CHARACTER),	// U+005E -> U+2191
  DEADTRANS(0x0878, L'_'  , 0x2193, NORMAL_CHARACTER),	// U+005F -> U+2193
  DEADTRANS(0x0878, L'b'  , 0x2015, NORMAL_CHARACTER),	// ‘b’ -> U+2015
  DEADTRANS(0x0878, L'l'  , 0x2500, NORMAL_CHARACTER),	// ‘l’ -> U+2500
  DEADTRANS(0x0878, L'm'  , 0x2014, NORMAL_CHARACTER),	// ‘m’ -> U+2014
  DEADTRANS(0x0878, L'n'  , 0x2013, NORMAL_CHARACTER),	// ‘n’ -> U+2013
  DEADTRANS(0x0878, L'|'  , 0x22a3, NORMAL_CHARACTER),	// U+007C -> U+22A3
  DEADTRANS(0x0878, L'~'  , 0x2011, NORMAL_CHARACTER),	// U+007E -> U+2011
  DEADTRANS(0x0878, L' '  , 0x00ad, NORMAL_CHARACTER),	// U+0020 -> U+00AD

// Dead key: Compose + .
  DEADTRANS(0x0879, L'#'  , 0x220e, NORMAL_CHARACTER),	// U+0023 -> U+220E
  DEADTRANS(0x0879, L'*'  , 0x2219, NORMAL_CHARACTER),	// U+002A -> U+2219
  DEADTRANS(0x0879, L','  , 0x00b7, NORMAL_CHARACTER),	// U+002C -> U+00B7
  DEADTRANS(0x0879, L'-'  , 0x2027, NORMAL_CHARACTER),	// U+002D -> U+2027
  DEADTRANS(0x0879, L'3'  , 0x0ad8, CHAINED_DEAD_KEY),	// ‘3’ -> ‹Compose + . + 3›
  DEADTRANS(0x0879, L'<'  , 0x21e0, NORMAL_CHARACTER),	// U+003C -> U+21E0
  DEADTRANS(0x0879, L'>'  , 0x21e2, NORMAL_CHARACTER),	// U+003E -> U+21E2
  DEADTRANS(0x0879, L'^'  , 0x21e1, NORMAL_CHARACTER),	// U+005E -> U+21E1
  DEADTRANS(0x0879, L'_'  , 0x21e3, NORMAL_CHARACTER),	// U+005F -> U+21E3
  DEADTRANS(0x0879, L'o'  , 0x2e30, NORMAL_CHARACTER),	// ‘o’ -> U+2E30
  DEADTRANS(0x0879, L'x'  , 0x22c5, NORMAL_CHARACTER),	// ‘x’ -> U+22C5
  DEADTRANS(0x0879, L'|'  , 0x205e, NORMAL_CHARACTER),	// U+007C -> U+205E
  DEADTRANS(0x0879, L'.'  , 0x02d9, CHAINED_DEAD_KEY),	// ‹Dot Below› -> ‹Dot Above›
  DEADTRANS(0x0879, 0x2025, 0x20e8, CHAINED_DEAD_KEY),	// ‹Diaeresis Below› -> ‹Triple Underdot›
  DEADTRANS(0x0879, L' '  , 0x2e31, NORMAL_CHARACTER),	// U+0020 -> U+2E31

// Dead key: Compose + /
  DEADTRANS(0x087a, L'.'  , 0x0338, NORMAL_CHARACTER),	// U+002E -> U+0338
  DEADTRANS(0x087a, L'/'  , 0x2afd, NORMAL_CHARACTER),	// U+002F -> U+2AFD
  DEADTRANS(0x087a, L'0'  , 0x2205, NORMAL_CHARACTER),	// ‘0’ -> U+2205
  DEADTRANS(0x087a, L'2'  , 0x0ad9, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + / + 2›
  DEADTRANS(0x087a, L':'  , 0x2044, NORMAL_CHARACTER),	// U+003A -> U+2044
  DEADTRANS(0x087a, L'<'  , 0x226e, NORMAL_CHARACTER),	// U+003C -> U+226E
  DEADTRANS(0x087a, L'='  , 0x2260, NORMAL_CHARACTER),	// U+003D -> U+2260
  DEADTRANS(0x087a, L'>'  , 0x226f, NORMAL_CHARACTER),	// U+003E -> U+226F
  DEADTRANS(0x087a, L'A'  , 0x023a, NORMAL_CHARACTER),	// ‘A’ -> ‘Ⱥ’
  DEADTRANS(0x087a, L'C'  , 0x023b, NORMAL_CHARACTER),	// ‘C’ -> ‘Ȼ’
  DEADTRANS(0x087a, L'E'  , 0x0246, NORMAL_CHARACTER),	// ‘E’ -> ‘Ɇ’
  DEADTRANS(0x087a, L'G'  , 0xa7a0, NORMAL_CHARACTER),	// ‘G’ -> ‘Ꞡ’
  DEADTRANS(0x087a, L'K'  , 0xa7a2, NORMAL_CHARACTER),	// ‘K’ -> ‘Ꞣ’
  DEADTRANS(0x087a, L'L'  , 0x0141, NORMAL_CHARACTER),	// ‘L’ -> ‘Ł’
  DEADTRANS(0x087a, L'N'  , 0xa7a4, NORMAL_CHARACTER),	// ‘N’ -> ‘Ꞥ’
  DEADTRANS(0x087a, L'O'  , 0x00d8, NORMAL_CHARACTER),	// ‘O’ -> ‘Ø’
  DEADTRANS(0x087a, L'Q'  , 0xa758, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ꝙ’
  DEADTRANS(0x087a, L'R'  , 0xa7a6, NORMAL_CHARACTER),	// ‘R’ -> ‘Ꞧ’
  DEADTRANS(0x087a, L'S'  , 0xa7a8, NORMAL_CHARACTER),	// ‘S’ -> ‘Ꞩ’
  DEADTRANS(0x087a, L'T'  , 0x023e, NORMAL_CHARACTER),	// ‘T’ -> ‘Ⱦ’
  DEADTRANS(0x087a, L'V'  , 0xa75e, NORMAL_CHARACTER),	// ‘V’ -> ‘Ꝟ’
  DEADTRANS(0x087a, L'\\' , 0x2227, NORMAL_CHARACTER),	// U+005C -> U+2227
  DEADTRANS(0x087a, L'a'  , 0x2c65, NORMAL_CHARACTER),	// ‘a’ -> ‘ⱥ’
  DEADTRANS(0x087a, L'b'  , 0x2422, NORMAL_CHARACTER),	// ‘b’ -> U+2422
  DEADTRANS(0x087a, L'c'  , 0x023c, NORMAL_CHARACTER),	// ‘c’ -> ‘ȼ’
  DEADTRANS(0x087a, L'e'  , 0x0247, NORMAL_CHARACTER),	// ‘e’ -> ‘ɇ’
  DEADTRANS(0x087a, L'f'  , 0x1e9c, NORMAL_CHARACTER),	// ‘f’ -> ‘ẜ’
  DEADTRANS(0x087a, L'g'  , 0xa7a1, NORMAL_CHARACTER),	// ‘g’ -> ‘ꞡ’
  DEADTRANS(0x087a, L'k'  , 0xa7a3, NORMAL_CHARACTER),	// ‘k’ -> ‘ꞣ’
  DEADTRANS(0x087a, L'l'  , 0x0142, NORMAL_CHARACTER),	// ‘l’ -> ‘ł’
  DEADTRANS(0x087a, L'n'  , 0xa7a5, NORMAL_CHARACTER),	// ‘n’ -> ‘ꞥ’
  DEADTRANS(0x087a, L'o'  , 0x00f8, NORMAL_CHARACTER),	// ‘o’ -> ‘ø’
  DEADTRANS(0x087a, L'p'  , 0x1d7d, NORMAL_CHARACTER),	// ‘p’ -> ‘ᵽ’
  DEADTRANS(0x087a, L'q'  , 0xa759, NORMAL_CHARACTER),	// ‘q’ -> ‘ꝙ’
  DEADTRANS(0x087a, L'r'  , 0xa7a7, NORMAL_CHARACTER),	// ‘r’ -> ‘ꞧ’
  DEADTRANS(0x087a, L's'  , 0xa7a9, NORMAL_CHARACTER),	// ‘s’ -> ‘ꞩ’
  DEADTRANS(0x087a, L't'  , 0x2c66, NORMAL_CHARACTER),	// ‘t’ -> ‘ⱦ’
  DEADTRANS(0x087a, L'u'  , 0xa7b8, NORMAL_CHARACTER),	// ‘u’ -> ‘Ꞹ’
  DEADTRANS(0x087a, L'v'  , 0xa75f, NORMAL_CHARACTER),	// ‘v’ -> ‘ꝟ’
  DEADTRANS(0x087a, L'|'  , 0x2224, NORMAL_CHARACTER),	// U+007C -> U+2224
  DEADTRANS(0x087a, 0x00a0, L'/'  , NORMAL_CHARACTER),	// U+00A0 -> U+002F
  DEADTRANS(0x087a, 0x017f, 0x1e9c, NORMAL_CHARACTER),	// ‘ſ’ -> ‘ẜ’
  DEADTRANS(0x087a, 0x2192, 0x219b, NORMAL_CHARACTER),	// U+2192 -> U+219B
  DEADTRANS(0x087a, 0x21d0, 0x21cd, NORMAL_CHARACTER),	// U+21D0 -> U+21CD
  DEADTRANS(0x087a, 0x21d2, 0x21cf, NORMAL_CHARACTER),	// U+21D2 -> U+21CF
  DEADTRANS(0x087a, 0x21d4, 0x21ce, NORMAL_CHARACTER),	// U+21D4 -> U+21CE
  DEADTRANS(0x087a, 0x2203, 0x2204, NORMAL_CHARACTER),	// U+2203 -> U+2204
  DEADTRANS(0x087a, 0x2208, 0x2209, NORMAL_CHARACTER),	// U+2208 -> U+2209
  DEADTRANS(0x087a, 0x2225, 0x2226, NORMAL_CHARACTER),	// U+2225 -> U+2226
  DEADTRANS(0x087a, 0x2282, 0x2284, NORMAL_CHARACTER),	// U+2282 -> U+2284
  DEADTRANS(0x087a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 0
  DEADTRANS(0x087b, L'\'' , 0x00b0, NORMAL_CHARACTER),	// U+0027 -> U+00B0
  DEADTRANS(0x087b, L'-'  , 0x00ad, NORMAL_CHARACTER),	// U+002D -> U+00AD
  DEADTRANS(0x087b, L'.'  , 0x030a, NORMAL_CHARACTER),	// U+002E -> U+030A
  DEADTRANS(0x087b, L'/'  , 0x0ada, CHAINED_DEAD_KEY),	// U+002F -> ‹Compose + 0 + /›
  DEADTRANS(0x087b, L'0'  , 0x00b0, NORMAL_CHARACTER),	// ‘0’ -> U+00B0
  DEADTRANS(0x087b, L'3'  , 0x2189, NORMAL_CHARACTER),	// ‘3’ -> ‘↉’
  DEADTRANS(0x087b, L'A'  , 0x00c5, NORMAL_CHARACTER),	// ‘A’ -> ‘Å’
  DEADTRANS(0x087b, L'U'  , 0x016e, NORMAL_CHARACTER),	// ‘U’ -> ‘Ů’
  DEADTRANS(0x087b, L'['  , 0x2308, NORMAL_CHARACTER),	// U+005B -> U+2308
  DEADTRANS(0x087b, L']'  , 0x2309, NORMAL_CHARACTER),	// U+005D -> U+2309
  DEADTRANS(0x087b, L'a'  , 0x00e5, NORMAL_CHARACTER),	// ‘a’ -> ‘å’
  DEADTRANS(0x087b, L'c'  , 0x2103, NORMAL_CHARACTER),	// ‘c’ -> U+2103
  DEADTRANS(0x087b, L'f'  , 0x2109, NORMAL_CHARACTER),	// ‘f’ -> U+2109
  DEADTRANS(0x087b, L'u'  , 0x016f, NORMAL_CHARACTER),	// ‘u’ -> ‘ů’
  DEADTRANS(0x087b, L'w'  , 0x1e98, NORMAL_CHARACTER),	// ‘w’ -> ‘ẘ’
  DEADTRANS(0x087b, L'y'  , 0x1e99, NORMAL_CHARACTER),	// ‘y’ -> ‘ẙ’
  DEADTRANS(0x087b, 0x00a0, 0x00b0, NORMAL_CHARACTER),	// U+00A0 -> U+00B0
  DEADTRANS(0x087b, L' '  , 0x02da, NORMAL_CHARACTER),	// U+0020 -> U+02DA

// Dead key: Compose + 1
  DEADTRANS(0x087c, L'\'' , 0x2032, NORMAL_CHARACTER),	// U+0027 -> U+2032
  DEADTRANS(0x087c, L'*'  , 0x2217, NORMAL_CHARACTER),	// U+002A -> U+2217
  DEADTRANS(0x087c, L'-'  , 0x2010, NORMAL_CHARACTER),	// U+002D -> U+2010
  DEADTRANS(0x087c, L'.'  , 0x2024, NORMAL_CHARACTER),	// U+002E -> U+2024
  DEADTRANS(0x087c, L'/'  , 0x2215, NORMAL_CHARACTER),	// U+002F -> U+2215
  DEADTRANS(0x087c, L'0'  , 0x23e8, NORMAL_CHARACTER),	// ‘0’ -> U+23E8
  DEADTRANS(0x087c, L'1'  , 0x0adb, CHAINED_DEAD_KEY),	// ‘1’ -> ‹Compose + 1 + 1›
  DEADTRANS(0x087c, L'2'  , 0x00bd, NORMAL_CHARACTER),	// ‘2’ -> ‘½’
  DEADTRANS(0x087c, L'3'  , 0x2153, NORMAL_CHARACTER),	// ‘3’ -> ‘⅓’
  DEADTRANS(0x087c, L'4'  , 0x00bc, NORMAL_CHARACTER),	// ‘4’ -> ‘¼’
  DEADTRANS(0x087c, L'5'  , 0x2155, NORMAL_CHARACTER),	// ‘5’ -> ‘⅕’
  DEADTRANS(0x087c, L'6'  , 0x2159, NORMAL_CHARACTER),	// ‘6’ -> ‘⅙’
  DEADTRANS(0x087c, L'7'  , 0x2150, NORMAL_CHARACTER),	// ‘7’ -> ‘⅐’
  DEADTRANS(0x087c, L'8'  , 0x215b, NORMAL_CHARACTER),	// ‘8’ -> ‘⅛’
  DEADTRANS(0x087c, L'9'  , 0x2151, NORMAL_CHARACTER),	// ‘9’ -> ‘⅑’
  DEADTRANS(0x087c, L'@'  , 0x0adc, CHAINED_DEAD_KEY),	// U+0040 -> ‹Compose + 1 + @›
  DEADTRANS(0x087c, L'e'  , 0x23e8, NORMAL_CHARACTER),	// ‘e’ -> U+23E8
  DEADTRANS(0x087c, L's'  , 0x222b, NORMAL_CHARACTER),	// ‘s’ -> U+222B
  DEADTRANS(0x087c, L'x'  , 0x22c6, NORMAL_CHARACTER),	// ‘x’ -> U+22C6
  DEADTRANS(0x087c, L'|'  , 0x2223, NORMAL_CHARACTER),	// U+007C -> U+2223
  DEADTRANS(0x087c, L' '  , 0x215f, NORMAL_CHARACTER),	// U+0020 -> ‘⅟’

// Dead key: Compose + 2
  DEADTRANS(0x087d, L'!'  , 0x203c, NORMAL_CHARACTER),	// U+0021 -> U+203C
  DEADTRANS(0x087d, L'%'  , 0x2030, NORMAL_CHARACTER),	// U+0025 -> U+2030
  DEADTRANS(0x087d, L'\'' , 0x2033, NORMAL_CHARACTER),	// U+0027 -> U+2033
  DEADTRANS(0x087d, L'('  , 0x2e28, NORMAL_CHARACTER),	// U+0028 -> U+2E28
  DEADTRANS(0x087d, L')'  , 0x2e29, NORMAL_CHARACTER),	// U+0029 -> U+2E29
  DEADTRANS(0x087d, L'*'  , 0x2051, NORMAL_CHARACTER),	// U+002A -> U+2051
  DEADTRANS(0x087d, L'-'  , 0x2e3a, NORMAL_CHARACTER),	// U+002D -> U+2E3A
  DEADTRANS(0x087d, L'.'  , 0x2025, NORMAL_CHARACTER),	// U+002E -> U+2025
  DEADTRANS(0x087d, L'/'  , 0x2afd, NORMAL_CHARACTER),	// U+002F -> U+2AFD
  DEADTRANS(0x087d, L'3'  , 0x2154, NORMAL_CHARACTER),	// ‘3’ -> ‘⅔’
  DEADTRANS(0x087d, L'4'  , 0xa75d, NORMAL_CHARACTER),	// ‘4’ -> ‘ꝝ’
  DEADTRANS(0x087d, L'5'  , 0x2156, NORMAL_CHARACTER),	// ‘5’ -> ‘⅖’
  DEADTRANS(0x087d, L':'  , 0x2237, NORMAL_CHARACTER),	// U+003A -> U+2237
  DEADTRANS(0x087d, L'<'  , 0x226a, NORMAL_CHARACTER),	// U+003C -> U+226A
  DEADTRANS(0x087d, L'>'  , 0x226b, NORMAL_CHARACTER),	// U+003E -> U+226B
  DEADTRANS(0x087d, L'?'  , 0x2047, NORMAL_CHARACTER),	// U+003F -> U+2047
  DEADTRANS(0x087d, L'@'  , 0x0add, CHAINED_DEAD_KEY),	// U+0040 -> ‹Compose + 2 + @›
  DEADTRANS(0x087d, L'`'  , 0x0ade, CHAINED_DEAD_KEY),	// U+0060 -> ‹Compose + 2 + `›
  DEADTRANS(0x087d, L'c'  , 0x0adf, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Compose + 2 + c›
  DEADTRANS(0x087d, L'd'  , 0x0ae4, CHAINED_DEAD_KEY),	// ‘d’ -> ‹Compose + 2 + d›
  DEADTRANS(0x087d, L'k'  , 0x0ae5, CHAINED_DEAD_KEY),	// ‘k’ -> ‹Compose + 2 + k›
  DEADTRANS(0x087d, L'm'  , 0x0af2, CHAINED_DEAD_KEY),	// ‘m’ -> ‹Compose + 2 + m›
  DEADTRANS(0x087d, L's'  , 0x222c, NORMAL_CHARACTER),	// ‘s’ -> U+222C
  DEADTRANS(0x087d, L'v'  , 0x221a, NORMAL_CHARACTER),	// ‘v’ -> U+221A
  DEADTRANS(0x087d, L'|'  , 0x2016, NORMAL_CHARACTER),	// U+007C -> U+2016
  DEADTRANS(0x087d, L'~'  , 0x2248, NORMAL_CHARACTER),	// U+007E -> U+2248
  DEADTRANS(0x087d, 0x2018, 0x201c, NORMAL_CHARACTER),	// U+2018 -> U+201C
  DEADTRANS(0x087d, 0x2019, 0x201d, NORMAL_CHARACTER),	// U+2019 -> U+201D
  DEADTRANS(0x087d, 0x222b, 0x222c, NORMAL_CHARACTER),	// U+222B -> U+222C
  DEADTRANS(0x087d, 0x27e8, 0x27ea, NORMAL_CHARACTER),	// U+27E8 -> U+27EA
  DEADTRANS(0x087d, 0x27e9, 0x27eb, NORMAL_CHARACTER),	// U+27E9 -> U+27EB
  DEADTRANS(0x087d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 3
  DEADTRANS(0x087e, L'!'  , 0x2762, NORMAL_CHARACTER),	// U+0021 -> U+2762
  DEADTRANS(0x087e, L'%'  , 0x2031, NORMAL_CHARACTER),	// U+0025 -> U+2031
  DEADTRANS(0x087e, L'\'' , 0x2034, NORMAL_CHARACTER),	// U+0027 -> U+2034
  DEADTRANS(0x087e, L'*'  , 0x2042, NORMAL_CHARACTER),	// U+002A -> U+2042
  DEADTRANS(0x087e, L'-'  , 0x2e3b, NORMAL_CHARACTER),	// U+002D -> U+2E3B
  DEADTRANS(0x087e, L'.'  , 0x2026, NORMAL_CHARACTER),	// U+002E -> U+2026
  DEADTRANS(0x087e, L'/'  , 0x2afb, NORMAL_CHARACTER),	// U+002F -> U+2AFB
  DEADTRANS(0x087e, L'4'  , 0x00be, NORMAL_CHARACTER),	// ‘4’ -> ‘¾’
  DEADTRANS(0x087e, L'5'  , 0x2157, NORMAL_CHARACTER),	// ‘5’ -> ‘⅗’
  DEADTRANS(0x087e, L'8'  , 0x215c, NORMAL_CHARACTER),	// ‘8’ -> ‘⅜’
  DEADTRANS(0x087e, L'<'  , 0x22d8, NORMAL_CHARACTER),	// U+003C -> U+22D8
  DEADTRANS(0x087e, L'>'  , 0x22d9, NORMAL_CHARACTER),	// U+003E -> U+22D9
  DEADTRANS(0x087e, L'@'  , 0x0af3, CHAINED_DEAD_KEY),	// U+0040 -> ‹Compose + 3 + @›
  DEADTRANS(0x087e, L'c'  , 0x0af4, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Compose + 3 + c›
  DEADTRANS(0x087e, L'd'  , 0x0af5, CHAINED_DEAD_KEY),	// ‘d’ -> ‹Compose + 3 + d›
  DEADTRANS(0x087e, L'k'  , 0x0af6, CHAINED_DEAD_KEY),	// ‘k’ -> ‹Compose + 3 + k›
  DEADTRANS(0x087e, L'm'  , 0x0af7, CHAINED_DEAD_KEY),	// ‘m’ -> ‹Compose + 3 + m›
  DEADTRANS(0x087e, L's'  , 0x222d, NORMAL_CHARACTER),	// ‘s’ -> U+222D
  DEADTRANS(0x087e, L'v'  , 0x221b, NORMAL_CHARACTER),	// ‘v’ -> U+221B
  DEADTRANS(0x087e, L'|'  , 0x2980, NORMAL_CHARACTER),	// U+007C -> U+2980
  DEADTRANS(0x087e, L'~'  , 0x224b, NORMAL_CHARACTER),	// U+007E -> U+224B
  DEADTRANS(0x087e, 0x222b, 0x222d, NORMAL_CHARACTER),	// U+222B -> U+222D
  DEADTRANS(0x087e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 4
  DEADTRANS(0x087f, L'\'' , 0x2057, NORMAL_CHARACTER),	// U+0027 -> U+2057
  DEADTRANS(0x087f, L'5'  , 0x2158, NORMAL_CHARACTER),	// ‘5’ -> ‘⅘’
  DEADTRANS(0x087f, L's'  , 0x2a0c, NORMAL_CHARACTER),	// ‘s’ -> U+2A0C
  DEADTRANS(0x087f, L'v'  , 0x221c, NORMAL_CHARACTER),	// ‘v’ -> U+221C
  DEADTRANS(0x087f, 0x222b, 0x2a0c, NORMAL_CHARACTER),	// U+222B -> U+2A0C
  DEADTRANS(0x087f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 5
  DEADTRANS(0x0880, L'6'  , 0x215a, NORMAL_CHARACTER),	// ‘6’ -> ‘⅚’
  DEADTRANS(0x0880, L'8'  , 0x215d, NORMAL_CHARACTER),	// ‘8’ -> ‘⅝’
  DEADTRANS(0x0880, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 7
  DEADTRANS(0x0881, L'7'  , 0x204a, NORMAL_CHARACTER),	// ‘7’ -> U+204A
  DEADTRANS(0x0881, L'8'  , 0x215e, NORMAL_CHARACTER),	// ‘8’ -> ‘⅞’
  DEADTRANS(0x0881, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 8
  DEADTRANS(0x0882, L'#'  , 0x2318, NORMAL_CHARACTER),	// U+0023 -> U+2318
  DEADTRANS(0x0882, L'8'  , 0x221e, NORMAL_CHARACTER),	// ‘8’ -> U+221E
  DEADTRANS(0x0882, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 9
  DEADTRANS(0x0883, L'9'  , 0xa76f, NORMAL_CHARACTER),	// ‘9’ -> ‘ꝯ’
  DEADTRANS(0x0883, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + :
  DEADTRANS(0x0884, L'-'  , 0x00f7, NORMAL_CHARACTER),	// U+002D -> U+00F7
  DEADTRANS(0x0884, L'.'  , 0x0308, NORMAL_CHARACTER),	// U+002E -> U+0308
  DEADTRANS(0x0884, L'/'  , 0x2236, NORMAL_CHARACTER),	// U+002F -> U+2236
  DEADTRANS(0x0884, L':'  , 0x2237, NORMAL_CHARACTER),	// U+003A -> U+2237
  DEADTRANS(0x0884, L'='  , 0x2254, NORMAL_CHARACTER),	// U+003D -> U+2254
  DEADTRANS(0x0884, L'A'  , 0x00c4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ä’
  DEADTRANS(0x0884, L'E'  , 0x00cb, NORMAL_CHARACTER),	// ‘E’ -> ‘Ë’
  DEADTRANS(0x0884, L'H'  , 0x1e26, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḧ’
  DEADTRANS(0x0884, L'I'  , 0x00cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ï’
  DEADTRANS(0x0884, L'O'  , 0x00d6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ö’
  DEADTRANS(0x0884, L'U'  , 0x00dc, NORMAL_CHARACTER),	// ‘U’ -> ‘Ü’
  DEADTRANS(0x0884, L'W'  , 0x1e84, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẅ’
  DEADTRANS(0x0884, L'X'  , 0x1e8c, NORMAL_CHARACTER),	// ‘X’ -> ‘Ẍ’
  DEADTRANS(0x0884, L'Y'  , 0x0178, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ÿ’
  DEADTRANS(0x0884, L'a'  , 0x00e4, NORMAL_CHARACTER),	// ‘a’ -> ‘ä’
  DEADTRANS(0x0884, L'e'  , 0x00eb, NORMAL_CHARACTER),	// ‘e’ -> ‘ë’
  DEADTRANS(0x0884, L'h'  , 0x1e27, NORMAL_CHARACTER),	// ‘h’ -> ‘ḧ’
  DEADTRANS(0x0884, L'i'  , 0x00ef, NORMAL_CHARACTER),	// ‘i’ -> ‘ï’
  DEADTRANS(0x0884, L'o'  , 0x00f6, NORMAL_CHARACTER),	// ‘o’ -> ‘ö’
  DEADTRANS(0x0884, L't'  , 0x1e97, NORMAL_CHARACTER),	// ‘t’ -> ‘ẗ’
  DEADTRANS(0x0884, L'u'  , 0x00fc, NORMAL_CHARACTER),	// ‘u’ -> ‘ü’
  DEADTRANS(0x0884, L'w'  , 0x1e85, NORMAL_CHARACTER),	// ‘w’ -> ‘ẅ’
  DEADTRANS(0x0884, L'x'  , 0x1e8d, NORMAL_CHARACTER),	// ‘x’ -> ‘ẍ’
  DEADTRANS(0x0884, L'y'  , 0x00ff, NORMAL_CHARACTER),	// ‘y’ -> ‘ÿ’
  DEADTRANS(0x0884, 0x00a0, 0x00a8, NORMAL_CHARACTER),	// U+00A0 -> U+00A8
  DEADTRANS(0x0884, 0x03b9, 0x03ca, NORMAL_CHARACTER),	// ‘ι’ -> ‘ϊ’
  DEADTRANS(0x0884, 0x03c5, 0x03cb, NORMAL_CHARACTER),	// ‘υ’ -> ‘ϋ’
  DEADTRANS(0x0884, L':'  , 0x2025, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Diaeresis Below›
  DEADTRANS(0x0884, 0x02d9, 0x20db, CHAINED_DEAD_KEY),	// ‹Dot Above› -> ‹Three Dots Above›
  DEADTRANS(0x0884, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ;
  DEADTRANS(0x0885, L'.'  , 0x0326, NORMAL_CHARACTER),	// U+002E -> U+0326
  DEADTRANS(0x0885, L';'  , 0x204f, NORMAL_CHARACTER),	// U+003B -> U+204F
  DEADTRANS(0x0885, L'S'  , 0x0218, NORMAL_CHARACTER),	// ‘S’ -> ‘Ș’
  DEADTRANS(0x0885, L'T'  , 0x021a, NORMAL_CHARACTER),	// ‘T’ -> ‘Ț’
  DEADTRANS(0x0885, L's'  , 0x0219, NORMAL_CHARACTER),	// ‘s’ -> ‘ș’
  DEADTRANS(0x0885, L't'  , 0x021b, NORMAL_CHARACTER),	// ‘t’ -> ‘ț’
  DEADTRANS(0x0885, 0x00a0, L','  , NORMAL_CHARACTER),	// U+00A0 -> U+002C
  DEADTRANS(0x0885, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + <
  DEADTRANS(0x0886, L'"'  , 0x201c, NORMAL_CHARACTER),	// U+0022 -> U+201C
  DEADTRANS(0x0886, L'\'' , 0x2018, NORMAL_CHARACTER),	// U+0027 -> U+2018
  DEADTRANS(0x0886, L'('  , 0x207d, NORMAL_CHARACTER),	// U+0028 -> U+207D
  DEADTRANS(0x0886, L')'  , 0x207e, NORMAL_CHARACTER),	// U+0029 -> U+207E
  DEADTRANS(0x0886, L'+'  , 0x207a, NORMAL_CHARACTER),	// U+002B -> U+207A
  DEADTRANS(0x0886, L'-'  , 0x2190, NORMAL_CHARACTER),	// U+002D -> U+2190
  DEADTRANS(0x0886, L'.'  , 0x0302, NORMAL_CHARACTER),	// U+002E -> U+0302
  DEADTRANS(0x0886, L'/'  , 0x2199, NORMAL_CHARACTER),	// U+002F -> U+2199
  DEADTRANS(0x0886, L'0'  , 0x2070, NORMAL_CHARACTER),	// ‘0’ -> ‘⁰’
  DEADTRANS(0x0886, L'1'  , 0x00b9, NORMAL_CHARACTER),	// ‘1’ -> ‘¹’
  DEADTRANS(0x0886, L'2'  , 0x00b2, NORMAL_CHARACTER),	// ‘2’ -> ‘²’
  DEADTRANS(0x0886, L'3'  , 0x00b3, NORMAL_CHARACTER),	// ‘3’ -> ‘³’
  DEADTRANS(0x0886, L'4'  , 0x2074, NORMAL_CHARACTER),	// ‘4’ -> ‘⁴’
  DEADTRANS(0x0886, L'5'  , 0x2075, NORMAL_CHARACTER),	// ‘5’ -> ‘⁵’
  DEADTRANS(0x0886, L'6'  , 0x2076, NORMAL_CHARACTER),	// ‘6’ -> ‘⁶’
  DEADTRANS(0x0886, L'7'  , 0x2077, NORMAL_CHARACTER),	// ‘7’ -> ‘⁷’
  DEADTRANS(0x0886, L'8'  , 0x2078, NORMAL_CHARACTER),	// ‘8’ -> ‘⁸’
  DEADTRANS(0x0886, L'9'  , 0x2079, NORMAL_CHARACTER),	// ‘9’ -> ‘⁹’
  DEADTRANS(0x0886, L'<'  , 0x226a, NORMAL_CHARACTER),	// U+003C -> U+226A
  DEADTRANS(0x0886, L'='  , 0x2a7d, NORMAL_CHARACTER),	// U+003D -> U+2A7D
  DEADTRANS(0x0886, L'>'  , 0x25ca, NORMAL_CHARACTER),	// U+003E -> U+25CA
  DEADTRANS(0x0886, L'?'  , 0x2e2e, NORMAL_CHARACTER),	// U+003F -> U+2E2E
  DEADTRANS(0x0886, L'A'  , 0x00c2, NORMAL_CHARACTER),	// ‘A’ -> ‘Â’
  DEADTRANS(0x0886, L'C'  , 0x0108, NORMAL_CHARACTER),	// ‘C’ -> ‘Ĉ’
  DEADTRANS(0x0886, L'E'  , 0x00ca, NORMAL_CHARACTER),	// ‘E’ -> ‘Ê’
  DEADTRANS(0x0886, L'G'  , 0x011c, NORMAL_CHARACTER),	// ‘G’ -> ‘Ĝ’
  DEADTRANS(0x0886, L'H'  , 0x0124, NORMAL_CHARACTER),	// ‘H’ -> ‘Ĥ’
  DEADTRANS(0x0886, L'I'  , 0x00ce, NORMAL_CHARACTER),	// ‘I’ -> ‘Î’
  DEADTRANS(0x0886, L'J'  , 0x0134, NORMAL_CHARACTER),	// ‘J’ -> ‘Ĵ’
  DEADTRANS(0x0886, L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(0x0886, L'O'  , 0x00d4, NORMAL_CHARACTER),	// ‘O’ -> ‘Ô’
  DEADTRANS(0x0886, L'S'  , 0x015c, NORMAL_CHARACTER),	// ‘S’ -> ‘Ŝ’
  DEADTRANS(0x0886, L'U'  , 0x00db, NORMAL_CHARACTER),	// ‘U’ -> ‘Û’
  DEADTRANS(0x0886, L'W'  , 0x0174, NORMAL_CHARACTER),	// ‘W’ -> ‘Ŵ’
  DEADTRANS(0x0886, L'Y'  , 0x0176, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ŷ’
  DEADTRANS(0x0886, L'Z'  , 0x1e90, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẑ’
  DEADTRANS(0x0886, L'\\' , 0x2196, NORMAL_CHARACTER),	// U+005C -> U+2196
  DEADTRANS(0x0886, L'_'  , 0x2264, NORMAL_CHARACTER),	// U+005F -> U+2264
  DEADTRANS(0x0886, L'a'  , 0x00e2, NORMAL_CHARACTER),	// ‘a’ -> ‘â’
  DEADTRANS(0x0886, L'c'  , 0x0109, NORMAL_CHARACTER),	// ‘c’ -> ‘ĉ’
  DEADTRANS(0x0886, L'e'  , 0x00ea, NORMAL_CHARACTER),	// ‘e’ -> ‘ê’
  DEADTRANS(0x0886, L'g'  , 0x011d, NORMAL_CHARACTER),	// ‘g’ -> ‘ĝ’
  DEADTRANS(0x0886, L'h'  , 0x0125, NORMAL_CHARACTER),	// ‘h’ -> ‘ĥ’
  DEADTRANS(0x0886, L'i'  , 0x00ee, NORMAL_CHARACTER),	// ‘i’ -> ‘î’
  DEADTRANS(0x0886, L'j'  , 0x0135, NORMAL_CHARACTER),	// ‘j’ -> ‘ĵ’
  DEADTRANS(0x0886, L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(0x0886, L'o'  , 0x00f4, NORMAL_CHARACTER),	// ‘o’ -> ‘ô’
  DEADTRANS(0x0886, L's'  , 0x015d, NORMAL_CHARACTER),	// ‘s’ -> ‘ŝ’
  DEADTRANS(0x0886, L'u'  , 0x00fb, NORMAL_CHARACTER),	// ‘u’ -> ‘û’
  DEADTRANS(0x0886, L'w'  , 0x0175, NORMAL_CHARACTER),	// ‘w’ -> ‘ŵ’
  DEADTRANS(0x0886, L'y'  , 0x0177, NORMAL_CHARACTER),	// ‘y’ -> ‘ŷ’
  DEADTRANS(0x0886, L'z'  , 0x1e91, NORMAL_CHARACTER),	// ‘z’ -> ‘ẑ’
  DEADTRANS(0x0886, L'~'  , 0x2272, NORMAL_CHARACTER),	// U+007E -> U+2272
  DEADTRANS(0x0886, 0x00a0, L'^'  , NORMAL_CHARACTER),	// U+00A0 -> U+005E
  DEADTRANS(0x0886, 0x2026, 0x22f1, NORMAL_CHARACTER),	// U+2026 -> U+22F1
  DEADTRANS(0x0886, 0x222b, 0x2231, NORMAL_CHARACTER),	// U+222B -> U+2231
  DEADTRANS(0x0886, L' '  , 0x02c6, NORMAL_CHARACTER),	// U+0020 -> ‘ˆ’

// Dead key: Compose + =
  DEADTRANS(0x0887, L'!'  , 0x0af8, CHAINED_DEAD_KEY),	// U+0021 -> ‹Compose + = + !›
  DEADTRANS(0x0887, L'*'  , 0x0b0d, CHAINED_DEAD_KEY),	// U+002A -> ‹Compose + = + *›
  DEADTRANS(0x0887, L'/'  , 0x0b12, CHAINED_DEAD_KEY),	// U+002F -> ‹Compose + = + /›
  DEADTRANS(0x0887, L'<'  , 0x21d0, NORMAL_CHARACTER),	// U+003C -> U+21D0
  DEADTRANS(0x0887, L'='  , 0x2261, NORMAL_CHARACTER),	// U+003D -> U+2261
  DEADTRANS(0x0887, L'>'  , 0x21d2, NORMAL_CHARACTER),	// U+003E -> U+21D2
  DEADTRANS(0x0887, L'?'  , 0x225f, NORMAL_CHARACTER),	// U+003F -> U+225F
  DEADTRANS(0x0887, L'^'  , 0x21d1, NORMAL_CHARACTER),	// U+005E -> U+21D1
  DEADTRANS(0x0887, L'_'  , 0x21d3, NORMAL_CHARACTER),	// U+005F -> U+21D3
  DEADTRANS(0x0887, L'd'  , 0x0b29, CHAINED_DEAD_KEY),	// ‘d’ -> ‹Compose + = + d›
  DEADTRANS(0x0887, L'|'  , 0x0b34, CHAINED_DEAD_KEY),	// U+007C -> ‹Compose + = + |›
  DEADTRANS(0x0887, L'~'  , 0x2a73, NORMAL_CHARACTER),	// U+007E -> U+2A73
  DEADTRANS(0x0887, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + >
  DEADTRANS(0x0888, L'"'  , 0x201d, NORMAL_CHARACTER),	// U+0022 -> U+201D
  DEADTRANS(0x0888, L'\'' , 0x2019, NORMAL_CHARACTER),	// U+0027 -> U+2019
  DEADTRANS(0x0888, L'('  , 0x208d, NORMAL_CHARACTER),	// U+0028 -> U+208D
  DEADTRANS(0x0888, L')'  , 0x208e, NORMAL_CHARACTER),	// U+0029 -> U+208E
  DEADTRANS(0x0888, L'+'  , 0x208a, NORMAL_CHARACTER),	// U+002B -> U+208A
  DEADTRANS(0x0888, L'.'  , 0x030c, NORMAL_CHARACTER),	// U+002E -> U+030C
  DEADTRANS(0x0888, L'/'  , 0x2197, NORMAL_CHARACTER),	// U+002F -> U+2197
  DEADTRANS(0x0888, L'0'  , 0x2080, NORMAL_CHARACTER),	// ‘0’ -> ‘₀’
  DEADTRANS(0x0888, L'1'  , 0x2081, NORMAL_CHARACTER),	// ‘1’ -> ‘₁’
  DEADTRANS(0x0888, L'2'  , 0x2082, NORMAL_CHARACTER),	// ‘2’ -> ‘₂’
  DEADTRANS(0x0888, L'3'  , 0x2083, NORMAL_CHARACTER),	// ‘3’ -> ‘₃’
  DEADTRANS(0x0888, L'4'  , 0x2084, NORMAL_CHARACTER),	// ‘4’ -> ‘₄’
  DEADTRANS(0x0888, L'5'  , 0x2085, NORMAL_CHARACTER),	// ‘5’ -> ‘₅’
  DEADTRANS(0x0888, L'6'  , 0x2086, NORMAL_CHARACTER),	// ‘6’ -> ‘₆’
  DEADTRANS(0x0888, L'7'  , 0x2087, NORMAL_CHARACTER),	// ‘7’ -> ‘₇’
  DEADTRANS(0x0888, L'8'  , 0x2088, NORMAL_CHARACTER),	// ‘8’ -> ‘₈’
  DEADTRANS(0x0888, L'9'  , 0x2089, NORMAL_CHARACTER),	// ‘9’ -> ‘₉’
  DEADTRANS(0x0888, L'<'  , 0x2277, NORMAL_CHARACTER),	// U+003C -> U+2277
  DEADTRANS(0x0888, L'='  , 0x2a7e, NORMAL_CHARACTER),	// U+003D -> U+2A7E
  DEADTRANS(0x0888, L'>'  , 0x226b, NORMAL_CHARACTER),	// U+003E -> U+226B
  DEADTRANS(0x0888, L'A'  , 0x01cd, NORMAL_CHARACTER),	// ‘A’ -> ‘Ǎ’
  DEADTRANS(0x0888, L'C'  , 0x010c, NORMAL_CHARACTER),	// ‘C’ -> ‘Č’
  DEADTRANS(0x0888, L'D'  , 0x010e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ď’
  DEADTRANS(0x0888, L'E'  , 0x011a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ě’
  DEADTRANS(0x0888, L'G'  , 0x01e6, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǧ’
  DEADTRANS(0x0888, L'H'  , 0x021e, NORMAL_CHARACTER),	// ‘H’ -> ‘Ȟ’
  DEADTRANS(0x0888, L'I'  , 0x01cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ǐ’
  DEADTRANS(0x0888, L'K'  , 0x01e8, NORMAL_CHARACTER),	// ‘K’ -> ‘Ǩ’
  DEADTRANS(0x0888, L'L'  , 0x013d, NORMAL_CHARACTER),	// ‘L’ -> ‘Ľ’
  DEADTRANS(0x0888, L'N'  , 0x0147, NORMAL_CHARACTER),	// ‘N’ -> ‘Ň’
  DEADTRANS(0x0888, L'O'  , 0x01d1, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǒ’
  DEADTRANS(0x0888, L'R'  , 0x0158, NORMAL_CHARACTER),	// ‘R’ -> ‘Ř’
  DEADTRANS(0x0888, L'S'  , 0x0160, NORMAL_CHARACTER),	// ‘S’ -> ‘Š’
  DEADTRANS(0x0888, L'T'  , 0x0164, NORMAL_CHARACTER),	// ‘T’ -> ‘Ť’
  DEADTRANS(0x0888, L'U'  , 0x01d3, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǔ’
  DEADTRANS(0x0888, L'Z'  , 0x017d, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ž’
  DEADTRANS(0x0888, L'\\' , 0x2198, NORMAL_CHARACTER),	// U+005C -> U+2198
  DEADTRANS(0x0888, L'_'  , 0x2265, NORMAL_CHARACTER),	// U+005F -> U+2265
  DEADTRANS(0x0888, L'a'  , 0x01ce, NORMAL_CHARACTER),	// ‘a’ -> ‘ǎ’
  DEADTRANS(0x0888, L'c'  , 0x010d, NORMAL_CHARACTER),	// ‘c’ -> ‘č’
  DEADTRANS(0x0888, L'd'  , 0x010f, NORMAL_CHARACTER),	// ‘d’ -> ‘ď’
  DEADTRANS(0x0888, L'e'  , 0x011b, NORMAL_CHARACTER),	// ‘e’ -> ‘ě’
  DEADTRANS(0x0888, L'g'  , 0x01e7, NORMAL_CHARACTER),	// ‘g’ -> ‘ǧ’
  DEADTRANS(0x0888, L'h'  , 0x021f, NORMAL_CHARACTER),	// ‘h’ -> ‘ȟ’
  DEADTRANS(0x0888, L'i'  , 0x01d0, NORMAL_CHARACTER),	// ‘i’ -> ‘ǐ’
  DEADTRANS(0x0888, L'j'  , 0x01f0, NORMAL_CHARACTER),	// ‘j’ -> ‘ǰ’
  DEADTRANS(0x0888, L'k'  , 0x01e9, NORMAL_CHARACTER),	// ‘k’ -> ‘ǩ’
  DEADTRANS(0x0888, L'l'  , 0x013e, NORMAL_CHARACTER),	// ‘l’ -> ‘ľ’
  DEADTRANS(0x0888, L'n'  , 0x0148, NORMAL_CHARACTER),	// ‘n’ -> ‘ň’
  DEADTRANS(0x0888, L'o'  , 0x01d2, NORMAL_CHARACTER),	// ‘o’ -> ‘ǒ’
  DEADTRANS(0x0888, L'r'  , 0x0159, NORMAL_CHARACTER),	// ‘r’ -> ‘ř’
  DEADTRANS(0x0888, L's'  , 0x0161, NORMAL_CHARACTER),	// ‘s’ -> ‘š’
  DEADTRANS(0x0888, L't'  , 0x0165, NORMAL_CHARACTER),	// ‘t’ -> ‘ť’
  DEADTRANS(0x0888, L'u'  , 0x01d4, NORMAL_CHARACTER),	// ‘u’ -> ‘ǔ’
  DEADTRANS(0x0888, L'z'  , 0x017e, NORMAL_CHARACTER),	// ‘z’ -> ‘ž’
  DEADTRANS(0x0888, L'~'  , 0x2273, NORMAL_CHARACTER),	// U+007E -> U+2273
  DEADTRANS(0x0888, 0x00dc, 0x01d9, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǚ’
  DEADTRANS(0x0888, 0x00fc, 0x01da, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǚ’
  DEADTRANS(0x0888, 0x2026, 0x22f0, NORMAL_CHARACTER),	// U+2026 -> U+22F0
  DEADTRANS(0x0888, 0x222b, 0x2a11, NORMAL_CHARACTER),	// U+222B -> U+2A11
  DEADTRANS(0x0888, L' '  , 0x02c7, NORMAL_CHARACTER),	// U+0020 -> ‘ˇ’

// Dead key: Compose + ?
  DEADTRANS(0x0889, L'!'  , 0x2048, NORMAL_CHARACTER),	// U+0021 -> U+2048
  DEADTRANS(0x0889, L'?'  , 0x2047, NORMAL_CHARACTER),	// U+003F -> U+2047
  DEADTRANS(0x0889, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @
  DEADTRANS(0x088a, L'%'  , 0x0b3a, CHAINED_DEAD_KEY),	// U+0025 -> ‹Compose + @ + %›
  DEADTRANS(0x088a, L'*'  , 0x229b, NORMAL_CHARACTER),	// U+002A -> U+229B
  DEADTRANS(0x088a, L'+'  , 0x2295, NORMAL_CHARACTER),	// U+002B -> U+2295
  DEADTRANS(0x088a, L'-'  , 0x2296, NORMAL_CHARACTER),	// U+002D -> U+2296
  DEADTRANS(0x088a, L'.'  , 0x2299, NORMAL_CHARACTER),	// U+002E -> U+2299
  DEADTRANS(0x088a, L'/'  , 0x2298, NORMAL_CHARACTER),	// U+002F -> U+2298
  DEADTRANS(0x088a, L'0'  , 0x24ea, NORMAL_CHARACTER),	// ‘0’ -> ‘⓪’
  DEADTRANS(0x088a, L'1'  , 0x2460, NORMAL_CHARACTER),	// ‘1’ -> ‘①’
  DEADTRANS(0x088a, L'2'  , 0x2461, NORMAL_CHARACTER),	// ‘2’ -> ‘②’
  DEADTRANS(0x088a, L'3'  , 0x2462, NORMAL_CHARACTER),	// ‘3’ -> ‘③’
  DEADTRANS(0x088a, L'4'  , 0x2463, NORMAL_CHARACTER),	// ‘4’ -> ‘④’
  DEADTRANS(0x088a, L'5'  , 0x2464, NORMAL_CHARACTER),	// ‘5’ -> ‘⑤’
  DEADTRANS(0x088a, L'6'  , 0x2465, NORMAL_CHARACTER),	// ‘6’ -> ‘⑥’
  DEADTRANS(0x088a, L'7'  , 0x2466, NORMAL_CHARACTER),	// ‘7’ -> ‘⑦’
  DEADTRANS(0x088a, L'8'  , 0x2467, NORMAL_CHARACTER),	// ‘8’ -> ‘⑧’
  DEADTRANS(0x088a, L'9'  , 0x2468, NORMAL_CHARACTER),	// ‘9’ -> ‘⑨’
  DEADTRANS(0x088a, L'<'  , 0x29c0, NORMAL_CHARACTER),	// U+003C -> U+29C0
  DEADTRANS(0x088a, L'='  , 0x229c, NORMAL_CHARACTER),	// U+003D -> U+229C
  DEADTRANS(0x088a, L'>'  , 0x29c1, NORMAL_CHARACTER),	// U+003E -> U+29C1
  DEADTRANS(0x088a, L'\\' , 0x29b8, NORMAL_CHARACTER),	// U+005C -> U+29B8
  DEADTRANS(0x088a, L'c'  , 0x00a9, NORMAL_CHARACTER),	// ‘c’ -> U+00A9
  DEADTRANS(0x088a, L'n'  , 0x0b46, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + @ + n›
  DEADTRANS(0x088a, L'o'  , 0x229a, NORMAL_CHARACTER),	// ‘o’ -> U+229A
  DEADTRANS(0x088a, L'p'  , 0x2117, NORMAL_CHARACTER),	// ‘p’ -> U+2117
  DEADTRANS(0x088a, L'r'  , 0x00ae, NORMAL_CHARACTER),	// ‘r’ -> U+00AE
  DEADTRANS(0x088a, L'x'  , 0x2297, NORMAL_CHARACTER),	// ‘x’ -> U+2297
  DEADTRANS(0x088a, L'|'  , 0x29b6, NORMAL_CHARACTER),	// U+007C -> U+29B6
  DEADTRANS(0x088a, 0x00d7, 0x2297, NORMAL_CHARACTER),	// U+00D7 -> U+2297
  DEADTRANS(0x088a, 0x2218, 0x229a, NORMAL_CHARACTER),	// U+2218 -> U+229A
  DEADTRANS(0x088a, 0x222b, 0x222e, NORMAL_CHARACTER),	// U+222B -> U+222E
  DEADTRANS(0x088a, 0x266b, 0x0b49, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + @ + Compose›
  DEADTRANS(0x088a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + A
  DEADTRANS(0x088b, L'A'  , 0x2200, NORMAL_CHARACTER),	// ‘A’ -> U+2200
  DEADTRANS(0x088b, L'E'  , 0x00c6, NORMAL_CHARACTER),	// ‘E’ -> ‘Æ’
  DEADTRANS(0x088b, L'O'  , 0xa734, NORMAL_CHARACTER),	// ‘O’ -> ‘Ꜵ’
  DEADTRANS(0x088b, L'S'  , 0x214d, NORMAL_CHARACTER),	// ‘S’ -> U+214D
  DEADTRANS(0x088b, L'U'  , 0xa736, NORMAL_CHARACTER),	// ‘U’ -> ‘Ꜷ’
  DEADTRANS(0x088b, L'V'  , 0xa738, NORMAL_CHARACTER),	// ‘V’ -> ‘Ꜹ’
  DEADTRANS(0x088b, L'e'  , 0x00c6, NORMAL_CHARACTER),	// ‘e’ -> ‘Æ’
  DEADTRANS(0x088b, L'o'  , 0xa734, NORMAL_CHARACTER),	// ‘o’ -> ‘Ꜵ’
  DEADTRANS(0x088b, L'v'  , 0xa738, NORMAL_CHARACTER),	// ‘v’ -> ‘Ꜹ’
  DEADTRANS(0x088b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + C
  DEADTRANS(0x088c, L'A'  , 0x0410, NORMAL_CHARACTER),	// ‘A’ -> ‘А’
  DEADTRANS(0x088c, L'B'  , 0x0411, NORMAL_CHARACTER),	// ‘B’ -> ‘Б’
  DEADTRANS(0x088c, L'C'  , 0x0426, NORMAL_CHARACTER),	// ‘C’ -> ‘Ц’
  DEADTRANS(0x088c, L'D'  , 0x0414, NORMAL_CHARACTER),	// ‘D’ -> ‘Д’
  DEADTRANS(0x088c, L'E'  , 0x0415, NORMAL_CHARACTER),	// ‘E’ -> ‘Е’
  DEADTRANS(0x088c, L'F'  , 0x0424, NORMAL_CHARACTER),	// ‘F’ -> ‘Ф’
  DEADTRANS(0x088c, L'G'  , 0x0413, NORMAL_CHARACTER),	// ‘G’ -> ‘Г’
  DEADTRANS(0x088c, L'H'  , 0x0425, NORMAL_CHARACTER),	// ‘H’ -> ‘Х’
  DEADTRANS(0x088c, L'I'  , 0x0418, NORMAL_CHARACTER),	// ‘I’ -> ‘И’
  DEADTRANS(0x088c, L'J'  , 0x042d, NORMAL_CHARACTER),	// ‘J’ -> ‘Э’
  DEADTRANS(0x088c, L'K'  , 0x041a, NORMAL_CHARACTER),	// ‘K’ -> ‘К’
  DEADTRANS(0x088c, L'L'  , 0x041b, NORMAL_CHARACTER),	// ‘L’ -> ‘Л’
  DEADTRANS(0x088c, L'M'  , 0x041c, NORMAL_CHARACTER),	// ‘M’ -> ‘М’
  DEADTRANS(0x088c, L'N'  , 0x041d, NORMAL_CHARACTER),	// ‘N’ -> ‘Н’
  DEADTRANS(0x088c, L'O'  , 0x041e, NORMAL_CHARACTER),	// ‘O’ -> ‘О’
  DEADTRANS(0x088c, L'P'  , 0x041f, NORMAL_CHARACTER),	// ‘P’ -> ‘П’
  DEADTRANS(0x088c, L'Q'  , 0x0416, NORMAL_CHARACTER),	// ‘Q’ -> ‘Ж’
  DEADTRANS(0x088c, L'R'  , 0x0420, NORMAL_CHARACTER),	// ‘R’ -> ‘Р’
  DEADTRANS(0x088c, L'S'  , 0x0421, NORMAL_CHARACTER),	// ‘S’ -> ‘С’
  DEADTRANS(0x088c, L'T'  , 0x0422, NORMAL_CHARACTER),	// ‘T’ -> ‘Т’
  DEADTRANS(0x088c, L'U'  , 0x0423, NORMAL_CHARACTER),	// ‘U’ -> ‘У’
  DEADTRANS(0x088c, L'V'  , 0x0412, NORMAL_CHARACTER),	// ‘V’ -> ‘В’
  DEADTRANS(0x088c, L'W'  , 0x0428, NORMAL_CHARACTER),	// ‘W’ -> ‘Ш’
  DEADTRANS(0x088c, L'X'  , 0x0427, NORMAL_CHARACTER),	// ‘X’ -> ‘Ч’
  DEADTRANS(0x088c, L'Y'  , 0x042b, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ы’
  DEADTRANS(0x088c, L'Z'  , 0x0417, NORMAL_CHARACTER),	// ‘Z’ -> ‘З’
  DEADTRANS(0x088c, L'a'  , 0x0430, NORMAL_CHARACTER),	// ‘a’ -> ‘а’
  DEADTRANS(0x088c, L'b'  , 0x0431, NORMAL_CHARACTER),	// ‘b’ -> ‘б’
  DEADTRANS(0x088c, L'c'  , 0x0446, NORMAL_CHARACTER),	// ‘c’ -> ‘ц’
  DEADTRANS(0x088c, L'd'  , 0x0434, NORMAL_CHARACTER),	// ‘d’ -> ‘д’
  DEADTRANS(0x088c, L'e'  , 0x0435, NORMAL_CHARACTER),	// ‘e’ -> ‘е’
  DEADTRANS(0x088c, L'f'  , 0x0444, NORMAL_CHARACTER),	// ‘f’ -> ‘ф’
  DEADTRANS(0x088c, L'g'  , 0x0433, NORMAL_CHARACTER),	// ‘g’ -> ‘г’
  DEADTRANS(0x088c, L'h'  , 0x0445, NORMAL_CHARACTER),	// ‘h’ -> ‘х’
  DEADTRANS(0x088c, L'i'  , 0x0438, NORMAL_CHARACTER),	// ‘i’ -> ‘и’
  DEADTRANS(0x088c, L'j'  , 0x044d, NORMAL_CHARACTER),	// ‘j’ -> ‘э’
  DEADTRANS(0x088c, L'k'  , 0x043a, NORMAL_CHARACTER),	// ‘k’ -> ‘к’
  DEADTRANS(0x088c, L'l'  , 0x043b, NORMAL_CHARACTER),	// ‘l’ -> ‘л’
  DEADTRANS(0x088c, L'm'  , 0x043c, NORMAL_CHARACTER),	// ‘m’ -> ‘м’
  DEADTRANS(0x088c, L'n'  , 0x043d, NORMAL_CHARACTER),	// ‘n’ -> ‘н’
  DEADTRANS(0x088c, L'o'  , 0x043e, NORMAL_CHARACTER),	// ‘o’ -> ‘о’
  DEADTRANS(0x088c, L'p'  , 0x043f, NORMAL_CHARACTER),	// ‘p’ -> ‘п’
  DEADTRANS(0x088c, L'q'  , 0x0436, NORMAL_CHARACTER),	// ‘q’ -> ‘ж’
  DEADTRANS(0x088c, L'r'  , 0x0440, NORMAL_CHARACTER),	// ‘r’ -> ‘р’
  DEADTRANS(0x088c, L's'  , 0x0441, NORMAL_CHARACTER),	// ‘s’ -> ‘с’
  DEADTRANS(0x088c, L't'  , 0x0442, NORMAL_CHARACTER),	// ‘t’ -> ‘т’
  DEADTRANS(0x088c, L'u'  , 0x0443, NORMAL_CHARACTER),	// ‘u’ -> ‘у’
  DEADTRANS(0x088c, L'v'  , 0x0432, NORMAL_CHARACTER),	// ‘v’ -> ‘в’
  DEADTRANS(0x088c, L'w'  , 0x0448, NORMAL_CHARACTER),	// ‘w’ -> ‘ш’
  DEADTRANS(0x088c, L'x'  , 0x0447, NORMAL_CHARACTER),	// ‘x’ -> ‘ч’
  DEADTRANS(0x088c, L'y'  , 0x044b, NORMAL_CHARACTER),	// ‘y’ -> ‘ы’
  DEADTRANS(0x088c, L'z'  , 0x0437, NORMAL_CHARACTER),	// ‘z’ -> ‘з’
  DEADTRANS(0x088c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + D
  DEADTRANS(0x088d, L'Z'  , 0x01f1, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ǳ’
  DEADTRANS(0x088d, L'z'  , 0x01f2, NORMAL_CHARACTER),	// ‘z’ -> ‘ǲ’
  DEADTRANS(0x088d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + E
  DEADTRANS(0x088e, L'E'  , 0x2203, NORMAL_CHARACTER),	// ‘E’ -> U+2203
  DEADTRANS(0x088e, L't'  , 0xa76a, NORMAL_CHARACTER),	// ‘t’ -> ‘Ꝫ’
  DEADTRANS(0x088e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + F
  DEADTRANS(0x088f, L'i'  , 0xfb03, NORMAL_CHARACTER),	// ‘i’ -> ‘ﬃ’
  DEADTRANS(0x088f, L'l'  , 0xfb04, NORMAL_CHARACTER),	// ‘l’ -> ‘ﬄ’
  DEADTRANS(0x088f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + G
  DEADTRANS(0x0890, L'B'  , 0x3387, NORMAL_CHARACTER),	// ‘B’ -> U+3387
  DEADTRANS(0x0890, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + H
  DEADTRANS(0x0891, L'v'  , 0x01f6, NORMAL_CHARACTER),	// ‘v’ -> ‘Ƕ’
  DEADTRANS(0x0891, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + I
  DEADTRANS(0x0892, L'0'  , 0x2169, NORMAL_CHARACTER),	// ‘0’ -> ‘Ⅹ’
  DEADTRANS(0x0892, L'1'  , 0x2160, NORMAL_CHARACTER),	// ‘1’ -> ‘Ⅰ’
  DEADTRANS(0x0892, L'5'  , 0x2164, NORMAL_CHARACTER),	// ‘5’ -> ‘Ⅴ’
  DEADTRANS(0x0892, L'C'  , 0x216d, NORMAL_CHARACTER),	// ‘C’ -> ‘Ⅽ’
  DEADTRANS(0x0892, L'D'  , 0x216e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ⅾ’
  DEADTRANS(0x0892, L'I'  , 0x2160, NORMAL_CHARACTER),	// ‘I’ -> ‘Ⅰ’
  DEADTRANS(0x0892, L'J'  , 0x0132, NORMAL_CHARACTER),	// ‘J’ -> ‘Ĳ’
  DEADTRANS(0x0892, L'L'  , 0x216c, NORMAL_CHARACTER),	// ‘L’ -> ‘Ⅼ’
  DEADTRANS(0x0892, L'M'  , 0x216f, NORMAL_CHARACTER),	// ‘M’ -> ‘Ⅿ’
  DEADTRANS(0x0892, L'V'  , 0x2164, NORMAL_CHARACTER),	// ‘V’ -> ‘Ⅴ’
  DEADTRANS(0x0892, L'X'  , 0x2169, NORMAL_CHARACTER),	// ‘X’ -> ‘Ⅹ’
  DEADTRANS(0x0892, L'c'  , 0x216d, NORMAL_CHARACTER),	// ‘c’ -> ‘Ⅽ’
  DEADTRANS(0x0892, L'd'  , 0x216e, NORMAL_CHARACTER),	// ‘d’ -> ‘Ⅾ’
  DEADTRANS(0x0892, L'i'  , 0x2160, NORMAL_CHARACTER),	// ‘i’ -> ‘Ⅰ’
  DEADTRANS(0x0892, L'l'  , 0x216c, NORMAL_CHARACTER),	// ‘l’ -> ‘Ⅼ’
  DEADTRANS(0x0892, L'm'  , 0x216f, NORMAL_CHARACTER),	// ‘m’ -> ‘Ⅿ’
  DEADTRANS(0x0892, L'v'  , 0x2164, NORMAL_CHARACTER),	// ‘v’ -> ‘Ⅴ’
  DEADTRANS(0x0892, L'x'  , 0x2169, NORMAL_CHARACTER),	// ‘x’ -> ‘Ⅹ’
  DEADTRANS(0x0892, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + L
  DEADTRANS(0x0893, L'J'  , 0x01c7, NORMAL_CHARACTER),	// ‘J’ -> ‘Ǉ’
  DEADTRANS(0x0893, L'j'  , 0x01c8, NORMAL_CHARACTER),	// ‘j’ -> ‘ǈ’
  DEADTRANS(0x0893, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M
  DEADTRANS(0x0894, L'!'  , 0x0b4e, CHAINED_DEAD_KEY),	// U+0021 -> ‹Compose + M + !›
  DEADTRANS(0x0894, L'&'  , 0x2227, NORMAL_CHARACTER),	// U+0026 -> U+2227
  DEADTRANS(0x0894, L'*'  , 0x00d7, NORMAL_CHARACTER),	// U+002A -> U+00D7
  DEADTRANS(0x0894, L'-'  , 0x2212, NORMAL_CHARACTER),	// U+002D -> U+2212
  DEADTRANS(0x0894, L'.'  , 0x2219, NORMAL_CHARACTER),	// U+002E -> U+2219
  DEADTRANS(0x0894, L'/'  , 0x2215, NORMAL_CHARACTER),	// U+002F -> U+2215
  DEADTRANS(0x0894, L'8'  , 0x221e, NORMAL_CHARACTER),	// ‘8’ -> U+221E
  DEADTRANS(0x0894, L'='  , 0x0b4f, CHAINED_DEAD_KEY),	// U+003D -> ‹Compose + M + =›
  DEADTRANS(0x0894, L'?'  , 0x00bf, NORMAL_CHARACTER),	// U+003F -> U+00BF
  DEADTRANS(0x0894, L'A'  , 0x2200, NORMAL_CHARACTER),	// ‘A’ -> U+2200
  DEADTRANS(0x0894, L'B'  , 0x3386, NORMAL_CHARACTER),	// ‘B’ -> U+3386
  DEADTRANS(0x0894, L'C'  , 0x2283, NORMAL_CHARACTER),	// ‘C’ -> U+2283
  DEADTRANS(0x0894, L'E'  , 0x2203, NORMAL_CHARACTER),	// ‘E’ -> U+2203
  DEADTRANS(0x0894, L'F'  , 0x0b51, CHAINED_DEAD_KEY),	// ‘F’ -> ‹Compose + M + F›
  DEADTRANS(0x0894, L'I'  , 0x0b52, CHAINED_DEAD_KEY),	// ‘I’ -> ‹Compose + M + I›
  DEADTRANS(0x0894, L'N'  , 0x2115, NORMAL_CHARACTER),	// ‘N’ -> ‘ℕ’
  DEADTRANS(0x0894, L'Q'  , 0x211a, NORMAL_CHARACTER),	// ‘Q’ -> ‘ℚ’
  DEADTRANS(0x0894, L'R'  , 0x211d, NORMAL_CHARACTER),	// ‘R’ -> ‘ℝ’
  DEADTRANS(0x0894, L'S'  , 0x0b53, CHAINED_DEAD_KEY),	// ‘S’ -> ‹Compose + M + S›
  DEADTRANS(0x0894, L'Z'  , 0x2124, NORMAL_CHARACTER),	// ‘Z’ -> ‘ℤ’
  DEADTRANS(0x0894, L'^'  , 0x2227, NORMAL_CHARACTER),	// U+005E -> U+2227
  DEADTRANS(0x0894, L'b'  , 0x0b54, CHAINED_DEAD_KEY),	// ‘b’ -> ‹Compose + M + b›
  DEADTRANS(0x0894, L'c'  , 0x2282, NORMAL_CHARACTER),	// ‘c’ -> U+2282
  DEADTRANS(0x0894, L'd'  , 0x0b55, CHAINED_DEAD_KEY),	// ‘d’ -> ‹Compose + M + d›
  DEADTRANS(0x0894, L'e'  , 0x2208, NORMAL_CHARACTER),	// ‘e’ -> U+2208
  DEADTRANS(0x0894, L'f'  , 0x0b58, CHAINED_DEAD_KEY),	// ‘f’ -> ‹Compose + M + f›
  DEADTRANS(0x0894, L'i'  , 0x0b59, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + M + i›
  DEADTRANS(0x0894, L'o'  , 0x2218, NORMAL_CHARACTER),	// ‘o’ -> U+2218
  DEADTRANS(0x0894, L's'  , 0x0b5a, CHAINED_DEAD_KEY),	// ‘s’ -> ‹Compose + M + s›
  DEADTRANS(0x0894, L'v'  , 0x2228, NORMAL_CHARACTER),	// ‘v’ -> U+2228
  DEADTRANS(0x0894, L'{'  , 0x0b5b, CHAINED_DEAD_KEY),	// U+007B -> ‹Compose + M + {›
  DEADTRANS(0x0894, 0x2018, 0x201b, NORMAL_CHARACTER),	// U+2018 -> U+201B
  DEADTRANS(0x0894, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + N
  DEADTRANS(0x0895, L'*'  , 0x2210, NORMAL_CHARACTER),	// U+002A -> U+2210
  DEADTRANS(0x0895, L'J'  , 0x01ca, NORMAL_CHARACTER),	// ‘J’ -> ‘Ǌ’
  DEADTRANS(0x0895, L'j'  , 0x01cb, NORMAL_CHARACTER),	// ‘j’ -> ‘ǋ’
  DEADTRANS(0x0895, L'o'  , 0x2116, NORMAL_CHARACTER),	// ‘o’ -> U+2116
  DEADTRANS(0x0895, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + O
  DEADTRANS(0x0896, L'E'  , 0x0152, NORMAL_CHARACTER),	// ‘E’ -> ‘Œ’
  DEADTRANS(0x0896, L'e'  , 0x0152, NORMAL_CHARACTER),	// ‘e’ -> ‘Œ’
  DEADTRANS(0x0896, L'u'  , 0x0222, NORMAL_CHARACTER),	// ‘u’ -> ‘Ȣ’
  DEADTRANS(0x0896, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + R
  DEADTRANS(0x0897, L'&'  , 0x214b, NORMAL_CHARACTER),	// U+0026 -> U+214B
  DEADTRANS(0x0897, L'*'  , 0x204e, NORMAL_CHARACTER),	// U+002A -> U+204E
  DEADTRANS(0x0897, L','  , 0x2e32, NORMAL_CHARACTER),	// U+002C -> U+2E32
  DEADTRANS(0x0897, L'2'  , 0x218a, NORMAL_CHARACTER),	// ‘2’ -> U+218A
  DEADTRANS(0x0897, L'3'  , 0x218b, NORMAL_CHARACTER),	// ‘3’ -> U+218B
  DEADTRANS(0x0897, L'8'  , 0x221e, NORMAL_CHARACTER),	// ‘8’ -> U+221E
  DEADTRANS(0x0897, L';'  , 0x2e35, NORMAL_CHARACTER),	// U+003B -> U+2E35
  DEADTRANS(0x0897, L'A'  , 0x2c6f, NORMAL_CHARACTER),	// ‘A’ -> ‘Ɐ’
  DEADTRANS(0x0897, L'C'  , 0x0186, NORMAL_CHARACTER),	// ‘C’ -> ‘Ɔ’
  DEADTRANS(0x0897, L'F'  , 0x2132, NORMAL_CHARACTER),	// ‘F’ -> ‘Ⅎ’
  DEADTRANS(0x0897, L'G'  , 0x2141, NORMAL_CHARACTER),	// ‘G’ -> U+2141
  DEADTRANS(0x0897, L'H'  , 0xa78d, NORMAL_CHARACTER),	// ‘H’ -> ‘Ɥ’
  DEADTRANS(0x0897, L'K'  , 0xa7b0, NORMAL_CHARACTER),	// ‘K’ -> ‘Ʞ’
  DEADTRANS(0x0897, L'L'  , 0xa780, NORMAL_CHARACTER),	// ‘L’ -> ‘Ꞁ’
  DEADTRANS(0x0897, L'M'  , 0xa7fa, NORMAL_CHARACTER),	// ‘M’ -> ‘ꟺ’
  DEADTRANS(0x0897, L'R'  , 0x027a, NORMAL_CHARACTER),	// ‘R’ -> ‘ɺ’
  DEADTRANS(0x0897, L'T'  , 0xa7b1, NORMAL_CHARACTER),	// ‘T’ -> ‘Ʇ’
  DEADTRANS(0x0897, L'V'  , 0x0245, NORMAL_CHARACTER),	// ‘V’ -> ‘Ʌ’
  DEADTRANS(0x0897, L'Y'  , 0x2144, NORMAL_CHARACTER),	// ‘Y’ -> U+2144
  DEADTRANS(0x0897, L'a'  , 0x0250, NORMAL_CHARACTER),	// ‘a’ -> ‘ɐ’
  DEADTRANS(0x0897, L'b'  , L'q'  , NORMAL_CHARACTER),	// ‘b’ -> ‘q’
  DEADTRANS(0x0897, L'c'  , 0x0254, NORMAL_CHARACTER),	// ‘c’ -> ‘ɔ’
  DEADTRANS(0x0897, L'e'  , 0x0259, NORMAL_CHARACTER),	// ‘e’ -> ‘ə’
  DEADTRANS(0x0897, L'f'  , 0x214e, NORMAL_CHARACTER),	// ‘f’ -> ‘ⅎ’
  DEADTRANS(0x0897, L'g'  , 0x1d77, NORMAL_CHARACTER),	// ‘g’ -> ‘ᵷ’
  DEADTRANS(0x0897, L'h'  , 0x0265, NORMAL_CHARACTER),	// ‘h’ -> ‘ɥ’
  DEADTRANS(0x0897, L'i'  , 0x1d09, NORMAL_CHARACTER),	// ‘i’ -> ‘ᴉ’
  DEADTRANS(0x0897, L'k'  , 0x029e, NORMAL_CHARACTER),	// ‘k’ -> ‘ʞ’
  DEADTRANS(0x0897, L'l'  , 0xa781, NORMAL_CHARACTER),	// ‘l’ -> ‘ꞁ’
  DEADTRANS(0x0897, L'm'  , 0x026f, NORMAL_CHARACTER),	// ‘m’ -> ‘ɯ’
  DEADTRANS(0x0897, L'p'  , L'd'  , NORMAL_CHARACTER),	// ‘p’ -> ‘d’
  DEADTRANS(0x0897, L'r'  , 0x0279, NORMAL_CHARACTER),	// ‘r’ -> ‘ɹ’
  DEADTRANS(0x0897, L't'  , 0x0287, NORMAL_CHARACTER),	// ‘t’ -> ‘ʇ’
  DEADTRANS(0x0897, L'v'  , 0x028c, NORMAL_CHARACTER),	// ‘v’ -> ‘ʌ’
  DEADTRANS(0x0897, L'w'  , 0x028d, NORMAL_CHARACTER),	// ‘w’ -> ‘ʍ’
  DEADTRANS(0x0897, L'y'  , 0x028e, NORMAL_CHARACTER),	// ‘y’ -> ‘ʎ’
  DEADTRANS(0x0897, 0x00ac, 0x2319, NORMAL_CHARACTER),	// U+00AC -> U+2319
  DEADTRANS(0x0897, 0x2026, 0x22ee, NORMAL_CHARACTER),	// U+2026 -> U+22EE
  DEADTRANS(0x0897, 0x2208, 0x220b, NORMAL_CHARACTER),	// U+2208 -> U+220B
  DEADTRANS(0x0897, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + S
  DEADTRANS(0x0898, L'S'  , 0x1e9e, NORMAL_CHARACTER),	// ‘S’ -> ‘ẞ’
  DEADTRANS(0x0898, L'Z'  , 0x1e9e, NORMAL_CHARACTER),	// ‘Z’ -> ‘ẞ’
  DEADTRANS(0x0898, L'a'  , 0xa78b, NORMAL_CHARACTER),	// ‘a’ -> ‘Ꞌ’
  DEADTRANS(0x0898, L'h'  , 0x01a9, NORMAL_CHARACTER),	// ‘h’ -> ‘Ʃ’
  DEADTRANS(0x0898, L's'  , 0x1e9e, NORMAL_CHARACTER),	// ‘s’ -> ‘ẞ’
  DEADTRANS(0x0898, L'z'  , 0x1e9e, NORMAL_CHARACTER),	// ‘z’ -> ‘ẞ’
  DEADTRANS(0x0898, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + T
  DEADTRANS(0x0899, L'^'  , 0x22a4, NORMAL_CHARACTER),	// U+005E -> U+22A4
  DEADTRANS(0x0899, L'_'  , 0x22a5, NORMAL_CHARACTER),	// U+005F -> U+22A5
  DEADTRANS(0x0899, L'h'  , 0x00de, NORMAL_CHARACTER),	// ‘h’ -> ‘Þ’
  DEADTRANS(0x0899, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + V
  DEADTRANS(0x089a, L'V'  , 0x2714, NORMAL_CHARACTER),	// ‘V’ -> U+2714
  DEADTRANS(0x089a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + W
  DEADTRANS(0x089b, L'y'  , 0x01f7, NORMAL_CHARACTER),	// ‘y’ -> ‘Ƿ’
  DEADTRANS(0x089b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + X
  DEADTRANS(0x089c, L'?'  , 0x0241, NORMAL_CHARACTER),	// U+003F -> ‘Ɂ’
  DEADTRANS(0x089c, L'X'  , 0x2718, NORMAL_CHARACTER),	// ‘X’ -> U+2718
  DEADTRANS(0x089c, L'x'  , 0x2717, NORMAL_CHARACTER),	// ‘x’ -> U+2717
  DEADTRANS(0x089c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Y
  DEADTRANS(0x089d, L'o'  , 0x021c, NORMAL_CHARACTER),	// ‘o’ -> ‘Ȝ’
  DEADTRANS(0x089d, L'r'  , 0x01a6, NORMAL_CHARACTER),	// ‘r’ -> ‘Ʀ’
  DEADTRANS(0x089d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Z
  DEADTRANS(0x089e, L'h'  , 0x01b7, NORMAL_CHARACTER),	// ‘h’ -> ‘Ʒ’
  DEADTRANS(0x089e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + [
  DEADTRANS(0x089f, L'.'  , 0x0328, NORMAL_CHARACTER),	// U+002E -> U+0328
  DEADTRANS(0x089f, L'0'  , 0x230a, NORMAL_CHARACTER),	// ‘0’ -> U+230A
  DEADTRANS(0x089f, L'A'  , 0x0104, NORMAL_CHARACTER),	// ‘A’ -> ‘Ą’
  DEADTRANS(0x089f, L'E'  , 0x0118, NORMAL_CHARACTER),	// ‘E’ -> ‘Ę’
  DEADTRANS(0x089f, L'I'  , 0x012e, NORMAL_CHARACTER),	// ‘I’ -> ‘Į’
  DEADTRANS(0x089f, L'O'  , 0x01ea, NORMAL_CHARACTER),	// ‘O’ -> ‘Ǫ’
  DEADTRANS(0x089f, L'U'  , 0x0172, NORMAL_CHARACTER),	// ‘U’ -> ‘Ų’
  DEADTRANS(0x089f, L'['  , 0x27e6, NORMAL_CHARACTER),	// U+005B -> U+27E6
  DEADTRANS(0x089f, L'a'  , 0x0105, NORMAL_CHARACTER),	// ‘a’ -> ‘ą’
  DEADTRANS(0x089f, L'c'  , 0x228f, NORMAL_CHARACTER),	// ‘c’ -> U+228F
  DEADTRANS(0x089f, L'e'  , 0x0119, NORMAL_CHARACTER),	// ‘e’ -> ‘ę’
  DEADTRANS(0x089f, L'i'  , 0x012f, NORMAL_CHARACTER),	// ‘i’ -> ‘į’
  DEADTRANS(0x089f, L'n'  , 0x0b64, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + [ + n›
  DEADTRANS(0x089f, L'o'  , 0x01eb, NORMAL_CHARACTER),	// ‘o’ -> ‘ǫ’
  DEADTRANS(0x089f, L'u'  , 0x0173, NORMAL_CHARACTER),	// ‘u’ -> ‘ų’
  DEADTRANS(0x089f, L'v'  , 0x23b4, NORMAL_CHARACTER),	// ‘v’ -> U+23B4
  DEADTRANS(0x089f, L'|'  , 0x27e6, NORMAL_CHARACTER),	// U+007C -> U+27E6
  DEADTRANS(0x089f, L' '  , 0x02db, NORMAL_CHARACTER),	// U+0020 -> U+02DB

// Dead key: Compose + U+002F
  DEADTRANS(0x08b5, L'/'  , 0x2228, NORMAL_CHARACTER),	// U+002F -> U+2228
  DEADTRANS(0x08b5, L'0'  , L'\0' , NORMAL_CHARACTER),	// ‘0’ -> U+0000
  DEADTRANS(0x08b5, L'b'  , 0x0008, NORMAL_CHARACTER),	// ‘b’ -> U+0008
  DEADTRANS(0x08b5, L'n'  , L'\n' , NORMAL_CHARACTER),	// ‘n’ -> U+000A
  DEADTRANS(0x08b5, L'r'  , L'\r' , NORMAL_CHARACTER),	// ‘r’ -> U+000D
  DEADTRANS(0x08b5, L't'  , L'\t' , NORMAL_CHARACTER),	// ‘t’ -> U+0009
  DEADTRANS(0x08b5, L'v'  , 0x000b, NORMAL_CHARACTER),	// ‘v’ -> U+000B
  DEADTRANS(0x08b5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ]
  DEADTRANS(0x08be, L'0'  , 0x230b, NORMAL_CHARACTER),	// ‘0’ -> U+230B
  DEADTRANS(0x08be, L']'  , 0x27e7, NORMAL_CHARACTER),	// U+005D -> U+27E7
  DEADTRANS(0x08be, L'c'  , 0x2290, NORMAL_CHARACTER),	// ‘c’ -> U+2290
  DEADTRANS(0x08be, L'v'  , 0x23b5, NORMAL_CHARACTER),	// ‘v’ -> U+23B5
  DEADTRANS(0x08be, L'|'  , 0x27e7, NORMAL_CHARACTER),	// U+007C -> U+27E7
  DEADTRANS(0x08be, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ^
  DEADTRANS(0x08bf, L'"'  , 0x201f, NORMAL_CHARACTER),	// U+0022 -> U+201F
  DEADTRANS(0x08bf, L'\'' , 0x201b, NORMAL_CHARACTER),	// U+0027 -> U+201B
  DEADTRANS(0x08bf, L'('  , 0x207d, NORMAL_CHARACTER),	// U+0028 -> U+207D
  DEADTRANS(0x08bf, L')'  , 0x207e, NORMAL_CHARACTER),	// U+0029 -> U+207E
  DEADTRANS(0x08bf, L'+'  , 0x207a, NORMAL_CHARACTER),	// U+002B -> U+207A
  DEADTRANS(0x08bf, L'-'  , 0x207b, NORMAL_CHARACTER),	// U+002D -> U+207B
  DEADTRANS(0x08bf, L'0'  , 0x2070, NORMAL_CHARACTER),	// ‘0’ -> ‘⁰’
  DEADTRANS(0x08bf, L'1'  , 0x00b9, NORMAL_CHARACTER),	// ‘1’ -> ‘¹’
  DEADTRANS(0x08bf, L'2'  , 0x00b2, NORMAL_CHARACTER),	// ‘2’ -> ‘²’
  DEADTRANS(0x08bf, L'3'  , 0x00b3, NORMAL_CHARACTER),	// ‘3’ -> ‘³’
  DEADTRANS(0x08bf, L'4'  , 0x2074, NORMAL_CHARACTER),	// ‘4’ -> ‘⁴’
  DEADTRANS(0x08bf, L'5'  , 0x2075, NORMAL_CHARACTER),	// ‘5’ -> ‘⁵’
  DEADTRANS(0x08bf, L'6'  , 0x2076, NORMAL_CHARACTER),	// ‘6’ -> ‘⁶’
  DEADTRANS(0x08bf, L'7'  , 0x2077, NORMAL_CHARACTER),	// ‘7’ -> ‘⁷’
  DEADTRANS(0x08bf, L'8'  , 0x2078, NORMAL_CHARACTER),	// ‘8’ -> ‘⁸’
  DEADTRANS(0x08bf, L'9'  , 0x2079, NORMAL_CHARACTER),	// ‘9’ -> ‘⁹’
  DEADTRANS(0x08bf, L'='  , 0x207c, NORMAL_CHARACTER),	// U+003D -> U+207C
  DEADTRANS(0x08bf, L'A'  , 0x1d2c, NORMAL_CHARACTER),	// ‘A’ -> ‘ᴬ’
  DEADTRANS(0x08bf, L'B'  , 0x1d2e, NORMAL_CHARACTER),	// ‘B’ -> ‘ᴮ’
  DEADTRANS(0x08bf, L'D'  , 0x1d30, NORMAL_CHARACTER),	// ‘D’ -> ‘ᴰ’
  DEADTRANS(0x08bf, L'E'  , 0x1d31, NORMAL_CHARACTER),	// ‘E’ -> ‘ᴱ’
  DEADTRANS(0x08bf, L'G'  , 0x1d33, NORMAL_CHARACTER),	// ‘G’ -> ‘ᴳ’
  DEADTRANS(0x08bf, L'H'  , 0x1d34, NORMAL_CHARACTER),	// ‘H’ -> ‘ᴴ’
  DEADTRANS(0x08bf, L'I'  , 0x1d35, NORMAL_CHARACTER),	// ‘I’ -> ‘ᴵ’
  DEADTRANS(0x08bf, L'J'  , 0x1d36, NORMAL_CHARACTER),	// ‘J’ -> ‘ᴶ’
  DEADTRANS(0x08bf, L'K'  , 0x1d37, NORMAL_CHARACTER),	// ‘K’ -> ‘ᴷ’
  DEADTRANS(0x08bf, L'L'  , 0x1d38, NORMAL_CHARACTER),	// ‘L’ -> ‘ᴸ’
  DEADTRANS(0x08bf, L'M'  , 0x1d39, NORMAL_CHARACTER),	// ‘M’ -> ‘ᴹ’
  DEADTRANS(0x08bf, L'N'  , 0x1d3a, NORMAL_CHARACTER),	// ‘N’ -> ‘ᴺ’
  DEADTRANS(0x08bf, L'O'  , 0x1d3c, NORMAL_CHARACTER),	// ‘O’ -> ‘ᴼ’
  DEADTRANS(0x08bf, L'P'  , 0x1d3e, NORMAL_CHARACTER),	// ‘P’ -> ‘ᴾ’
  DEADTRANS(0x08bf, L'R'  , 0x1d3f, NORMAL_CHARACTER),	// ‘R’ -> ‘ᴿ’
  DEADTRANS(0x08bf, L'T'  , 0x1d40, NORMAL_CHARACTER),	// ‘T’ -> ‘ᵀ’
  DEADTRANS(0x08bf, L'U'  , 0x1d41, NORMAL_CHARACTER),	// ‘U’ -> ‘ᵁ’
  DEADTRANS(0x08bf, L'W'  , 0x1d42, NORMAL_CHARACTER),	// ‘W’ -> ‘ᵂ’
  DEADTRANS(0x08bf, L'['  , 0x2e22, NORMAL_CHARACTER),	// U+005B -> U+2E22
  DEADTRANS(0x08bf, L']'  , 0x2e23, NORMAL_CHARACTER),	// U+005D -> U+2E23
  DEADTRANS(0x08bf, L'_'  , 0x0b65, CHAINED_DEAD_KEY),	// U+005F -> ‹Compose + ^ + _›
  DEADTRANS(0x08bf, L'a'  , 0x1d43, NORMAL_CHARACTER),	// ‘a’ -> ‘ᵃ’
  DEADTRANS(0x08bf, L'b'  , 0x1d47, NORMAL_CHARACTER),	// ‘b’ -> ‘ᵇ’
  DEADTRANS(0x08bf, L'c'  , 0x1d9c, NORMAL_CHARACTER),	// ‘c’ -> ‘ᶜ’
  DEADTRANS(0x08bf, L'd'  , 0x1d48, NORMAL_CHARACTER),	// ‘d’ -> ‘ᵈ’
  DEADTRANS(0x08bf, L'e'  , 0x1d49, NORMAL_CHARACTER),	// ‘e’ -> ‘ᵉ’
  DEADTRANS(0x08bf, L'f'  , 0x1da0, NORMAL_CHARACTER),	// ‘f’ -> ‘ᶠ’
  DEADTRANS(0x08bf, L'g'  , 0x1d4d, NORMAL_CHARACTER),	// ‘g’ -> ‘ᵍ’
  DEADTRANS(0x08bf, L'h'  , 0x02b0, NORMAL_CHARACTER),	// ‘h’ -> ‘ʰ’
  DEADTRANS(0x08bf, L'i'  , 0x2071, NORMAL_CHARACTER),	// ‘i’ -> ‘ⁱ’
  DEADTRANS(0x08bf, L'j'  , 0x02b2, NORMAL_CHARACTER),	// ‘j’ -> ‘ʲ’
  DEADTRANS(0x08bf, L'k'  , 0x1d4f, NORMAL_CHARACTER),	// ‘k’ -> ‘ᵏ’
  DEADTRANS(0x08bf, L'l'  , 0x02e1, NORMAL_CHARACTER),	// ‘l’ -> ‘ˡ’
  DEADTRANS(0x08bf, L'm'  , 0x1d50, NORMAL_CHARACTER),	// ‘m’ -> ‘ᵐ’
  DEADTRANS(0x08bf, L'n'  , 0x207f, NORMAL_CHARACTER),	// ‘n’ -> ‘ⁿ’
  DEADTRANS(0x08bf, L'o'  , 0x1d52, NORMAL_CHARACTER),	// ‘o’ -> ‘ᵒ’
  DEADTRANS(0x08bf, L'p'  , 0x1d56, NORMAL_CHARACTER),	// ‘p’ -> ‘ᵖ’
  DEADTRANS(0x08bf, L'r'  , 0x02b3, NORMAL_CHARACTER),	// ‘r’ -> ‘ʳ’
  DEADTRANS(0x08bf, L's'  , 0x02e2, NORMAL_CHARACTER),	// ‘s’ -> ‘ˢ’
  DEADTRANS(0x08bf, L't'  , 0x1d57, NORMAL_CHARACTER),	// ‘t’ -> ‘ᵗ’
  DEADTRANS(0x08bf, L'u'  , 0x1d58, NORMAL_CHARACTER),	// ‘u’ -> ‘ᵘ’
  DEADTRANS(0x08bf, L'v'  , 0x1d5b, NORMAL_CHARACTER),	// ‘v’ -> ‘ᵛ’
  DEADTRANS(0x08bf, L'w'  , 0x02b7, NORMAL_CHARACTER),	// ‘w’ -> ‘ʷ’
  DEADTRANS(0x08bf, L'x'  , 0x02e3, NORMAL_CHARACTER),	// ‘x’ -> ‘ˣ’
  DEADTRANS(0x08bf, L'y'  , 0x02b8, NORMAL_CHARACTER),	// ‘y’ -> ‘ʸ’
  DEADTRANS(0x08bf, L'z'  , 0x1dbb, NORMAL_CHARACTER),	// ‘z’ -> ‘ᶻ’
  DEADTRANS(0x08bf, L'|'  , 0x0b78, CHAINED_DEAD_KEY),	// U+007C -> ‹Compose + ^ + |›
  DEADTRANS(0x08bf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + _
  DEADTRANS(0x08c0, L'"'  , 0x201e, NORMAL_CHARACTER),	// U+0022 -> U+201E
  DEADTRANS(0x08c0, L'\'' , 0x201a, NORMAL_CHARACTER),	// U+0027 -> U+201A
  DEADTRANS(0x08c0, L'('  , 0x208d, NORMAL_CHARACTER),	// U+0028 -> U+208D
  DEADTRANS(0x08c0, L')'  , 0x208e, NORMAL_CHARACTER),	// U+0029 -> U+208E
  DEADTRANS(0x08c0, L'*'  , 0x204e, NORMAL_CHARACTER),	// U+002A -> U+204E
  DEADTRANS(0x08c0, L'+'  , 0x208a, NORMAL_CHARACTER),	// U+002B -> U+208A
  DEADTRANS(0x08c0, L'-'  , 0x208b, NORMAL_CHARACTER),	// U+002D -> U+208B
  DEADTRANS(0x08c0, L'0'  , 0x2080, NORMAL_CHARACTER),	// ‘0’ -> ‘₀’
  DEADTRANS(0x08c0, L'1'  , 0x2081, NORMAL_CHARACTER),	// ‘1’ -> ‘₁’
  DEADTRANS(0x08c0, L'2'  , 0x2082, NORMAL_CHARACTER),	// ‘2’ -> ‘₂’
  DEADTRANS(0x08c0, L'3'  , 0x2083, NORMAL_CHARACTER),	// ‘3’ -> ‘₃’
  DEADTRANS(0x08c0, L'4'  , 0x2084, NORMAL_CHARACTER),	// ‘4’ -> ‘₄’
  DEADTRANS(0x08c0, L'5'  , 0x2085, NORMAL_CHARACTER),	// ‘5’ -> ‘₅’
  DEADTRANS(0x08c0, L'6'  , 0x2086, NORMAL_CHARACTER),	// ‘6’ -> ‘₆’
  DEADTRANS(0x08c0, L'7'  , 0x2087, NORMAL_CHARACTER),	// ‘7’ -> ‘₇’
  DEADTRANS(0x08c0, L'8'  , 0x2088, NORMAL_CHARACTER),	// ‘8’ -> ‘₈’
  DEADTRANS(0x08c0, L'9'  , 0x2089, NORMAL_CHARACTER),	// ‘9’ -> ‘₉’
  DEADTRANS(0x08c0, L':'  , 0x0b79, CHAINED_DEAD_KEY),	// U+003A -> ‹Compose + _ + :›
  DEADTRANS(0x08c0, L'<'  , 0x0b7a, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + _ + <›
  DEADTRANS(0x08c0, L'='  , 0x208c, NORMAL_CHARACTER),	// U+003D -> U+208C
  DEADTRANS(0x08c0, L'>'  , 0x0b7b, CHAINED_DEAD_KEY),	// U+003E -> ‹Compose + _ + >›
  DEADTRANS(0x08c0, L'['  , 0x2e24, NORMAL_CHARACTER),	// U+005B -> U+2E24
  DEADTRANS(0x08c0, L']'  , 0x2e25, NORMAL_CHARACTER),	// U+005D -> U+2E25
  DEADTRANS(0x08c0, L'^'  , 0x2038, NORMAL_CHARACTER),	// U+005E -> U+2038
  DEADTRANS(0x08c0, L'`'  , 0x0b7c, CHAINED_DEAD_KEY),	// U+0060 -> ‹Compose + _ + `›
  DEADTRANS(0x08c0, L'a'  , 0x2090, NORMAL_CHARACTER),	// ‘a’ -> ‘ₐ’
  DEADTRANS(0x08c0, L'e'  , 0x2091, NORMAL_CHARACTER),	// ‘e’ -> ‘ₑ’
  DEADTRANS(0x08c0, L'h'  , 0x2095, NORMAL_CHARACTER),	// ‘h’ -> ‘ₕ’
  DEADTRANS(0x08c0, L'i'  , 0x1d62, NORMAL_CHARACTER),	// ‘i’ -> ‘ᵢ’
  DEADTRANS(0x08c0, L'j'  , 0x2c7c, NORMAL_CHARACTER),	// ‘j’ -> ‘ⱼ’
  DEADTRANS(0x08c0, L'k'  , 0x2096, NORMAL_CHARACTER),	// ‘k’ -> ‘ₖ’
  DEADTRANS(0x08c0, L'l'  , 0x2097, NORMAL_CHARACTER),	// ‘l’ -> ‘ₗ’
  DEADTRANS(0x08c0, L'm'  , 0x2098, NORMAL_CHARACTER),	// ‘m’ -> ‘ₘ’
  DEADTRANS(0x08c0, L'n'  , 0x2099, NORMAL_CHARACTER),	// ‘n’ -> ‘ₙ’
  DEADTRANS(0x08c0, L'o'  , 0x2092, NORMAL_CHARACTER),	// ‘o’ -> ‘ₒ’
  DEADTRANS(0x08c0, L'p'  , 0x209a, NORMAL_CHARACTER),	// ‘p’ -> ‘ₚ’
  DEADTRANS(0x08c0, L'r'  , 0x1d63, NORMAL_CHARACTER),	// ‘r’ -> ‘ᵣ’
  DEADTRANS(0x08c0, L's'  , 0x209b, NORMAL_CHARACTER),	// ‘s’ -> ‘ₛ’
  DEADTRANS(0x08c0, L't'  , 0x209c, NORMAL_CHARACTER),	// ‘t’ -> ‘ₜ’
  DEADTRANS(0x08c0, L'u'  , 0x1d64, NORMAL_CHARACTER),	// ‘u’ -> ‘ᵤ’
  DEADTRANS(0x08c0, L'v'  , 0x1d65, NORMAL_CHARACTER),	// ‘v’ -> ‘ᵥ’
  DEADTRANS(0x08c0, L'x'  , 0x2093, NORMAL_CHARACTER),	// ‘x’ -> ‘ₓ’
  DEADTRANS(0x08c0, L'|'  , 0x0b7d, CHAINED_DEAD_KEY),	// U+007C -> ‹Compose + _ + |›
  DEADTRANS(0x08c0, L'~'  , 0x0b7e, CHAINED_DEAD_KEY),	// U+007E -> ‹Compose + _ + ~›
  DEADTRANS(0x08c0, 0x03b9, 0x037a, NORMAL_CHARACTER),	// ‘ι’ -> ‘ͺ’
  DEADTRANS(0x08c0, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + `
  DEADTRANS(0x08c1, L'.'  , 0x0300, NORMAL_CHARACTER),	// U+002E -> U+0300
  DEADTRANS(0x08c1, L'A'  , 0x00c0, NORMAL_CHARACTER),	// ‘A’ -> ‘À’
  DEADTRANS(0x08c1, L'E'  , 0x00c8, NORMAL_CHARACTER),	// ‘E’ -> ‘È’
  DEADTRANS(0x08c1, L'I'  , 0x00cc, NORMAL_CHARACTER),	// ‘I’ -> ‘Ì’
  DEADTRANS(0x08c1, L'N'  , 0x01f8, NORMAL_CHARACTER),	// ‘N’ -> ‘Ǹ’
  DEADTRANS(0x08c1, L'O'  , 0x00d2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ò’
  DEADTRANS(0x08c1, L'U'  , 0x00d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ù’
  DEADTRANS(0x08c1, L'V'  , 0x01db, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǜ’
  DEADTRANS(0x08c1, L'W'  , 0x1e80, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẁ’
  DEADTRANS(0x08c1, L'Y'  , 0x1ef2, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỳ’
  DEADTRANS(0x08c1, L'a'  , 0x00e0, NORMAL_CHARACTER),	// ‘a’ -> ‘à’
  DEADTRANS(0x08c1, L'e'  , 0x00e8, NORMAL_CHARACTER),	// ‘e’ -> ‘è’
  DEADTRANS(0x08c1, L'i'  , 0x00ec, NORMAL_CHARACTER),	// ‘i’ -> ‘ì’
  DEADTRANS(0x08c1, L'n'  , 0x01f9, NORMAL_CHARACTER),	// ‘n’ -> ‘ǹ’
  DEADTRANS(0x08c1, L'o'  , 0x00f2, NORMAL_CHARACTER),	// ‘o’ -> ‘ò’
  DEADTRANS(0x08c1, L'u'  , 0x00f9, NORMAL_CHARACTER),	// ‘u’ -> ‘ù’
  DEADTRANS(0x08c1, L'v'  , 0x01dc, NORMAL_CHARACTER),	// ‘v’ -> ‘ǜ’
  DEADTRANS(0x08c1, L'w'  , 0x1e81, NORMAL_CHARACTER),	// ‘w’ -> ‘ẁ’
  DEADTRANS(0x08c1, L'y'  , 0x1ef3, NORMAL_CHARACTER),	// ‘y’ -> ‘ỳ’
  DEADTRANS(0x08c1, 0x00a0, L'`'  , NORMAL_CHARACTER),	// U+00A0 -> U+0060
  DEADTRANS(0x08c1, 0x00dc, 0x01db, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǜ’
  DEADTRANS(0x08c1, 0x00fc, 0x01dc, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǜ’
  DEADTRANS(0x08c1, 0x03a9, 0x1ffa, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὼ’
  DEADTRANS(0x08c1, 0x03b1, 0x1f70, NORMAL_CHARACTER),	// ‘α’ -> ‘ὰ’
  DEADTRANS(0x08c1, 0x03b5, 0x1f72, NORMAL_CHARACTER),	// ‘ε’ -> ‘ὲ’
  DEADTRANS(0x08c1, 0x03b7, 0x1f74, NORMAL_CHARACTER),	// ‘η’ -> ‘ὴ’
  DEADTRANS(0x08c1, 0x03b9, 0x1f76, NORMAL_CHARACTER),	// ‘ι’ -> ‘ὶ’
  DEADTRANS(0x08c1, 0x03bf, 0x1f78, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὸ’
  DEADTRANS(0x08c1, 0x03c5, 0x1f7a, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὺ’
  DEADTRANS(0x08c1, 0x03c9, 0x1f7c, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὼ’
  DEADTRANS(0x08c1, L'`'  , 0x030f, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Double Grave Accent›
  DEADTRANS(0x08c1, L' '  , 0x02cb, NORMAL_CHARACTER),	// U+0020 -> ‘ˋ’

// Dead key: Compose + a
  DEADTRANS(0x08c2, L'_'  , 0x1d00, NORMAL_CHARACTER),	// U+005F -> ‘ᴀ’
  DEADTRANS(0x08c2, L'a'  , 0x00aa, NORMAL_CHARACTER),	// ‘a’ -> ‘ª’
  DEADTRANS(0x08c2, L'c'  , 0x2100, NORMAL_CHARACTER),	// ‘c’ -> U+2100
  DEADTRANS(0x08c2, L'e'  , 0x00e6, NORMAL_CHARACTER),	// ‘e’ -> ‘æ’
  DEADTRANS(0x08c2, L'o'  , 0xa735, NORMAL_CHARACTER),	// ‘o’ -> ‘ꜵ’
  DEADTRANS(0x08c2, L's'  , 0x2101, NORMAL_CHARACTER),	// ‘s’ -> U+2101
  DEADTRANS(0x08c2, L'u'  , 0xa737, NORMAL_CHARACTER),	// ‘u’ -> ‘ꜷ’
  DEADTRANS(0x08c2, L'v'  , 0xa739, NORMAL_CHARACTER),	// ‘v’ -> ‘ꜹ’
  DEADTRANS(0x08c2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + b
  DEADTRANS(0x08c3, L'_'  , 0x0299, NORMAL_CHARACTER),	// U+005F -> ‘ʙ’
  DEADTRANS(0x08c3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + c
  DEADTRANS(0x08c4, L','  , 0x2e34, NORMAL_CHARACTER),	// U+002C -> U+2E34
  DEADTRANS(0x08c4, L'.'  , 0x00b7, NORMAL_CHARACTER),	// U+002E -> U+00B7
  DEADTRANS(0x08c4, L'_'  , 0x1d04, NORMAL_CHARACTER),	// U+005F -> ‘ᴄ’
  DEADTRANS(0x08c4, L'h'  , 0xe03b, NORMAL_CHARACTER),	// ‘h’ -> U+E03B
  DEADTRANS(0x08c4, L'k'  , 0xe03a, NORMAL_CHARACTER),	// ‘k’ -> U+E03A
  DEADTRANS(0x08c4, L'm'  , 0x339d, NORMAL_CHARACTER),	// ‘m’ -> U+339D
  DEADTRANS(0x08c4, L'o'  , 0x2105, NORMAL_CHARACTER),	// ‘o’ -> U+2105
  DEADTRANS(0x08c4, L't'  , 0xe03d, NORMAL_CHARACTER),	// ‘t’ -> U+E03D
  DEADTRANS(0x08c4, L'u'  , 0x2106, NORMAL_CHARACTER),	// ‘u’ -> U+2106
  DEADTRANS(0x08c4, 0x2026, 0x22ef, NORMAL_CHARACTER),	// U+2026 -> U+22EF
  DEADTRANS(0x08c4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + d
  DEADTRANS(0x08c5, L'B'  , 0x33c8, NORMAL_CHARACTER),	// ‘B’ -> U+33C8
  DEADTRANS(0x08c5, L'_'  , 0x1d05, NORMAL_CHARACTER),	// U+005F -> ‘ᴅ’
  DEADTRANS(0x08c5, L'l'  , 0x3397, NORMAL_CHARACTER),	// ‘l’ -> U+3397
  DEADTRANS(0x08c5, L'm'  , 0x3377, NORMAL_CHARACTER),	// ‘m’ -> U+3377
  DEADTRANS(0x08c5, L'z'  , 0x01f3, NORMAL_CHARACTER),	// ‘z’ -> ‘ǳ’
  DEADTRANS(0x08c5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + e
  DEADTRANS(0x08c6, L'_'  , 0x1d07, NORMAL_CHARACTER),	// U+005F -> ‘ᴇ’
  DEADTRANS(0x08c6, L'e'  , 0x212e, NORMAL_CHARACTER),	// ‘e’ -> U+212E
  DEADTRANS(0x08c6, L't'  , 0xa76b, NORMAL_CHARACTER),	// ‘t’ -> ‘ꝫ’
  DEADTRANS(0x08c6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + f
  DEADTRANS(0x08c7, L'_'  , 0xa730, NORMAL_CHARACTER),	// U+005F -> ‘ꜰ’
  DEADTRANS(0x08c7, L'a'  , 0x0b7f, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + f + a›
  DEADTRANS(0x08c7, L'b'  , 0xe030, NORMAL_CHARACTER),	// ‘b’ -> U+E030
  DEADTRANS(0x08c7, L'f'  , 0xfb00, NORMAL_CHARACTER),	// ‘f’ -> ‘ﬀ’
  DEADTRANS(0x08c7, L'h'  , 0xe036, NORMAL_CHARACTER),	// ‘h’ -> U+E036
  DEADTRANS(0x08c7, L'i'  , 0xfb01, NORMAL_CHARACTER),	// ‘i’ -> ‘ﬁ’
  DEADTRANS(0x08c7, L'j'  , 0xe037, NORMAL_CHARACTER),	// ‘j’ -> U+E037
  DEADTRANS(0x08c7, L'k'  , 0xe038, NORMAL_CHARACTER),	// ‘k’ -> U+E038
  DEADTRANS(0x08c7, L'l'  , 0xfb02, NORMAL_CHARACTER),	// ‘l’ -> ‘ﬂ’
  DEADTRANS(0x08c7, L'm'  , 0x3399, NORMAL_CHARACTER),	// ‘m’ -> U+3399
  DEADTRANS(0x08c7, L's'  , 0x017f, NORMAL_CHARACTER),	// ‘s’ -> ‘ſ’
  DEADTRANS(0x08c7, L't'  , 0xfb05, NORMAL_CHARACTER),	// ‘t’ -> ‘ﬅ’
  DEADTRANS(0x08c7, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + g
  DEADTRANS(0x08c8, L'A'  , 0x0391, NORMAL_CHARACTER),	// ‘A’ -> ‘Α’
  DEADTRANS(0x08c8, L'B'  , 0x0392, NORMAL_CHARACTER),	// ‘B’ -> ‘Β’
  DEADTRANS(0x08c8, L'C'  , 0x03a8, NORMAL_CHARACTER),	// ‘C’ -> ‘Ψ’
  DEADTRANS(0x08c8, L'D'  , 0x0394, NORMAL_CHARACTER),	// ‘D’ -> ‘Δ’
  DEADTRANS(0x08c8, L'E'  , 0x0395, NORMAL_CHARACTER),	// ‘E’ -> ‘Ε’
  DEADTRANS(0x08c8, L'F'  , 0x03a6, NORMAL_CHARACTER),	// ‘F’ -> ‘Φ’
  DEADTRANS(0x08c8, L'G'  , 0x0393, NORMAL_CHARACTER),	// ‘G’ -> ‘Γ’
  DEADTRANS(0x08c8, L'H'  , 0x0397, NORMAL_CHARACTER),	// ‘H’ -> ‘Η’
  DEADTRANS(0x08c8, L'I'  , 0x0399, NORMAL_CHARACTER),	// ‘I’ -> ‘Ι’
  DEADTRANS(0x08c8, L'J'  , 0x039e, NORMAL_CHARACTER),	// ‘J’ -> ‘Ξ’
  DEADTRANS(0x08c8, L'K'  , 0x039a, NORMAL_CHARACTER),	// ‘K’ -> ‘Κ’
  DEADTRANS(0x08c8, L'L'  , 0x039b, NORMAL_CHARACTER),	// ‘L’ -> ‘Λ’
  DEADTRANS(0x08c8, L'M'  , 0x039c, NORMAL_CHARACTER),	// ‘M’ -> ‘Μ’
  DEADTRANS(0x08c8, L'N'  , 0x039d, NORMAL_CHARACTER),	// ‘N’ -> ‘Ν’
  DEADTRANS(0x08c8, L'O'  , 0x039f, NORMAL_CHARACTER),	// ‘O’ -> ‘Ο’
  DEADTRANS(0x08c8, L'P'  , 0x03a0, NORMAL_CHARACTER),	// ‘P’ -> ‘Π’
  DEADTRANS(0x08c8, L'R'  , 0x03a1, NORMAL_CHARACTER),	// ‘R’ -> ‘Ρ’
  DEADTRANS(0x08c8, L'S'  , 0x03a3, NORMAL_CHARACTER),	// ‘S’ -> ‘Σ’
  DEADTRANS(0x08c8, L'T'  , 0x03a4, NORMAL_CHARACTER),	// ‘T’ -> ‘Τ’
  DEADTRANS(0x08c8, L'U'  , 0x0398, NORMAL_CHARACTER),	// ‘U’ -> ‘Θ’
  DEADTRANS(0x08c8, L'V'  , 0x03a9, NORMAL_CHARACTER),	// ‘V’ -> ‘Ω’
  DEADTRANS(0x08c8, L'X'  , 0x03a7, NORMAL_CHARACTER),	// ‘X’ -> ‘Χ’
  DEADTRANS(0x08c8, L'Y'  , 0x03a5, NORMAL_CHARACTER),	// ‘Y’ -> ‘Υ’
  DEADTRANS(0x08c8, L'Z'  , 0x0396, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ζ’
  DEADTRANS(0x08c8, L'_'  , 0x0262, NORMAL_CHARACTER),	// U+005F -> ‘ɢ’
  DEADTRANS(0x08c8, L'a'  , 0x03b1, NORMAL_CHARACTER),	// ‘a’ -> ‘α’
  DEADTRANS(0x08c8, L'b'  , 0x03b2, NORMAL_CHARACTER),	// ‘b’ -> ‘β’
  DEADTRANS(0x08c8, L'c'  , 0x03c8, NORMAL_CHARACTER),	// ‘c’ -> ‘ψ’
  DEADTRANS(0x08c8, L'd'  , 0x03b4, NORMAL_CHARACTER),	// ‘d’ -> ‘δ’
  DEADTRANS(0x08c8, L'e'  , 0x03b5, NORMAL_CHARACTER),	// ‘e’ -> ‘ε’
  DEADTRANS(0x08c8, L'f'  , 0x03c6, NORMAL_CHARACTER),	// ‘f’ -> ‘φ’
  DEADTRANS(0x08c8, L'g'  , 0x03b3, NORMAL_CHARACTER),	// ‘g’ -> ‘γ’
  DEADTRANS(0x08c8, L'h'  , 0x03b7, NORMAL_CHARACTER),	// ‘h’ -> ‘η’
  DEADTRANS(0x08c8, L'i'  , 0x03b9, NORMAL_CHARACTER),	// ‘i’ -> ‘ι’
  DEADTRANS(0x08c8, L'j'  , 0x03be, NORMAL_CHARACTER),	// ‘j’ -> ‘ξ’
  DEADTRANS(0x08c8, L'k'  , 0x03ba, NORMAL_CHARACTER),	// ‘k’ -> ‘κ’
  DEADTRANS(0x08c8, L'l'  , 0x03bb, NORMAL_CHARACTER),	// ‘l’ -> ‘λ’
  DEADTRANS(0x08c8, L'm'  , 0x03bc, NORMAL_CHARACTER),	// ‘m’ -> ‘μ’
  DEADTRANS(0x08c8, L'n'  , 0x03bd, NORMAL_CHARACTER),	// ‘n’ -> ‘ν’
  DEADTRANS(0x08c8, L'o'  , 0x03bf, NORMAL_CHARACTER),	// ‘o’ -> ‘ο’
  DEADTRANS(0x08c8, L'p'  , 0x03c0, NORMAL_CHARACTER),	// ‘p’ -> ‘π’
  DEADTRANS(0x08c8, L'r'  , 0x03c1, NORMAL_CHARACTER),	// ‘r’ -> ‘ρ’
  DEADTRANS(0x08c8, L's'  , 0x03c3, NORMAL_CHARACTER),	// ‘s’ -> ‘σ’
  DEADTRANS(0x08c8, L't'  , 0x03c4, NORMAL_CHARACTER),	// ‘t’ -> ‘τ’
  DEADTRANS(0x08c8, L'u'  , 0x03b8, NORMAL_CHARACTER),	// ‘u’ -> ‘θ’
  DEADTRANS(0x08c8, L'v'  , 0x03c9, NORMAL_CHARACTER),	// ‘v’ -> ‘ω’
  DEADTRANS(0x08c8, L'w'  , 0x03c2, NORMAL_CHARACTER),	// ‘w’ -> ‘ς’
  DEADTRANS(0x08c8, L'x'  , 0x03c7, NORMAL_CHARACTER),	// ‘x’ -> ‘χ’
  DEADTRANS(0x08c8, L'y'  , 0x03c5, NORMAL_CHARACTER),	// ‘y’ -> ‘υ’
  DEADTRANS(0x08c8, L'z'  , 0x03b6, NORMAL_CHARACTER),	// ‘z’ -> ‘ζ’
  DEADTRANS(0x08c8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + h
  DEADTRANS(0x08c9, L'_'  , 0x029c, NORMAL_CHARACTER),	// U+005F -> ‘ʜ’
  DEADTRANS(0x08c9, L'v'  , 0x0195, NORMAL_CHARACTER),	// ‘v’ -> ‘ƕ’
  DEADTRANS(0x08c9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + i
  DEADTRANS(0x08ca, L'.'  , 0x0131, NORMAL_CHARACTER),	// U+002E -> ‘ı’
  DEADTRANS(0x08ca, L'0'  , 0x2179, NORMAL_CHARACTER),	// ‘0’ -> ‘ⅹ’
  DEADTRANS(0x08ca, L'1'  , 0x2170, NORMAL_CHARACTER),	// ‘1’ -> ‘ⅰ’
  DEADTRANS(0x08ca, L'5'  , 0x2174, NORMAL_CHARACTER),	// ‘5’ -> ‘ⅴ’
  DEADTRANS(0x08ca, L'C'  , 0x216d, NORMAL_CHARACTER),	// ‘C’ -> ‘Ⅽ’
  DEADTRANS(0x08ca, L'D'  , 0x216e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ⅾ’
  DEADTRANS(0x08ca, L'I'  , 0x2160, NORMAL_CHARACTER),	// ‘I’ -> ‘Ⅰ’
  DEADTRANS(0x08ca, L'L'  , 0x216c, NORMAL_CHARACTER),	// ‘L’ -> ‘Ⅼ’
  DEADTRANS(0x08ca, L'M'  , 0x216f, NORMAL_CHARACTER),	// ‘M’ -> ‘Ⅿ’
  DEADTRANS(0x08ca, L'V'  , 0x2164, NORMAL_CHARACTER),	// ‘V’ -> ‘Ⅴ’
  DEADTRANS(0x08ca, L'X'  , 0x2169, NORMAL_CHARACTER),	// ‘X’ -> ‘Ⅹ’
  DEADTRANS(0x08ca, L'_'  , 0x026a, NORMAL_CHARACTER),	// U+005F -> ‘ɪ’
  DEADTRANS(0x08ca, L'c'  , 0x217d, NORMAL_CHARACTER),	// ‘c’ -> ‘ⅽ’
  DEADTRANS(0x08ca, L'd'  , 0x217e, NORMAL_CHARACTER),	// ‘d’ -> ‘ⅾ’
  DEADTRANS(0x08ca, L'i'  , 0x2170, NORMAL_CHARACTER),	// ‘i’ -> ‘ⅰ’
  DEADTRANS(0x08ca, L'j'  , 0x0133, NORMAL_CHARACTER),	// ‘j’ -> ‘ĳ’
  DEADTRANS(0x08ca, L'l'  , 0x217c, NORMAL_CHARACTER),	// ‘l’ -> ‘ⅼ’
  DEADTRANS(0x08ca, L'm'  , 0x217f, NORMAL_CHARACTER),	// ‘m’ -> ‘ⅿ’
  DEADTRANS(0x08ca, L'v'  , 0x2174, NORMAL_CHARACTER),	// ‘v’ -> ‘ⅴ’
  DEADTRANS(0x08ca, L'x'  , 0x2179, NORMAL_CHARACTER),	// ‘x’ -> ‘ⅹ’
  DEADTRANS(0x08ca, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + j
  DEADTRANS(0x08cb, L'.'  , 0x0237, NORMAL_CHARACTER),	// U+002E -> ‘ȷ’
  DEADTRANS(0x08cb, L'_'  , 0x1d0a, NORMAL_CHARACTER),	// U+005F -> ‘ᴊ’
  DEADTRANS(0x08cb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + k
  DEADTRANS(0x08cc, L'B'  , 0x3385, NORMAL_CHARACTER),	// ‘B’ -> U+3385
  DEADTRANS(0x08cc, L'_'  , 0x1d0b, NORMAL_CHARACTER),	// U+005F -> ‘ᴋ’
  DEADTRANS(0x08cc, L'g'  , 0x338f, NORMAL_CHARACTER),	// ‘g’ -> U+338F
  DEADTRANS(0x08cc, L'l'  , 0x3398, NORMAL_CHARACTER),	// ‘l’ -> U+3398
  DEADTRANS(0x08cc, L'm'  , 0x339e, NORMAL_CHARACTER),	// ‘m’ -> U+339E
  DEADTRANS(0x08cc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + l
  DEADTRANS(0x08cd, L'_'  , 0x029f, NORMAL_CHARACTER),	// U+005F -> ‘ʟ’
  DEADTRANS(0x08cd, L'b'  , 0x2114, NORMAL_CHARACTER),	// ‘b’ -> U+2114
  DEADTRANS(0x08cd, L'j'  , 0x01c9, NORMAL_CHARACTER),	// ‘j’ -> ‘ǉ’
  DEADTRANS(0x08cd, L'l'  , 0x2113, NORMAL_CHARACTER),	// ‘l’ -> ‘ℓ’
  DEADTRANS(0x08cd, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + m
  DEADTRANS(0x08ce, L'*'  , 0x204e, NORMAL_CHARACTER),	// U+002A -> U+204E
  DEADTRANS(0x08ce, L'2'  , 0x33a1, NORMAL_CHARACTER),	// ‘2’ -> U+33A1
  DEADTRANS(0x08ce, L'3'  , 0x0190, NORMAL_CHARACTER),	// ‘3’ -> ‘Ɛ’
  DEADTRANS(0x08ce, L';'  , 0x204f, NORMAL_CHARACTER),	// U+003B -> U+204F
  DEADTRANS(0x08ce, L'?'  , 0x2e2e, NORMAL_CHARACTER),	// U+003F -> U+2E2E
  DEADTRANS(0x08ce, L'A'  , 0x2c6f, NORMAL_CHARACTER),	// ‘A’ -> ‘Ɐ’
  DEADTRANS(0x08ce, L'C'  , 0x0186, NORMAL_CHARACTER),	// ‘C’ -> ‘Ɔ’
  DEADTRANS(0x08ce, L'E'  , 0x018e, NORMAL_CHARACTER),	// ‘E’ -> ‘Ǝ’
  DEADTRANS(0x08ce, L'K'  , 0xa7b0, NORMAL_CHARACTER),	// ‘K’ -> ‘Ʞ’
  DEADTRANS(0x08ce, L'P'  , 0xa7fc, NORMAL_CHARACTER),	// ‘P’ -> ‘ꟼ’
  DEADTRANS(0x08ce, L'R'  , 0x1d19, NORMAL_CHARACTER),	// ‘R’ -> ‘ᴙ’
  DEADTRANS(0x08ce, L'S'  , 0x01a7, NORMAL_CHARACTER),	// ‘S’ -> ‘Ƨ’
  DEADTRANS(0x08ce, L'T'  , 0xa7b1, NORMAL_CHARACTER),	// ‘T’ -> ‘Ʇ’
  DEADTRANS(0x08ce, L'V'  , 0x0245, NORMAL_CHARACTER),	// ‘V’ -> ‘Ʌ’
  DEADTRANS(0x08ce, L'_'  , 0x1d0d, NORMAL_CHARACTER),	// U+005F -> ‘ᴍ’
  DEADTRANS(0x08ce, L'c'  , 0x0254, NORMAL_CHARACTER),	// ‘c’ -> ‘ɔ’
  DEADTRANS(0x08ce, L'g'  , 0x338e, NORMAL_CHARACTER),	// ‘g’ -> U+338E
  DEADTRANS(0x08ce, L'l'  , 0x3396, NORMAL_CHARACTER),	// ‘l’ -> U+3396
  DEADTRANS(0x08ce, L'm'  , 0x339c, NORMAL_CHARACTER),	// ‘m’ -> U+339C
  DEADTRANS(0x08ce, L's'  , 0x01a8, NORMAL_CHARACTER),	// ‘s’ -> ‘ƨ’
  DEADTRANS(0x08ce, L'u'  , 0x00b5, NORMAL_CHARACTER),	// ‘u’ -> ‘µ’
  DEADTRANS(0x08ce, 0x2019, 0x201b, NORMAL_CHARACTER),	// U+2019 -> U+201B
  DEADTRANS(0x08ce, 0x201d, 0x201f, NORMAL_CHARACTER),	// U+201D -> U+201F
  DEADTRANS(0x08ce, 0x2208, 0x220b, NORMAL_CHARACTER),	// U+2208 -> U+220B
  DEADTRANS(0x08ce, 0x2282, 0x2283, NORMAL_CHARACTER),	// U+2282 -> U+2283
  DEADTRANS(0x08ce, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + n
  DEADTRANS(0x08cf, L'*'  , 0x220f, NORMAL_CHARACTER),	// U+002A -> U+220F
  DEADTRANS(0x08cf, L'+'  , 0x2211, NORMAL_CHARACTER),	// U+002B -> U+2211
  DEADTRANS(0x08cf, L'_'  , 0x0274, NORMAL_CHARACTER),	// U+005F -> ‘ɴ’
  DEADTRANS(0x08cf, L'j'  , 0x01cc, NORMAL_CHARACTER),	// ‘j’ -> ‘ǌ’
  DEADTRANS(0x08cf, L'm'  , 0x339a, NORMAL_CHARACTER),	// ‘m’ -> U+339A
  DEADTRANS(0x08cf, L'o'  , 0x2116, NORMAL_CHARACTER),	// ‘o’ -> U+2116
  DEADTRANS(0x08cf, L'x'  , 0x2a09, NORMAL_CHARACTER),	// ‘x’ -> U+2A09
  DEADTRANS(0x08cf, 0x00b0, 0x2116, NORMAL_CHARACTER),	// U+00B0 -> U+2116
  DEADTRANS(0x08cf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + o
  DEADTRANS(0x08d0, L'.'  , 0x25cc, NORMAL_CHARACTER),	// U+002E -> U+25CC
  DEADTRANS(0x08d0, L'/'  , 0x2300, NORMAL_CHARACTER),	// U+002F -> U+2300
  DEADTRANS(0x08d0, L'_'  , 0x1d0f, NORMAL_CHARACTER),	// U+005F -> ‘ᴏ’
  DEADTRANS(0x08d0, L'c'  , 0x2103, NORMAL_CHARACTER),	// ‘c’ -> U+2103
  DEADTRANS(0x08d0, L'e'  , 0x0153, NORMAL_CHARACTER),	// ‘e’ -> ‘œ’
  DEADTRANS(0x08d0, L'f'  , 0x2109, NORMAL_CHARACTER),	// ‘f’ -> U+2109
  DEADTRANS(0x08d0, L'o'  , 0x2218, NORMAL_CHARACTER),	// ‘o’ -> U+2218
  DEADTRANS(0x08d0, L'u'  , 0x0223, NORMAL_CHARACTER),	// ‘u’ -> ‘ȣ’
  DEADTRANS(0x08d0, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + p
  DEADTRANS(0x08d1, L'_'  , 0x1d18, NORMAL_CHARACTER),	// U+005F -> ‘ᴘ’
  DEADTRANS(0x08d1, L'e'  , 0x0b80, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + p + e›
  DEADTRANS(0x08d1, L'p'  , 0x0b81, CHAINED_DEAD_KEY),	// ‘p’ -> ‹Compose + p + p›
  DEADTRANS(0x08d1, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + q
  DEADTRANS(0x08d2, L'_'  , 0xa7af, NORMAL_CHARACTER),	// U+005F -> ‘ꞯ’
  DEADTRANS(0x08d2, L'e'  , 0x0b84, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + q + e›
  DEADTRANS(0x08d2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + r
  DEADTRANS(0x0984, L'_'  , 0x0280, NORMAL_CHARACTER),	// U+005F -> ‘ʀ’
  DEADTRANS(0x0984, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + s
  DEADTRANS(0x098d, L'_'  , 0xa731, NORMAL_CHARACTER),	// U+005F -> ‘ꜱ’
  DEADTRANS(0x098d, L'a'  , 0xa78c, NORMAL_CHARACTER),	// ‘a’ -> ‘ꞌ’
  DEADTRANS(0x098d, L'h'  , 0x0283, NORMAL_CHARACTER),	// ‘h’ -> ‘ʃ’
  DEADTRANS(0x098d, L'm'  , 0x2120, NORMAL_CHARACTER),	// ‘m’ -> U+2120
  DEADTRANS(0x098d, L's'  , 0x00df, NORMAL_CHARACTER),	// ‘s’ -> ‘ß’
  DEADTRANS(0x098d, L't'  , 0xfb06, NORMAL_CHARACTER),	// ‘t’ -> ‘ﬆ’
  DEADTRANS(0x098d, L'z'  , 0x00df, NORMAL_CHARACTER),	// ‘z’ -> ‘ß’
  DEADTRANS(0x098d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + t
  DEADTRANS(0x098e, L'_'  , 0x1d1b, NORMAL_CHARACTER),	// U+005F -> ‘ᴛ’
  DEADTRANS(0x098e, L'e'  , 0x0b8b, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + t + e›
  DEADTRANS(0x098e, L'h'  , 0x00fe, NORMAL_CHARACTER),	// ‘h’ -> ‘þ’
  DEADTRANS(0x098e, L'm'  , 0x2122, NORMAL_CHARACTER),	// ‘m’ -> U+2122
  DEADTRANS(0x098e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + u
  DEADTRANS(0x0991, L'_'  , 0x1d1c, NORMAL_CHARACTER),	// U+005F -> ‘ᴜ’
  DEADTRANS(0x0991, L'g'  , 0x338d, NORMAL_CHARACTER),	// ‘g’ -> U+338D
  DEADTRANS(0x0991, L'l'  , 0x3395, NORMAL_CHARACTER),	// ‘l’ -> U+3395
  DEADTRANS(0x0991, L'm'  , 0x339b, NORMAL_CHARACTER),	// ‘m’ -> U+339B
  DEADTRANS(0x0991, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + v
  DEADTRANS(0x0992, L'('  , 0xfe35, NORMAL_CHARACTER),	// U+0028 -> U+FE35
  DEADTRANS(0x0992, L')'  , 0xfe36, NORMAL_CHARACTER),	// U+0029 -> U+FE36
  DEADTRANS(0x0992, L'['  , 0xfe39, NORMAL_CHARACTER),	// U+005B -> U+FE39
  DEADTRANS(0x0992, L']'  , 0xfe3a, NORMAL_CHARACTER),	// U+005D -> U+FE3A
  DEADTRANS(0x0992, L'_'  , 0x1d20, NORMAL_CHARACTER),	// U+005F -> ‘ᴠ’
  DEADTRANS(0x0992, L'v'  , 0x2713, NORMAL_CHARACTER),	// ‘v’ -> U+2713
  DEADTRANS(0x0992, L'{'  , 0xfe37, NORMAL_CHARACTER),	// U+007B -> U+FE37
  DEADTRANS(0x0992, L'}'  , 0xfe38, NORMAL_CHARACTER),	// U+007D -> U+FE38
  DEADTRANS(0x0992, L'~'  , 0x2e2f, NORMAL_CHARACTER),	// U+007E -> ‘ⸯ’
  DEADTRANS(0x0992, 0x2026, 0x22ee, NORMAL_CHARACTER),	// U+2026 -> U+22EE
  DEADTRANS(0x0992, 0x27e8, 0xfe3f, NORMAL_CHARACTER),	// U+27E8 -> U+FE3F
  DEADTRANS(0x0992, 0x27e9, 0xfe40, NORMAL_CHARACTER),	// U+27E9 -> U+FE40
  DEADTRANS(0x0992, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + w
  DEADTRANS(0x09a9, L'*'  , 0x0b8c, CHAINED_DEAD_KEY),	// U+002A -> ‹Compose + w + *›
  DEADTRANS(0x09a9, L'<'  , 0x21dc, NORMAL_CHARACTER),	// U+003C -> U+21DC
  DEADTRANS(0x09a9, L'>'  , 0x21dd, NORMAL_CHARACTER),	// U+003E -> U+21DD
  DEADTRANS(0x09a9, L'_'  , 0x1d21, NORMAL_CHARACTER),	// U+005F -> ‘ᴡ’
  DEADTRANS(0x09a9, L'y'  , 0x01bf, NORMAL_CHARACTER),	// ‘y’ -> ‘ƿ’
  DEADTRANS(0x09a9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + x
  DEADTRANS(0x09b1, L'?'  , 0x0242, NORMAL_CHARACTER),	// U+003F -> ‘ɂ’
  DEADTRANS(0x09b1, L'x'  , 0x00d7, NORMAL_CHARACTER),	// ‘x’ -> U+00D7
  DEADTRANS(0x09b1, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + y
  DEADTRANS(0x09b3, L'_'  , 0x028f, NORMAL_CHARACTER),	// U+005F -> ‘ʏ’
  DEADTRANS(0x09b3, L'o'  , 0x021d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȝ’
  DEADTRANS(0x09b3, L'r'  , 0x0280, NORMAL_CHARACTER),	// ‘r’ -> ‘ʀ’
  DEADTRANS(0x09b3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + z
  DEADTRANS(0x09b4, L'_'  , 0x1d22, NORMAL_CHARACTER),	// U+005F -> ‘ᴢ’
  DEADTRANS(0x09b4, L'h'  , 0x0292, NORMAL_CHARACTER),	// ‘h’ -> ‘ʒ’
  DEADTRANS(0x09b4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + {
  DEADTRANS(0x09b5, L'c'  , 0x2282, NORMAL_CHARACTER),	// ‘c’ -> U+2282
  DEADTRANS(0x09b5, L'i'  , 0x2229, NORMAL_CHARACTER),	// ‘i’ -> U+2229
  DEADTRANS(0x09b5, L'n'  , 0x0b91, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + { + n›
  DEADTRANS(0x09b5, L'u'  , 0x222a, NORMAL_CHARACTER),	// ‘u’ -> U+222A
  DEADTRANS(0x09b5, L'v'  , 0x23de, NORMAL_CHARACTER),	// ‘v’ -> U+23DE
  DEADTRANS(0x09b5, L'{'  , 0x2983, NORMAL_CHARACTER),	// U+007B -> U+2983
  DEADTRANS(0x09b5, L'|'  , 0x2983, NORMAL_CHARACTER),	// U+007C -> U+2983
  DEADTRANS(0x09b5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + |
  DEADTRANS(0x09ba, L'-'  , 0x22a2, NORMAL_CHARACTER),	// U+002D -> U+22A2
  DEADTRANS(0x09ba, L'.'  , 0x0304, NORMAL_CHARACTER),	// U+002E -> U+0304
  DEADTRANS(0x09ba, L'A'  , 0x0100, NORMAL_CHARACTER),	// ‘A’ -> ‘Ā’
  DEADTRANS(0x09ba, L'E'  , 0x0112, NORMAL_CHARACTER),	// ‘E’ -> ‘Ē’
  DEADTRANS(0x09ba, L'G'  , 0x1e20, NORMAL_CHARACTER),	// ‘G’ -> ‘Ḡ’
  DEADTRANS(0x09ba, L'I'  , 0x012a, NORMAL_CHARACTER),	// ‘I’ -> ‘Ī’
  DEADTRANS(0x09ba, L'O'  , 0x014c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ō’
  DEADTRANS(0x09ba, L'U'  , 0x016a, NORMAL_CHARACTER),	// ‘U’ -> ‘Ū’
  DEADTRANS(0x09ba, L'V'  , 0x01d5, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǖ’
  DEADTRANS(0x09ba, L'Y'  , 0x0232, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ȳ’
  DEADTRANS(0x09ba, L'a'  , 0x0101, NORMAL_CHARACTER),	// ‘a’ -> ‘ā’
  DEADTRANS(0x09ba, L'e'  , 0x0113, NORMAL_CHARACTER),	// ‘e’ -> ‘ē’
  DEADTRANS(0x09ba, L'g'  , 0x1e21, NORMAL_CHARACTER),	// ‘g’ -> ‘ḡ’
  DEADTRANS(0x09ba, L'i'  , 0x012b, NORMAL_CHARACTER),	// ‘i’ -> ‘ī’
  DEADTRANS(0x09ba, L'o'  , 0x014d, NORMAL_CHARACTER),	// ‘o’ -> ‘ō’
  DEADTRANS(0x09ba, L'u'  , 0x016b, NORMAL_CHARACTER),	// ‘u’ -> ‘ū’
  DEADTRANS(0x09ba, L'v'  , 0x01d6, NORMAL_CHARACTER),	// ‘v’ -> ‘ǖ’
  DEADTRANS(0x09ba, L'y'  , 0x0233, NORMAL_CHARACTER),	// ‘y’ -> ‘ȳ’
  DEADTRANS(0x09ba, L'|'  , 0x2016, NORMAL_CHARACTER),	// U+007C -> U+2016
  DEADTRANS(0x09ba, 0x00a0, 0x00af, NORMAL_CHARACTER),	// U+00A0 -> U+00AF
  DEADTRANS(0x09ba, 0x00c4, 0x01de, NORMAL_CHARACTER),	// ‘Ä’ -> ‘Ǟ’
  DEADTRANS(0x09ba, 0x00d6, 0x022a, NORMAL_CHARACTER),	// ‘Ö’ -> ‘Ȫ’
  DEADTRANS(0x09ba, 0x00dc, 0x01d5, NORMAL_CHARACTER),	// ‘Ü’ -> ‘Ǖ’
  DEADTRANS(0x09ba, 0x00e4, 0x01df, NORMAL_CHARACTER),	// ‘ä’ -> ‘ǟ’
  DEADTRANS(0x09ba, 0x00f6, 0x022b, NORMAL_CHARACTER),	// ‘ö’ -> ‘ȫ’
  DEADTRANS(0x09ba, 0x00fc, 0x01d6, NORMAL_CHARACTER),	// ‘ü’ -> ‘ǖ’
  DEADTRANS(0x09ba, 0x03b1, 0x1fb1, NORMAL_CHARACTER),	// ‘α’ -> ‘ᾱ’
  DEADTRANS(0x09ba, 0x03b9, 0x1fd1, NORMAL_CHARACTER),	// ‘ι’ -> ‘ῑ’
  DEADTRANS(0x09ba, 0x03c5, 0x1fe1, NORMAL_CHARACTER),	// ‘υ’ -> ‘ῡ’
  DEADTRANS(0x09ba, L' '  , 0x02c9, NORMAL_CHARACTER),	// U+0020 -> ‘ˉ’

// Dead key: Compose + }
  DEADTRANS(0x09bb, L'c'  , 0x2283, NORMAL_CHARACTER),	// ‘c’ -> U+2283
  DEADTRANS(0x09bb, L'v'  , 0x23df, NORMAL_CHARACTER),	// ‘v’ -> U+23DF
  DEADTRANS(0x09bb, L'|'  , 0x2984, NORMAL_CHARACTER),	// U+007C -> U+2984
  DEADTRANS(0x09bb, L'}'  , 0x2984, NORMAL_CHARACTER),	// U+007D -> U+2984
  DEADTRANS(0x09bb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ~
  DEADTRANS(0x09c5, L'-'  , 0x2243, NORMAL_CHARACTER),	// U+002D -> U+2243
  DEADTRANS(0x09c5, L'.'  , 0x0303, NORMAL_CHARACTER),	// U+002E -> U+0303
  DEADTRANS(0x09c5, L'<'  , 0x2a9d, NORMAL_CHARACTER),	// U+003C -> U+2A9D
  DEADTRANS(0x09c5, L'='  , 0x2245, NORMAL_CHARACTER),	// U+003D -> U+2245
  DEADTRANS(0x09c5, L'>'  , 0x2a9e, NORMAL_CHARACTER),	// U+003E -> U+2A9E
  DEADTRANS(0x09c5, L'A'  , 0x00c3, NORMAL_CHARACTER),	// ‘A’ -> ‘Ã’
  DEADTRANS(0x09c5, L'E'  , 0x1ebc, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẽ’
  DEADTRANS(0x09c5, L'I'  , 0x0128, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĩ’
  DEADTRANS(0x09c5, L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(0x09c5, L'O'  , 0x00d5, NORMAL_CHARACTER),	// ‘O’ -> ‘Õ’
  DEADTRANS(0x09c5, L'U'  , 0x0168, NORMAL_CHARACTER),	// ‘U’ -> ‘Ũ’
  DEADTRANS(0x09c5, L'V'  , 0x1e7c, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṽ’
  DEADTRANS(0x09c5, L'Y'  , 0x1ef8, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỹ’
  DEADTRANS(0x09c5, L'a'  , 0x00e3, NORMAL_CHARACTER),	// ‘a’ -> ‘ã’
  DEADTRANS(0x09c5, L'e'  , 0x1ebd, NORMAL_CHARACTER),	// ‘e’ -> ‘ẽ’
  DEADTRANS(0x09c5, L'i'  , 0x0129, NORMAL_CHARACTER),	// ‘i’ -> ‘ĩ’
  DEADTRANS(0x09c5, L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(0x09c5, L'o'  , 0x00f5, NORMAL_CHARACTER),	// ‘o’ -> ‘õ’
  DEADTRANS(0x09c5, L'u'  , 0x0169, NORMAL_CHARACTER),	// ‘u’ -> ‘ũ’
  DEADTRANS(0x09c5, L'v'  , 0x1e7d, NORMAL_CHARACTER),	// ‘v’ -> ‘ṽ’
  DEADTRANS(0x09c5, L'y'  , 0x1ef9, NORMAL_CHARACTER),	// ‘y’ -> ‘ỹ’
  DEADTRANS(0x09c5, L'~'  , 0x2248, NORMAL_CHARACTER),	// U+007E -> U+2248
  DEADTRANS(0x09c5, 0x00a0, L'~'  , NORMAL_CHARACTER),	// U+00A0 -> U+007E
  DEADTRANS(0x09c5, 0x00d6, 0x1e4e, NORMAL_CHARACTER),	// ‘Ö’ -> ‘Ṏ’
  DEADTRANS(0x09c5, 0x00f6, 0x1e4f, NORMAL_CHARACTER),	// ‘ö’ -> ‘ṏ’
  DEADTRANS(0x09c5, L'~'  , 0x02f7, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Tilde Below›
  DEADTRANS(0x09c5, L' '  , 0x02dc, NORMAL_CHARACTER),	// U+0020 -> U+02DC

// Dead key: Compose + °
  DEADTRANS(0x09c6, L'C'  , 0x2103, NORMAL_CHARACTER),	// ‘C’ -> U+2103
  DEADTRANS(0x09c6, L'F'  , 0x2109, NORMAL_CHARACTER),	// ‘F’ -> U+2109
  DEADTRANS(0x09c6, L'c'  , 0x2103, NORMAL_CHARACTER),	// ‘c’ -> U+2103
  DEADTRANS(0x09c6, L'f'  , 0x2109, NORMAL_CHARACTER),	// ‘f’ -> U+2109
  DEADTRANS(0x09c6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ·
  DEADTRANS(0x09c9, 0x00b7, 0x22ef, NORMAL_CHARACTER),	// U+00B7 -> U+22EF
  DEADTRANS(0x09c9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ‘
  DEADTRANS(0x09ca, L'"'  , 0x201f, NORMAL_CHARACTER),	// U+0022 -> U+201F
  DEADTRANS(0x09ca, L'\'' , 0x201b, NORMAL_CHARACTER),	// U+0027 -> U+201B
  DEADTRANS(0x09ca, L'['  , 0x2e24, NORMAL_CHARACTER),	// U+005B -> U+2E24
  DEADTRANS(0x09ca, L']'  , 0x2e25, NORMAL_CHARACTER),	// U+005D -> U+2E25
  DEADTRANS(0x09ca, 0x2018, 0x201c, NORMAL_CHARACTER),	// U+2018 -> U+201C
  DEADTRANS(0x09ca, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ’
  DEADTRANS(0x09cf, L'"'  , 0x201f, NORMAL_CHARACTER),	// U+0022 -> U+201F
  DEADTRANS(0x09cf, L'\'' , 0x201b, NORMAL_CHARACTER),	// U+0027 -> U+201B
  DEADTRANS(0x09cf, L','  , 0x201a, NORMAL_CHARACTER),	// U+002C -> U+201A
  DEADTRANS(0x09cf, L'<'  , 0x2039, NORMAL_CHARACTER),	// U+003C -> U+2039
  DEADTRANS(0x09cf, L'>'  , 0x203a, NORMAL_CHARACTER),	// U+003E -> U+203A
  DEADTRANS(0x09cf, L'['  , 0x2e22, NORMAL_CHARACTER),	// U+005B -> U+2E22
  DEADTRANS(0x09cf, L']'  , 0x2e23, NORMAL_CHARACTER),	// U+005D -> U+2E23
  DEADTRANS(0x09cf, L'_'  , 0x0b96, CHAINED_DEAD_KEY),	// U+005F -> ‹Compose + ’ + _›
  DEADTRANS(0x09cf, 0x2019, 0x201d, NORMAL_CHARACTER),	// U+2019 -> U+201D
  DEADTRANS(0x09cf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + €
  DEADTRANS(0x09d0, L'&'  , 0x20b0, NORMAL_CHARACTER),	// U+0026 -> U+20B0
  DEADTRANS(0x09d0, L'A'  , 0x20b3, NORMAL_CHARACTER),	// ‘A’ -> U+20B3
  DEADTRANS(0x09d0, L'B'  , 0x0e3f, NORMAL_CHARACTER),	// ‘B’ -> U+0E3F
  DEADTRANS(0x09d0, L'C'  , 0x20a1, NORMAL_CHARACTER),	// ‘C’ -> U+20A1
  DEADTRANS(0x09d0, L'D'  , 0x20af, NORMAL_CHARACTER),	// ‘D’ -> U+20AF
  DEADTRANS(0x09d0, L'F'  , 0x20a3, NORMAL_CHARACTER),	// ‘F’ -> U+20A3
  DEADTRANS(0x09d0, L'I'  , 0x0294, NORMAL_CHARACTER),	// ‘I’ -> ‘ʔ’
  DEADTRANS(0x09d0, L'L'  , 0x20a4, NORMAL_CHARACTER),	// ‘L’ -> U+20A4
  DEADTRANS(0x09d0, L'M'  , 0x2133, NORMAL_CHARACTER),	// ‘M’ -> ‘ℳ’
  DEADTRANS(0x09d0, L'O'  , 0x0af1, NORMAL_CHARACTER),	// ‘O’ -> U+0AF1
  DEADTRANS(0x09d0, L'P'  , 0x20a7, NORMAL_CHARACTER),	// ‘P’ -> U+20A7
  DEADTRANS(0x09d0, L'R'  , 0x20b9, NORMAL_CHARACTER),	// ‘R’ -> U+20B9
  DEADTRANS(0x09d0, L'S'  , 0x20b7, NORMAL_CHARACTER),	// ‘S’ -> U+20B7
  DEADTRANS(0x09d0, L'T'  , 0x20ae, NORMAL_CHARACTER),	// ‘T’ -> U+20AE
  DEADTRANS(0x09d0, L'U'  , 0x5713, NORMAL_CHARACTER),	// ‘U’ -> ‘圓’
  DEADTRANS(0x09d0, L'Y'  , 0x5186, NORMAL_CHARACTER),	// ‘Y’ -> ‘円’
  DEADTRANS(0x09d0, L'a'  , 0x060b, NORMAL_CHARACTER),	// ‘a’ -> U+060B
  DEADTRANS(0x09d0, L'b'  , 0x20bf, NORMAL_CHARACTER),	// ‘b’ -> U+20BF
  DEADTRANS(0x09d0, L'c'  , 0x00a2, NORMAL_CHARACTER),	// ‘c’ -> U+00A2
  DEADTRANS(0x09d0, L'd'  , 0x20ab, NORMAL_CHARACTER),	// ‘d’ -> U+20AB
  DEADTRANS(0x09d0, L'e'  , 0x20a0, NORMAL_CHARACTER),	// ‘e’ -> U+20A0
  DEADTRANS(0x09d0, L'f'  , 0x0192, NORMAL_CHARACTER),	// ‘f’ -> ‘ƒ’
  DEADTRANS(0x09d0, L'g'  , 0x20b2, NORMAL_CHARACTER),	// ‘g’ -> U+20B2
  DEADTRANS(0x09d0, L'h'  , 0x20b4, NORMAL_CHARACTER),	// ‘h’ -> U+20B4
  DEADTRANS(0x09d0, L'i'  , 0xfdfc, NORMAL_CHARACTER),	// ‘i’ -> U+FDFC
  DEADTRANS(0x09d0, L'k'  , 0x20ad, NORMAL_CHARACTER),	// ‘k’ -> U+20AD
  DEADTRANS(0x09d0, L'l'  , 0x20ba, NORMAL_CHARACTER),	// ‘l’ -> U+20BA
  DEADTRANS(0x09d0, L'm'  , 0x20a5, NORMAL_CHARACTER),	// ‘m’ -> U+20A5
  DEADTRANS(0x09d0, L'n'  , 0x20a6, NORMAL_CHARACTER),	// ‘n’ -> U+20A6
  DEADTRANS(0x09d0, L'o'  , 0x0bf9, NORMAL_CHARACTER),	// ‘o’ -> U+0BF9
  DEADTRANS(0x09d0, L'p'  , 0x20b1, NORMAL_CHARACTER),	// ‘p’ -> U+20B1
  DEADTRANS(0x09d0, L'r'  , 0x20bd, NORMAL_CHARACTER),	// ‘r’ -> U+20BD
  DEADTRANS(0x09d0, L's'  , 0x20aa, NORMAL_CHARACTER),	// ‘s’ -> U+20AA
  DEADTRANS(0x09d0, L't'  , 0x20b8, NORMAL_CHARACTER),	// ‘t’ -> U+20B8
  DEADTRANS(0x09d0, L'u'  , 0x5143, NORMAL_CHARACTER),	// ‘u’ -> ‘元’
  DEADTRANS(0x09d0, L'w'  , 0x20a9, NORMAL_CHARACTER),	// ‘w’ -> U+20A9
  DEADTRANS(0x09d0, L'y'  , 0x00a5, NORMAL_CHARACTER),	// ‘y’ -> U+00A5
  DEADTRANS(0x09d0, 0x00a3, 0x20b6, NORMAL_CHARACTER),	// U+00A3 -> U+20B6
  DEADTRANS(0x09d0, L','  , 0x20a2, NORMAL_CHARACTER),	// ‹Cedilla› -> U+20A2
  DEADTRANS(0x09d0, L'/'  , 0x20be, NORMAL_CHARACTER),	// ‹Long Solidus Overlay› -> U+20BE
  DEADTRANS(0x09d0, 0x00af, 0x20bc, NORMAL_CHARACTER),	// ‹Macron› -> U+20BC
  DEADTRANS(0x09d0, 0x02d8, 0x20a8, NORMAL_CHARACTER),	// ‹Breve› -> U+20A8
  DEADTRANS(0x09d0, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ⟨
  DEADTRANS(0x09d1, 0x27e8, 0x27ea, NORMAL_CHARACTER),	// U+27E8 -> U+27EA
  DEADTRANS(0x09d1, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ⟩
  DEADTRANS(0x09d2, 0x27e9, 0x27eb, NORMAL_CHARACTER),	// U+27E9 -> U+27EB
  DEADTRANS(0x09d2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose
  DEADTRANS(0x09d3, L'!'  , 0x203c, NORMAL_CHARACTER),	// U+0021 -> U+203C
  DEADTRANS(0x09d3, L'%'  , 0x2030, NORMAL_CHARACTER),	// U+0025 -> U+2030
  DEADTRANS(0x09d3, L'('  , 0x2e28, NORMAL_CHARACTER),	// U+0028 -> U+2E28
  DEADTRANS(0x09d3, L')'  , 0x2e29, NORMAL_CHARACTER),	// U+0029 -> U+2E29
  DEADTRANS(0x09d3, L'*'  , 0x2051, NORMAL_CHARACTER),	// U+002A -> U+2051
  DEADTRANS(0x09d3, L','  , 0x201e, NORMAL_CHARACTER),	// U+002C -> U+201E
  DEADTRANS(0x09d3, L'-'  , 0x2013, NORMAL_CHARACTER),	// U+002D -> U+2013
  DEADTRANS(0x09d3, L'.'  , 0x2025, NORMAL_CHARACTER),	// U+002E -> U+2025
  DEADTRANS(0x09d3, L'/'  , 0x2afd, NORMAL_CHARACTER),	// U+002F -> U+2AFD
  DEADTRANS(0x09d3, L'2'  , 0x0b97, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + Compose + 2›
  DEADTRANS(0x09d3, L'9'  , 0x0b98, CHAINED_DEAD_KEY),	// ‘9’ -> ‹Compose + Compose + 9›
  DEADTRANS(0x09d3, L':'  , 0x2237, NORMAL_CHARACTER),	// U+003A -> U+2237
  DEADTRANS(0x09d3, L'<'  , 0x226a, NORMAL_CHARACTER),	// U+003C -> U+226A
  DEADTRANS(0x09d3, L'>'  , 0x226b, NORMAL_CHARACTER),	// U+003E -> U+226B
  DEADTRANS(0x09d3, L'?'  , 0x2047, NORMAL_CHARACTER),	// U+003F -> U+2047
  DEADTRANS(0x09d3, L'@'  , 0x0b9b, CHAINED_DEAD_KEY),	// U+0040 -> ‹Compose + Compose + @›
  DEADTRANS(0x09d3, L'|'  , 0x2016, NORMAL_CHARACTER),	// U+007C -> U+2016
  DEADTRANS(0x09d3, 0x2018, 0x201c, NORMAL_CHARACTER),	// U+2018 -> U+201C
  DEADTRANS(0x09d3, 0x2019, 0x201d, NORMAL_CHARACTER),	// U+2019 -> U+201D
  DEADTRANS(0x09d3, 0x27e8, 0x27ea, NORMAL_CHARACTER),	// U+27E8 -> U+27EA
  DEADTRANS(0x09d3, 0x27e9, 0x27eb, NORMAL_CHARACTER),	// U+27E9 -> U+27EB
  DEADTRANS(0x09d3, 0x266b, 0x0ba6, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + Compose + Compose›
  DEADTRANS(0x09d3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +   + <
  DEADTRANS(0x09d4, L'F'  , 0x2619, NORMAL_CHARACTER),	// ‘F’ -> U+2619
  DEADTRANS(0x09d4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +   + >
  DEADTRANS(0x09d5, L'F'  , 0x2767, NORMAL_CHARACTER),	// ‘F’ -> U+2767
  DEADTRANS(0x09d5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +   + c
  DEADTRANS(0x09d6, L'r'  , 0x240d, NORMAL_CHARACTER),	// ‘r’ -> U+240D
  DEADTRANS(0x09d6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +   + h
  DEADTRANS(0x09d8, L't'  , 0x2409, NORMAL_CHARACTER),	// ‘t’ -> U+2409
  DEADTRANS(0x09d8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +   + l
  DEADTRANS(0x09d9, L'f'  , 0x240a, NORMAL_CHARACTER),	// ‘f’ -> U+240A
  DEADTRANS(0x09d9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose +   + s
  DEADTRANS(0x09da, L'p'  , 0x2420, NORMAL_CHARACTER),	// ‘p’ -> U+2420
  DEADTRANS(0x09da, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + -
  DEADTRANS(0x09db, L'*'  , 0x09de, CHAINED_DEAD_KEY),	// U+002A -> ‹Compose + ! + - + *›
  DEADTRANS(0x09db, L'<'  , 0x219a, NORMAL_CHARACTER),	// U+003C -> U+219A
  DEADTRANS(0x09db, L'>'  , 0x219b, NORMAL_CHARACTER),	// U+003E -> U+219B
  DEADTRANS(0x09db, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + - + *
  DEADTRANS(0x09de, L'<'  , 0x09e4, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + ! + - + * + <›
  DEADTRANS(0x09de, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + - + * + <
  DEADTRANS(0x09e4, L'>'  , 0x21ae, NORMAL_CHARACTER),	// U+003E -> U+21AE
  DEADTRANS(0x09e4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + <
  DEADTRANS(0x09e5, L'>'  , 0x2278, NORMAL_CHARACTER),	// U+003E -> U+2278
  DEADTRANS(0x09e5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + >
  DEADTRANS(0x09ff, L'<'  , 0x2279, NORMAL_CHARACTER),	// U+003C -> U+2279
  DEADTRANS(0x09ff, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + E
  DEADTRANS(0x0a00, L'E'  , 0x2204, NORMAL_CHARACTER),	// ‘E’ -> U+2204
  DEADTRANS(0x0a00, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ! + ~
  DEADTRANS(0x0a04, L'-'  , 0x2244, NORMAL_CHARACTER),	// U+002D -> U+2244
  DEADTRANS(0x0a04, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + % + <
  DEADTRANS(0x0a0b, L'>'  , 0x29eb, NORMAL_CHARACTER),	// U+003E -> U+29EB
  DEADTRANS(0x0a0b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + !
  DEADTRANS(0x0a0c, L'c'  , 0x0a0d, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Compose + & + ! + c›
  DEADTRANS(0x0a0c, L'e'  , 0x0a37, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + & + ! + e›
  DEADTRANS(0x0a0c, L'i'  , 0x0a46, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + ! + i›
  DEADTRANS(0x0a0c, L'n'  , 0x0a49, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + ! + n›
  DEADTRANS(0x0a0c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c
  DEADTRANS(0x0a0d, L'o'  , 0x0a0e, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + & + ! + c + o›
  DEADTRANS(0x0a0d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c + o
  DEADTRANS(0x0a0e, L'n'  , 0x0a11, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + ! + c + o + n›
  DEADTRANS(0x0a0e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c + o + n
  DEADTRANS(0x0a11, L't'  , 0x0a12, CHAINED_DEAD_KEY),	// ‘t’ -> ‹Compose + & + ! + c + o + n + t›
  DEADTRANS(0x0a11, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c + o + n + t
  DEADTRANS(0x0a12, L'a'  , 0x0a29, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + & + ! + c + o + n + t + a›
  DEADTRANS(0x0a12, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c + o + n + t + a
  DEADTRANS(0x0a29, L'i'  , 0x0a31, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + ! + c + o + n + t + a + i›
  DEADTRANS(0x0a29, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c + o + n + t + a + i
  DEADTRANS(0x0a31, L'n'  , 0x0a34, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + ! + c + o + n + t + a + i + n›
  DEADTRANS(0x0a31, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + c + o + n + t + a + i + n
  DEADTRANS(0x0a34, L's'  , 0x220c, NORMAL_CHARACTER),	// ‘s’ -> U+220C
  DEADTRANS(0x0a34, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e
  DEADTRANS(0x0a37, L'l'  , 0x0a3a, CHAINED_DEAD_KEY),	// ‘l’ -> ‹Compose + & + ! + e + l›
  DEADTRANS(0x0a37, L'x'  , 0x0a3d, CHAINED_DEAD_KEY),	// ‘x’ -> ‹Compose + & + ! + e + x›
  DEADTRANS(0x0a37, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e + l
  DEADTRANS(0x0a3a, L'e'  , 0x0a3b, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + & + ! + e + l + e›
  DEADTRANS(0x0a3a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e + l + e
  DEADTRANS(0x0a3b, L'm'  , 0x2209, NORMAL_CHARACTER),	// ‘m’ -> U+2209
  DEADTRANS(0x0a3b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e + x
  DEADTRANS(0x0a3d, L'i'  , 0x0a43, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + ! + e + x + i›
  DEADTRANS(0x0a3d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e + x + i
  DEADTRANS(0x0a43, L's'  , 0x0a44, CHAINED_DEAD_KEY),	// ‘s’ -> ‹Compose + & + ! + e + x + i + s›
  DEADTRANS(0x0a43, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e + x + i + s
  DEADTRANS(0x0a44, L't'  , 0x0a45, CHAINED_DEAD_KEY),	// ‘t’ -> ‹Compose + & + ! + e + x + i + s + t›
  DEADTRANS(0x0a44, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + e + x + i + s + t
  DEADTRANS(0x0a45, L's'  , 0x2204, NORMAL_CHARACTER),	// ‘s’ -> U+2204
  DEADTRANS(0x0a45, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + i
  DEADTRANS(0x0a46, L'n'  , 0x2209, NORMAL_CHARACTER),	// ‘n’ -> U+2209
  DEADTRANS(0x0a46, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + ! + n
  DEADTRANS(0x0a49, L'i'  , 0x220c, NORMAL_CHARACTER),	// ‘i’ -> U+220C
  DEADTRANS(0x0a49, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + O
  DEADTRANS(0x0a4a, L'h'  , 0x0a4e, CHAINED_DEAD_KEY),	// ‘h’ -> ‹Compose + & + O + h›
  DEADTRANS(0x0a4a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + O + h
  DEADTRANS(0x0a4e, L'm'  , 0x2127, NORMAL_CHARACTER),	// ‘m’ -> U+2127
  DEADTRANS(0x0a4e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + a
  DEADTRANS(0x0a4f, L'n'  , 0x0a50, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + a + n›
  DEADTRANS(0x0a4f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + a + n
  DEADTRANS(0x0a50, L'd'  , 0x2227, NORMAL_CHARACTER),	// ‘d’ -> U+2227
  DEADTRANS(0x0a50, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c
  DEADTRANS(0x0a52, L'o'  , 0x0a53, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + & + c + o›
  DEADTRANS(0x0a52, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c + o
  DEADTRANS(0x0a53, L'n'  , 0x0a54, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + c + o + n›
  DEADTRANS(0x0a53, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c + o + n
  DEADTRANS(0x0a54, L't'  , 0x0a55, CHAINED_DEAD_KEY),	// ‘t’ -> ‹Compose + & + c + o + n + t›
  DEADTRANS(0x0a54, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c + o + n + t
  DEADTRANS(0x0a55, L'a'  , 0x0a56, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + & + c + o + n + t + a›
  DEADTRANS(0x0a55, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c + o + n + t + a
  DEADTRANS(0x0a56, L'i'  , 0x0a57, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + c + o + n + t + a + i›
  DEADTRANS(0x0a56, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c + o + n + t + a + i
  DEADTRANS(0x0a57, L'n'  , 0x0a58, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + c + o + n + t + a + i + n›
  DEADTRANS(0x0a57, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + c + o + n + t + a + i + n
  DEADTRANS(0x0a58, L's'  , 0x220b, NORMAL_CHARACTER),	// ‘s’ -> U+220B
  DEADTRANS(0x0a58, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + d
  DEADTRANS(0x0a5d, L'e'  , 0x0a5f, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + & + d + e›
  DEADTRANS(0x0a5d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + d + e
  DEADTRANS(0x0a5f, L'g'  , 0x00b0, NORMAL_CHARACTER),	// ‘g’ -> U+00B0
  DEADTRANS(0x0a5f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e
  DEADTRANS(0x0a60, L'l'  , 0x0a61, CHAINED_DEAD_KEY),	// ‘l’ -> ‹Compose + & + e + l›
  DEADTRANS(0x0a60, L's'  , 0x0a63, CHAINED_DEAD_KEY),	// ‘s’ -> ‹Compose + & + e + s›
  DEADTRANS(0x0a60, L'x'  , 0x0a77, CHAINED_DEAD_KEY),	// ‘x’ -> ‹Compose + & + e + x›
  DEADTRANS(0x0a60, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + l
  DEADTRANS(0x0a61, L'e'  , 0x0a62, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + & + e + l + e›
  DEADTRANS(0x0a61, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + l + e
  DEADTRANS(0x0a62, L'm'  , 0x2208, NORMAL_CHARACTER),	// ‘m’ -> U+2208
  DEADTRANS(0x0a62, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + s
  DEADTRANS(0x0a63, L't'  , 0x0a64, CHAINED_DEAD_KEY),	// ‘t’ -> ‹Compose + & + e + s + t›
  DEADTRANS(0x0a63, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + s + t
  DEADTRANS(0x0a64, L'i'  , 0x0a65, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + e + s + t + i›
  DEADTRANS(0x0a64, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + s + t + i
  DEADTRANS(0x0a65, L'm'  , 0x212e, NORMAL_CHARACTER),	// ‘m’ -> U+212E
  DEADTRANS(0x0a65, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + x
  DEADTRANS(0x0a77, L'i'  , 0x0a78, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + & + e + x + i›
  DEADTRANS(0x0a77, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + x + i
  DEADTRANS(0x0a78, L's'  , 0x0a79, CHAINED_DEAD_KEY),	// ‘s’ -> ‹Compose + & + e + x + i + s›
  DEADTRANS(0x0a78, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + x + i + s
  DEADTRANS(0x0a79, L't'  , 0x0a7a, CHAINED_DEAD_KEY),	// ‘t’ -> ‹Compose + & + e + x + i + s + t›
  DEADTRANS(0x0a79, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + e + x + i + s + t
  DEADTRANS(0x0a7a, L's'  , 0x2203, NORMAL_CHARACTER),	// ‘s’ -> U+2203
  DEADTRANS(0x0a7a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + f
  DEADTRANS(0x0a7b, L'o'  , 0x0a7c, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + & + f + o›
  DEADTRANS(0x0a7b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + f + o
  DEADTRANS(0x0a7c, L'r'  , 0x0a7d, CHAINED_DEAD_KEY),	// ‘r’ -> ‹Compose + & + f + o + r›
  DEADTRANS(0x0a7c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + f + o + r
  DEADTRANS(0x0a7d, L'a'  , 0x0a7e, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + & + f + o + r + a›
  DEADTRANS(0x0a7d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + f + o + r + a
  DEADTRANS(0x0a7e, L'l'  , 0x0a7f, CHAINED_DEAD_KEY),	// ‘l’ -> ‹Compose + & + f + o + r + a + l›
  DEADTRANS(0x0a7e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + f + o + r + a + l
  DEADTRANS(0x0a7f, L'l'  , 0x2200, NORMAL_CHARACTER),	// ‘l’ -> U+2200
  DEADTRANS(0x0a7f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + i
  DEADTRANS(0x0a80, L'n'  , 0x2208, NORMAL_CHARACTER),	// ‘n’ -> U+2208
  DEADTRANS(0x0a80, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + n
  DEADTRANS(0x0a84, L'a'  , 0x0a8e, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + & + n + a›
  DEADTRANS(0x0a84, L'i'  , 0x220b, NORMAL_CHARACTER),	// ‘i’ -> U+220B
  DEADTRANS(0x0a84, L'o'  , 0x0aa9, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + & + n + o›
  DEADTRANS(0x0a84, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + n + a
  DEADTRANS(0x0a8e, L'n'  , 0x0a92, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + & + n + a + n›
  DEADTRANS(0x0a8e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + n + a + n
  DEADTRANS(0x0a92, L'd'  , 0x22bc, NORMAL_CHARACTER),	// ‘d’ -> U+22BC
  DEADTRANS(0x0a92, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + n + o
  DEADTRANS(0x0aa9, L'r'  , 0x22bd, NORMAL_CHARACTER),	// ‘r’ -> U+22BD
  DEADTRANS(0x0aa9, L't'  , 0x00ac, NORMAL_CHARACTER),	// ‘t’ -> U+00AC
  DEADTRANS(0x0aa9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + o
  DEADTRANS(0x0ab1, L'h'  , 0x0ab4, CHAINED_DEAD_KEY),	// ‘h’ -> ‹Compose + & + o + h›
  DEADTRANS(0x0ab1, L'r'  , 0x2228, NORMAL_CHARACTER),	// ‘r’ -> U+2228
  DEADTRANS(0x0ab1, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + o + h
  DEADTRANS(0x0ab4, L'm'  , 0x03a9, NORMAL_CHARACTER),	// ‘m’ -> ‘Ω’
  DEADTRANS(0x0ab4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + x
  DEADTRANS(0x0aba, L'o'  , 0x0abb, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + & + x + o›
  DEADTRANS(0x0aba, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + & + x + o
  DEADTRANS(0x0abb, L'r'  , 0x22bb, NORMAL_CHARACTER),	// ‘r’ -> U+22BB
  DEADTRANS(0x0abb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ' + _
  DEADTRANS(0x0ac6, L'['  , 0x2e24, NORMAL_CHARACTER),	// U+005B -> U+2E24
  DEADTRANS(0x0ac6, L']'  , 0x2e25, NORMAL_CHARACTER),	// U+005D -> U+2E25
  DEADTRANS(0x0ac6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ( + 2
  DEADTRANS(0x0aca, L'<'  , 0x27ea, NORMAL_CHARACTER),	// U+003C -> U+27EA
  DEADTRANS(0x0aca, L'>'  , 0x27eb, NORMAL_CHARACTER),	// U+003E -> U+27EB
  DEADTRANS(0x0aca, L'w'  , 0x29da, NORMAL_CHARACTER),	// ‘w’ -> U+29DA
  DEADTRANS(0x0aca, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ( + v
  DEADTRANS(0x0ace, L'2'  , 0x0acf, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + ( + v + 2›
  DEADTRANS(0x0ace, L'<'  , 0xfe3f, NORMAL_CHARACTER),	// U+003C -> U+FE3F
  DEADTRANS(0x0ace, L'>'  , 0xfe40, NORMAL_CHARACTER),	// U+003E -> U+FE40
  DEADTRANS(0x0ace, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ( + v + 2
  DEADTRANS(0x0acf, L'<'  , 0xfe3d, NORMAL_CHARACTER),	// U+003C -> U+FE3D
  DEADTRANS(0x0acf, L'>'  , 0xfe3e, NORMAL_CHARACTER),	// U+003E -> U+FE3E
  DEADTRANS(0x0acf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ) + 2
  DEADTRANS(0x0ad1, L'<'  , 0x27eb, NORMAL_CHARACTER),	// U+003C -> U+27EB
  DEADTRANS(0x0ad1, L'w'  , 0x29db, NORMAL_CHARACTER),	// ‘w’ -> U+29DB
  DEADTRANS(0x0ad1, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ) + v
  DEADTRANS(0x0ad2, L'2'  , 0x0ad3, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + ) + v + 2›
  DEADTRANS(0x0ad2, L'<'  , 0xfe40, NORMAL_CHARACTER),	// U+003C -> U+FE40
  DEADTRANS(0x0ad2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ) + v + 2
  DEADTRANS(0x0ad3, L'<'  , 0xfe3e, NORMAL_CHARACTER),	// U+003C -> U+FE3E
  DEADTRANS(0x0ad3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + * + %
  DEADTRANS(0x0ad4, L'.'  , 0x25e6, NORMAL_CHARACTER),	// U+002E -> U+25E6
  DEADTRANS(0x0ad4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + - + *
  DEADTRANS(0x0ad5, L'<'  , 0x0ad6, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + - + * + <›
  DEADTRANS(0x0ad5, L'^'  , 0x0ad7, CHAINED_DEAD_KEY),	// U+005E -> ‹Compose + - + * + ^›
  DEADTRANS(0x0ad5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + - + * + <
  DEADTRANS(0x0ad6, L'>'  , 0x2194, NORMAL_CHARACTER),	// U+003E -> U+2194
  DEADTRANS(0x0ad6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + - + * + ^
  DEADTRANS(0x0ad7, L'_'  , 0x2195, NORMAL_CHARACTER),	// U+005F -> U+2195
  DEADTRANS(0x0ad7, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + . + 3
  DEADTRANS(0x0ad8, L'<'  , 0x22f1, NORMAL_CHARACTER),	// U+003C -> U+22F1
  DEADTRANS(0x0ad8, L'>'  , 0x22f0, NORMAL_CHARACTER),	// U+003E -> U+22F0
  DEADTRANS(0x0ad8, L'_'  , 0x2026, NORMAL_CHARACTER),	// U+005F -> U+2026
  DEADTRANS(0x0ad8, L'c'  , 0x22ef, NORMAL_CHARACTER),	// ‘c’ -> U+22EF
  DEADTRANS(0x0ad8, L'v'  , 0x22ee, NORMAL_CHARACTER),	// ‘v’ -> U+22EE
  DEADTRANS(0x0ad8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + / + 2
  DEADTRANS(0x0ad9, L'|'  , 0x2226, NORMAL_CHARACTER),	// U+007C -> U+2226
  DEADTRANS(0x0ad9, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 0 + /
  DEADTRANS(0x0ada, L'0'  , 0x2052, NORMAL_CHARACTER),	// ‘0’ -> U+2052
  DEADTRANS(0x0ada, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 1 + 1
  DEADTRANS(0x0adb, L'0'  , 0x2152, NORMAL_CHARACTER),	// ‘0’ -> ‘⅒’
  DEADTRANS(0x0adb, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 1 + @
  DEADTRANS(0x0adc, L's'  , 0x222e, NORMAL_CHARACTER),	// ‘s’ -> U+222E
  DEADTRANS(0x0adc, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 2 + @
  DEADTRANS(0x0add, L's'  , 0x222f, NORMAL_CHARACTER),	// ‘s’ -> U+222F
  DEADTRANS(0x0add, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 2 + `
  DEADTRANS(0x0ade, L'.'  , 0x030f, NORMAL_CHARACTER),	// U+002E -> U+030F
  DEADTRANS(0x0ade, L'A'  , 0x0200, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȁ’
  DEADTRANS(0x0ade, L'E'  , 0x0204, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȅ’
  DEADTRANS(0x0ade, L'I'  , 0x0208, NORMAL_CHARACTER),	// ‘I’ -> ‘Ȉ’
  DEADTRANS(0x0ade, L'O'  , 0x020c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȍ’
  DEADTRANS(0x0ade, L'R'  , 0x0210, NORMAL_CHARACTER),	// ‘R’ -> ‘Ȑ’
  DEADTRANS(0x0ade, L'U'  , 0x0214, NORMAL_CHARACTER),	// ‘U’ -> ‘Ȕ’
  DEADTRANS(0x0ade, L'a'  , 0x0201, NORMAL_CHARACTER),	// ‘a’ -> ‘ȁ’
  DEADTRANS(0x0ade, L'e'  , 0x0205, NORMAL_CHARACTER),	// ‘e’ -> ‘ȅ’
  DEADTRANS(0x0ade, L'i'  , 0x0209, NORMAL_CHARACTER),	// ‘i’ -> ‘ȉ’
  DEADTRANS(0x0ade, L'o'  , 0x020d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȍ’
  DEADTRANS(0x0ade, L'r'  , 0x0211, NORMAL_CHARACTER),	// ‘r’ -> ‘ȑ’
  DEADTRANS(0x0ade, L'u'  , 0x0215, NORMAL_CHARACTER),	// ‘u’ -> ‘ȕ’
  DEADTRANS(0x0ade, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 2 + c
  DEADTRANS(0x0adf, L'm'  , 0x33a0, NORMAL_CHARACTER),	// ‘m’ -> U+33A0
  DEADTRANS(0x0adf, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 2 + d
  DEADTRANS(0x0ae4, L'm'  , 0x3378, NORMAL_CHARACTER),	// ‘m’ -> U+3378
  DEADTRANS(0x0ae4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 2 + k
  DEADTRANS(0x0ae5, L'm'  , 0x33a2, NORMAL_CHARACTER),	// ‘m’ -> U+33A2
  DEADTRANS(0x0ae5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 2 + m
  DEADTRANS(0x0af2, L'm'  , 0x339f, NORMAL_CHARACTER),	// ‘m’ -> U+339F
  DEADTRANS(0x0af2, L' '  , 0x33a1, NORMAL_CHARACTER),	// U+0020 -> U+33A1

// Dead key: Compose + 3 + @
  DEADTRANS(0x0af3, L's'  , 0x2230, NORMAL_CHARACTER),	// ‘s’ -> U+2230
  DEADTRANS(0x0af3, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 3 + c
  DEADTRANS(0x0af4, L'm'  , 0x33a4, NORMAL_CHARACTER),	// ‘m’ -> U+33A4
  DEADTRANS(0x0af4, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 3 + d
  DEADTRANS(0x0af5, L'm'  , 0x3379, NORMAL_CHARACTER),	// ‘m’ -> U+3379
  DEADTRANS(0x0af5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 3 + k
  DEADTRANS(0x0af6, L'm'  , 0x33a6, NORMAL_CHARACTER),	// ‘m’ -> U+33A6
  DEADTRANS(0x0af6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + 3 + m
  DEADTRANS(0x0af7, L'm'  , 0x33a3, NORMAL_CHARACTER),	// ‘m’ -> U+33A3
  DEADTRANS(0x0af7, L' '  , 0x33a5, NORMAL_CHARACTER),	// U+0020 -> U+33A5

// Dead key: Compose + = + !
  DEADTRANS(0x0af8, L'*'  , 0x0b00, CHAINED_DEAD_KEY),	// U+002A -> ‹Compose + = + ! + *›
  DEADTRANS(0x0af8, L'<'  , 0x21cd, NORMAL_CHARACTER),	// U+003C -> U+21CD
  DEADTRANS(0x0af8, L'>'  , 0x21cf, NORMAL_CHARACTER),	// U+003E -> U+21CF
  DEADTRANS(0x0af8, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + ! + *
  DEADTRANS(0x0b00, L'<'  , 0x0b04, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + = + ! + * + <›
  DEADTRANS(0x0b00, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + ! + * + <
  DEADTRANS(0x0b04, L'>'  , 0x21ce, NORMAL_CHARACTER),	// U+003E -> U+21CE
  DEADTRANS(0x0b04, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + *
  DEADTRANS(0x0b0d, L'<'  , 0x0b0e, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + = + * + <›
  DEADTRANS(0x0b0d, L'^'  , 0x0b11, CHAINED_DEAD_KEY),	// U+005E -> ‹Compose + = + * + ^›
  DEADTRANS(0x0b0d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + * + <
  DEADTRANS(0x0b0e, L'>'  , 0x21d4, NORMAL_CHARACTER),	// U+003E -> U+21D4
  DEADTRANS(0x0b0e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + * + ^
  DEADTRANS(0x0b11, L'_'  , 0x21d5, NORMAL_CHARACTER),	// U+005F -> U+21D5
  DEADTRANS(0x0b11, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + /
  DEADTRANS(0x0b12, L'/'  , 0x29e3, NORMAL_CHARACTER),	// U+002F -> U+29E3
  DEADTRANS(0x0b12, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + d
  DEADTRANS(0x0b29, L'e'  , 0x0b31, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + = + d + e›
  DEADTRANS(0x0b29, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + d + e
  DEADTRANS(0x0b31, L'f'  , 0x225d, NORMAL_CHARACTER),	// ‘f’ -> U+225D
  DEADTRANS(0x0b31, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + = + |
  DEADTRANS(0x0b34, L'|'  , 0x22d5, NORMAL_CHARACTER),	// U+007C -> U+22D5
  DEADTRANS(0x0b34, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @ + %
  DEADTRANS(0x0b3a, L'0'  , 0x24ff, NORMAL_CHARACTER),	// ‘0’ -> ‘⓿’
  DEADTRANS(0x0b3a, L'1'  , 0x2776, NORMAL_CHARACTER),	// ‘1’ -> ‘❶’
  DEADTRANS(0x0b3a, L'2'  , 0x2777, NORMAL_CHARACTER),	// ‘2’ -> ‘❷’
  DEADTRANS(0x0b3a, L'3'  , 0x2778, NORMAL_CHARACTER),	// ‘3’ -> ‘❸’
  DEADTRANS(0x0b3a, L'4'  , 0x2779, NORMAL_CHARACTER),	// ‘4’ -> ‘❹’
  DEADTRANS(0x0b3a, L'5'  , 0x277a, NORMAL_CHARACTER),	// ‘5’ -> ‘❺’
  DEADTRANS(0x0b3a, L'6'  , 0x277b, NORMAL_CHARACTER),	// ‘6’ -> ‘❻’
  DEADTRANS(0x0b3a, L'7'  , 0x277c, NORMAL_CHARACTER),	// ‘7’ -> ‘❼’
  DEADTRANS(0x0b3a, L'8'  , 0x277d, NORMAL_CHARACTER),	// ‘8’ -> ‘❽’
  DEADTRANS(0x0b3a, L'9'  , 0x277e, NORMAL_CHARACTER),	// ‘9’ -> ‘❾’
  DEADTRANS(0x0b3a, 0x266b, 0x0b3b, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + @ + % + Compose›
  DEADTRANS(0x0b3a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @ + % + Compose
  DEADTRANS(0x0b3b, L'0'  , 0x277f, NORMAL_CHARACTER),	// ‘0’ -> ‘❿’
  DEADTRANS(0x0b3b, L'1'  , 0x24eb, NORMAL_CHARACTER),	// ‘1’ -> ‘⓫’
  DEADTRANS(0x0b3b, L'2'  , 0x24ec, NORMAL_CHARACTER),	// ‘2’ -> ‘⓬’
  DEADTRANS(0x0b3b, L'3'  , 0x24ed, NORMAL_CHARACTER),	// ‘3’ -> ‘⓭’
  DEADTRANS(0x0b3b, L'4'  , 0x24ee, NORMAL_CHARACTER),	// ‘4’ -> ‘⓮’
  DEADTRANS(0x0b3b, L'5'  , 0x24ef, NORMAL_CHARACTER),	// ‘5’ -> ‘⓯’
  DEADTRANS(0x0b3b, L'6'  , 0x24f0, NORMAL_CHARACTER),	// ‘6’ -> ‘⓰’
  DEADTRANS(0x0b3b, L'7'  , 0x24f1, NORMAL_CHARACTER),	// ‘7’ -> ‘⓱’
  DEADTRANS(0x0b3b, L'8'  , 0x24f2, NORMAL_CHARACTER),	// ‘8’ -> ‘⓲’
  DEADTRANS(0x0b3b, L'9'  , 0x24f3, NORMAL_CHARACTER),	// ‘9’ -> ‘⓳’
  DEADTRANS(0x0b3b, 0x266b, 0x0b45, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + @ + % + Compose + Compose›
  DEADTRANS(0x0b3b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @ + % + Compose + Compose
  DEADTRANS(0x0b45, L'0'  , 0x24f4, NORMAL_CHARACTER),	// ‘0’ -> ‘⓴’
  DEADTRANS(0x0b45, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @ + n
  DEADTRANS(0x0b46, L'+'  , 0x2a01, NORMAL_CHARACTER),	// U+002B -> U+2A01
  DEADTRANS(0x0b46, L'.'  , 0x2a00, NORMAL_CHARACTER),	// U+002E -> U+2A00
  DEADTRANS(0x0b46, L'x'  , 0x2a02, NORMAL_CHARACTER),	// ‘x’ -> U+2A02
  DEADTRANS(0x0b46, 0x00d7, 0x2a02, NORMAL_CHARACTER),	// U+00D7 -> U+2A02
  DEADTRANS(0x0b46, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @ + Compose
  DEADTRANS(0x0b49, L'0'  , 0x2469, NORMAL_CHARACTER),	// ‘0’ -> ‘⑩’
  DEADTRANS(0x0b49, L'1'  , 0x246a, NORMAL_CHARACTER),	// ‘1’ -> ‘⑪’
  DEADTRANS(0x0b49, L'2'  , 0x246b, NORMAL_CHARACTER),	// ‘2’ -> ‘⑫’
  DEADTRANS(0x0b49, L'3'  , 0x246c, NORMAL_CHARACTER),	// ‘3’ -> ‘⑬’
  DEADTRANS(0x0b49, L'4'  , 0x246d, NORMAL_CHARACTER),	// ‘4’ -> ‘⑭’
  DEADTRANS(0x0b49, L'5'  , 0x246e, NORMAL_CHARACTER),	// ‘5’ -> ‘⑮’
  DEADTRANS(0x0b49, L'6'  , 0x246f, NORMAL_CHARACTER),	// ‘6’ -> ‘⑯’
  DEADTRANS(0x0b49, L'7'  , 0x2470, NORMAL_CHARACTER),	// ‘7’ -> ‘⑰’
  DEADTRANS(0x0b49, L'8'  , 0x2471, NORMAL_CHARACTER),	// ‘8’ -> ‘⑱’
  DEADTRANS(0x0b49, L'9'  , 0x2472, NORMAL_CHARACTER),	// ‘9’ -> ‘⑲’
  DEADTRANS(0x0b49, 0x266b, 0x0b4a, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + @ + Compose + Compose›
  DEADTRANS(0x0b49, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + @ + Compose + Compose
  DEADTRANS(0x0b4a, L'0'  , 0x2473, NORMAL_CHARACTER),	// ‘0’ -> ‘⑳’
  DEADTRANS(0x0b4a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + !
  DEADTRANS(0x0b4e, L'!'  , 0x00ac, NORMAL_CHARACTER),	// U+0021 -> U+00AC
  DEADTRANS(0x0b4e, L'='  , 0x2260, NORMAL_CHARACTER),	// U+003D -> U+2260
  DEADTRANS(0x0b4e, L'E'  , 0x2204, NORMAL_CHARACTER),	// ‘E’ -> U+2204
  DEADTRANS(0x0b4e, L'e'  , 0x2209, NORMAL_CHARACTER),	// ‘e’ -> U+2209
  DEADTRANS(0x0b4e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + =
  DEADTRANS(0x0b4f, L'!'  , 0x0b50, CHAINED_DEAD_KEY),	// U+0021 -> ‹Compose + M + = + !›
  DEADTRANS(0x0b4f, L'='  , 0x2261, NORMAL_CHARACTER),	// U+003D -> U+2261
  DEADTRANS(0x0b4f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + = + !
  DEADTRANS(0x0b50, L'='  , 0x2262, NORMAL_CHARACTER),	// U+003D -> U+2262
  DEADTRANS(0x0b50, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + d
  DEADTRANS(0x0b55, L'C'  , 0x2102, NORMAL_CHARACTER),	// ‘C’ -> ‘ℂ’
  DEADTRANS(0x0b55, L'H'  , 0x210d, NORMAL_CHARACTER),	// ‘H’ -> ‘ℍ’
  DEADTRANS(0x0b55, L'N'  , 0x2115, NORMAL_CHARACTER),	// ‘N’ -> ‘ℕ’
  DEADTRANS(0x0b55, L'P'  , 0x2119, NORMAL_CHARACTER),	// ‘P’ -> ‘ℙ’
  DEADTRANS(0x0b55, L'Q'  , 0x211a, NORMAL_CHARACTER),	// ‘Q’ -> ‘ℚ’
  DEADTRANS(0x0b55, L'R'  , 0x211d, NORMAL_CHARACTER),	// ‘R’ -> ‘ℝ’
  DEADTRANS(0x0b55, L'Z'  , 0x2124, NORMAL_CHARACTER),	// ‘Z’ -> ‘ℤ’
  DEADTRANS(0x0b55, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + f
  DEADTRANS(0x0b58, L'C'  , 0x212d, NORMAL_CHARACTER),	// ‘C’ -> ‘ℭ’
  DEADTRANS(0x0b58, L'H'  , 0x210c, NORMAL_CHARACTER),	// ‘H’ -> ‘ℌ’
  DEADTRANS(0x0b58, L'I'  , 0x2111, NORMAL_CHARACTER),	// ‘I’ -> ‘ℑ’
  DEADTRANS(0x0b58, L'R'  , 0x211c, NORMAL_CHARACTER),	// ‘R’ -> ‘ℜ’
  DEADTRANS(0x0b58, L'Z'  , 0x2128, NORMAL_CHARACTER),	// ‘Z’ -> ‘ℨ’
  DEADTRANS(0x0b58, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + i
  DEADTRANS(0x0b59, L'0'  , L'0'  , NORMAL_CHARACTER),	// ‘0’ -> ‘0’
  DEADTRANS(0x0b59, L'1'  , L'1'  , NORMAL_CHARACTER),	// ‘1’ -> ‘1’
  DEADTRANS(0x0b59, L'2'  , L'2'  , NORMAL_CHARACTER),	// ‘2’ -> ‘2’
  DEADTRANS(0x0b59, L'3'  , L'3'  , NORMAL_CHARACTER),	// ‘3’ -> ‘3’
  DEADTRANS(0x0b59, L'4'  , L'4'  , NORMAL_CHARACTER),	// ‘4’ -> ‘4’
  DEADTRANS(0x0b59, L'5'  , L'5'  , NORMAL_CHARACTER),	// ‘5’ -> ‘5’
  DEADTRANS(0x0b59, L'6'  , L'6'  , NORMAL_CHARACTER),	// ‘6’ -> ‘6’
  DEADTRANS(0x0b59, L'7'  , L'7'  , NORMAL_CHARACTER),	// ‘7’ -> ‘7’
  DEADTRANS(0x0b59, L'8'  , L'8'  , NORMAL_CHARACTER),	// ‘8’ -> ‘8’
  DEADTRANS(0x0b59, L'9'  , L'9'  , NORMAL_CHARACTER),	// ‘9’ -> ‘9’
  DEADTRANS(0x0b59, L'h'  , 0x210e, NORMAL_CHARACTER),	// ‘h’ -> ‘ℎ’
  DEADTRANS(0x0b59, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + s
  DEADTRANS(0x0b5a, L'B'  , 0x212c, NORMAL_CHARACTER),	// ‘B’ -> ‘ℬ’
  DEADTRANS(0x0b5a, L'E'  , 0x2130, NORMAL_CHARACTER),	// ‘E’ -> ‘ℰ’
  DEADTRANS(0x0b5a, L'F'  , 0x2131, NORMAL_CHARACTER),	// ‘F’ -> ‘ℱ’
  DEADTRANS(0x0b5a, L'H'  , 0x210b, NORMAL_CHARACTER),	// ‘H’ -> ‘ℋ’
  DEADTRANS(0x0b5a, L'I'  , 0x2110, NORMAL_CHARACTER),	// ‘I’ -> ‘ℐ’
  DEADTRANS(0x0b5a, L'L'  , 0x2112, NORMAL_CHARACTER),	// ‘L’ -> ‘ℒ’
  DEADTRANS(0x0b5a, L'M'  , 0x2133, NORMAL_CHARACTER),	// ‘M’ -> ‘ℳ’
  DEADTRANS(0x0b5a, L'R'  , 0x211b, NORMAL_CHARACTER),	// ‘R’ -> ‘ℛ’
  DEADTRANS(0x0b5a, L'e'  , 0x212f, NORMAL_CHARACTER),	// ‘e’ -> ‘ℯ’
  DEADTRANS(0x0b5a, L'g'  , 0x210a, NORMAL_CHARACTER),	// ‘g’ -> ‘ℊ’
  DEADTRANS(0x0b5a, L'o'  , 0x2134, NORMAL_CHARACTER),	// ‘o’ -> ‘ℴ’
  DEADTRANS(0x0b5a, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + {
  DEADTRANS(0x0b5b, L'!'  , 0x0b5e, CHAINED_DEAD_KEY),	// U+0021 -> ‹Compose + M + { + !›
  DEADTRANS(0x0b5b, L'e'  , 0x2208, NORMAL_CHARACTER),	// ‘e’ -> U+2208
  DEADTRANS(0x0b5b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + M + { + !
  DEADTRANS(0x0b5e, L'e'  , 0x2209, NORMAL_CHARACTER),	// ‘e’ -> U+2209
  DEADTRANS(0x0b5e, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + [ + n
  DEADTRANS(0x0b64, L'i'  , 0x2a05, NORMAL_CHARACTER),	// ‘i’ -> U+2A05
  DEADTRANS(0x0b64, L'u'  , 0x2a06, NORMAL_CHARACTER),	// ‘u’ -> U+2A06
  DEADTRANS(0x0b64, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ^ + _
  DEADTRANS(0x0b65, L'a'  , 0x00aa, NORMAL_CHARACTER),	// ‘a’ -> ‘ª’
  DEADTRANS(0x0b65, L'o'  , 0x00ba, NORMAL_CHARACTER),	// ‘o’ -> ‘º’
  DEADTRANS(0x0b65, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ^ + |
  DEADTRANS(0x0b78, L'_'  , 0x2195, NORMAL_CHARACTER),	// U+005F -> U+2195
  DEADTRANS(0x0b78, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + _ + :
  DEADTRANS(0x0b79, L'.'  , 0x0324, NORMAL_CHARACTER),	// U+002E -> U+0324
  DEADTRANS(0x0b79, L'U'  , 0x1e72, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṳ’
  DEADTRANS(0x0b79, L'u'  , 0x1e73, NORMAL_CHARACTER),	// ‘u’ -> ‘ṳ’
  DEADTRANS(0x0b79, 0x00a0, 0x2025, NORMAL_CHARACTER),	// U+00A0 -> U+2025
  DEADTRANS(0x0b79, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + _ + <
  DEADTRANS(0x0b7a, L'.'  , 0x032d, NORMAL_CHARACTER),	// U+002E -> U+032D
  DEADTRANS(0x0b7a, L'D'  , 0x1e12, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḓ’
  DEADTRANS(0x0b7a, L'E'  , 0x1e18, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḙ’
  DEADTRANS(0x0b7a, L'L'  , 0x1e3c, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḽ’
  DEADTRANS(0x0b7a, L'N'  , 0x1e4a, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṋ’
  DEADTRANS(0x0b7a, L'T'  , 0x1e70, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṱ’
  DEADTRANS(0x0b7a, L'U'  , 0x1e76, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṷ’
  DEADTRANS(0x0b7a, L'd'  , 0x1e13, NORMAL_CHARACTER),	// ‘d’ -> ‘ḓ’
  DEADTRANS(0x0b7a, L'e'  , 0x1e19, NORMAL_CHARACTER),	// ‘e’ -> ‘ḙ’
  DEADTRANS(0x0b7a, L'l'  , 0x1e3d, NORMAL_CHARACTER),	// ‘l’ -> ‘ḽ’
  DEADTRANS(0x0b7a, L'n'  , 0x1e4b, NORMAL_CHARACTER),	// ‘n’ -> ‘ṋ’
  DEADTRANS(0x0b7a, L't'  , 0x1e71, NORMAL_CHARACTER),	// ‘t’ -> ‘ṱ’
  DEADTRANS(0x0b7a, L'u'  , 0x1e77, NORMAL_CHARACTER),	// ‘u’ -> ‘ṷ’
  DEADTRANS(0x0b7a, 0x00a0, 0x2038, NORMAL_CHARACTER),	// U+00A0 -> U+2038
  DEADTRANS(0x0b7a, L' '  , 0xa788, NORMAL_CHARACTER),	// U+0020 -> ‘ꞈ’

// Dead key: Compose + _ + >
  DEADTRANS(0x0b7b, L'.'  , 0x032c, NORMAL_CHARACTER),	// U+002E -> U+032C
  DEADTRANS(0x0b7b, L' '  , 0x02ec, NORMAL_CHARACTER),	// U+0020 -> ‘ˬ’

// Dead key: Compose + _ + `
  DEADTRANS(0x0b7c, L'.'  , 0x0316, NORMAL_CHARACTER),	// U+002E -> U+0316
  DEADTRANS(0x0b7c, L' '  , 0x02ce, NORMAL_CHARACTER),	// U+0020 -> ‘ˎ’

// Dead key: Compose + _ + |
  DEADTRANS(0x0b7d, L'.'  , 0x0331, NORMAL_CHARACTER),	// U+002E -> U+0331
  DEADTRANS(0x0b7d, L'B'  , 0x1e06, NORMAL_CHARACTER),	// ‘B’ -> ‘Ḇ’
  DEADTRANS(0x0b7d, L'D'  , 0x1e0e, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḏ’
  DEADTRANS(0x0b7d, L'K'  , 0x1e34, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḵ’
  DEADTRANS(0x0b7d, L'L'  , 0x1e3a, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḻ’
  DEADTRANS(0x0b7d, L'N'  , 0x1e48, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṉ’
  DEADTRANS(0x0b7d, L'R'  , 0x1e5e, NORMAL_CHARACTER),	// ‘R’ -> ‘Ṟ’
  DEADTRANS(0x0b7d, L'T'  , 0x1e6e, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṯ’
  DEADTRANS(0x0b7d, L'Z'  , 0x1e94, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẕ’
  DEADTRANS(0x0b7d, L'b'  , 0x1e07, NORMAL_CHARACTER),	// ‘b’ -> ‘ḇ’
  DEADTRANS(0x0b7d, L'd'  , 0x1e0f, NORMAL_CHARACTER),	// ‘d’ -> ‘ḏ’
  DEADTRANS(0x0b7d, L'h'  , 0x1e96, NORMAL_CHARACTER),	// ‘h’ -> ‘ẖ’
  DEADTRANS(0x0b7d, L'k'  , 0x1e35, NORMAL_CHARACTER),	// ‘k’ -> ‘ḵ’
  DEADTRANS(0x0b7d, L'l'  , 0x1e3b, NORMAL_CHARACTER),	// ‘l’ -> ‘ḻ’
  DEADTRANS(0x0b7d, L'n'  , 0x1e49, NORMAL_CHARACTER),	// ‘n’ -> ‘ṉ’
  DEADTRANS(0x0b7d, L'r'  , 0x1e5f, NORMAL_CHARACTER),	// ‘r’ -> ‘ṟ’
  DEADTRANS(0x0b7d, L't'  , 0x1e6f, NORMAL_CHARACTER),	// ‘t’ -> ‘ṯ’
  DEADTRANS(0x0b7d, L'z'  , 0x1e95, NORMAL_CHARACTER),	// ‘z’ -> ‘ẕ’
  DEADTRANS(0x0b7d, L' '  , 0x02cd, NORMAL_CHARACTER),	// U+0020 -> ‘ˍ’

// Dead key: Compose + _ + ~
  DEADTRANS(0x0b7e, L'.'  , 0x0330, NORMAL_CHARACTER),	// U+002E -> U+0330
  DEADTRANS(0x0b7e, L'E'  , 0x1e1a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḛ’
  DEADTRANS(0x0b7e, L'I'  , 0x1e2c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḭ’
  DEADTRANS(0x0b7e, L'U'  , 0x1e74, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṵ’
  DEADTRANS(0x0b7e, L'e'  , 0x1e1b, NORMAL_CHARACTER),	// ‘e’ -> ‘ḛ’
  DEADTRANS(0x0b7e, L'i'  , 0x1e2d, NORMAL_CHARACTER),	// ‘i’ -> ‘ḭ’
  DEADTRANS(0x0b7e, L'u'  , 0x1e75, NORMAL_CHARACTER),	// ‘u’ -> ‘ṵ’
  DEADTRANS(0x0b7e, L' '  , 0x02f7, NORMAL_CHARACTER),	// U+0020 -> U+02F7

// Dead key: Compose + f + a
  DEADTRANS(0x0b7f, L'x'  , 0x213b, NORMAL_CHARACTER),	// ‘x’ -> U+213B
  DEADTRANS(0x0b7f, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + p + e
  DEADTRANS(0x0b80, L'r'  , 0x214c, NORMAL_CHARACTER),	// ‘r’ -> U+214C
  DEADTRANS(0x0b80, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + p + p
  DEADTRANS(0x0b81, L'm'  , 0x33d9, NORMAL_CHARACTER),	// ‘m’ -> U+33D9
  DEADTRANS(0x0b81, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + q + e
  DEADTRANS(0x0b84, L'd'  , 0x220e, NORMAL_CHARACTER),	// ‘d’ -> U+220E
  DEADTRANS(0x0b84, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + t + e
  DEADTRANS(0x0b8b, L'l'  , 0x2121, NORMAL_CHARACTER),	// ‘l’ -> U+2121
  DEADTRANS(0x0b8b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + w + *
  DEADTRANS(0x0b8c, L'<'  , 0x0b8d, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + w + * + <›
  DEADTRANS(0x0b8c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + w + * + <
  DEADTRANS(0x0b8d, L'>'  , 0x21ad, NORMAL_CHARACTER),	// U+003E -> U+21AD
  DEADTRANS(0x0b8d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + { + n
  DEADTRANS(0x0b91, L'i'  , 0x22c2, NORMAL_CHARACTER),	// ‘i’ -> U+22C2
  DEADTRANS(0x0b91, L'u'  , 0x22c3, NORMAL_CHARACTER),	// ‘u’ -> U+22C3
  DEADTRANS(0x0b91, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + ’ + _
  DEADTRANS(0x0b96, L'['  , 0x2e24, NORMAL_CHARACTER),	// U+005B -> U+2E24
  DEADTRANS(0x0b96, L']'  , 0x2e25, NORMAL_CHARACTER),	// U+005D -> U+2E25
  DEADTRANS(0x0b96, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + 2
  DEADTRANS(0x0b97, L'4'  , 0xa75c, NORMAL_CHARACTER),	// ‘4’ -> ‘Ꝝ’
  DEADTRANS(0x0b97, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + 9
  DEADTRANS(0x0b98, L'9'  , 0xa76e, NORMAL_CHARACTER),	// ‘9’ -> ‘Ꝯ’
  DEADTRANS(0x0b98, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + @
  DEADTRANS(0x0b9b, L'%'  , 0x0b9d, CHAINED_DEAD_KEY),	// U+0025 -> ‹Compose + Compose + @ + %›
  DEADTRANS(0x0b9b, L'0'  , 0x24ea, NORMAL_CHARACTER),	// ‘0’ -> ‘⓪’
  DEADTRANS(0x0b9b, L'1'  , 0x2460, NORMAL_CHARACTER),	// ‘1’ -> ‘①’
  DEADTRANS(0x0b9b, L'2'  , 0x2461, NORMAL_CHARACTER),	// ‘2’ -> ‘②’
  DEADTRANS(0x0b9b, L'3'  , 0x2462, NORMAL_CHARACTER),	// ‘3’ -> ‘③’
  DEADTRANS(0x0b9b, L'4'  , 0x2463, NORMAL_CHARACTER),	// ‘4’ -> ‘④’
  DEADTRANS(0x0b9b, L'5'  , 0x2464, NORMAL_CHARACTER),	// ‘5’ -> ‘⑤’
  DEADTRANS(0x0b9b, L'6'  , 0x2465, NORMAL_CHARACTER),	// ‘6’ -> ‘⑥’
  DEADTRANS(0x0b9b, L'7'  , 0x2466, NORMAL_CHARACTER),	// ‘7’ -> ‘⑦’
  DEADTRANS(0x0b9b, L'8'  , 0x2467, NORMAL_CHARACTER),	// ‘8’ -> ‘⑧’
  DEADTRANS(0x0b9b, L'9'  , 0x2468, NORMAL_CHARACTER),	// ‘9’ -> ‘⑨’
  DEADTRANS(0x0b9b, L'A'  , 0x24b6, NORMAL_CHARACTER),	// ‘A’ -> U+24B6
  DEADTRANS(0x0b9b, L'B'  , 0x24b7, NORMAL_CHARACTER),	// ‘B’ -> U+24B7
  DEADTRANS(0x0b9b, L'C'  , 0x24b8, NORMAL_CHARACTER),	// ‘C’ -> U+24B8
  DEADTRANS(0x0b9b, L'D'  , 0x24b9, NORMAL_CHARACTER),	// ‘D’ -> U+24B9
  DEADTRANS(0x0b9b, L'E'  , 0x24ba, NORMAL_CHARACTER),	// ‘E’ -> U+24BA
  DEADTRANS(0x0b9b, L'F'  , 0x24bb, NORMAL_CHARACTER),	// ‘F’ -> U+24BB
  DEADTRANS(0x0b9b, L'G'  , 0x24bc, NORMAL_CHARACTER),	// ‘G’ -> U+24BC
  DEADTRANS(0x0b9b, L'H'  , 0x24bd, NORMAL_CHARACTER),	// ‘H’ -> U+24BD
  DEADTRANS(0x0b9b, L'I'  , 0x24be, NORMAL_CHARACTER),	// ‘I’ -> U+24BE
  DEADTRANS(0x0b9b, L'J'  , 0x24bf, NORMAL_CHARACTER),	// ‘J’ -> U+24BF
  DEADTRANS(0x0b9b, L'K'  , 0x24c0, NORMAL_CHARACTER),	// ‘K’ -> U+24C0
  DEADTRANS(0x0b9b, L'L'  , 0x24c1, NORMAL_CHARACTER),	// ‘L’ -> U+24C1
  DEADTRANS(0x0b9b, L'M'  , 0x24c2, NORMAL_CHARACTER),	// ‘M’ -> U+24C2
  DEADTRANS(0x0b9b, L'N'  , 0x24c3, NORMAL_CHARACTER),	// ‘N’ -> U+24C3
  DEADTRANS(0x0b9b, L'O'  , 0x24c4, NORMAL_CHARACTER),	// ‘O’ -> U+24C4
  DEADTRANS(0x0b9b, L'P'  , 0x24c5, NORMAL_CHARACTER),	// ‘P’ -> U+24C5
  DEADTRANS(0x0b9b, L'Q'  , 0x24c6, NORMAL_CHARACTER),	// ‘Q’ -> U+24C6
  DEADTRANS(0x0b9b, L'R'  , 0x24c7, NORMAL_CHARACTER),	// ‘R’ -> U+24C7
  DEADTRANS(0x0b9b, L'S'  , 0x24c8, NORMAL_CHARACTER),	// ‘S’ -> U+24C8
  DEADTRANS(0x0b9b, L'T'  , 0x24c9, NORMAL_CHARACTER),	// ‘T’ -> U+24C9
  DEADTRANS(0x0b9b, L'U'  , 0x24ca, NORMAL_CHARACTER),	// ‘U’ -> U+24CA
  DEADTRANS(0x0b9b, L'V'  , 0x24cb, NORMAL_CHARACTER),	// ‘V’ -> U+24CB
  DEADTRANS(0x0b9b, L'W'  , 0x24cc, NORMAL_CHARACTER),	// ‘W’ -> U+24CC
  DEADTRANS(0x0b9b, L'X'  , 0x24cd, NORMAL_CHARACTER),	// ‘X’ -> U+24CD
  DEADTRANS(0x0b9b, L'Y'  , 0x24ce, NORMAL_CHARACTER),	// ‘Y’ -> U+24CE
  DEADTRANS(0x0b9b, L'Z'  , 0x24cf, NORMAL_CHARACTER),	// ‘Z’ -> U+24CF
  DEADTRANS(0x0b9b, L'a'  , 0x24d0, NORMAL_CHARACTER),	// ‘a’ -> U+24D0
  DEADTRANS(0x0b9b, L'b'  , 0x24d1, NORMAL_CHARACTER),	// ‘b’ -> U+24D1
  DEADTRANS(0x0b9b, L'c'  , 0x24d2, NORMAL_CHARACTER),	// ‘c’ -> U+24D2
  DEADTRANS(0x0b9b, L'd'  , 0x24d3, NORMAL_CHARACTER),	// ‘d’ -> U+24D3
  DEADTRANS(0x0b9b, L'e'  , 0x24d4, NORMAL_CHARACTER),	// ‘e’ -> U+24D4
  DEADTRANS(0x0b9b, L'f'  , 0x24d5, NORMAL_CHARACTER),	// ‘f’ -> U+24D5
  DEADTRANS(0x0b9b, L'g'  , 0x24d6, NORMAL_CHARACTER),	// ‘g’ -> U+24D6
  DEADTRANS(0x0b9b, L'h'  , 0x24d7, NORMAL_CHARACTER),	// ‘h’ -> U+24D7
  DEADTRANS(0x0b9b, L'i'  , 0x24d8, NORMAL_CHARACTER),	// ‘i’ -> U+24D8
  DEADTRANS(0x0b9b, L'j'  , 0x24d9, NORMAL_CHARACTER),	// ‘j’ -> U+24D9
  DEADTRANS(0x0b9b, L'k'  , 0x24da, NORMAL_CHARACTER),	// ‘k’ -> U+24DA
  DEADTRANS(0x0b9b, L'l'  , 0x24db, NORMAL_CHARACTER),	// ‘l’ -> U+24DB
  DEADTRANS(0x0b9b, L'm'  , 0x24dc, NORMAL_CHARACTER),	// ‘m’ -> U+24DC
  DEADTRANS(0x0b9b, L'n'  , 0x24dd, NORMAL_CHARACTER),	// ‘n’ -> U+24DD
  DEADTRANS(0x0b9b, L'o'  , 0x24de, NORMAL_CHARACTER),	// ‘o’ -> U+24DE
  DEADTRANS(0x0b9b, L'p'  , 0x24df, NORMAL_CHARACTER),	// ‘p’ -> U+24DF
  DEADTRANS(0x0b9b, L'q'  , 0x24e0, NORMAL_CHARACTER),	// ‘q’ -> U+24E0
  DEADTRANS(0x0b9b, L'r'  , 0x24e1, NORMAL_CHARACTER),	// ‘r’ -> U+24E1
  DEADTRANS(0x0b9b, L's'  , 0x24e2, NORMAL_CHARACTER),	// ‘s’ -> U+24E2
  DEADTRANS(0x0b9b, L't'  , 0x24e3, NORMAL_CHARACTER),	// ‘t’ -> U+24E3
  DEADTRANS(0x0b9b, L'u'  , 0x24e4, NORMAL_CHARACTER),	// ‘u’ -> U+24E4
  DEADTRANS(0x0b9b, L'v'  , 0x24e5, NORMAL_CHARACTER),	// ‘v’ -> U+24E5
  DEADTRANS(0x0b9b, L'w'  , 0x24e6, NORMAL_CHARACTER),	// ‘w’ -> U+24E6
  DEADTRANS(0x0b9b, L'x'  , 0x24e7, NORMAL_CHARACTER),	// ‘x’ -> U+24E7
  DEADTRANS(0x0b9b, L'y'  , 0x24e8, NORMAL_CHARACTER),	// ‘y’ -> U+24E8
  DEADTRANS(0x0b9b, L'z'  , 0x24e9, NORMAL_CHARACTER),	// ‘z’ -> U+24E9
  DEADTRANS(0x0b9b, 0x266b, 0x0ba2, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + Compose + @ + Compose›
  DEADTRANS(0x0b9b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + @ + %
  DEADTRANS(0x0b9d, L'0'  , 0x24ff, NORMAL_CHARACTER),	// ‘0’ -> ‘⓿’
  DEADTRANS(0x0b9d, L'1'  , 0x2776, NORMAL_CHARACTER),	// ‘1’ -> ‘❶’
  DEADTRANS(0x0b9d, L'2'  , 0x2777, NORMAL_CHARACTER),	// ‘2’ -> ‘❷’
  DEADTRANS(0x0b9d, L'3'  , 0x2778, NORMAL_CHARACTER),	// ‘3’ -> ‘❸’
  DEADTRANS(0x0b9d, L'4'  , 0x2779, NORMAL_CHARACTER),	// ‘4’ -> ‘❹’
  DEADTRANS(0x0b9d, L'5'  , 0x277a, NORMAL_CHARACTER),	// ‘5’ -> ‘❺’
  DEADTRANS(0x0b9d, L'6'  , 0x277b, NORMAL_CHARACTER),	// ‘6’ -> ‘❻’
  DEADTRANS(0x0b9d, L'7'  , 0x277c, NORMAL_CHARACTER),	// ‘7’ -> ‘❼’
  DEADTRANS(0x0b9d, L'8'  , 0x277d, NORMAL_CHARACTER),	// ‘8’ -> ‘❽’
  DEADTRANS(0x0b9d, L'9'  , 0x277e, NORMAL_CHARACTER),	// ‘9’ -> ‘❾’
  DEADTRANS(0x0b9d, 0x266b, 0x0ba0, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + Compose + @ + % + Compose›
  DEADTRANS(0x0b9d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + @ + % + Compose
  DEADTRANS(0x0ba0, L'0'  , 0x277f, NORMAL_CHARACTER),	// ‘0’ -> ‘❿’
  DEADTRANS(0x0ba0, L'1'  , 0x24eb, NORMAL_CHARACTER),	// ‘1’ -> ‘⓫’
  DEADTRANS(0x0ba0, L'2'  , 0x24ec, NORMAL_CHARACTER),	// ‘2’ -> ‘⓬’
  DEADTRANS(0x0ba0, L'3'  , 0x24ed, NORMAL_CHARACTER),	// ‘3’ -> ‘⓭’
  DEADTRANS(0x0ba0, L'4'  , 0x24ee, NORMAL_CHARACTER),	// ‘4’ -> ‘⓮’
  DEADTRANS(0x0ba0, L'5'  , 0x24ef, NORMAL_CHARACTER),	// ‘5’ -> ‘⓯’
  DEADTRANS(0x0ba0, L'6'  , 0x24f0, NORMAL_CHARACTER),	// ‘6’ -> ‘⓰’
  DEADTRANS(0x0ba0, L'7'  , 0x24f1, NORMAL_CHARACTER),	// ‘7’ -> ‘⓱’
  DEADTRANS(0x0ba0, L'8'  , 0x24f2, NORMAL_CHARACTER),	// ‘8’ -> ‘⓲’
  DEADTRANS(0x0ba0, L'9'  , 0x24f3, NORMAL_CHARACTER),	// ‘9’ -> ‘⓳’
  DEADTRANS(0x0ba0, 0x266b, 0x0ba1, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + Compose + @ + % + Compose + Compose›
  DEADTRANS(0x0ba0, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + @ + % + Compose + Compose
  DEADTRANS(0x0ba1, L'0'  , 0x24f4, NORMAL_CHARACTER),	// ‘0’ -> ‘⓴’
  DEADTRANS(0x0ba1, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + @ + Compose
  DEADTRANS(0x0ba2, L'0'  , 0x2469, NORMAL_CHARACTER),	// ‘0’ -> ‘⑩’
  DEADTRANS(0x0ba2, L'1'  , 0x246a, NORMAL_CHARACTER),	// ‘1’ -> ‘⑪’
  DEADTRANS(0x0ba2, L'2'  , 0x246b, NORMAL_CHARACTER),	// ‘2’ -> ‘⑫’
  DEADTRANS(0x0ba2, L'3'  , 0x246c, NORMAL_CHARACTER),	// ‘3’ -> ‘⑬’
  DEADTRANS(0x0ba2, L'4'  , 0x246d, NORMAL_CHARACTER),	// ‘4’ -> ‘⑭’
  DEADTRANS(0x0ba2, L'5'  , 0x246e, NORMAL_CHARACTER),	// ‘5’ -> ‘⑮’
  DEADTRANS(0x0ba2, L'6'  , 0x246f, NORMAL_CHARACTER),	// ‘6’ -> ‘⑯’
  DEADTRANS(0x0ba2, L'7'  , 0x2470, NORMAL_CHARACTER),	// ‘7’ -> ‘⑰’
  DEADTRANS(0x0ba2, L'8'  , 0x2471, NORMAL_CHARACTER),	// ‘8’ -> ‘⑱’
  DEADTRANS(0x0ba2, L'9'  , 0x2472, NORMAL_CHARACTER),	// ‘9’ -> ‘⑲’
  DEADTRANS(0x0ba2, 0x266b, 0x0ba5, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + Compose + @ + Compose + Compose›
  DEADTRANS(0x0ba2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + @ + Compose + Compose
  DEADTRANS(0x0ba5, L'0'  , 0x2473, NORMAL_CHARACTER),	// ‘0’ -> ‘⑳’
  DEADTRANS(0x0ba5, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Compose + Compose + Compose
  DEADTRANS(0x0ba6, L'!'  , 0x2762, NORMAL_CHARACTER),	// U+0021 -> U+2762
  DEADTRANS(0x0ba6, L'%'  , 0x2031, NORMAL_CHARACTER),	// U+0025 -> U+2031
  DEADTRANS(0x0ba6, L'*'  , 0x2042, NORMAL_CHARACTER),	// U+002A -> U+2042
  DEADTRANS(0x0ba6, L'-'  , 0x2014, NORMAL_CHARACTER),	// U+002D -> U+2014
  DEADTRANS(0x0ba6, L'.'  , 0x2026, NORMAL_CHARACTER),	// U+002E -> U+2026
  DEADTRANS(0x0ba6, L'/'  , 0x2afb, NORMAL_CHARACTER),	// U+002F -> U+2AFB
  DEADTRANS(0x0ba6, L'<'  , 0x22d8, NORMAL_CHARACTER),	// U+003C -> U+22D8
  DEADTRANS(0x0ba6, L'>'  , 0x22d9, NORMAL_CHARACTER),	// U+003E -> U+22D9
  DEADTRANS(0x0ba6, L'|'  , 0x2980, NORMAL_CHARACTER),	// U+007C -> U+2980
  DEADTRANS(0x0ba6, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Reversed Comma Above
  DEADTRANS(0x201b, L'.'  , 0x0314, NORMAL_CHARACTER),	// U+002E -> U+0314
  DEADTRANS(0x201b, 0x00a0, 0x201b, NORMAL_CHARACTER),	// U+00A0 -> U+201B
  DEADTRANS(0x201b, 0x03a9, 0x1f69, NORMAL_CHARACTER),	// ‘Ω’ -> ‘Ὡ’
  DEADTRANS(0x201b, 0x03b1, 0x1f01, NORMAL_CHARACTER),	// ‘α’ -> ‘ἁ’
  DEADTRANS(0x201b, 0x03b5, 0x1f11, NORMAL_CHARACTER),	// ‘ε’ -> ‘ἑ’
  DEADTRANS(0x201b, 0x03b7, 0x1f21, NORMAL_CHARACTER),	// ‘η’ -> ‘ἡ’
  DEADTRANS(0x201b, 0x03b9, 0x1f31, NORMAL_CHARACTER),	// ‘ι’ -> ‘ἱ’
  DEADTRANS(0x201b, 0x03bf, 0x1f41, NORMAL_CHARACTER),	// ‘ο’ -> ‘ὁ’
  DEADTRANS(0x201b, 0x03c1, 0x1fe5, NORMAL_CHARACTER),	// ‘ρ’ -> ‘ῥ’
  DEADTRANS(0x201b, 0x03c5, 0x1f51, NORMAL_CHARACTER),	// ‘υ’ -> ‘ὑ’
  DEADTRANS(0x201b, 0x03c9, 0x1f61, NORMAL_CHARACTER),	// ‘ω’ -> ‘ὡ’
  DEADTRANS(0x201b, L'`'  , 0x085d, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Reversed Comma Above + Grave Accent›
  DEADTRANS(0x201b, 0x00b4, 0x085f, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Reversed Comma Above + Acute Accent›
  DEADTRANS(0x201b, L' '  , 0x02bd, NORMAL_CHARACTER),	// U+0020 -> ‘ʽ’

// Dead key: Diaeresis Below
  DEADTRANS(0x2025, L'.'  , 0x0324, NORMAL_CHARACTER),	// U+002E -> U+0324
  DEADTRANS(0x2025, L'U'  , 0x1e72, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṳ’
  DEADTRANS(0x2025, L'u'  , 0x1e73, NORMAL_CHARACTER),	// ‘u’ -> ‘ṳ’
  DEADTRANS(0x2025, 0x00a0, 0x2025, NORMAL_CHARACTER),	// U+00A0 -> U+2025
  DEADTRANS(0x2025, L' '  , 0x2025, NORMAL_CHARACTER),	// U+0020 -> U+2025

// Dead key: Three Dots Above
  DEADTRANS(0x20db, L'.'  , 0x20db, NORMAL_CHARACTER),	// U+002E -> U+20DB
  DEADTRANS(0x20db, L' '  , 0x20db, NORMAL_CHARACTER),	// U+0020 -> U+20DB

// Dead key: Triple Underdot
  DEADTRANS(0x20e8, L'.'  , 0x20e8, NORMAL_CHARACTER),	// U+002E -> U+20E8
  DEADTRANS(0x20e8, L' '  , 0x20e8, NORMAL_CHARACTER),	// U+0020 -> U+20E8

// Dead key: Drehen
  DEADTRANS(0x21bb, L'A'  , 0x2200, NORMAL_CHARACTER),	// ‘A’ -> U+2200
  DEADTRANS(0x21bb, L' '  , 0x21bb, NORMAL_CHARACTER),	// U+0020 -> U+21BB

// Dead key: Compose
  DEADTRANS(0x266b, L'!'  , 0x086c, CHAINED_DEAD_KEY),	// U+0021 -> ‹Compose + !›
  DEADTRANS(0x266b, L'"'  , 0x086d, CHAINED_DEAD_KEY),	// U+0022 -> ‹Compose + "›
  DEADTRANS(0x266b, L'#'  , 0x086e, CHAINED_DEAD_KEY),	// U+0023 -> ‹Compose + #›
  DEADTRANS(0x266b, L'$'  , 0x086f, CHAINED_DEAD_KEY),	// U+0024 -> ‹Compose + $›
  DEADTRANS(0x266b, L'%'  , 0x0870, CHAINED_DEAD_KEY),	// U+0025 -> ‹Compose + %›
  DEADTRANS(0x266b, L'&'  , 0x0871, CHAINED_DEAD_KEY),	// U+0026 -> ‹Compose + &›
  DEADTRANS(0x266b, L'\'' , 0x0872, CHAINED_DEAD_KEY),	// U+0027 -> ‹Compose + '›
  DEADTRANS(0x266b, L'('  , 0x0873, CHAINED_DEAD_KEY),	// U+0028 -> ‹Compose + (›
  DEADTRANS(0x266b, L')'  , 0x0874, CHAINED_DEAD_KEY),	// U+0029 -> ‹Compose + )›
  DEADTRANS(0x266b, L'*'  , 0x0875, CHAINED_DEAD_KEY),	// U+002A -> ‹Compose + *›
  DEADTRANS(0x266b, L'+'  , 0x0876, CHAINED_DEAD_KEY),	// U+002B -> ‹Compose + +›
  DEADTRANS(0x266b, L','  , 0x0877, CHAINED_DEAD_KEY),	// U+002C -> ‹Compose + ,›
  DEADTRANS(0x266b, L'-'  , 0x0878, CHAINED_DEAD_KEY),	// U+002D -> ‹Compose + -›
  DEADTRANS(0x266b, L'.'  , 0x0879, CHAINED_DEAD_KEY),	// U+002E -> ‹Compose + .›
  DEADTRANS(0x266b, L'/'  , 0x087a, CHAINED_DEAD_KEY),	// U+002F -> ‹Compose + /›
  DEADTRANS(0x266b, L'0'  , 0x087b, CHAINED_DEAD_KEY),	// ‘0’ -> ‹Compose + 0›
  DEADTRANS(0x266b, L'1'  , 0x087c, CHAINED_DEAD_KEY),	// ‘1’ -> ‹Compose + 1›
  DEADTRANS(0x266b, L'2'  , 0x087d, CHAINED_DEAD_KEY),	// ‘2’ -> ‹Compose + 2›
  DEADTRANS(0x266b, L'3'  , 0x087e, CHAINED_DEAD_KEY),	// ‘3’ -> ‹Compose + 3›
  DEADTRANS(0x266b, L'4'  , 0x087f, CHAINED_DEAD_KEY),	// ‘4’ -> ‹Compose + 4›
  DEADTRANS(0x266b, L'5'  , 0x0880, CHAINED_DEAD_KEY),	// ‘5’ -> ‹Compose + 5›
  DEADTRANS(0x266b, L'7'  , 0x0881, CHAINED_DEAD_KEY),	// ‘7’ -> ‹Compose + 7›
  DEADTRANS(0x266b, L'8'  , 0x0882, CHAINED_DEAD_KEY),	// ‘8’ -> ‹Compose + 8›
  DEADTRANS(0x266b, L'9'  , 0x0883, CHAINED_DEAD_KEY),	// ‘9’ -> ‹Compose + 9›
  DEADTRANS(0x266b, L':'  , 0x0884, CHAINED_DEAD_KEY),	// U+003A -> ‹Compose + :›
  DEADTRANS(0x266b, L';'  , 0x0885, CHAINED_DEAD_KEY),	// U+003B -> ‹Compose + ;›
  DEADTRANS(0x266b, L'<'  , 0x0886, CHAINED_DEAD_KEY),	// U+003C -> ‹Compose + <›
  DEADTRANS(0x266b, L'='  , 0x0887, CHAINED_DEAD_KEY),	// U+003D -> ‹Compose + =›
  DEADTRANS(0x266b, L'>'  , 0x0888, CHAINED_DEAD_KEY),	// U+003E -> ‹Compose + >›
  DEADTRANS(0x266b, L'?'  , 0x0889, CHAINED_DEAD_KEY),	// U+003F -> ‹Compose + ?›
  DEADTRANS(0x266b, L'@'  , 0x088a, CHAINED_DEAD_KEY),	// U+0040 -> ‹Compose + @›
  DEADTRANS(0x266b, L'A'  , 0x088b, CHAINED_DEAD_KEY),	// ‘A’ -> ‹Compose + A›
  DEADTRANS(0x266b, L'C'  , 0x088c, CHAINED_DEAD_KEY),	// ‘C’ -> ‹Compose + C›
  DEADTRANS(0x266b, L'D'  , 0x088d, CHAINED_DEAD_KEY),	// ‘D’ -> ‹Compose + D›
  DEADTRANS(0x266b, L'E'  , 0x088e, CHAINED_DEAD_KEY),	// ‘E’ -> ‹Compose + E›
  DEADTRANS(0x266b, L'F'  , 0x088f, CHAINED_DEAD_KEY),	// ‘F’ -> ‹Compose + F›
  DEADTRANS(0x266b, L'G'  , 0x0890, CHAINED_DEAD_KEY),	// ‘G’ -> ‹Compose + G›
  DEADTRANS(0x266b, L'H'  , 0x0891, CHAINED_DEAD_KEY),	// ‘H’ -> ‹Compose + H›
  DEADTRANS(0x266b, L'I'  , 0x0892, CHAINED_DEAD_KEY),	// ‘I’ -> ‹Compose + I›
  DEADTRANS(0x266b, L'L'  , 0x0893, CHAINED_DEAD_KEY),	// ‘L’ -> ‹Compose + L›
  DEADTRANS(0x266b, L'M'  , 0x0894, CHAINED_DEAD_KEY),	// ‘M’ -> ‹Compose + M›
  DEADTRANS(0x266b, L'N'  , 0x0895, CHAINED_DEAD_KEY),	// ‘N’ -> ‹Compose + N›
  DEADTRANS(0x266b, L'O'  , 0x0896, CHAINED_DEAD_KEY),	// ‘O’ -> ‹Compose + O›
  DEADTRANS(0x266b, L'R'  , 0x0897, CHAINED_DEAD_KEY),	// ‘R’ -> ‹Compose + R›
  DEADTRANS(0x266b, L'S'  , 0x0898, CHAINED_DEAD_KEY),	// ‘S’ -> ‹Compose + S›
  DEADTRANS(0x266b, L'T'  , 0x0899, CHAINED_DEAD_KEY),	// ‘T’ -> ‹Compose + T›
  DEADTRANS(0x266b, L'V'  , 0x089a, CHAINED_DEAD_KEY),	// ‘V’ -> ‹Compose + V›
  DEADTRANS(0x266b, L'W'  , 0x089b, CHAINED_DEAD_KEY),	// ‘W’ -> ‹Compose + W›
  DEADTRANS(0x266b, L'X'  , 0x089c, CHAINED_DEAD_KEY),	// ‘X’ -> ‹Compose + X›
  DEADTRANS(0x266b, L'Y'  , 0x089d, CHAINED_DEAD_KEY),	// ‘Y’ -> ‹Compose + Y›
  DEADTRANS(0x266b, L'Z'  , 0x089e, CHAINED_DEAD_KEY),	// ‘Z’ -> ‹Compose + Z›
  DEADTRANS(0x266b, L'['  , 0x089f, CHAINED_DEAD_KEY),	// U+005B -> ‹Compose + [›
  DEADTRANS(0x266b, L'\\' , 0x08b5, CHAINED_DEAD_KEY),	// U+005C -> ‹Compose + \›
  DEADTRANS(0x266b, L']'  , 0x08be, CHAINED_DEAD_KEY),	// U+005D -> ‹Compose + ]›
  DEADTRANS(0x266b, L'^'  , 0x08bf, CHAINED_DEAD_KEY),	// U+005E -> ‹Compose + ^›
  DEADTRANS(0x266b, L'_'  , 0x08c0, CHAINED_DEAD_KEY),	// U+005F -> ‹Compose + _›
  DEADTRANS(0x266b, L'`'  , 0x08c1, CHAINED_DEAD_KEY),	// U+0060 -> ‹Compose + `›
  DEADTRANS(0x266b, L'a'  , 0x08c2, CHAINED_DEAD_KEY),	// ‘a’ -> ‹Compose + a›
  DEADTRANS(0x266b, L'b'  , 0x08c3, CHAINED_DEAD_KEY),	// ‘b’ -> ‹Compose + b›
  DEADTRANS(0x266b, L'c'  , 0x08c4, CHAINED_DEAD_KEY),	// ‘c’ -> ‹Compose + c›
  DEADTRANS(0x266b, L'd'  , 0x08c5, CHAINED_DEAD_KEY),	// ‘d’ -> ‹Compose + d›
  DEADTRANS(0x266b, L'e'  , 0x08c6, CHAINED_DEAD_KEY),	// ‘e’ -> ‹Compose + e›
  DEADTRANS(0x266b, L'f'  , 0x08c7, CHAINED_DEAD_KEY),	// ‘f’ -> ‹Compose + f›
  DEADTRANS(0x266b, L'g'  , 0x08c8, CHAINED_DEAD_KEY),	// ‘g’ -> ‹Compose + g›
  DEADTRANS(0x266b, L'h'  , 0x08c9, CHAINED_DEAD_KEY),	// ‘h’ -> ‹Compose + h›
  DEADTRANS(0x266b, L'i'  , 0x08ca, CHAINED_DEAD_KEY),	// ‘i’ -> ‹Compose + i›
  DEADTRANS(0x266b, L'j'  , 0x08cb, CHAINED_DEAD_KEY),	// ‘j’ -> ‹Compose + j›
  DEADTRANS(0x266b, L'k'  , 0x08cc, CHAINED_DEAD_KEY),	// ‘k’ -> ‹Compose + k›
  DEADTRANS(0x266b, L'l'  , 0x08cd, CHAINED_DEAD_KEY),	// ‘l’ -> ‹Compose + l›
  DEADTRANS(0x266b, L'm'  , 0x08ce, CHAINED_DEAD_KEY),	// ‘m’ -> ‹Compose + m›
  DEADTRANS(0x266b, L'n'  , 0x08cf, CHAINED_DEAD_KEY),	// ‘n’ -> ‹Compose + n›
  DEADTRANS(0x266b, L'o'  , 0x08d0, CHAINED_DEAD_KEY),	// ‘o’ -> ‹Compose + o›
  DEADTRANS(0x266b, L'p'  , 0x08d1, CHAINED_DEAD_KEY),	// ‘p’ -> ‹Compose + p›
  DEADTRANS(0x266b, L'q'  , 0x08d2, CHAINED_DEAD_KEY),	// ‘q’ -> ‹Compose + q›
  DEADTRANS(0x266b, L'r'  , 0x0984, CHAINED_DEAD_KEY),	// ‘r’ -> ‹Compose + r›
  DEADTRANS(0x266b, L's'  , 0x098d, CHAINED_DEAD_KEY),	// ‘s’ -> ‹Compose + s›
  DEADTRANS(0x266b, L't'  , 0x098e, CHAINED_DEAD_KEY),	// ‘t’ -> ‹Compose + t›
  DEADTRANS(0x266b, L'u'  , 0x0991, CHAINED_DEAD_KEY),	// ‘u’ -> ‹Compose + u›
  DEADTRANS(0x266b, L'v'  , 0x0992, CHAINED_DEAD_KEY),	// ‘v’ -> ‹Compose + v›
  DEADTRANS(0x266b, L'w'  , 0x09a9, CHAINED_DEAD_KEY),	// ‘w’ -> ‹Compose + w›
  DEADTRANS(0x266b, L'x'  , 0x09b1, CHAINED_DEAD_KEY),	// ‘x’ -> ‹Compose + x›
  DEADTRANS(0x266b, L'y'  , 0x09b3, CHAINED_DEAD_KEY),	// ‘y’ -> ‹Compose + y›
  DEADTRANS(0x266b, L'z'  , 0x09b4, CHAINED_DEAD_KEY),	// ‘z’ -> ‹Compose + z›
  DEADTRANS(0x266b, L'{'  , 0x09b5, CHAINED_DEAD_KEY),	// U+007B -> ‹Compose + {›
  DEADTRANS(0x266b, L'|'  , 0x09ba, CHAINED_DEAD_KEY),	// U+007C -> ‹Compose + |›
  DEADTRANS(0x266b, L'}'  , 0x09bb, CHAINED_DEAD_KEY),	// U+007D -> ‹Compose + }›
  DEADTRANS(0x266b, L'~'  , 0x09c5, CHAINED_DEAD_KEY),	// U+007E -> ‹Compose + ~›
  DEADTRANS(0x266b, 0x00b0, 0x09c6, CHAINED_DEAD_KEY),	// U+00B0 -> ‹Compose + °›
  DEADTRANS(0x266b, 0x00b7, 0x09c9, CHAINED_DEAD_KEY),	// U+00B7 -> ‹Compose + ·›
  DEADTRANS(0x266b, 0x2018, 0x09ca, CHAINED_DEAD_KEY),	// U+2018 -> ‹Compose + ‘›
  DEADTRANS(0x266b, 0x2019, 0x09cf, CHAINED_DEAD_KEY),	// U+2019 -> ‹Compose + ’›
  DEADTRANS(0x266b, 0x20ac, 0x09d0, CHAINED_DEAD_KEY),	// U+20AC -> ‹Compose + €›
  DEADTRANS(0x266b, 0x27e8, 0x09d1, CHAINED_DEAD_KEY),	// U+27E8 -> ‹Compose + ⟨›
  DEADTRANS(0x266b, 0x27e9, 0x09d2, CHAINED_DEAD_KEY),	// U+27E9 -> ‹Compose + ⟩›
  DEADTRANS(0x266b, 0x266b, 0x09d3, CHAINED_DEAD_KEY),	// ‹Compose› -> ‹Compose + Compose›
  DEADTRANS(0x266b, L' '  , 0x086b, CHAINED_DEAD_KEY),	// U+0020 -> ‹Compose +  ›

  0, 0
};

/***************************************************************************\\
 * Ligatures
\\***************************************************************************/


static ALLOC_SECTION_LDATA KBDTABLES KbdTables = {
  // Modifier keys
  &CharModifiers,

  // Characters tables
  aVkToWcharTable,

  // Diacritics
  aDeadKey,

  // Names of Keys
  aKeyNames,
  aKeyNamesExt,
  aKeyNamesDead,

  // Scan codes to Virtual Keys
  ausVK,
  sizeof(ausVK) / sizeof(ausVK[0]),
  aE0VscToVk,
  aE1VscToVk,

  // Locale-specific special processing
  MAKELONG(0, KBD_VERSION),

  // Ligatures
  0, 0, NULL // No ligatures
};

PKBDTABLES KbdLayerDescriptor(VOID)
{
  return &KbdTables;
};
