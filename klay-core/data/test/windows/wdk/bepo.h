
#define KBD_TYPE 4
#include "kbd.h"
#include <dontuse.h>

// Extra definitions for FE keyboards [FIXME]
#define VK_DBE_ALPHANUMERIC              0x0f0
#define VK_DBE_KATAKANA                  0x0f1
#define VK_DBE_HIRAGANA                  0x0f2
#define VK_DBE_ROMAN                     0x0f5
#define VK_DBE_NOROMAN                   0x0f6

// Modifiers bits
// Windows predefined values:
// * KBDSHIFT       1<<0
// * KBDCTRL        1<<1
// * KBDALT         1<<2
// * KBDKANA        1<<3
// * KBDROYA        1<<4
// * KBDLOYA        1<<5
// * KBDGRPSELTAP   1<<7
#define KBD_NUMERIC     1<<3
#define KBD_ISO_LEVEL_3 1<<4
#define KBD_ISO_LEVEL_5 1<<5

// Extra definitions of TYPEDEF_VK_TO_WCHARS
TYPEDEF_VK_TO_WCHARS(11) //  VK_TO_WCHARS11, *PVK_TO_WCHARS11;
TYPEDEF_VK_TO_WCHARS(12) //  VK_TO_WCHARS12, *PVK_TO_WCHARS12;
TYPEDEF_VK_TO_WCHARS(13) //  VK_TO_WCHARS13, *PVK_TO_WCHARS13;
TYPEDEF_VK_TO_WCHARS(14) //  VK_TO_WCHARS14, *PVK_TO_WCHARS14;
TYPEDEF_VK_TO_WCHARS(15) //  VK_TO_WCHARS15, *PVK_TO_WCHARS15;
TYPEDEF_VK_TO_WCHARS(16) //  VK_TO_WCHARS16, *PVK_TO_WCHARS16;
TYPEDEF_VK_TO_WCHARS(17) //  VK_TO_WCHARS17, *PVK_TO_WCHARS17;
TYPEDEF_VK_TO_WCHARS(18) //  VK_TO_WCHARS18, *PVK_TO_WCHARS18;
TYPEDEF_VK_TO_WCHARS(19) //  VK_TO_WCHARS19, *PVK_TO_WCHARS19;
TYPEDEF_VK_TO_WCHARS(20) //  VK_TO_WCHARS20, *PVK_TO_WCHARS20;
TYPEDEF_VK_TO_WCHARS(21) //  VK_TO_WCHARS21, *PVK_TO_WCHARS21;
TYPEDEF_VK_TO_WCHARS(22) //  VK_TO_WCHARS22, *PVK_TO_WCHARS22;
TYPEDEF_VK_TO_WCHARS(23) //  VK_TO_WCHARS23, *PVK_TO_WCHARS23;
TYPEDEF_VK_TO_WCHARS(24) //  VK_TO_WCHARS24, *PVK_TO_WCHARS24;
TYPEDEF_VK_TO_WCHARS(25) //  VK_TO_WCHARS25, *PVK_TO_WCHARS25;
TYPEDEF_VK_TO_WCHARS(26) //  VK_TO_WCHARS26, *PVK_TO_WCHARS26;
TYPEDEF_VK_TO_WCHARS(27) //  VK_TO_WCHARS27, *PVK_TO_WCHARS27;
TYPEDEF_VK_TO_WCHARS(28) //  VK_TO_WCHARS28, *PVK_TO_WCHARS28;
TYPEDEF_VK_TO_WCHARS(29) //  VK_TO_WCHARS29, *PVK_TO_WCHARS29;
TYPEDEF_VK_TO_WCHARS(30) //  VK_TO_WCHARS30, *PVK_TO_WCHARS30;
TYPEDEF_VK_TO_WCHARS(31) //  VK_TO_WCHARS31, *PVK_TO_WCHARS31;
TYPEDEF_VK_TO_WCHARS(32) //  VK_TO_WCHARS32, *PVK_TO_WCHARS32;

// Swap first arguments of the macro of the Windows API.
#undef DEADTRANS
#define DEADTRANS(dk, ch, comp, flags) { MAKELONG(ch, dk), comp, flags}

// Flag for the result of dead key combo
#define NORMAL_CHARACTER 0x0000
#define CHAINED_DEAD_KEY 0x0001 // DKF_DEAD

// Extra definitions of TYPEDEF_LIGATURE
TYPEDEF_LIGATURE(6)
TYPEDEF_LIGATURE(7)
TYPEDEF_LIGATURE(8)
TYPEDEF_LIGATURE(9)
TYPEDEF_LIGATURE(10)
TYPEDEF_LIGATURE(11)
TYPEDEF_LIGATURE(12)
TYPEDEF_LIGATURE(13)
TYPEDEF_LIGATURE(14)
TYPEDEF_LIGATURE(15)
TYPEDEF_LIGATURE(16)

// Mapping scancode -> virtual key
// Key Q -> VK_B
#undef  T10
#define T10 _EQ('B')
// Key W -> VK_OEM_1
#undef  T11
#define T11 _EQ(OEM_1)
// Key E -> VK_P
#undef  T12
#define T12 _EQ('P')
// Key R -> VK_O
#undef  T13
#define T13 _EQ('O')
// Key T -> VK_OEM_2
#undef  T14
#define T14 _EQ(OEM_2)
// Key Y -> VK_OEM_4
#undef  T15
#define T15 _EQ(OEM_4)
// Key U -> VK_V
#undef  T16
#define T16 _EQ('V')
// Key I -> VK_D
#undef  T17
#define T17 _EQ('D')
// Key O -> VK_L
#undef  T18
#define T18 _EQ('L')
// Key P -> VK_J
#undef  T19
#define T19 _EQ('J')
// Key LBracket -> VK_Z
#undef  T1A
#define T1A _EQ('Z')
// Key RBracket -> VK_W
#undef  T1B
#define T1B _EQ('W')
// Key S -> VK_U
#undef  T1F
#define T1F _EQ('U')
// Key D -> VK_I
#undef  T20
#define T20 _EQ('I')
// Key F -> VK_E
#undef  T21
#define T21 _EQ('E')
// Key G -> VK_OEM_COMMA
#undef  T22
#define T22 _EQ(OEM_COMMA)
// Key H -> VK_C
#undef  T23
#define T23 _EQ('C')
// Key J -> VK_T
#undef  T24
#define T24 _EQ('T')
// Key K -> VK_S
#undef  T25
#define T25 _EQ('S')
// Key L -> VK_R
#undef  T26
#define T26 _EQ('R')
// Key Semicolon -> VK_N
#undef  T27
#define T27 _EQ('N')
// Key Quote -> VK_M
#undef  T28
#define T28 _EQ('M')
// Key Grave -> VK_OEM_7
#undef  T29
#define T29 _EQ(OEM_7)
// Key Z -> VK_OEM_6
#undef  T2C
#define T2C _EQ(OEM_6)
// Key X -> VK_Y
#undef  T2D
#define T2D _EQ('Y')
// Key C -> VK_X
#undef  T2E
#define T2E _EQ('X')
// Key V -> VK_OEM_PERIOD
#undef  T2F
#define T2F _EQ(OEM_PERIOD)
// Key B -> VK_K
#undef  T30
#define T30 _EQ('K')
// Key N -> VK_OEM_3
#undef  T31
#define T31 _EQ(OEM_3)
// Key M -> VK_Q
#undef  T32
#define T32 _EQ('Q')
// Key Comma -> VK_G
#undef  T33
#define T33 _EQ('G')
// Key Period -> VK_H
#undef  T34
#define T34 _EQ('H')
// Key Slash -> VK_F
#undef  T35
#define T35 _EQ('F')

