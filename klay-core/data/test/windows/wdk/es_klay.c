
#include <windows.h>
#include "kbd.h"
#include "es_klay.h"

#if defined(_M_IA64)
#pragma section(".data")
#define ALLOC_SECTION_LDATA __declspec(allocate(".data"))
#else
#pragma data_seg(".data")
#define ALLOC_SECTION_LDATA
#endif

// ausVK[] - Virtual Scan Code to Virtual Key conversion table
static ALLOC_SECTION_LDATA USHORT ausVK[] = {
  T00, T01, T02, T03, T04, T05, T06, T07,
  T08, T09, T0A, T0B, T0C, T0D, T0E, T0F,
  T10, T11, T12, T13, T14, T15, T16, T17,
  T18, T19, T1A, T1B, T1C, T1D, T1E, T1F,
  T20, T21, T22, T23, T24, T25, T26, T27,
  T28, T29, T2A, T2B, T2C, T2D, T2E, T2F,
  T30, T31, T32, T33, T34, T35,

  /*
   * Right-hand Shift key must have KBDEXT bit set.
   */
  T36 | KBDEXT,
  T37 | KBDMULTIVK,               // numpad_* + Shift/Alt -> SnapShot

  T38, T39, T3A, T3B, T3C, T3D, T3E,
  T3F, T40, T41, T42, T43, T44,

  /*
   * NumLock Key:
   *     KBDEXT     - VK_NUMLOCK is an Extended key
   *     KBDMULTIVK - VK_NUMLOCK or VK_PAUSE (without or with CTRL)
   */
  T45 | KBDEXT | KBDMULTIVK,
  T46 | KBDMULTIVK,

  /*
   * Number Pad keys:
   *     KBDNUMPAD  - digits 0-9 and decimal point.
   *     KBDSPECIAL - require special processing by Windows
   */
  T47 | KBDNUMPAD | KBDSPECIAL,   // Numpad 7 (Home)
  T48 | KBDNUMPAD | KBDSPECIAL,   // Numpad 8 (Up),
  T49 | KBDNUMPAD | KBDSPECIAL,   // Numpad 9 (PgUp),
  T4A,
  T4B | KBDNUMPAD | KBDSPECIAL,   // Numpad 4 (Left),
  T4C | KBDNUMPAD | KBDSPECIAL,   // Numpad 5 (Clear),
  T4D | KBDNUMPAD | KBDSPECIAL,   // Numpad 6 (Right),
  T4E,
  T4F | KBDNUMPAD | KBDSPECIAL,   // Numpad 1 (End),
  T50 | KBDNUMPAD | KBDSPECIAL,   // Numpad 2 (Down),
  T51 | KBDNUMPAD | KBDSPECIAL,   // Numpad 3 (PgDn),
  T52 | KBDNUMPAD | KBDSPECIAL,   // Numpad 0 (Ins),
  T53 | KBDNUMPAD | KBDSPECIAL,   // Numpad . (Del),

  T54, T55, T56, T57, T58, T59, T5A, T5B,
  T5C, T5D, T5E, T5F, T60, T61, T62, T63,
  T64, T65, T66, T67, T68, T69, T6A, T6B,
  T6C, T6D, T6E, T6F, T70, T71, T72, T73,
  T74, T75, T76, T77, T78, T79, T7A, T7B,
  T7C, T7D, T7E, T7F
};

static ALLOC_SECTION_LDATA VSC_VK aE0VscToVk[] = {
  { 0x10, X10 | KBDEXT              },  // Speedracer: Previous Track
  { 0x19, X19 | KBDEXT              },  // Speedracer: Next Track
  { 0x1D, X1D | KBDEXT              },  // RControl
  { 0x20, X20 | KBDEXT              },  // Speedracer: Volume Mute
  { 0x21, X21 | KBDEXT              },  // Speedracer: Launch App 2
  { 0x22, X22 | KBDEXT              },  // Speedracer: Media Play/Pause
  { 0x24, X24 | KBDEXT              },  // Speedracer: Media Stop
  { 0x2E, X2E | KBDEXT              },  // Speedracer: Volume Down
  { 0x30, X30 | KBDEXT              },  // Speedracer: Volume Up
  { 0x32, X32 | KBDEXT              },  // Speedracer: Browser Home
  { 0x35, X35 | KBDEXT              },  // Numpad Divide
  { 0x37, X37 | KBDEXT              },  // Snapshot
  { 0x38, X38 | KBDEXT              },  // RMenu
  { 0x47, X47 | KBDEXT              },  // Home
  { 0x48, X48 | KBDEXT              },  // Up
  { 0x49, X49 | KBDEXT              },  // Prior
  { 0x4B, X4B | KBDEXT              },  // Left
  { 0x4D, X4D | KBDEXT              },  // Right
  { 0x4F, X4F | KBDEXT              },  // End
  { 0x50, X50 | KBDEXT              },  // Down
  { 0x51, X51 | KBDEXT              },  // Next
  { 0x52, X52 | KBDEXT              },  // Insert
  { 0x53, X53 | KBDEXT              },  // Delete
  { 0x5B, X5B | KBDEXT              },  // Left Win
  { 0x5C, X5C | KBDEXT              },  // Right Win
  { 0x5D, X5D | KBDEXT              },  // Application
  { 0x5F, X5F | KBDEXT              },  // Speedracer: Sleep
  { 0x65, X65 | KBDEXT              },  // Speedracer: Browser Search
  { 0x66, X66 | KBDEXT              },  // Speedracer: Browser Favorites
  { 0x67, X67 | KBDEXT              },  // Speedracer: Browser Refresh
  { 0x68, X68 | KBDEXT              },  // Speedracer: Browser Stop
  { 0x69, X69 | KBDEXT              },  // Speedracer: Browser Forward
  { 0x6A, X6A | KBDEXT              },  // Speedracer: Browser Back
  { 0x6B, X6B | KBDEXT              },  // Speedracer: Launch App 1
  { 0x6C, X6C | KBDEXT              },  // Speedracer: Launch Mail
  { 0x6D, X6D | KBDEXT              },  // Speedracer: Launch Media Selector
  { 0x1C, X1C | KBDEXT              },  // Numpad Enter
  { 0x46, X46 | KBDEXT              },  // Break (Ctrl + Pause)
  { 0,      0                       }
};

static ALLOC_SECTION_LDATA VSC_VK aE1VscToVk[] = {
  { 0x1D, Y1D                       },  // Pause
  { 0   ,   0                       }
};

// aVkToBits[] - map Virtual Keys to Modifier Bits
static ALLOC_SECTION_LDATA VK_TO_BIT aVkToBits[] = {
  { VK_CONTROL   , KBDCTRL                        },
  { VK_MENU      , KBDALT                         },
  { VK_SHIFT     , KBDSHIFT                       },
  { 0, 0 }
};

// aModification[] - map character modifier bits to modification number
static ALLOC_SECTION_LDATA MODIFIERS CharModifiers = {
  &aVkToBits[0],
  7,
  {
  // Level index -> Modifiers pressed       -> Level name
    0            /*                         -> Lower Alphanumeric       */,
    1            /* Shift                   -> Upper Alphanumeric       */,
    2            /*       Control           -> Control                  */,
    SHFT_INVALID /* Shift Control           -> (No valid level)         */,
    SHFT_INVALID /*               Alternate -> (No valid level)         */,
    SHFT_INVALID /* Shift         Alternate -> (No valid level)         */,
    3            /*       Control Alternate -> ISO Level 3              */
  }
};

/***************************************************************************\\
*
* aVkToWch<n>[]  - Virtual Key to WCHAR translation for <n> shift states
*
* Table attributes: Unordered Scan, null-terminated
*
* Search this table for an entry with a matching Virtual Key to find the
* corresponding unshifted and shifted WCHAR characters.
*
* Special values for VirtualKey (column 1)
*     0xff          - dead chars for the previous entry
*     0             - terminate the list
*
* Special values for Attributes (column 2)
*     CAPLOK bit    - CAPS-LOCK affect this key like SHIFT
*
* Special values for wch[*] (column 3 & 4)
*     WCH_NONE      - No character
*     WCH_DEAD      - Dead Key (diaresis) or invalid (US keyboard has none)
*     WCH_LGTR      - Ligature (generates multiple characters)
*
\\***************************************************************************/

// Errors encountered while creating aVkToWch tables:

static ALLOC_SECTION_LDATA VK_TO_WCHARS2 aVkToWchOther2[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 
  /*                                U+0030 0, U+003D = */
  {'0'          , 0               , L'0'    , L'='    },
  /*                                U+0037 7, U+002F / */
  {'7'          , 0               , L'7'    , L'/'    },
  /*                                U+0038 8, U+0028 ( */
  {'8'          , 0               , L'8'    , L'('    },
  /*                                U+0039 9, U+0029 ) */
  {'9'          , 0               , L'9'    , L')'    },
  /*                                U+0061 a, U+0041 A */
  {'A'          , CAPLOK          , L'a'    , L'A'    },
  /*                                U+0062 b, U+0042 B */
  {'B'          , CAPLOK          , L'b'    , L'B'    },
  /*                                U+0063 c, U+0043 C */
  {'C'          , CAPLOK          , L'c'    , L'C'    },
  /*                                U+0064 d, U+0044 D */
  {'D'          , CAPLOK          , L'd'    , L'D'    },
  /*                                U+0066 f, U+0046 F */
  {'F'          , CAPLOK          , L'f'    , L'F'    },
  /*                                U+0067 g, U+0047 G */
  {'G'          , CAPLOK          , L'g'    , L'G'    },
  /*                                U+0068 h, U+0048 H */
  {'H'          , CAPLOK          , L'h'    , L'H'    },
  /*                                U+0069 i, U+0049 I */
  {'I'          , CAPLOK          , L'i'    , L'I'    },
  /*                                U+006A j, U+004A J */
  {'J'          , CAPLOK          , L'j'    , L'J'    },
  /*                                U+006B k, U+004B K */
  {'K'          , CAPLOK          , L'k'    , L'K'    },
  /*                                U+006C l, U+004C L */
  {'L'          , CAPLOK          , L'l'    , L'L'    },
  /*                                U+006D m, U+004D M */
  {'M'          , CAPLOK          , L'm'    , L'M'    },
  /*                                U+006E n, U+004E N */
  {'N'          , CAPLOK          , L'n'    , L'N'    },
  /*                                U+006F o, U+004F O */
  {'O'          , CAPLOK          , L'o'    , L'O'    },
  /*                                U+0070 p, U+0050 P */
  {'P'          , CAPLOK          , L'p'    , L'P'    },
  /*                                U+0071 q, U+0051 Q */
  {'Q'          , CAPLOK          , L'q'    , L'Q'    },
  /*                                U+0072 r, U+0052 R */
  {'R'          , CAPLOK          , L'r'    , L'R'    },
  /*                                U+0073 s, U+0053 S */
  {'S'          , CAPLOK          , L's'    , L'S'    },
  /*                                U+0074 t, U+0054 T */
  {'T'          , CAPLOK          , L't'    , L'T'    },
  /*                                U+0075 u, U+0055 U */
  {'U'          , CAPLOK          , L'u'    , L'U'    },
  /*                                U+0076 v, U+0056 V */
  {'V'          , CAPLOK          , L'v'    , L'V'    },
  /*                                U+0077 w, U+0057 W */
  {'W'          , CAPLOK          , L'w'    , L'W'    },
  /*                                U+0078 x, U+0058 X */
  {'X'          , CAPLOK          , L'x'    , L'X'    },
  /*                                U+0079 y, U+0059 Y */
  {'Y'          , CAPLOK          , L'y'    , L'Y'    },
  /*                                U+007A z, U+005A Z */
  {'Z'          , CAPLOK          , L'z'    , L'Z'    },
  /*                                KP_Decimal, KP_Decimal */
  {VK_ABNT_C2   , 0               , L'.'    , L'.'    },
  /*                                KP_Add  , KP_Add   */
  {VK_ADD       , 0               , L'+'    , L'+'    },
  /*                                KP_Equal, KP_Equal */
  {VK_CLEAR     , 0               , L'='    , L'='    },
  /*                                KP_Separator, KP_Separator */
  {VK_DECIMAL   , 0               , L','    , L','    },
  /*                                KP_Divide, KP_Divide */
  {VK_DIVIDE    , 0               , L'/'    , L'/'    },
  /*                                KP_Multiply, KP_Multiply */
  {VK_MULTIPLY  , 0               , L'*'    , L'*'    },
  /*                                U+00F1 ñ, U+00D1 Ñ */
  {VK_OEM_3     , CAPLOK          , 0x00f1  , 0x00d1  },
  /*                                U+0027 ', U+003F ? */
  {VK_OEM_4     , 0               , L'\''   , L'?'    },
  /*                                U+00A1 ¡, U+00BF ¿ */
  {VK_OEM_6     , 0               , 0x00a1  , 0x00bf  },
  /*                                U+002C ,, U+003B ; */
  {VK_OEM_COMMA , 0               , L','    , L';'    },
  /*                                U+002D -, U+005F _ */
  {VK_OEM_MINUS , 0               , L'-'    , L'_'    },
  /*                                U+002E ., U+003A : */
  {VK_OEM_PERIOD, 0               , L'.'    , L':'    },
  /*                                KP_Subtract, KP_Subtract */
  {VK_SUBTRACT  , 0               , L'-'    , L'-'    },
  /*                                Tab     , Tab      */
  {VK_TAB       , 0               , L'\t'   , L'\t'   },
  {0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS3 aVkToWchOther3[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 
  /*                                BackSpace, BackSpace, U+007F ⌦ */
  {VK_BACK      , 0               , 0x0008  , 0x0008  , 0x007f  },
  /*                                U+0003 ␃, U+0003 ␃, U+0003 ␃ */
  {VK_CANCEL    , 0               , 0x0003  , 0x0003  , 0x0003  },
  /*                                Escape  , Escape  , Escape   */
  {VK_ESCAPE    , 0               , 0x001b  , 0x001b  , 0x001b  },
  /*                                U+003C <, U+003E >, U+001C ␜ */
  {VK_OEM_102   , 0               , L'<'    , L'>'    , 0x001c  },
  /*                                Return  , Return  , U+000A ␊ */
  {VK_RETURN    , 0               , L'\r'   , L'\r'   , L'\n'   },
  /*                                U+0020 ␣, U+0020 ␣, U+0020 ␣ */
  {VK_SPACE     , 0               , L' '    , L' '    , L' '    },
  {0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS4 aVkToWchOther4[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 , Level 3 
  /*                                U+0031 1, U+0021 !, �       , U+007C | */
  {'1'          , 0               , L'1'    , L'!'    , WCH_NONE, L'|'    },
  /*                                U+0032 2, U+0022 ", �       , U+0040 @ */
  {'2'          , 0               , L'2'    , L'"'    , WCH_NONE, L'@'    },
  /*                                U+0033 3, U+00B7 ·, �       , U+0023 # */
  {'3'          , 0               , L'3'    , 0x00b7  , WCH_NONE, L'#'    },
  /*                                U+0034 4, U+0024 $, �       , ‹Tilde›  */
  {'4'          , 0               , L'4'    , L'$'    , WCH_NONE, WCH_DEAD},
  {0xff         , 0               , WCH_NONE, WCH_NONE, WCH_NONE, L'~'    },
  /*                                U+0035 5, U+0025 %, �       , U+20AC € */
  {'5'          , 0               , L'5'    , L'%'    , WCH_NONE, 0x20ac  },
  /*                                U+0036 6, U+0026 &, �       , U+00AC ¬ */
  {'6'          , 0               , L'6'    , L'&'    , WCH_NONE, 0x00ac  },
  /*                                U+0065 e, U+0045 E, �       , U+20AC € */
  {'E'          , CAPLOK          , L'e'    , L'E'    , WCH_NONE, 0x20ac  },
  /*                                ‹Grave Accent›, ‹Circumflex Accent›, U+001B ␛, U+005B [ */
  {VK_OEM_1     , 0               , WCH_DEAD, WCH_DEAD, 0x001b  , L'['    },
  {0xff         , 0               , L'`'    , 0x02c6  , WCH_NONE, WCH_NONE},
  /*                                U+00E7 ç, U+00C7 Ç, U+001C ␜, U+007D } */
  {VK_OEM_2     , CAPLOK          , 0x00e7  , 0x00c7  , 0x001c  , L'}'    },
  /*                                U+00BA º, U+00AA ª, �       , U+005C \ */
  {VK_OEM_5     , 0               , 0x00ba  , 0x00aa  , WCH_NONE, L'\\'   },
  /*                                ‹Acute Accent›, ‹Diaeresis›, �       , U+007B { */
  {VK_OEM_7     , 0               , WCH_DEAD, WCH_DEAD, WCH_NONE, L'{'    },
  {0xff         , 0               , 0x00b4  , L':'    , WCH_NONE, WCH_NONE},
  /*                                U+002B +, U+002A *, U+001D ␝, U+005D ] */
  {VK_OEM_PLUS  , 0               , L'+'    , L'*'    , 0x001d  , L']'    },
  {0, 0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS1 aVkToWchNumpad1[] = {
  // Virtual key, Modifier options, Level 0 
  /*                                KP_0     */
  {VK_NUMPAD0   , 0               , L'0'    },
  /*                                KP_1     */
  {VK_NUMPAD1   , 0               , L'1'    },
  /*                                KP_2     */
  {VK_NUMPAD2   , 0               , L'2'    },
  /*                                KP_3     */
  {VK_NUMPAD3   , 0               , L'3'    },
  /*                                KP_4     */
  {VK_NUMPAD4   , 0               , L'4'    },
  /*                                KP_5     */
  {VK_NUMPAD5   , 0               , L'5'    },
  /*                                KP_6     */
  {VK_NUMPAD6   , 0               , L'6'    },
  /*                                KP_7     */
  {VK_NUMPAD7   , 0               , L'7'    },
  /*                                KP_8     */
  {VK_NUMPAD8   , 0               , L'8'    },
  /*                                KP_9     */
  {VK_NUMPAD9   , 0               , L'9'    },
  {0, 0, 0}
};



// aVkToWcharTable - List the various aVkToWchN tables in use
static ALLOC_SECTION_LDATA VK_TO_WCHAR_TABLE aVkToWcharTable[] = {
  { (PVK_TO_WCHARS1)aVkToWchOther2 , 2 , sizeof(aVkToWchOther2[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther3 , 3 , sizeof(aVkToWchOther3[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther4 , 4 , sizeof(aVkToWchOther4[0]) },
  // The numpad keys must come last so that VkKeyScan interprets number characters
  // as coming from the main section of the kbd before considering the numpad.
  { (PVK_TO_WCHARS1)aVkToWchNumpad1 , 1 , sizeof(aVkToWchNumpad1[0]) },
  {                      NULL , 0 , 0                     },
};

/***************************************************************************\\
* aKeyNames[], aKeyNamesExt[] - Virtual Scancode to Key Name tables
*
* Table attributes: Ordered Scan (by scancode), null-terminated
*
* Only the names of Extended, NumPad, Dead and Non-Printable keys are here.
* (Keys producing printable characters are named by that character)
\\***************************************************************************/
static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNames[] = {
  0x01, L"Esc",
  0x0e, L"Backspace",
  0x0f, L"Tab",
  0x1c, L"Enter",
  0x1d, L"Left Ctrl",
  0x2a, L"Left Shift",
  0x36, L"Right Shift",
  0x37, L"Num *",
  0x38, L"Left Alt",
  0x39, L"Space",
  0x3a, L"Caps Lock",
  0x3b, L"F1",
  0x3c, L"F2",
  0x3d, L"F3",
  0x3e, L"F4",
  0x3f, L"F5",
  0x40, L"F6",
  0x41, L"F7",
  0x42, L"F8",
  0x43, L"F9",
  0x44, L"F10",
  0x45, L"Pause",
  0x46, L"Scroll Lock",
  0x47, L"Num 7",
  0x48, L"Num 8",
  0x49, L"Num 9",
  0x4a, L"Num -",
  0x4b, L"Num 4",
  0x4c, L"Num 5",
  0x4d, L"Num 6",
  0x4e, L"Num +",
  0x4f, L"Num 1",
  0x50, L"Num 2",
  0x51, L"Num 3",
  0x52, L"Num 0",
  0x53, L"Num Del",
  0x54, L"Sys Req",
  0x57, L"F11",
  0x58, L"F12",
  0x64, L"F13",
  0x65, L"F14",
  0x66, L"F15",
  0x67, L"F16",
  0x68, L"F17",
  0x69, L"F18",
  0x6a, L"F19",
  0x6b, L"F20",
  0x6c, L"F21",
  0x6d, L"F22",
  0x6e, L"F23",
  0x76, L"F24",
  0, NULL
};

static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNamesExt[] = {
  0x1c, L"Num Enter",
  0x1d, L"Right Ctrl",
  0x35, L"Num /",
  0x37, L"Prnt Scrn",
  0x38, L"Right Alt",
  0x45, L"Num Lock",
  0x46, L"Break",
  0x47, L"Home",
  0x48, L"Up",
  0x49, L"Page Up",
  0x4b, L"Left",
  0x4d, L"Right",
  0x4f, L"End",
  0x50, L"Down",
  0x51, L"Page Down",
  0x52, L"Insert",
  0x53, L"Delete",
  0x54, L"<00>",
  0x56, L"Help",
  0x5b, L"Left Windows",
  0x5c, L"Right Windows",
  0x5d, L"Application",
  0, NULL
};

/***************************************************************************\\
 * Dead Keys
\\***************************************************************************/
static ALLOC_SECTION_LDATA DEADKEY_LPWSTR aKeyNamesDead[] = {
  L":"     L"Diaeresis",
  L"`"     L"Grave Accent",
  L"~"     L"Tilde",
  L"\u00b4"   L"Acute Accent",
  L"\u02c6"   L"Circumflex Accent",
  L"\u02dd"   L"Double Acute Accent",
  L"\u02f7"   L"Tilde Below",
  L"\u030f"   L"Double Grave Accent",
  L"\u0378"   L"Diaeresis + Grave Accent",
  L"\u0379"   L"Diaeresis + Tilde",
  L"\u0380"   L"Diaeresis + Acute Accent",
  L"\u0381"   L"Grave Accent + Diaeresis",
  L"\u0382"   L"Grave Accent + Circumflex Accent",
  L"\u0383"   L"Tilde + Diaeresis",
  L"\u038b"   L"Tilde + Acute Accent",
  L"\u038d"   L"Tilde + Circumflex Accent",
  L"\u03a2"   L"Acute Accent + Diaeresis",
  L"\u0530"   L"Acute Accent + Tilde",
  L"\u0557"   L"Acute Accent + Circumflex Accent",
  L"\u0558"   L"Circumflex Accent + Grave Accent",
  L"\u058b"   L"Circumflex Accent + Tilde",
  L"\u058c"   L"Circumflex Accent + Acute Accent",
  L"\u2025"   L"Diaeresis Below",
  L"\u2038"   L"Circumflex Accent Below",

  NULL
};

static ALLOC_SECTION_LDATA DEADKEY aDeadKey[] = {

// Dead key: Diaeresis
  DEADTRANS(L':'  , L'.'  , 0x0308, NORMAL_CHARACTER),	// U+002E -> U+0308
  DEADTRANS(L':'  , L'A'  , 0x00c4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ä’
  DEADTRANS(L':'  , L'E'  , 0x00cb, NORMAL_CHARACTER),	// ‘E’ -> ‘Ë’
  DEADTRANS(L':'  , L'H'  , 0x1e26, NORMAL_CHARACTER),	// ‘H’ -> ‘Ḧ’
  DEADTRANS(L':'  , L'I'  , 0x00cf, NORMAL_CHARACTER),	// ‘I’ -> ‘Ï’
  DEADTRANS(L':'  , L'O'  , 0x00d6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ö’
  DEADTRANS(L':'  , L'U'  , 0x00dc, NORMAL_CHARACTER),	// ‘U’ -> ‘Ü’
  DEADTRANS(L':'  , L'W'  , 0x1e84, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẅ’
  DEADTRANS(L':'  , L'X'  , 0x1e8c, NORMAL_CHARACTER),	// ‘X’ -> ‘Ẍ’
  DEADTRANS(L':'  , L'Y'  , 0x0178, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ÿ’
  DEADTRANS(L':'  , L'a'  , 0x00e4, NORMAL_CHARACTER),	// ‘a’ -> ‘ä’
  DEADTRANS(L':'  , L'e'  , 0x00eb, NORMAL_CHARACTER),	// ‘e’ -> ‘ë’
  DEADTRANS(L':'  , L'h'  , 0x1e27, NORMAL_CHARACTER),	// ‘h’ -> ‘ḧ’
  DEADTRANS(L':'  , L'i'  , 0x00ef, NORMAL_CHARACTER),	// ‘i’ -> ‘ï’
  DEADTRANS(L':'  , L'o'  , 0x00f6, NORMAL_CHARACTER),	// ‘o’ -> ‘ö’
  DEADTRANS(L':'  , L't'  , 0x1e97, NORMAL_CHARACTER),	// ‘t’ -> ‘ẗ’
  DEADTRANS(L':'  , L'u'  , 0x00fc, NORMAL_CHARACTER),	// ‘u’ -> ‘ü’
  DEADTRANS(L':'  , L'w'  , 0x1e85, NORMAL_CHARACTER),	// ‘w’ -> ‘ẅ’
  DEADTRANS(L':'  , L'x'  , 0x1e8d, NORMAL_CHARACTER),	// ‘x’ -> ‘ẍ’
  DEADTRANS(L':'  , L'y'  , 0x00ff, NORMAL_CHARACTER),	// ‘y’ -> ‘ÿ’
  DEADTRANS(L':'  , L':'  , 0x2025, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Diaeresis Below›
  DEADTRANS(L':'  , L'`'  , 0x0378, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Diaeresis + Grave Accent›
  DEADTRANS(L':'  , L'~'  , 0x0379, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Diaeresis + Tilde›
  DEADTRANS(L':'  , 0x00b4, 0x0380, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Diaeresis + Acute Accent›
  DEADTRANS(L':'  , L' '  , L':'  , NORMAL_CHARACTER),	// U+0020 -> U+003A

// Dead key: Grave Accent
  DEADTRANS(L'`'  , L'.'  , 0x0300, NORMAL_CHARACTER),	// U+002E -> U+0300
  DEADTRANS(L'`'  , L'A'  , 0x00c0, NORMAL_CHARACTER),	// ‘A’ -> ‘À’
  DEADTRANS(L'`'  , L'E'  , 0x00c8, NORMAL_CHARACTER),	// ‘E’ -> ‘È’
  DEADTRANS(L'`'  , L'I'  , 0x00cc, NORMAL_CHARACTER),	// ‘I’ -> ‘Ì’
  DEADTRANS(L'`'  , L'N'  , 0x01f8, NORMAL_CHARACTER),	// ‘N’ -> ‘Ǹ’
  DEADTRANS(L'`'  , L'O'  , 0x00d2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ò’
  DEADTRANS(L'`'  , L'U'  , 0x00d9, NORMAL_CHARACTER),	// ‘U’ -> ‘Ù’
  DEADTRANS(L'`'  , L'V'  , 0x01db, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǜ’
  DEADTRANS(L'`'  , L'W'  , 0x1e80, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẁ’
  DEADTRANS(L'`'  , L'Y'  , 0x1ef2, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỳ’
  DEADTRANS(L'`'  , L'a'  , 0x00e0, NORMAL_CHARACTER),	// ‘a’ -> ‘à’
  DEADTRANS(L'`'  , L'e'  , 0x00e8, NORMAL_CHARACTER),	// ‘e’ -> ‘è’
  DEADTRANS(L'`'  , L'i'  , 0x00ec, NORMAL_CHARACTER),	// ‘i’ -> ‘ì’
  DEADTRANS(L'`'  , L'n'  , 0x01f9, NORMAL_CHARACTER),	// ‘n’ -> ‘ǹ’
  DEADTRANS(L'`'  , L'o'  , 0x00f2, NORMAL_CHARACTER),	// ‘o’ -> ‘ò’
  DEADTRANS(L'`'  , L'u'  , 0x00f9, NORMAL_CHARACTER),	// ‘u’ -> ‘ù’
  DEADTRANS(L'`'  , L'v'  , 0x01dc, NORMAL_CHARACTER),	// ‘v’ -> ‘ǜ’
  DEADTRANS(L'`'  , L'w'  , 0x1e81, NORMAL_CHARACTER),	// ‘w’ -> ‘ẁ’
  DEADTRANS(L'`'  , L'y'  , 0x1ef3, NORMAL_CHARACTER),	// ‘y’ -> ‘ỳ’
  DEADTRANS(L'`'  , L':'  , 0x0381, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Grave Accent + Diaeresis›
  DEADTRANS(L'`'  , L'`'  , 0x030f, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Double Grave Accent›
  DEADTRANS(L'`'  , 0x02c6, 0x0382, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Grave Accent + Circumflex Accent›
  DEADTRANS(L'`'  , L' '  , 0x02cb, NORMAL_CHARACTER),	// U+0020 -> ‘ˋ’

// Dead key: Tilde
  DEADTRANS(L'~'  , L'-'  , 0x2243, NORMAL_CHARACTER),	// U+002D -> U+2243
  DEADTRANS(L'~'  , L'.'  , 0x0303, NORMAL_CHARACTER),	// U+002E -> U+0303
  DEADTRANS(L'~'  , L'A'  , 0x00c3, NORMAL_CHARACTER),	// ‘A’ -> ‘Ã’
  DEADTRANS(L'~'  , L'E'  , 0x1ebc, NORMAL_CHARACTER),	// ‘E’ -> ‘Ẽ’
  DEADTRANS(L'~'  , L'I'  , 0x0128, NORMAL_CHARACTER),	// ‘I’ -> ‘Ĩ’
  DEADTRANS(L'~'  , L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(L'~'  , L'O'  , 0x00d5, NORMAL_CHARACTER),	// ‘O’ -> ‘Õ’
  DEADTRANS(L'~'  , L'U'  , 0x0168, NORMAL_CHARACTER),	// ‘U’ -> ‘Ũ’
  DEADTRANS(L'~'  , L'V'  , 0x1e7c, NORMAL_CHARACTER),	// ‘V’ -> ‘Ṽ’
  DEADTRANS(L'~'  , L'Y'  , 0x1ef8, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ỹ’
  DEADTRANS(L'~'  , L'a'  , 0x00e3, NORMAL_CHARACTER),	// ‘a’ -> ‘ã’
  DEADTRANS(L'~'  , L'e'  , 0x1ebd, NORMAL_CHARACTER),	// ‘e’ -> ‘ẽ’
  DEADTRANS(L'~'  , L'i'  , 0x0129, NORMAL_CHARACTER),	// ‘i’ -> ‘ĩ’
  DEADTRANS(L'~'  , L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(L'~'  , L'o'  , 0x00f5, NORMAL_CHARACTER),	// ‘o’ -> ‘õ’
  DEADTRANS(L'~'  , L'u'  , 0x0169, NORMAL_CHARACTER),	// ‘u’ -> ‘ũ’
  DEADTRANS(L'~'  , L'v'  , 0x1e7d, NORMAL_CHARACTER),	// ‘v’ -> ‘ṽ’
  DEADTRANS(L'~'  , L'y'  , 0x1ef9, NORMAL_CHARACTER),	// ‘y’ -> ‘ỹ’
  DEADTRANS(L'~'  , L':'  , 0x0383, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Tilde + Diaeresis›
  DEADTRANS(L'~'  , L'~'  , 0x02f7, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Tilde Below›
  DEADTRANS(L'~'  , 0x00b4, 0x038b, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Tilde + Acute Accent›
  DEADTRANS(L'~'  , 0x02c6, 0x038d, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Tilde + Circumflex Accent›
  DEADTRANS(L'~'  , L' '  , 0x02dc, NORMAL_CHARACTER),	// U+0020 -> U+02DC

// Dead key: Acute Accent
  DEADTRANS(0x00b4, L'.'  , 0x0301, NORMAL_CHARACTER),	// U+002E -> U+0301
  DEADTRANS(0x00b4, L'A'  , 0x00c1, NORMAL_CHARACTER),	// ‘A’ -> ‘Á’
  DEADTRANS(0x00b4, L'C'  , 0x0106, NORMAL_CHARACTER),	// ‘C’ -> ‘Ć’
  DEADTRANS(0x00b4, L'E'  , 0x00c9, NORMAL_CHARACTER),	// ‘E’ -> ‘É’
  DEADTRANS(0x00b4, L'G'  , 0x01f4, NORMAL_CHARACTER),	// ‘G’ -> ‘Ǵ’
  DEADTRANS(0x00b4, L'I'  , 0x00cd, NORMAL_CHARACTER),	// ‘I’ -> ‘Í’
  DEADTRANS(0x00b4, L'K'  , 0x1e30, NORMAL_CHARACTER),	// ‘K’ -> ‘Ḱ’
  DEADTRANS(0x00b4, L'L'  , 0x0139, NORMAL_CHARACTER),	// ‘L’ -> ‘Ĺ’
  DEADTRANS(0x00b4, L'M'  , 0x1e3e, NORMAL_CHARACTER),	// ‘M’ -> ‘Ḿ’
  DEADTRANS(0x00b4, L'N'  , 0x0143, NORMAL_CHARACTER),	// ‘N’ -> ‘Ń’
  DEADTRANS(0x00b4, L'O'  , 0x00d3, NORMAL_CHARACTER),	// ‘O’ -> ‘Ó’
  DEADTRANS(0x00b4, L'P'  , 0x1e54, NORMAL_CHARACTER),	// ‘P’ -> ‘Ṕ’
  DEADTRANS(0x00b4, L'R'  , 0x0154, NORMAL_CHARACTER),	// ‘R’ -> ‘Ŕ’
  DEADTRANS(0x00b4, L'S'  , 0x015a, NORMAL_CHARACTER),	// ‘S’ -> ‘Ś’
  DEADTRANS(0x00b4, L'U'  , 0x00da, NORMAL_CHARACTER),	// ‘U’ -> ‘Ú’
  DEADTRANS(0x00b4, L'V'  , 0x01d7, NORMAL_CHARACTER),	// ‘V’ -> ‘Ǘ’
  DEADTRANS(0x00b4, L'W'  , 0x1e82, NORMAL_CHARACTER),	// ‘W’ -> ‘Ẃ’
  DEADTRANS(0x00b4, L'Y'  , 0x00dd, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ý’
  DEADTRANS(0x00b4, L'Z'  , 0x0179, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ź’
  DEADTRANS(0x00b4, L'a'  , 0x00e1, NORMAL_CHARACTER),	// ‘a’ -> ‘á’
  DEADTRANS(0x00b4, L'c'  , 0x0107, NORMAL_CHARACTER),	// ‘c’ -> ‘ć’
  DEADTRANS(0x00b4, L'e'  , 0x00e9, NORMAL_CHARACTER),	// ‘e’ -> ‘é’
  DEADTRANS(0x00b4, L'g'  , 0x01f5, NORMAL_CHARACTER),	// ‘g’ -> ‘ǵ’
  DEADTRANS(0x00b4, L'i'  , 0x00ed, NORMAL_CHARACTER),	// ‘i’ -> ‘í’
  DEADTRANS(0x00b4, L'k'  , 0x1e31, NORMAL_CHARACTER),	// ‘k’ -> ‘ḱ’
  DEADTRANS(0x00b4, L'l'  , 0x013a, NORMAL_CHARACTER),	// ‘l’ -> ‘ĺ’
  DEADTRANS(0x00b4, L'm'  , 0x1e3f, NORMAL_CHARACTER),	// ‘m’ -> ‘ḿ’
  DEADTRANS(0x00b4, L'n'  , 0x0144, NORMAL_CHARACTER),	// ‘n’ -> ‘ń’
  DEADTRANS(0x00b4, L'o'  , 0x00f3, NORMAL_CHARACTER),	// ‘o’ -> ‘ó’
  DEADTRANS(0x00b4, L'p'  , 0x1e55, NORMAL_CHARACTER),	// ‘p’ -> ‘ṕ’
  DEADTRANS(0x00b4, L'r'  , 0x0155, NORMAL_CHARACTER),	// ‘r’ -> ‘ŕ’
  DEADTRANS(0x00b4, L's'  , 0x015b, NORMAL_CHARACTER),	// ‘s’ -> ‘ś’
  DEADTRANS(0x00b4, L'u'  , 0x00fa, NORMAL_CHARACTER),	// ‘u’ -> ‘ú’
  DEADTRANS(0x00b4, L'v'  , 0x01d8, NORMAL_CHARACTER),	// ‘v’ -> ‘ǘ’
  DEADTRANS(0x00b4, L'w'  , 0x1e83, NORMAL_CHARACTER),	// ‘w’ -> ‘ẃ’
  DEADTRANS(0x00b4, L'y'  , 0x00fd, NORMAL_CHARACTER),	// ‘y’ -> ‘ý’
  DEADTRANS(0x00b4, L'z'  , 0x017a, NORMAL_CHARACTER),	// ‘z’ -> ‘ź’
  DEADTRANS(0x00b4, 0x00c7, 0x1e08, NORMAL_CHARACTER),	// ‘Ç’ -> ‘Ḉ’
  DEADTRANS(0x00b4, 0x00e7, 0x1e09, NORMAL_CHARACTER),	// ‘ç’ -> ‘ḉ’
  DEADTRANS(0x00b4, L':'  , 0x03a2, CHAINED_DEAD_KEY),	// ‹Diaeresis› -> ‹Acute Accent + Diaeresis›
  DEADTRANS(0x00b4, L'~'  , 0x0530, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Acute Accent + Tilde›
  DEADTRANS(0x00b4, 0x00b4, 0x02dd, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Double Acute Accent›
  DEADTRANS(0x00b4, 0x02c6, 0x0557, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Acute Accent + Circumflex Accent›
  DEADTRANS(0x00b4, L' '  , 0x02ca, NORMAL_CHARACTER),	// U+0020 -> ‘ˊ’

// Dead key: Circumflex Accent
  DEADTRANS(0x02c6, L'('  , 0x207d, NORMAL_CHARACTER),	// U+0028 -> U+207D
  DEADTRANS(0x02c6, L')'  , 0x207e, NORMAL_CHARACTER),	// U+0029 -> U+207E
  DEADTRANS(0x02c6, L'+'  , 0x207a, NORMAL_CHARACTER),	// U+002B -> U+207A
  DEADTRANS(0x02c6, L'.'  , 0x0302, NORMAL_CHARACTER),	// U+002E -> U+0302
  DEADTRANS(0x02c6, L'0'  , 0x2070, NORMAL_CHARACTER),	// ‘0’ -> ‘⁰’
  DEADTRANS(0x02c6, L'1'  , 0x00b9, NORMAL_CHARACTER),	// ‘1’ -> ‘¹’
  DEADTRANS(0x02c6, L'2'  , 0x00b2, NORMAL_CHARACTER),	// ‘2’ -> ‘²’
  DEADTRANS(0x02c6, L'3'  , 0x00b3, NORMAL_CHARACTER),	// ‘3’ -> ‘³’
  DEADTRANS(0x02c6, L'4'  , 0x2074, NORMAL_CHARACTER),	// ‘4’ -> ‘⁴’
  DEADTRANS(0x02c6, L'5'  , 0x2075, NORMAL_CHARACTER),	// ‘5’ -> ‘⁵’
  DEADTRANS(0x02c6, L'6'  , 0x2076, NORMAL_CHARACTER),	// ‘6’ -> ‘⁶’
  DEADTRANS(0x02c6, L'7'  , 0x2077, NORMAL_CHARACTER),	// ‘7’ -> ‘⁷’
  DEADTRANS(0x02c6, L'8'  , 0x2078, NORMAL_CHARACTER),	// ‘8’ -> ‘⁸’
  DEADTRANS(0x02c6, L'9'  , 0x2079, NORMAL_CHARACTER),	// ‘9’ -> ‘⁹’
  DEADTRANS(0x02c6, L'A'  , 0x00c2, NORMAL_CHARACTER),	// ‘A’ -> ‘Â’
  DEADTRANS(0x02c6, L'C'  , 0x0108, NORMAL_CHARACTER),	// ‘C’ -> ‘Ĉ’
  DEADTRANS(0x02c6, L'E'  , 0x00ca, NORMAL_CHARACTER),	// ‘E’ -> ‘Ê’
  DEADTRANS(0x02c6, L'G'  , 0x011c, NORMAL_CHARACTER),	// ‘G’ -> ‘Ĝ’
  DEADTRANS(0x02c6, L'H'  , 0x0124, NORMAL_CHARACTER),	// ‘H’ -> ‘Ĥ’
  DEADTRANS(0x02c6, L'I'  , 0x00ce, NORMAL_CHARACTER),	// ‘I’ -> ‘Î’
  DEADTRANS(0x02c6, L'J'  , 0x0134, NORMAL_CHARACTER),	// ‘J’ -> ‘Ĵ’
  DEADTRANS(0x02c6, L'N'  , 0x00d1, NORMAL_CHARACTER),	// ‘N’ -> ‘Ñ’
  DEADTRANS(0x02c6, L'O'  , 0x00d4, NORMAL_CHARACTER),	// ‘O’ -> ‘Ô’
  DEADTRANS(0x02c6, L'S'  , 0x015c, NORMAL_CHARACTER),	// ‘S’ -> ‘Ŝ’
  DEADTRANS(0x02c6, L'U'  , 0x00db, NORMAL_CHARACTER),	// ‘U’ -> ‘Û’
  DEADTRANS(0x02c6, L'W'  , 0x0174, NORMAL_CHARACTER),	// ‘W’ -> ‘Ŵ’
  DEADTRANS(0x02c6, L'Y'  , 0x0176, NORMAL_CHARACTER),	// ‘Y’ -> ‘Ŷ’
  DEADTRANS(0x02c6, L'Z'  , 0x1e90, NORMAL_CHARACTER),	// ‘Z’ -> ‘Ẑ’
  DEADTRANS(0x02c6, L'a'  , 0x00e2, NORMAL_CHARACTER),	// ‘a’ -> ‘â’
  DEADTRANS(0x02c6, L'c'  , 0x0109, NORMAL_CHARACTER),	// ‘c’ -> ‘ĉ’
  DEADTRANS(0x02c6, L'e'  , 0x00ea, NORMAL_CHARACTER),	// ‘e’ -> ‘ê’
  DEADTRANS(0x02c6, L'g'  , 0x011d, NORMAL_CHARACTER),	// ‘g’ -> ‘ĝ’
  DEADTRANS(0x02c6, L'h'  , 0x0125, NORMAL_CHARACTER),	// ‘h’ -> ‘ĥ’
  DEADTRANS(0x02c6, L'i'  , 0x00ee, NORMAL_CHARACTER),	// ‘i’ -> ‘î’
  DEADTRANS(0x02c6, L'j'  , 0x0135, NORMAL_CHARACTER),	// ‘j’ -> ‘ĵ’
  DEADTRANS(0x02c6, L'n'  , 0x00f1, NORMAL_CHARACTER),	// ‘n’ -> ‘ñ’
  DEADTRANS(0x02c6, L'o'  , 0x00f4, NORMAL_CHARACTER),	// ‘o’ -> ‘ô’
  DEADTRANS(0x02c6, L's'  , 0x015d, NORMAL_CHARACTER),	// ‘s’ -> ‘ŝ’
  DEADTRANS(0x02c6, L'u'  , 0x00fb, NORMAL_CHARACTER),	// ‘u’ -> ‘û’
  DEADTRANS(0x02c6, L'w'  , 0x0175, NORMAL_CHARACTER),	// ‘w’ -> ‘ŵ’
  DEADTRANS(0x02c6, L'y'  , 0x0177, NORMAL_CHARACTER),	// ‘y’ -> ‘ŷ’
  DEADTRANS(0x02c6, L'z'  , 0x1e91, NORMAL_CHARACTER),	// ‘z’ -> ‘ẑ’
  DEADTRANS(0x02c6, L'`'  , 0x0558, CHAINED_DEAD_KEY),	// ‹Grave Accent› -> ‹Circumflex Accent + Grave Accent›
  DEADTRANS(0x02c6, L'~'  , 0x058b, CHAINED_DEAD_KEY),	// ‹Tilde› -> ‹Circumflex Accent + Tilde›
  DEADTRANS(0x02c6, 0x00b4, 0x058c, CHAINED_DEAD_KEY),	// ‹Acute Accent› -> ‹Circumflex Accent + Acute Accent›
  DEADTRANS(0x02c6, 0x02c6, 0x2038, CHAINED_DEAD_KEY),	// ‹Circumflex Accent› -> ‹Circumflex Accent Below›
  DEADTRANS(0x02c6, L' '  , 0x02c6, NORMAL_CHARACTER),	// U+0020 -> ‘ˆ’

// Dead key: Double Acute Accent
  DEADTRANS(0x02dd, L'.'  , 0x030b, NORMAL_CHARACTER),	// U+002E -> U+030B
  DEADTRANS(0x02dd, L'O'  , 0x0150, NORMAL_CHARACTER),	// ‘O’ -> ‘Ő’
  DEADTRANS(0x02dd, L'U'  , 0x0170, NORMAL_CHARACTER),	// ‘U’ -> ‘Ű’
  DEADTRANS(0x02dd, L'o'  , 0x0151, NORMAL_CHARACTER),	// ‘o’ -> ‘ő’
  DEADTRANS(0x02dd, L'u'  , 0x0171, NORMAL_CHARACTER),	// ‘u’ -> ‘ű’
  DEADTRANS(0x02dd, L' '  , 0x02dd, NORMAL_CHARACTER),	// U+0020 -> U+02DD

// Dead key: Tilde Below
  DEADTRANS(0x02f7, L'.'  , 0x0330, NORMAL_CHARACTER),	// U+002E -> U+0330
  DEADTRANS(0x02f7, L'E'  , 0x1e1a, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḛ’
  DEADTRANS(0x02f7, L'I'  , 0x1e2c, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḭ’
  DEADTRANS(0x02f7, L'U'  , 0x1e74, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṵ’
  DEADTRANS(0x02f7, L'e'  , 0x1e1b, NORMAL_CHARACTER),	// ‘e’ -> ‘ḛ’
  DEADTRANS(0x02f7, L'i'  , 0x1e2d, NORMAL_CHARACTER),	// ‘i’ -> ‘ḭ’
  DEADTRANS(0x02f7, L'u'  , 0x1e75, NORMAL_CHARACTER),	// ‘u’ -> ‘ṵ’
  DEADTRANS(0x02f7, 0x02f7, L'~'  , CHAINED_DEAD_KEY),	// ‹Tilde Below› -> ‹Tilde›
  DEADTRANS(0x02f7, L' '  , 0x02f7, NORMAL_CHARACTER),	// U+0020 -> U+02F7

// Dead key: Double Grave Accent
  DEADTRANS(0x030f, L'.'  , 0x030f, NORMAL_CHARACTER),	// U+002E -> U+030F
  DEADTRANS(0x030f, L'A'  , 0x0200, NORMAL_CHARACTER),	// ‘A’ -> ‘Ȁ’
  DEADTRANS(0x030f, L'E'  , 0x0204, NORMAL_CHARACTER),	// ‘E’ -> ‘Ȅ’
  DEADTRANS(0x030f, L'I'  , 0x0208, NORMAL_CHARACTER),	// ‘I’ -> ‘Ȉ’
  DEADTRANS(0x030f, L'O'  , 0x020c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ȍ’
  DEADTRANS(0x030f, L'R'  , 0x0210, NORMAL_CHARACTER),	// ‘R’ -> ‘Ȑ’
  DEADTRANS(0x030f, L'U'  , 0x0214, NORMAL_CHARACTER),	// ‘U’ -> ‘Ȕ’
  DEADTRANS(0x030f, L'a'  , 0x0201, NORMAL_CHARACTER),	// ‘a’ -> ‘ȁ’
  DEADTRANS(0x030f, L'e'  , 0x0205, NORMAL_CHARACTER),	// ‘e’ -> ‘ȅ’
  DEADTRANS(0x030f, L'i'  , 0x0209, NORMAL_CHARACTER),	// ‘i’ -> ‘ȉ’
  DEADTRANS(0x030f, L'o'  , 0x020d, NORMAL_CHARACTER),	// ‘o’ -> ‘ȍ’
  DEADTRANS(0x030f, L'r'  , 0x0211, NORMAL_CHARACTER),	// ‘r’ -> ‘ȑ’
  DEADTRANS(0x030f, L'u'  , 0x0215, NORMAL_CHARACTER),	// ‘u’ -> ‘ȕ’
  DEADTRANS(0x030f, L' '  , 0x030f, NORMAL_CHARACTER),	// U+0020 -> U+030F

// Dead key: Diaeresis + Grave Accent
  DEADTRANS(0x0378, L'U'  , 0x01db, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǜ’
  DEADTRANS(0x0378, L'u'  , 0x01dc, NORMAL_CHARACTER),	// ‘u’ -> ‘ǜ’
  DEADTRANS(0x0378, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Tilde
  DEADTRANS(0x0379, L'O'  , 0x1e4e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṏ’
  DEADTRANS(0x0379, L'o'  , 0x1e4f, NORMAL_CHARACTER),	// ‘o’ -> ‘ṏ’
  DEADTRANS(0x0379, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis + Acute Accent
  DEADTRANS(0x0380, L'I'  , 0x1e2e, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḯ’
  DEADTRANS(0x0380, L'U'  , 0x01d7, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǘ’
  DEADTRANS(0x0380, L'i'  , 0x1e2f, NORMAL_CHARACTER),	// ‘i’ -> ‘ḯ’
  DEADTRANS(0x0380, L'u'  , 0x01d8, NORMAL_CHARACTER),	// ‘u’ -> ‘ǘ’
  DEADTRANS(0x0380, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Diaeresis
  DEADTRANS(0x0381, L'U'  , 0x01db, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǜ’
  DEADTRANS(0x0381, L'u'  , 0x01dc, NORMAL_CHARACTER),	// ‘u’ -> ‘ǜ’
  DEADTRANS(0x0381, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Grave Accent + Circumflex Accent
  DEADTRANS(0x0382, L'A'  , 0x1ea6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ầ’
  DEADTRANS(0x0382, L'E'  , 0x1ec0, NORMAL_CHARACTER),	// ‘E’ -> ‘Ề’
  DEADTRANS(0x0382, L'O'  , 0x1ed2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ồ’
  DEADTRANS(0x0382, L'a'  , 0x1ea7, NORMAL_CHARACTER),	// ‘a’ -> ‘ầ’
  DEADTRANS(0x0382, L'e'  , 0x1ec1, NORMAL_CHARACTER),	// ‘e’ -> ‘ề’
  DEADTRANS(0x0382, L'o'  , 0x1ed3, NORMAL_CHARACTER),	// ‘o’ -> ‘ồ’
  DEADTRANS(0x0382, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Diaeresis
  DEADTRANS(0x0383, L'O'  , 0x1e4e, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṏ’
  DEADTRANS(0x0383, L'o'  , 0x1e4f, NORMAL_CHARACTER),	// ‘o’ -> ‘ṏ’
  DEADTRANS(0x0383, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Acute Accent
  DEADTRANS(0x038b, L'O'  , 0x1e4c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṍ’
  DEADTRANS(0x038b, L'U'  , 0x1e78, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṹ’
  DEADTRANS(0x038b, L'o'  , 0x1e4d, NORMAL_CHARACTER),	// ‘o’ -> ‘ṍ’
  DEADTRANS(0x038b, L'u'  , 0x1e79, NORMAL_CHARACTER),	// ‘u’ -> ‘ṹ’
  DEADTRANS(0x038b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Tilde + Circumflex Accent
  DEADTRANS(0x038d, L'A'  , 0x1eaa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẫ’
  DEADTRANS(0x038d, L'E'  , 0x1ec4, NORMAL_CHARACTER),	// ‘E’ -> ‘Ễ’
  DEADTRANS(0x038d, L'O'  , 0x1ed6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỗ’
  DEADTRANS(0x038d, L'a'  , 0x1eab, NORMAL_CHARACTER),	// ‘a’ -> ‘ẫ’
  DEADTRANS(0x038d, L'e'  , 0x1ec5, NORMAL_CHARACTER),	// ‘e’ -> ‘ễ’
  DEADTRANS(0x038d, L'o'  , 0x1ed7, NORMAL_CHARACTER),	// ‘o’ -> ‘ỗ’
  DEADTRANS(0x038d, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Diaeresis
  DEADTRANS(0x03a2, L'I'  , 0x1e2e, NORMAL_CHARACTER),	// ‘I’ -> ‘Ḯ’
  DEADTRANS(0x03a2, L'U'  , 0x01d7, NORMAL_CHARACTER),	// ‘U’ -> ‘Ǘ’
  DEADTRANS(0x03a2, L'i'  , 0x1e2f, NORMAL_CHARACTER),	// ‘i’ -> ‘ḯ’
  DEADTRANS(0x03a2, L'u'  , 0x01d8, NORMAL_CHARACTER),	// ‘u’ -> ‘ǘ’
  DEADTRANS(0x03a2, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Tilde
  DEADTRANS(0x0530, L'O'  , 0x1e4c, NORMAL_CHARACTER),	// ‘O’ -> ‘Ṍ’
  DEADTRANS(0x0530, L'U'  , 0x1e78, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṹ’
  DEADTRANS(0x0530, L'o'  , 0x1e4d, NORMAL_CHARACTER),	// ‘o’ -> ‘ṍ’
  DEADTRANS(0x0530, L'u'  , 0x1e79, NORMAL_CHARACTER),	// ‘u’ -> ‘ṹ’
  DEADTRANS(0x0530, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Acute Accent + Circumflex Accent
  DEADTRANS(0x0557, L'A'  , 0x1ea4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ấ’
  DEADTRANS(0x0557, L'E'  , 0x1ebe, NORMAL_CHARACTER),	// ‘E’ -> ‘Ế’
  DEADTRANS(0x0557, L'O'  , 0x1ed0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ố’
  DEADTRANS(0x0557, L'a'  , 0x1ea5, NORMAL_CHARACTER),	// ‘a’ -> ‘ấ’
  DEADTRANS(0x0557, L'e'  , 0x1ebf, NORMAL_CHARACTER),	// ‘e’ -> ‘ế’
  DEADTRANS(0x0557, L'o'  , 0x1ed1, NORMAL_CHARACTER),	// ‘o’ -> ‘ố’
  DEADTRANS(0x0557, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Grave Accent
  DEADTRANS(0x0558, L'A'  , 0x1ea6, NORMAL_CHARACTER),	// ‘A’ -> ‘Ầ’
  DEADTRANS(0x0558, L'E'  , 0x1ec0, NORMAL_CHARACTER),	// ‘E’ -> ‘Ề’
  DEADTRANS(0x0558, L'O'  , 0x1ed2, NORMAL_CHARACTER),	// ‘O’ -> ‘Ồ’
  DEADTRANS(0x0558, L'a'  , 0x1ea7, NORMAL_CHARACTER),	// ‘a’ -> ‘ầ’
  DEADTRANS(0x0558, L'e'  , 0x1ec1, NORMAL_CHARACTER),	// ‘e’ -> ‘ề’
  DEADTRANS(0x0558, L'o'  , 0x1ed3, NORMAL_CHARACTER),	// ‘o’ -> ‘ồ’
  DEADTRANS(0x0558, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Tilde
  DEADTRANS(0x058b, L'A'  , 0x1eaa, NORMAL_CHARACTER),	// ‘A’ -> ‘Ẫ’
  DEADTRANS(0x058b, L'E'  , 0x1ec4, NORMAL_CHARACTER),	// ‘E’ -> ‘Ễ’
  DEADTRANS(0x058b, L'O'  , 0x1ed6, NORMAL_CHARACTER),	// ‘O’ -> ‘Ỗ’
  DEADTRANS(0x058b, L'a'  , 0x1eab, NORMAL_CHARACTER),	// ‘a’ -> ‘ẫ’
  DEADTRANS(0x058b, L'e'  , 0x1ec5, NORMAL_CHARACTER),	// ‘e’ -> ‘ễ’
  DEADTRANS(0x058b, L'o'  , 0x1ed7, NORMAL_CHARACTER),	// ‘o’ -> ‘ỗ’
  DEADTRANS(0x058b, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Circumflex Accent + Acute Accent
  DEADTRANS(0x058c, L'A'  , 0x1ea4, NORMAL_CHARACTER),	// ‘A’ -> ‘Ấ’
  DEADTRANS(0x058c, L'E'  , 0x1ebe, NORMAL_CHARACTER),	// ‘E’ -> ‘Ế’
  DEADTRANS(0x058c, L'O'  , 0x1ed0, NORMAL_CHARACTER),	// ‘O’ -> ‘Ố’
  DEADTRANS(0x058c, L'a'  , 0x1ea5, NORMAL_CHARACTER),	// ‘a’ -> ‘ấ’
  DEADTRANS(0x058c, L'e'  , 0x1ebf, NORMAL_CHARACTER),	// ‘e’ -> ‘ế’
  DEADTRANS(0x058c, L'o'  , 0x1ed1, NORMAL_CHARACTER),	// ‘o’ -> ‘ố’
  DEADTRANS(0x058c, L' '  , L' '  , NORMAL_CHARACTER),	// U+0020 -> U+0020

// Dead key: Diaeresis Below
  DEADTRANS(0x2025, L'.'  , 0x0324, NORMAL_CHARACTER),	// U+002E -> U+0324
  DEADTRANS(0x2025, L'U'  , 0x1e72, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṳ’
  DEADTRANS(0x2025, L'u'  , 0x1e73, NORMAL_CHARACTER),	// ‘u’ -> ‘ṳ’
  DEADTRANS(0x2025, 0x2025, L':'  , CHAINED_DEAD_KEY),	// ‹Diaeresis Below› -> ‹Diaeresis›
  DEADTRANS(0x2025, L' '  , 0x2025, NORMAL_CHARACTER),	// U+0020 -> U+2025

// Dead key: Circumflex Accent Below
  DEADTRANS(0x2038, L'.'  , 0x032d, NORMAL_CHARACTER),	// U+002E -> U+032D
  DEADTRANS(0x2038, L'D'  , 0x1e12, NORMAL_CHARACTER),	// ‘D’ -> ‘Ḓ’
  DEADTRANS(0x2038, L'E'  , 0x1e18, NORMAL_CHARACTER),	// ‘E’ -> ‘Ḙ’
  DEADTRANS(0x2038, L'L'  , 0x1e3c, NORMAL_CHARACTER),	// ‘L’ -> ‘Ḽ’
  DEADTRANS(0x2038, L'N'  , 0x1e4a, NORMAL_CHARACTER),	// ‘N’ -> ‘Ṋ’
  DEADTRANS(0x2038, L'T'  , 0x1e70, NORMAL_CHARACTER),	// ‘T’ -> ‘Ṱ’
  DEADTRANS(0x2038, L'U'  , 0x1e76, NORMAL_CHARACTER),	// ‘U’ -> ‘Ṷ’
  DEADTRANS(0x2038, L'd'  , 0x1e13, NORMAL_CHARACTER),	// ‘d’ -> ‘ḓ’
  DEADTRANS(0x2038, L'e'  , 0x1e19, NORMAL_CHARACTER),	// ‘e’ -> ‘ḙ’
  DEADTRANS(0x2038, L'l'  , 0x1e3d, NORMAL_CHARACTER),	// ‘l’ -> ‘ḽ’
  DEADTRANS(0x2038, L'n'  , 0x1e4b, NORMAL_CHARACTER),	// ‘n’ -> ‘ṋ’
  DEADTRANS(0x2038, L't'  , 0x1e71, NORMAL_CHARACTER),	// ‘t’ -> ‘ṱ’
  DEADTRANS(0x2038, L'u'  , 0x1e77, NORMAL_CHARACTER),	// ‘u’ -> ‘ṷ’
  DEADTRANS(0x2038, 0x2038, 0x02c6, CHAINED_DEAD_KEY),	// ‹Circumflex Accent Below› -> ‹Circumflex Accent›
  DEADTRANS(0x2038, L' '  , 0xa788, NORMAL_CHARACTER),	// U+0020 -> ‘ꞈ’

  0, 0
};

/***************************************************************************\\
 * Ligatures
\\***************************************************************************/


static ALLOC_SECTION_LDATA KBDTABLES KbdTables = {
  // Modifier keys
  &CharModifiers,

  // Characters tables
  aVkToWcharTable,

  // Diacritics
  aDeadKey,

  // Names of Keys
  aKeyNames,
  aKeyNamesExt,
  aKeyNamesDead,

  // Scan codes to Virtual Keys
  ausVK,
  sizeof(ausVK) / sizeof(ausVK[0]),
  aE0VscToVk,
  aE1VscToVk,

  // Locale-specific special processing
  MAKELONG(KLLF_ALTGR, KBD_VERSION),

  // Ligatures
  0, 0, NULL // No ligatures
};

PKBDTABLES KbdLayerDescriptor(VOID)
{
  return &KbdTables;
};
