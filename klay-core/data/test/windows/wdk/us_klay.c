
#include <windows.h>
#include "kbd.h"
#include "us_klay.h"

#if defined(_M_IA64)
#pragma section(".data")
#define ALLOC_SECTION_LDATA __declspec(allocate(".data"))
#else
#pragma data_seg(".data")
#define ALLOC_SECTION_LDATA
#endif

// ausVK[] - Virtual Scan Code to Virtual Key conversion table
static ALLOC_SECTION_LDATA USHORT ausVK[] = {
  T00, T01, T02, T03, T04, T05, T06, T07,
  T08, T09, T0A, T0B, T0C, T0D, T0E, T0F,
  T10, T11, T12, T13, T14, T15, T16, T17,
  T18, T19, T1A, T1B, T1C, T1D, T1E, T1F,
  T20, T21, T22, T23, T24, T25, T26, T27,
  T28, T29, T2A, T2B, T2C, T2D, T2E, T2F,
  T30, T31, T32, T33, T34, T35,

  /*
   * Right-hand Shift key must have KBDEXT bit set.
   */
  T36 | KBDEXT,
  T37 | KBDMULTIVK,               // numpad_* + Shift/Alt -> SnapShot

  T38, T39, T3A, T3B, T3C, T3D, T3E,
  T3F, T40, T41, T42, T43, T44,

  /*
   * NumLock Key:
   *     KBDEXT     - VK_NUMLOCK is an Extended key
   *     KBDMULTIVK - VK_NUMLOCK or VK_PAUSE (without or with CTRL)
   */
  T45 | KBDEXT | KBDMULTIVK,
  T46 | KBDMULTIVK,

  /*
   * Number Pad keys:
   *     KBDNUMPAD  - digits 0-9 and decimal point.
   *     KBDSPECIAL - require special processing by Windows
   */
  T47 | KBDNUMPAD | KBDSPECIAL,   // Numpad 7 (Home)
  T48 | KBDNUMPAD | KBDSPECIAL,   // Numpad 8 (Up),
  T49 | KBDNUMPAD | KBDSPECIAL,   // Numpad 9 (PgUp),
  T4A,
  T4B | KBDNUMPAD | KBDSPECIAL,   // Numpad 4 (Left),
  T4C | KBDNUMPAD | KBDSPECIAL,   // Numpad 5 (Clear),
  T4D | KBDNUMPAD | KBDSPECIAL,   // Numpad 6 (Right),
  T4E,
  T4F | KBDNUMPAD | KBDSPECIAL,   // Numpad 1 (End),
  T50 | KBDNUMPAD | KBDSPECIAL,   // Numpad 2 (Down),
  T51 | KBDNUMPAD | KBDSPECIAL,   // Numpad 3 (PgDn),
  T52 | KBDNUMPAD | KBDSPECIAL,   // Numpad 0 (Ins),
  T53 | KBDNUMPAD | KBDSPECIAL,   // Numpad . (Del),

  T54, T55, T56, T57, T58, T59, T5A, T5B,
  T5C, T5D, T5E, T5F, T60, T61, T62, T63,
  T64, T65, T66, T67, T68, T69, T6A, T6B,
  T6C, T6D, T6E, T6F, T70, T71, T72, T73,
  T74, T75, T76, T77, T78, T79, T7A, T7B,
  T7C, T7D, T7E, T7F
};

static ALLOC_SECTION_LDATA VSC_VK aE0VscToVk[] = {
  { 0x10, X10 | KBDEXT              },  // Speedracer: Previous Track
  { 0x19, X19 | KBDEXT              },  // Speedracer: Next Track
  { 0x1D, X1D | KBDEXT              },  // RControl
  { 0x20, X20 | KBDEXT              },  // Speedracer: Volume Mute
  { 0x21, X21 | KBDEXT              },  // Speedracer: Launch App 2
  { 0x22, X22 | KBDEXT              },  // Speedracer: Media Play/Pause
  { 0x24, X24 | KBDEXT              },  // Speedracer: Media Stop
  { 0x2E, X2E | KBDEXT              },  // Speedracer: Volume Down
  { 0x30, X30 | KBDEXT              },  // Speedracer: Volume Up
  { 0x32, X32 | KBDEXT              },  // Speedracer: Browser Home
  { 0x35, X35 | KBDEXT              },  // Numpad Divide
  { 0x37, X37 | KBDEXT              },  // Snapshot
  { 0x38, X38 | KBDEXT              },  // RMenu
  { 0x47, X47 | KBDEXT              },  // Home
  { 0x48, X48 | KBDEXT              },  // Up
  { 0x49, X49 | KBDEXT              },  // Prior
  { 0x4B, X4B | KBDEXT              },  // Left
  { 0x4D, X4D | KBDEXT              },  // Right
  { 0x4F, X4F | KBDEXT              },  // End
  { 0x50, X50 | KBDEXT              },  // Down
  { 0x51, X51 | KBDEXT              },  // Next
  { 0x52, X52 | KBDEXT              },  // Insert
  { 0x53, X53 | KBDEXT              },  // Delete
  { 0x5B, X5B | KBDEXT              },  // Left Win
  { 0x5C, X5C | KBDEXT              },  // Right Win
  { 0x5D, X5D | KBDEXT              },  // Application
  { 0x5F, X5F | KBDEXT              },  // Speedracer: Sleep
  { 0x65, X65 | KBDEXT              },  // Speedracer: Browser Search
  { 0x66, X66 | KBDEXT              },  // Speedracer: Browser Favorites
  { 0x67, X67 | KBDEXT              },  // Speedracer: Browser Refresh
  { 0x68, X68 | KBDEXT              },  // Speedracer: Browser Stop
  { 0x69, X69 | KBDEXT              },  // Speedracer: Browser Forward
  { 0x6A, X6A | KBDEXT              },  // Speedracer: Browser Back
  { 0x6B, X6B | KBDEXT              },  // Speedracer: Launch App 1
  { 0x6C, X6C | KBDEXT              },  // Speedracer: Launch Mail
  { 0x6D, X6D | KBDEXT              },  // Speedracer: Launch Media Selector
  { 0x1C, X1C | KBDEXT              },  // Numpad Enter
  { 0x46, X46 | KBDEXT              },  // Break (Ctrl + Pause)
  { 0,      0                       }
};

static ALLOC_SECTION_LDATA VSC_VK aE1VscToVk[] = {
  { 0x1D, Y1D                       },  // Pause
  { 0   ,   0                       }
};

// aVkToBits[] - map Virtual Keys to Modifier Bits
static ALLOC_SECTION_LDATA VK_TO_BIT aVkToBits[] = {
  { VK_CONTROL   , KBDCTRL                        },
  { VK_MENU      , KBDALT                         },
  { VK_SHIFT     , KBDSHIFT                       },
  { 0, 0 }
};

// aModification[] - map character modifier bits to modification number
static ALLOC_SECTION_LDATA MODIFIERS CharModifiers = {
  &aVkToBits[0],
  3,
  {
  // Level index -> Modifiers pressed       -> Level name
    0            /*                         -> Lower Alphanumeric       */,
    1            /* Shift                   -> Upper Alphanumeric       */,
    2            /*       Control           -> Control                  */
  }
};

/***************************************************************************\\
*
* aVkToWch<n>[]  - Virtual Key to WCHAR translation for <n> shift states
*
* Table attributes: Unordered Scan, null-terminated
*
* Search this table for an entry with a matching Virtual Key to find the
* corresponding unshifted and shifted WCHAR characters.
*
* Special values for VirtualKey (column 1)
*     0xff          - dead chars for the previous entry
*     0             - terminate the list
*
* Special values for Attributes (column 2)
*     CAPLOK bit    - CAPS-LOCK affect this key like SHIFT
*
* Special values for wch[*] (column 3 & 4)
*     WCH_NONE      - No character
*     WCH_DEAD      - Dead Key (diaresis) or invalid (US keyboard has none)
*     WCH_LGTR      - Ligature (generates multiple characters)
*
\\***************************************************************************/

// Errors encountered while creating aVkToWch tables:

static ALLOC_SECTION_LDATA VK_TO_WCHARS2 aVkToWchOther2[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 
  /*                                U+0030 0, U+0029 ) */
  {'0'          , 0               , L'0'    , L')'    },
  /*                                U+0031 1, U+0021 ! */
  {'1'          , 0               , L'1'    , L'!'    },
  /*                                U+0032 2, U+0040 @ */
  {'2'          , 0               , L'2'    , L'@'    },
  /*                                U+0033 3, U+0023 # */
  {'3'          , 0               , L'3'    , L'#'    },
  /*                                U+0034 4, U+0025 % */
  {'4'          , 0               , L'4'    , L'%'    },
  /*                                U+0035 5, U+0024 $ */
  {'5'          , 0               , L'5'    , L'$'    },
  /*                                U+0036 6, U+005E ^ */
  {'6'          , 0               , L'6'    , L'^'    },
  /*                                U+0037 7, U+0026 & */
  {'7'          , 0               , L'7'    , L'&'    },
  /*                                U+0038 8, U+002A * */
  {'8'          , 0               , L'8'    , L'*'    },
  /*                                U+0039 9, U+0028 ( */
  {'9'          , 0               , L'9'    , L'('    },
  /*                                U+0061 a, U+0041 A */
  {'A'          , CAPLOK          , L'a'    , L'A'    },
  /*                                U+0062 b, U+0042 B */
  {'B'          , CAPLOK          , L'b'    , L'B'    },
  /*                                U+0063 c, U+0043 C */
  {'C'          , CAPLOK          , L'c'    , L'C'    },
  /*                                U+0064 d, U+0044 D */
  {'D'          , CAPLOK          , L'd'    , L'D'    },
  /*                                U+0065 e, U+0045 E */
  {'E'          , CAPLOK          , L'e'    , L'E'    },
  /*                                U+0066 f, U+0046 F */
  {'F'          , CAPLOK          , L'f'    , L'F'    },
  /*                                U+0067 g, U+0047 G */
  {'G'          , CAPLOK          , L'g'    , L'G'    },
  /*                                U+0068 h, U+0048 H */
  {'H'          , CAPLOK          , L'h'    , L'H'    },
  /*                                U+0069 i, U+0049 I */
  {'I'          , CAPLOK          , L'i'    , L'I'    },
  /*                                U+006A j, U+004A J */
  {'J'          , CAPLOK          , L'j'    , L'J'    },
  /*                                U+006B k, U+004B K */
  {'K'          , CAPLOK          , L'k'    , L'K'    },
  /*                                U+006C l, U+004C L */
  {'L'          , CAPLOK          , L'l'    , L'L'    },
  /*                                U+006D m, U+004D M */
  {'M'          , CAPLOK          , L'm'    , L'M'    },
  /*                                U+006E n, U+004E N */
  {'N'          , CAPLOK          , L'n'    , L'N'    },
  /*                                U+006F o, U+004F O */
  {'O'          , CAPLOK          , L'o'    , L'O'    },
  /*                                U+0070 p, U+0050 P */
  {'P'          , CAPLOK          , L'p'    , L'P'    },
  /*                                U+0071 q, U+0051 Q */
  {'Q'          , CAPLOK          , L'q'    , L'Q'    },
  /*                                U+0072 r, U+0052 R */
  {'R'          , CAPLOK          , L'r'    , L'R'    },
  /*                                U+0073 s, U+0053 S */
  {'S'          , CAPLOK          , L's'    , L'S'    },
  /*                                U+0074 t, U+0054 T */
  {'T'          , CAPLOK          , L't'    , L'T'    },
  /*                                U+0075 u, U+0055 U */
  {'U'          , CAPLOK          , L'u'    , L'U'    },
  /*                                U+0076 v, U+0056 V */
  {'V'          , CAPLOK          , L'v'    , L'V'    },
  /*                                U+0077 w, U+0057 W */
  {'W'          , CAPLOK          , L'w'    , L'W'    },
  /*                                U+0078 x, U+0058 X */
  {'X'          , CAPLOK          , L'x'    , L'X'    },
  /*                                U+0079 y, U+0059 Y */
  {'Y'          , CAPLOK          , L'y'    , L'Y'    },
  /*                                U+007A z, U+005A Z */
  {'Z'          , CAPLOK          , L'z'    , L'Z'    },
  /*                                KP_Decimal, KP_Decimal */
  {VK_ABNT_C2   , 0               , L'.'    , L'.'    },
  /*                                KP_Add  , KP_Add   */
  {VK_ADD       , 0               , L'+'    , L'+'    },
  /*                                KP_Equal, KP_Equal */
  {VK_CLEAR     , 0               , L'='    , L'='    },
  /*                                KP_Decimal, KP_Decimal */
  {VK_DECIMAL   , 0               , L'.'    , L'.'    },
  /*                                KP_Divide, KP_Divide */
  {VK_DIVIDE    , 0               , L'/'    , L'/'    },
  /*                                KP_Multiply, KP_Multiply */
  {VK_MULTIPLY  , 0               , L'*'    , L'*'    },
  /*                                U+003B ;, U+003A : */
  {VK_OEM_1     , 0               , L';'    , L':'    },
  /*                                U+002F /, U+003F ? */
  {VK_OEM_2     , 0               , L'/'    , L'?'    },
  /*                                U+0060 `, U+007E ~ */
  {VK_OEM_3     , 0               , L'`'    , L'~'    },
  /*                                U+0027 ', U+0022 " */
  {VK_OEM_7     , 0               , L'\''   , L'"'    },
  /*                                U+002C ,, U+003C < */
  {VK_OEM_COMMA , 0               , L','    , L'<'    },
  /*                                U+002D -, U+005F _ */
  {VK_OEM_MINUS , 0               , L'-'    , L'_'    },
  /*                                U+002E ., U+003E > */
  {VK_OEM_PERIOD, 0               , L'.'    , L'>'    },
  /*                                U+003D =, U+002B + */
  {VK_OEM_PLUS  , 0               , L'='    , L'+'    },
  /*                                KP_Subtract, KP_Subtract */
  {VK_SUBTRACT  , 0               , L'-'    , L'-'    },
  /*                                Tab     , Tab      */
  {VK_TAB       , 0               , L'\t'   , L'\t'   },
  {0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS3 aVkToWchOther3[] = {
  // Virtual key, Modifier options, Level 0 , Level 1 , Level 2 
  /*                                BackSpace, BackSpace, U+007F ⌦ */
  {VK_BACK      , 0               , 0x0008  , 0x0008  , 0x007f  },
  /*                                U+0003 ␃, U+0003 ␃, U+0003 ␃ */
  {VK_CANCEL    , 0               , 0x0003  , 0x0003  , 0x0003  },
  /*                                Escape  , Escape  , Escape   */
  {VK_ESCAPE    , 0               , 0x001b  , 0x001b  , 0x001b  },
  /*                                U+005C \, U+007C |, U+001C ␜ */
  {VK_OEM_102   , 0               , L'\\'   , L'|'    , 0x001c  },
  /*                                U+005B [, U+007B {, U+001B ␛ */
  {VK_OEM_4     , 0               , L'['    , L'{'    , 0x001b  },
  /*                                U+005C \, U+007C |, U+001C ␜ */
  {VK_OEM_5     , 0               , L'\\'   , L'|'    , 0x001c  },
  /*                                U+005D ], U+007D }, U+001D ␝ */
  {VK_OEM_6     , 0               , L']'    , L'}'    , 0x001d  },
  /*                                Return  , Return  , U+000A ␊ */
  {VK_RETURN    , 0               , L'\r'   , L'\r'   , L'\n'   },
  /*                                U+0020 ␣, U+0020 ␣, U+0020 ␣ */
  {VK_SPACE     , 0               , L' '    , L' '    , L' '    },
  {0, 0, 0, 0, 0}
};

static ALLOC_SECTION_LDATA VK_TO_WCHARS1 aVkToWchNumpad1[] = {
  // Virtual key, Modifier options, Level 0 
  /*                                KP_0     */
  {VK_NUMPAD0   , 0               , L'0'    },
  /*                                KP_1     */
  {VK_NUMPAD1   , 0               , L'1'    },
  /*                                KP_2     */
  {VK_NUMPAD2   , 0               , L'2'    },
  /*                                KP_3     */
  {VK_NUMPAD3   , 0               , L'3'    },
  /*                                KP_4     */
  {VK_NUMPAD4   , 0               , L'4'    },
  /*                                KP_5     */
  {VK_NUMPAD5   , 0               , L'5'    },
  /*                                KP_6     */
  {VK_NUMPAD6   , 0               , L'6'    },
  /*                                KP_7     */
  {VK_NUMPAD7   , 0               , L'7'    },
  /*                                KP_8     */
  {VK_NUMPAD8   , 0               , L'8'    },
  /*                                KP_9     */
  {VK_NUMPAD9   , 0               , L'9'    },
  {0, 0, 0}
};



// aVkToWcharTable - List the various aVkToWchN tables in use
static ALLOC_SECTION_LDATA VK_TO_WCHAR_TABLE aVkToWcharTable[] = {
  { (PVK_TO_WCHARS1)aVkToWchOther2 , 2 , sizeof(aVkToWchOther2[0]) },
  { (PVK_TO_WCHARS1)aVkToWchOther3 , 3 , sizeof(aVkToWchOther3[0]) },
  // The numpad keys must come last so that VkKeyScan interprets number characters
  // as coming from the main section of the kbd before considering the numpad.
  { (PVK_TO_WCHARS1)aVkToWchNumpad1 , 1 , sizeof(aVkToWchNumpad1[0]) },
  {                      NULL , 0 , 0                     },
};

/***************************************************************************\\
* aKeyNames[], aKeyNamesExt[] - Virtual Scancode to Key Name tables
*
* Table attributes: Ordered Scan (by scancode), null-terminated
*
* Only the names of Extended, NumPad, Dead and Non-Printable keys are here.
* (Keys producing printable characters are named by that character)
\\***************************************************************************/
static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNames[] = {
  0x01, L"Esc",
  0x0e, L"Backspace",
  0x0f, L"Tab",
  0x1c, L"Enter",
  0x1d, L"Left Ctrl",
  0x2a, L"Left Shift",
  0x36, L"Right Shift",
  0x37, L"Num *",
  0x38, L"Left Alt",
  0x39, L"Space",
  0x3a, L"Caps Lock",
  0x3b, L"F1",
  0x3c, L"F2",
  0x3d, L"F3",
  0x3e, L"F4",
  0x3f, L"F5",
  0x40, L"F6",
  0x41, L"F7",
  0x42, L"F8",
  0x43, L"F9",
  0x44, L"F10",
  0x45, L"Pause",
  0x46, L"Scroll Lock",
  0x47, L"Num 7",
  0x48, L"Num 8",
  0x49, L"Num 9",
  0x4a, L"Num -",
  0x4b, L"Num 4",
  0x4c, L"Num 5",
  0x4d, L"Num 6",
  0x4e, L"Num +",
  0x4f, L"Num 1",
  0x50, L"Num 2",
  0x51, L"Num 3",
  0x52, L"Num 0",
  0x53, L"Num Del",
  0x54, L"Sys Req",
  0x57, L"F11",
  0x58, L"F12",
  0x64, L"F13",
  0x65, L"F14",
  0x66, L"F15",
  0x67, L"F16",
  0x68, L"F17",
  0x69, L"F18",
  0x6a, L"F19",
  0x6b, L"F20",
  0x6c, L"F21",
  0x6d, L"F22",
  0x6e, L"F23",
  0x76, L"F24",
  0, NULL
};

static ALLOC_SECTION_LDATA VSC_LPWSTR aKeyNamesExt[] = {
  0x1c, L"Num Enter",
  0x1d, L"Right Ctrl",
  0x35, L"Num /",
  0x37, L"Prnt Scrn",
  0x38, L"Right Alt",
  0x45, L"Num Lock",
  0x46, L"Break",
  0x47, L"Home",
  0x48, L"Up",
  0x49, L"Page Up",
  0x4b, L"Left",
  0x4d, L"Right",
  0x4f, L"End",
  0x50, L"Down",
  0x51, L"Page Down",
  0x52, L"Insert",
  0x53, L"Delete",
  0x54, L"<00>",
  0x56, L"Help",
  0x5b, L"Left Windows",
  0x5c, L"Right Windows",
  0x5d, L"Application",
  0, NULL
};

/***************************************************************************\\
 * Dead Keys
\\***************************************************************************/
static ALLOC_SECTION_LDATA DEADKEY_LPWSTR aKeyNamesDead[] = {

  NULL
};

static ALLOC_SECTION_LDATA DEADKEY aDeadKey[] = {

  0, 0
};

/***************************************************************************\\
 * Ligatures
\\***************************************************************************/


static ALLOC_SECTION_LDATA KBDTABLES KbdTables = {
  // Modifier keys
  &CharModifiers,

  // Characters tables
  aVkToWcharTable,

  // Diacritics
  aDeadKey,

  // Names of Keys
  aKeyNames,
  aKeyNamesExt,
  aKeyNamesDead,

  // Scan codes to Virtual Keys
  ausVK,
  sizeof(ausVK) / sizeof(ausVK[0]),
  aE0VscToVk,
  aE1VscToVk,

  // Locale-specific special processing
  MAKELONG(0, KBD_VERSION),

  // Ligatures
  0, 0, NULL // No ligatures
};

PKBDTABLES KbdLayerDescriptor(VOID)
{
  return &KbdTables;
};
