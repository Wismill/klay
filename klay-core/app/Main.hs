{-# LANGUAGE OverloadedLists #-}

module Main where

import Data.List (nub)
import Data.Foldable (traverse_)
import System.FilePath ((</>))

import Options.Applicative
import Main.Utf8 (withUtf8)

import Klay.App.Symbols qualified as Chars
import Klay.App.Check qualified as Check
import Klay.App.Common
import Klay.App.Export
import Klay.App.Import qualified as I
import Klay.Export.CSV (CSVFormat(..))
import Klay.Export.Linux (LinuxFormat(..))
import Klay.Export.Text (TextFormat(..))
import Klay.Export.Windows (WindowsFormat(..))
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Carpalx qualified as Carpalx
import Klay.Keyboard.Layout.Examples.Colemak qualified as Colemak
import Klay.Keyboard.Layout.Examples.Dvorak qualified as Dvorak
import Klay.Keyboard.Layout.Examples.Latin qualified as Latin
import Klay.Keyboard.Layout.Examples.Latin.Type4 qualified as Latin4
import Klay.Keyboard.Layout.Examples.Neo2 qualified as Neo2
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Workman qualified as Workman
import Klay.OS (OsSpecific(..))
import Klay.Typing.Method
import Klay.Typing.Method.ISO.O0 qualified as O0
import Klay.Typing.Method.TypeMatrix.Default qualified as TM
import Klay.Utils.UserInput

main :: IO ()
main = withUtf8 $ defaultMain
  [ US.layout
  , DE.layout
  , ES.layout
  , Carpalx.layout
  , Colemak.layout
  , Dvorak.layout
  , Workman.layout
  , Bépo.layout
  , Neo2.layout
  , Latin.layout
  , Latin4.layout
  ]
  [ O0.typingMethod, TM.typingMethod ]

defaultMain :: [MultiOsRawLayout] -> [TypingMethod] -> IO ()
defaultMain ls ms = execParser opts >>= processOptions
  where
    opts = info (parser' <**> helper)
      ( fullDesc
     <> progDesc "klay ─ Keyboard layout designer"
     <> header "Toolkit for keyboard layout design" )
    parser' = subparser $ mconcat [check_cmd, symbols_cmd, export_cmd]
    check_cmd = command "check" (info
      (Check <$> checkParser ls <**> helper)
      (progDesc "Check the keyboard layout"))
    symbols_cmd = command "symbols" (info
      (Symbols <$> symbolsParser ls <**> helper)
      (progDesc "Output the reachable symbols of the layout"))
    export_cmd = command "export" (info
      (Export <$> exportParser ls ms <**> helper)
      (progDesc "Export files"))

data Command
  = Check CheckOptions
  | Symbols SymbolsOptions
  | Export ExportOptions

processOptions :: Command -> IO ()
processOptions (Symbols opts) = layoutsSymbols opts
processOptions (Check opts)      = checkLayouts opts
processOptions (Export opts)     = generator opts

--- Export --------------------------------------------------------------------

data GeneratorFormat
  = CSV     !CSVFormat
  | Json
  | Kmonad
  | Linux   !LinuxFormat
  | Text    !TextFormat
  | Yaml
  | Windows !WindowsFormat
  deriving (Eq, Ord, Show)

data ExportOptions = ExportOptions
  { _exportLayouts :: [MultiOsRawLayout]
  , _exportInputFiles :: [FilePath]
  , _exportInputFormat :: I.ImportFormat
  , _exportOS :: OsSpecific
  , _exportGeometry :: H.Geometry
  , _exportPath :: FilePath
  , _exportFormats :: [GeneratorFormat]
  }

generator :: ExportOptions -> IO ()
generator opts = do
  imported_layouts <- I.importLayouts (_exportInputFormat opts) inputPaths
  traverse_ export_files (liftA2 (,) (layouts <> imported_layouts) formats)
  where
    export_files :: (MultiOsRawLayout, GeneratorFormat) -> IO ()
    export_files (l, f) = case f of
      CSV f'             -> exportCsv path f' the_os l
      Kmonad             -> exportKmonad path l
      Json               -> exportJson path l
      Yaml               -> exportYaml path l
      Linux XKB          -> exportXkb path l
      Text f'            -> exportText path f' the_os geometry l
      Windows WDK        -> exportWdk path l
      Windows (MSKLC f') -> exportMsklc path f' l
      where path = mkLayoutPath l
    the_os = _exportOS opts
    geometry = _exportGeometry opts
    layouts = _exportLayouts opts
    formats = nub . _exportFormats $ opts
    destinationPath = _exportPath opts
    inputPaths = _exportInputFiles opts
    mkLayoutPath :: MultiOsRawLayout -> FilePath
    mkLayoutPath = (destinationPath </>) . escapeFilename' . _name . _metadata

exportParser :: [MultiOsRawLayout] -> [TypingMethod] -> Parser ExportOptions
exportParser ls _ms = ExportOptions
  <$> many (layoutOption "layout" 'l' "Layout to export at index LAYOUT" ls)
  <*> I.importOption
  <*> I.importFormatOption
  <*> (osOption <|> pure OsIndependent)
  <*> geometryOption "Geometry to export"
  <*> outputDirOption
  <*> (mconcat <$>
      some ( flag' allFormats (long "all-formats" <> short 'F' <> help "Export all formats")
        <|> kmonadFlags Kmonad
        <|> textFlags Text
        <|> csvFlags CSV
        <|> jsonFlags Json
        <|> yamlFlags Yaml
        <|> mkLinuxFlags Linux
        <|> windowsFlags Windows
          )
      )
  where allFormats = mconcat
          [ [Kmonad]
          , Text <$> allTextFormats
          , CSV  <$> enumFromTo minBound maxBound
          , mkLinuxFormats Linux
          , mkWindowsFormats Windows
          ]

--- Check ---------------------------------------------------------------------

data CheckOptions = CheckOptions
  { _checkLayouts :: [MultiOsRawLayout]
  , _checkInputFiles :: [FilePath]
  , _checkInputFormat :: I.ImportFormat
  , _checkOs :: [OsSpecific]
  }

checkLayouts :: CheckOptions -> IO ()
checkLayouts opts = do
  imported_layouts <- I.importLayouts (_checkInputFormat opts) (_checkInputFiles opts)
  let layouts = _checkLayouts opts <> imported_layouts
  Check.checkLayouts layouts (_checkOs opts)

checkParser :: [MultiOsRawLayout] -> Parser CheckOptions
checkParser ls = CheckOptions
  <$> some (layoutOption "layout" 'l' "Layout to check at index LAYOUT" ls)
  <*> I.importOption
  <*> I.importFormatOption
  <*> many osOption'

--- Symbols ----------------------------------------------------------------

data SymbolsOptions = SymbolsOptions
  { _symbolsLayouts :: [MultiOsRawLayout]
  , _symbolsInputFiles :: [FilePath]
  , _symbolsInputFormat :: I.ImportFormat
  , _symbolsOs :: OsSpecific
  , _symbolsDetails :: Chars.DetailsLevel
  , _symbolsCodePoints :: Chars.CodePoints
  }

layoutsSymbols :: SymbolsOptions -> IO ()
layoutsSymbols opts = do
  imported_layouts <- I.importLayouts (_symbolsInputFormat opts) (_symbolsInputFiles opts)
  let layouts = _symbolsLayouts opts <> imported_layouts
  Chars.layoutsSymbols layouts (_symbolsOs opts) (_symbolsDetails opts) (_symbolsCodePoints opts)

symbolsParser :: [MultiOsRawLayout] -> Parser SymbolsOptions
symbolsParser ls = SymbolsOptions
  <$> some (layoutOption "layout" 'l' "Layout to analyse at index LAYOUT" ls)
  <*> I.importOption
  <*> I.importFormatOption
  <*> (osOption <|> pure OsIndependent)
  <*> Chars.detailsOption
  <*> Chars.codePointsOption
