# Klay

`klay-core` is part of __[Klay]__, a cross-platform command-line utility to design __keyboard layouts__.

# `klay-core`

This package `klay-core` provides the following features:

* Import a layout from the following formats:

* __[YAML]__
* __[JSON]__
* __[MSKLC]__

* Export a layout in the following formats:

  * __Text:__ ASCII art for documentation
  * __CSV:__ list of characters by key combination
  * __Linux:__ [XKB] and [XCompose]
  * __Windows:__

      * [MSKLC]: Microsoft Keyboard Layout Creator
      * C sources to build with either [MSKLC] or [WDK]

* Check a layout for possible errors, such as:

    * Locale characters coverage
    * Incorrect dead key definition

[Klay]: https://gitlab.com/Wismill/klay/
[YAML]: https://yaml.org/
[JSON]: https://www.json.org/
[XKB]: https://wiki.archlinux.org/index.php/X_keyboard_extension
[XCompose]: https://en.wikipedia.org/wiki/Compose_key
[MSKLC]: https://microsoft-keyboard-layout-creator.software.informer.com/1.4/
[WDK]: https://docs.microsoft.com/en-us/windows-hardware/drivers/download-the-wdk
