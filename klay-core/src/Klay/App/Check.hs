module Klay.App.Check
  ( checkLayouts
  , printDeadKeysErrors
  ) where

import Data.Foldable (traverse_, forM_, foldl')
import Data.List (intersperse)
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NE
import Data.Map.Strict qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import Control.Monad (unless, void)
import Data.Text.IO qualified as TIO
import Data.Text.Encoding qualified as T

import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal
import Data.Yaml qualified as Yaml

import Klay.App.Common
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Check qualified as C
import Klay.Check.Locales qualified as L
import Klay.Utils.Orphans.BCP47 ()
import Klay.Utils.UserInput (escapeLine)
import Klay.OS (OsSpecific(..), allOsSpecific)


checkLayouts :: [MultiOsRawLayout] -> [OsSpecific] -> IO ()
checkLayouts ls oss = traverse_ checkLayout ls
  where
    oss' = maybe allOsSpecific NE.nub (NE.nonEmpty oss)
    checkLayout :: MultiOsRawLayout -> IO ()
    checkLayout l = do
      putDoc $ mkTitle (mconcat ["*** Layout: ", escapeLine . _name . _metadata $ l, " ***"]) <> line
      let errors@C.LayoutErrors
            { C._checkMappingErrors=mapping_errors
            , C._checkDkErrors=dkErrors
            } = C.checkLayoutByOs oss' l
          vks = C._vkNotMapped <$> mapping_errors
      if errors == mempty
        then putStrLn "No error detected"
        else do
          putDoc $ mkError "There were errors detected:" <> line
          unless (null vks) do
            putDoc $ mkStrong "• The following virtual keys are mapped to a physical key but to no actions:" <> line
            void $ Map.traverseWithKey (\os vk -> putStrLn $ mconcat [showItem os, ": ", show vk]) vks
          printDeadKeysErrors dkErrors
      let lr = L.checkLocales l
          f (L.ExemplarsResult s n c) = L.ExemplarsResult (Set.toList s) (Set.toList n) c
          lr' = fmap (fmap (fmap (fmap f))) lr
      TIO.putStrLn . T.decodeUtf8 . Yaml.encode $ lr'

printDeadKeysErrors :: DeadKeyError -> IO ()
printDeadKeysErrors
  DeadKeyError{ _dkUndefined=dk_undefined
              , _dkEmptyDefinition=dk_empty
              , _dkUndecidable=dk_undecidable
              , _dkUnmappable=dk_unmappable
              , _dkUnsupported=dk_unsupported
              , _dkCombosErrors=combos_errors
              , _dkOverlappingBaseChars=overlapping_dk
              , _dkOverlappingCombos=overlapping_combos} = do
  unless (null overlapping_dk) do
    putDoc $ mkEmphasis "• The following dead keys use the same base character:" <> line
    void $ Map.traverseWithKey (\c dks -> putStrLn $ showOveralappingDK c dks) overlapping_dk
  unless (null dk_undefined) do
    putDoc $ mkEmphasis "• The following dead keys have no definitions at all in the layout:" <> line
    forM_ dk_undefined (putStrLn . showItem)
  unless (null dk_empty) do
    putDoc $ mkEmphasis "• The following dead keys have no valid definitions in the layout:" <> line
    forM_ dk_empty (putStrLn . showItem)
  unless (null dk_undecidable) do
    putDoc $ mkEmphasis "• The following dead keys have undecidable definitions in the layout:" <> line
    forM_ dk_undecidable (putStrLn . showItem)
  unless (null dk_unmappable) do
    putDoc $ mkEmphasis "• The following dead keys cannot be mapped in the layout:" <> line
    forM_ dk_unmappable (putStrLn . showItem)
  unless (null overlapping_combos) do
    putDoc $ mkEmphasis "• The following dead key combinations are overlapping:" <> line
    void $ Map.traverseWithKey (\dk es -> putStrLn $ showOveralappingCombos dk es) overlapping_combos
  unless (null combos_errors) do
    putDoc $ mkEmphasis "• The following dead key combinations are invalid:" <> line
    void $ Map.traverseWithKey (\dk es -> putStrLn $ showCombosErrors dk es) combos_errors
  unless (null dk_unsupported) do
    putDoc $ mkEmphasis "• The following dead keys are not supported (unknown error):" <> line
    forM_ dk_unsupported (putStrLn . showItem)
  where
    showOveralappingDK :: Char -> Set DeadKey -> String
    showOveralappingDK c dks = mconcat $ "  ⁃ Base character: ‘" : [c] : "’ " : foldl' (\acc dk -> "\n    ⁃ " : prettyDeadKey dk : acc) mempty dks
    showCombosErrors :: DeadKey -> DeadKeyComboErrors -> String
    showCombosErrors dk es = mconcat $ "  ⁃ " : prettyDeadKey dk : fmap showComboErrors es
    showComboErrors :: (DeadKeyCombo, DeadKeyComboError) -> String
    showComboErrors (combo, DeadKeyComboError symbols result) = case (symbols_msg, result_msg) of
      ("", "") -> mempty
      (msg, "") -> combo_msg <> msg
      ("", msg) -> combo_msg <> msg
      (msg1, msg2) -> mconcat
        [ combo_msg
        , "\n      ⁃", msg1
        , "\n      ⁃", msg2 ]
      where
        combo_msg = mconcat ["\n    ⁃ Combo: [ ", prettyDeadKeyCombo combo, " ]:"]
        symbols_msg = show_invalid_symbols symbols
        result_msg = show_invalid_result result
    showOveralappingCombos :: NonEmpty Symbol -> Set (DeadKey, DeadKeyCombo) -> String
    showOveralappingCombos symbols combos = mconcat $ "  ⁃ Overlapping combo: " : show_combo_symbols symbols : Set.foldl' showOveralappingCombo mempty combos
    showOveralappingCombo :: [String] -> (DeadKey, DeadKeyCombo) -> [String]
    showOveralappingCombo acc (dk, combo) = "\n    ⁃ " : showDeadKeyName dk : ", Combo: " : prettyDeadKeyCombo combo : acc
    show_combo_symbols :: NonEmpty Symbol -> String
    show_combo_symbols symbols = mconcat . NE.toList $ NE.intersperse " + " (fmap prettySymbol symbols)
    show_invalid_symbols ss
      | null ss = mempty
      | otherwise = mconcat [" Invalid symbols: ", mconcat . intersperse ", " . fmap prettySymbol . Set.toList $ ss]
    show_invalid_result Nothing  = mempty
    show_invalid_result (Just r) = mconcat [" Invalid result: ", prettyResult r]

showItem :: Show a => a -> String
showItem = ("  ⁃ " <>) . show
