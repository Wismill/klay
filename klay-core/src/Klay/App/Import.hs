module Klay.App.Import
  ( ImportFormat(..)
  , importLayouts
  , importOption
  , importFormatOption
  ) where

import Data.Text.Lazy qualified as TL
import Data.Foldable (traverse_)
import Data.Bifunctor

import Options.Applicative
import Options.Applicative.Help.Pretty qualified as O
import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal

import Klay.App.Common
import Klay.Import.Yaml (importYamlFile)
import Klay.Import.Windows.MSKLC (parseMsklcLayoutFile)
import Klay.Keyboard.Layout

data ImportFormat
  -- = Auto -- [TODO]
  = Yaml
  | MSKLC
  deriving (Bounded, Enum, Eq, Show, Read)

-- | Import layouts from YAML/JSON files
importLayouts :: ImportFormat -> [FilePath] -> IO [MultiOsRawLayout]
importLayouts format inputs = do
  import_results <- traverse decode inputs
  let (errors, imported_layouts) = foldr go mempty import_results
  traverse_ show_error errors
  pure imported_layouts
  where go (Left (p, e)) = first ((p, e):)
        go (Right l)     = second (l:)
        decode p = first (p,) <$> case format of
          Yaml  -> importYamlFile p
          MSKLC -> parseMsklcLayoutFile p
        show_error (p, e) = putDoc $ (mkError . mconcat)
          [ "[ERROR] Cannot load file \""
          , TL.pack p
          , "\": "
          , TL.pack e
          ] <> line

importOption :: Parser [FilePath]
importOption = many (strOption
    ( long "input"
  <> short 'i'
  <> help "Layout file to import"
  <> metavar "PATH" ))

importFormatOption :: Parser ImportFormat
importFormatOption = option auto
  ( short 'f'
  <> long "from"
  <> metavar "FORMAT"
  <> showDefault
  <> value Yaml
  <> helpDoc (Just helpContent) )
  where
    helpContent = O.vcat
      $ O.text "Input format"
      : O.text "Possible values:"
      : (O.punctuate ", " . fmap (O.indent 2 . O.text . show) $ (enumFrom minBound :: [ImportFormat]))
