{-# LANGUAGE OverloadedLists     #-}
{-# LANGUAGE MonadComprehensions #-}

module Klay.App.Symbols
  ( DetailsLevel(..)
  , CodePoints(..)
  , layoutsSymbols
  , codePointsOption
  , detailsOption
  ) where

import Data.Char (isAlphaNum, isPunctuation, isSymbol)
import Data.Foldable (foldl', traverse_)
import Data.Semigroup (sconcat)
import Data.Map.Strict qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import Data.List (intersperse, sortBy)
import Data.List.NonEmpty qualified as NE
import Data.Function (on)
import Control.Monad (unless, void)

import Options.Applicative
import Options.Applicative.Help.Pretty qualified as O
import Data.Text.Prettyprint.Doc hiding (SChar)
import Data.Text.Prettyprint.Doc.Render.Terminal

import Klay.Keyboard.Hardware.Key (KeySequence, KeyModCombo(..), sequenceDifficulty)
import Klay.App.Common
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Symbols qualified as S
import Klay.Keyboard.Layout.Group
import Klay.Utils.Unicode (showUnicode)
import Klay.Utils.Unicode.Scripts (UnicodeScript(Common))
import Klay.Utils.Unicode.Blocks (UnicodeBlock, prettyUnicodeBlock)
import Klay.Utils.UserInput (escapeLine)
import Klay.OS (OsSpecific(..))

data DetailsLevel
  = Detailed
  | Summary
  deriving (Eq, Ord, Enum, Bounded, Show, Read)

data CodePoints
  = AllCodePoints
  | OnlyInvisibleChars
  deriving (Eq, Ord, Enum, Bounded, Show, Read)

layoutsSymbols
  :: [MultiOsRawLayout]
  -> OsSpecific
  -> DetailsLevel
  -> CodePoints
  -> IO ()
layoutsSymbols ls os details code_points =
  traverse_ layoutSymbols ls
  where
    layoutSymbols :: MultiOsRawLayout -> IO ()
    layoutSymbols layout@Layout
      { _metadata=metadata
      , _deadKeys=dkd } = do
        putDoc $ mkTitle (mconcat ["*** Layout: ", escapeLine . _name $ metadata, " ***"]) <> line
        traverse_ (group_characters dkd) $ finalGroupsLayouts os layout
    group_characters
      :: DeadKeyDefinitions
      -> KeyFinalGroup
      -> IO ()
    group_characters dkd g = do
      let S.ReachableResults{S._rByDirectInput=bs, S._rByDeadKey=ds} = S.groupReachableSymbols dkd g
      let bs' = S.onlyChars bs
      let ds' = S.onlyChars ds
      let dks = S.onlyDeadKeys bs
      putDoc $ mkField ("Group: " <> (escapeLine . _groupName $ g)) <> line
      --void $ Map.traverseWithKey (block_characters mempty) (S.groupByUnicodeBlock cs)
      putDoc $ mkField "• Caracters: direct input" <> line
      void $ Map.traverseWithKey charactersByScript (S.groupByUnicodeScript bs')
      putDoc $ mkField "• Caracters: by dead keys" <> line
      let dcs' = mkVkSequence bs <$> ds'
      void $ Map.traverseWithKey charactersByScript (S.groupByUnicodeScript dcs')
      putDoc $ mkField "• Dead keys" <> line
      void $ Map.traverseWithKey print_dead_key dks

    mkVkSequence :: S.BaseResults -> Map.Map DeadKey (Set (NE.NonEmpty Symbol)) -> Set KeySequence
    mkVkSequence bs = Map.foldlWithKey' go mempty
      where
        go :: Set KeySequence -> DeadKey -> Set (NE.NonEmpty Symbol) -> Set KeySequence
        go acc dk combos = case Map.lookup (RDeadKey dk) bs of
          Nothing      -> error $ "Dead key not found: " <> show dk -- [FIXME] Partial function
          Just dk_seqs -> foldl' (go' (Set.toList dk_seqs)) acc combos

        go' :: [KeySequence] -> Set KeySequence -> NE.NonEmpty Symbol -> Set KeySequence
        go' dk_seqs acc combo =
          let combo_seqs = fmap sconcat . mapM go'' $ combo
          in acc <> Set.fromList [dk_seq <> combo_seq | dk_seq <- dk_seqs, combo_seq <- combo_seqs]

        go'' :: Symbol -> [KeySequence]
        go'' (SChar c)     = Set.toList $ bs Map.! RChar c -- [FIXME] Partial function
        go'' (SDeadKey dk) = Set.toList $ bs Map.! RDeadKey dk -- [FIXME] Partial function

    charactersByScript :: UnicodeScript -> Map.Map Char (Set KeySequence) -> IO ()
    charactersByScript Common cs = do
      putStrLn $ "  • Script: " <> show Common
      void $ Map.traverseWithKey (block_characters "  ") (S.groupByUnicodeBlock cs)
    charactersByScript script cs = do
      putStrLn $ "  • Script: " <> show script
      putStrLn . mconcat $ "    Characters: " : case code_points of
        AllCodePoints -> intersperse ", " . fmap prettyChar $ Map.keys cs
        OnlyInvisibleChars -> prettyChar <$> Map.keys cs
      unless (details == Summary) $
        void $ Map.traverseWithKey print_char cs

    block_characters :: String -> UnicodeBlock -> Map.Map Char (Set KeySequence) -> IO ()
    block_characters padding block cs = do
      putStrLn $ padding <> "  • Block: " <> prettyUnicodeBlock block
      putStrLn . mconcat $ padding <> "    Characters: " : case code_points of
        AllCodePoints -> intersperse ", " . fmap prettyChar $ Map.keys cs
        OnlyInvisibleChars -> prettyChar <$> Map.keys cs
      unless (details == Summary) $
        void $ Map.traverseWithKey print_char cs

    print_char :: Char -> Set KeySequence -> IO ()
    print_char c ss = do
      putStrLn $ "    ⁃ " <> prettyChar c -- [TODO] Group by Unicode block
      traverse_ print_sequence . sort_sequences $ ss

    print_dead_key :: DeadKey -> Set KeySequence -> IO ()
    print_dead_key dk ds = do
      putStrLn $ "  ⁃ " <> show dk
      unless (details == Summary) $
        traverse_ print_sequence . sort_sequences $ ds

    sort_sequences :: Set KeySequence -> [KeySequence]
    sort_sequences = sortBy (compare `on` sequenceDifficulty) . Set.toList

    print_sequence cs = do
      putStr "      ⁃ "
      putStrLn . sconcat . NE.intersperse ", " . fmap show_vk_combo $ cs

    show_vk_combo :: KeyModCombo -> String
    show_vk_combo (KeyModCombo ms vk)
      | null ms   = show vk
      | otherwise = mconcat
          [ show_modifiers ms
          , " + "
          , show vk ]

    show_modifiers = mconcat . intersperse " + " . fmap show . Set.toList

    prettyChar c
      | isAlphaNum c || isPunctuation c || isSymbol c = case code_points of
        AllCodePoints -> c : mconcat [" (", showUnicode c, ")"]
        OnlyInvisibleChars -> [c]
      | otherwise =  case code_points of
        AllCodePoints -> showUnicode c
        OnlyInvisibleChars -> mconcat [" (", showUnicode c, ") "]

codePointsOption :: Parser CodePoints
codePointsOption = option auto
   ( long "code-points"
  <> short 'c'
  <> metavar "CODE_POINTS"
  <> value OnlyInvisibleChars
  <> showDefault
  <> helpDoc (Just helpContent) )
  where
    helpContent = O.vcat
      $ O.text "Show Unicode code points."
      : O.text "Possible values:"
      : (O.punctuate ", " . fmap (O.indent 2 . O.text . show) $ (enumFrom minBound :: [CodePoints]))


detailsOption :: Parser DetailsLevel
detailsOption = option auto
   ( long "details"
  <> short 'd'
  <> metavar "LEVEL"
  <> value Summary
  <> showDefault
  <> helpDoc (Just helpContent) )
  where
    helpContent = O.vcat
      $ O.text "Level of details."
      : O.text "Possible values:"
      : (O.punctuate ", " . fmap (O.indent 2 . O.text . show) $ (enumFrom minBound :: [DetailsLevel]))
