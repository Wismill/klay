{-# LANGUAGE OverloadedLists   #-}

module Klay.App.Export
  ( outputDirOption
  , kmonadFlags
  , exportKmonad
  , textFlags
  , allTextFormats
  , exportText
  , csvFlags
  , exportCsv
  , jsonFlags
  , exportJson
  , yamlFlags
  , exportYaml
  , mkLinuxFlags
  , mkLinuxFormats
  , exportXkb
  , windowsFlags
  , mkWindowsFormats
  , exportWdk
  , exportMsklc
  , mkLayoutName
  , mkOsPath
  , mkTitleT
  ) where


import Data.Char (toLower)
import Data.Text.Lazy qualified as TL
import System.FilePath ((</>))

import Options.Applicative
import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal

import Klay.App.Common
import Klay.Export.CSV (CSVFormat(..), generateCsv)
import Klay.Export.Linux (LinuxFormat(..), generateXkb)
import Klay.Export.Json (generateJsonFile)
import Klay.Export.Kmonad (generateKmonad)
import Klay.Export.Text
  ( TextFormat(..), LayoutSize(..)
  , allTextFormats, generateConsole, generateCommonMarkFile)
import Klay.Export.Yaml (generateYamlFile)
import Klay.Export.Windows (WindowsFormat(..), MsKlcC(..), generateWdk, generateMsklc)
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Layout
import Klay.OS (OsSpecific(..), linux, windows)

-- | Parser for output directory
outputDirOption :: Parser FilePath
outputDirOption = strOption
  ( long "output"
  <> short 'o'
  <> help "Directory to export the files"
  <> showDefault
  <> value "."
  <> metavar "PATH" )

-- Kmonad
kmonadFlags :: a -> Parser [a]
kmonadFlags a = flag' [a] (long "kmonad" <> short 'K' <> help "Export Kmonad files")

exportKmonad :: FilePath -> MultiOsRawLayout -> IO ()
exportKmonad p l = do
  putDoc $ mkTitleT ["[INFO] Export Kmonad files of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateKmonad path l
  where path = p </> "kmonad"

-- Text
textFlags :: (TextFormat -> a) -> Parser [a]
textFlags f
  =   flag' [f (Console Mini)] (long "text" <> short 't' <> help "Export in text format (mini layout) to `stdout`.")
  <|> flag' [f (Console Big)]  (long "Text" <> short 'T' <> help "Export in text format (big layout) to `stdout`.")
  <|> flag' [f CommonMark]     (long "doc"  <> short 'd' <> help "Export documentation in Common Mark format.")

exportText :: FilePath -> TextFormat -> OsSpecific -> H.Geometry -> MultiOsRawLayout -> IO ()
exportText _ (Console s) os g l = generateConsole s os g l
exportText p CommonMark  os g l = do
  putDoc $ mkTitleT ["[INFO] Export documentation of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateCommonMarkFile path os g l
  where path = p </> "doc"


csvFlags :: (CSVFormat -> a) -> Parser [a]
csvFlags f = flag' [f CSVFormat] (long "csv" <> short 'c' <> help "Export in CSV format")

exportCsv :: FilePath -> CSVFormat -> OsSpecific -> MultiOsRawLayout -> IO ()
exportCsv p f os l = do
  putDoc $ mkTitleT ["[INFO] Export CSV files of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateCsv f path os l
  where path = p </> "csv" </> mkOsPath os

jsonFlags :: a -> Parser [a]
jsonFlags a = flag' [a] (long "json" <> short 'j' <> help "Export in JSON format")

exportJson :: FilePath -> MultiOsRawLayout -> IO ()
exportJson p l = do
  putDoc $ mkTitleT ["[INFO] Export JSON file of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateJsonFile path l
  where path = p </> "json"

yamlFlags :: a -> Parser [a]
yamlFlags a = flag' [a] (long "yaml" <> short 'y' <> help "Export in YAML format")

exportYaml :: FilePath -> MultiOsRawLayout -> IO ()
exportYaml p l = do
  putDoc $ mkTitleT ["[INFO] Export YAML file of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateYamlFile path l
  where path = p </> "yaml"


-- Linux
mkLinuxFlags :: (LinuxFormat -> a) -> Parser [a]
mkLinuxFlags f
  =   flag' [f XKB] (long "xkb" <> short 'x' <> help "Export XKB files")
  <|> flag' (mkLinuxFormats f) (long "linux" <> short 'L' <> help "Export all Linux formats")

mkLinuxFormats :: (LinuxFormat -> a) -> [a]
mkLinuxFormats = (<$> enumFrom minBound)

exportXkb :: FilePath -> MultiOsRawLayout -> IO ()
exportXkb p l = do
  putDoc $ mkTitleT ["[INFO] Export XKB files of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateXkb path l
  where path = p </> mkOsPath linux </> "xkb"


-- Windows
windowsFlags :: (WindowsFormat -> a) -> Parser [a]
windowsFlags f
  =   flag' [f WDK] (long "wdk" <> short 'w' <> help "Export WDK files")
  <|> flag' [f (MSKLC KlcWithoutC)] (long "klc" <> short 'k' <> help "Export MSKLC files without C source files")
  <|> flag' [f (MSKLC KlcWithC)] (long "klc-c" <> help "Export MSKLC files with C source files")
  <|> flag' (mkWindowsFormats f) (long "windows" <> short 'W' <> help "Export all Windows formats")

mkWindowsFormats :: (WindowsFormat -> a) -> [a]
mkWindowsFormats = (<$> enumFrom minBound)

exportWdk :: FilePath -> MultiOsRawLayout -> IO ()
exportWdk p l = do
  putDoc $ mkTitleT ["[INFO] Export WDK files of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateWdk path l
  where path = p </> mkOsPath windows </> "wdk"

exportMsklc :: FilePath -> MsKlcC -> MultiOsRawLayout -> IO ()
exportMsklc p f l = do
  putDoc $ mkTitleT ["[INFO] Export MSKLC files of layout “", mkLayoutName l, "” to: ", TL.pack path] <> line
  generateMsklc path l f
  where path = p </> mkOsPath windows </> "msklc"


--- Utils ---------------------------------------------------------------------

mkOsPath :: OsSpecific -> FilePath
mkOsPath OsIndependent = mempty
mkOsPath (OsSpecific os) = toLower <$> show os

mkTitleT :: [TL.Text] -> Doc AnsiStyle
mkTitleT = mkTitle . mconcat
