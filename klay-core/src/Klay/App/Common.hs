{-# LANGUAGE OverloadedLists #-}

module Klay.App.Common
  ( typingMethodOption
  , layoutOption
  , osOption
  , osOption'
  , geometryOption
  , accessibilityOption
  , fingerLoadRangeOption
  , fingerBalanceOption
  , consecutiveUseSameFingerOption
  , bigStepsOption
  , hintDirectionOption
  , trigramsOption
  , mkTitle
  , mkError
  , mkStrong
  , mkEmphasis
  , mkField
  , mkPercentage
  ) where


import Prelude hiding (lookup)
import Numeric.Natural
import Data.List.NonEmpty qualified as NE
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Foldable (foldl')
import Control.Monad ((>=>))

import Options.Applicative
import Options.Applicative.Help.Pretty qualified as O

import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal

import TextShow.Data.Floating (showbFFloat)

import Klay.Utils.UserInput
import Klay.Keyboard.Layout
import Klay.Typing.Method
import Klay.Keyboard.Hardware.Geometry (Geometry)
import Klay.OS (OsSpecific(..), OS(..))
import Klay.Utils (lookup)


typingMethodOption :: NE.NonEmpty TypingMethod -> Parser TypingMethod
typingMethodOption ms = option (maybeReader (auto' >=> get_method_by_index))
  $  long "typing-method"
  <> short 'm'
  <> metavar "METHOD"
  <> helpDoc (Just helpContent)
  where
    ms' = NE.toList ms
    helpContent = O.vcat
      $ O.text "Use typing method METHOD."
      : O.text "Possible values:"
      : fmap mkMethodLabel (zip [1..] ms')
    mkMethodLabel :: (Natural, TypingMethod) -> O.Doc
    mkMethodLabel (idx, m) = O.text $ mconcat ["  ", show idx, ". ", T.unpack . methodName $ m]
    get_method_by_index :: Natural -> Maybe TypingMethod
    get_method_by_index 0   = Nothing
    get_method_by_index idx = lookup ms' . pred $ idx
    auto' arg = case reads arg of
      [(r, "")] -> Just r
      _         -> Nothing

layoutOption :: String -> Char -> String -> [MultiOsRawLayout] -> Parser MultiOsRawLayout
layoutOption l s t ls = option (maybeReader (auto' >=> get_layout_by_index))
  $  long l
  <> short s
  <> metavar "LAYOUT"
  <> helpDoc (Just helpContent)
  where
    helpContent = O.vcat
      $ O.text t
      : O.text "Possible values:"
      : (reverse . snd $ foldl' mkMethodLabel (1, mempty) ls)
    mkMethodLabel :: (Natural, [O.Doc]) -> MultiOsRawLayout -> (Natural, [O.Doc])
    mkMethodLabel (idx, doc) layout = (idx + 1, (:doc) . O.text . mconcat $ ["  ", show idx, ". ", TL.unpack . escapeLine . _name . _metadata $ layout])
    get_layout_by_index :: Natural -> Maybe MultiOsRawLayout
    get_layout_by_index 0   = Nothing
    get_layout_by_index idx = lookup ls (idx - 1)
    auto' arg = case reads arg of
      [(r, "")] -> Just r
      _         -> Nothing

osOption :: Parser OsSpecific
osOption = option (OsSpecific <$> auto)
   ( long "os"
  -- <> short 'o'
  <> value OsIndependent
  <> showDefault
  <> metavar "OS"
  <> helpDoc (Just helpContent) )
  where
    helpContent = O.vcat
      $ O.text "Target OS. Possible values:"
      : O.punctuate ", " (O.indent 2 . O.text . show <$> (enumFromTo minBound maxBound :: [OS]))

osOption' :: Parser OsSpecific
osOption' = option ((OsSpecific <$> auto) <|> auto')
   ( long "os"
  -- <> short 'o'
  <> metavar "OS"
  <> helpDoc (Just helpContent) )
  where
    os_indepedent = "none"
    auto' = maybeReader \s -> if s == os_indepedent
      then Just OsIndependent
      else Nothing
    helpContent = O.vcat
      $ O.text "Target OS. Possible values:"
      : O.punctuate ", " (O.indent 2 . O.text <$> os_indepedent : fmap show (enumFromTo minBound maxBound :: [OS]))

geometryOption :: String -> Parser Geometry
geometryOption t = option auto
   ( long "geometry"
  <> short 'g'
  <> metavar "GEOMETRY"
  <> helpDoc (Just helpContent) )
  where
    helpContent = O.vcat
      $ O.text t
      : O.text "Possible values:"
      : (O.punctuate ", " . fmap (O.indent 2 . O.text . show) $ (enumFrom minBound :: [Geometry]))

accessibilityOption :: Parser Double
accessibilityOption = option auto
    ( short 'a'
   <> help "Accessibility weight"
   <> showDefault
   <> value 0.45 )

fingerLoadRangeOption :: Parser Double
fingerLoadRangeOption = option auto
      ( short 'f'
     <> help "Max finger load weight"
     <> showDefault
     <> value 1 )

fingerBalanceOption :: Parser Double
fingerBalanceOption = option auto
     ( short 'b'
    <> help "Finger balance weight"
    <> showDefault
    <> value 0 )

consecutiveUseSameFingerOption :: Parser Double
consecutiveUseSameFingerOption = option auto
      ( short 'c'
     <> help "Consecutive use of the same finger weight"
     <> showDefault
     <> value 0.8 )

bigStepsOption :: Parser Double
bigStepsOption = option auto
      ( short 'B'
     <> help "Big steps weight"
     <> showDefault
     <> value 0.7 )

hintDirectionOption :: Parser Double
hintDirectionOption = option auto
      ( short 'd'
     <> help "Direction hint weight"
     <> showDefault
     <> value 0.6 )

trigramsOption :: Parser Double
trigramsOption = option auto
      ( short 't'
     <> help "Trigrams weight"
     <> showDefault
     <> value 1 )


-- Formatting ------------------------------------------------------------------

mkTitle :: TL.Text -> Doc AnsiStyle
mkTitle = annotate (color Magenta <> bold) . pretty

mkError :: TL.Text -> Doc AnsiStyle
mkError = annotate (color Red) . pretty

mkStrong :: TL.Text -> Doc AnsiStyle
mkStrong = annotate bold . pretty

mkEmphasis :: TL.Text -> Doc AnsiStyle
mkEmphasis = annotate bold . pretty

mkField :: TL.Text -> Doc AnsiStyle
mkField = annotate (color Green <> bold) . pretty

mkPercentage :: (RealFloat a) => a -> Doc AnsiStyle
mkPercentage = pretty . TB.toLazyText . (<> "%") . showbFFloat (Just 2) . (*100)
