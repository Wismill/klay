{-# LANGUAGE OverloadedLists      #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}

module Klay.Utils
  ( -- * Map
    (⇢)
  , foldlMWithKey'
  , foldrMWithKey'
    -- * List
  , deleteAt
  , replaceAt
  , lookup
  , chunksOf
  , chunksOfNE
  , normalise, normalise2
  , subsequencesOfSize
    -- * Bits
  , ToBitMask(..)
  , Combo(..)
    -- * JSON
  , jsonOptions
  , toJsonOptionsNewtype
  , jsonKeyOptions
  ) where


import Prelude hiding (lookup)
import Numeric.Natural (Natural)
import Data.List (unfoldr)
import Data.List.NonEmpty qualified as NE
import Data.Bits
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Control.DeepSeq
import GHC.Exts (IsList(..))
import GHC.Generics

import Data.Aeson qualified as Aeson

--- Map -----------------------------------------------------------------------

(⇢) :: a -> b -> (a, b)
a ⇢ b = (a, b)
infixr 1 ⇢

-- | Missing function from @Map.Strict@
foldlMWithKey' :: (Monad m) => (b -> k -> a -> m b) -> b -> Map.Map k a -> m b
foldlMWithKey' f b = Map.foldlWithKey' go (pure b)
  where go b' k a = b' >>= \b'' -> f b'' k a

-- | Missing function from @Map.Strict@
foldrMWithKey' :: (Monad m) => (k -> a -> b -> m b) -> b -> Map.Map k a -> m b
foldrMWithKey' f b = Map.foldrWithKey' go (pure b)
  where go k a b' = b' >>= \b'' -> f k a b''

normalise :: Map.Map k Double -> Map.Map k Double
normalise as = fmap (/ total) as
  where total = sum as

normalise2 :: Real v => Map.Map k1 (Map.Map k2 v) -> Map.Map k1 (Map.Map k2 Double)
normalise2 as = fmap (fmap (fromRational . (/ total) . toRational)) as
  where total = sum  $ fmap (sum . fmap toRational) as

--- List ----------------------------------------------------------------------

-- | Delete the element of a list at a given index
deleteAt :: Natural -> [a] -> [a]
deleteAt idx xs = lft <> drop 1 rgt
  where (lft, rgt) = splitAt (fromIntegral idx) xs

-- | Safe access by index to an element of a list
lookup :: [a] -> Natural -> Maybe a
lookup []     _ = Nothing
lookup (x:_)  0 = Just x
lookup (_:xs) n = lookup xs (pred n)

replaceAt :: [a] -> Natural -> a -> [a]
replaceAt []     _ _ = []
replaceAt (_:xs) 0 x = x:xs
replaceAt xs     n x = take (fromIntegral (n - 1)) xs <> (x : drop (fromIntegral n) xs)

chunksOf :: Int -> [a] -> [[a]]
chunksOf n = unfoldr \case
  [] -> Nothing
  l  -> Just (splitAt n l)

chunksOfNE :: Int -> NE.NonEmpty a -> NE.NonEmpty [a]
chunksOfNE n = NE.unfoldr \xs ->
  let (l, r) = NE.splitAt n xs
  in (l, NE.nonEmpty r)

-- Adapted from: https://stackoverflow.com/a/21288092
subsequencesOfSize :: Int -> [a] -> [[a]]
subsequencesOfSize n xs = let l = length xs
                          in if n > l then [] else subsequencesBySize xs !! (l - n)
  where
    subsequencesBySize []     = [[[]]]
    subsequencesBySize (y:ys) = let next = subsequencesBySize ys
                                in zipWith (<>) ([]:next) (fmap (fmap (y:)) next <> [[]])

--- Bits ----------------------------------------------------------------------

-- Source: https://stackoverflow.com/a/15911213
class ToBitMask a where
  toBitMask :: a -> Int
  -- | Using a DefaultSignatures extension to declare a default signature with
  -- an `Enum` constraint without affecting the constraints of the class itself.
  default toBitMask :: Enum a => a -> Int
  toBitMask = shiftL 1 . fromEnum

instance (Traversable t, ToBitMask a) => ToBitMask (t a) where
  toBitMask = foldr (.|.) 0 . fmap toBitMask

newtype Combo a = Combo{unCombo :: Set a}
  deriving stock (Show, Generic)
  deriving newtype (Eq, NFData)
  deriving (IsList) via (Set a)
  deriving (Foldable) via Set

instance (ToBitMask a) => ToBitMask (Combo a) where
  toBitMask = foldr (.|.) 0 . Set.map toBitMask . unCombo


--- JSON ----------------------------------------------------------------------

jsonOptions :: (String -> String) -> Aeson.Options
jsonOptions f = Aeson.defaultOptions
  { Aeson.fieldLabelModifier = f
  , Aeson.rejectUnknownFields = True
  , Aeson.sumEncoding = Aeson.ObjectWithSingleField
  }

toJsonOptionsNewtype :: Aeson.Options
toJsonOptionsNewtype = Aeson.defaultOptions
  { Aeson.rejectUnknownFields = True
  , Aeson.unwrapUnaryRecords = True
  }

jsonKeyOptions :: (String -> String) -> Aeson.JSONKeyOptions
jsonKeyOptions f = Aeson.defaultJSONKeyOptions
  { Aeson.keyModifier = f}
