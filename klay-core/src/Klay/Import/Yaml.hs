module Klay.Import.Yaml
  ( importYamlFile
  , importYaml
  , fromYamlLayout
  ) where

import Data.ByteString qualified as BS
import Data.Bifunctor (Bifunctor(..))

import Data.Yaml qualified as Yaml

import Klay.Keyboard.Layout qualified as L
import Klay.Keyboard.Layout.Action.DeadKey qualified as DK
import Klay.Keyboard.Layout.Group (imapGroups)
import Klay.Export.Yaml
import Klay.Export.Yaml.Group qualified as G

importYamlFile :: FilePath -> IO (Either String L.MultiOsRawLayout)
importYamlFile p = bimap Yaml.prettyPrintParseException fromYamlLayout
                 <$> Yaml.decodeFileEither p

importYaml :: BS.ByteString -> Either String L.MultiOsRawLayout
importYaml bs = bimap Yaml.prettyPrintParseException fromYamlLayout
               $ Yaml.decodeEither' bs

fromYamlLayout :: Layout -> L.MultiOsRawLayout
fromYamlLayout layout = L.Layout
  { L._metadata = _metadata layout
  , L._dualFunctionKeys = _dual_functions layout
  , L._options = _options layout
  , L._deadKeys
    = DK.mkDeadKeyDefinitions
    . unRawDeadKeyDefinitions
    . _dead_keys
    $ layout
  , L._groups = imapGroups G.fromYamlGroup (_groups layout)
  }
