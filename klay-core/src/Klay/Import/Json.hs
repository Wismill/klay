module Klay.Import.Json
  ( importJsonFile
  , importJson
  ) where

import Data.Aeson qualified as Aeson

import Data.ByteString.Lazy qualified as BS

import Klay.Keyboard.Layout
import Klay.Import.Yaml (fromYamlLayout)

importJsonFile :: FilePath -> IO (Either String MultiOsRawLayout)
importJsonFile = fmap importJson . BS.readFile

importJson :: BS.ByteString -> Either String MultiOsRawLayout
importJson = fmap fromYamlLayout . Aeson.eitherDecode'
