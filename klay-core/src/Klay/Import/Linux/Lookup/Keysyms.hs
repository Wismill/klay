-- [NOTE] Auto-generated module. Do not edit manually

module Klay.Import.Linux.Lookup.Keysyms
  ( keysyms
  , keysymsMapping
  , keysymToAction
  , keysymToSymbol
  , keysymToChar
  , actionsMapping
  , symbolsMapping
  , mActionToKeysym
  , mSymbolToKeysym
  ) where

import Prelude hiding (Left, Right)
import Data.Maybe (mapMaybe)
import Data.Text qualified as T
import Data.HashMap.Strict qualified as HMap

import {-# SOURCE #-} Klay.Keyboard.Layout.Action.Action (Action(..))
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Compose qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.Currency qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Greek qualified as DK
import Klay.Keyboard.Layout.Action.Special (SpecialAction(..))
import Klay.Keyboard.Layout.Modifier qualified as M

mSymbolToKeysym :: Symbol -> Maybe T.Text
mSymbolToKeysym = (symbolsMapping HMap.!?)

symbolsMapping :: HMap.HashMap Symbol T.Text
symbolsMapping
  = HMap.fromList
  . mapMaybe go
  $ keysyms
  where
    go (_, keysym, action, True) = (,keysym) <$> case action of
      AChar    c  -> Just (SChar c)
      ADeadKey dk -> Just (SDeadKey dk)
      _           -> Nothing
    go _ = Nothing

keysymToSymbol :: T.Text -> Maybe Symbol
keysymToSymbol keysym = case HMap.lookup keysym keysymsMapping of
  Just (AChar    c ) -> Just (SChar c)
  Just (ADeadKey dk) -> Just (SDeadKey dk)
  _                  -> Nothing

keysymToChar :: T.Text -> Maybe Char
keysymToChar keysym = case HMap.lookup keysym keysymsMapping of
  Just (AChar c) -> Just c
  _              -> Nothing

mActionToKeysym :: Action -> Maybe T.Text
mActionToKeysym = (actionsMapping HMap.!?)

keysymToAction :: T.Text -> Maybe Action
keysymToAction = (keysymsMapping HMap.!?)

actionsMapping :: HMap.HashMap Action T.Text
actionsMapping = HMap.fromList . mapMaybe go $ keysyms
  where go (_, a, b, True) = Just (b, a)
        go _               = Nothing

keysymsMapping :: HMap.HashMap T.Text Action
keysymsMapping = HMap.fromList $ (\(_, a, b, _) -> (a, b)) <$> keysyms

todo :: DeadKey
todo = DeadKey "[TODO]" "�" '�'

{-| Keysyms raw mapping to 'Action's.

The fields are:

1. Keysym numeric identifier
2. Keysym string identifier
3. Corresponding 'Action'
4. Whether it will be used for encoding actions

Sources:

- [X11 protocol](https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html#keysym_encoding)
- @\/usr\/include\/X11\/keysymdef.h@
- [libxkbcommon](https://github.com/xkbcommon/libxkbcommon/blob/master/src/keysym-utf.c#L867)
-}
keysyms :: [(Int, T.Text, Action, Bool)]
keysyms =
  [ (0x00000000, "NoSymbol", UndefinedAction, True) -- Special KeySym
  , (0x00ffffff, "VoidSymbol", NoAction, True) -- Void symbol
  --
  -- TTY function keys
  --
  , (0x0000ff08, "BackSpace", ASpecial BackSpace, True) -- U+0008
  , (0x0000ff09, "Tab", ASpecial Tab, True) -- U+0009
  , (0x0000ff0a, "Linefeed", AChar '\x000a', True) -- U+000A
  , (0x0000ff0b, "Clear", AChar '\x000b', True) -- U+000B
  , (0x0000ff0d, "Return", ASpecial Return, True) -- U+000D
  , (0x0000ff13, "Pause", ASpecial Pause, True) -- Pause, hold
  , (0x0000ff14, "Scroll_Lock", ASpecial Scroll_Lock, True)
  , (0x0000ff15, "Sys_Req", ASpecial Sys_Req, True)
  , (0x0000ff1b, "Escape", ASpecial Escape, True) -- U+001B
  , (0x0000ffff, "Delete", ASpecial Delete, True) -- Delete, rubout
  --
  -- International & multi-key character composition
  --
  , (0x0000ff20, "Multi_key", ADeadKey DK.compose, True) -- Multi-key character compose
  , (0x0000ff37, "Codeinput", ASpecial Codeinput, True)
  , (0x0000ff3c, "SingleCandidate", ASpecial SingleCandidate, True)
  , (0x0000ff3d, "MultipleCandidate", ASpecial MultipleCandidate, True)
  , (0x0000ff3e, "PreviousCandidate", ASpecial PreviousCandidate, True)
  --
  -- Japanese keyboard support
  --
  , (0x0000ff21, "Kanji", ASpecial Kanji, True) -- Kanji, Kanji convert
  , (0x0000ff22, "Muhenkan", ASpecial Muhenkan, True) -- Cancel Conversion
  , (0x0000ff23, "Henkan_Mode", ASpecial Henkan, False) -- Start\/Stop Conversion
  , (0x0000ff23, "Henkan", ASpecial Henkan, True) -- Alias for Henkan_Mode
  , (0x0000ff24, "Romaji", ASpecial Romaji, True) -- to Romaji
  , (0x0000ff25, "Hiragana", ASpecial Hiragana, True) -- to Hiragana
  , (0x0000ff26, "Katakana", ASpecial Katakana, True) -- to Katakana
  , (0x0000ff27, "Hiragana_Katakana", ASpecial Hiragana_Katakana, True) -- Hiragana\/Katakana toggle
  , (0x0000ff28, "Zenkaku", ASpecial Zenkaku, True) -- to Zenkaku
  , (0x0000ff29, "Hankaku", ASpecial Hankaku, True) -- to Hankaku
  , (0x0000ff2a, "Zenkaku_Hankaku", ASpecial Zenkaku_Hankaku, True) -- Zenkaku\/Hankaku toggle
  , (0x0000ff2b, "Touroku", ASpecial Touroku, True) -- Add to Dictionary
  , (0x0000ff2c, "Massyo", ASpecial Massyo, True) -- Delete from Dictionary
  , (0x0000ff2d, "Kana_Lock", ASpecial Kana_Lock, True) -- Kana Lock
  , (0x0000ff2e, "Kana_Shift", ASpecial Kana_Shift, True) -- Kana Shift
  , (0x0000ff2f, "Eisu_Shift", ASpecial Eisu_Shift, True) -- Alphanumeric Shift
  , (0x0000ff30, "Eisu_toggle", ASpecial Eisu_Toggle, True) -- Alphanumeric toggle
  , (0x0000ff37, "Kanji_Bangou", ASpecial Codeinput, False) -- Alias for Codeinput
  , (0x0000ff3d, "Zen_Koho", ASpecial MultipleCandidate, False) -- Multiple\/All Candidate(s) (alias for MultipleCandidate)
  , (0x0000ff3e, "Mae_Koho", ASpecial PreviousCandidate, False) -- Previous Candidate (alias for PreviousCandidate)
  --
  -- Cursor control & motion
  --
  , (0x0000ff50, "Home", ASpecial Home, True)
  , (0x0000ff51, "Left", ASpecial Left, True) -- Move left, left arrow
  , (0x0000ff52, "Up", ASpecial Up, True) -- Move up, up arrow
  , (0x0000ff53, "Right", ASpecial Right, True) -- Move right, right arrow
  , (0x0000ff54, "Down", ASpecial Down, True) -- Move down, down arrow
  , (0x0000ff55, "Prior", ASpecial Prior, True) -- Prior, previous
  , (0x0000ff55, "Page_Up", ASpecial Prior, False) -- Alias for Prior
  , (0x0000ff56, "Next", ASpecial Next, True) -- Next
  , (0x0000ff56, "Page_Down", ASpecial Next, False) -- Alias for Next
  , (0x0000ff57, "End", ASpecial End, True) -- EOL
  , (0x0000ff58, "Begin", ASpecial Begin, True) -- BOL
  --
  -- Misc functions
  --
  , (0x0000ff60, "Select", ASpecial Select, True) -- Select, mark
  , (0x0000ff61, "Print", ASpecial Print, True)
  , (0x0000ff62, "Execute", ASpecial Execute, True) -- Execute, run, do
  , (0x0000ff63, "Insert", ASpecial Insert, True) -- Insert, insert here
  , (0x0000ff65, "Undo", ASpecial Undo, True)
  , (0x0000ff66, "Redo", ASpecial Redo, True) -- Redo, again
  , (0x0000ff67, "Menu", ASpecial Menu, True)
  , (0x0000ff68, "Find", ASpecial Find, True) -- Find, search
  , (0x0000ff69, "Cancel", ASpecial Cancel, True) -- Cancel, stop, abort, exit
  , (0x0000ff6a, "Help", ASpecial Help, True) -- Help
  , (0x0000ff6b, "Break", ASpecial Break, True)
  , (0x0000ff7e, "Mode_switch", ASpecial Iso_Group_Shift, False) -- Character set switch
  , (0x0000ff7e, "script_switch", ASpecial Iso_Group_Shift, False) -- Alias for mode_switch
  , (0x0000ff7f, "Num_Lock", AModifier M.NumericLock, True)
  --
  -- Keypad functions, keypad numbers
  --
  , (0x0000ff80, "KP_Space", ASpecial KP_Space, True) -- U+0020 SPACE
  , (0x0000ff89, "KP_Tab", ASpecial KP_Tab, True) -- U+0009
  , (0x0000ff8d, "KP_Enter", ASpecial KP_Enter, True) -- U+000D
  , (0x0000ff91, "KP_F1", ASpecial KP_F1, True) -- PF1, KP_A, ...
  , (0x0000ff92, "KP_F2", ASpecial KP_F2, True)
  , (0x0000ff93, "KP_F3", ASpecial KP_F3, True)
  , (0x0000ff94, "KP_F4", ASpecial KP_F4, True)
  , (0x0000ff95, "KP_Home", ASpecial KP_Home, True)
  , (0x0000ff96, "KP_Left", ASpecial KP_Left, True)
  , (0x0000ff97, "KP_Up", ASpecial KP_Up, True)
  , (0x0000ff98, "KP_Right", ASpecial KP_Right, True)
  , (0x0000ff99, "KP_Down", ASpecial KP_Down, True)
  , (0x0000ff9a, "KP_Prior", ASpecial KP_Prior, True)
  , (0x0000ff9a, "KP_Page_Up", ASpecial KP_Prior, False) -- Alias for KP_Prior
  , (0x0000ff9b, "KP_Next", ASpecial KP_Next, True)
  , (0x0000ff9b, "KP_Page_Down", ASpecial KP_Next, False) -- Alias for KP_Next
  , (0x0000ff9c, "KP_End", ASpecial KP_End, True)
  , (0x0000ff9d, "KP_Begin", ASpecial KP_Begin, True)
  , (0x0000ff9e, "KP_Insert", ASpecial KP_Insert, True)
  , (0x0000ff9f, "KP_Delete", ASpecial KP_Delete, True)
  , (0x0000ffbd, "KP_Equal", ASpecial KP_Equal, True) -- U+003D EQUALS SIGN
  , (0x0000ffaa, "KP_Multiply", ASpecial KP_Multiply, True) -- U+002A ASTERISK
  , (0x0000ffab, "KP_Add", ASpecial KP_Add, True) -- U+002B PLUS SIGN
  , (0x0000ffac, "KP_Separator", ASpecial KP_Separator, True) -- U+002C COMMA
  , (0x0000ffad, "KP_Subtract", ASpecial KP_Subtract, True) -- U+002D HYPHEN-MINUS
  , (0x0000ffae, "KP_Decimal", ASpecial KP_Decimal, True) -- U+002E FULL STOP
  , (0x0000ffaf, "KP_Divide", ASpecial KP_Divide, True) -- U+002F SOLIDUS
  , (0x0000ffb0, "KP_0", ASpecial KP_0, True) -- U+0030 DIGIT ZERO
  , (0x0000ffb1, "KP_1", ASpecial KP_1, True) -- U+0031 DIGIT ONE
  , (0x0000ffb2, "KP_2", ASpecial KP_2, True) -- U+0032 DIGIT TWO
  , (0x0000ffb3, "KP_3", ASpecial KP_3, True) -- U+0033 DIGIT THREE
  , (0x0000ffb4, "KP_4", ASpecial KP_4, True) -- U+0034 DIGIT FOUR
  , (0x0000ffb5, "KP_5", ASpecial KP_5, True) -- U+0035 DIGIT FIVE
  , (0x0000ffb6, "KP_6", ASpecial KP_6, True) -- U+0036 DIGIT SIX
  , (0x0000ffb7, "KP_7", ASpecial KP_7, True) -- U+0037 DIGIT SEVEN
  , (0x0000ffb8, "KP_8", ASpecial KP_8, True) -- U+0038 DIGIT EIGHT
  , (0x0000ffb9, "KP_9", ASpecial KP_9, True) -- U+0039 DIGIT NINE
  --
  -- Auxiliary functions
  --
  , (0x0000ffbe, "F1", ASpecial F1, True)
  , (0x0000ffbf, "F2", ASpecial F2, True)
  , (0x0000ffc0, "F3", ASpecial F3, True)
  , (0x0000ffc1, "F4", ASpecial F4, True)
  , (0x0000ffc2, "F5", ASpecial F5, True)
  , (0x0000ffc3, "F6", ASpecial F6, True)
  , (0x0000ffc4, "F7", ASpecial F7, True)
  , (0x0000ffc5, "F8", ASpecial F8, True)
  , (0x0000ffc6, "F9", ASpecial F9, True)
  , (0x0000ffc7, "F10", ASpecial F10, True)
  , (0x0000ffc8, "F11", ASpecial F11, True)
  , (0x0000ffc8, "L1", ASpecial F11, False) -- Alias for F11
  , (0x0000ffc9, "F12", ASpecial F12, True)
  , (0x0000ffc9, "L2", ASpecial F12, False) -- Alias for F12
  , (0x0000ffca, "F13", ASpecial F13, True)
  , (0x0000ffca, "L3", ASpecial F13, False) -- Alias for F13
  , (0x0000ffcb, "F14", ASpecial F14, True)
  , (0x0000ffcb, "L4", ASpecial F14, False) -- Alias for F14
  , (0x0000ffcc, "F15", ASpecial F15, True)
  , (0x0000ffcc, "L5", ASpecial F15, False) -- Alias for F15
  , (0x0000ffcd, "F16", ASpecial F16, True)
  , (0x0000ffcd, "L6", ASpecial F16, False) -- Alias for F16
  , (0x0000ffce, "F17", ASpecial F17, True)
  , (0x0000ffce, "L7", ASpecial F17, False) -- Alias for F17
  , (0x0000ffcf, "F18", ASpecial F18, True)
  , (0x0000ffcf, "L8", ASpecial F18, False) -- Alias for F18
  , (0x0000ffd0, "F19", ASpecial F19, True)
  , (0x0000ffd0, "L9", ASpecial F19, False) -- Alias for F19
  , (0x0000ffd1, "F20", ASpecial F20, True)
  , (0x0000ffd1, "L10", ASpecial F20, False) -- Alias for F20
  , (0x0000ffd2, "F21", ASpecial F21, True)
  , (0x0000ffd2, "R1", ASpecial F21, False) -- Alias for F21
  , (0x0000ffd3, "F22", ASpecial F22, True)
  , (0x0000ffd3, "R2", ASpecial F22, False) -- Alias for F22
  , (0x0000ffd4, "F23", ASpecial F23, True)
  , (0x0000ffd4, "R3", ASpecial F23, False) -- Alias for F23
  , (0x0000ffd5, "F24", ASpecial F24, True)
  , (0x0000ffd5, "R4", ASpecial F24, False) -- Alias for F24
  , (0x0000ffd6, "F25", ASpecial F25, True)
  , (0x0000ffd6, "R5", ASpecial F25, False) -- Alias for F25
  , (0x0000ffd7, "F26", ASpecial F26, True)
  , (0x0000ffd7, "R6", ASpecial F26, False) -- Alias for F26
  , (0x0000ffd8, "F27", ASpecial F27, True)
  , (0x0000ffd8, "R7", ASpecial F27, False) -- Alias for F27
  , (0x0000ffd9, "F28", ASpecial F28, True)
  , (0x0000ffd9, "R8", ASpecial F28, False) -- Alias for F28
  , (0x0000ffda, "F29", ASpecial F29, True)
  , (0x0000ffda, "R9", ASpecial F29, False) -- Alias for F29
  , (0x0000ffdb, "F30", ASpecial F30, True)
  , (0x0000ffdb, "R10", ASpecial F30, False) -- Alias for F30
  , (0x0000ffdc, "F31", ASpecial F31, True)
  , (0x0000ffdc, "R11", ASpecial F31, False) -- Alias for F31
  , (0x0000ffdd, "F32", ASpecial F32, True)
  , (0x0000ffdd, "R12", ASpecial F32, False) -- Alias for F32
  , (0x0000ffde, "F33", ASpecial F33, True)
  , (0x0000ffde, "R13", ASpecial F33, False) -- Alias for F33
  , (0x0000ffdf, "F34", ASpecial F34, True)
  , (0x0000ffdf, "R14", ASpecial F34, False) -- Alias for F34
  , (0x0000ffe0, "F35", ASpecial F35, True)
  , (0x0000ffe0, "R15", ASpecial F35, False) -- Alias for F35
  --
  -- Modifiers
  --
  , (0x0000ffe1, "Shift_L", AModifier M.LShift, True) -- Left shift
  , (0x0000ffe2, "Shift_R", AModifier M.RShift, True) -- Right shift
  , (0x0000ffe3, "Control_L", AModifier M.LControl, True) -- Left control
  , (0x0000ffe4, "Control_R", AModifier M.RControl, True) -- Right control
  , (0x0000ffe5, "Caps_Lock", AModifier M.CapsLock, True) -- Caps lock
  , (0x0000ffe6, "Shift_Lock", AModifier M.ShiftLock, True) -- Shift lock
  , (0x0000ffe7, "Meta_L", ASpecial Meta_L, True) -- Left meta
  , (0x0000ffe8, "Meta_R", ASpecial Meta_R, True) -- Right meta
  , (0x0000ffe9, "Alt_L", AModifier M.LAlternate, True) -- Left alt
  , (0x0000ffea, "Alt_R", AModifier M.RAlternate, True) -- Right alt
  , (0x0000ffeb, "Super_L", AModifier M.LSuper, True) -- Left super
  , (0x0000ffec, "Super_R", AModifier M.RSuper, True) -- Right super
  , (0x0000ffed, "Hyper_L", ASpecial Hyper_L, True) -- Left hyper
  , (0x0000ffee, "Hyper_R", ASpecial Hyper_R, True) -- Right hyper
  --
  -- Keyboard (XKB) Extension function and modifier keys
  --
  , (0x0000fe01, "ISO_Lock", AModifier M.CapsSet, True)
  , (0x0000fe02, "ISO_Level2_Latch", AModifier M.ShiftLatch, True)
  , (0x0000fe03, "ISO_Level3_Shift", AModifier M.IsoLevel3Set, True)
  , (0x0000fe04, "ISO_Level3_Latch", AModifier M.IsoLevel3Latch, True)
  , (0x0000fe05, "ISO_Level3_Lock", AModifier M.IsoLevel3Lock, True)
  , (0x0000fe11, "ISO_Level5_Shift", AModifier M.IsoLevel5Set, True)
  , (0x0000fe12, "ISO_Level5_Latch", AModifier M.IsoLevel5Latch, True)
  , (0x0000fe13, "ISO_Level5_Lock", AModifier M.IsoLevel5Lock, True)
  , (0x0000ff7e, "ISO_Group_Shift", ASpecial Iso_Group_Shift, True) -- Alias for mode_switch
  , (0x0000fe06, "ISO_Group_Latch", ASpecial Iso_Group_Latch, True)
  , (0x0000fe07, "ISO_Group_Lock", ASpecial Iso_Group_Lock, True)
  , (0x0000fe08, "ISO_Next_Group", ASpecial Iso_Next_Group, True)
  , (0x0000fe09, "ISO_Next_Group_Lock", ASpecial Iso_Next_Group_Lock, True)
  , (0x0000fe0a, "ISO_Prev_Group", ASpecial Iso_Prev_Group, True)
  , (0x0000fe0b, "ISO_Prev_Group_Lock", ASpecial Iso_Prev_Group_Lock, True)
  , (0x0000fe0c, "ISO_First_Group", ASpecial Iso_First_Group, True)
  , (0x0000fe0d, "ISO_First_Group_Lock", ASpecial Iso_First_Group_Lock, True)
  , (0x0000fe0e, "ISO_Last_Group", ASpecial Iso_Last_Group, True)
  , (0x0000fe0f, "ISO_Last_Group_Lock", ASpecial Iso_Last_Group_Lock, True)
  , (0x0000fe20, "ISO_Left_Tab", ASpecial Iso_Left_Tab, True)
  , (0x0000fe21, "ISO_Move_Line_Up", ASpecial Iso_Move_Line_Up, True)
  , (0x0000fe22, "ISO_Move_Line_Down", ASpecial Iso_Move_Line_Down, True)
  , (0x0000fe23, "ISO_Partial_Line_Up", ASpecial Iso_Partial_Line_Up, True)
  , (0x0000fe24, "ISO_Partial_Line_Down", ASpecial Iso_Partial_Line_Down, True)
  , (0x0000fe25, "ISO_Partial_Space_Left", ASpecial Iso_Partial_Space_Left, True)
  , (0x0000fe26, "ISO_Partial_Space_Right", ASpecial Iso_Partial_Space_Right, True)
  , (0x0000fe27, "ISO_Set_Margin_Left", ASpecial Iso_Set_Margin_Left, True)
  , (0x0000fe28, "ISO_Set_Margin_Right", ASpecial Iso_Set_Margin_Right, True)
  , (0x0000fe29, "ISO_Release_Margin_Left", ASpecial Iso_Release_Margin_Left, True)
  , (0x0000fe2a, "ISO_Release_Margin_Right", ASpecial Iso_Release_Margin_Right, True)
  , (0x0000fe2b, "ISO_Release_Both_Margins", ASpecial Iso_Release_Both_Margins, True)
  , (0x0000fe2c, "ISO_Fast_Cursor_Left", ASpecial Iso_Fast_Cursor_Left, True)
  , (0x0000fe2d, "ISO_Fast_Cursor_Right", ASpecial Iso_Fast_Cursor_Right, True)
  , (0x0000fe2e, "ISO_Fast_Cursor_Up", ASpecial Iso_Fast_Cursor_Up, True)
  , (0x0000fe2f, "ISO_Fast_Cursor_Down", ASpecial Iso_Fast_Cursor_Down, True)
  , (0x0000fe30, "ISO_Continuous_Underline", ASpecial Iso_Continuous_Underline, True)
  , (0x0000fe31, "ISO_Discontinuous_Underline", ASpecial Iso_Discontinuous_Underline, True)
  , (0x0000fe32, "ISO_Emphasize", ASpecial Iso_Emphasize, True)
  , (0x0000fe33, "ISO_Center_Object", ASpecial Iso_Center_Object, True)
  , (0x0000fe34, "ISO_Enter", ASpecial Iso_Enter, True)
  --
  -- Dead keys
  --
  , (0x0000fe50, "dead_grave", ADeadKey DK.graveAbove, True)
  , (0x0000fe51, "dead_acute", ADeadKey DK.acuteAbove, True)
  , (0x0000fe52, "dead_circumflex", ADeadKey DK.circumflexAbove, True)
  , (0x0000fe53, "dead_tilde", ADeadKey DK.tildeAbove, True)
  , (0x0000fe53, "dead_perispomeni", ADeadKey DK.tildeAbove, False) -- alias for dead_tilde
  , (0x0000fe54, "dead_macron", ADeadKey DK.macronAbove, True)
  , (0x0000fe55, "dead_breve", ADeadKey DK.breveAbove, True)
  , (0x0000fe56, "dead_abovedot", ADeadKey DK.dotAbove, True)
  , (0x0000fe57, "dead_diaeresis", ADeadKey DK.diaeresisAbove, True)
  , (0x0000fe58, "dead_abovering", ADeadKey DK.ringAbove, True)
  , (0x0000fe59, "dead_doubleacute", ADeadKey DK.doubledAcute, True)
  , (0x0000fe5a, "dead_caron", ADeadKey DK.caronAbove, True)
  , (0x0000fe5b, "dead_cedilla", ADeadKey DK.cedillaBelow, True)
  , (0x0000fe5c, "dead_ogonek", ADeadKey DK.ogonek, True)
  , (0x0000fe5d, "dead_iota", ADeadKey todo, True)
  , (0x0000fe5e, "dead_voiced_sound", ADeadKey todo, True)
  , (0x0000fe5f, "dead_semivoiced_sound", ADeadKey todo, True)
  , (0x0000fe60, "dead_belowdot", ADeadKey DK.dotBelow, True)
  , (0x0000fe61, "dead_hook", ADeadKey todo, True)
  , (0x0000fe62, "dead_horn", ADeadKey DK.horn, True)
  , (0x0000fe63, "dead_stroke", ADeadKey DK.longStroke, True)
  , (0x0000fe64, "dead_abovecomma", ADeadKey DK.commaAbove, True)
  , (0x0000fe64, "dead_psili", ADeadKey DK.commaAbove, False) -- alias for dead_abovecomma
  , (0x0000fe65, "dead_abovereversedcomma", ADeadKey DK.reversedCommaAbove, True)
  , (0x0000fe65, "dead_dasia", ADeadKey DK.reversedCommaAbove, False) -- alias for dead_abovereversedcomma
  , (0x0000fe66, "dead_doublegrave", ADeadKey DK.doubledGrave, True)
  , (0x0000fe67, "dead_belowring", ADeadKey DK.ringBelow, True)
  , (0x0000fe68, "dead_belowmacron", ADeadKey DK.macronBelow, True)
  , (0x0000fe69, "dead_belowcircumflex", ADeadKey DK.circumflexBelow, True)
  , (0x0000fe6a, "dead_belowtilde", ADeadKey DK.tildeBelow, True)
  , (0x0000fe6b, "dead_belowbreve", ADeadKey DK.breveBelow, True)
  , (0x0000fe6c, "dead_belowdiaeresis", ADeadKey DK.diaeresisBelow, True)
  , (0x0000fe6d, "dead_invertedbreve", ADeadKey DK.invertedBreve, True)
  , (0x0000fe6e, "dead_belowcomma", ADeadKey DK.commaBelow, True)
  , (0x0000fe6f, "dead_currency", ADeadKey DK.bépoCurrency, True)
  --
  -- Extra dead elements for German T3 layout
  --
  , (0x0000fe90, "dead_lowline", ADeadKey DK.lowLine, True)
  , (0x0000fe91, "dead_aboveverticalline", ADeadKey DK.verticalLineAbove, True)
  , (0x0000fe92, "dead_belowverticalline", ADeadKey DK.verticalLineBelow, True)
  , (0x0000fe93, "dead_longsolidusoverlay", ADeadKey DK.longSolidusOverlay, True)
  --
  -- Dead vowels for universal syllable entry
  --
  , (0x0000fe80, "dead_a", ADeadKey todo, True)
  , (0x0000fe81, "dead_A", ADeadKey todo, True)
  , (0x0000fe82, "dead_e", ADeadKey todo, True)
  , (0x0000fe83, "dead_E", ADeadKey todo, True)
  , (0x0000fe84, "dead_i", ADeadKey todo, True)
  , (0x0000fe85, "dead_I", ADeadKey todo, True)
  , (0x0000fe86, "dead_o", ADeadKey todo, True)
  , (0x0000fe87, "dead_O", ADeadKey todo, True)
  , (0x0000fe88, "dead_u", ADeadKey todo, True)
  , (0x0000fe89, "dead_U", ADeadKey todo, True)
  , (0x0000fe8a, "dead_small_schwa", ADeadKey todo, True)
  , (0x0000fe8b, "dead_capital_schwa", ADeadKey todo, True)
  , (0x0000fe8c, "dead_greek", ADeadKey DK.greek, True)
  --
  -- Miscelleanous X11
  --
  , (0x0000fed0, "First_Virtual_Screen", ASpecial First_Virtual_Screen, True)
  , (0x0000fed1, "Prev_Virtual_Screen", ASpecial Prev_Virtual_Screen, True)
  , (0x0000fed2, "Next_Virtual_Screen", ASpecial Next_Virtual_Screen, True)
  , (0x0000fed4, "Last_Virtual_Screen", ASpecial Last_Virtual_Screen, True)
  , (0x0000fed5, "Terminate_Server", ASpecial Terminate_Server, True)
  , (0x0000fe70, "AccessX_Enable", ASpecial Accessx_Enable, True)
  , (0x0000fe71, "AccessX_Feedback_Enable", ASpecial Accessx_Feedback_Enable, True)
  , (0x0000fe72, "RepeatKeys_Enable", ASpecial Repeatkeys_Enable, True)
  , (0x0000fe73, "SlowKeys_Enable", ASpecial Slowkeys_Enable, True)
  , (0x0000fe74, "BounceKeys_Enable", ASpecial Bouncekeys_Enable, True)
  , (0x0000fe75, "StickyKeys_Enable", ASpecial Stickykeys_Enable, True)
  , (0x0000fe76, "MouseKeys_Enable", ASpecial Mousekeys_Enable, True)
  , (0x0000fe77, "MouseKeys_Accel_Enable", ASpecial Mousekeys_Accel_Enable, True)
  , (0x0000fe78, "Overlay1_Enable", ASpecial Overlay1_Enable, True)
  , (0x0000fe79, "Overlay2_Enable", ASpecial Overlay2_Enable, True)
  , (0x0000fe7a, "AudibleBell_Enable", ASpecial Audiblebell_Enable, True)
  , (0x0000fee0, "Pointer_Left", ASpecial Pointer_Left, True)
  , (0x0000fee1, "Pointer_Right", ASpecial Pointer_Right, True)
  , (0x0000fee2, "Pointer_Up", ASpecial Pointer_Up, True)
  , (0x0000fee3, "Pointer_Down", ASpecial Pointer_Down, True)
  , (0x0000fee4, "Pointer_UpLeft", ASpecial Pointer_Upleft, True)
  , (0x0000fee5, "Pointer_UpRight", ASpecial Pointer_Upright, True)
  , (0x0000fee6, "Pointer_DownLeft", ASpecial Pointer_Downleft, True)
  , (0x0000fee7, "Pointer_DownRight", ASpecial Pointer_Downright, True)
  , (0x0000fee8, "Pointer_Button_Dflt", ASpecial Pointer_Button_Dflt, True)
  , (0x0000fee9, "Pointer_Button1", ASpecial Pointer_Button1, True)
  , (0x0000feea, "Pointer_Button2", ASpecial Pointer_Button2, True)
  , (0x0000feeb, "Pointer_Button3", ASpecial Pointer_Button3, True)
  , (0x0000feec, "Pointer_Button4", ASpecial Pointer_Button4, True)
  , (0x0000feed, "Pointer_Button5", ASpecial Pointer_Button5, True)
  , (0x0000feee, "Pointer_DblClick_Dflt", ASpecial Pointer_Dblclick_Dflt, True)
  , (0x0000feef, "Pointer_DblClick1", ASpecial Pointer_Dblclick1, True)
  , (0x0000fef0, "Pointer_DblClick2", ASpecial Pointer_Dblclick2, True)
  , (0x0000fef1, "Pointer_DblClick3", ASpecial Pointer_Dblclick3, True)
  , (0x0000fef2, "Pointer_DblClick4", ASpecial Pointer_Dblclick4, True)
  , (0x0000fef3, "Pointer_DblClick5", ASpecial Pointer_Dblclick5, True)
  , (0x0000fef4, "Pointer_Drag_Dflt", ASpecial Pointer_Drag_Dflt, True)
  , (0x0000fef5, "Pointer_Drag1", ASpecial Pointer_Drag1, True)
  , (0x0000fef6, "Pointer_Drag2", ASpecial Pointer_Drag2, True)
  , (0x0000fef7, "Pointer_Drag3", ASpecial Pointer_Drag3, True)
  , (0x0000fef8, "Pointer_Drag4", ASpecial Pointer_Drag4, True)
  , (0x0000fefd, "Pointer_Drag5", ASpecial Pointer_Drag5, True)
  , (0x0000fef9, "Pointer_EnableKeys", ASpecial Pointer_Enablekeys, True)
  , (0x0000fefa, "Pointer_Accelerate", ASpecial Pointer_Accelerate, True)
  , (0x0000fefb, "Pointer_DfltBtnNext", ASpecial Pointer_Dfltbtnnext, True)
  , (0x0000fefc, "Pointer_DfltBtnPrev", ASpecial Pointer_Dfltbtnprev, True)
  --
  -- Single-Stroke Multiple-Character N-Graph Keysyms For The X Input Method
  --
  , (0x0000fea0, "ch", ASpecial XXXch, True)
  , (0x0000fea1, "Ch", ASpecial Ch, True)
  , (0x0000fea2, "CH", ASpecial CH, True)
  , (0x0000fea3, "c_h", ASpecial XXXc_h, True)
  , (0x0000fea4, "C_h", ASpecial C_h, True)
  , (0x0000fea5, "C_H", ASpecial C_H, True)
  --
  -- 3270 Terminal Keys
  --
  , (0x0000fd01, "3270_Duplicate", ASpecial XXX3270_Duplicate, True)
  , (0x0000fd02, "3270_FieldMark", ASpecial XXX3270_FieldMark, True)
  , (0x0000fd03, "3270_Right2", ASpecial XXX3270_Right2, True)
  , (0x0000fd04, "3270_Left2", ASpecial XXX3270_Left2, True)
  , (0x0000fd05, "3270_BackTab", ASpecial XXX3270_BackTab, True)
  , (0x0000fd06, "3270_EraseEOF", ASpecial XXX3270_EraseEOF, True)
  , (0x0000fd07, "3270_EraseInput", ASpecial XXX3270_EraseInput, True)
  , (0x0000fd08, "3270_Reset", ASpecial XXX3270_Reset, True)
  , (0x0000fd09, "3270_Quit", ASpecial XXX3270_Quit, True)
  , (0x0000fd0a, "3270_PA1", ASpecial XXX3270_PA1, True)
  , (0x0000fd0b, "3270_PA2", ASpecial XXX3270_PA2, True)
  , (0x0000fd0c, "3270_PA3", ASpecial XXX3270_PA3, True)
  , (0x0000fd0d, "3270_Test", ASpecial XXX3270_Test, True)
  , (0x0000fd0e, "3270_Attn", ASpecial XXX3270_Attn, True)
  , (0x0000fd0f, "3270_CursorBlink", ASpecial XXX3270_CursorBlink, True)
  , (0x0000fd10, "3270_AltCursor", ASpecial XXX3270_AltCursor, True)
  , (0x0000fd11, "3270_KeyClick", ASpecial XXX3270_KeyClick, True)
  , (0x0000fd12, "3270_Jump", ASpecial XXX3270_Jump, True)
  , (0x0000fd13, "3270_Ident", ASpecial XXX3270_Ident, True)
  , (0x0000fd14, "3270_Rule", ASpecial XXX3270_Rule, True)
  , (0x0000fd15, "3270_Copy", ASpecial XXX3270_Copy, True)
  , (0x0000fd16, "3270_Play", ASpecial XXX3270_Play, True)
  , (0x0000fd17, "3270_Setup", ASpecial XXX3270_Setup, True)
  , (0x0000fd18, "3270_Record", ASpecial XXX3270_Record, True)
  , (0x0000fd19, "3270_ChangeScreen", ASpecial XXX3270_ChangeScreen, True)
  , (0x0000fd1a, "3270_DeleteWord", ASpecial XXX3270_DeleteWord, True)
  , (0x0000fd1b, "3270_ExSelect", ASpecial XXX3270_ExSelect, True)
  , (0x0000fd1c, "3270_CursorSelect", ASpecial XXX3270_CursorSelect, True)
  , (0x0000fd1d, "3270_PrintScreen", ASpecial XXX3270_PrintScreen, True)
  , (0x0000fd1e, "3270_Enter", ASpecial XXX3270_Enter, True)
  --
  -- Latin 1 (ISO/IEC 8859-1 = Unicode U+0020..U+00FF)
  --
  , (0x00000020, "space", AChar '\x0020', True) -- U+0020 SPACE
  , (0x00000021, "exclam", AChar '\x0021', True) -- U+0021 EXCLAMATION MARK
  , (0x00000022, "quotedbl", AChar '\x0022', True) -- U+0022 QUOTATION MARK
  , (0x00000023, "numbersign", AChar '\x0023', True) -- U+0023 NUMBER SIGN
  , (0x00000024, "dollar", AChar '\x0024', True) -- U+0024 DOLLAR SIGN
  , (0x00000025, "percent", AChar '\x0025', True) -- U+0025 PERCENT SIGN
  , (0x00000026, "ampersand", AChar '\x0026', True) -- U+0026 AMPERSAND
  , (0x00000027, "apostrophe", AChar '\x0027', True) -- U+0027 APOSTROPHE
  , (0x00000027, "quoteright", AChar '\x0027', False) -- U+0027 APOSTROPHE [NOTE] deprecated keysym
  , (0x00000028, "parenleft", AChar '\x0028', True) -- U+0028 LEFT PARENTHESIS
  , (0x00000029, "parenright", AChar '\x0029', True) -- U+0029 RIGHT PARENTHESIS
  , (0x0000002a, "asterisk", AChar '\x002a', True) -- U+002A ASTERISK
  , (0x0000002b, "plus", AChar '\x002b', True) -- U+002B PLUS SIGN
  , (0x0000002c, "comma", AChar '\x002c', True) -- U+002C COMMA
  , (0x0000002d, "minus", AChar '\x002d', True) -- U+002D HYPHEN-MINUS
  , (0x0000002e, "period", AChar '\x002e', True) -- U+002E FULL STOP
  , (0x0000002f, "slash", AChar '\x002f', True) -- U+002F SOLIDUS
  , (0x00000030, "0", AChar '\x0030', True) -- U+0030 DIGIT ZERO
  , (0x00000031, "1", AChar '\x0031', True) -- U+0031 DIGIT ONE
  , (0x00000032, "2", AChar '\x0032', True) -- U+0032 DIGIT TWO
  , (0x00000033, "3", AChar '\x0033', True) -- U+0033 DIGIT THREE
  , (0x00000034, "4", AChar '\x0034', True) -- U+0034 DIGIT FOUR
  , (0x00000035, "5", AChar '\x0035', True) -- U+0035 DIGIT FIVE
  , (0x00000036, "6", AChar '\x0036', True) -- U+0036 DIGIT SIX
  , (0x00000037, "7", AChar '\x0037', True) -- U+0037 DIGIT SEVEN
  , (0x00000038, "8", AChar '\x0038', True) -- U+0038 DIGIT EIGHT
  , (0x00000039, "9", AChar '\x0039', True) -- U+0039 DIGIT NINE
  , (0x0000003a, "colon", AChar '\x003a', True) -- U+003A COLON
  , (0x0000003b, "semicolon", AChar '\x003b', True) -- U+003B SEMICOLON
  , (0x0000003c, "less", AChar '\x003c', True) -- U+003C LESS-THAN SIGN
  , (0x0000003d, "equal", AChar '\x003d', True) -- U+003D EQUALS SIGN
  , (0x0000003e, "greater", AChar '\x003e', True) -- U+003E GREATER-THAN SIGN
  , (0x0000003f, "question", AChar '\x003f', True) -- U+003F QUESTION MARK
  , (0x00000040, "at", AChar '\x0040', True) -- U+0040 COMMERCIAL AT
  , (0x00000041, "A", AChar '\x0041', True) -- U+0041 LATIN CAPITAL LETTER A
  , (0x00000042, "B", AChar '\x0042', True) -- U+0042 LATIN CAPITAL LETTER B
  , (0x00000043, "C", AChar '\x0043', True) -- U+0043 LATIN CAPITAL LETTER C
  , (0x00000044, "D", AChar '\x0044', True) -- U+0044 LATIN CAPITAL LETTER D
  , (0x00000045, "E", AChar '\x0045', True) -- U+0045 LATIN CAPITAL LETTER E
  , (0x00000046, "F", AChar '\x0046', True) -- U+0046 LATIN CAPITAL LETTER F
  , (0x00000047, "G", AChar '\x0047', True) -- U+0047 LATIN CAPITAL LETTER G
  , (0x00000048, "H", AChar '\x0048', True) -- U+0048 LATIN CAPITAL LETTER H
  , (0x00000049, "I", AChar '\x0049', True) -- U+0049 LATIN CAPITAL LETTER I
  , (0x0000004a, "J", AChar '\x004a', True) -- U+004A LATIN CAPITAL LETTER J
  , (0x0000004b, "K", AChar '\x004b', True) -- U+004B LATIN CAPITAL LETTER K
  , (0x0000004c, "L", AChar '\x004c', True) -- U+004C LATIN CAPITAL LETTER L
  , (0x0000004d, "M", AChar '\x004d', True) -- U+004D LATIN CAPITAL LETTER M
  , (0x0000004e, "N", AChar '\x004e', True) -- U+004E LATIN CAPITAL LETTER N
  , (0x0000004f, "O", AChar '\x004f', True) -- U+004F LATIN CAPITAL LETTER O
  , (0x00000050, "P", AChar '\x0050', True) -- U+0050 LATIN CAPITAL LETTER P
  , (0x00000051, "Q", AChar '\x0051', True) -- U+0051 LATIN CAPITAL LETTER Q
  , (0x00000052, "R", AChar '\x0052', True) -- U+0052 LATIN CAPITAL LETTER R
  , (0x00000053, "S", AChar '\x0053', True) -- U+0053 LATIN CAPITAL LETTER S
  , (0x00000054, "T", AChar '\x0054', True) -- U+0054 LATIN CAPITAL LETTER T
  , (0x00000055, "U", AChar '\x0055', True) -- U+0055 LATIN CAPITAL LETTER U
  , (0x00000056, "V", AChar '\x0056', True) -- U+0056 LATIN CAPITAL LETTER V
  , (0x00000057, "W", AChar '\x0057', True) -- U+0057 LATIN CAPITAL LETTER W
  , (0x00000058, "X", AChar '\x0058', True) -- U+0058 LATIN CAPITAL LETTER X
  , (0x00000059, "Y", AChar '\x0059', True) -- U+0059 LATIN CAPITAL LETTER Y
  , (0x0000005a, "Z", AChar '\x005a', True) -- U+005A LATIN CAPITAL LETTER Z
  , (0x0000005b, "bracketleft", AChar '\x005b', True) -- U+005B LEFT SQUARE BRACKET
  , (0x0000005c, "backslash", AChar '\x005c', True) -- U+005C REVERSE SOLIDUS
  , (0x0000005d, "bracketright", AChar '\x005d', True) -- U+005D RIGHT SQUARE BRACKET
  , (0x0000005e, "asciicircum", AChar '\x005e', True) -- U+005E CIRCUMFLEX ACCENT
  , (0x0000005f, "underscore", AChar '\x005f', True) -- U+005F LOW LINE
  , (0x00000060, "grave", AChar '\x0060', True) -- U+0060 GRAVE ACCENT
  , (0x00000060, "quoteleft", AChar '\x0060', False) -- U+0060 GRAVE ACCENT [NOTE] deprecated keysym
  , (0x00000061, "a", AChar '\x0061', True) -- U+0061 LATIN SMALL LETTER A
  , (0x00000062, "b", AChar '\x0062', True) -- U+0062 LATIN SMALL LETTER B
  , (0x00000063, "c", AChar '\x0063', True) -- U+0063 LATIN SMALL LETTER C
  , (0x00000064, "d", AChar '\x0064', True) -- U+0064 LATIN SMALL LETTER D
  , (0x00000065, "e", AChar '\x0065', True) -- U+0065 LATIN SMALL LETTER E
  , (0x00000066, "f", AChar '\x0066', True) -- U+0066 LATIN SMALL LETTER F
  , (0x00000067, "g", AChar '\x0067', True) -- U+0067 LATIN SMALL LETTER G
  , (0x00000068, "h", AChar '\x0068', True) -- U+0068 LATIN SMALL LETTER H
  , (0x00000069, "i", AChar '\x0069', True) -- U+0069 LATIN SMALL LETTER I
  , (0x0000006a, "j", AChar '\x006a', True) -- U+006A LATIN SMALL LETTER J
  , (0x0000006b, "k", AChar '\x006b', True) -- U+006B LATIN SMALL LETTER K
  , (0x0000006c, "l", AChar '\x006c', True) -- U+006C LATIN SMALL LETTER L
  , (0x0000006d, "m", AChar '\x006d', True) -- U+006D LATIN SMALL LETTER M
  , (0x0000006e, "n", AChar '\x006e', True) -- U+006E LATIN SMALL LETTER N
  , (0x0000006f, "o", AChar '\x006f', True) -- U+006F LATIN SMALL LETTER O
  , (0x00000070, "p", AChar '\x0070', True) -- U+0070 LATIN SMALL LETTER P
  , (0x00000071, "q", AChar '\x0071', True) -- U+0071 LATIN SMALL LETTER Q
  , (0x00000072, "r", AChar '\x0072', True) -- U+0072 LATIN SMALL LETTER R
  , (0x00000073, "s", AChar '\x0073', True) -- U+0073 LATIN SMALL LETTER S
  , (0x00000074, "t", AChar '\x0074', True) -- U+0074 LATIN SMALL LETTER T
  , (0x00000075, "u", AChar '\x0075', True) -- U+0075 LATIN SMALL LETTER U
  , (0x00000076, "v", AChar '\x0076', True) -- U+0076 LATIN SMALL LETTER V
  , (0x00000077, "w", AChar '\x0077', True) -- U+0077 LATIN SMALL LETTER W
  , (0x00000078, "x", AChar '\x0078', True) -- U+0078 LATIN SMALL LETTER X
  , (0x00000079, "y", AChar '\x0079', True) -- U+0079 LATIN SMALL LETTER Y
  , (0x0000007a, "z", AChar '\x007a', True) -- U+007A LATIN SMALL LETTER Z
  , (0x0000007b, "braceleft", AChar '\x007b', True) -- U+007B LEFT CURLY BRACKET
  , (0x0000007c, "bar", AChar '\x007c', True) -- U+007C VERTICAL LINE
  , (0x0000007d, "braceright", AChar '\x007d', True) -- U+007D RIGHT CURLY BRACKET
  , (0x0000007e, "asciitilde", AChar '\x007e', True) -- U+007E TILDE
  , (0x000000a0, "nobreakspace", AChar '\x00a0', True) -- U+00A0 NO-BREAK SPACE
  , (0x000000a1, "exclamdown", AChar '\x00a1', True) -- U+00A1 INVERTED EXCLAMATION MARK
  , (0x000000a2, "cent", AChar '\x00a2', True) -- U+00A2 CENT SIGN
  , (0x000000a3, "sterling", AChar '\x00a3', True) -- U+00A3 POUND SIGN
  , (0x000000a4, "currency", AChar '\x00a4', True) -- U+00A4 CURRENCY SIGN
  , (0x000000a5, "yen", AChar '\x00a5', True) -- U+00A5 YEN SIGN
  , (0x000000a6, "brokenbar", AChar '\x00a6', True) -- U+00A6 BROKEN BAR
  , (0x000000a7, "section", AChar '\x00a7', True) -- U+00A7 SECTION SIGN
  , (0x000000a8, "diaeresis", AChar '\x00a8', True) -- U+00A8 DIAERESIS
  , (0x000000a9, "copyright", AChar '\x00a9', True) -- U+00A9 COPYRIGHT SIGN
  , (0x000000aa, "ordfeminine", AChar '\x00aa', True) -- U+00AA FEMININE ORDINAL INDICATOR
  , (0x000000ab, "guillemotleft", AChar '\x00ab', True) -- U+00AB LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
  , (0x000000ac, "notsign", AChar '\x00ac', True) -- U+00AC NOT SIGN
  , (0x000000ad, "hyphen", AChar '\x00ad', True) -- U+00AD SOFT HYPHEN
  , (0x000000ae, "registered", AChar '\x00ae', True) -- U+00AE REGISTERED SIGN
  , (0x000000af, "macron", AChar '\x00af', True) -- U+00AF MACRON
  , (0x000000b0, "degree", AChar '\x00b0', True) -- U+00B0 DEGREE SIGN
  , (0x000000b1, "plusminus", AChar '\x00b1', True) -- U+00B1 PLUS-MINUS SIGN
  , (0x000000b2, "twosuperior", AChar '\x00b2', True) -- U+00B2 SUPERSCRIPT TWO
  , (0x000000b3, "threesuperior", AChar '\x00b3', True) -- U+00B3 SUPERSCRIPT THREE
  , (0x000000b4, "acute", AChar '\x00b4', True) -- U+00B4 ACUTE ACCENT
  , (0x000000b5, "mu", AChar '\x00b5', True) -- U+00B5 MICRO SIGN
  , (0x000000b6, "paragraph", AChar '\x00b6', True) -- U+00B6 PILCROW SIGN
  , (0x000000b7, "periodcentered", AChar '\x00b7', True) -- U+00B7 MIDDLE DOT
  , (0x000000b8, "cedilla", AChar '\x00b8', True) -- U+00B8 CEDILLA
  , (0x000000b9, "onesuperior", AChar '\x00b9', True) -- U+00B9 SUPERSCRIPT ONE
  , (0x000000ba, "masculine", AChar '\x00ba', True) -- U+00BA MASCULINE ORDINAL INDICATOR
  , (0x000000bb, "guillemotright", AChar '\x00bb', True) -- U+00BB RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
  , (0x000000bc, "onequarter", AChar '\x00bc', True) -- U+00BC VULGAR FRACTION ONE QUARTER
  , (0x000000bd, "onehalf", AChar '\x00bd', True) -- U+00BD VULGAR FRACTION ONE HALF
  , (0x000000be, "threequarters", AChar '\x00be', True) -- U+00BE VULGAR FRACTION THREE QUARTERS
  , (0x000000bf, "questiondown", AChar '\x00bf', True) -- U+00BF INVERTED QUESTION MARK
  , (0x000000c0, "Agrave", AChar '\x00c0', True) -- U+00C0 LATIN CAPITAL LETTER A WITH GRAVE
  , (0x000000c1, "Aacute", AChar '\x00c1', True) -- U+00C1 LATIN CAPITAL LETTER A WITH ACUTE
  , (0x000000c2, "Acircumflex", AChar '\x00c2', True) -- U+00C2 LATIN CAPITAL LETTER A WITH CIRCUMFLEX
  , (0x000000c3, "Atilde", AChar '\x00c3', True) -- U+00C3 LATIN CAPITAL LETTER A WITH TILDE
  , (0x000000c4, "Adiaeresis", AChar '\x00c4', True) -- U+00C4 LATIN CAPITAL LETTER A WITH DIAERESIS
  , (0x000000c5, "Aring", AChar '\x00c5', True) -- U+00C5 LATIN CAPITAL LETTER A WITH RING ABOVE
  , (0x000000c6, "AE", AChar '\x00c6', True) -- U+00C6 LATIN CAPITAL LETTER AE
  , (0x000000c7, "Ccedilla", AChar '\x00c7', True) -- U+00C7 LATIN CAPITAL LETTER C WITH CEDILLA
  , (0x000000c8, "Egrave", AChar '\x00c8', True) -- U+00C8 LATIN CAPITAL LETTER E WITH GRAVE
  , (0x000000c9, "Eacute", AChar '\x00c9', True) -- U+00C9 LATIN CAPITAL LETTER E WITH ACUTE
  , (0x000000ca, "Ecircumflex", AChar '\x00ca', True) -- U+00CA LATIN CAPITAL LETTER E WITH CIRCUMFLEX
  , (0x000000cb, "Ediaeresis", AChar '\x00cb', True) -- U+00CB LATIN CAPITAL LETTER E WITH DIAERESIS
  , (0x000000cc, "Igrave", AChar '\x00cc', True) -- U+00CC LATIN CAPITAL LETTER I WITH GRAVE
  , (0x000000cd, "Iacute", AChar '\x00cd', True) -- U+00CD LATIN CAPITAL LETTER I WITH ACUTE
  , (0x000000ce, "Icircumflex", AChar '\x00ce', True) -- U+00CE LATIN CAPITAL LETTER I WITH CIRCUMFLEX
  , (0x000000cf, "Idiaeresis", AChar '\x00cf', True) -- U+00CF LATIN CAPITAL LETTER I WITH DIAERESIS
  , (0x000000d0, "ETH", AChar '\x00d0', True) -- U+00D0 LATIN CAPITAL LETTER ETH
  , (0x000000d0, "Eth", AChar '\x00d0', False) -- U+00D0 LATIN CAPITAL LETTER ETH [NOTE] deprecated keysym
  , (0x000000d1, "Ntilde", AChar '\x00d1', True) -- U+00D1 LATIN CAPITAL LETTER N WITH TILDE
  , (0x000000d2, "Ograve", AChar '\x00d2', True) -- U+00D2 LATIN CAPITAL LETTER O WITH GRAVE
  , (0x000000d3, "Oacute", AChar '\x00d3', True) -- U+00D3 LATIN CAPITAL LETTER O WITH ACUTE
  , (0x000000d4, "Ocircumflex", AChar '\x00d4', True) -- U+00D4 LATIN CAPITAL LETTER O WITH CIRCUMFLEX
  , (0x000000d5, "Otilde", AChar '\x00d5', True) -- U+00D5 LATIN CAPITAL LETTER O WITH TILDE
  , (0x000000d6, "Odiaeresis", AChar '\x00d6', True) -- U+00D6 LATIN CAPITAL LETTER O WITH DIAERESIS
  , (0x000000d7, "multiply", AChar '\x00d7', True) -- U+00D7 MULTIPLICATION SIGN
  , (0x000000d8, "Oslash", AChar '\x00d8', True) -- U+00D8 LATIN CAPITAL LETTER O WITH STROKE
  , (0x000000d8, "Ooblique", AChar '\x00d8', False) -- U+00D8 LATIN CAPITAL LETTER O WITH STROKE
  , (0x000000d9, "Ugrave", AChar '\x00d9', True) -- U+00D9 LATIN CAPITAL LETTER U WITH GRAVE
  , (0x000000da, "Uacute", AChar '\x00da', True) -- U+00DA LATIN CAPITAL LETTER U WITH ACUTE
  , (0x000000db, "Ucircumflex", AChar '\x00db', True) -- U+00DB LATIN CAPITAL LETTER U WITH CIRCUMFLEX
  , (0x000000dc, "Udiaeresis", AChar '\x00dc', True) -- U+00DC LATIN CAPITAL LETTER U WITH DIAERESIS
  , (0x000000dd, "Yacute", AChar '\x00dd', True) -- U+00DD LATIN CAPITAL LETTER Y WITH ACUTE
  , (0x000000de, "THORN", AChar '\x00de', True) -- U+00DE LATIN CAPITAL LETTER THORN
  , (0x000000de, "Thorn", AChar '\x00de', False) -- U+00DE LATIN CAPITAL LETTER THORN [NOTE] deprecated keysym
  , (0x000000df, "ssharp", AChar '\x00df', True) -- U+00DF LATIN SMALL LETTER SHARP S
  , (0x000000e0, "agrave", AChar '\x00e0', True) -- U+00E0 LATIN SMALL LETTER A WITH GRAVE
  , (0x000000e1, "aacute", AChar '\x00e1', True) -- U+00E1 LATIN SMALL LETTER A WITH ACUTE
  , (0x000000e2, "acircumflex", AChar '\x00e2', True) -- U+00E2 LATIN SMALL LETTER A WITH CIRCUMFLEX
  , (0x000000e3, "atilde", AChar '\x00e3', True) -- U+00E3 LATIN SMALL LETTER A WITH TILDE
  , (0x000000e4, "adiaeresis", AChar '\x00e4', True) -- U+00E4 LATIN SMALL LETTER A WITH DIAERESIS
  , (0x000000e5, "aring", AChar '\x00e5', True) -- U+00E5 LATIN SMALL LETTER A WITH RING ABOVE
  , (0x000000e6, "ae", AChar '\x00e6', True) -- U+00E6 LATIN SMALL LETTER AE
  , (0x000000e7, "ccedilla", AChar '\x00e7', True) -- U+00E7 LATIN SMALL LETTER C WITH CEDILLA
  , (0x000000e8, "egrave", AChar '\x00e8', True) -- U+00E8 LATIN SMALL LETTER E WITH GRAVE
  , (0x000000e9, "eacute", AChar '\x00e9', True) -- U+00E9 LATIN SMALL LETTER E WITH ACUTE
  , (0x000000ea, "ecircumflex", AChar '\x00ea', True) -- U+00EA LATIN SMALL LETTER E WITH CIRCUMFLEX
  , (0x000000eb, "ediaeresis", AChar '\x00eb', True) -- U+00EB LATIN SMALL LETTER E WITH DIAERESIS
  , (0x000000ec, "igrave", AChar '\x00ec', True) -- U+00EC LATIN SMALL LETTER I WITH GRAVE
  , (0x000000ed, "iacute", AChar '\x00ed', True) -- U+00ED LATIN SMALL LETTER I WITH ACUTE
  , (0x000000ee, "icircumflex", AChar '\x00ee', True) -- U+00EE LATIN SMALL LETTER I WITH CIRCUMFLEX
  , (0x000000ef, "idiaeresis", AChar '\x00ef', True) -- U+00EF LATIN SMALL LETTER I WITH DIAERESIS
  , (0x000000f0, "eth", AChar '\x00f0', True) -- U+00F0 LATIN SMALL LETTER ETH
  , (0x000000f1, "ntilde", AChar '\x00f1', True) -- U+00F1 LATIN SMALL LETTER N WITH TILDE
  , (0x000000f2, "ograve", AChar '\x00f2', True) -- U+00F2 LATIN SMALL LETTER O WITH GRAVE
  , (0x000000f3, "oacute", AChar '\x00f3', True) -- U+00F3 LATIN SMALL LETTER O WITH ACUTE
  , (0x000000f4, "ocircumflex", AChar '\x00f4', True) -- U+00F4 LATIN SMALL LETTER O WITH CIRCUMFLEX
  , (0x000000f5, "otilde", AChar '\x00f5', True) -- U+00F5 LATIN SMALL LETTER O WITH TILDE
  , (0x000000f6, "odiaeresis", AChar '\x00f6', True) -- U+00F6 LATIN SMALL LETTER O WITH DIAERESIS
  , (0x000000f7, "division", AChar '\x00f7', True) -- U+00F7 DIVISION SIGN
  , (0x000000f8, "oslash", AChar '\x00f8', True) -- U+00F8 LATIN SMALL LETTER O WITH STROKE
  , (0x000000f8, "ooblique", AChar '\x00f8', False) -- U+00F8 LATIN SMALL LETTER O WITH STROKE
  , (0x000000f9, "ugrave", AChar '\x00f9', True) -- U+00F9 LATIN SMALL LETTER U WITH GRAVE
  , (0x000000fa, "uacute", AChar '\x00fa', True) -- U+00FA LATIN SMALL LETTER U WITH ACUTE
  , (0x000000fb, "ucircumflex", AChar '\x00fb', True) -- U+00FB LATIN SMALL LETTER U WITH CIRCUMFLEX
  , (0x000000fc, "udiaeresis", AChar '\x00fc', True) -- U+00FC LATIN SMALL LETTER U WITH DIAERESIS
  , (0x000000fd, "yacute", AChar '\x00fd', True) -- U+00FD LATIN SMALL LETTER Y WITH ACUTE
  , (0x000000fe, "thorn", AChar '\x00fe', True) -- U+00FE LATIN SMALL LETTER THORN
  , (0x000000ff, "ydiaeresis", AChar '\x00ff', True) -- U+00FF LATIN SMALL LETTER Y WITH DIAERESIS
  --
  -- Latin 2
  --
  , (0x000001a1, "Aogonek", AChar '\x0104', True) -- U+0104 LATIN CAPITAL LETTER A WITH OGONEK
  , (0x000001a2, "breve", AChar '\x02d8', True) -- U+02D8 BREVE
  , (0x000001a3, "Lstroke", AChar '\x0141', True) -- U+0141 LATIN CAPITAL LETTER L WITH STROKE
  , (0x000001a5, "Lcaron", AChar '\x013d', True) -- U+013D LATIN CAPITAL LETTER L WITH CARON
  , (0x000001a6, "Sacute", AChar '\x015a', True) -- U+015A LATIN CAPITAL LETTER S WITH ACUTE
  , (0x000001a9, "Scaron", AChar '\x0160', True) -- U+0160 LATIN CAPITAL LETTER S WITH CARON
  , (0x000001aa, "Scedilla", AChar '\x015e', True) -- U+015E LATIN CAPITAL LETTER S WITH CEDILLA
  , (0x000001ab, "Tcaron", AChar '\x0164', True) -- U+0164 LATIN CAPITAL LETTER T WITH CARON
  , (0x000001ac, "Zacute", AChar '\x0179', True) -- U+0179 LATIN CAPITAL LETTER Z WITH ACUTE
  , (0x000001ae, "Zcaron", AChar '\x017d', True) -- U+017D LATIN CAPITAL LETTER Z WITH CARON
  , (0x000001af, "Zabovedot", AChar '\x017b', True) -- U+017B LATIN CAPITAL LETTER Z WITH DOT ABOVE
  , (0x000001b1, "aogonek", AChar '\x0105', True) -- U+0105 LATIN SMALL LETTER A WITH OGONEK
  , (0x000001b2, "ogonek", AChar '\x02db', True) -- U+02DB OGONEK
  , (0x000001b3, "lstroke", AChar '\x0142', True) -- U+0142 LATIN SMALL LETTER L WITH STROKE
  , (0x000001b5, "lcaron", AChar '\x013e', True) -- U+013E LATIN SMALL LETTER L WITH CARON
  , (0x000001b6, "sacute", AChar '\x015b', True) -- U+015B LATIN SMALL LETTER S WITH ACUTE
  , (0x000001b7, "caron", AChar '\x02c7', True) -- U+02C7 CARON
  , (0x000001b9, "scaron", AChar '\x0161', True) -- U+0161 LATIN SMALL LETTER S WITH CARON
  , (0x000001ba, "scedilla", AChar '\x015f', True) -- U+015F LATIN SMALL LETTER S WITH CEDILLA
  , (0x000001bb, "tcaron", AChar '\x0165', True) -- U+0165 LATIN SMALL LETTER T WITH CARON
  , (0x000001bc, "zacute", AChar '\x017a', True) -- U+017A LATIN SMALL LETTER Z WITH ACUTE
  , (0x000001bd, "doubleacute", AChar '\x02dd', True) -- U+02DD DOUBLE ACUTE ACCENT
  , (0x000001be, "zcaron", AChar '\x017e', True) -- U+017E LATIN SMALL LETTER Z WITH CARON
  , (0x000001bf, "zabovedot", AChar '\x017c', True) -- U+017C LATIN SMALL LETTER Z WITH DOT ABOVE
  , (0x000001c0, "Racute", AChar '\x0154', True) -- U+0154 LATIN CAPITAL LETTER R WITH ACUTE
  , (0x000001c3, "Abreve", AChar '\x0102', True) -- U+0102 LATIN CAPITAL LETTER A WITH BREVE
  , (0x000001c5, "Lacute", AChar '\x0139', True) -- U+0139 LATIN CAPITAL LETTER L WITH ACUTE
  , (0x000001c6, "Cacute", AChar '\x0106', True) -- U+0106 LATIN CAPITAL LETTER C WITH ACUTE
  , (0x000001c8, "Ccaron", AChar '\x010c', True) -- U+010C LATIN CAPITAL LETTER C WITH CARON
  , (0x000001ca, "Eogonek", AChar '\x0118', True) -- U+0118 LATIN CAPITAL LETTER E WITH OGONEK
  , (0x000001cc, "Ecaron", AChar '\x011a', True) -- U+011A LATIN CAPITAL LETTER E WITH CARON
  , (0x000001cf, "Dcaron", AChar '\x010e', True) -- U+010E LATIN CAPITAL LETTER D WITH CARON
  , (0x000001d0, "Dstroke", AChar '\x0110', True) -- U+0110 LATIN CAPITAL LETTER D WITH STROKE
  , (0x000001d1, "Nacute", AChar '\x0143', True) -- U+0143 LATIN CAPITAL LETTER N WITH ACUTE
  , (0x000001d2, "Ncaron", AChar '\x0147', True) -- U+0147 LATIN CAPITAL LETTER N WITH CARON
  , (0x000001d5, "Odoubleacute", AChar '\x0150', True) -- U+0150 LATIN CAPITAL LETTER O WITH DOUBLE ACUTE
  , (0x000001d8, "Rcaron", AChar '\x0158', True) -- U+0158 LATIN CAPITAL LETTER R WITH CARON
  , (0x000001d9, "Uring", AChar '\x016e', True) -- U+016E LATIN CAPITAL LETTER U WITH RING ABOVE
  , (0x000001db, "Udoubleacute", AChar '\x0170', True) -- U+0170 LATIN CAPITAL LETTER U WITH DOUBLE ACUTE
  , (0x000001de, "Tcedilla", AChar '\x0162', True) -- U+0162 LATIN CAPITAL LETTER T WITH CEDILLA
  , (0x000001e0, "racute", AChar '\x0155', True) -- U+0155 LATIN SMALL LETTER R WITH ACUTE
  , (0x000001e3, "abreve", AChar '\x0103', True) -- U+0103 LATIN SMALL LETTER A WITH BREVE
  , (0x000001e5, "lacute", AChar '\x013a', True) -- U+013A LATIN SMALL LETTER L WITH ACUTE
  , (0x000001e6, "cacute", AChar '\x0107', True) -- U+0107 LATIN SMALL LETTER C WITH ACUTE
  , (0x000001e8, "ccaron", AChar '\x010d', True) -- U+010D LATIN SMALL LETTER C WITH CARON
  , (0x000001ea, "eogonek", AChar '\x0119', True) -- U+0119 LATIN SMALL LETTER E WITH OGONEK
  , (0x000001ec, "ecaron", AChar '\x011b', True) -- U+011B LATIN SMALL LETTER E WITH CARON
  , (0x000001ef, "dcaron", AChar '\x010f', True) -- U+010F LATIN SMALL LETTER D WITH CARON
  , (0x000001f0, "dstroke", AChar '\x0111', True) -- U+0111 LATIN SMALL LETTER D WITH STROKE
  , (0x000001f1, "nacute", AChar '\x0144', True) -- U+0144 LATIN SMALL LETTER N WITH ACUTE
  , (0x000001f2, "ncaron", AChar '\x0148', True) -- U+0148 LATIN SMALL LETTER N WITH CARON
  , (0x000001f5, "odoubleacute", AChar '\x0151', True) -- U+0151 LATIN SMALL LETTER O WITH DOUBLE ACUTE
  , (0x000001f8, "rcaron", AChar '\x0159', True) -- U+0159 LATIN SMALL LETTER R WITH CARON
  , (0x000001f9, "uring", AChar '\x016f', True) -- U+016F LATIN SMALL LETTER U WITH RING ABOVE
  , (0x000001fb, "udoubleacute", AChar '\x0171', True) -- U+0171 LATIN SMALL LETTER U WITH DOUBLE ACUTE
  , (0x000001fe, "tcedilla", AChar '\x0163', True) -- U+0163 LATIN SMALL LETTER T WITH CEDILLA
  , (0x000001ff, "abovedot", AChar '\x02d9', True) -- U+02D9 DOT ABOVE
  --
  -- Latin 3
  --
  , (0x000002a1, "Hstroke", AChar '\x0126', True) -- U+0126 LATIN CAPITAL LETTER H WITH STROKE
  , (0x000002a6, "Hcircumflex", AChar '\x0124', True) -- U+0124 LATIN CAPITAL LETTER H WITH CIRCUMFLEX
  , (0x000002a9, "Iabovedot", AChar '\x0130', True) -- U+0130 LATIN CAPITAL LETTER I WITH DOT ABOVE
  , (0x000002ab, "Gbreve", AChar '\x011e', True) -- U+011E LATIN CAPITAL LETTER G WITH BREVE
  , (0x000002ac, "Jcircumflex", AChar '\x0134', True) -- U+0134 LATIN CAPITAL LETTER J WITH CIRCUMFLEX
  , (0x000002b1, "hstroke", AChar '\x0127', True) -- U+0127 LATIN SMALL LETTER H WITH STROKE
  , (0x000002b6, "hcircumflex", AChar '\x0125', True) -- U+0125 LATIN SMALL LETTER H WITH CIRCUMFLEX
  , (0x000002b9, "idotless", AChar '\x0131', True) -- U+0131 LATIN SMALL LETTER DOTLESS I
  , (0x000002bb, "gbreve", AChar '\x011f', True) -- U+011F LATIN SMALL LETTER G WITH BREVE
  , (0x000002bc, "jcircumflex", AChar '\x0135', True) -- U+0135 LATIN SMALL LETTER J WITH CIRCUMFLEX
  , (0x000002c5, "Cabovedot", AChar '\x010a', True) -- U+010A LATIN CAPITAL LETTER C WITH DOT ABOVE
  , (0x000002c6, "Ccircumflex", AChar '\x0108', True) -- U+0108 LATIN CAPITAL LETTER C WITH CIRCUMFLEX
  , (0x000002d5, "Gabovedot", AChar '\x0120', True) -- U+0120 LATIN CAPITAL LETTER G WITH DOT ABOVE
  , (0x000002d8, "Gcircumflex", AChar '\x011c', True) -- U+011C LATIN CAPITAL LETTER G WITH CIRCUMFLEX
  , (0x000002dd, "Ubreve", AChar '\x016c', True) -- U+016C LATIN CAPITAL LETTER U WITH BREVE
  , (0x000002de, "Scircumflex", AChar '\x015c', True) -- U+015C LATIN CAPITAL LETTER S WITH CIRCUMFLEX
  , (0x000002e5, "cabovedot", AChar '\x010b', True) -- U+010B LATIN SMALL LETTER C WITH DOT ABOVE
  , (0x000002e6, "ccircumflex", AChar '\x0109', True) -- U+0109 LATIN SMALL LETTER C WITH CIRCUMFLEX
  , (0x000002f5, "gabovedot", AChar '\x0121', True) -- U+0121 LATIN SMALL LETTER G WITH DOT ABOVE
  , (0x000002f8, "gcircumflex", AChar '\x011d', True) -- U+011D LATIN SMALL LETTER G WITH CIRCUMFLEX
  , (0x000002fd, "ubreve", AChar '\x016d', True) -- U+016D LATIN SMALL LETTER U WITH BREVE
  , (0x000002fe, "scircumflex", AChar '\x015d', True) -- U+015D LATIN SMALL LETTER S WITH CIRCUMFLEX
  --
  -- Latin 4
  --
  , (0x000003a2, "kra", AChar '\x0138', True) -- U+0138 LATIN SMALL LETTER KRA
  , (0x000003a2, "kappa", AChar '\x0138', False) -- U+0138 LATIN SMALL LETTER KRA [NOTE] deprecated keysym
  , (0x000003a3, "Rcedilla", AChar '\x0156', True) -- U+0156 LATIN CAPITAL LETTER R WITH CEDILLA
  , (0x000003a5, "Itilde", AChar '\x0128', True) -- U+0128 LATIN CAPITAL LETTER I WITH TILDE
  , (0x000003a6, "Lcedilla", AChar '\x013b', True) -- U+013B LATIN CAPITAL LETTER L WITH CEDILLA
  , (0x000003aa, "Emacron", AChar '\x0112', True) -- U+0112 LATIN CAPITAL LETTER E WITH MACRON
  , (0x000003ab, "Gcedilla", AChar '\x0122', True) -- U+0122 LATIN CAPITAL LETTER G WITH CEDILLA
  , (0x000003ac, "Tslash", AChar '\x0166', True) -- U+0166 LATIN CAPITAL LETTER T WITH STROKE
  , (0x000003b3, "rcedilla", AChar '\x0157', True) -- U+0157 LATIN SMALL LETTER R WITH CEDILLA
  , (0x000003b5, "itilde", AChar '\x0129', True) -- U+0129 LATIN SMALL LETTER I WITH TILDE
  , (0x000003b6, "lcedilla", AChar '\x013c', True) -- U+013C LATIN SMALL LETTER L WITH CEDILLA
  , (0x000003ba, "emacron", AChar '\x0113', True) -- U+0113 LATIN SMALL LETTER E WITH MACRON
  , (0x000003bb, "gcedilla", AChar '\x0123', True) -- U+0123 LATIN SMALL LETTER G WITH CEDILLA
  , (0x000003bc, "tslash", AChar '\x0167', True) -- U+0167 LATIN SMALL LETTER T WITH STROKE
  , (0x000003bd, "ENG", AChar '\x014a', True) -- U+014A LATIN CAPITAL LETTER ENG
  , (0x000003bf, "eng", AChar '\x014b', True) -- U+014B LATIN SMALL LETTER ENG
  , (0x000003c0, "Amacron", AChar '\x0100', True) -- U+0100 LATIN CAPITAL LETTER A WITH MACRON
  , (0x000003c7, "Iogonek", AChar '\x012e', True) -- U+012E LATIN CAPITAL LETTER I WITH OGONEK
  , (0x000003cc, "Eabovedot", AChar '\x0116', True) -- U+0116 LATIN CAPITAL LETTER E WITH DOT ABOVE
  , (0x000003cf, "Imacron", AChar '\x012a', True) -- U+012A LATIN CAPITAL LETTER I WITH MACRON
  , (0x000003d1, "Ncedilla", AChar '\x0145', True) -- U+0145 LATIN CAPITAL LETTER N WITH CEDILLA
  , (0x000003d2, "Omacron", AChar '\x014c', True) -- U+014C LATIN CAPITAL LETTER O WITH MACRON
  , (0x000003d3, "Kcedilla", AChar '\x0136', True) -- U+0136 LATIN CAPITAL LETTER K WITH CEDILLA
  , (0x000003d9, "Uogonek", AChar '\x0172', True) -- U+0172 LATIN CAPITAL LETTER U WITH OGONEK
  , (0x000003dd, "Utilde", AChar '\x0168', True) -- U+0168 LATIN CAPITAL LETTER U WITH TILDE
  , (0x000003de, "Umacron", AChar '\x016a', True) -- U+016A LATIN CAPITAL LETTER U WITH MACRON
  , (0x000003e0, "amacron", AChar '\x0101', True) -- U+0101 LATIN SMALL LETTER A WITH MACRON
  , (0x000003e7, "iogonek", AChar '\x012f', True) -- U+012F LATIN SMALL LETTER I WITH OGONEK
  , (0x000003ec, "eabovedot", AChar '\x0117', True) -- U+0117 LATIN SMALL LETTER E WITH DOT ABOVE
  , (0x000003ef, "imacron", AChar '\x012b', True) -- U+012B LATIN SMALL LETTER I WITH MACRON
  , (0x000003f1, "ncedilla", AChar '\x0146', True) -- U+0146 LATIN SMALL LETTER N WITH CEDILLA
  , (0x000003f2, "omacron", AChar '\x014d', True) -- U+014D LATIN SMALL LETTER O WITH MACRON
  , (0x000003f3, "kcedilla", AChar '\x0137', True) -- U+0137 LATIN SMALL LETTER K WITH CEDILLA
  , (0x000003f9, "uogonek", AChar '\x0173', True) -- U+0173 LATIN SMALL LETTER U WITH OGONEK
  , (0x000003fd, "utilde", AChar '\x0169', True) -- U+0169 LATIN SMALL LETTER U WITH TILDE
  , (0x000003fe, "umacron", AChar '\x016b', True) -- U+016B LATIN SMALL LETTER U WITH MACRON
  --
  -- Latin 8
  --
  , (0x01000174, "Wcircumflex", AChar '\x0174', True) -- U+0174 LATIN CAPITAL LETTER W WITH CIRCUMFLEX
  , (0x01000175, "wcircumflex", AChar '\x0175', True) -- U+0175 LATIN SMALL LETTER W WITH CIRCUMFLEX
  , (0x01000176, "Ycircumflex", AChar '\x0176', True) -- U+0176 LATIN CAPITAL LETTER Y WITH CIRCUMFLEX
  , (0x01000177, "ycircumflex", AChar '\x0177', True) -- U+0177 LATIN SMALL LETTER Y WITH CIRCUMFLEX
  , (0x01001e02, "Babovedot", AChar '\x1e02', True) -- U+1E02 LATIN CAPITAL LETTER B WITH DOT ABOVE
  , (0x01001e03, "babovedot", AChar '\x1e03', True) -- U+1E03 LATIN SMALL LETTER B WITH DOT ABOVE
  , (0x01001e0a, "Dabovedot", AChar '\x1e0a', True) -- U+1E0A LATIN CAPITAL LETTER D WITH DOT ABOVE
  , (0x01001e0b, "dabovedot", AChar '\x1e0b', True) -- U+1E0B LATIN SMALL LETTER D WITH DOT ABOVE
  , (0x01001e1e, "Fabovedot", AChar '\x1e1e', True) -- U+1E1E LATIN CAPITAL LETTER F WITH DOT ABOVE
  , (0x01001e1f, "fabovedot", AChar '\x1e1f', True) -- U+1E1F LATIN SMALL LETTER F WITH DOT ABOVE
  , (0x01001e40, "Mabovedot", AChar '\x1e40', True) -- U+1E40 LATIN CAPITAL LETTER M WITH DOT ABOVE
  , (0x01001e41, "mabovedot", AChar '\x1e41', True) -- U+1E41 LATIN SMALL LETTER M WITH DOT ABOVE
  , (0x01001e56, "Pabovedot", AChar '\x1e56', True) -- U+1E56 LATIN CAPITAL LETTER P WITH DOT ABOVE
  , (0x01001e57, "pabovedot", AChar '\x1e57', True) -- U+1E57 LATIN SMALL LETTER P WITH DOT ABOVE
  , (0x01001e60, "Sabovedot", AChar '\x1e60', True) -- U+1E60 LATIN CAPITAL LETTER S WITH DOT ABOVE
  , (0x01001e61, "sabovedot", AChar '\x1e61', True) -- U+1E61 LATIN SMALL LETTER S WITH DOT ABOVE
  , (0x01001e6a, "Tabovedot", AChar '\x1e6a', True) -- U+1E6A LATIN CAPITAL LETTER T WITH DOT ABOVE
  , (0x01001e6b, "tabovedot", AChar '\x1e6b', True) -- U+1E6B LATIN SMALL LETTER T WITH DOT ABOVE
  , (0x01001e80, "Wgrave", AChar '\x1e80', True) -- U+1E80 LATIN CAPITAL LETTER W WITH GRAVE
  , (0x01001e81, "wgrave", AChar '\x1e81', True) -- U+1E81 LATIN SMALL LETTER W WITH GRAVE
  , (0x01001e82, "Wacute", AChar '\x1e82', True) -- U+1E82 LATIN CAPITAL LETTER W WITH ACUTE
  , (0x01001e83, "wacute", AChar '\x1e83', True) -- U+1E83 LATIN SMALL LETTER W WITH ACUTE
  , (0x01001e84, "Wdiaeresis", AChar '\x1e84', True) -- U+1E84 LATIN CAPITAL LETTER W WITH DIAERESIS
  , (0x01001e85, "wdiaeresis", AChar '\x1e85', True) -- U+1E85 LATIN SMALL LETTER W WITH DIAERESIS
  , (0x01001ef2, "Ygrave", AChar '\x1ef2', True) -- U+1EF2 LATIN CAPITAL LETTER Y WITH GRAVE
  , (0x01001ef3, "ygrave", AChar '\x1ef3', True) -- U+1EF3 LATIN SMALL LETTER Y WITH GRAVE
  --
  -- Latin 9
  --
  , (0x000013bc, "OE", AChar '\x0152', True) -- U+0152 LATIN CAPITAL LIGATURE OE
  , (0x000013bd, "oe", AChar '\x0153', True) -- U+0153 LATIN SMALL LIGATURE OE
  , (0x000013be, "Ydiaeresis", AChar '\x0178', True) -- U+0178 LATIN CAPITAL LETTER Y WITH DIAERESIS
  --
  -- Katakana
  --
  , (0x0000047e, "overline", AChar '\x203e', True) -- U+203E OVERLINE
  , (0x000004a1, "kana_fullstop", AChar '\x3002', True) -- U+3002 IDEOGRAPHIC FULL STOP
  , (0x000004a2, "kana_openingbracket", AChar '\x300c', True) -- U+300C LEFT CORNER BRACKET
  , (0x000004a3, "kana_closingbracket", AChar '\x300d', True) -- U+300D RIGHT CORNER BRACKET
  , (0x000004a4, "kana_comma", AChar '\x3001', True) -- U+3001 IDEOGRAPHIC COMMA
  , (0x000004a5, "kana_conjunctive", AChar '\x30fb', True) -- U+30FB KATAKANA MIDDLE DOT
  , (0x000004a5, "kana_middledot", AChar '\x30fb', False) -- U+30FB KATAKANA MIDDLE DOT [NOTE] deprecated keysym
  , (0x000004a6, "kana_WO", AChar '\x30f2', True) -- U+30F2 KATAKANA LETTER WO
  , (0x000004a7, "kana_a", AChar '\x30a1', True) -- U+30A1 KATAKANA LETTER SMALL A
  , (0x000004a8, "kana_i", AChar '\x30a3', True) -- U+30A3 KATAKANA LETTER SMALL I
  , (0x000004a9, "kana_u", AChar '\x30a5', True) -- U+30A5 KATAKANA LETTER SMALL U
  , (0x000004aa, "kana_e", AChar '\x30a7', True) -- U+30A7 KATAKANA LETTER SMALL E
  , (0x000004ab, "kana_o", AChar '\x30a9', True) -- U+30A9 KATAKANA LETTER SMALL O
  , (0x000004ac, "kana_ya", AChar '\x30e3', True) -- U+30E3 KATAKANA LETTER SMALL YA
  , (0x000004ad, "kana_yu", AChar '\x30e5', True) -- U+30E5 KATAKANA LETTER SMALL YU
  , (0x000004ae, "kana_yo", AChar '\x30e7', True) -- U+30E7 KATAKANA LETTER SMALL YO
  , (0x000004af, "kana_tsu", AChar '\x30c3', True) -- U+30C3 KATAKANA LETTER SMALL TU
  , (0x000004af, "kana_tu", AChar '\x30c3', False) -- U+30C3 KATAKANA LETTER SMALL TU [NOTE] deprecated keysym
  , (0x000004b0, "prolongedsound", AChar '\x30fc', True) -- U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK
  , (0x000004b1, "kana_A", AChar '\x30a2', True) -- U+30A2 KATAKANA LETTER A
  , (0x000004b2, "kana_I", AChar '\x30a4', True) -- U+30A4 KATAKANA LETTER I
  , (0x000004b3, "kana_U", AChar '\x30a6', True) -- U+30A6 KATAKANA LETTER U
  , (0x000004b4, "kana_E", AChar '\x30a8', True) -- U+30A8 KATAKANA LETTER E
  , (0x000004b5, "kana_O", AChar '\x30aa', True) -- U+30AA KATAKANA LETTER O
  , (0x000004b6, "kana_KA", AChar '\x30ab', True) -- U+30AB KATAKANA LETTER KA
  , (0x000004b7, "kana_KI", AChar '\x30ad', True) -- U+30AD KATAKANA LETTER KI
  , (0x000004b8, "kana_KU", AChar '\x30af', True) -- U+30AF KATAKANA LETTER KU
  , (0x000004b9, "kana_KE", AChar '\x30b1', True) -- U+30B1 KATAKANA LETTER KE
  , (0x000004ba, "kana_KO", AChar '\x30b3', True) -- U+30B3 KATAKANA LETTER KO
  , (0x000004bb, "kana_SA", AChar '\x30b5', True) -- U+30B5 KATAKANA LETTER SA
  , (0x000004bc, "kana_SHI", AChar '\x30b7', True) -- U+30B7 KATAKANA LETTER SI
  , (0x000004bd, "kana_SU", AChar '\x30b9', True) -- U+30B9 KATAKANA LETTER SU
  , (0x000004be, "kana_SE", AChar '\x30bb', True) -- U+30BB KATAKANA LETTER SE
  , (0x000004bf, "kana_SO", AChar '\x30bd', True) -- U+30BD KATAKANA LETTER SO
  , (0x000004c0, "kana_TA", AChar '\x30bf', True) -- U+30BF KATAKANA LETTER TA
  , (0x000004c1, "kana_CHI", AChar '\x30c1', True) -- U+30C1 KATAKANA LETTER TI
  , (0x000004c1, "kana_TI", AChar '\x30c1', False) -- U+30C1 KATAKANA LETTER TI [NOTE] deprecated keysym
  , (0x000004c2, "kana_TSU", AChar '\x30c4', True) -- U+30C4 KATAKANA LETTER TU
  , (0x000004c2, "kana_TU", AChar '\x30c4', False) -- U+30C4 KATAKANA LETTER TU [NOTE] deprecated keysym
  , (0x000004c3, "kana_TE", AChar '\x30c6', True) -- U+30C6 KATAKANA LETTER TE
  , (0x000004c4, "kana_TO", AChar '\x30c8', True) -- U+30C8 KATAKANA LETTER TO
  , (0x000004c5, "kana_NA", AChar '\x30ca', True) -- U+30CA KATAKANA LETTER NA
  , (0x000004c6, "kana_NI", AChar '\x30cb', True) -- U+30CB KATAKANA LETTER NI
  , (0x000004c7, "kana_NU", AChar '\x30cc', True) -- U+30CC KATAKANA LETTER NU
  , (0x000004c8, "kana_NE", AChar '\x30cd', True) -- U+30CD KATAKANA LETTER NE
  , (0x000004c9, "kana_NO", AChar '\x30ce', True) -- U+30CE KATAKANA LETTER NO
  , (0x000004ca, "kana_HA", AChar '\x30cf', True) -- U+30CF KATAKANA LETTER HA
  , (0x000004cb, "kana_HI", AChar '\x30d2', True) -- U+30D2 KATAKANA LETTER HI
  , (0x000004cc, "kana_FU", AChar '\x30d5', True) -- U+30D5 KATAKANA LETTER HU
  , (0x000004cc, "kana_HU", AChar '\x30d5', False) -- U+30D5 KATAKANA LETTER HU [NOTE] deprecated keysym
  , (0x000004cd, "kana_HE", AChar '\x30d8', True) -- U+30D8 KATAKANA LETTER HE
  , (0x000004ce, "kana_HO", AChar '\x30db', True) -- U+30DB KATAKANA LETTER HO
  , (0x000004cf, "kana_MA", AChar '\x30de', True) -- U+30DE KATAKANA LETTER MA
  , (0x000004d0, "kana_MI", AChar '\x30df', True) -- U+30DF KATAKANA LETTER MI
  , (0x000004d1, "kana_MU", AChar '\x30e0', True) -- U+30E0 KATAKANA LETTER MU
  , (0x000004d2, "kana_ME", AChar '\x30e1', True) -- U+30E1 KATAKANA LETTER ME
  , (0x000004d3, "kana_MO", AChar '\x30e2', True) -- U+30E2 KATAKANA LETTER MO
  , (0x000004d4, "kana_YA", AChar '\x30e4', True) -- U+30E4 KATAKANA LETTER YA
  , (0x000004d5, "kana_YU", AChar '\x30e6', True) -- U+30E6 KATAKANA LETTER YU
  , (0x000004d6, "kana_YO", AChar '\x30e8', True) -- U+30E8 KATAKANA LETTER YO
  , (0x000004d7, "kana_RA", AChar '\x30e9', True) -- U+30E9 KATAKANA LETTER RA
  , (0x000004d8, "kana_RI", AChar '\x30ea', True) -- U+30EA KATAKANA LETTER RI
  , (0x000004d9, "kana_RU", AChar '\x30eb', True) -- U+30EB KATAKANA LETTER RU
  , (0x000004da, "kana_RE", AChar '\x30ec', True) -- U+30EC KATAKANA LETTER RE
  , (0x000004db, "kana_RO", AChar '\x30ed', True) -- U+30ED KATAKANA LETTER RO
  , (0x000004dc, "kana_WA", AChar '\x30ef', True) -- U+30EF KATAKANA LETTER WA
  , (0x000004dd, "kana_N", AChar '\x30f3', True) -- U+30F3 KATAKANA LETTER N
  , (0x000004de, "voicedsound", AChar '\x309b', True) -- U+309B KATAKANA-HIRAGANA VOICED SOUND MARK
  , (0x000004df, "semivoicedsound", AChar '\x309c', True) -- U+309C KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK
  , (0x0000ff7e, "kana_switch", ASpecial Iso_Group_Shift, False) -- Alias for mode_switch
  --
  -- Arabic
  --
  , (0x010006f0, "Farsi_0", AChar '\x06f0', True) -- U+06F0 EXTENDED ARABIC-INDIC DIGIT ZERO
  , (0x010006f1, "Farsi_1", AChar '\x06f1', True) -- U+06F1 EXTENDED ARABIC-INDIC DIGIT ONE
  , (0x010006f2, "Farsi_2", AChar '\x06f2', True) -- U+06F2 EXTENDED ARABIC-INDIC DIGIT TWO
  , (0x010006f3, "Farsi_3", AChar '\x06f3', True) -- U+06F3 EXTENDED ARABIC-INDIC DIGIT THREE
  , (0x010006f4, "Farsi_4", AChar '\x06f4', True) -- U+06F4 EXTENDED ARABIC-INDIC DIGIT FOUR
  , (0x010006f5, "Farsi_5", AChar '\x06f5', True) -- U+06F5 EXTENDED ARABIC-INDIC DIGIT FIVE
  , (0x010006f6, "Farsi_6", AChar '\x06f6', True) -- U+06F6 EXTENDED ARABIC-INDIC DIGIT SIX
  , (0x010006f7, "Farsi_7", AChar '\x06f7', True) -- U+06F7 EXTENDED ARABIC-INDIC DIGIT SEVEN
  , (0x010006f8, "Farsi_8", AChar '\x06f8', True) -- U+06F8 EXTENDED ARABIC-INDIC DIGIT EIGHT
  , (0x010006f9, "Farsi_9", AChar '\x06f9', True) -- U+06F9 EXTENDED ARABIC-INDIC DIGIT NINE
  , (0x0100066a, "Arabic_percent", AChar '\x066a', True) -- U+066A ARABIC PERCENT SIGN
  , (0x01000670, "Arabic_superscript_alef", AChar '\x0670', True) -- U+0670 ARABIC LETTER SUPERSCRIPT ALEF
  , (0x01000679, "Arabic_tteh", AChar '\x0679', True) -- U+0679 ARABIC LETTER TTEH
  , (0x0100067e, "Arabic_peh", AChar '\x067e', True) -- U+067E ARABIC LETTER PEH
  , (0x01000686, "Arabic_tcheh", AChar '\x0686', True) -- U+0686 ARABIC LETTER TCHEH
  , (0x01000688, "Arabic_ddal", AChar '\x0688', True) -- U+0688 ARABIC LETTER DDAL
  , (0x01000691, "Arabic_rreh", AChar '\x0691', True) -- U+0691 ARABIC LETTER RREH
  , (0x000005ac, "Arabic_comma", AChar '\x060c', True) -- U+060C ARABIC COMMA
  , (0x010006d4, "Arabic_fullstop", AChar '\x06d4', True) -- U+06D4 ARABIC FULL STOP
  , (0x01000660, "Arabic_0", AChar '\x0660', True) -- U+0660 ARABIC-INDIC DIGIT ZERO
  , (0x01000661, "Arabic_1", AChar '\x0661', True) -- U+0661 ARABIC-INDIC DIGIT ONE
  , (0x01000662, "Arabic_2", AChar '\x0662', True) -- U+0662 ARABIC-INDIC DIGIT TWO
  , (0x01000663, "Arabic_3", AChar '\x0663', True) -- U+0663 ARABIC-INDIC DIGIT THREE
  , (0x01000664, "Arabic_4", AChar '\x0664', True) -- U+0664 ARABIC-INDIC DIGIT FOUR
  , (0x01000665, "Arabic_5", AChar '\x0665', True) -- U+0665 ARABIC-INDIC DIGIT FIVE
  , (0x01000666, "Arabic_6", AChar '\x0666', True) -- U+0666 ARABIC-INDIC DIGIT SIX
  , (0x01000667, "Arabic_7", AChar '\x0667', True) -- U+0667 ARABIC-INDIC DIGIT SEVEN
  , (0x01000668, "Arabic_8", AChar '\x0668', True) -- U+0668 ARABIC-INDIC DIGIT EIGHT
  , (0x01000669, "Arabic_9", AChar '\x0669', True) -- U+0669 ARABIC-INDIC DIGIT NINE
  , (0x000005bb, "Arabic_semicolon", AChar '\x061b', True) -- U+061B ARABIC SEMICOLON
  , (0x000005bf, "Arabic_question_mark", AChar '\x061f', True) -- U+061F ARABIC QUESTION MARK
  , (0x000005c1, "Arabic_hamza", AChar '\x0621', True) -- U+0621 ARABIC LETTER HAMZA
  , (0x000005c2, "Arabic_maddaonalef", AChar '\x0622', True) -- U+0622 ARABIC LETTER ALEF WITH MADDA ABOVE
  , (0x000005c3, "Arabic_hamzaonalef", AChar '\x0623', True) -- U+0623 ARABIC LETTER ALEF WITH HAMZA ABOVE
  , (0x000005c4, "Arabic_hamzaonwaw", AChar '\x0624', True) -- U+0624 ARABIC LETTER WAW WITH HAMZA ABOVE
  , (0x000005c5, "Arabic_hamzaunderalef", AChar '\x0625', True) -- U+0625 ARABIC LETTER ALEF WITH HAMZA BELOW
  , (0x000005c6, "Arabic_hamzaonyeh", AChar '\x0626', True) -- U+0626 ARABIC LETTER YEH WITH HAMZA ABOVE
  , (0x000005c7, "Arabic_alef", AChar '\x0627', True) -- U+0627 ARABIC LETTER ALEF
  , (0x000005c8, "Arabic_beh", AChar '\x0628', True) -- U+0628 ARABIC LETTER BEH
  , (0x000005c9, "Arabic_tehmarbuta", AChar '\x0629', True) -- U+0629 ARABIC LETTER TEH MARBUTA
  , (0x000005ca, "Arabic_teh", AChar '\x062a', True) -- U+062A ARABIC LETTER TEH
  , (0x000005cb, "Arabic_theh", AChar '\x062b', True) -- U+062B ARABIC LETTER THEH
  , (0x000005cc, "Arabic_jeem", AChar '\x062c', True) -- U+062C ARABIC LETTER JEEM
  , (0x000005cd, "Arabic_hah", AChar '\x062d', True) -- U+062D ARABIC LETTER HAH
  , (0x000005ce, "Arabic_khah", AChar '\x062e', True) -- U+062E ARABIC LETTER KHAH
  , (0x000005cf, "Arabic_dal", AChar '\x062f', True) -- U+062F ARABIC LETTER DAL
  , (0x000005d0, "Arabic_thal", AChar '\x0630', True) -- U+0630 ARABIC LETTER THAL
  , (0x000005d1, "Arabic_ra", AChar '\x0631', True) -- U+0631 ARABIC LETTER REH
  , (0x000005d2, "Arabic_zain", AChar '\x0632', True) -- U+0632 ARABIC LETTER ZAIN
  , (0x000005d3, "Arabic_seen", AChar '\x0633', True) -- U+0633 ARABIC LETTER SEEN
  , (0x000005d4, "Arabic_sheen", AChar '\x0634', True) -- U+0634 ARABIC LETTER SHEEN
  , (0x000005d5, "Arabic_sad", AChar '\x0635', True) -- U+0635 ARABIC LETTER SAD
  , (0x000005d6, "Arabic_dad", AChar '\x0636', True) -- U+0636 ARABIC LETTER DAD
  , (0x000005d7, "Arabic_tah", AChar '\x0637', True) -- U+0637 ARABIC LETTER TAH
  , (0x000005d8, "Arabic_zah", AChar '\x0638', True) -- U+0638 ARABIC LETTER ZAH
  , (0x000005d9, "Arabic_ain", AChar '\x0639', True) -- U+0639 ARABIC LETTER AIN
  , (0x000005da, "Arabic_ghain", AChar '\x063a', True) -- U+063A ARABIC LETTER GHAIN
  , (0x000005e0, "Arabic_tatweel", AChar '\x0640', True) -- U+0640 ARABIC TATWEEL
  , (0x000005e1, "Arabic_feh", AChar '\x0641', True) -- U+0641 ARABIC LETTER FEH
  , (0x000005e2, "Arabic_qaf", AChar '\x0642', True) -- U+0642 ARABIC LETTER QAF
  , (0x000005e3, "Arabic_kaf", AChar '\x0643', True) -- U+0643 ARABIC LETTER KAF
  , (0x000005e4, "Arabic_lam", AChar '\x0644', True) -- U+0644 ARABIC LETTER LAM
  , (0x000005e5, "Arabic_meem", AChar '\x0645', True) -- U+0645 ARABIC LETTER MEEM
  , (0x000005e6, "Arabic_noon", AChar '\x0646', True) -- U+0646 ARABIC LETTER NOON
  , (0x000005e7, "Arabic_ha", AChar '\x0647', True) -- U+0647 ARABIC LETTER HEH
  , (0x000005e7, "Arabic_heh", AChar '\x0647', False) -- U+0647 ARABIC LETTER HEH [NOTE] deprecated keysym
  , (0x000005e8, "Arabic_waw", AChar '\x0648', True) -- U+0648 ARABIC LETTER WAW
  , (0x000005e9, "Arabic_alefmaksura", AChar '\x0649', True) -- U+0649 ARABIC LETTER ALEF MAKSURA
  , (0x000005ea, "Arabic_yeh", AChar '\x064a', True) -- U+064A ARABIC LETTER YEH
  , (0x000005eb, "Arabic_fathatan", AChar '\x064b', True) -- U+064B ARABIC FATHATAN
  , (0x000005ec, "Arabic_dammatan", AChar '\x064c', True) -- U+064C ARABIC DAMMATAN
  , (0x000005ed, "Arabic_kasratan", AChar '\x064d', True) -- U+064D ARABIC KASRATAN
  , (0x000005ee, "Arabic_fatha", AChar '\x064e', True) -- U+064E ARABIC FATHA
  , (0x000005ef, "Arabic_damma", AChar '\x064f', True) -- U+064F ARABIC DAMMA
  , (0x000005f0, "Arabic_kasra", AChar '\x0650', True) -- U+0650 ARABIC KASRA
  , (0x000005f1, "Arabic_shadda", AChar '\x0651', True) -- U+0651 ARABIC SHADDA
  , (0x000005f2, "Arabic_sukun", AChar '\x0652', True) -- U+0652 ARABIC SUKUN
  , (0x01000653, "Arabic_madda_above", AChar '\x0653', True) -- U+0653 ARABIC MADDAH ABOVE
  , (0x01000654, "Arabic_hamza_above", AChar '\x0654', True) -- U+0654 ARABIC HAMZA ABOVE
  , (0x01000655, "Arabic_hamza_below", AChar '\x0655', True) -- U+0655 ARABIC HAMZA BELOW
  , (0x01000698, "Arabic_jeh", AChar '\x0698', True) -- U+0698 ARABIC LETTER JEH
  , (0x010006a4, "Arabic_veh", AChar '\x06a4', True) -- U+06A4 ARABIC LETTER VEH
  , (0x010006a9, "Arabic_keheh", AChar '\x06a9', True) -- U+06A9 ARABIC LETTER KEHEH
  , (0x010006af, "Arabic_gaf", AChar '\x06af', True) -- U+06AF ARABIC LETTER GAF
  , (0x010006ba, "Arabic_noon_ghunna", AChar '\x06ba', True) -- U+06BA ARABIC LETTER NOON GHUNNA
  , (0x010006be, "Arabic_heh_doachashmee", AChar '\x06be', True) -- U+06BE ARABIC LETTER HEH DOACHASHMEE
  , (0x010006cc, "Farsi_yeh", AChar '\x06cc', True) -- U+06CC ARABIC LETTER FARSI YEH
  , (0x010006cc, "Arabic_farsi_yeh", AChar '\x06cc', False) -- U+06CC ARABIC LETTER FARSI YEH
  , (0x010006d2, "Arabic_yeh_baree", AChar '\x06d2', True) -- U+06D2 ARABIC LETTER YEH BARREE
  , (0x010006c1, "Arabic_heh_goal", AChar '\x06c1', True) -- U+06C1 ARABIC LETTER HEH GOAL
  , (0x0000ff7e, "Arabic_switch", ASpecial Iso_Group_Shift, False) -- Alias for mode_switch
  --
  -- Cyrillic
  --
  , (0x01000492, "Cyrillic_GHE_bar", AChar '\x0492', True) -- U+0492 CYRILLIC CAPITAL LETTER GHE WITH STROKE
  , (0x01000493, "Cyrillic_ghe_bar", AChar '\x0493', True) -- U+0493 CYRILLIC SMALL LETTER GHE WITH STROKE
  , (0x01000496, "Cyrillic_ZHE_descender", AChar '\x0496', True) -- U+0496 CYRILLIC CAPITAL LETTER ZHE WITH DESCENDER
  , (0x01000497, "Cyrillic_zhe_descender", AChar '\x0497', True) -- U+0497 CYRILLIC SMALL LETTER ZHE WITH DESCENDER
  , (0x0100049a, "Cyrillic_KA_descender", AChar '\x049a', True) -- U+049A CYRILLIC CAPITAL LETTER KA WITH DESCENDER
  , (0x0100049b, "Cyrillic_ka_descender", AChar '\x049b', True) -- U+049B CYRILLIC SMALL LETTER KA WITH DESCENDER
  , (0x0100049c, "Cyrillic_KA_vertstroke", AChar '\x049c', True) -- U+049C CYRILLIC CAPITAL LETTER KA WITH VERTICAL STROKE
  , (0x0100049d, "Cyrillic_ka_vertstroke", AChar '\x049d', True) -- U+049D CYRILLIC SMALL LETTER KA WITH VERTICAL STROKE
  , (0x010004a2, "Cyrillic_EN_descender", AChar '\x04a2', True) -- U+04A2 CYRILLIC CAPITAL LETTER EN WITH DESCENDER
  , (0x010004a3, "Cyrillic_en_descender", AChar '\x04a3', True) -- U+04A3 CYRILLIC SMALL LETTER EN WITH DESCENDER
  , (0x010004ae, "Cyrillic_U_straight", AChar '\x04ae', True) -- U+04AE CYRILLIC CAPITAL LETTER STRAIGHT U
  , (0x010004af, "Cyrillic_u_straight", AChar '\x04af', True) -- U+04AF CYRILLIC SMALL LETTER STRAIGHT U
  , (0x010004b0, "Cyrillic_U_straight_bar", AChar '\x04b0', True) -- U+04B0 CYRILLIC CAPITAL LETTER STRAIGHT U WITH STROKE
  , (0x010004b1, "Cyrillic_u_straight_bar", AChar '\x04b1', True) -- U+04B1 CYRILLIC SMALL LETTER STRAIGHT U WITH STROKE
  , (0x010004b2, "Cyrillic_HA_descender", AChar '\x04b2', True) -- U+04B2 CYRILLIC CAPITAL LETTER HA WITH DESCENDER
  , (0x010004b3, "Cyrillic_ha_descender", AChar '\x04b3', True) -- U+04B3 CYRILLIC SMALL LETTER HA WITH DESCENDER
  , (0x010004b6, "Cyrillic_CHE_descender", AChar '\x04b6', True) -- U+04B6 CYRILLIC CAPITAL LETTER CHE WITH DESCENDER
  , (0x010004b7, "Cyrillic_che_descender", AChar '\x04b7', True) -- U+04B7 CYRILLIC SMALL LETTER CHE WITH DESCENDER
  , (0x010004b8, "Cyrillic_CHE_vertstroke", AChar '\x04b8', True) -- U+04B8 CYRILLIC CAPITAL LETTER CHE WITH VERTICAL STROKE
  , (0x010004b9, "Cyrillic_che_vertstroke", AChar '\x04b9', True) -- U+04B9 CYRILLIC SMALL LETTER CHE WITH VERTICAL STROKE
  , (0x010004ba, "Cyrillic_SHHA", AChar '\x04ba', True) -- U+04BA CYRILLIC CAPITAL LETTER SHHA
  , (0x010004bb, "Cyrillic_shha", AChar '\x04bb', True) -- U+04BB CYRILLIC SMALL LETTER SHHA
  , (0x010004d8, "Cyrillic_SCHWA", AChar '\x04d8', True) -- U+04D8 CYRILLIC CAPITAL LETTER SCHWA
  , (0x010004d9, "Cyrillic_schwa", AChar '\x04d9', True) -- U+04D9 CYRILLIC SMALL LETTER SCHWA
  , (0x010004e2, "Cyrillic_I_macron", AChar '\x04e2', True) -- U+04E2 CYRILLIC CAPITAL LETTER I WITH MACRON
  , (0x010004e3, "Cyrillic_i_macron", AChar '\x04e3', True) -- U+04E3 CYRILLIC SMALL LETTER I WITH MACRON
  , (0x010004e8, "Cyrillic_O_bar", AChar '\x04e8', True) -- U+04E8 CYRILLIC CAPITAL LETTER BARRED O
  , (0x010004e9, "Cyrillic_o_bar", AChar '\x04e9', True) -- U+04E9 CYRILLIC SMALL LETTER BARRED O
  , (0x010004ee, "Cyrillic_U_macron", AChar '\x04ee', True) -- U+04EE CYRILLIC CAPITAL LETTER U WITH MACRON
  , (0x010004ef, "Cyrillic_u_macron", AChar '\x04ef', True) -- U+04EF CYRILLIC SMALL LETTER U WITH MACRON
  , (0x000006a1, "Serbian_dje", AChar '\x0452', True) -- U+0452 CYRILLIC SMALL LETTER DJE
  , (0x000006a2, "Macedonia_gje", AChar '\x0453', True) -- U+0453 CYRILLIC SMALL LETTER GJE
  , (0x000006a3, "Cyrillic_io", AChar '\x0451', True) -- U+0451 CYRILLIC SMALL LETTER IO
  , (0x000006a4, "Ukrainian_ie", AChar '\x0454', True) -- U+0454 CYRILLIC SMALL LETTER UKRAINIAN IE
  , (0x000006a4, "Ukranian_je", AChar '\x0454', False) -- U+0454 CYRILLIC SMALL LETTER UKRAINIAN IE [NOTE] deprecated keysym
  , (0x000006a5, "Macedonia_dse", AChar '\x0455', True) -- U+0455 CYRILLIC SMALL LETTER DZE
  , (0x000006a6, "Ukrainian_i", AChar '\x0456', True) -- U+0456 CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I
  , (0x000006a6, "Ukranian_i", AChar '\x0456', False) -- U+0456 CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I [NOTE] deprecated keysym
  , (0x000006a7, "Ukrainian_yi", AChar '\x0457', True) -- U+0457 CYRILLIC SMALL LETTER YI
  , (0x000006a7, "Ukranian_yi", AChar '\x0457', False) -- U+0457 CYRILLIC SMALL LETTER YI [NOTE] deprecated keysym
  , (0x000006a8, "Cyrillic_je", AChar '\x0458', True) -- U+0458 CYRILLIC SMALL LETTER JE
  , (0x000006a8, "Serbian_je", AChar '\x0458', False) -- U+0458 CYRILLIC SMALL LETTER JE [NOTE] deprecated keysym
  , (0x000006a9, "Cyrillic_lje", AChar '\x0459', True) -- U+0459 CYRILLIC SMALL LETTER LJE
  , (0x000006a9, "Serbian_lje", AChar '\x0459', False) -- U+0459 CYRILLIC SMALL LETTER LJE [NOTE] deprecated keysym
  , (0x000006aa, "Cyrillic_nje", AChar '\x045a', True) -- U+045A CYRILLIC SMALL LETTER NJE
  , (0x000006aa, "Serbian_nje", AChar '\x045a', False) -- U+045A CYRILLIC SMALL LETTER NJE [NOTE] deprecated keysym
  , (0x000006ab, "Serbian_tshe", AChar '\x045b', True) -- U+045B CYRILLIC SMALL LETTER TSHE
  , (0x000006ac, "Macedonia_kje", AChar '\x045c', True) -- U+045C CYRILLIC SMALL LETTER KJE
  , (0x000006ad, "Ukrainian_ghe_with_upturn", AChar '\x0491', True) -- U+0491 CYRILLIC SMALL LETTER GHE WITH UPTURN
  , (0x000006ae, "Byelorussian_shortu", AChar '\x045e', True) -- U+045E CYRILLIC SMALL LETTER SHORT U
  , (0x000006af, "Cyrillic_dzhe", AChar '\x045f', True) -- U+045F CYRILLIC SMALL LETTER DZHE
  , (0x000006af, "Serbian_dze", AChar '\x045f', False) -- U+045F CYRILLIC SMALL LETTER DZHE [NOTE] deprecated keysym
  , (0x000006b0, "numerosign", AChar '\x2116', True) -- U+2116 NUMERO SIGN
  , (0x000006b1, "Serbian_DJE", AChar '\x0402', True) -- U+0402 CYRILLIC CAPITAL LETTER DJE
  , (0x000006b2, "Macedonia_GJE", AChar '\x0403', True) -- U+0403 CYRILLIC CAPITAL LETTER GJE
  , (0x000006b3, "Cyrillic_IO", AChar '\x0401', True) -- U+0401 CYRILLIC CAPITAL LETTER IO
  , (0x000006b4, "Ukrainian_IE", AChar '\x0404', True) -- U+0404 CYRILLIC CAPITAL LETTER UKRAINIAN IE
  , (0x000006b4, "Ukranian_JE", AChar '\x0404', False) -- U+0404 CYRILLIC CAPITAL LETTER UKRAINIAN IE [NOTE] deprecated keysym
  , (0x000006b5, "Macedonia_DSE", AChar '\x0405', True) -- U+0405 CYRILLIC CAPITAL LETTER DZE
  , (0x000006b6, "Ukrainian_I", AChar '\x0406', True) -- U+0406 CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I
  , (0x000006b6, "Ukranian_I", AChar '\x0406', False) -- U+0406 CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I [NOTE] deprecated keysym
  , (0x000006b7, "Ukrainian_YI", AChar '\x0407', True) -- U+0407 CYRILLIC CAPITAL LETTER YI
  , (0x000006b7, "Ukranian_YI", AChar '\x0407', False) -- U+0407 CYRILLIC CAPITAL LETTER YI [NOTE] deprecated keysym
  , (0x000006b8, "Cyrillic_JE", AChar '\x0408', True) -- U+0408 CYRILLIC CAPITAL LETTER JE
  , (0x000006b8, "Serbian_JE", AChar '\x0408', False) -- U+0408 CYRILLIC CAPITAL LETTER JE [NOTE] deprecated keysym
  , (0x000006b9, "Cyrillic_LJE", AChar '\x0409', True) -- U+0409 CYRILLIC CAPITAL LETTER LJE
  , (0x000006b9, "Serbian_LJE", AChar '\x0409', False) -- U+0409 CYRILLIC CAPITAL LETTER LJE [NOTE] deprecated keysym
  , (0x000006ba, "Cyrillic_NJE", AChar '\x040a', True) -- U+040A CYRILLIC CAPITAL LETTER NJE
  , (0x000006ba, "Serbian_NJE", AChar '\x040a', False) -- U+040A CYRILLIC CAPITAL LETTER NJE [NOTE] deprecated keysym
  , (0x000006bb, "Serbian_TSHE", AChar '\x040b', True) -- U+040B CYRILLIC CAPITAL LETTER TSHE
  , (0x000006bc, "Macedonia_KJE", AChar '\x040c', True) -- U+040C CYRILLIC CAPITAL LETTER KJE
  , (0x000006bd, "Ukrainian_GHE_WITH_UPTURN", AChar '\x0490', True) -- U+0490 CYRILLIC CAPITAL LETTER GHE WITH UPTURN
  , (0x000006be, "Byelorussian_SHORTU", AChar '\x040e', True) -- U+040E CYRILLIC CAPITAL LETTER SHORT U
  , (0x000006bf, "Cyrillic_DZHE", AChar '\x040f', True) -- U+040F CYRILLIC CAPITAL LETTER DZHE
  , (0x000006bf, "Serbian_DZE", AChar '\x040f', False) -- U+040F CYRILLIC CAPITAL LETTER DZHE [NOTE] deprecated keysym
  , (0x000006c0, "Cyrillic_yu", AChar '\x044e', True) -- U+044E CYRILLIC SMALL LETTER YU
  , (0x000006c1, "Cyrillic_a", AChar '\x0430', True) -- U+0430 CYRILLIC SMALL LETTER A
  , (0x000006c2, "Cyrillic_be", AChar '\x0431', True) -- U+0431 CYRILLIC SMALL LETTER BE
  , (0x000006c3, "Cyrillic_tse", AChar '\x0446', True) -- U+0446 CYRILLIC SMALL LETTER TSE
  , (0x000006c4, "Cyrillic_de", AChar '\x0434', True) -- U+0434 CYRILLIC SMALL LETTER DE
  , (0x000006c5, "Cyrillic_ie", AChar '\x0435', True) -- U+0435 CYRILLIC SMALL LETTER IE
  , (0x000006c6, "Cyrillic_ef", AChar '\x0444', True) -- U+0444 CYRILLIC SMALL LETTER EF
  , (0x000006c7, "Cyrillic_ghe", AChar '\x0433', True) -- U+0433 CYRILLIC SMALL LETTER GHE
  , (0x000006c8, "Cyrillic_ha", AChar '\x0445', True) -- U+0445 CYRILLIC SMALL LETTER HA
  , (0x000006c9, "Cyrillic_i", AChar '\x0438', True) -- U+0438 CYRILLIC SMALL LETTER I
  , (0x000006ca, "Cyrillic_shorti", AChar '\x0439', True) -- U+0439 CYRILLIC SMALL LETTER SHORT I
  , (0x000006cb, "Cyrillic_ka", AChar '\x043a', True) -- U+043A CYRILLIC SMALL LETTER KA
  , (0x000006cc, "Cyrillic_el", AChar '\x043b', True) -- U+043B CYRILLIC SMALL LETTER EL
  , (0x000006cd, "Cyrillic_em", AChar '\x043c', True) -- U+043C CYRILLIC SMALL LETTER EM
  , (0x000006ce, "Cyrillic_en", AChar '\x043d', True) -- U+043D CYRILLIC SMALL LETTER EN
  , (0x000006cf, "Cyrillic_o", AChar '\x043e', True) -- U+043E CYRILLIC SMALL LETTER O
  , (0x000006d0, "Cyrillic_pe", AChar '\x043f', True) -- U+043F CYRILLIC SMALL LETTER PE
  , (0x000006d1, "Cyrillic_ya", AChar '\x044f', True) -- U+044F CYRILLIC SMALL LETTER YA
  , (0x000006d2, "Cyrillic_er", AChar '\x0440', True) -- U+0440 CYRILLIC SMALL LETTER ER
  , (0x000006d3, "Cyrillic_es", AChar '\x0441', True) -- U+0441 CYRILLIC SMALL LETTER ES
  , (0x000006d4, "Cyrillic_te", AChar '\x0442', True) -- U+0442 CYRILLIC SMALL LETTER TE
  , (0x000006d5, "Cyrillic_u", AChar '\x0443', True) -- U+0443 CYRILLIC SMALL LETTER U
  , (0x000006d6, "Cyrillic_zhe", AChar '\x0436', True) -- U+0436 CYRILLIC SMALL LETTER ZHE
  , (0x000006d7, "Cyrillic_ve", AChar '\x0432', True) -- U+0432 CYRILLIC SMALL LETTER VE
  , (0x000006d8, "Cyrillic_softsign", AChar '\x044c', True) -- U+044C CYRILLIC SMALL LETTER SOFT SIGN
  , (0x000006d9, "Cyrillic_yeru", AChar '\x044b', True) -- U+044B CYRILLIC SMALL LETTER YERU
  , (0x000006da, "Cyrillic_ze", AChar '\x0437', True) -- U+0437 CYRILLIC SMALL LETTER ZE
  , (0x000006db, "Cyrillic_sha", AChar '\x0448', True) -- U+0448 CYRILLIC SMALL LETTER SHA
  , (0x000006dc, "Cyrillic_e", AChar '\x044d', True) -- U+044D CYRILLIC SMALL LETTER E
  , (0x000006dd, "Cyrillic_shcha", AChar '\x0449', True) -- U+0449 CYRILLIC SMALL LETTER SHCHA
  , (0x000006de, "Cyrillic_che", AChar '\x0447', True) -- U+0447 CYRILLIC SMALL LETTER CHE
  , (0x000006df, "Cyrillic_hardsign", AChar '\x044a', True) -- U+044A CYRILLIC SMALL LETTER HARD SIGN
  , (0x000006e0, "Cyrillic_YU", AChar '\x042e', True) -- U+042E CYRILLIC CAPITAL LETTER YU
  , (0x000006e1, "Cyrillic_A", AChar '\x0410', True) -- U+0410 CYRILLIC CAPITAL LETTER A
  , (0x000006e2, "Cyrillic_BE", AChar '\x0411', True) -- U+0411 CYRILLIC CAPITAL LETTER BE
  , (0x000006e3, "Cyrillic_TSE", AChar '\x0426', True) -- U+0426 CYRILLIC CAPITAL LETTER TSE
  , (0x000006e4, "Cyrillic_DE", AChar '\x0414', True) -- U+0414 CYRILLIC CAPITAL LETTER DE
  , (0x000006e5, "Cyrillic_IE", AChar '\x0415', True) -- U+0415 CYRILLIC CAPITAL LETTER IE
  , (0x000006e6, "Cyrillic_EF", AChar '\x0424', True) -- U+0424 CYRILLIC CAPITAL LETTER EF
  , (0x000006e7, "Cyrillic_GHE", AChar '\x0413', True) -- U+0413 CYRILLIC CAPITAL LETTER GHE
  , (0x000006e8, "Cyrillic_HA", AChar '\x0425', True) -- U+0425 CYRILLIC CAPITAL LETTER HA
  , (0x000006e9, "Cyrillic_I", AChar '\x0418', True) -- U+0418 CYRILLIC CAPITAL LETTER I
  , (0x000006ea, "Cyrillic_SHORTI", AChar '\x0419', True) -- U+0419 CYRILLIC CAPITAL LETTER SHORT I
  , (0x000006eb, "Cyrillic_KA", AChar '\x041a', True) -- U+041A CYRILLIC CAPITAL LETTER KA
  , (0x000006ec, "Cyrillic_EL", AChar '\x041b', True) -- U+041B CYRILLIC CAPITAL LETTER EL
  , (0x000006ed, "Cyrillic_EM", AChar '\x041c', True) -- U+041C CYRILLIC CAPITAL LETTER EM
  , (0x000006ee, "Cyrillic_EN", AChar '\x041d', True) -- U+041D CYRILLIC CAPITAL LETTER EN
  , (0x000006ef, "Cyrillic_O", AChar '\x041e', True) -- U+041E CYRILLIC CAPITAL LETTER O
  , (0x000006f0, "Cyrillic_PE", AChar '\x041f', True) -- U+041F CYRILLIC CAPITAL LETTER PE
  , (0x000006f1, "Cyrillic_YA", AChar '\x042f', True) -- U+042F CYRILLIC CAPITAL LETTER YA
  , (0x000006f2, "Cyrillic_ER", AChar '\x0420', True) -- U+0420 CYRILLIC CAPITAL LETTER ER
  , (0x000006f3, "Cyrillic_ES", AChar '\x0421', True) -- U+0421 CYRILLIC CAPITAL LETTER ES
  , (0x000006f4, "Cyrillic_TE", AChar '\x0422', True) -- U+0422 CYRILLIC CAPITAL LETTER TE
  , (0x000006f5, "Cyrillic_U", AChar '\x0423', True) -- U+0423 CYRILLIC CAPITAL LETTER U
  , (0x000006f6, "Cyrillic_ZHE", AChar '\x0416', True) -- U+0416 CYRILLIC CAPITAL LETTER ZHE
  , (0x000006f7, "Cyrillic_VE", AChar '\x0412', True) -- U+0412 CYRILLIC CAPITAL LETTER VE
  , (0x000006f8, "Cyrillic_SOFTSIGN", AChar '\x042c', True) -- U+042C CYRILLIC CAPITAL LETTER SOFT SIGN
  , (0x000006f9, "Cyrillic_YERU", AChar '\x042b', True) -- U+042B CYRILLIC CAPITAL LETTER YERU
  , (0x000006fa, "Cyrillic_ZE", AChar '\x0417', True) -- U+0417 CYRILLIC CAPITAL LETTER ZE
  , (0x000006fb, "Cyrillic_SHA", AChar '\x0428', True) -- U+0428 CYRILLIC CAPITAL LETTER SHA
  , (0x000006fc, "Cyrillic_E", AChar '\x042d', True) -- U+042D CYRILLIC CAPITAL LETTER E
  , (0x000006fd, "Cyrillic_SHCHA", AChar '\x0429', True) -- U+0429 CYRILLIC CAPITAL LETTER SHCHA
  , (0x000006fe, "Cyrillic_CHE", AChar '\x0427', True) -- U+0427 CYRILLIC CAPITAL LETTER CHE
  , (0x000006ff, "Cyrillic_HARDSIGN", AChar '\x042a', True) -- U+042A CYRILLIC CAPITAL LETTER HARD SIGN
  --
  -- Greek
  --
  , (0x000007a1, "Greek_ALPHAaccent", AChar '\x0386', True) -- U+0386 GREEK CAPITAL LETTER ALPHA WITH TONOS
  , (0x000007a2, "Greek_EPSILONaccent", AChar '\x0388', True) -- U+0388 GREEK CAPITAL LETTER EPSILON WITH TONOS
  , (0x000007a3, "Greek_ETAaccent", AChar '\x0389', True) -- U+0389 GREEK CAPITAL LETTER ETA WITH TONOS
  , (0x000007a4, "Greek_IOTAaccent", AChar '\x038a', True) -- U+038A GREEK CAPITAL LETTER IOTA WITH TONOS
  , (0x000007a5, "Greek_IOTAdieresis", AChar '\x03aa', True) -- U+03AA GREEK CAPITAL LETTER IOTA WITH DIALYTIKA
  , (0x000007a5, "Greek_IOTAdiaeresis", AChar '\x03aa', False) -- U+03AA GREEK CAPITAL LETTER IOTA WITH DIALYTIKA
  , (0x000007a7, "Greek_OMICRONaccent", AChar '\x038c', True) -- U+038C GREEK CAPITAL LETTER OMICRON WITH TONOS
  , (0x000007a8, "Greek_UPSILONaccent", AChar '\x038e', True) -- U+038E GREEK CAPITAL LETTER UPSILON WITH TONOS
  , (0x000007a9, "Greek_UPSILONdieresis", AChar '\x03ab', True) -- U+03AB GREEK CAPITAL LETTER UPSILON WITH DIALYTIKA
  , (0x000007ab, "Greek_OMEGAaccent", AChar '\x038f', True) -- U+038F GREEK CAPITAL LETTER OMEGA WITH TONOS
  , (0x000007ae, "Greek_accentdieresis", AChar '\x0385', True) -- U+0385 GREEK DIALYTIKA TONOS
  , (0x000007af, "Greek_horizbar", AChar '\x2015', True) -- U+2015 HORIZONTAL BAR
  , (0x000007b1, "Greek_alphaaccent", AChar '\x03ac', True) -- U+03AC GREEK SMALL LETTER ALPHA WITH TONOS
  , (0x000007b2, "Greek_epsilonaccent", AChar '\x03ad', True) -- U+03AD GREEK SMALL LETTER EPSILON WITH TONOS
  , (0x000007b3, "Greek_etaaccent", AChar '\x03ae', True) -- U+03AE GREEK SMALL LETTER ETA WITH TONOS
  , (0x000007b4, "Greek_iotaaccent", AChar '\x03af', True) -- U+03AF GREEK SMALL LETTER IOTA WITH TONOS
  , (0x000007b5, "Greek_iotadieresis", AChar '\x03ca', True) -- U+03CA GREEK SMALL LETTER IOTA WITH DIALYTIKA
  , (0x000007b6, "Greek_iotaaccentdieresis", AChar '\x0390', True) -- U+0390 GREEK SMALL LETTER IOTA WITH DIALYTIKA AND TONOS
  , (0x000007b7, "Greek_omicronaccent", AChar '\x03cc', True) -- U+03CC GREEK SMALL LETTER OMICRON WITH TONOS
  , (0x000007b8, "Greek_upsilonaccent", AChar '\x03cd', True) -- U+03CD GREEK SMALL LETTER UPSILON WITH TONOS
  , (0x000007b9, "Greek_upsilondieresis", AChar '\x03cb', True) -- U+03CB GREEK SMALL LETTER UPSILON WITH DIALYTIKA
  , (0x000007ba, "Greek_upsilonaccentdieresis", AChar '\x03b0', True) -- U+03B0 GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS
  , (0x000007bb, "Greek_omegaaccent", AChar '\x03ce', True) -- U+03CE GREEK SMALL LETTER OMEGA WITH TONOS
  , (0x000007c1, "Greek_ALPHA", AChar '\x0391', True) -- U+0391 GREEK CAPITAL LETTER ALPHA
  , (0x000007c2, "Greek_BETA", AChar '\x0392', True) -- U+0392 GREEK CAPITAL LETTER BETA
  , (0x000007c3, "Greek_GAMMA", AChar '\x0393', True) -- U+0393 GREEK CAPITAL LETTER GAMMA
  , (0x000007c4, "Greek_DELTA", AChar '\x0394', True) -- U+0394 GREEK CAPITAL LETTER DELTA
  , (0x000007c5, "Greek_EPSILON", AChar '\x0395', True) -- U+0395 GREEK CAPITAL LETTER EPSILON
  , (0x000007c6, "Greek_ZETA", AChar '\x0396', True) -- U+0396 GREEK CAPITAL LETTER ZETA
  , (0x000007c7, "Greek_ETA", AChar '\x0397', True) -- U+0397 GREEK CAPITAL LETTER ETA
  , (0x000007c8, "Greek_THETA", AChar '\x0398', True) -- U+0398 GREEK CAPITAL LETTER THETA
  , (0x000007c9, "Greek_IOTA", AChar '\x0399', True) -- U+0399 GREEK CAPITAL LETTER IOTA
  , (0x000007ca, "Greek_KAPPA", AChar '\x039a', True) -- U+039A GREEK CAPITAL LETTER KAPPA
  , (0x000007cb, "Greek_LAMDA", AChar '\x039b', True) -- U+039B GREEK CAPITAL LETTER LAMDA
  , (0x000007cb, "Greek_LAMBDA", AChar '\x039b', False) -- U+039B GREEK CAPITAL LETTER LAMDA
  , (0x000007cc, "Greek_MU", AChar '\x039c', True) -- U+039C GREEK CAPITAL LETTER MU
  , (0x000007cd, "Greek_NU", AChar '\x039d', True) -- U+039D GREEK CAPITAL LETTER NU
  , (0x000007ce, "Greek_XI", AChar '\x039e', True) -- U+039E GREEK CAPITAL LETTER XI
  , (0x000007cf, "Greek_OMICRON", AChar '\x039f', True) -- U+039F GREEK CAPITAL LETTER OMICRON
  , (0x000007d0, "Greek_PI", AChar '\x03a0', True) -- U+03A0 GREEK CAPITAL LETTER PI
  , (0x000007d1, "Greek_RHO", AChar '\x03a1', True) -- U+03A1 GREEK CAPITAL LETTER RHO
  , (0x000007d2, "Greek_SIGMA", AChar '\x03a3', True) -- U+03A3 GREEK CAPITAL LETTER SIGMA
  , (0x000007d4, "Greek_TAU", AChar '\x03a4', True) -- U+03A4 GREEK CAPITAL LETTER TAU
  , (0x000007d5, "Greek_UPSILON", AChar '\x03a5', True) -- U+03A5 GREEK CAPITAL LETTER UPSILON
  , (0x000007d6, "Greek_PHI", AChar '\x03a6', True) -- U+03A6 GREEK CAPITAL LETTER PHI
  , (0x000007d7, "Greek_CHI", AChar '\x03a7', True) -- U+03A7 GREEK CAPITAL LETTER CHI
  , (0x000007d8, "Greek_PSI", AChar '\x03a8', True) -- U+03A8 GREEK CAPITAL LETTER PSI
  , (0x000007d9, "Greek_OMEGA", AChar '\x03a9', True) -- U+03A9 GREEK CAPITAL LETTER OMEGA
  , (0x000007e1, "Greek_alpha", AChar '\x03b1', True) -- U+03B1 GREEK SMALL LETTER ALPHA
  , (0x000007e2, "Greek_beta", AChar '\x03b2', True) -- U+03B2 GREEK SMALL LETTER BETA
  , (0x000007e3, "Greek_gamma", AChar '\x03b3', True) -- U+03B3 GREEK SMALL LETTER GAMMA
  , (0x000007e4, "Greek_delta", AChar '\x03b4', True) -- U+03B4 GREEK SMALL LETTER DELTA
  , (0x000007e5, "Greek_epsilon", AChar '\x03b5', True) -- U+03B5 GREEK SMALL LETTER EPSILON
  , (0x000007e6, "Greek_zeta", AChar '\x03b6', True) -- U+03B6 GREEK SMALL LETTER ZETA
  , (0x000007e7, "Greek_eta", AChar '\x03b7', True) -- U+03B7 GREEK SMALL LETTER ETA
  , (0x000007e8, "Greek_theta", AChar '\x03b8', True) -- U+03B8 GREEK SMALL LETTER THETA
  , (0x000007e9, "Greek_iota", AChar '\x03b9', True) -- U+03B9 GREEK SMALL LETTER IOTA
  , (0x000007ea, "Greek_kappa", AChar '\x03ba', True) -- U+03BA GREEK SMALL LETTER KAPPA
  , (0x000007eb, "Greek_lamda", AChar '\x03bb', True) -- U+03BB GREEK SMALL LETTER LAMDA
  , (0x000007eb, "Greek_lambda", AChar '\x03bb', False) -- U+03BB GREEK SMALL LETTER LAMDA
  , (0x000007ec, "Greek_mu", AChar '\x03bc', True) -- U+03BC GREEK SMALL LETTER MU
  , (0x000007ed, "Greek_nu", AChar '\x03bd', True) -- U+03BD GREEK SMALL LETTER NU
  , (0x000007ee, "Greek_xi", AChar '\x03be', True) -- U+03BE GREEK SMALL LETTER XI
  , (0x000007ef, "Greek_omicron", AChar '\x03bf', True) -- U+03BF GREEK SMALL LETTER OMICRON
  , (0x000007f0, "Greek_pi", AChar '\x03c0', True) -- U+03C0 GREEK SMALL LETTER PI
  , (0x000007f1, "Greek_rho", AChar '\x03c1', True) -- U+03C1 GREEK SMALL LETTER RHO
  , (0x000007f2, "Greek_sigma", AChar '\x03c3', True) -- U+03C3 GREEK SMALL LETTER SIGMA
  , (0x000007f3, "Greek_finalsmallsigma", AChar '\x03c2', True) -- U+03C2 GREEK SMALL LETTER FINAL SIGMA
  , (0x000007f4, "Greek_tau", AChar '\x03c4', True) -- U+03C4 GREEK SMALL LETTER TAU
  , (0x000007f5, "Greek_upsilon", AChar '\x03c5', True) -- U+03C5 GREEK SMALL LETTER UPSILON
  , (0x000007f6, "Greek_phi", AChar '\x03c6', True) -- U+03C6 GREEK SMALL LETTER PHI
  , (0x000007f7, "Greek_chi", AChar '\x03c7', True) -- U+03C7 GREEK SMALL LETTER CHI
  , (0x000007f8, "Greek_psi", AChar '\x03c8', True) -- U+03C8 GREEK SMALL LETTER PSI
  , (0x000007f9, "Greek_omega", AChar '\x03c9', True) -- U+03C9 GREEK SMALL LETTER OMEGA
  , (0x0000ff7e, "Greek_switch", ASpecial Iso_Group_Shift, False) -- Alias for mode_switch
  --
  -- Technical (from the DEC VT330/VT420 Technical Character Set, http://vt100.net/charsets/technical.html)
  --
  , (0x000008a1, "leftradical", AChar '\x23b7', True) -- U+23B7 RADICAL SYMBOL BOTTOM
  , (0x000008a2, "topleftradical", AChar '\x250c', False) -- U+250C BOX DRAWINGS LIGHT DOWN AND RIGHT [NOTE] deprecated keysym
  , (0x000008a3, "horizconnector", AChar '\x2500', False) -- U+2500 BOX DRAWINGS LIGHT HORIZONTAL [NOTE] deprecated keysym
  , (0x000008a4, "topintegral", AChar '\x2320', True) -- U+2320 TOP HALF INTEGRAL
  , (0x000008a5, "botintegral", AChar '\x2321', True) -- U+2321 BOTTOM HALF INTEGRAL
  , (0x000008a6, "vertconnector", AChar '\x2502', False) -- U+2502 BOX DRAWINGS LIGHT VERTICAL [NOTE] deprecated keysym
  , (0x000008a7, "topleftsqbracket", AChar '\x23a1', True) -- U+23A1 LEFT SQUARE BRACKET UPPER CORNER
  , (0x000008a8, "botleftsqbracket", AChar '\x23a3', True) -- U+23A3 LEFT SQUARE BRACKET LOWER CORNER
  , (0x000008a9, "toprightsqbracket", AChar '\x23a4', True) -- U+23A4 RIGHT SQUARE BRACKET UPPER CORNER
  , (0x000008aa, "botrightsqbracket", AChar '\x23a6', True) -- U+23A6 RIGHT SQUARE BRACKET LOWER CORNER
  , (0x000008ab, "topleftparens", AChar '\x239b', True) -- U+239B LEFT PARENTHESIS UPPER HOOK
  , (0x000008ac, "botleftparens", AChar '\x239d', True) -- U+239D LEFT PARENTHESIS LOWER HOOK
  , (0x000008ad, "toprightparens", AChar '\x239e', True) -- U+239E RIGHT PARENTHESIS UPPER HOOK
  , (0x000008ae, "botrightparens", AChar '\x23a0', True) -- U+23A0 RIGHT PARENTHESIS LOWER HOOK
  , (0x000008af, "leftmiddlecurlybrace", AChar '\x23a8', True) -- U+23A8 LEFT CURLY BRACKET MIDDLE PIECE
  , (0x000008b0, "rightmiddlecurlybrace", AChar '\x23ac', True) -- U+23AC RIGHT CURLY BRACKET MIDDLE PIECE
  , (0x000008b1, "topleftsummation", ASpecial Topleftsummation, True)
  , (0x000008b2, "botleftsummation", ASpecial Botleftsummation, True)
  , (0x000008b3, "topvertsummationconnector", ASpecial Topvertsummationconnector, True)
  , (0x000008b4, "botvertsummationconnector", ASpecial Botvertsummationconnector, True)
  , (0x000008b5, "toprightsummation", ASpecial Toprightsummation, True)
  , (0x000008b6, "botrightsummation", ASpecial Botrightsummation, True)
  , (0x000008b7, "rightmiddlesummation", ASpecial Rightmiddlesummation, True)
  , (0x000008bc, "lessthanequal", AChar '\x2264', True) -- U+2264 LESS-THAN OR EQUAL TO
  , (0x000008bd, "notequal", AChar '\x2260', True) -- U+2260 NOT EQUAL TO
  , (0x000008be, "greaterthanequal", AChar '\x2265', True) -- U+2265 GREATER-THAN OR EQUAL TO
  , (0x000008bf, "integral", AChar '\x222b', True) -- U+222B INTEGRAL
  , (0x000008c0, "therefore", AChar '\x2234', True) -- U+2234 THEREFORE
  , (0x000008c1, "variation", AChar '\x221d', True) -- U+221D PROPORTIONAL TO
  , (0x000008c2, "infinity", AChar '\x221e', True) -- U+221E INFINITY
  , (0x000008c5, "nabla", AChar '\x2207', True) -- U+2207 NABLA
  , (0x000008c8, "approximate", AChar '\x223c', True) -- U+223C TILDE OPERATOR
  , (0x000008c9, "similarequal", AChar '\x2243', True) -- U+2243 ASYMPTOTICALLY EQUAL TO
  , (0x000008cd, "ifonlyif", AChar '\x21d4', True) -- U+21D4 LEFT RIGHT DOUBLE ARROW
  , (0x000008ce, "implies", AChar '\x21d2', True) -- U+21D2 RIGHTWARDS DOUBLE ARROW
  , (0x000008cf, "identical", AChar '\x2261', True) -- U+2261 IDENTICAL TO
  , (0x000008d6, "radical", AChar '\x221a', True) -- U+221A SQUARE ROOT
  , (0x000008da, "includedin", AChar '\x2282', True) -- U+2282 SUBSET OF
  , (0x000008db, "includes", AChar '\x2283', True) -- U+2283 SUPERSET OF
  , (0x000008dc, "intersection", AChar '\x2229', True) -- U+2229 INTERSECTION
  , (0x000008dd, "union", AChar '\x222a', True) -- U+222A UNION
  , (0x000008de, "logicaland", AChar '\x2227', True) -- U+2227 LOGICAL AND
  , (0x000008df, "logicalor", AChar '\x2228', True) -- U+2228 LOGICAL OR
  , (0x000008ef, "partialderivative", AChar '\x2202', True) -- U+2202 PARTIAL DIFFERENTIAL
  , (0x000008f6, "function", AChar '\x0192', True) -- U+0192 LATIN SMALL LETTER F WITH HOOK
  , (0x000008fb, "leftarrow", AChar '\x2190', True) -- U+2190 LEFTWARDS ARROW
  , (0x000008fc, "uparrow", AChar '\x2191', True) -- U+2191 UPWARDS ARROW
  , (0x000008fd, "rightarrow", AChar '\x2192', True) -- U+2192 RIGHTWARDS ARROW
  , (0x000008fe, "downarrow", AChar '\x2193', True) -- U+2193 DOWNWARDS ARROW
  --
  -- Special (from the DEC VT100 Special Graphics Character Set)
  --
  , (0x000009df, "blank", ASpecial Blank, True)
  , (0x000009e0, "soliddiamond", AChar '\x25c6', True) -- U+25C6 BLACK DIAMOND
  , (0x000009e1, "checkerboard", AChar '\x2592', True) -- U+2592 MEDIUM SHADE
  , (0x000009e2, "ht", AChar '\x2409', True) -- U+2409 SYMBOL FOR HORIZONTAL TABULATION
  , (0x000009e3, "ff", AChar '\x240c', True) -- U+240C SYMBOL FOR FORM FEED
  , (0x000009e4, "cr", AChar '\x240d', True) -- U+240D SYMBOL FOR CARRIAGE RETURN
  , (0x000009e5, "lf", AChar '\x240a', True) -- U+240A SYMBOL FOR LINE FEED
  , (0x000009e8, "nl", AChar '\x2424', True) -- U+2424 SYMBOL FOR NEWLINE
  , (0x000009e9, "vt", AChar '\x240b', True) -- U+240B SYMBOL FOR VERTICAL TABULATION
  , (0x000009ea, "lowrightcorner", AChar '\x2518', True) -- U+2518 BOX DRAWINGS LIGHT UP AND LEFT
  , (0x000009eb, "uprightcorner", AChar '\x2510', True) -- U+2510 BOX DRAWINGS LIGHT DOWN AND LEFT
  , (0x000009ec, "upleftcorner", AChar '\x250c', True) -- U+250C BOX DRAWINGS LIGHT DOWN AND RIGHT
  , (0x000009ed, "lowleftcorner", AChar '\x2514', True) -- U+2514 BOX DRAWINGS LIGHT UP AND RIGHT
  , (0x000009ee, "crossinglines", AChar '\x253c', True) -- U+253C BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
  , (0x000009ef, "horizlinescan1", AChar '\x23ba', True) -- U+23BA HORIZONTAL SCAN LINE-1
  , (0x000009f0, "horizlinescan3", AChar '\x23bb', True) -- U+23BB HORIZONTAL SCAN LINE-3
  , (0x000009f1, "horizlinescan5", AChar '\x2500', True) -- U+2500 BOX DRAWINGS LIGHT HORIZONTAL
  , (0x000009f2, "horizlinescan7", AChar '\x23bc', True) -- U+23BC HORIZONTAL SCAN LINE-7
  , (0x000009f3, "horizlinescan9", AChar '\x23bd', True) -- U+23BD HORIZONTAL SCAN LINE-9
  , (0x000009f4, "leftt", AChar '\x251c', True) -- U+251C BOX DRAWINGS LIGHT VERTICAL AND RIGHT
  , (0x000009f5, "rightt", AChar '\x2524', True) -- U+2524 BOX DRAWINGS LIGHT VERTICAL AND LEFT
  , (0x000009f6, "bott", AChar '\x2534', True) -- U+2534 BOX DRAWINGS LIGHT UP AND HORIZONTAL
  , (0x000009f7, "topt", AChar '\x252c', True) -- U+252C BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
  , (0x000009f8, "vertbar", AChar '\x2502', True) -- U+2502 BOX DRAWINGS LIGHT VERTICAL
  --
  -- Publishing (these are probably from a long forgotten DEC Publishing font that once shipped with DECwrite)
  --
  , (0x00000aa1, "emspace", AChar '\x2003', True) -- U+2003 EM SPACE
  , (0x00000aa2, "enspace", AChar '\x2002', True) -- U+2002 EN SPACE
  , (0x00000aa3, "em3space", AChar '\x2004', True) -- U+2004 THREE-PER-EM SPACE
  , (0x00000aa4, "em4space", AChar '\x2005', True) -- U+2005 FOUR-PER-EM SPACE
  , (0x00000aa5, "digitspace", AChar '\x2007', True) -- U+2007 FIGURE SPACE
  , (0x00000aa6, "punctspace", AChar '\x2008', True) -- U+2008 PUNCTUATION SPACE
  , (0x00000aa7, "thinspace", AChar '\x2009', True) -- U+2009 THIN SPACE
  , (0x00000aa8, "hairspace", AChar '\x200a', True) -- U+200A HAIR SPACE
  , (0x00000aa9, "emdash", AChar '\x2014', True) -- U+2014 EM DASH
  , (0x00000aaa, "endash", AChar '\x2013', True) -- U+2013 EN DASH
  , (0x00000aac, "signifblank", AChar '\x2423', False) -- U+2423 OPEN BOX [NOTE] deprecated keysym
  , (0x00000aae, "ellipsis", AChar '\x2026', True) -- U+2026 HORIZONTAL ELLIPSIS
  , (0x00000aaf, "doubbaselinedot", AChar '\x2025', True) -- U+2025 TWO DOT LEADER
  , (0x00000ab0, "onethird", AChar '\x2153', True) -- U+2153 VULGAR FRACTION ONE THIRD
  , (0x00000ab1, "twothirds", AChar '\x2154', True) -- U+2154 VULGAR FRACTION TWO THIRDS
  , (0x00000ab2, "onefifth", AChar '\x2155', True) -- U+2155 VULGAR FRACTION ONE FIFTH
  , (0x00000ab3, "twofifths", AChar '\x2156', True) -- U+2156 VULGAR FRACTION TWO FIFTHS
  , (0x00000ab4, "threefifths", AChar '\x2157', True) -- U+2157 VULGAR FRACTION THREE FIFTHS
  , (0x00000ab5, "fourfifths", AChar '\x2158', True) -- U+2158 VULGAR FRACTION FOUR FIFTHS
  , (0x00000ab6, "onesixth", AChar '\x2159', True) -- U+2159 VULGAR FRACTION ONE SIXTH
  , (0x00000ab7, "fivesixths", AChar '\x215a', True) -- U+215A VULGAR FRACTION FIVE SIXTHS
  , (0x00000ab8, "careof", AChar '\x2105', True) -- U+2105 CARE OF
  , (0x00000abb, "figdash", AChar '\x2012', True) -- U+2012 FIGURE DASH
  , (0x00000abc, "leftanglebracket", AChar '\x27e8', False) -- U+27E8 MATHEMATICAL LEFT ANGLE BRACKET [NOTE] deprecated keysym
  , (0x00000abd, "decimalpoint", AChar '\x002e', False) -- U+002E FULL STOP [NOTE] deprecated keysym
  , (0x00000abe, "rightanglebracket", AChar '\x27e9', False) -- U+27E9 MATHEMATICAL RIGHT ANGLE BRACKET [NOTE] deprecated keysym
  , (0x00000abf, "marker", ASpecial Marker, True)
  , (0x00000ac3, "oneeighth", AChar '\x215b', True) -- U+215B VULGAR FRACTION ONE EIGHTH
  , (0x00000ac4, "threeeighths", AChar '\x215c', True) -- U+215C VULGAR FRACTION THREE EIGHTHS
  , (0x00000ac5, "fiveeighths", AChar '\x215d', True) -- U+215D VULGAR FRACTION FIVE EIGHTHS
  , (0x00000ac6, "seveneighths", AChar '\x215e', True) -- U+215E VULGAR FRACTION SEVEN EIGHTHS
  , (0x00000ac9, "trademark", AChar '\x2122', True) -- U+2122 TRADE MARK SIGN
  , (0x00000aca, "signaturemark", AChar '\x2613', False) -- U+2613 SALTIRE [NOTE] deprecated keysym
  , (0x00000acb, "trademarkincircle", ASpecial Trademarkincircle, True)
  , (0x00000acc, "leftopentriangle", AChar '\x25c1', False) -- U+25C1 WHITE LEFT-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000acd, "rightopentriangle", AChar '\x25b7', False) -- U+25B7 WHITE RIGHT-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000ace, "emopencircle", AChar '\x25cb', False) -- U+25CB WHITE CIRCLE [NOTE] deprecated keysym
  , (0x00000acf, "emopenrectangle", AChar '\x25af', False) -- U+25AF WHITE VERTICAL RECTANGLE [NOTE] deprecated keysym
  , (0x00000ad0, "leftsinglequotemark", AChar '\x2018', True) -- U+2018 LEFT SINGLE QUOTATION MARK
  , (0x00000ad1, "rightsinglequotemark", AChar '\x2019', True) -- U+2019 RIGHT SINGLE QUOTATION MARK
  , (0x00000ad2, "leftdoublequotemark", AChar '\x201c', True) -- U+201C LEFT DOUBLE QUOTATION MARK
  , (0x00000ad3, "rightdoublequotemark", AChar '\x201d', True) -- U+201D RIGHT DOUBLE QUOTATION MARK
  , (0x00000ad4, "prescription", AChar '\x211e', True) -- U+211E PRESCRIPTION TAKE
  , (0x00000ad5, "permille", AChar '\x2030', True) -- U+2030 PER MILLE SIGN
  , (0x00000ad6, "minutes", AChar '\x2032', True) -- U+2032 PRIME
  , (0x00000ad7, "seconds", AChar '\x2033', True) -- U+2033 DOUBLE PRIME
  , (0x00000ad9, "latincross", AChar '\x271d', True) -- U+271D LATIN CROSS
  , (0x00000ada, "hexagram", ASpecial Hexagram, True)
  , (0x00000adb, "filledrectbullet", AChar '\x25ac', False) -- U+25AC BLACK RECTANGLE [NOTE] deprecated keysym
  , (0x00000adc, "filledlefttribullet", AChar '\x25c0', False) -- U+25C0 BLACK LEFT-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000add, "filledrighttribullet", AChar '\x25b6', False) -- U+25B6 BLACK RIGHT-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000ade, "emfilledcircle", AChar '\x25cf', False) -- U+25CF BLACK CIRCLE [NOTE] deprecated keysym
  , (0x00000adf, "emfilledrect", AChar '\x25ae', False) -- U+25AE BLACK VERTICAL RECTANGLE [NOTE] deprecated keysym
  , (0x00000ae0, "enopencircbullet", AChar '\x25e6', False) -- U+25E6 WHITE BULLET [NOTE] deprecated keysym
  , (0x00000ae1, "enopensquarebullet", AChar '\x25ab', False) -- U+25AB WHITE SMALL SQUARE [NOTE] deprecated keysym
  , (0x00000ae2, "openrectbullet", AChar '\x25ad', False) -- U+25AD WHITE RECTANGLE [NOTE] deprecated keysym
  , (0x00000ae3, "opentribulletup", AChar '\x25b3', False) -- U+25B3 WHITE UP-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000ae4, "opentribulletdown", AChar '\x25bd', False) -- U+25BD WHITE DOWN-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000ae5, "openstar", AChar '\x2606', False) -- U+2606 WHITE STAR [NOTE] deprecated keysym
  , (0x00000ae6, "enfilledcircbullet", AChar '\x2022', False) -- U+2022 BULLET [NOTE] deprecated keysym
  , (0x00000ae7, "enfilledsqbullet", AChar '\x25aa', False) -- U+25AA BLACK SMALL SQUARE [NOTE] deprecated keysym
  , (0x00000ae8, "filledtribulletup", AChar '\x25b2', False) -- U+25B2 BLACK UP-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000ae9, "filledtribulletdown", AChar '\x25bc', False) -- U+25BC BLACK DOWN-POINTING TRIANGLE [NOTE] deprecated keysym
  , (0x00000aea, "leftpointer", AChar '\x261c', False) -- U+261C WHITE LEFT POINTING INDEX [NOTE] deprecated keysym
  , (0x00000aeb, "rightpointer", AChar '\x261e', False) -- U+261E WHITE RIGHT POINTING INDEX [NOTE] deprecated keysym
  , (0x00000aec, "club", AChar '\x2663', True) -- U+2663 BLACK CLUB SUIT
  , (0x00000aed, "diamond", AChar '\x2666', True) -- U+2666 BLACK DIAMOND SUIT
  , (0x00000aee, "heart", AChar '\x2665', True) -- U+2665 BLACK HEART SUIT
  , (0x00000af0, "maltesecross", AChar '\x2720', True) -- U+2720 MALTESE CROSS
  , (0x00000af1, "dagger", AChar '\x2020', True) -- U+2020 DAGGER
  , (0x00000af2, "doubledagger", AChar '\x2021', True) -- U+2021 DOUBLE DAGGER
  , (0x00000af3, "checkmark", AChar '\x2713', True) -- U+2713 CHECK MARK
  , (0x00000af4, "ballotcross", AChar '\x2717', True) -- U+2717 BALLOT X
  , (0x00000af5, "musicalsharp", AChar '\x266f', True) -- U+266F MUSIC SHARP SIGN
  , (0x00000af6, "musicalflat", AChar '\x266d', True) -- U+266D MUSIC FLAT SIGN
  , (0x00000af7, "malesymbol", AChar '\x2642', True) -- U+2642 MALE SIGN
  , (0x00000af8, "femalesymbol", AChar '\x2640', True) -- U+2640 FEMALE SIGN
  , (0x00000af9, "telephone", AChar '\x260e', True) -- U+260E BLACK TELEPHONE
  , (0x00000afa, "telephonerecorder", AChar '\x2315', True) -- U+2315 TELEPHONE RECORDER
  , (0x00000afb, "phonographcopyright", AChar '\x2117', True) -- U+2117 SOUND RECORDING COPYRIGHT
  , (0x00000afc, "caret", AChar '\x2038', True) -- U+2038 CARET
  , (0x00000afd, "singlelowquotemark", AChar '\x201a', True) -- U+201A SINGLE LOW-9 QUOTATION MARK
  , (0x00000afe, "doublelowquotemark", AChar '\x201e', True) -- U+201E DOUBLE LOW-9 QUOTATION MARK
  , (0x00000aff, "cursor", ASpecial Cursor, True)
  --
  -- APL
  --
  , (0x00000ba3, "leftcaret", AChar '\x003c', False) -- U+003C LESS-THAN SIGN [NOTE] deprecated keysym
  , (0x00000ba6, "rightcaret", AChar '\x003e', False) -- U+003E GREATER-THAN SIGN [NOTE] deprecated keysym
  , (0x00000ba8, "downcaret", AChar '\x2228', False) -- U+2228 LOGICAL OR [NOTE] deprecated keysym
  , (0x00000ba9, "upcaret", AChar '\x2227', False) -- U+2227 LOGICAL AND [NOTE] deprecated keysym
  , (0x00000bc0, "overbar", AChar '\x00af', False) -- U+00AF MACRON [NOTE] deprecated keysym
  , (0x00000bc2, "downtack", AChar '\x22a4', True) -- U+22A4 DOWN TACK
  , (0x00000bc3, "upshoe", AChar '\x2229', False) -- U+2229 INTERSECTION [NOTE] deprecated keysym
  , (0x00000bc4, "downstile", AChar '\x230a', True) -- U+230A LEFT FLOOR
  , (0x00000bc6, "underbar", AChar '\x005f', False) -- U+005F LOW LINE [NOTE] deprecated keysym
  , (0x00000bca, "jot", AChar '\x2218', True) -- U+2218 RING OPERATOR
  , (0x00000bcc, "quad", AChar '\x2395', True) -- U+2395 APL FUNCTIONAL SYMBOL QUAD
  , (0x00000bce, "uptack", AChar '\x22a5', True) -- U+22A5 UP TACK
  , (0x00000bcf, "circle", AChar '\x25cb', True) -- U+25CB WHITE CIRCLE
  , (0x00000bd3, "upstile", AChar '\x2308', True) -- U+2308 LEFT CEILING
  , (0x00000bd6, "downshoe", AChar '\x222a', False) -- U+222A UNION [NOTE] deprecated keysym
  , (0x00000bd8, "rightshoe", AChar '\x2283', False) -- U+2283 SUPERSET OF [NOTE] deprecated keysym
  , (0x00000bda, "leftshoe", AChar '\x2282', False) -- U+2282 SUBSET OF [NOTE] deprecated keysym
  , (0x00000bdc, "lefttack", AChar '\x22a2', False) -- U+22A2 RIGHT TACK [WARNING] Ambiguous keysym: conflict on its Unicode value between C header comment (U+22A2 RIGHT TACK) and xkb_keysym_to_utf32 (U+22A3 LEFT TACK)
  , (0x00000bfc, "righttack", AChar '\x22a3', False) -- U+22A3 LEFT TACK [WARNING] Ambiguous keysym: conflict on its Unicode value between C header comment (U+22A3 LEFT TACK) and xkb_keysym_to_utf32 (U+22A2 RIGHT TACK)
  --
  -- Hebrew
  --
  , (0x00000cdf, "hebrew_doublelowline", AChar '\x2017', True) -- U+2017 DOUBLE LOW LINE
  , (0x00000ce0, "hebrew_aleph", AChar '\x05d0', True) -- U+05D0 HEBREW LETTER ALEF
  , (0x00000ce1, "hebrew_bet", AChar '\x05d1', True) -- U+05D1 HEBREW LETTER BET
  , (0x00000ce1, "hebrew_beth", AChar '\x05d1', False) -- U+05D1 HEBREW LETTER BET [NOTE] deprecated keysym
  , (0x00000ce2, "hebrew_gimel", AChar '\x05d2', True) -- U+05D2 HEBREW LETTER GIMEL
  , (0x00000ce2, "hebrew_gimmel", AChar '\x05d2', False) -- U+05D2 HEBREW LETTER GIMEL [NOTE] deprecated keysym
  , (0x00000ce3, "hebrew_dalet", AChar '\x05d3', True) -- U+05D3 HEBREW LETTER DALET
  , (0x00000ce3, "hebrew_daleth", AChar '\x05d3', False) -- U+05D3 HEBREW LETTER DALET [NOTE] deprecated keysym
  , (0x00000ce4, "hebrew_he", AChar '\x05d4', True) -- U+05D4 HEBREW LETTER HE
  , (0x00000ce5, "hebrew_waw", AChar '\x05d5', True) -- U+05D5 HEBREW LETTER VAV
  , (0x00000ce6, "hebrew_zain", AChar '\x05d6', True) -- U+05D6 HEBREW LETTER ZAYIN
  , (0x00000ce6, "hebrew_zayin", AChar '\x05d6', False) -- U+05D6 HEBREW LETTER ZAYIN [NOTE] deprecated keysym
  , (0x00000ce7, "hebrew_chet", AChar '\x05d7', True) -- U+05D7 HEBREW LETTER HET
  , (0x00000ce7, "hebrew_het", AChar '\x05d7', False) -- U+05D7 HEBREW LETTER HET [NOTE] deprecated keysym
  , (0x00000ce8, "hebrew_tet", AChar '\x05d8', True) -- U+05D8 HEBREW LETTER TET
  , (0x00000ce8, "hebrew_teth", AChar '\x05d8', False) -- U+05D8 HEBREW LETTER TET [NOTE] deprecated keysym
  , (0x00000ce9, "hebrew_yod", AChar '\x05d9', True) -- U+05D9 HEBREW LETTER YOD
  , (0x00000cea, "hebrew_finalkaph", AChar '\x05da', True) -- U+05DA HEBREW LETTER FINAL KAF
  , (0x00000ceb, "hebrew_kaph", AChar '\x05db', True) -- U+05DB HEBREW LETTER KAF
  , (0x00000cec, "hebrew_lamed", AChar '\x05dc', True) -- U+05DC HEBREW LETTER LAMED
  , (0x00000ced, "hebrew_finalmem", AChar '\x05dd', True) -- U+05DD HEBREW LETTER FINAL MEM
  , (0x00000cee, "hebrew_mem", AChar '\x05de', True) -- U+05DE HEBREW LETTER MEM
  , (0x00000cef, "hebrew_finalnun", AChar '\x05df', True) -- U+05DF HEBREW LETTER FINAL NUN
  , (0x00000cf0, "hebrew_nun", AChar '\x05e0', True) -- U+05E0 HEBREW LETTER NUN
  , (0x00000cf1, "hebrew_samech", AChar '\x05e1', True) -- U+05E1 HEBREW LETTER SAMEKH
  , (0x00000cf1, "hebrew_samekh", AChar '\x05e1', False) -- U+05E1 HEBREW LETTER SAMEKH [NOTE] deprecated keysym
  , (0x00000cf2, "hebrew_ayin", AChar '\x05e2', True) -- U+05E2 HEBREW LETTER AYIN
  , (0x00000cf3, "hebrew_finalpe", AChar '\x05e3', True) -- U+05E3 HEBREW LETTER FINAL PE
  , (0x00000cf4, "hebrew_pe", AChar '\x05e4', True) -- U+05E4 HEBREW LETTER PE
  , (0x00000cf5, "hebrew_finalzade", AChar '\x05e5', True) -- U+05E5 HEBREW LETTER FINAL TSADI
  , (0x00000cf5, "hebrew_finalzadi", AChar '\x05e5', False) -- U+05E5 HEBREW LETTER FINAL TSADI [NOTE] deprecated keysym
  , (0x00000cf6, "hebrew_zade", AChar '\x05e6', True) -- U+05E6 HEBREW LETTER TSADI
  , (0x00000cf6, "hebrew_zadi", AChar '\x05e6', False) -- U+05E6 HEBREW LETTER TSADI [NOTE] deprecated keysym
  , (0x00000cf7, "hebrew_qoph", AChar '\x05e7', True) -- U+05E7 HEBREW LETTER QOF
  , (0x00000cf7, "hebrew_kuf", AChar '\x05e7', False) -- U+05E7 HEBREW LETTER QOF [NOTE] deprecated keysym
  , (0x00000cf8, "hebrew_resh", AChar '\x05e8', True) -- U+05E8 HEBREW LETTER RESH
  , (0x00000cf9, "hebrew_shin", AChar '\x05e9', True) -- U+05E9 HEBREW LETTER SHIN
  , (0x00000cfa, "hebrew_taw", AChar '\x05ea', True) -- U+05EA HEBREW LETTER TAV
  , (0x00000cfa, "hebrew_taf", AChar '\x05ea', False) -- U+05EA HEBREW LETTER TAV [NOTE] deprecated keysym
  , (0x0000ff7e, "Hebrew_switch", ASpecial Iso_Group_Shift, False) -- Alias for mode_switch
  --
  -- Thai
  --
  , (0x00000da1, "Thai_kokai", AChar '\x0e01', True) -- U+0E01 THAI CHARACTER KO KAI
  , (0x00000da2, "Thai_khokhai", AChar '\x0e02', True) -- U+0E02 THAI CHARACTER KHO KHAI
  , (0x00000da3, "Thai_khokhuat", AChar '\x0e03', True) -- U+0E03 THAI CHARACTER KHO KHUAT
  , (0x00000da4, "Thai_khokhwai", AChar '\x0e04', True) -- U+0E04 THAI CHARACTER KHO KHWAI
  , (0x00000da5, "Thai_khokhon", AChar '\x0e05', True) -- U+0E05 THAI CHARACTER KHO KHON
  , (0x00000da6, "Thai_khorakhang", AChar '\x0e06', True) -- U+0E06 THAI CHARACTER KHO RAKHANG
  , (0x00000da7, "Thai_ngongu", AChar '\x0e07', True) -- U+0E07 THAI CHARACTER NGO NGU
  , (0x00000da8, "Thai_chochan", AChar '\x0e08', True) -- U+0E08 THAI CHARACTER CHO CHAN
  , (0x00000da9, "Thai_choching", AChar '\x0e09', True) -- U+0E09 THAI CHARACTER CHO CHING
  , (0x00000daa, "Thai_chochang", AChar '\x0e0a', True) -- U+0E0A THAI CHARACTER CHO CHANG
  , (0x00000dab, "Thai_soso", AChar '\x0e0b', True) -- U+0E0B THAI CHARACTER SO SO
  , (0x00000dac, "Thai_chochoe", AChar '\x0e0c', True) -- U+0E0C THAI CHARACTER CHO CHOE
  , (0x00000dad, "Thai_yoying", AChar '\x0e0d', True) -- U+0E0D THAI CHARACTER YO YING
  , (0x00000dae, "Thai_dochada", AChar '\x0e0e', True) -- U+0E0E THAI CHARACTER DO CHADA
  , (0x00000daf, "Thai_topatak", AChar '\x0e0f', True) -- U+0E0F THAI CHARACTER TO PATAK
  , (0x00000db0, "Thai_thothan", AChar '\x0e10', True) -- U+0E10 THAI CHARACTER THO THAN
  , (0x00000db1, "Thai_thonangmontho", AChar '\x0e11', True) -- U+0E11 THAI CHARACTER THO NANGMONTHO
  , (0x00000db2, "Thai_thophuthao", AChar '\x0e12', True) -- U+0E12 THAI CHARACTER THO PHUTHAO
  , (0x00000db3, "Thai_nonen", AChar '\x0e13', True) -- U+0E13 THAI CHARACTER NO NEN
  , (0x00000db4, "Thai_dodek", AChar '\x0e14', True) -- U+0E14 THAI CHARACTER DO DEK
  , (0x00000db5, "Thai_totao", AChar '\x0e15', True) -- U+0E15 THAI CHARACTER TO TAO
  , (0x00000db6, "Thai_thothung", AChar '\x0e16', True) -- U+0E16 THAI CHARACTER THO THUNG
  , (0x00000db7, "Thai_thothahan", AChar '\x0e17', True) -- U+0E17 THAI CHARACTER THO THAHAN
  , (0x00000db8, "Thai_thothong", AChar '\x0e18', True) -- U+0E18 THAI CHARACTER THO THONG
  , (0x00000db9, "Thai_nonu", AChar '\x0e19', True) -- U+0E19 THAI CHARACTER NO NU
  , (0x00000dba, "Thai_bobaimai", AChar '\x0e1a', True) -- U+0E1A THAI CHARACTER BO BAIMAI
  , (0x00000dbb, "Thai_popla", AChar '\x0e1b', True) -- U+0E1B THAI CHARACTER PO PLA
  , (0x00000dbc, "Thai_phophung", AChar '\x0e1c', True) -- U+0E1C THAI CHARACTER PHO PHUNG
  , (0x00000dbd, "Thai_fofa", AChar '\x0e1d', True) -- U+0E1D THAI CHARACTER FO FA
  , (0x00000dbe, "Thai_phophan", AChar '\x0e1e', True) -- U+0E1E THAI CHARACTER PHO PHAN
  , (0x00000dbf, "Thai_fofan", AChar '\x0e1f', True) -- U+0E1F THAI CHARACTER FO FAN
  , (0x00000dc0, "Thai_phosamphao", AChar '\x0e20', True) -- U+0E20 THAI CHARACTER PHO SAMPHAO
  , (0x00000dc1, "Thai_moma", AChar '\x0e21', True) -- U+0E21 THAI CHARACTER MO MA
  , (0x00000dc2, "Thai_yoyak", AChar '\x0e22', True) -- U+0E22 THAI CHARACTER YO YAK
  , (0x00000dc3, "Thai_rorua", AChar '\x0e23', True) -- U+0E23 THAI CHARACTER RO RUA
  , (0x00000dc4, "Thai_ru", AChar '\x0e24', True) -- U+0E24 THAI CHARACTER RU
  , (0x00000dc5, "Thai_loling", AChar '\x0e25', True) -- U+0E25 THAI CHARACTER LO LING
  , (0x00000dc6, "Thai_lu", AChar '\x0e26', True) -- U+0E26 THAI CHARACTER LU
  , (0x00000dc7, "Thai_wowaen", AChar '\x0e27', True) -- U+0E27 THAI CHARACTER WO WAEN
  , (0x00000dc8, "Thai_sosala", AChar '\x0e28', True) -- U+0E28 THAI CHARACTER SO SALA
  , (0x00000dc9, "Thai_sorusi", AChar '\x0e29', True) -- U+0E29 THAI CHARACTER SO RUSI
  , (0x00000dca, "Thai_sosua", AChar '\x0e2a', True) -- U+0E2A THAI CHARACTER SO SUA
  , (0x00000dcb, "Thai_hohip", AChar '\x0e2b', True) -- U+0E2B THAI CHARACTER HO HIP
  , (0x00000dcc, "Thai_lochula", AChar '\x0e2c', True) -- U+0E2C THAI CHARACTER LO CHULA
  , (0x00000dcd, "Thai_oang", AChar '\x0e2d', True) -- U+0E2D THAI CHARACTER O ANG
  , (0x00000dce, "Thai_honokhuk", AChar '\x0e2e', True) -- U+0E2E THAI CHARACTER HO NOKHUK
  , (0x00000dcf, "Thai_paiyannoi", AChar '\x0e2f', True) -- U+0E2F THAI CHARACTER PAIYANNOI
  , (0x00000dd0, "Thai_saraa", AChar '\x0e30', True) -- U+0E30 THAI CHARACTER SARA A
  , (0x00000dd1, "Thai_maihanakat", AChar '\x0e31', True) -- U+0E31 THAI CHARACTER MAI HAN-AKAT
  , (0x00000dd2, "Thai_saraaa", AChar '\x0e32', True) -- U+0E32 THAI CHARACTER SARA AA
  , (0x00000dd3, "Thai_saraam", AChar '\x0e33', True) -- U+0E33 THAI CHARACTER SARA AM
  , (0x00000dd4, "Thai_sarai", AChar '\x0e34', True) -- U+0E34 THAI CHARACTER SARA I
  , (0x00000dd5, "Thai_saraii", AChar '\x0e35', True) -- U+0E35 THAI CHARACTER SARA II
  , (0x00000dd6, "Thai_saraue", AChar '\x0e36', True) -- U+0E36 THAI CHARACTER SARA UE
  , (0x00000dd7, "Thai_sarauee", AChar '\x0e37', True) -- U+0E37 THAI CHARACTER SARA UEE
  , (0x00000dd8, "Thai_sarau", AChar '\x0e38', True) -- U+0E38 THAI CHARACTER SARA U
  , (0x00000dd9, "Thai_sarauu", AChar '\x0e39', True) -- U+0E39 THAI CHARACTER SARA UU
  , (0x00000dda, "Thai_phinthu", AChar '\x0e3a', True) -- U+0E3A THAI CHARACTER PHINTHU
  , (0x00000dde, "Thai_maihanakat_maitho", AChar '\x0e3e', True) -- U+0E3E
  , (0x00000ddf, "Thai_baht", AChar '\x0e3f', True) -- U+0E3F THAI CURRENCY SYMBOL BAHT
  , (0x00000de0, "Thai_sarae", AChar '\x0e40', True) -- U+0E40 THAI CHARACTER SARA E
  , (0x00000de1, "Thai_saraae", AChar '\x0e41', True) -- U+0E41 THAI CHARACTER SARA AE
  , (0x00000de2, "Thai_sarao", AChar '\x0e42', True) -- U+0E42 THAI CHARACTER SARA O
  , (0x00000de3, "Thai_saraaimaimuan", AChar '\x0e43', True) -- U+0E43 THAI CHARACTER SARA AI MAIMUAN
  , (0x00000de4, "Thai_saraaimaimalai", AChar '\x0e44', True) -- U+0E44 THAI CHARACTER SARA AI MAIMALAI
  , (0x00000de5, "Thai_lakkhangyao", AChar '\x0e45', True) -- U+0E45 THAI CHARACTER LAKKHANGYAO
  , (0x00000de6, "Thai_maiyamok", AChar '\x0e46', True) -- U+0E46 THAI CHARACTER MAIYAMOK
  , (0x00000de7, "Thai_maitaikhu", AChar '\x0e47', True) -- U+0E47 THAI CHARACTER MAITAIKHU
  , (0x00000de8, "Thai_maiek", AChar '\x0e48', True) -- U+0E48 THAI CHARACTER MAI EK
  , (0x00000de9, "Thai_maitho", AChar '\x0e49', True) -- U+0E49 THAI CHARACTER MAI THO
  , (0x00000dea, "Thai_maitri", AChar '\x0e4a', True) -- U+0E4A THAI CHARACTER MAI TRI
  , (0x00000deb, "Thai_maichattawa", AChar '\x0e4b', True) -- U+0E4B THAI CHARACTER MAI CHATTAWA
  , (0x00000dec, "Thai_thanthakhat", AChar '\x0e4c', True) -- U+0E4C THAI CHARACTER THANTHAKHAT
  , (0x00000ded, "Thai_nikhahit", AChar '\x0e4d', True) -- U+0E4D THAI CHARACTER NIKHAHIT
  , (0x00000df0, "Thai_leksun", AChar '\x0e50', True) -- U+0E50 THAI DIGIT ZERO
  , (0x00000df1, "Thai_leknung", AChar '\x0e51', True) -- U+0E51 THAI DIGIT ONE
  , (0x00000df2, "Thai_leksong", AChar '\x0e52', True) -- U+0E52 THAI DIGIT TWO
  , (0x00000df3, "Thai_leksam", AChar '\x0e53', True) -- U+0E53 THAI DIGIT THREE
  , (0x00000df4, "Thai_leksi", AChar '\x0e54', True) -- U+0E54 THAI DIGIT FOUR
  , (0x00000df5, "Thai_lekha", AChar '\x0e55', True) -- U+0E55 THAI DIGIT FIVE
  , (0x00000df6, "Thai_lekhok", AChar '\x0e56', True) -- U+0E56 THAI DIGIT SIX
  , (0x00000df7, "Thai_lekchet", AChar '\x0e57', True) -- U+0E57 THAI DIGIT SEVEN
  , (0x00000df8, "Thai_lekpaet", AChar '\x0e58', True) -- U+0E58 THAI DIGIT EIGHT
  , (0x00000df9, "Thai_lekkao", AChar '\x0e59', True) -- U+0E59 THAI DIGIT NINE
  --
  -- Korean
  --
  , (0x0000ff31, "Hangul", ASpecial Hangul, True) -- Hangul start\/stop(toggle)
  , (0x0000ff32, "Hangul_Start", ASpecial Hangul_Start, True) -- Hangul start
  , (0x0000ff33, "Hangul_End", ASpecial Hangul_End, True) -- Hangul end, English start
  , (0x0000ff34, "Hangul_Hanja", ASpecial Hangul_Hanja, True) -- Start Hangul->Hanja Conversion
  , (0x0000ff35, "Hangul_Jamo", ASpecial Hangul_Jamo, True) -- Hangul Jamo mode
  , (0x0000ff36, "Hangul_Romaja", ASpecial Hangul_Romaja, True) -- Hangul Romaja mode
  , (0x0000ff37, "Hangul_Codeinput", ASpecial Codeinput, False) -- Hangul code input mode (alias for Codeinput)
  , (0x0000ff38, "Hangul_Jeonja", ASpecial Hangul_Jeonja, True) -- Jeonja mode
  , (0x0000ff39, "Hangul_Banja", ASpecial Hangul_Banja, True) -- Banja mode
  , (0x0000ff3a, "Hangul_PreHanja", ASpecial Hangul_Prehanja, True) -- Pre Hanja conversion
  , (0x0000ff3b, "Hangul_PostHanja", ASpecial Hangul_Posthanja, True) -- Post Hanja conversion
  , (0x0000ff3c, "Hangul_SingleCandidate", ASpecial SingleCandidate, False) -- Single candidate (alias for SingleCandidate)
  , (0x0000ff3d, "Hangul_MultipleCandidate", ASpecial MultipleCandidate, False) -- Multiple candidate (alias for MultipleCandidate)
  , (0x0000ff3e, "Hangul_PreviousCandidate", ASpecial PreviousCandidate, False) -- Previous candidate (alias for PreviousCandidate)
  , (0x0000ff3f, "Hangul_Special", ASpecial Hangul_Special, True) -- Special symbols
  , (0x0000ff7e, "Hangul_switch", ASpecial Iso_Group_Shift, False) -- Alias for mode_switch
  --
  -- Hangul Consonant Characters
  --
  , (0x00000ea1, "Hangul_Kiyeog", AChar '\x3131', True) -- U+3131 HANGUL LETTER KIYEOK
  , (0x00000ea2, "Hangul_SsangKiyeog", AChar '\x3132', True) -- U+3132 HANGUL LETTER SSANGKIYEOK
  , (0x00000ea3, "Hangul_KiyeogSios", AChar '\x3133', True) -- U+3133 HANGUL LETTER KIYEOK-SIOS
  , (0x00000ea4, "Hangul_Nieun", AChar '\x3134', True) -- U+3134 HANGUL LETTER NIEUN
  , (0x00000ea5, "Hangul_NieunJieuj", AChar '\x3135', True) -- U+3135 HANGUL LETTER NIEUN-CIEUC
  , (0x00000ea6, "Hangul_NieunHieuh", AChar '\x3136', True) -- U+3136 HANGUL LETTER NIEUN-HIEUH
  , (0x00000ea7, "Hangul_Dikeud", AChar '\x3137', True) -- U+3137 HANGUL LETTER TIKEUT
  , (0x00000ea8, "Hangul_SsangDikeud", AChar '\x3138', True) -- U+3138 HANGUL LETTER SSANGTIKEUT
  , (0x00000ea9, "Hangul_Rieul", AChar '\x3139', True) -- U+3139 HANGUL LETTER RIEUL
  , (0x00000eaa, "Hangul_RieulKiyeog", AChar '\x313a', True) -- U+313A HANGUL LETTER RIEUL-KIYEOK
  , (0x00000eab, "Hangul_RieulMieum", AChar '\x313b', True) -- U+313B HANGUL LETTER RIEUL-MIEUM
  , (0x00000eac, "Hangul_RieulPieub", AChar '\x313c', True) -- U+313C HANGUL LETTER RIEUL-PIEUP
  , (0x00000ead, "Hangul_RieulSios", AChar '\x313d', True) -- U+313D HANGUL LETTER RIEUL-SIOS
  , (0x00000eae, "Hangul_RieulTieut", AChar '\x313e', True) -- U+313E HANGUL LETTER RIEUL-THIEUTH
  , (0x00000eaf, "Hangul_RieulPhieuf", AChar '\x313f', True) -- U+313F HANGUL LETTER RIEUL-PHIEUPH
  , (0x00000eb0, "Hangul_RieulHieuh", AChar '\x3140', True) -- U+3140 HANGUL LETTER RIEUL-HIEUH
  , (0x00000eb1, "Hangul_Mieum", AChar '\x3141', True) -- U+3141 HANGUL LETTER MIEUM
  , (0x00000eb2, "Hangul_Pieub", AChar '\x3142', True) -- U+3142 HANGUL LETTER PIEUP
  , (0x00000eb3, "Hangul_SsangPieub", AChar '\x3143', True) -- U+3143 HANGUL LETTER SSANGPIEUP
  , (0x00000eb4, "Hangul_PieubSios", AChar '\x3144', True) -- U+3144 HANGUL LETTER PIEUP-SIOS
  , (0x00000eb5, "Hangul_Sios", AChar '\x3145', True) -- U+3145 HANGUL LETTER SIOS
  , (0x00000eb6, "Hangul_SsangSios", AChar '\x3146', True) -- U+3146 HANGUL LETTER SSANGSIOS
  , (0x00000eb7, "Hangul_Ieung", AChar '\x3147', True) -- U+3147 HANGUL LETTER IEUNG
  , (0x00000eb8, "Hangul_Jieuj", AChar '\x3148', True) -- U+3148 HANGUL LETTER CIEUC
  , (0x00000eb9, "Hangul_SsangJieuj", AChar '\x3149', True) -- U+3149 HANGUL LETTER SSANGCIEUC
  , (0x00000eba, "Hangul_Cieuc", AChar '\x314a', True) -- U+314A HANGUL LETTER CHIEUCH
  , (0x00000ebb, "Hangul_Khieuq", AChar '\x314b', True) -- U+314B HANGUL LETTER KHIEUKH
  , (0x00000ebc, "Hangul_Tieut", AChar '\x314c', True) -- U+314C HANGUL LETTER THIEUTH
  , (0x00000ebd, "Hangul_Phieuf", AChar '\x314d', True) -- U+314D HANGUL LETTER PHIEUPH
  , (0x00000ebe, "Hangul_Hieuh", AChar '\x314e', True) -- U+314E HANGUL LETTER HIEUH
  --
  -- Hangul Vowel Characters
  --
  , (0x00000ebf, "Hangul_A", AChar '\x314f', True) -- U+314F HANGUL LETTER A
  , (0x00000ec0, "Hangul_AE", AChar '\x3150', True) -- U+3150 HANGUL LETTER AE
  , (0x00000ec1, "Hangul_YA", AChar '\x3151', True) -- U+3151 HANGUL LETTER YA
  , (0x00000ec2, "Hangul_YAE", AChar '\x3152', True) -- U+3152 HANGUL LETTER YAE
  , (0x00000ec3, "Hangul_EO", AChar '\x3153', True) -- U+3153 HANGUL LETTER EO
  , (0x00000ec4, "Hangul_E", AChar '\x3154', True) -- U+3154 HANGUL LETTER E
  , (0x00000ec5, "Hangul_YEO", AChar '\x3155', True) -- U+3155 HANGUL LETTER YEO
  , (0x00000ec6, "Hangul_YE", AChar '\x3156', True) -- U+3156 HANGUL LETTER YE
  , (0x00000ec7, "Hangul_O", AChar '\x3157', True) -- U+3157 HANGUL LETTER O
  , (0x00000ec8, "Hangul_WA", AChar '\x3158', True) -- U+3158 HANGUL LETTER WA
  , (0x00000ec9, "Hangul_WAE", AChar '\x3159', True) -- U+3159 HANGUL LETTER WAE
  , (0x00000eca, "Hangul_OE", AChar '\x315a', True) -- U+315A HANGUL LETTER OE
  , (0x00000ecb, "Hangul_YO", AChar '\x315b', True) -- U+315B HANGUL LETTER YO
  , (0x00000ecc, "Hangul_U", AChar '\x315c', True) -- U+315C HANGUL LETTER U
  , (0x00000ecd, "Hangul_WEO", AChar '\x315d', True) -- U+315D HANGUL LETTER WEO
  , (0x00000ece, "Hangul_WE", AChar '\x315e', True) -- U+315E HANGUL LETTER WE
  , (0x00000ecf, "Hangul_WI", AChar '\x315f', True) -- U+315F HANGUL LETTER WI
  , (0x00000ed0, "Hangul_YU", AChar '\x3160', True) -- U+3160 HANGUL LETTER YU
  , (0x00000ed1, "Hangul_EU", AChar '\x3161', True) -- U+3161 HANGUL LETTER EU
  , (0x00000ed2, "Hangul_YI", AChar '\x3162', True) -- U+3162 HANGUL LETTER YI
  , (0x00000ed3, "Hangul_I", AChar '\x3163', True) -- U+3163 HANGUL LETTER I
  --
  -- Hangul syllable-final (JongSeong) Characters
  --
  , (0x00000ed4, "Hangul_J_Kiyeog", AChar '\x11a8', True) -- U+11A8 HANGUL JONGSEONG KIYEOK
  , (0x00000ed5, "Hangul_J_SsangKiyeog", AChar '\x11a9', True) -- U+11A9 HANGUL JONGSEONG SSANGKIYEOK
  , (0x00000ed6, "Hangul_J_KiyeogSios", AChar '\x11aa', True) -- U+11AA HANGUL JONGSEONG KIYEOK-SIOS
  , (0x00000ed7, "Hangul_J_Nieun", AChar '\x11ab', True) -- U+11AB HANGUL JONGSEONG NIEUN
  , (0x00000ed8, "Hangul_J_NieunJieuj", AChar '\x11ac', True) -- U+11AC HANGUL JONGSEONG NIEUN-CIEUC
  , (0x00000ed9, "Hangul_J_NieunHieuh", AChar '\x11ad', True) -- U+11AD HANGUL JONGSEONG NIEUN-HIEUH
  , (0x00000eda, "Hangul_J_Dikeud", AChar '\x11ae', True) -- U+11AE HANGUL JONGSEONG TIKEUT
  , (0x00000edb, "Hangul_J_Rieul", AChar '\x11af', True) -- U+11AF HANGUL JONGSEONG RIEUL
  , (0x00000edc, "Hangul_J_RieulKiyeog", AChar '\x11b0', True) -- U+11B0 HANGUL JONGSEONG RIEUL-KIYEOK
  , (0x00000edd, "Hangul_J_RieulMieum", AChar '\x11b1', True) -- U+11B1 HANGUL JONGSEONG RIEUL-MIEUM
  , (0x00000ede, "Hangul_J_RieulPieub", AChar '\x11b2', True) -- U+11B2 HANGUL JONGSEONG RIEUL-PIEUP
  , (0x00000edf, "Hangul_J_RieulSios", AChar '\x11b3', True) -- U+11B3 HANGUL JONGSEONG RIEUL-SIOS
  , (0x00000ee0, "Hangul_J_RieulTieut", AChar '\x11b4', True) -- U+11B4 HANGUL JONGSEONG RIEUL-THIEUTH
  , (0x00000ee1, "Hangul_J_RieulPhieuf", AChar '\x11b5', True) -- U+11B5 HANGUL JONGSEONG RIEUL-PHIEUPH
  , (0x00000ee2, "Hangul_J_RieulHieuh", AChar '\x11b6', True) -- U+11B6 HANGUL JONGSEONG RIEUL-HIEUH
  , (0x00000ee3, "Hangul_J_Mieum", AChar '\x11b7', True) -- U+11B7 HANGUL JONGSEONG MIEUM
  , (0x00000ee4, "Hangul_J_Pieub", AChar '\x11b8', True) -- U+11B8 HANGUL JONGSEONG PIEUP
  , (0x00000ee5, "Hangul_J_PieubSios", AChar '\x11b9', True) -- U+11B9 HANGUL JONGSEONG PIEUP-SIOS
  , (0x00000ee6, "Hangul_J_Sios", AChar '\x11ba', True) -- U+11BA HANGUL JONGSEONG SIOS
  , (0x00000ee7, "Hangul_J_SsangSios", AChar '\x11bb', True) -- U+11BB HANGUL JONGSEONG SSANGSIOS
  , (0x00000ee8, "Hangul_J_Ieung", AChar '\x11bc', True) -- U+11BC HANGUL JONGSEONG IEUNG
  , (0x00000ee9, "Hangul_J_Jieuj", AChar '\x11bd', True) -- U+11BD HANGUL JONGSEONG CIEUC
  , (0x00000eea, "Hangul_J_Cieuc", AChar '\x11be', True) -- U+11BE HANGUL JONGSEONG CHIEUCH
  , (0x00000eeb, "Hangul_J_Khieuq", AChar '\x11bf', True) -- U+11BF HANGUL JONGSEONG KHIEUKH
  , (0x00000eec, "Hangul_J_Tieut", AChar '\x11c0', True) -- U+11C0 HANGUL JONGSEONG THIEUTH
  , (0x00000eed, "Hangul_J_Phieuf", AChar '\x11c1', True) -- U+11C1 HANGUL JONGSEONG PHIEUPH
  , (0x00000eee, "Hangul_J_Hieuh", AChar '\x11c2', True) -- U+11C2 HANGUL JONGSEONG HIEUH
  --
  -- Ancient Hangul Consonant Characters
  --
  , (0x00000eef, "Hangul_RieulYeorinHieuh", AChar '\x316d', True) -- U+316D HANGUL LETTER RIEUL-YEORINHIEUH
  , (0x00000ef0, "Hangul_SunkyeongeumMieum", AChar '\x3171', True) -- U+3171 HANGUL LETTER KAPYEOUNMIEUM
  , (0x00000ef1, "Hangul_SunkyeongeumPieub", AChar '\x3178', True) -- U+3178 HANGUL LETTER KAPYEOUNPIEUP
  , (0x00000ef2, "Hangul_PanSios", AChar '\x317f', True) -- U+317F HANGUL LETTER PANSIOS
  , (0x00000ef3, "Hangul_KkogjiDalrinIeung", ASpecial Hangul_Kkogjidalrinieung, True)
  , (0x00000ef4, "Hangul_SunkyeongeumPhieuf", AChar '\x3184', True) -- U+3184 HANGUL LETTER KAPYEOUNPHIEUPH
  , (0x00000ef5, "Hangul_YeorinHieuh", AChar '\x3186', True) -- U+3186 HANGUL LETTER YEORINHIEUH
  --
  -- Ancient Hangul Vowel Characters
  --
  , (0x00000ef6, "Hangul_AraeA", AChar '\x318d', True) -- U+318D HANGUL LETTER ARAEA
  , (0x00000ef7, "Hangul_AraeAE", AChar '\x318e', True) -- U+318E HANGUL LETTER ARAEAE
  --
  -- Ancient Hangul syllable-final (JongSeong) Characters
  --
  , (0x00000ef8, "Hangul_J_PanSios", AChar '\x11eb', True) -- U+11EB HANGUL JONGSEONG PANSIOS
  , (0x00000ef9, "Hangul_J_KkogjiDalrinIeung", AChar '\x11f0', True) -- U+11F0 HANGUL JONGSEONG YESIEUNG
  , (0x00000efa, "Hangul_J_YeorinHieuh", AChar '\x11f9', True) -- U+11F9 HANGUL JONGSEONG YEORINHIEUH
  --
  -- Korean currency symbol
  --
  , (0x00000eff, "Korean_Won", AChar '\x20a9', False) -- U+20A9 WON SIGN [NOTE] deprecated keysym
  --
  -- Armenian
  --
  , (0x01000587, "Armenian_ligature_ew", AChar '\x0587', True) -- U+0587 ARMENIAN SMALL LIGATURE ECH YIWN
  , (0x01000589, "Armenian_full_stop", AChar '\x0589', True) -- U+0589 ARMENIAN FULL STOP
  , (0x01000589, "Armenian_verjaket", AChar '\x0589', False) -- U+0589 ARMENIAN FULL STOP
  , (0x0100055d, "Armenian_separation_mark", AChar '\x055d', True) -- U+055D ARMENIAN COMMA
  , (0x0100055d, "Armenian_but", AChar '\x055d', False) -- U+055D ARMENIAN COMMA
  , (0x0100058a, "Armenian_hyphen", AChar '\x058a', True) -- U+058A ARMENIAN HYPHEN
  , (0x0100058a, "Armenian_yentamna", AChar '\x058a', False) -- U+058A ARMENIAN HYPHEN
  , (0x0100055c, "Armenian_exclam", AChar '\x055c', True) -- U+055C ARMENIAN EXCLAMATION MARK
  , (0x0100055c, "Armenian_amanak", AChar '\x055c', False) -- U+055C ARMENIAN EXCLAMATION MARK
  , (0x0100055b, "Armenian_accent", AChar '\x055b', True) -- U+055B ARMENIAN EMPHASIS MARK
  , (0x0100055b, "Armenian_shesht", AChar '\x055b', False) -- U+055B ARMENIAN EMPHASIS MARK
  , (0x0100055e, "Armenian_question", AChar '\x055e', True) -- U+055E ARMENIAN QUESTION MARK
  , (0x0100055e, "Armenian_paruyk", AChar '\x055e', False) -- U+055E ARMENIAN QUESTION MARK
  , (0x01000531, "Armenian_AYB", AChar '\x0531', True) -- U+0531 ARMENIAN CAPITAL LETTER AYB
  , (0x01000561, "Armenian_ayb", AChar '\x0561', True) -- U+0561 ARMENIAN SMALL LETTER AYB
  , (0x01000532, "Armenian_BEN", AChar '\x0532', True) -- U+0532 ARMENIAN CAPITAL LETTER BEN
  , (0x01000562, "Armenian_ben", AChar '\x0562', True) -- U+0562 ARMENIAN SMALL LETTER BEN
  , (0x01000533, "Armenian_GIM", AChar '\x0533', True) -- U+0533 ARMENIAN CAPITAL LETTER GIM
  , (0x01000563, "Armenian_gim", AChar '\x0563', True) -- U+0563 ARMENIAN SMALL LETTER GIM
  , (0x01000534, "Armenian_DA", AChar '\x0534', True) -- U+0534 ARMENIAN CAPITAL LETTER DA
  , (0x01000564, "Armenian_da", AChar '\x0564', True) -- U+0564 ARMENIAN SMALL LETTER DA
  , (0x01000535, "Armenian_YECH", AChar '\x0535', True) -- U+0535 ARMENIAN CAPITAL LETTER ECH
  , (0x01000565, "Armenian_yech", AChar '\x0565', True) -- U+0565 ARMENIAN SMALL LETTER ECH
  , (0x01000536, "Armenian_ZA", AChar '\x0536', True) -- U+0536 ARMENIAN CAPITAL LETTER ZA
  , (0x01000566, "Armenian_za", AChar '\x0566', True) -- U+0566 ARMENIAN SMALL LETTER ZA
  , (0x01000537, "Armenian_E", AChar '\x0537', True) -- U+0537 ARMENIAN CAPITAL LETTER EH
  , (0x01000567, "Armenian_e", AChar '\x0567', True) -- U+0567 ARMENIAN SMALL LETTER EH
  , (0x01000538, "Armenian_AT", AChar '\x0538', True) -- U+0538 ARMENIAN CAPITAL LETTER ET
  , (0x01000568, "Armenian_at", AChar '\x0568', True) -- U+0568 ARMENIAN SMALL LETTER ET
  , (0x01000539, "Armenian_TO", AChar '\x0539', True) -- U+0539 ARMENIAN CAPITAL LETTER TO
  , (0x01000569, "Armenian_to", AChar '\x0569', True) -- U+0569 ARMENIAN SMALL LETTER TO
  , (0x0100053a, "Armenian_ZHE", AChar '\x053a', True) -- U+053A ARMENIAN CAPITAL LETTER ZHE
  , (0x0100056a, "Armenian_zhe", AChar '\x056a', True) -- U+056A ARMENIAN SMALL LETTER ZHE
  , (0x0100053b, "Armenian_INI", AChar '\x053b', True) -- U+053B ARMENIAN CAPITAL LETTER INI
  , (0x0100056b, "Armenian_ini", AChar '\x056b', True) -- U+056B ARMENIAN SMALL LETTER INI
  , (0x0100053c, "Armenian_LYUN", AChar '\x053c', True) -- U+053C ARMENIAN CAPITAL LETTER LIWN
  , (0x0100056c, "Armenian_lyun", AChar '\x056c', True) -- U+056C ARMENIAN SMALL LETTER LIWN
  , (0x0100053d, "Armenian_KHE", AChar '\x053d', True) -- U+053D ARMENIAN CAPITAL LETTER XEH
  , (0x0100056d, "Armenian_khe", AChar '\x056d', True) -- U+056D ARMENIAN SMALL LETTER XEH
  , (0x0100053e, "Armenian_TSA", AChar '\x053e', True) -- U+053E ARMENIAN CAPITAL LETTER CA
  , (0x0100056e, "Armenian_tsa", AChar '\x056e', True) -- U+056E ARMENIAN SMALL LETTER CA
  , (0x0100053f, "Armenian_KEN", AChar '\x053f', True) -- U+053F ARMENIAN CAPITAL LETTER KEN
  , (0x0100056f, "Armenian_ken", AChar '\x056f', True) -- U+056F ARMENIAN SMALL LETTER KEN
  , (0x01000540, "Armenian_HO", AChar '\x0540', True) -- U+0540 ARMENIAN CAPITAL LETTER HO
  , (0x01000570, "Armenian_ho", AChar '\x0570', True) -- U+0570 ARMENIAN SMALL LETTER HO
  , (0x01000541, "Armenian_DZA", AChar '\x0541', True) -- U+0541 ARMENIAN CAPITAL LETTER JA
  , (0x01000571, "Armenian_dza", AChar '\x0571', True) -- U+0571 ARMENIAN SMALL LETTER JA
  , (0x01000542, "Armenian_GHAT", AChar '\x0542', True) -- U+0542 ARMENIAN CAPITAL LETTER GHAD
  , (0x01000572, "Armenian_ghat", AChar '\x0572', True) -- U+0572 ARMENIAN SMALL LETTER GHAD
  , (0x01000543, "Armenian_TCHE", AChar '\x0543', True) -- U+0543 ARMENIAN CAPITAL LETTER CHEH
  , (0x01000573, "Armenian_tche", AChar '\x0573', True) -- U+0573 ARMENIAN SMALL LETTER CHEH
  , (0x01000544, "Armenian_MEN", AChar '\x0544', True) -- U+0544 ARMENIAN CAPITAL LETTER MEN
  , (0x01000574, "Armenian_men", AChar '\x0574', True) -- U+0574 ARMENIAN SMALL LETTER MEN
  , (0x01000545, "Armenian_HI", AChar '\x0545', True) -- U+0545 ARMENIAN CAPITAL LETTER YI
  , (0x01000575, "Armenian_hi", AChar '\x0575', True) -- U+0575 ARMENIAN SMALL LETTER YI
  , (0x01000546, "Armenian_NU", AChar '\x0546', True) -- U+0546 ARMENIAN CAPITAL LETTER NOW
  , (0x01000576, "Armenian_nu", AChar '\x0576', True) -- U+0576 ARMENIAN SMALL LETTER NOW
  , (0x01000547, "Armenian_SHA", AChar '\x0547', True) -- U+0547 ARMENIAN CAPITAL LETTER SHA
  , (0x01000577, "Armenian_sha", AChar '\x0577', True) -- U+0577 ARMENIAN SMALL LETTER SHA
  , (0x01000548, "Armenian_VO", AChar '\x0548', True) -- U+0548 ARMENIAN CAPITAL LETTER VO
  , (0x01000578, "Armenian_vo", AChar '\x0578', True) -- U+0578 ARMENIAN SMALL LETTER VO
  , (0x01000549, "Armenian_CHA", AChar '\x0549', True) -- U+0549 ARMENIAN CAPITAL LETTER CHA
  , (0x01000579, "Armenian_cha", AChar '\x0579', True) -- U+0579 ARMENIAN SMALL LETTER CHA
  , (0x0100054a, "Armenian_PE", AChar '\x054a', True) -- U+054A ARMENIAN CAPITAL LETTER PEH
  , (0x0100057a, "Armenian_pe", AChar '\x057a', True) -- U+057A ARMENIAN SMALL LETTER PEH
  , (0x0100054b, "Armenian_JE", AChar '\x054b', True) -- U+054B ARMENIAN CAPITAL LETTER JHEH
  , (0x0100057b, "Armenian_je", AChar '\x057b', True) -- U+057B ARMENIAN SMALL LETTER JHEH
  , (0x0100054c, "Armenian_RA", AChar '\x054c', True) -- U+054C ARMENIAN CAPITAL LETTER RA
  , (0x0100057c, "Armenian_ra", AChar '\x057c', True) -- U+057C ARMENIAN SMALL LETTER RA
  , (0x0100054d, "Armenian_SE", AChar '\x054d', True) -- U+054D ARMENIAN CAPITAL LETTER SEH
  , (0x0100057d, "Armenian_se", AChar '\x057d', True) -- U+057D ARMENIAN SMALL LETTER SEH
  , (0x0100054e, "Armenian_VEV", AChar '\x054e', True) -- U+054E ARMENIAN CAPITAL LETTER VEW
  , (0x0100057e, "Armenian_vev", AChar '\x057e', True) -- U+057E ARMENIAN SMALL LETTER VEW
  , (0x0100054f, "Armenian_TYUN", AChar '\x054f', True) -- U+054F ARMENIAN CAPITAL LETTER TIWN
  , (0x0100057f, "Armenian_tyun", AChar '\x057f', True) -- U+057F ARMENIAN SMALL LETTER TIWN
  , (0x01000550, "Armenian_RE", AChar '\x0550', True) -- U+0550 ARMENIAN CAPITAL LETTER REH
  , (0x01000580, "Armenian_re", AChar '\x0580', True) -- U+0580 ARMENIAN SMALL LETTER REH
  , (0x01000551, "Armenian_TSO", AChar '\x0551', True) -- U+0551 ARMENIAN CAPITAL LETTER CO
  , (0x01000581, "Armenian_tso", AChar '\x0581', True) -- U+0581 ARMENIAN SMALL LETTER CO
  , (0x01000552, "Armenian_VYUN", AChar '\x0552', True) -- U+0552 ARMENIAN CAPITAL LETTER YIWN
  , (0x01000582, "Armenian_vyun", AChar '\x0582', True) -- U+0582 ARMENIAN SMALL LETTER YIWN
  , (0x01000553, "Armenian_PYUR", AChar '\x0553', True) -- U+0553 ARMENIAN CAPITAL LETTER PIWR
  , (0x01000583, "Armenian_pyur", AChar '\x0583', True) -- U+0583 ARMENIAN SMALL LETTER PIWR
  , (0x01000554, "Armenian_KE", AChar '\x0554', True) -- U+0554 ARMENIAN CAPITAL LETTER KEH
  , (0x01000584, "Armenian_ke", AChar '\x0584', True) -- U+0584 ARMENIAN SMALL LETTER KEH
  , (0x01000555, "Armenian_O", AChar '\x0555', True) -- U+0555 ARMENIAN CAPITAL LETTER OH
  , (0x01000585, "Armenian_o", AChar '\x0585', True) -- U+0585 ARMENIAN SMALL LETTER OH
  , (0x01000556, "Armenian_FE", AChar '\x0556', True) -- U+0556 ARMENIAN CAPITAL LETTER FEH
  , (0x01000586, "Armenian_fe", AChar '\x0586', True) -- U+0586 ARMENIAN SMALL LETTER FEH
  , (0x0100055a, "Armenian_apostrophe", AChar '\x055a', True) -- U+055A ARMENIAN APOSTROPHE
  --
  -- Georgian
  --
  , (0x010010d0, "Georgian_an", AChar '\x10d0', True) -- U+10D0 GEORGIAN LETTER AN
  , (0x010010d1, "Georgian_ban", AChar '\x10d1', True) -- U+10D1 GEORGIAN LETTER BAN
  , (0x010010d2, "Georgian_gan", AChar '\x10d2', True) -- U+10D2 GEORGIAN LETTER GAN
  , (0x010010d3, "Georgian_don", AChar '\x10d3', True) -- U+10D3 GEORGIAN LETTER DON
  , (0x010010d4, "Georgian_en", AChar '\x10d4', True) -- U+10D4 GEORGIAN LETTER EN
  , (0x010010d5, "Georgian_vin", AChar '\x10d5', True) -- U+10D5 GEORGIAN LETTER VIN
  , (0x010010d6, "Georgian_zen", AChar '\x10d6', True) -- U+10D6 GEORGIAN LETTER ZEN
  , (0x010010d7, "Georgian_tan", AChar '\x10d7', True) -- U+10D7 GEORGIAN LETTER TAN
  , (0x010010d8, "Georgian_in", AChar '\x10d8', True) -- U+10D8 GEORGIAN LETTER IN
  , (0x010010d9, "Georgian_kan", AChar '\x10d9', True) -- U+10D9 GEORGIAN LETTER KAN
  , (0x010010da, "Georgian_las", AChar '\x10da', True) -- U+10DA GEORGIAN LETTER LAS
  , (0x010010db, "Georgian_man", AChar '\x10db', True) -- U+10DB GEORGIAN LETTER MAN
  , (0x010010dc, "Georgian_nar", AChar '\x10dc', True) -- U+10DC GEORGIAN LETTER NAR
  , (0x010010dd, "Georgian_on", AChar '\x10dd', True) -- U+10DD GEORGIAN LETTER ON
  , (0x010010de, "Georgian_par", AChar '\x10de', True) -- U+10DE GEORGIAN LETTER PAR
  , (0x010010df, "Georgian_zhar", AChar '\x10df', True) -- U+10DF GEORGIAN LETTER ZHAR
  , (0x010010e0, "Georgian_rae", AChar '\x10e0', True) -- U+10E0 GEORGIAN LETTER RAE
  , (0x010010e1, "Georgian_san", AChar '\x10e1', True) -- U+10E1 GEORGIAN LETTER SAN
  , (0x010010e2, "Georgian_tar", AChar '\x10e2', True) -- U+10E2 GEORGIAN LETTER TAR
  , (0x010010e3, "Georgian_un", AChar '\x10e3', True) -- U+10E3 GEORGIAN LETTER UN
  , (0x010010e4, "Georgian_phar", AChar '\x10e4', True) -- U+10E4 GEORGIAN LETTER PHAR
  , (0x010010e5, "Georgian_khar", AChar '\x10e5', True) -- U+10E5 GEORGIAN LETTER KHAR
  , (0x010010e6, "Georgian_ghan", AChar '\x10e6', True) -- U+10E6 GEORGIAN LETTER GHAN
  , (0x010010e7, "Georgian_qar", AChar '\x10e7', True) -- U+10E7 GEORGIAN LETTER QAR
  , (0x010010e8, "Georgian_shin", AChar '\x10e8', True) -- U+10E8 GEORGIAN LETTER SHIN
  , (0x010010e9, "Georgian_chin", AChar '\x10e9', True) -- U+10E9 GEORGIAN LETTER CHIN
  , (0x010010ea, "Georgian_can", AChar '\x10ea', True) -- U+10EA GEORGIAN LETTER CAN
  , (0x010010eb, "Georgian_jil", AChar '\x10eb', True) -- U+10EB GEORGIAN LETTER JIL
  , (0x010010ec, "Georgian_cil", AChar '\x10ec', True) -- U+10EC GEORGIAN LETTER CIL
  , (0x010010ed, "Georgian_char", AChar '\x10ed', True) -- U+10ED GEORGIAN LETTER CHAR
  , (0x010010ee, "Georgian_xan", AChar '\x10ee', True) -- U+10EE GEORGIAN LETTER XAN
  , (0x010010ef, "Georgian_jhan", AChar '\x10ef', True) -- U+10EF GEORGIAN LETTER JHAN
  , (0x010010f0, "Georgian_hae", AChar '\x10f0', True) -- U+10F0 GEORGIAN LETTER HAE
  , (0x010010f1, "Georgian_he", AChar '\x10f1', True) -- U+10F1 GEORGIAN LETTER HE
  , (0x010010f2, "Georgian_hie", AChar '\x10f2', True) -- U+10F2 GEORGIAN LETTER HIE
  , (0x010010f3, "Georgian_we", AChar '\x10f3', True) -- U+10F3 GEORGIAN LETTER WE
  , (0x010010f4, "Georgian_har", AChar '\x10f4', True) -- U+10F4 GEORGIAN LETTER HAR
  , (0x010010f5, "Georgian_hoe", AChar '\x10f5', True) -- U+10F5 GEORGIAN LETTER HOE
  , (0x010010f6, "Georgian_fi", AChar '\x10f6', True) -- U+10F6 GEORGIAN LETTER FI
  --
  -- Azeri (and other Turkic or Caucasian languages)
  --
  , (0x01001e8a, "Xabovedot", AChar '\x1e8a', True) -- U+1E8A LATIN CAPITAL LETTER X WITH DOT ABOVE
  , (0x0100012c, "Ibreve", AChar '\x012c', True) -- U+012C LATIN CAPITAL LETTER I WITH BREVE
  , (0x010001b5, "Zstroke", AChar '\x01b5', True) -- U+01B5 LATIN CAPITAL LETTER Z WITH STROKE
  , (0x010001e6, "Gcaron", AChar '\x01e6', True) -- U+01E6 LATIN CAPITAL LETTER G WITH CARON
  , (0x010001d1, "Ocaron", AChar '\x01d1', True) -- U+01D1 LATIN CAPITAL LETTER O WITH CARON
  , (0x0100019f, "Obarred", AChar '\x019f', True) -- U+019F LATIN CAPITAL LETTER O WITH MIDDLE TILDE
  , (0x01001e8b, "xabovedot", AChar '\x1e8b', True) -- U+1E8B LATIN SMALL LETTER X WITH DOT ABOVE
  , (0x0100012d, "ibreve", AChar '\x012d', True) -- U+012D LATIN SMALL LETTER I WITH BREVE
  , (0x010001b6, "zstroke", AChar '\x01b6', True) -- U+01B6 LATIN SMALL LETTER Z WITH STROKE
  , (0x010001e7, "gcaron", AChar '\x01e7', True) -- U+01E7 LATIN SMALL LETTER G WITH CARON
  , (0x010001d2, "ocaron", AChar '\x01d2', True) -- U+01D2 LATIN SMALL LETTER O WITH CARON
  , (0x01000275, "obarred", AChar '\x0275', True) -- U+0275 LATIN SMALL LETTER BARRED O
  , (0x0100018f, "SCHWA", AChar '\x018f', True) -- U+018F LATIN CAPITAL LETTER SCHWA
  , (0x01000259, "schwa", AChar '\x0259', True) -- U+0259 LATIN SMALL LETTER SCHWA
  , (0x010001b7, "EZH", AChar '\x01b7', True) -- U+01B7 LATIN CAPITAL LETTER EZH
  , (0x01000292, "ezh", AChar '\x0292', True) -- U+0292 LATIN SMALL LETTER EZH
  , (0x01001e36, "Lbelowdot", AChar '\x1e36', True) -- U+1E36 LATIN CAPITAL LETTER L WITH DOT BELOW
  , (0x01001e37, "lbelowdot", AChar '\x1e37', True) -- U+1E37 LATIN SMALL LETTER L WITH DOT BELOW
  --
  -- Vietnamese
  --
  , (0x01001ea0, "Abelowdot", AChar '\x1ea0', True) -- U+1EA0 LATIN CAPITAL LETTER A WITH DOT BELOW
  , (0x01001ea1, "abelowdot", AChar '\x1ea1', True) -- U+1EA1 LATIN SMALL LETTER A WITH DOT BELOW
  , (0x01001ea2, "Ahook", AChar '\x1ea2', True) -- U+1EA2 LATIN CAPITAL LETTER A WITH HOOK ABOVE
  , (0x01001ea3, "ahook", AChar '\x1ea3', True) -- U+1EA3 LATIN SMALL LETTER A WITH HOOK ABOVE
  , (0x01001ea4, "Acircumflexacute", AChar '\x1ea4', True) -- U+1EA4 LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND ACUTE
  , (0x01001ea5, "acircumflexacute", AChar '\x1ea5', True) -- U+1EA5 LATIN SMALL LETTER A WITH CIRCUMFLEX AND ACUTE
  , (0x01001ea6, "Acircumflexgrave", AChar '\x1ea6', True) -- U+1EA6 LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND GRAVE
  , (0x01001ea7, "acircumflexgrave", AChar '\x1ea7', True) -- U+1EA7 LATIN SMALL LETTER A WITH CIRCUMFLEX AND GRAVE
  , (0x01001ea8, "Acircumflexhook", AChar '\x1ea8', True) -- U+1EA8 LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND HOOK ABOVE
  , (0x01001ea9, "acircumflexhook", AChar '\x1ea9', True) -- U+1EA9 LATIN SMALL LETTER A WITH CIRCUMFLEX AND HOOK ABOVE
  , (0x01001eaa, "Acircumflextilde", AChar '\x1eaa', True) -- U+1EAA LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND TILDE
  , (0x01001eab, "acircumflextilde", AChar '\x1eab', True) -- U+1EAB LATIN SMALL LETTER A WITH CIRCUMFLEX AND TILDE
  , (0x01001eac, "Acircumflexbelowdot", AChar '\x1eac', True) -- U+1EAC LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND DOT BELOW
  , (0x01001ead, "acircumflexbelowdot", AChar '\x1ead', True) -- U+1EAD LATIN SMALL LETTER A WITH CIRCUMFLEX AND DOT BELOW
  , (0x01001eae, "Abreveacute", AChar '\x1eae', True) -- U+1EAE LATIN CAPITAL LETTER A WITH BREVE AND ACUTE
  , (0x01001eaf, "abreveacute", AChar '\x1eaf', True) -- U+1EAF LATIN SMALL LETTER A WITH BREVE AND ACUTE
  , (0x01001eb0, "Abrevegrave", AChar '\x1eb0', True) -- U+1EB0 LATIN CAPITAL LETTER A WITH BREVE AND GRAVE
  , (0x01001eb1, "abrevegrave", AChar '\x1eb1', True) -- U+1EB1 LATIN SMALL LETTER A WITH BREVE AND GRAVE
  , (0x01001eb2, "Abrevehook", AChar '\x1eb2', True) -- U+1EB2 LATIN CAPITAL LETTER A WITH BREVE AND HOOK ABOVE
  , (0x01001eb3, "abrevehook", AChar '\x1eb3', True) -- U+1EB3 LATIN SMALL LETTER A WITH BREVE AND HOOK ABOVE
  , (0x01001eb4, "Abrevetilde", AChar '\x1eb4', True) -- U+1EB4 LATIN CAPITAL LETTER A WITH BREVE AND TILDE
  , (0x01001eb5, "abrevetilde", AChar '\x1eb5', True) -- U+1EB5 LATIN SMALL LETTER A WITH BREVE AND TILDE
  , (0x01001eb6, "Abrevebelowdot", AChar '\x1eb6', True) -- U+1EB6 LATIN CAPITAL LETTER A WITH BREVE AND DOT BELOW
  , (0x01001eb7, "abrevebelowdot", AChar '\x1eb7', True) -- U+1EB7 LATIN SMALL LETTER A WITH BREVE AND DOT BELOW
  , (0x01001eb8, "Ebelowdot", AChar '\x1eb8', True) -- U+1EB8 LATIN CAPITAL LETTER E WITH DOT BELOW
  , (0x01001eb9, "ebelowdot", AChar '\x1eb9', True) -- U+1EB9 LATIN SMALL LETTER E WITH DOT BELOW
  , (0x01001eba, "Ehook", AChar '\x1eba', True) -- U+1EBA LATIN CAPITAL LETTER E WITH HOOK ABOVE
  , (0x01001ebb, "ehook", AChar '\x1ebb', True) -- U+1EBB LATIN SMALL LETTER E WITH HOOK ABOVE
  , (0x01001ebc, "Etilde", AChar '\x1ebc', True) -- U+1EBC LATIN CAPITAL LETTER E WITH TILDE
  , (0x01001ebd, "etilde", AChar '\x1ebd', True) -- U+1EBD LATIN SMALL LETTER E WITH TILDE
  , (0x01001ebe, "Ecircumflexacute", AChar '\x1ebe', True) -- U+1EBE LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND ACUTE
  , (0x01001ebf, "ecircumflexacute", AChar '\x1ebf', True) -- U+1EBF LATIN SMALL LETTER E WITH CIRCUMFLEX AND ACUTE
  , (0x01001ec0, "Ecircumflexgrave", AChar '\x1ec0', True) -- U+1EC0 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND GRAVE
  , (0x01001ec1, "ecircumflexgrave", AChar '\x1ec1', True) -- U+1EC1 LATIN SMALL LETTER E WITH CIRCUMFLEX AND GRAVE
  , (0x01001ec2, "Ecircumflexhook", AChar '\x1ec2', True) -- U+1EC2 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND HOOK ABOVE
  , (0x01001ec3, "ecircumflexhook", AChar '\x1ec3', True) -- U+1EC3 LATIN SMALL LETTER E WITH CIRCUMFLEX AND HOOK ABOVE
  , (0x01001ec4, "Ecircumflextilde", AChar '\x1ec4', True) -- U+1EC4 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND TILDE
  , (0x01001ec5, "ecircumflextilde", AChar '\x1ec5', True) -- U+1EC5 LATIN SMALL LETTER E WITH CIRCUMFLEX AND TILDE
  , (0x01001ec6, "Ecircumflexbelowdot", AChar '\x1ec6', True) -- U+1EC6 LATIN CAPITAL LETTER E WITH CIRCUMFLEX AND DOT BELOW
  , (0x01001ec7, "ecircumflexbelowdot", AChar '\x1ec7', True) -- U+1EC7 LATIN SMALL LETTER E WITH CIRCUMFLEX AND DOT BELOW
  , (0x01001ec8, "Ihook", AChar '\x1ec8', True) -- U+1EC8 LATIN CAPITAL LETTER I WITH HOOK ABOVE
  , (0x01001ec9, "ihook", AChar '\x1ec9', True) -- U+1EC9 LATIN SMALL LETTER I WITH HOOK ABOVE
  , (0x01001eca, "Ibelowdot", AChar '\x1eca', True) -- U+1ECA LATIN CAPITAL LETTER I WITH DOT BELOW
  , (0x01001ecb, "ibelowdot", AChar '\x1ecb', True) -- U+1ECB LATIN SMALL LETTER I WITH DOT BELOW
  , (0x01001ecc, "Obelowdot", AChar '\x1ecc', True) -- U+1ECC LATIN CAPITAL LETTER O WITH DOT BELOW
  , (0x01001ecd, "obelowdot", AChar '\x1ecd', True) -- U+1ECD LATIN SMALL LETTER O WITH DOT BELOW
  , (0x01001ece, "Ohook", AChar '\x1ece', True) -- U+1ECE LATIN CAPITAL LETTER O WITH HOOK ABOVE
  , (0x01001ecf, "ohook", AChar '\x1ecf', True) -- U+1ECF LATIN SMALL LETTER O WITH HOOK ABOVE
  , (0x01001ed0, "Ocircumflexacute", AChar '\x1ed0', True) -- U+1ED0 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND ACUTE
  , (0x01001ed1, "ocircumflexacute", AChar '\x1ed1', True) -- U+1ED1 LATIN SMALL LETTER O WITH CIRCUMFLEX AND ACUTE
  , (0x01001ed2, "Ocircumflexgrave", AChar '\x1ed2', True) -- U+1ED2 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND GRAVE
  , (0x01001ed3, "ocircumflexgrave", AChar '\x1ed3', True) -- U+1ED3 LATIN SMALL LETTER O WITH CIRCUMFLEX AND GRAVE
  , (0x01001ed4, "Ocircumflexhook", AChar '\x1ed4', True) -- U+1ED4 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND HOOK ABOVE
  , (0x01001ed5, "ocircumflexhook", AChar '\x1ed5', True) -- U+1ED5 LATIN SMALL LETTER O WITH CIRCUMFLEX AND HOOK ABOVE
  , (0x01001ed6, "Ocircumflextilde", AChar '\x1ed6', True) -- U+1ED6 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND TILDE
  , (0x01001ed7, "ocircumflextilde", AChar '\x1ed7', True) -- U+1ED7 LATIN SMALL LETTER O WITH CIRCUMFLEX AND TILDE
  , (0x01001ed8, "Ocircumflexbelowdot", AChar '\x1ed8', True) -- U+1ED8 LATIN CAPITAL LETTER O WITH CIRCUMFLEX AND DOT BELOW
  , (0x01001ed9, "ocircumflexbelowdot", AChar '\x1ed9', True) -- U+1ED9 LATIN SMALL LETTER O WITH CIRCUMFLEX AND DOT BELOW
  , (0x01001eda, "Ohornacute", AChar '\x1eda', True) -- U+1EDA LATIN CAPITAL LETTER O WITH HORN AND ACUTE
  , (0x01001edb, "ohornacute", AChar '\x1edb', True) -- U+1EDB LATIN SMALL LETTER O WITH HORN AND ACUTE
  , (0x01001edc, "Ohorngrave", AChar '\x1edc', True) -- U+1EDC LATIN CAPITAL LETTER O WITH HORN AND GRAVE
  , (0x01001edd, "ohorngrave", AChar '\x1edd', True) -- U+1EDD LATIN SMALL LETTER O WITH HORN AND GRAVE
  , (0x01001ede, "Ohornhook", AChar '\x1ede', True) -- U+1EDE LATIN CAPITAL LETTER O WITH HORN AND HOOK ABOVE
  , (0x01001edf, "ohornhook", AChar '\x1edf', True) -- U+1EDF LATIN SMALL LETTER O WITH HORN AND HOOK ABOVE
  , (0x01001ee0, "Ohorntilde", AChar '\x1ee0', True) -- U+1EE0 LATIN CAPITAL LETTER O WITH HORN AND TILDE
  , (0x01001ee1, "ohorntilde", AChar '\x1ee1', True) -- U+1EE1 LATIN SMALL LETTER O WITH HORN AND TILDE
  , (0x01001ee2, "Ohornbelowdot", AChar '\x1ee2', True) -- U+1EE2 LATIN CAPITAL LETTER O WITH HORN AND DOT BELOW
  , (0x01001ee3, "ohornbelowdot", AChar '\x1ee3', True) -- U+1EE3 LATIN SMALL LETTER O WITH HORN AND DOT BELOW
  , (0x01001ee4, "Ubelowdot", AChar '\x1ee4', True) -- U+1EE4 LATIN CAPITAL LETTER U WITH DOT BELOW
  , (0x01001ee5, "ubelowdot", AChar '\x1ee5', True) -- U+1EE5 LATIN SMALL LETTER U WITH DOT BELOW
  , (0x01001ee6, "Uhook", AChar '\x1ee6', True) -- U+1EE6 LATIN CAPITAL LETTER U WITH HOOK ABOVE
  , (0x01001ee7, "uhook", AChar '\x1ee7', True) -- U+1EE7 LATIN SMALL LETTER U WITH HOOK ABOVE
  , (0x01001ee8, "Uhornacute", AChar '\x1ee8', True) -- U+1EE8 LATIN CAPITAL LETTER U WITH HORN AND ACUTE
  , (0x01001ee9, "uhornacute", AChar '\x1ee9', True) -- U+1EE9 LATIN SMALL LETTER U WITH HORN AND ACUTE
  , (0x01001eea, "Uhorngrave", AChar '\x1eea', True) -- U+1EEA LATIN CAPITAL LETTER U WITH HORN AND GRAVE
  , (0x01001eeb, "uhorngrave", AChar '\x1eeb', True) -- U+1EEB LATIN SMALL LETTER U WITH HORN AND GRAVE
  , (0x01001eec, "Uhornhook", AChar '\x1eec', True) -- U+1EEC LATIN CAPITAL LETTER U WITH HORN AND HOOK ABOVE
  , (0x01001eed, "uhornhook", AChar '\x1eed', True) -- U+1EED LATIN SMALL LETTER U WITH HORN AND HOOK ABOVE
  , (0x01001eee, "Uhorntilde", AChar '\x1eee', True) -- U+1EEE LATIN CAPITAL LETTER U WITH HORN AND TILDE
  , (0x01001eef, "uhorntilde", AChar '\x1eef', True) -- U+1EEF LATIN SMALL LETTER U WITH HORN AND TILDE
  , (0x01001ef0, "Uhornbelowdot", AChar '\x1ef0', True) -- U+1EF0 LATIN CAPITAL LETTER U WITH HORN AND DOT BELOW
  , (0x01001ef1, "uhornbelowdot", AChar '\x1ef1', True) -- U+1EF1 LATIN SMALL LETTER U WITH HORN AND DOT BELOW
  , (0x01001ef4, "Ybelowdot", AChar '\x1ef4', True) -- U+1EF4 LATIN CAPITAL LETTER Y WITH DOT BELOW
  , (0x01001ef5, "ybelowdot", AChar '\x1ef5', True) -- U+1EF5 LATIN SMALL LETTER Y WITH DOT BELOW
  , (0x01001ef6, "Yhook", AChar '\x1ef6', True) -- U+1EF6 LATIN CAPITAL LETTER Y WITH HOOK ABOVE
  , (0x01001ef7, "yhook", AChar '\x1ef7', True) -- U+1EF7 LATIN SMALL LETTER Y WITH HOOK ABOVE
  , (0x01001ef8, "Ytilde", AChar '\x1ef8', True) -- U+1EF8 LATIN CAPITAL LETTER Y WITH TILDE
  , (0x01001ef9, "ytilde", AChar '\x1ef9', True) -- U+1EF9 LATIN SMALL LETTER Y WITH TILDE
  , (0x010001a0, "Ohorn", AChar '\x01a0', True) -- U+01A0 LATIN CAPITAL LETTER O WITH HORN
  , (0x010001a1, "ohorn", AChar '\x01a1', True) -- U+01A1 LATIN SMALL LETTER O WITH HORN
  , (0x010001af, "Uhorn", AChar '\x01af', True) -- U+01AF LATIN CAPITAL LETTER U WITH HORN
  , (0x010001b0, "uhorn", AChar '\x01b0', True) -- U+01B0 LATIN SMALL LETTER U WITH HORN
  --
  -- Currencies
  --
  , (0x010020a0, "EcuSign", AChar '\x20a0', True) -- U+20A0 EURO-CURRENCY SIGN
  , (0x010020a1, "ColonSign", AChar '\x20a1', True) -- U+20A1 COLON SIGN
  , (0x010020a2, "CruzeiroSign", AChar '\x20a2', True) -- U+20A2 CRUZEIRO SIGN
  , (0x010020a3, "FFrancSign", AChar '\x20a3', True) -- U+20A3 FRENCH FRANC SIGN
  , (0x010020a4, "LiraSign", AChar '\x20a4', True) -- U+20A4 LIRA SIGN
  , (0x010020a5, "MillSign", AChar '\x20a5', True) -- U+20A5 MILL SIGN
  , (0x010020a6, "NairaSign", AChar '\x20a6', True) -- U+20A6 NAIRA SIGN
  , (0x010020a7, "PesetaSign", AChar '\x20a7', True) -- U+20A7 PESETA SIGN
  , (0x010020a8, "RupeeSign", AChar '\x20a8', True) -- U+20A8 RUPEE SIGN
  , (0x010020a9, "WonSign", AChar '\x20a9', True) -- U+20A9 WON SIGN
  , (0x010020aa, "NewSheqelSign", AChar '\x20aa', True) -- U+20AA NEW SHEQEL SIGN
  , (0x010020ab, "DongSign", AChar '\x20ab', True) -- U+20AB DONG SIGN
  , (0x000020ac, "EuroSign", AChar '\x20ac', True) -- U+20AC EURO SIGN
  --
  -- Maths
  --
  , (0x01002070, "zerosuperior", AChar '\x2070', True) -- U+2070 SUPERSCRIPT ZERO
  , (0x01002074, "foursuperior", AChar '\x2074', True) -- U+2074 SUPERSCRIPT FOUR
  , (0x01002075, "fivesuperior", AChar '\x2075', True) -- U+2075 SUPERSCRIPT FIVE
  , (0x01002076, "sixsuperior", AChar '\x2076', True) -- U+2076 SUPERSCRIPT SIX
  , (0x01002077, "sevensuperior", AChar '\x2077', True) -- U+2077 SUPERSCRIPT SEVEN
  , (0x01002078, "eightsuperior", AChar '\x2078', True) -- U+2078 SUPERSCRIPT EIGHT
  , (0x01002079, "ninesuperior", AChar '\x2079', True) -- U+2079 SUPERSCRIPT NINE
  , (0x01002080, "zerosubscript", AChar '\x2080', True) -- U+2080 SUBSCRIPT ZERO
  , (0x01002081, "onesubscript", AChar '\x2081', True) -- U+2081 SUBSCRIPT ONE
  , (0x01002082, "twosubscript", AChar '\x2082', True) -- U+2082 SUBSCRIPT TWO
  , (0x01002083, "threesubscript", AChar '\x2083', True) -- U+2083 SUBSCRIPT THREE
  , (0x01002084, "foursubscript", AChar '\x2084', True) -- U+2084 SUBSCRIPT FOUR
  , (0x01002085, "fivesubscript", AChar '\x2085', True) -- U+2085 SUBSCRIPT FIVE
  , (0x01002086, "sixsubscript", AChar '\x2086', True) -- U+2086 SUBSCRIPT SIX
  , (0x01002087, "sevensubscript", AChar '\x2087', True) -- U+2087 SUBSCRIPT SEVEN
  , (0x01002088, "eightsubscript", AChar '\x2088', True) -- U+2088 SUBSCRIPT EIGHT
  , (0x01002089, "ninesubscript", AChar '\x2089', True) -- U+2089 SUBSCRIPT NINE
  , (0x01002202, "partdifferential", AChar '\x2202', False) -- U+2202 PARTIAL DIFFERENTIAL
  , (0x01002205, "emptyset", AChar '\x2205', True) -- U+2205 EMPTY SET
  , (0x01002208, "elementof", AChar '\x2208', True) -- U+2208 ELEMENT OF
  , (0x01002209, "notelementof", AChar '\x2209', True) -- U+2209 NOT AN ELEMENT OF
  , (0x0100220b, "containsas", AChar '\x220b', True) -- U+220B CONTAINS AS MEMBER
  , (0x0100221a, "squareroot", AChar '\x221a', False) -- U+221A SQUARE ROOT
  , (0x0100221b, "cuberoot", AChar '\x221b', True) -- U+221B CUBE ROOT
  , (0x0100221c, "fourthroot", AChar '\x221c', True) -- U+221C FOURTH ROOT
  , (0x0100222c, "dintegral", AChar '\x222c', True) -- U+222C DOUBLE INTEGRAL
  , (0x0100222d, "tintegral", AChar '\x222d', True) -- U+222D TRIPLE INTEGRAL
  , (0x01002235, "because", AChar '\x2235', True) -- U+2235 BECAUSE
  , (0x01002248, "approxeq", AChar '\x2248', False) -- U+2248 ALMOST EQUAL TO [WARNING] Ambiguous keysym: conflict on its Unicode value between C header comment (U+2248 ALMOST EQUAL TO) and xkb_keysym_to_utf32 (U+2245 APPROXIMATELY EQUAL TO)
  , (0x01002247, "notapproxeq", AChar '\x2247', True) -- U+2247 NEITHER APPROXIMATELY NOR ACTUALLY EQUAL TO
  , (0x01002262, "notidentical", AChar '\x2262', True) -- U+2262 NOT IDENTICAL TO
  , (0x01002263, "stricteq", AChar '\x2263', True) -- U+2263 STRICTLY EQUIVALENT TO
  --
  -- Braille
  --
  , (0x0000fff1, "braille_dot_1", ASpecial Braille_Dot_1, True)
  , (0x0000fff2, "braille_dot_2", ASpecial Braille_Dot_2, True)
  , (0x0000fff3, "braille_dot_3", ASpecial Braille_Dot_3, True)
  , (0x0000fff4, "braille_dot_4", ASpecial Braille_Dot_4, True)
  , (0x0000fff5, "braille_dot_5", ASpecial Braille_Dot_5, True)
  , (0x0000fff6, "braille_dot_6", ASpecial Braille_Dot_6, True)
  , (0x0000fff7, "braille_dot_7", ASpecial Braille_Dot_7, True)
  , (0x0000fff8, "braille_dot_8", ASpecial Braille_Dot_8, True)
  , (0x0000fff9, "braille_dot_9", ASpecial Braille_Dot_9, True)
  , (0x0000fffa, "braille_dot_10", ASpecial Braille_Dot_10, True)
  --
  -- Braille
  --
  , (0x01002800, "braille_blank", AChar '\x2800', True) -- U+2800 BRAILLE PATTERN BLANK
  , (0x01002801, "braille_dots_1", AChar '\x2801', True) -- U+2801 BRAILLE PATTERN DOTS-1
  , (0x01002802, "braille_dots_2", AChar '\x2802', True) -- U+2802 BRAILLE PATTERN DOTS-2
  , (0x01002803, "braille_dots_12", AChar '\x2803', True) -- U+2803 BRAILLE PATTERN DOTS-12
  , (0x01002804, "braille_dots_3", AChar '\x2804', True) -- U+2804 BRAILLE PATTERN DOTS-3
  , (0x01002805, "braille_dots_13", AChar '\x2805', True) -- U+2805 BRAILLE PATTERN DOTS-13
  , (0x01002806, "braille_dots_23", AChar '\x2806', True) -- U+2806 BRAILLE PATTERN DOTS-23
  , (0x01002807, "braille_dots_123", AChar '\x2807', True) -- U+2807 BRAILLE PATTERN DOTS-123
  , (0x01002808, "braille_dots_4", AChar '\x2808', True) -- U+2808 BRAILLE PATTERN DOTS-4
  , (0x01002809, "braille_dots_14", AChar '\x2809', True) -- U+2809 BRAILLE PATTERN DOTS-14
  , (0x0100280a, "braille_dots_24", AChar '\x280a', True) -- U+280A BRAILLE PATTERN DOTS-24
  , (0x0100280b, "braille_dots_124", AChar '\x280b', True) -- U+280B BRAILLE PATTERN DOTS-124
  , (0x0100280c, "braille_dots_34", AChar '\x280c', True) -- U+280C BRAILLE PATTERN DOTS-34
  , (0x0100280d, "braille_dots_134", AChar '\x280d', True) -- U+280D BRAILLE PATTERN DOTS-134
  , (0x0100280e, "braille_dots_234", AChar '\x280e', True) -- U+280E BRAILLE PATTERN DOTS-234
  , (0x0100280f, "braille_dots_1234", AChar '\x280f', True) -- U+280F BRAILLE PATTERN DOTS-1234
  , (0x01002810, "braille_dots_5", AChar '\x2810', True) -- U+2810 BRAILLE PATTERN DOTS-5
  , (0x01002811, "braille_dots_15", AChar '\x2811', True) -- U+2811 BRAILLE PATTERN DOTS-15
  , (0x01002812, "braille_dots_25", AChar '\x2812', True) -- U+2812 BRAILLE PATTERN DOTS-25
  , (0x01002813, "braille_dots_125", AChar '\x2813', True) -- U+2813 BRAILLE PATTERN DOTS-125
  , (0x01002814, "braille_dots_35", AChar '\x2814', True) -- U+2814 BRAILLE PATTERN DOTS-35
  , (0x01002815, "braille_dots_135", AChar '\x2815', True) -- U+2815 BRAILLE PATTERN DOTS-135
  , (0x01002816, "braille_dots_235", AChar '\x2816', True) -- U+2816 BRAILLE PATTERN DOTS-235
  , (0x01002817, "braille_dots_1235", AChar '\x2817', True) -- U+2817 BRAILLE PATTERN DOTS-1235
  , (0x01002818, "braille_dots_45", AChar '\x2818', True) -- U+2818 BRAILLE PATTERN DOTS-45
  , (0x01002819, "braille_dots_145", AChar '\x2819', True) -- U+2819 BRAILLE PATTERN DOTS-145
  , (0x0100281a, "braille_dots_245", AChar '\x281a', True) -- U+281A BRAILLE PATTERN DOTS-245
  , (0x0100281b, "braille_dots_1245", AChar '\x281b', True) -- U+281B BRAILLE PATTERN DOTS-1245
  , (0x0100281c, "braille_dots_345", AChar '\x281c', True) -- U+281C BRAILLE PATTERN DOTS-345
  , (0x0100281d, "braille_dots_1345", AChar '\x281d', True) -- U+281D BRAILLE PATTERN DOTS-1345
  , (0x0100281e, "braille_dots_2345", AChar '\x281e', True) -- U+281E BRAILLE PATTERN DOTS-2345
  , (0x0100281f, "braille_dots_12345", AChar '\x281f', True) -- U+281F BRAILLE PATTERN DOTS-12345
  , (0x01002820, "braille_dots_6", AChar '\x2820', True) -- U+2820 BRAILLE PATTERN DOTS-6
  , (0x01002821, "braille_dots_16", AChar '\x2821', True) -- U+2821 BRAILLE PATTERN DOTS-16
  , (0x01002822, "braille_dots_26", AChar '\x2822', True) -- U+2822 BRAILLE PATTERN DOTS-26
  , (0x01002823, "braille_dots_126", AChar '\x2823', True) -- U+2823 BRAILLE PATTERN DOTS-126
  , (0x01002824, "braille_dots_36", AChar '\x2824', True) -- U+2824 BRAILLE PATTERN DOTS-36
  , (0x01002825, "braille_dots_136", AChar '\x2825', True) -- U+2825 BRAILLE PATTERN DOTS-136
  , (0x01002826, "braille_dots_236", AChar '\x2826', True) -- U+2826 BRAILLE PATTERN DOTS-236
  , (0x01002827, "braille_dots_1236", AChar '\x2827', True) -- U+2827 BRAILLE PATTERN DOTS-1236
  , (0x01002828, "braille_dots_46", AChar '\x2828', True) -- U+2828 BRAILLE PATTERN DOTS-46
  , (0x01002829, "braille_dots_146", AChar '\x2829', True) -- U+2829 BRAILLE PATTERN DOTS-146
  , (0x0100282a, "braille_dots_246", AChar '\x282a', True) -- U+282A BRAILLE PATTERN DOTS-246
  , (0x0100282b, "braille_dots_1246", AChar '\x282b', True) -- U+282B BRAILLE PATTERN DOTS-1246
  , (0x0100282c, "braille_dots_346", AChar '\x282c', True) -- U+282C BRAILLE PATTERN DOTS-346
  , (0x0100282d, "braille_dots_1346", AChar '\x282d', True) -- U+282D BRAILLE PATTERN DOTS-1346
  , (0x0100282e, "braille_dots_2346", AChar '\x282e', True) -- U+282E BRAILLE PATTERN DOTS-2346
  , (0x0100282f, "braille_dots_12346", AChar '\x282f', True) -- U+282F BRAILLE PATTERN DOTS-12346
  , (0x01002830, "braille_dots_56", AChar '\x2830', True) -- U+2830 BRAILLE PATTERN DOTS-56
  , (0x01002831, "braille_dots_156", AChar '\x2831', True) -- U+2831 BRAILLE PATTERN DOTS-156
  , (0x01002832, "braille_dots_256", AChar '\x2832', True) -- U+2832 BRAILLE PATTERN DOTS-256
  , (0x01002833, "braille_dots_1256", AChar '\x2833', True) -- U+2833 BRAILLE PATTERN DOTS-1256
  , (0x01002834, "braille_dots_356", AChar '\x2834', True) -- U+2834 BRAILLE PATTERN DOTS-356
  , (0x01002835, "braille_dots_1356", AChar '\x2835', True) -- U+2835 BRAILLE PATTERN DOTS-1356
  , (0x01002836, "braille_dots_2356", AChar '\x2836', True) -- U+2836 BRAILLE PATTERN DOTS-2356
  , (0x01002837, "braille_dots_12356", AChar '\x2837', True) -- U+2837 BRAILLE PATTERN DOTS-12356
  , (0x01002838, "braille_dots_456", AChar '\x2838', True) -- U+2838 BRAILLE PATTERN DOTS-456
  , (0x01002839, "braille_dots_1456", AChar '\x2839', True) -- U+2839 BRAILLE PATTERN DOTS-1456
  , (0x0100283a, "braille_dots_2456", AChar '\x283a', True) -- U+283A BRAILLE PATTERN DOTS-2456
  , (0x0100283b, "braille_dots_12456", AChar '\x283b', True) -- U+283B BRAILLE PATTERN DOTS-12456
  , (0x0100283c, "braille_dots_3456", AChar '\x283c', True) -- U+283C BRAILLE PATTERN DOTS-3456
  , (0x0100283d, "braille_dots_13456", AChar '\x283d', True) -- U+283D BRAILLE PATTERN DOTS-13456
  , (0x0100283e, "braille_dots_23456", AChar '\x283e', True) -- U+283E BRAILLE PATTERN DOTS-23456
  , (0x0100283f, "braille_dots_123456", AChar '\x283f', True) -- U+283F BRAILLE PATTERN DOTS-123456
  , (0x01002840, "braille_dots_7", AChar '\x2840', True) -- U+2840 BRAILLE PATTERN DOTS-7
  , (0x01002841, "braille_dots_17", AChar '\x2841', True) -- U+2841 BRAILLE PATTERN DOTS-17
  , (0x01002842, "braille_dots_27", AChar '\x2842', True) -- U+2842 BRAILLE PATTERN DOTS-27
  , (0x01002843, "braille_dots_127", AChar '\x2843', True) -- U+2843 BRAILLE PATTERN DOTS-127
  , (0x01002844, "braille_dots_37", AChar '\x2844', True) -- U+2844 BRAILLE PATTERN DOTS-37
  , (0x01002845, "braille_dots_137", AChar '\x2845', True) -- U+2845 BRAILLE PATTERN DOTS-137
  , (0x01002846, "braille_dots_237", AChar '\x2846', True) -- U+2846 BRAILLE PATTERN DOTS-237
  , (0x01002847, "braille_dots_1237", AChar '\x2847', True) -- U+2847 BRAILLE PATTERN DOTS-1237
  , (0x01002848, "braille_dots_47", AChar '\x2848', True) -- U+2848 BRAILLE PATTERN DOTS-47
  , (0x01002849, "braille_dots_147", AChar '\x2849', True) -- U+2849 BRAILLE PATTERN DOTS-147
  , (0x0100284a, "braille_dots_247", AChar '\x284a', True) -- U+284A BRAILLE PATTERN DOTS-247
  , (0x0100284b, "braille_dots_1247", AChar '\x284b', True) -- U+284B BRAILLE PATTERN DOTS-1247
  , (0x0100284c, "braille_dots_347", AChar '\x284c', True) -- U+284C BRAILLE PATTERN DOTS-347
  , (0x0100284d, "braille_dots_1347", AChar '\x284d', True) -- U+284D BRAILLE PATTERN DOTS-1347
  , (0x0100284e, "braille_dots_2347", AChar '\x284e', True) -- U+284E BRAILLE PATTERN DOTS-2347
  , (0x0100284f, "braille_dots_12347", AChar '\x284f', True) -- U+284F BRAILLE PATTERN DOTS-12347
  , (0x01002850, "braille_dots_57", AChar '\x2850', True) -- U+2850 BRAILLE PATTERN DOTS-57
  , (0x01002851, "braille_dots_157", AChar '\x2851', True) -- U+2851 BRAILLE PATTERN DOTS-157
  , (0x01002852, "braille_dots_257", AChar '\x2852', True) -- U+2852 BRAILLE PATTERN DOTS-257
  , (0x01002853, "braille_dots_1257", AChar '\x2853', True) -- U+2853 BRAILLE PATTERN DOTS-1257
  , (0x01002854, "braille_dots_357", AChar '\x2854', True) -- U+2854 BRAILLE PATTERN DOTS-357
  , (0x01002855, "braille_dots_1357", AChar '\x2855', True) -- U+2855 BRAILLE PATTERN DOTS-1357
  , (0x01002856, "braille_dots_2357", AChar '\x2856', True) -- U+2856 BRAILLE PATTERN DOTS-2357
  , (0x01002857, "braille_dots_12357", AChar '\x2857', True) -- U+2857 BRAILLE PATTERN DOTS-12357
  , (0x01002858, "braille_dots_457", AChar '\x2858', True) -- U+2858 BRAILLE PATTERN DOTS-457
  , (0x01002859, "braille_dots_1457", AChar '\x2859', True) -- U+2859 BRAILLE PATTERN DOTS-1457
  , (0x0100285a, "braille_dots_2457", AChar '\x285a', True) -- U+285A BRAILLE PATTERN DOTS-2457
  , (0x0100285b, "braille_dots_12457", AChar '\x285b', True) -- U+285B BRAILLE PATTERN DOTS-12457
  , (0x0100285c, "braille_dots_3457", AChar '\x285c', True) -- U+285C BRAILLE PATTERN DOTS-3457
  , (0x0100285d, "braille_dots_13457", AChar '\x285d', True) -- U+285D BRAILLE PATTERN DOTS-13457
  , (0x0100285e, "braille_dots_23457", AChar '\x285e', True) -- U+285E BRAILLE PATTERN DOTS-23457
  , (0x0100285f, "braille_dots_123457", AChar '\x285f', True) -- U+285F BRAILLE PATTERN DOTS-123457
  , (0x01002860, "braille_dots_67", AChar '\x2860', True) -- U+2860 BRAILLE PATTERN DOTS-67
  , (0x01002861, "braille_dots_167", AChar '\x2861', True) -- U+2861 BRAILLE PATTERN DOTS-167
  , (0x01002862, "braille_dots_267", AChar '\x2862', True) -- U+2862 BRAILLE PATTERN DOTS-267
  , (0x01002863, "braille_dots_1267", AChar '\x2863', True) -- U+2863 BRAILLE PATTERN DOTS-1267
  , (0x01002864, "braille_dots_367", AChar '\x2864', True) -- U+2864 BRAILLE PATTERN DOTS-367
  , (0x01002865, "braille_dots_1367", AChar '\x2865', True) -- U+2865 BRAILLE PATTERN DOTS-1367
  , (0x01002866, "braille_dots_2367", AChar '\x2866', True) -- U+2866 BRAILLE PATTERN DOTS-2367
  , (0x01002867, "braille_dots_12367", AChar '\x2867', True) -- U+2867 BRAILLE PATTERN DOTS-12367
  , (0x01002868, "braille_dots_467", AChar '\x2868', True) -- U+2868 BRAILLE PATTERN DOTS-467
  , (0x01002869, "braille_dots_1467", AChar '\x2869', True) -- U+2869 BRAILLE PATTERN DOTS-1467
  , (0x0100286a, "braille_dots_2467", AChar '\x286a', True) -- U+286A BRAILLE PATTERN DOTS-2467
  , (0x0100286b, "braille_dots_12467", AChar '\x286b', True) -- U+286B BRAILLE PATTERN DOTS-12467
  , (0x0100286c, "braille_dots_3467", AChar '\x286c', True) -- U+286C BRAILLE PATTERN DOTS-3467
  , (0x0100286d, "braille_dots_13467", AChar '\x286d', True) -- U+286D BRAILLE PATTERN DOTS-13467
  , (0x0100286e, "braille_dots_23467", AChar '\x286e', True) -- U+286E BRAILLE PATTERN DOTS-23467
  , (0x0100286f, "braille_dots_123467", AChar '\x286f', True) -- U+286F BRAILLE PATTERN DOTS-123467
  , (0x01002870, "braille_dots_567", AChar '\x2870', True) -- U+2870 BRAILLE PATTERN DOTS-567
  , (0x01002871, "braille_dots_1567", AChar '\x2871', True) -- U+2871 BRAILLE PATTERN DOTS-1567
  , (0x01002872, "braille_dots_2567", AChar '\x2872', True) -- U+2872 BRAILLE PATTERN DOTS-2567
  , (0x01002873, "braille_dots_12567", AChar '\x2873', True) -- U+2873 BRAILLE PATTERN DOTS-12567
  , (0x01002874, "braille_dots_3567", AChar '\x2874', True) -- U+2874 BRAILLE PATTERN DOTS-3567
  , (0x01002875, "braille_dots_13567", AChar '\x2875', True) -- U+2875 BRAILLE PATTERN DOTS-13567
  , (0x01002876, "braille_dots_23567", AChar '\x2876', True) -- U+2876 BRAILLE PATTERN DOTS-23567
  , (0x01002877, "braille_dots_123567", AChar '\x2877', True) -- U+2877 BRAILLE PATTERN DOTS-123567
  , (0x01002878, "braille_dots_4567", AChar '\x2878', True) -- U+2878 BRAILLE PATTERN DOTS-4567
  , (0x01002879, "braille_dots_14567", AChar '\x2879', True) -- U+2879 BRAILLE PATTERN DOTS-14567
  , (0x0100287a, "braille_dots_24567", AChar '\x287a', True) -- U+287A BRAILLE PATTERN DOTS-24567
  , (0x0100287b, "braille_dots_124567", AChar '\x287b', True) -- U+287B BRAILLE PATTERN DOTS-124567
  , (0x0100287c, "braille_dots_34567", AChar '\x287c', True) -- U+287C BRAILLE PATTERN DOTS-34567
  , (0x0100287d, "braille_dots_134567", AChar '\x287d', True) -- U+287D BRAILLE PATTERN DOTS-134567
  , (0x0100287e, "braille_dots_234567", AChar '\x287e', True) -- U+287E BRAILLE PATTERN DOTS-234567
  , (0x0100287f, "braille_dots_1234567", AChar '\x287f', True) -- U+287F BRAILLE PATTERN DOTS-1234567
  , (0x01002880, "braille_dots_8", AChar '\x2880', True) -- U+2880 BRAILLE PATTERN DOTS-8
  , (0x01002881, "braille_dots_18", AChar '\x2881', True) -- U+2881 BRAILLE PATTERN DOTS-18
  , (0x01002882, "braille_dots_28", AChar '\x2882', True) -- U+2882 BRAILLE PATTERN DOTS-28
  , (0x01002883, "braille_dots_128", AChar '\x2883', True) -- U+2883 BRAILLE PATTERN DOTS-128
  , (0x01002884, "braille_dots_38", AChar '\x2884', True) -- U+2884 BRAILLE PATTERN DOTS-38
  , (0x01002885, "braille_dots_138", AChar '\x2885', True) -- U+2885 BRAILLE PATTERN DOTS-138
  , (0x01002886, "braille_dots_238", AChar '\x2886', True) -- U+2886 BRAILLE PATTERN DOTS-238
  , (0x01002887, "braille_dots_1238", AChar '\x2887', True) -- U+2887 BRAILLE PATTERN DOTS-1238
  , (0x01002888, "braille_dots_48", AChar '\x2888', True) -- U+2888 BRAILLE PATTERN DOTS-48
  , (0x01002889, "braille_dots_148", AChar '\x2889', True) -- U+2889 BRAILLE PATTERN DOTS-148
  , (0x0100288a, "braille_dots_248", AChar '\x288a', True) -- U+288A BRAILLE PATTERN DOTS-248
  , (0x0100288b, "braille_dots_1248", AChar '\x288b', True) -- U+288B BRAILLE PATTERN DOTS-1248
  , (0x0100288c, "braille_dots_348", AChar '\x288c', True) -- U+288C BRAILLE PATTERN DOTS-348
  , (0x0100288d, "braille_dots_1348", AChar '\x288d', True) -- U+288D BRAILLE PATTERN DOTS-1348
  , (0x0100288e, "braille_dots_2348", AChar '\x288e', True) -- U+288E BRAILLE PATTERN DOTS-2348
  , (0x0100288f, "braille_dots_12348", AChar '\x288f', True) -- U+288F BRAILLE PATTERN DOTS-12348
  , (0x01002890, "braille_dots_58", AChar '\x2890', True) -- U+2890 BRAILLE PATTERN DOTS-58
  , (0x01002891, "braille_dots_158", AChar '\x2891', True) -- U+2891 BRAILLE PATTERN DOTS-158
  , (0x01002892, "braille_dots_258", AChar '\x2892', True) -- U+2892 BRAILLE PATTERN DOTS-258
  , (0x01002893, "braille_dots_1258", AChar '\x2893', True) -- U+2893 BRAILLE PATTERN DOTS-1258
  , (0x01002894, "braille_dots_358", AChar '\x2894', True) -- U+2894 BRAILLE PATTERN DOTS-358
  , (0x01002895, "braille_dots_1358", AChar '\x2895', True) -- U+2895 BRAILLE PATTERN DOTS-1358
  , (0x01002896, "braille_dots_2358", AChar '\x2896', True) -- U+2896 BRAILLE PATTERN DOTS-2358
  , (0x01002897, "braille_dots_12358", AChar '\x2897', True) -- U+2897 BRAILLE PATTERN DOTS-12358
  , (0x01002898, "braille_dots_458", AChar '\x2898', True) -- U+2898 BRAILLE PATTERN DOTS-458
  , (0x01002899, "braille_dots_1458", AChar '\x2899', True) -- U+2899 BRAILLE PATTERN DOTS-1458
  , (0x0100289a, "braille_dots_2458", AChar '\x289a', True) -- U+289A BRAILLE PATTERN DOTS-2458
  , (0x0100289b, "braille_dots_12458", AChar '\x289b', True) -- U+289B BRAILLE PATTERN DOTS-12458
  , (0x0100289c, "braille_dots_3458", AChar '\x289c', True) -- U+289C BRAILLE PATTERN DOTS-3458
  , (0x0100289d, "braille_dots_13458", AChar '\x289d', True) -- U+289D BRAILLE PATTERN DOTS-13458
  , (0x0100289e, "braille_dots_23458", AChar '\x289e', True) -- U+289E BRAILLE PATTERN DOTS-23458
  , (0x0100289f, "braille_dots_123458", AChar '\x289f', True) -- U+289F BRAILLE PATTERN DOTS-123458
  , (0x010028a0, "braille_dots_68", AChar '\x28a0', True) -- U+28A0 BRAILLE PATTERN DOTS-68
  , (0x010028a1, "braille_dots_168", AChar '\x28a1', True) -- U+28A1 BRAILLE PATTERN DOTS-168
  , (0x010028a2, "braille_dots_268", AChar '\x28a2', True) -- U+28A2 BRAILLE PATTERN DOTS-268
  , (0x010028a3, "braille_dots_1268", AChar '\x28a3', True) -- U+28A3 BRAILLE PATTERN DOTS-1268
  , (0x010028a4, "braille_dots_368", AChar '\x28a4', True) -- U+28A4 BRAILLE PATTERN DOTS-368
  , (0x010028a5, "braille_dots_1368", AChar '\x28a5', True) -- U+28A5 BRAILLE PATTERN DOTS-1368
  , (0x010028a6, "braille_dots_2368", AChar '\x28a6', True) -- U+28A6 BRAILLE PATTERN DOTS-2368
  , (0x010028a7, "braille_dots_12368", AChar '\x28a7', True) -- U+28A7 BRAILLE PATTERN DOTS-12368
  , (0x010028a8, "braille_dots_468", AChar '\x28a8', True) -- U+28A8 BRAILLE PATTERN DOTS-468
  , (0x010028a9, "braille_dots_1468", AChar '\x28a9', True) -- U+28A9 BRAILLE PATTERN DOTS-1468
  , (0x010028aa, "braille_dots_2468", AChar '\x28aa', True) -- U+28AA BRAILLE PATTERN DOTS-2468
  , (0x010028ab, "braille_dots_12468", AChar '\x28ab', True) -- U+28AB BRAILLE PATTERN DOTS-12468
  , (0x010028ac, "braille_dots_3468", AChar '\x28ac', True) -- U+28AC BRAILLE PATTERN DOTS-3468
  , (0x010028ad, "braille_dots_13468", AChar '\x28ad', True) -- U+28AD BRAILLE PATTERN DOTS-13468
  , (0x010028ae, "braille_dots_23468", AChar '\x28ae', True) -- U+28AE BRAILLE PATTERN DOTS-23468
  , (0x010028af, "braille_dots_123468", AChar '\x28af', True) -- U+28AF BRAILLE PATTERN DOTS-123468
  , (0x010028b0, "braille_dots_568", AChar '\x28b0', True) -- U+28B0 BRAILLE PATTERN DOTS-568
  , (0x010028b1, "braille_dots_1568", AChar '\x28b1', True) -- U+28B1 BRAILLE PATTERN DOTS-1568
  , (0x010028b2, "braille_dots_2568", AChar '\x28b2', True) -- U+28B2 BRAILLE PATTERN DOTS-2568
  , (0x010028b3, "braille_dots_12568", AChar '\x28b3', True) -- U+28B3 BRAILLE PATTERN DOTS-12568
  , (0x010028b4, "braille_dots_3568", AChar '\x28b4', True) -- U+28B4 BRAILLE PATTERN DOTS-3568
  , (0x010028b5, "braille_dots_13568", AChar '\x28b5', True) -- U+28B5 BRAILLE PATTERN DOTS-13568
  , (0x010028b6, "braille_dots_23568", AChar '\x28b6', True) -- U+28B6 BRAILLE PATTERN DOTS-23568
  , (0x010028b7, "braille_dots_123568", AChar '\x28b7', True) -- U+28B7 BRAILLE PATTERN DOTS-123568
  , (0x010028b8, "braille_dots_4568", AChar '\x28b8', True) -- U+28B8 BRAILLE PATTERN DOTS-4568
  , (0x010028b9, "braille_dots_14568", AChar '\x28b9', True) -- U+28B9 BRAILLE PATTERN DOTS-14568
  , (0x010028ba, "braille_dots_24568", AChar '\x28ba', True) -- U+28BA BRAILLE PATTERN DOTS-24568
  , (0x010028bb, "braille_dots_124568", AChar '\x28bb', True) -- U+28BB BRAILLE PATTERN DOTS-124568
  , (0x010028bc, "braille_dots_34568", AChar '\x28bc', True) -- U+28BC BRAILLE PATTERN DOTS-34568
  , (0x010028bd, "braille_dots_134568", AChar '\x28bd', True) -- U+28BD BRAILLE PATTERN DOTS-134568
  , (0x010028be, "braille_dots_234568", AChar '\x28be', True) -- U+28BE BRAILLE PATTERN DOTS-234568
  , (0x010028bf, "braille_dots_1234568", AChar '\x28bf', True) -- U+28BF BRAILLE PATTERN DOTS-1234568
  , (0x010028c0, "braille_dots_78", AChar '\x28c0', True) -- U+28C0 BRAILLE PATTERN DOTS-78
  , (0x010028c1, "braille_dots_178", AChar '\x28c1', True) -- U+28C1 BRAILLE PATTERN DOTS-178
  , (0x010028c2, "braille_dots_278", AChar '\x28c2', True) -- U+28C2 BRAILLE PATTERN DOTS-278
  , (0x010028c3, "braille_dots_1278", AChar '\x28c3', True) -- U+28C3 BRAILLE PATTERN DOTS-1278
  , (0x010028c4, "braille_dots_378", AChar '\x28c4', True) -- U+28C4 BRAILLE PATTERN DOTS-378
  , (0x010028c5, "braille_dots_1378", AChar '\x28c5', True) -- U+28C5 BRAILLE PATTERN DOTS-1378
  , (0x010028c6, "braille_dots_2378", AChar '\x28c6', True) -- U+28C6 BRAILLE PATTERN DOTS-2378
  , (0x010028c7, "braille_dots_12378", AChar '\x28c7', True) -- U+28C7 BRAILLE PATTERN DOTS-12378
  , (0x010028c8, "braille_dots_478", AChar '\x28c8', True) -- U+28C8 BRAILLE PATTERN DOTS-478
  , (0x010028c9, "braille_dots_1478", AChar '\x28c9', True) -- U+28C9 BRAILLE PATTERN DOTS-1478
  , (0x010028ca, "braille_dots_2478", AChar '\x28ca', True) -- U+28CA BRAILLE PATTERN DOTS-2478
  , (0x010028cb, "braille_dots_12478", AChar '\x28cb', True) -- U+28CB BRAILLE PATTERN DOTS-12478
  , (0x010028cc, "braille_dots_3478", AChar '\x28cc', True) -- U+28CC BRAILLE PATTERN DOTS-3478
  , (0x010028cd, "braille_dots_13478", AChar '\x28cd', True) -- U+28CD BRAILLE PATTERN DOTS-13478
  , (0x010028ce, "braille_dots_23478", AChar '\x28ce', True) -- U+28CE BRAILLE PATTERN DOTS-23478
  , (0x010028cf, "braille_dots_123478", AChar '\x28cf', True) -- U+28CF BRAILLE PATTERN DOTS-123478
  , (0x010028d0, "braille_dots_578", AChar '\x28d0', True) -- U+28D0 BRAILLE PATTERN DOTS-578
  , (0x010028d1, "braille_dots_1578", AChar '\x28d1', True) -- U+28D1 BRAILLE PATTERN DOTS-1578
  , (0x010028d2, "braille_dots_2578", AChar '\x28d2', True) -- U+28D2 BRAILLE PATTERN DOTS-2578
  , (0x010028d3, "braille_dots_12578", AChar '\x28d3', True) -- U+28D3 BRAILLE PATTERN DOTS-12578
  , (0x010028d4, "braille_dots_3578", AChar '\x28d4', True) -- U+28D4 BRAILLE PATTERN DOTS-3578
  , (0x010028d5, "braille_dots_13578", AChar '\x28d5', True) -- U+28D5 BRAILLE PATTERN DOTS-13578
  , (0x010028d6, "braille_dots_23578", AChar '\x28d6', True) -- U+28D6 BRAILLE PATTERN DOTS-23578
  , (0x010028d7, "braille_dots_123578", AChar '\x28d7', True) -- U+28D7 BRAILLE PATTERN DOTS-123578
  , (0x010028d8, "braille_dots_4578", AChar '\x28d8', True) -- U+28D8 BRAILLE PATTERN DOTS-4578
  , (0x010028d9, "braille_dots_14578", AChar '\x28d9', True) -- U+28D9 BRAILLE PATTERN DOTS-14578
  , (0x010028da, "braille_dots_24578", AChar '\x28da', True) -- U+28DA BRAILLE PATTERN DOTS-24578
  , (0x010028db, "braille_dots_124578", AChar '\x28db', True) -- U+28DB BRAILLE PATTERN DOTS-124578
  , (0x010028dc, "braille_dots_34578", AChar '\x28dc', True) -- U+28DC BRAILLE PATTERN DOTS-34578
  , (0x010028dd, "braille_dots_134578", AChar '\x28dd', True) -- U+28DD BRAILLE PATTERN DOTS-134578
  , (0x010028de, "braille_dots_234578", AChar '\x28de', True) -- U+28DE BRAILLE PATTERN DOTS-234578
  , (0x010028df, "braille_dots_1234578", AChar '\x28df', True) -- U+28DF BRAILLE PATTERN DOTS-1234578
  , (0x010028e0, "braille_dots_678", AChar '\x28e0', True) -- U+28E0 BRAILLE PATTERN DOTS-678
  , (0x010028e1, "braille_dots_1678", AChar '\x28e1', True) -- U+28E1 BRAILLE PATTERN DOTS-1678
  , (0x010028e2, "braille_dots_2678", AChar '\x28e2', True) -- U+28E2 BRAILLE PATTERN DOTS-2678
  , (0x010028e3, "braille_dots_12678", AChar '\x28e3', True) -- U+28E3 BRAILLE PATTERN DOTS-12678
  , (0x010028e4, "braille_dots_3678", AChar '\x28e4', True) -- U+28E4 BRAILLE PATTERN DOTS-3678
  , (0x010028e5, "braille_dots_13678", AChar '\x28e5', True) -- U+28E5 BRAILLE PATTERN DOTS-13678
  , (0x010028e6, "braille_dots_23678", AChar '\x28e6', True) -- U+28E6 BRAILLE PATTERN DOTS-23678
  , (0x010028e7, "braille_dots_123678", AChar '\x28e7', True) -- U+28E7 BRAILLE PATTERN DOTS-123678
  , (0x010028e8, "braille_dots_4678", AChar '\x28e8', True) -- U+28E8 BRAILLE PATTERN DOTS-4678
  , (0x010028e9, "braille_dots_14678", AChar '\x28e9', True) -- U+28E9 BRAILLE PATTERN DOTS-14678
  , (0x010028ea, "braille_dots_24678", AChar '\x28ea', True) -- U+28EA BRAILLE PATTERN DOTS-24678
  , (0x010028eb, "braille_dots_124678", AChar '\x28eb', True) -- U+28EB BRAILLE PATTERN DOTS-124678
  , (0x010028ec, "braille_dots_34678", AChar '\x28ec', True) -- U+28EC BRAILLE PATTERN DOTS-34678
  , (0x010028ed, "braille_dots_134678", AChar '\x28ed', True) -- U+28ED BRAILLE PATTERN DOTS-134678
  , (0x010028ee, "braille_dots_234678", AChar '\x28ee', True) -- U+28EE BRAILLE PATTERN DOTS-234678
  , (0x010028ef, "braille_dots_1234678", AChar '\x28ef', True) -- U+28EF BRAILLE PATTERN DOTS-1234678
  , (0x010028f0, "braille_dots_5678", AChar '\x28f0', True) -- U+28F0 BRAILLE PATTERN DOTS-5678
  , (0x010028f1, "braille_dots_15678", AChar '\x28f1', True) -- U+28F1 BRAILLE PATTERN DOTS-15678
  , (0x010028f2, "braille_dots_25678", AChar '\x28f2', True) -- U+28F2 BRAILLE PATTERN DOTS-25678
  , (0x010028f3, "braille_dots_125678", AChar '\x28f3', True) -- U+28F3 BRAILLE PATTERN DOTS-125678
  , (0x010028f4, "braille_dots_35678", AChar '\x28f4', True) -- U+28F4 BRAILLE PATTERN DOTS-35678
  , (0x010028f5, "braille_dots_135678", AChar '\x28f5', True) -- U+28F5 BRAILLE PATTERN DOTS-135678
  , (0x010028f6, "braille_dots_235678", AChar '\x28f6', True) -- U+28F6 BRAILLE PATTERN DOTS-235678
  , (0x010028f7, "braille_dots_1235678", AChar '\x28f7', True) -- U+28F7 BRAILLE PATTERN DOTS-1235678
  , (0x010028f8, "braille_dots_45678", AChar '\x28f8', True) -- U+28F8 BRAILLE PATTERN DOTS-45678
  , (0x010028f9, "braille_dots_145678", AChar '\x28f9', True) -- U+28F9 BRAILLE PATTERN DOTS-145678
  , (0x010028fa, "braille_dots_245678", AChar '\x28fa', True) -- U+28FA BRAILLE PATTERN DOTS-245678
  , (0x010028fb, "braille_dots_1245678", AChar '\x28fb', True) -- U+28FB BRAILLE PATTERN DOTS-1245678
  , (0x010028fc, "braille_dots_345678", AChar '\x28fc', True) -- U+28FC BRAILLE PATTERN DOTS-345678
  , (0x010028fd, "braille_dots_1345678", AChar '\x28fd', True) -- U+28FD BRAILLE PATTERN DOTS-1345678
  , (0x010028fe, "braille_dots_2345678", AChar '\x28fe', True) -- U+28FE BRAILLE PATTERN DOTS-2345678
  , (0x010028ff, "braille_dots_12345678", AChar '\x28ff', True) -- U+28FF BRAILLE PATTERN DOTS-12345678
  --
  -- Sinhala (http://unicode.org/charts/PDF/U0D80.pdf)
  --
  , (0x01000d82, "Sinh_ng", AChar '\x0d82', True) -- U+0D82 SINHALA SIGN ANUSVARAYA
  , (0x01000d83, "Sinh_h2", AChar '\x0d83', True) -- U+0D83 SINHALA SIGN VISARGAYA
  , (0x01000d85, "Sinh_a", AChar '\x0d85', True) -- U+0D85 SINHALA LETTER AYANNA
  , (0x01000d86, "Sinh_aa", AChar '\x0d86', True) -- U+0D86 SINHALA LETTER AAYANNA
  , (0x01000d87, "Sinh_ae", AChar '\x0d87', True) -- U+0D87 SINHALA LETTER AEYANNA
  , (0x01000d88, "Sinh_aee", AChar '\x0d88', True) -- U+0D88 SINHALA LETTER AEEYANNA
  , (0x01000d89, "Sinh_i", AChar '\x0d89', True) -- U+0D89 SINHALA LETTER IYANNA
  , (0x01000d8a, "Sinh_ii", AChar '\x0d8a', True) -- U+0D8A SINHALA LETTER IIYANNA
  , (0x01000d8b, "Sinh_u", AChar '\x0d8b', True) -- U+0D8B SINHALA LETTER UYANNA
  , (0x01000d8c, "Sinh_uu", AChar '\x0d8c', True) -- U+0D8C SINHALA LETTER UUYANNA
  , (0x01000d8d, "Sinh_ri", AChar '\x0d8d', True) -- U+0D8D SINHALA LETTER IRUYANNA
  , (0x01000d8e, "Sinh_rii", AChar '\x0d8e', True) -- U+0D8E SINHALA LETTER IRUUYANNA
  , (0x01000d8f, "Sinh_lu", AChar '\x0d8f', True) -- U+0D8F SINHALA LETTER ILUYANNA
  , (0x01000d90, "Sinh_luu", AChar '\x0d90', True) -- U+0D90 SINHALA LETTER ILUUYANNA
  , (0x01000d91, "Sinh_e", AChar '\x0d91', True) -- U+0D91 SINHALA LETTER EYANNA
  , (0x01000d92, "Sinh_ee", AChar '\x0d92', True) -- U+0D92 SINHALA LETTER EEYANNA
  , (0x01000d93, "Sinh_ai", AChar '\x0d93', True) -- U+0D93 SINHALA LETTER AIYANNA
  , (0x01000d94, "Sinh_o", AChar '\x0d94', True) -- U+0D94 SINHALA LETTER OYANNA
  , (0x01000d95, "Sinh_oo", AChar '\x0d95', True) -- U+0D95 SINHALA LETTER OOYANNA
  , (0x01000d96, "Sinh_au", AChar '\x0d96', True) -- U+0D96 SINHALA LETTER AUYANNA
  , (0x01000d9a, "Sinh_ka", AChar '\x0d9a', True) -- U+0D9A SINHALA LETTER ALPAPRAANA KAYANNA
  , (0x01000d9b, "Sinh_kha", AChar '\x0d9b', True) -- U+0D9B SINHALA LETTER MAHAAPRAANA KAYANNA
  , (0x01000d9c, "Sinh_ga", AChar '\x0d9c', True) -- U+0D9C SINHALA LETTER ALPAPRAANA GAYANNA
  , (0x01000d9d, "Sinh_gha", AChar '\x0d9d', True) -- U+0D9D SINHALA LETTER MAHAAPRAANA GAYANNA
  , (0x01000d9e, "Sinh_ng2", AChar '\x0d9e', True) -- U+0D9E SINHALA LETTER KANTAJA NAASIKYAYA
  , (0x01000d9f, "Sinh_nga", AChar '\x0d9f', True) -- U+0D9F SINHALA LETTER SANYAKA GAYANNA
  , (0x01000da0, "Sinh_ca", AChar '\x0da0', True) -- U+0DA0 SINHALA LETTER ALPAPRAANA CAYANNA
  , (0x01000da1, "Sinh_cha", AChar '\x0da1', True) -- U+0DA1 SINHALA LETTER MAHAAPRAANA CAYANNA
  , (0x01000da2, "Sinh_ja", AChar '\x0da2', True) -- U+0DA2 SINHALA LETTER ALPAPRAANA JAYANNA
  , (0x01000da3, "Sinh_jha", AChar '\x0da3', True) -- U+0DA3 SINHALA LETTER MAHAAPRAANA JAYANNA
  , (0x01000da4, "Sinh_nya", AChar '\x0da4', True) -- U+0DA4 SINHALA LETTER TAALUJA NAASIKYAYA
  , (0x01000da5, "Sinh_jnya", AChar '\x0da5', True) -- U+0DA5 SINHALA LETTER TAALUJA SANYOOGA NAAKSIKYAYA
  , (0x01000da6, "Sinh_nja", AChar '\x0da6', True) -- U+0DA6 SINHALA LETTER SANYAKA JAYANNA
  , (0x01000da7, "Sinh_tta", AChar '\x0da7', True) -- U+0DA7 SINHALA LETTER ALPAPRAANA TTAYANNA
  , (0x01000da8, "Sinh_ttha", AChar '\x0da8', True) -- U+0DA8 SINHALA LETTER MAHAAPRAANA TTAYANNA
  , (0x01000da9, "Sinh_dda", AChar '\x0da9', True) -- U+0DA9 SINHALA LETTER ALPAPRAANA DDAYANNA
  , (0x01000daa, "Sinh_ddha", AChar '\x0daa', True) -- U+0DAA SINHALA LETTER MAHAAPRAANA DDAYANNA
  , (0x01000dab, "Sinh_nna", AChar '\x0dab', True) -- U+0DAB SINHALA LETTER MUURDHAJA NAYANNA
  , (0x01000dac, "Sinh_ndda", AChar '\x0dac', True) -- U+0DAC SINHALA LETTER SANYAKA DDAYANNA
  , (0x01000dad, "Sinh_tha", AChar '\x0dad', True) -- U+0DAD SINHALA LETTER ALPAPRAANA TAYANNA
  , (0x01000dae, "Sinh_thha", AChar '\x0dae', True) -- U+0DAE SINHALA LETTER MAHAAPRAANA TAYANNA
  , (0x01000daf, "Sinh_dha", AChar '\x0daf', True) -- U+0DAF SINHALA LETTER ALPAPRAANA DAYANNA
  , (0x01000db0, "Sinh_dhha", AChar '\x0db0', True) -- U+0DB0 SINHALA LETTER MAHAAPRAANA DAYANNA
  , (0x01000db1, "Sinh_na", AChar '\x0db1', True) -- U+0DB1 SINHALA LETTER DANTAJA NAYANNA
  , (0x01000db3, "Sinh_ndha", AChar '\x0db3', True) -- U+0DB3 SINHALA LETTER SANYAKA DAYANNA
  , (0x01000db4, "Sinh_pa", AChar '\x0db4', True) -- U+0DB4 SINHALA LETTER ALPAPRAANA PAYANNA
  , (0x01000db5, "Sinh_pha", AChar '\x0db5', True) -- U+0DB5 SINHALA LETTER MAHAAPRAANA PAYANNA
  , (0x01000db6, "Sinh_ba", AChar '\x0db6', True) -- U+0DB6 SINHALA LETTER ALPAPRAANA BAYANNA
  , (0x01000db7, "Sinh_bha", AChar '\x0db7', True) -- U+0DB7 SINHALA LETTER MAHAAPRAANA BAYANNA
  , (0x01000db8, "Sinh_ma", AChar '\x0db8', True) -- U+0DB8 SINHALA LETTER MAYANNA
  , (0x01000db9, "Sinh_mba", AChar '\x0db9', True) -- U+0DB9 SINHALA LETTER AMBA BAYANNA
  , (0x01000dba, "Sinh_ya", AChar '\x0dba', True) -- U+0DBA SINHALA LETTER YAYANNA
  , (0x01000dbb, "Sinh_ra", AChar '\x0dbb', True) -- U+0DBB SINHALA LETTER RAYANNA
  , (0x01000dbd, "Sinh_la", AChar '\x0dbd', True) -- U+0DBD SINHALA LETTER DANTAJA LAYANNA
  , (0x01000dc0, "Sinh_va", AChar '\x0dc0', True) -- U+0DC0 SINHALA LETTER VAYANNA
  , (0x01000dc1, "Sinh_sha", AChar '\x0dc1', True) -- U+0DC1 SINHALA LETTER TAALUJA SAYANNA
  , (0x01000dc2, "Sinh_ssha", AChar '\x0dc2', True) -- U+0DC2 SINHALA LETTER MUURDHAJA SAYANNA
  , (0x01000dc3, "Sinh_sa", AChar '\x0dc3', True) -- U+0DC3 SINHALA LETTER DANTAJA SAYANNA
  , (0x01000dc4, "Sinh_ha", AChar '\x0dc4', True) -- U+0DC4 SINHALA LETTER HAYANNA
  , (0x01000dc5, "Sinh_lla", AChar '\x0dc5', True) -- U+0DC5 SINHALA LETTER MUURDHAJA LAYANNA
  , (0x01000dc6, "Sinh_fa", AChar '\x0dc6', True) -- U+0DC6 SINHALA LETTER FAYANNA
  , (0x01000dca, "Sinh_al", AChar '\x0dca', True) -- U+0DCA SINHALA SIGN AL-LAKUNA
  , (0x01000dcf, "Sinh_aa2", AChar '\x0dcf', True) -- U+0DCF SINHALA VOWEL SIGN AELA-PILLA
  , (0x01000dd0, "Sinh_ae2", AChar '\x0dd0', True) -- U+0DD0 SINHALA VOWEL SIGN KETTI AEDA-PILLA
  , (0x01000dd1, "Sinh_aee2", AChar '\x0dd1', True) -- U+0DD1 SINHALA VOWEL SIGN DIGA AEDA-PILLA
  , (0x01000dd2, "Sinh_i2", AChar '\x0dd2', True) -- U+0DD2 SINHALA VOWEL SIGN KETTI IS-PILLA
  , (0x01000dd3, "Sinh_ii2", AChar '\x0dd3', True) -- U+0DD3 SINHALA VOWEL SIGN DIGA IS-PILLA
  , (0x01000dd4, "Sinh_u2", AChar '\x0dd4', True) -- U+0DD4 SINHALA VOWEL SIGN KETTI PAA-PILLA
  , (0x01000dd6, "Sinh_uu2", AChar '\x0dd6', True) -- U+0DD6 SINHALA VOWEL SIGN DIGA PAA-PILLA
  , (0x01000dd8, "Sinh_ru2", AChar '\x0dd8', True) -- U+0DD8 SINHALA VOWEL SIGN GAETTA-PILLA
  , (0x01000dd9, "Sinh_e2", AChar '\x0dd9', True) -- U+0DD9 SINHALA VOWEL SIGN KOMBUVA
  , (0x01000dda, "Sinh_ee2", AChar '\x0dda', True) -- U+0DDA SINHALA VOWEL SIGN DIGA KOMBUVA
  , (0x01000ddb, "Sinh_ai2", AChar '\x0ddb', True) -- U+0DDB SINHALA VOWEL SIGN KOMBU DEKA
  , (0x01000ddc, "Sinh_o2", AChar '\x0ddc', True) -- U+0DDC SINHALA VOWEL SIGN KOMBUVA HAA AELA-PILLA
  , (0x01000ddd, "Sinh_oo2", AChar '\x0ddd', True) -- U+0DDD SINHALA VOWEL SIGN KOMBUVA HAA DIGA AELA-PILLA
  , (0x01000dde, "Sinh_au2", AChar '\x0dde', True) -- U+0DDE SINHALA VOWEL SIGN KOMBUVA HAA GAYANUKITTA
  , (0x01000ddf, "Sinh_lu2", AChar '\x0ddf', True) -- U+0DDF SINHALA VOWEL SIGN GAYANUKITTA
  , (0x01000df2, "Sinh_ruu2", AChar '\x0df2', True) -- U+0DF2 SINHALA VOWEL SIGN DIGA GAETTA-PILLA
  , (0x01000df3, "Sinh_luu2", AChar '\x0df3', True) -- U+0DF3 SINHALA VOWEL SIGN DIGA GAYANUKITTA
  , (0x01000df4, "Sinh_kunddaliya", AChar '\x0df4', True) -- U+0DF4 SINHALA PUNCTUATION KUNDDALIYA
  --
  -- XF86
  --
  , (0x1008ff01, "XF86ModeLock", ASpecial XF86ModeLock, True) -- Mode Switch Lock
  --
  -- XF86: Backlight controls
  --
  , (0x1008ff02, "XF86MonBrightnessUp", ASpecial XF86MonBrightnessUp, True) -- Monitor\/panel brightness
  , (0x1008ff03, "XF86MonBrightnessDown", ASpecial XF86MonBrightnessDown, True) -- Monitor\/panel brightness
  , (0x1008ff04, "XF86KbdLightOnOff", ASpecial XF86KbdLightOnOff, True) -- Keyboards may be lit
  , (0x1008ff05, "XF86KbdBrightnessUp", ASpecial XF86KbdBrightnessUp, True) -- Keyboards may be lit
  , (0x1008ff06, "XF86KbdBrightnessDown", ASpecial XF86KbdBrightnessDown, True) -- Keyboards may be lit
  , (0x1008ff07, "XF86MonBrightnessCycle", ASpecial XF86MonBrightnessCycle, True) -- Monitor\/panel brightness
  --
  -- XF86: Keys found on some "Internet" keyboards.
  --
  , (0x1008ff10, "XF86Standby", ASpecial XF86Standby, True) -- System into standby mode
  , (0x1008ff11, "XF86AudioLowerVolume", ASpecial XF86AudioLowerVolume, True) -- Volume control down
  , (0x1008ff12, "XF86AudioMute", ASpecial XF86AudioMute, True) -- Mute sound from the system
  , (0x1008ff13, "XF86AudioRaiseVolume", ASpecial XF86AudioRaiseVolume, True) -- Volume control up
  , (0x1008ff14, "XF86AudioPlay", ASpecial XF86AudioPlay, True) -- Start playing of audio >
  , (0x1008ff15, "XF86AudioStop", ASpecial XF86AudioStop, True) -- Stop playing audio
  , (0x1008ff16, "XF86AudioPrev", ASpecial XF86AudioPrev, True) -- Previous track
  , (0x1008ff17, "XF86AudioNext", ASpecial XF86AudioNext, True) -- Next track
  , (0x1008ff18, "XF86HomePage", ASpecial XF86HomePage, True) -- Display user's home page
  , (0x1008ff19, "XF86Mail", ASpecial XF86Mail, True) -- Invoke user's mail program
  , (0x1008ff1a, "XF86Start", ASpecial XF86Start, True) -- Start application
  , (0x1008ff1b, "XF86Search", ASpecial XF86Search, True) -- Search
  , (0x1008ff1c, "XF86AudioRecord", ASpecial XF86AudioRecord, True) -- Record audio application
  --
  -- XF86: These are sometimes found on PDA's (e.g. Palm, PocketPC or elsewhere)
  --
  , (0x1008ff1d, "XF86Calculator", ASpecial XF86Calculator, True) -- Invoke calculator program
  , (0x1008ff1e, "XF86Memo", ASpecial XF86Memo, True) -- Invoke Memo taking program
  , (0x1008ff1f, "XF86ToDoList", ASpecial XF86ToDoList, True) -- Invoke To Do List program
  , (0x1008ff20, "XF86Calendar", ASpecial XF86Calendar, True) -- Invoke Calendar program
  , (0x1008ff21, "XF86PowerDown", ASpecial XF86PowerDown, True) -- Deep sleep the system
  , (0x1008ff22, "XF86ContrastAdjust", ASpecial XF86ContrastAdjust, True) -- Adjust screen contrast
  , (0x1008ff23, "XF86RockerUp", ASpecial XF86RockerUp, True) -- Rocker switches exist up
  , (0x1008ff24, "XF86RockerDown", ASpecial XF86RockerDown, True) -- and down
  , (0x1008ff25, "XF86RockerEnter", ASpecial XF86RockerEnter, True) -- and let you press them
  --
  -- XF86: Some more "Internet" keyboard symbols
  --
  , (0x1008ff26, "XF86Back", ASpecial XF86Back, True) -- Like back on a browser
  , (0x1008ff27, "XF86Forward", ASpecial XF86Forward, True) -- Like forward on a browser
  , (0x1008ff28, "XF86Stop", ASpecial XF86Stop, True) -- Stop current operation
  , (0x1008ff29, "XF86Refresh", ASpecial XF86Refresh, True) -- Refresh the page
  , (0x1008ff2a, "XF86PowerOff", ASpecial XF86PowerOff, True) -- Power off system entirely
  , (0x1008ff2b, "XF86WakeUp", ASpecial XF86WakeUp, True) -- Wake up system from sleep
  , (0x1008ff2c, "XF86Eject", ASpecial XF86Eject, True) -- Eject device (e.g. DVD)
  , (0x1008ff2d, "XF86ScreenSaver", ASpecial XF86ScreenSaver, True) -- Invoke screensaver
  , (0x1008ff2e, "XF86WWW", ASpecial XF86WWW, True) -- Invoke web browser
  , (0x1008ff2f, "XF86Sleep", ASpecial XF86Sleep, True) -- Put system to sleep
  , (0x1008ff30, "XF86Favorites", ASpecial XF86Favorites, True) -- Show favorite locations
  , (0x1008ff31, "XF86AudioPause", ASpecial XF86AudioPause, True) -- Pause audio playing
  , (0x1008ff32, "XF86AudioMedia", ASpecial XF86AudioMedia, True) -- Launch media collection app
  , (0x1008ff33, "XF86MyComputer", ASpecial XF86MyComputer, True) -- Display "My Computer" window
  , (0x1008ff34, "XF86VendorHome", ASpecial XF86VendorHome, True) -- Display vendor home web site
  , (0x1008ff35, "XF86LightBulb", ASpecial XF86LightBulb, True) -- Light bulb keys exist
  , (0x1008ff36, "XF86Shop", ASpecial XF86Shop, True) -- Display shopping web site
  , (0x1008ff37, "XF86History", ASpecial XF86History, True) -- Show history of web surfing
  , (0x1008ff38, "XF86OpenURL", ASpecial XF86OpenURL, True) -- Open selected URL
  , (0x1008ff39, "XF86AddFavorite", ASpecial XF86AddFavorite, True) -- Add URL to favorites list
  , (0x1008ff3a, "XF86HotLinks", ASpecial XF86HotLinks, True) -- Show "hot" links
  , (0x1008ff3b, "XF86BrightnessAdjust", ASpecial XF86BrightnessAdjust, True) -- Invoke brightness adj. UI
  , (0x1008ff3c, "XF86Finance", ASpecial XF86Finance, True) -- Display financial site
  , (0x1008ff3d, "XF86Community", ASpecial XF86Community, True) -- Display user's community
  , (0x1008ff3e, "XF86AudioRewind", ASpecial XF86AudioRewind, True) -- "rewind" audio track
  , (0x1008ff3f, "XF86BackForward", ASpecial XF86BackForward, True) -- ???
  , (0x1008ff40, "XF86Launch0", ASpecial XF86Launch0, True) -- Launch Application
  , (0x1008ff41, "XF86Launch1", ASpecial XF86Launch1, True) -- Launch Application
  , (0x1008ff42, "XF86Launch2", ASpecial XF86Launch2, True) -- Launch Application
  , (0x1008ff43, "XF86Launch3", ASpecial XF86Launch3, True) -- Launch Application
  , (0x1008ff44, "XF86Launch4", ASpecial XF86Launch4, True) -- Launch Application
  , (0x1008ff45, "XF86Launch5", ASpecial XF86Launch5, True) -- Launch Application
  , (0x1008ff46, "XF86Launch6", ASpecial XF86Launch6, True) -- Launch Application
  , (0x1008ff47, "XF86Launch7", ASpecial XF86Launch7, True) -- Launch Application
  , (0x1008ff48, "XF86Launch8", ASpecial XF86Launch8, True) -- Launch Application
  , (0x1008ff49, "XF86Launch9", ASpecial XF86Launch9, True) -- Launch Application
  , (0x1008ff4a, "XF86LaunchA", ASpecial XF86LaunchA, True) -- Launch Application
  , (0x1008ff4b, "XF86LaunchB", ASpecial XF86LaunchB, True) -- Launch Application
  , (0x1008ff4c, "XF86LaunchC", ASpecial XF86LaunchC, True) -- Launch Application
  , (0x1008ff4d, "XF86LaunchD", ASpecial XF86LaunchD, True) -- Launch Application
  , (0x1008ff4e, "XF86LaunchE", ASpecial XF86LaunchE, True) -- Launch Application
  , (0x1008ff4f, "XF86LaunchF", ASpecial XF86LaunchF, True) -- Launch Application
  , (0x1008ff50, "XF86ApplicationLeft", ASpecial XF86ApplicationLeft, True) -- switch to application, left
  , (0x1008ff51, "XF86ApplicationRight", ASpecial XF86ApplicationRight, True) -- switch to application, right
  , (0x1008ff52, "XF86Book", ASpecial XF86Book, True) -- Launch bookreader
  , (0x1008ff53, "XF86CD", ASpecial XF86CD, True) -- Launch CD\/DVD player
  , (0x1008ff54, "XF86Calculater", ASpecial XF86Calculater, True) -- Launch Calculater
  , (0x1008ff55, "XF86Clear", ASpecial XF86Clear, True) -- Clear window, screen
  , (0x1008ff56, "XF86Close", ASpecial XF86Close, True) -- Close window
  , (0x1008ff57, "XF86Copy", ASpecial XF86Copy, True) -- Copy selection
  , (0x1008ff58, "XF86Cut", ASpecial XF86Cut, True) -- Cut selection
  , (0x1008ff59, "XF86Display", ASpecial XF86Display, True) -- Output switch key
  , (0x1008ff5a, "XF86DOS", ASpecial XF86DOS, True) -- Launch DOS (emulation)
  , (0x1008ff5b, "XF86Documents", ASpecial XF86Documents, True) -- Open documents window
  , (0x1008ff5c, "XF86Excel", ASpecial XF86Excel, True) -- Launch spread sheet
  , (0x1008ff5d, "XF86Explorer", ASpecial XF86Explorer, True) -- Launch file explorer
  , (0x1008ff5e, "XF86Game", ASpecial XF86Game, True) -- Launch game
  , (0x1008ff5f, "XF86Go", ASpecial XF86Go, True) -- Go to URL
  , (0x1008ff60, "XF86iTouch", ASpecial XF86iTouch, True) -- Logitech iTouch- don't use
  , (0x1008ff61, "XF86LogOff", ASpecial XF86LogOff, True) -- Log off system
  , (0x1008ff62, "XF86Market", ASpecial XF86Market, True) -- ??
  , (0x1008ff63, "XF86Meeting", ASpecial XF86Meeting, True) -- enter meeting in calendar
  , (0x1008ff65, "XF86MenuKB", ASpecial XF86MenuKB, True) -- distinguish keyboard from PB
  , (0x1008ff66, "XF86MenuPB", ASpecial XF86MenuPB, True) -- distinguish PB from keyboard
  , (0x1008ff67, "XF86MySites", ASpecial XF86MySites, True) -- Favourites
  , (0x1008ff68, "XF86New", ASpecial XF86New, True) -- New (folder, document...
  , (0x1008ff69, "XF86News", ASpecial XF86News, True) -- News
  , (0x1008ff6a, "XF86OfficeHome", ASpecial XF86OfficeHome, True) -- Office home (old Staroffice)
  , (0x1008ff6b, "XF86Open", ASpecial XF86Open, True) -- Open
  , (0x1008ff6c, "XF86Option", ASpecial XF86Option, True) -- ??
  , (0x1008ff6d, "XF86Paste", ASpecial XF86Paste, True) -- Paste
  , (0x1008ff6e, "XF86Phone", ASpecial XF86Phone, True) -- Launch phone; dial number
  , (0x1008ff70, "XF86Q", ASpecial XF86Q, True) -- Compaq's Q - don't use
  , (0x1008ff72, "XF86Reply", ASpecial XF86Reply, True) -- Reply e.g., mail
  , (0x1008ff73, "XF86Reload", ASpecial XF86Reload, True) -- Reload web page, file, etc.
  , (0x1008ff74, "XF86RotateWindows", ASpecial XF86RotateWindows, True) -- Rotate windows e.g. xrandr
  , (0x1008ff75, "XF86RotationPB", ASpecial XF86RotationPB, True) -- don't use
  , (0x1008ff76, "XF86RotationKB", ASpecial XF86RotationKB, True) -- don't use
  , (0x1008ff77, "XF86Save", ASpecial XF86Save, True) -- Save (file, document, state
  , (0x1008ff78, "XF86ScrollUp", ASpecial XF86ScrollUp, True) -- Scroll window\/contents up
  , (0x1008ff79, "XF86ScrollDown", ASpecial XF86ScrollDown, True) -- Scrool window\/contentd down
  , (0x1008ff7a, "XF86ScrollClick", ASpecial XF86ScrollClick, True) -- Use XKB mousekeys instead
  , (0x1008ff7b, "XF86Send", ASpecial XF86Send, True) -- Send mail, file, object
  , (0x1008ff7c, "XF86Spell", ASpecial XF86Spell, True) -- Spell checker
  , (0x1008ff7d, "XF86SplitScreen", ASpecial XF86SplitScreen, True) -- Split window or screen
  , (0x1008ff7e, "XF86Support", ASpecial XF86Support, True) -- Get support (??)
  , (0x1008ff7f, "XF86TaskPane", ASpecial XF86TaskPane, True) -- Show tasks
  , (0x1008ff80, "XF86Terminal", ASpecial XF86Terminal, True) -- Launch terminal emulator
  , (0x1008ff81, "XF86Tools", ASpecial XF86Tools, True) -- toolbox of desktop\/app.
  , (0x1008ff82, "XF86Travel", ASpecial XF86Travel, True) -- ??
  , (0x1008ff84, "XF86UserPB", ASpecial XF86UserPB, True) -- ??
  , (0x1008ff85, "XF86User1KB", ASpecial XF86User1KB, True) -- ??
  , (0x1008ff86, "XF86User2KB", ASpecial XF86User2KB, True) -- ??
  , (0x1008ff87, "XF86Video", ASpecial XF86Video, True) -- Launch video player
  , (0x1008ff88, "XF86WheelButton", ASpecial XF86WheelButton, True) -- button from a mouse wheel
  , (0x1008ff89, "XF86Word", ASpecial XF86Word, True) -- Launch word processor
  , (0x1008ff8a, "XF86Xfer", ASpecial XF86Xfer, True)
  , (0x1008ff8b, "XF86ZoomIn", ASpecial XF86ZoomIn, True) -- zoom in view, map, etc.
  , (0x1008ff8c, "XF86ZoomOut", ASpecial XF86ZoomOut, True) -- zoom out view, map, etc.
  , (0x1008ff8d, "XF86Away", ASpecial XF86Away, True) -- mark yourself as away
  , (0x1008ff8e, "XF86Messenger", ASpecial XF86Messenger, True) -- as in instant messaging
  , (0x1008ff8f, "XF86WebCam", ASpecial XF86WebCam, True) -- Launch web camera app.
  , (0x1008ff90, "XF86MailForward", ASpecial XF86MailForward, True) -- Forward in mail
  , (0x1008ff91, "XF86Pictures", ASpecial XF86Pictures, True) -- Show pictures
  , (0x1008ff92, "XF86Music", ASpecial XF86Music, True) -- Launch music application
  , (0x1008ff93, "XF86Battery", ASpecial XF86Battery, True) -- Display battery information
  , (0x1008ff94, "XF86Bluetooth", ASpecial XF86Bluetooth, True) -- Enable\/disable Bluetooth
  , (0x1008ff95, "XF86WLAN", ASpecial XF86WLAN, True) -- Enable\/disable WLAN
  , (0x1008ff96, "XF86UWB", ASpecial XF86UWB, True) -- Enable\/disable UWB
  , (0x1008ff97, "XF86AudioForward", ASpecial XF86AudioForward, True) -- fast-forward audio track
  , (0x1008ff98, "XF86AudioRepeat", ASpecial XF86AudioRepeat, True) -- toggle repeat mode
  , (0x1008ff99, "XF86AudioRandomPlay", ASpecial XF86AudioRandomPlay, True) -- toggle shuffle mode
  , (0x1008ff9a, "XF86Subtitle", ASpecial XF86Subtitle, True) -- cycle through subtitle
  , (0x1008ff9b, "XF86AudioCycleTrack", ASpecial XF86AudioCycleTrack, True) -- cycle through audio tracks
  , (0x1008ff9c, "XF86CycleAngle", ASpecial XF86CycleAngle, True) -- cycle through angles
  , (0x1008ff9d, "XF86FrameBack", ASpecial XF86FrameBack, True) -- video: go one frame back
  , (0x1008ff9e, "XF86FrameForward", ASpecial XF86FrameForward, True) -- video: go one frame forward
  , (0x1008ff9f, "XF86Time", ASpecial XF86Time, True) -- display, or shows an entry for time seeking
  , (0x1008ffa0, "XF86Select", ASpecial XF86Select, True) -- Select button on joypads and remotes
  , (0x1008ffa1, "XF86View", ASpecial XF86View, True) -- Show a view options\/properties
  , (0x1008ffa2, "XF86TopMenu", ASpecial XF86TopMenu, True) -- Go to a top-level menu in a video
  , (0x1008ffa3, "XF86Red", ASpecial XF86Red, True) -- Red button
  , (0x1008ffa4, "XF86Green", ASpecial XF86Green, True) -- Green button
  , (0x1008ffa5, "XF86Yellow", ASpecial XF86Yellow, True) -- Yellow button
  , (0x1008ffa6, "XF86Blue", ASpecial XF86Blue, True) -- Blue button
  , (0x1008ffa7, "XF86Suspend", ASpecial XF86Suspend, True) -- Sleep to RAM
  , (0x1008ffa8, "XF86Hibernate", ASpecial XF86Hibernate, True) -- Sleep to disk
  , (0x1008ffa9, "XF86TouchpadToggle", ASpecial XF86TouchpadToggle, True) -- Toggle between touchpad\/trackstick
  , (0x1008ffb0, "XF86TouchpadOn", ASpecial XF86TouchpadOn, True) -- The touchpad got switched on
  , (0x1008ffb1, "XF86TouchpadOff", ASpecial XF86TouchpadOff, True) -- The touchpad got switched off
  , (0x1008ffb2, "XF86AudioMicMute", ASpecial XF86AudioMicMute, True) -- Mute the Mic from the system
  , (0x1008ffb3, "XF86Keyboard", ASpecial XF86Keyboard, True) -- User defined keyboard related action
  , (0x1008ffb4, "XF86WWAN", ASpecial XF86WWAN, True) -- Toggle WWAN (LTE, UMTS, etc.) radio
  , (0x1008ffb5, "XF86RFKill", ASpecial XF86RFKill, True) -- Toggle radios on\/off
  , (0x1008ffb6, "XF86AudioPreset", ASpecial XF86AudioPreset, True) -- Select equalizer preset, e.g. theatre-mode
  , (0x1008ffb7, "XF86RotationLockToggle", ASpecial XF86RotationLockToggle, True) -- Toggle screen rotation lock on\/off
  , (0x1008ffb8, "XF86FullScreen", ASpecial XF86FullScreen, True) -- Toggle fullscreen
  --
  -- XF86: Virtual terminals on some operating systems
  --
  , (0x1008fe01, "XF86Switch_VT_1", ASpecial XF86Switch_Vt_1, True)
  , (0x1008fe02, "XF86Switch_VT_2", ASpecial XF86Switch_Vt_2, True)
  , (0x1008fe03, "XF86Switch_VT_3", ASpecial XF86Switch_Vt_3, True)
  , (0x1008fe04, "XF86Switch_VT_4", ASpecial XF86Switch_Vt_4, True)
  , (0x1008fe05, "XF86Switch_VT_5", ASpecial XF86Switch_Vt_5, True)
  , (0x1008fe06, "XF86Switch_VT_6", ASpecial XF86Switch_Vt_6, True)
  , (0x1008fe07, "XF86Switch_VT_7", ASpecial XF86Switch_Vt_7, True)
  , (0x1008fe08, "XF86Switch_VT_8", ASpecial XF86Switch_Vt_8, True)
  , (0x1008fe09, "XF86Switch_VT_9", ASpecial XF86Switch_Vt_9, True)
  , (0x1008fe0a, "XF86Switch_VT_10", ASpecial XF86Switch_Vt_10, True)
  , (0x1008fe0b, "XF86Switch_VT_11", ASpecial XF86Switch_Vt_11, True)
  , (0x1008fe0c, "XF86Switch_VT_12", ASpecial XF86Switch_Vt_12, True)
  , (0x1008fe20, "XF86Ungrab", ASpecial XF86Ungrab, True) -- force ungrab
  , (0x1008fe21, "XF86ClearGrab", ASpecial XF86ClearGrab, True) -- kill application with grab
  , (0x1008fe22, "XF86Next_VMode", ASpecial XF86Next_Vmode, True) -- next video mode available
  , (0x1008fe23, "XF86Prev_VMode", ASpecial XF86Prev_Vmode, True) -- prev. video mode available
  , (0x1008fe24, "XF86LogWindowTree", ASpecial XF86LogWindowTree, True) -- print window tree to log
  , (0x1008fe25, "XF86LogGrabInfo", ASpecial XF86LogGrabInfo, True) -- print all active grabs to log
  --
  -- Sun: Floating Accent
  --
  , (0x1005ff00, "SunFA_Grave", ASpecial Sunfa_Grave, True)
  , (0x1005ff01, "SunFA_Circum", ASpecial Sunfa_Circum, True)
  , (0x1005ff02, "SunFA_Tilde", ASpecial Sunfa_Tilde, True)
  , (0x1005ff03, "SunFA_Acute", ASpecial Sunfa_Acute, True)
  , (0x1005ff04, "SunFA_Diaeresis", ASpecial Sunfa_Diaeresis, True)
  , (0x1005ff05, "SunFA_Cedilla", ASpecial Sunfa_Cedilla, True)
  --
  -- Sun: Miscellaneous Functions
  --
  , (0x1005ff10, "SunF36", ASpecial SunF36, True) -- Labeled F11
  , (0x1005ff11, "SunF37", ASpecial SunF37, True) -- Labeled F12
  , (0x1005ff60, "SunSys_Req", ASpecial Sunsys_Req, True)
  , (0x0000ff61, "SunPrint_Screen", ASpecial Print, False) -- Same as XK_Print (alias for Print)
  , (0x0000ff20, "SunCompose", ADeadKey DK.compose, False) -- Same as XK_Multi_key (alias for Multi_key)
  , (0x0000ff7e, "SunAltGraph", ASpecial Iso_Group_Shift, False) -- Same as XK_Mode_switch (alias for ISO_Group_Shift)
  --
  -- Sun: Cursor Control
  --
  , (0x0000ff55, "SunPageUp", ASpecial Prior, False) -- Same as XK_Prior (alias for Prior)
  , (0x0000ff56, "SunPageDown", ASpecial Next, False) -- Same as XK_Next (alias for Next)
  --
  -- Sun: Open Look Functions
  --
  , (0x0000ff65, "SunUndo", ASpecial Undo, False) -- Same as XK_Undo (alias for Undo)
  , (0x0000ff66, "SunAgain", ASpecial Redo, False) -- Same as XK_Redo (alias for Redo)
  , (0x0000ff68, "SunFind", ASpecial Find, False) -- Same as XK_Find (alias for Find)
  , (0x0000ff69, "SunStop", ASpecial Cancel, False) -- Same as XK_Cancel (alias for Cancel)
  , (0x1005ff70, "SunProps", ASpecial SunProps, True)
  , (0x1005ff71, "SunFront", ASpecial SunFront, True)
  , (0x1005ff72, "SunCopy", ASpecial SunCopy, True)
  , (0x1005ff73, "SunOpen", ASpecial SunOpen, True)
  , (0x1005ff74, "SunPaste", ASpecial SunPaste, True)
  , (0x1005ff75, "SunCut", ASpecial SunCut, True)
  , (0x1005ff76, "SunPowerSwitch", ASpecial SunPowerSwitch, True)
  , (0x1005ff77, "SunAudioLowerVolume", ASpecial SunAudioLowerVolume, True)
  , (0x1005ff78, "SunAudioMute", ASpecial SunAudioMute, True)
  , (0x1005ff79, "SunAudioRaiseVolume", ASpecial SunAudioRaiseVolume, True)
  , (0x1005ff7a, "SunVideoDegauss", ASpecial SunVideoDegauss, True)
  , (0x1005ff7b, "SunVideoLowerBrightness", ASpecial SunVideoLowerBrightness, True)
  , (0x1005ff7c, "SunVideoRaiseBrightness", ASpecial SunVideoRaiseBrightness, True)
  , (0x1005ff7d, "SunPowerSwitchShift", ASpecial SunPowerSwitchShift, True)
  --
  -- DEC private keysyms
  --
  , (0x1000feb0, "Dring_accent", ASpecial Dring_Accent, True)
  , (0x1000fe5e, "Dcircumflex_accent", ASpecial Dcircumflex_Accent, True)
  , (0x1000fe2c, "Dcedilla_accent", ASpecial Dcedilla_Accent, True)
  , (0x1000fe27, "Dacute_accent", ASpecial Dacute_Accent, True)
  , (0x1000fe60, "Dgrave_accent", ASpecial Dgrave_Accent, True)
  , (0x1000fe7e, "Dtilde", ASpecial Dtilde, True)
  , (0x1000fe22, "Ddiaeresis", ASpecial Ddiaeresis, True)
  , (0x1000ff00, "DRemove", ASpecial DRemove, True) -- Remove
  --
  -- HP keysyms
  --
  , (0x1000ff6f, "hpClearLine", ASpecial Hpclearline, True)
  , (0x1000ff70, "hpInsertLine", ASpecial Hpinsertline, True)
  , (0x1000ff71, "hpDeleteLine", ASpecial Hpdeleteline, True)
  , (0x1000ff72, "hpInsertChar", ASpecial Hpinsertchar, True)
  , (0x1000ff73, "hpDeleteChar", ASpecial Hpdeletechar, True)
  , (0x1000ff74, "hpBackTab", ASpecial Hpbacktab, True)
  , (0x1000ff75, "hpKP_BackTab", ASpecial Hpkp_Backtab, True)
  , (0x1000ff48, "hpModelock1", ASpecial Hpmodelock1, True)
  , (0x1000ff49, "hpModelock2", ASpecial Hpmodelock2, True)
  , (0x1000ff6c, "hpReset", ASpecial Hpreset, True)
  , (0x1000ff6d, "hpSystem", ASpecial Hpsystem, True)
  , (0x1000ff6e, "hpUser", ASpecial Hpuser, True)
  -- Excluded keysym: hpmute_acute. Probably a duplicate of type DeadKey (cannot decide)
  -- Excluded keysym: hpmute_grave. Probably a duplicate of type DeadKey (cannot decide)
  -- Excluded keysym: hpmute_asciicircum. Probably a duplicate of type DeadKey (cannot decide)
  -- Excluded keysym: hpmute_diaeresis. Probably a duplicate of type DeadKey (cannot decide)
  -- Excluded keysym: hpmute_asciitilde. Probably a duplicate of type DeadKey (cannot decide)
  -- Excluded keysym: hplira. Probably a duplicate of type Char (cannot decide)
  -- Excluded keysym: hpguilder. Probably a duplicate of type Char (cannot decide)
  -- Excluded keysym: hpYdiaeresis. Probably a duplicate of type Char (cannot decide)
  -- Excluded keysym: hpIO. Probably a duplicate of type Char (cannot decide)
  -- Excluded keysym: hplongminus. Probably a duplicate of type Char (cannot decide)
  , (0x100000fc, "hpblock", ASpecial Hpblock, True)
  --
  -- OSF keysyms
  --
  , (0x1004ff02, "osfCopy", ASpecial Osfcopy, True)
  , (0x1004ff03, "osfCut", ASpecial Osfcut, True)
  , (0x1004ff04, "osfPaste", ASpecial Osfpaste, True)
  , (0x1004ff07, "osfBackTab", ASpecial Osfbacktab, True)
  , (0x1004ff08, "osfBackSpace", ASpecial Osfbackspace, True)
  , (0x1004ff0b, "osfClear", ASpecial Osfclear, True)
  , (0x1004ff1b, "osfEscape", ASpecial Osfescape, True)
  , (0x1004ff31, "osfAddMode", ASpecial Osfaddmode, True)
  , (0x1004ff32, "osfPrimaryPaste", ASpecial Osfprimarypaste, True)
  , (0x1004ff33, "osfQuickPaste", ASpecial Osfquickpaste, True)
  , (0x1004ff40, "osfPageLeft", ASpecial Osfpageleft, True)
  , (0x1004ff41, "osfPageUp", ASpecial Osfpageup, True)
  , (0x1004ff42, "osfPageDown", ASpecial Osfpagedown, True)
  , (0x1004ff43, "osfPageRight", ASpecial Osfpageright, True)
  , (0x1004ff44, "osfActivate", ASpecial Osfactivate, True)
  , (0x1004ff45, "osfMenuBar", ASpecial Osfmenubar, True)
  , (0x1004ff51, "osfLeft", ASpecial Osfleft, True)
  , (0x1004ff52, "osfUp", ASpecial Osfup, True)
  , (0x1004ff53, "osfRight", ASpecial Osfright, True)
  , (0x1004ff54, "osfDown", ASpecial Osfdown, True)
  , (0x1004ff57, "osfEndLine", ASpecial Osfendline, True)
  , (0x1004ff58, "osfBeginLine", ASpecial Osfbeginline, True)
  , (0x1004ff59, "osfEndData", ASpecial Osfenddata, True)
  , (0x1004ff5a, "osfBeginData", ASpecial Osfbegindata, True)
  , (0x1004ff5b, "osfPrevMenu", ASpecial Osfprevmenu, True)
  , (0x1004ff5c, "osfNextMenu", ASpecial Osfnextmenu, True)
  , (0x1004ff5d, "osfPrevField", ASpecial Osfprevfield, True)
  , (0x1004ff5e, "osfNextField", ASpecial Osfnextfield, True)
  , (0x1004ff60, "osfSelect", ASpecial Osfselect, True)
  , (0x1004ff63, "osfInsert", ASpecial Osfinsert, True)
  , (0x1004ff65, "osfUndo", ASpecial Osfundo, True)
  , (0x1004ff67, "osfMenu", ASpecial Osfmenu, True)
  , (0x1004ff69, "osfCancel", ASpecial Osfcancel, True)
  , (0x1004ff6a, "osfHelp", ASpecial Osfhelp, True)
  , (0x1004ff71, "osfSelectAll", ASpecial Osfselectall, True)
  , (0x1004ff72, "osfDeselectAll", ASpecial Osfdeselectall, True)
  , (0x1004ff73, "osfReselect", ASpecial Osfreselect, True)
  , (0x1004ff74, "osfExtend", ASpecial Osfextend, True)
  , (0x1004ff78, "osfRestore", ASpecial Osfrestore, True)
  , (0x1004ffff, "osfDelete", ASpecial Osfdelete, True)
  -- Deprecated non-character keysym: Reset
  -- Deprecated non-character keysym: System
  -- Deprecated non-character keysym: User
  -- Deprecated non-character keysym: ClearLine
  -- Deprecated non-character keysym: InsertLine
  -- Deprecated non-character keysym: DeleteLine
  -- Deprecated non-character keysym: InsertChar
  -- Deprecated non-character keysym: DeleteChar
  -- Deprecated non-character keysym: BackTab
  -- Deprecated non-character keysym: KP_BackTab
  -- Deprecated non-character keysym: Ext16bit_L
  -- Deprecated non-character keysym: Ext16bit_R
  -- Deprecated non-character keysym: mute_acute
  -- Deprecated non-character keysym: mute_grave
  -- Deprecated non-character keysym: mute_asciicircum
  -- Deprecated non-character keysym: mute_diaeresis
  -- Deprecated non-character keysym: mute_asciitilde
  -- Deprecated non-character keysym: lira
  -- Deprecated non-character keysym: guilder
  -- Deprecated non-character keysym: IO
  -- Deprecated non-character keysym: longminus
  -- Deprecated non-character keysym: block
  ]
