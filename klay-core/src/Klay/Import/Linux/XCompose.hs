module Klay.Import.Linux.XCompose
  ( XComposeEntry
  , parseCompose
  , parseComposeFile
  , symbolP
  , rawKeysymP
  , unicodeKeysymP
  , resultP'
  ) where

import Data.Char (chr, isAlphaNum)
import Data.String (IsString(..))
import Data.Text qualified as T
import Data.Text.IO qualified as T
import Data.List.NonEmpty qualified as NE
import Data.Void (Void)
import Data.Functor ((<&>), void)
import Data.Bifunctor (first)

import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Klay.Keyboard.Layout.Action.DeadKey hiding (char)
import Klay.Keyboard.Layout.Action.DeadKey.Predefined (dkFromBaseChar)
import Klay.Import.Linux.Lookup.Keysyms qualified as KS

-- See also: /usr/share/X11/locale/{used_locale}/Compose

type XComposeEntry = Either (Char, Result) DeadKeySequence

parseComposeFile :: FilePath -> IO (Either String [XComposeEntry])
parseComposeFile path = parseCompose path <$> T.readFile path

parseCompose :: String -> T.Text -> Either String [XComposeEntry]
parseCompose file = first errorBundlePretty . parse composeP file

type Parser = Parsec Void T.Text

composeP :: Parser [XComposeEntry]
composeP = do
  skipMany skipP
  entryP `sepEndBy` some skipP <* eof

skipP :: Parser ()
skipP
  =   try (void eol)
  <|> try space1
  <|> try (L.skipLineComment "#")
  <|> try (L.skipLineComment "include")

entryP :: Parser XComposeEntry
entryP
  =   (Left <$> charSequenceP <?> "Char sequence")
  <|> (Right <$> deadKeySequenceP <?> "Dead key sequence")

charSequenceP :: Parser (Char, Result)
charSequenceP = try do
  s <- eventP symbolP
  case s of
    SDeadKey _ -> empty
    SChar c    -> do
      space
      void (char ':')
      space
      resultP >>= fmap (c,) . \case
        RDeadKey _ -> empty
        r          -> pure r

deadKeySequenceP :: Parser DeadKeySequence
deadKeySequenceP = do
  dk <- try $ eventP deadKeyP
  space1
  symbols <- NE.fromList <$> eventP symbolP `sepEndBy1` space1
  void (char ':')
  space
  result <- resultP
  pure (dk, DeadKeyCombo symbols result)

-- [TODO] modifier?
eventP :: Parser a -> Parser a
eventP = between (try (char '<')) (char '>')

symbolP :: Parser Symbol
symbolP = (namedKeysymP <|> fmap SChar unicodeKeysymP) <?> "Symbol keysym"

namedKeysymP :: Parser Symbol
namedKeysymP
  = try (rawKeysymP >>= maybe (fail "Invalid keysym") pure . KS.keysymToSymbol)
  <?> "Named keysym"

rawKeysymP :: Parser T.Text
rawKeysymP = takeWhile1P (Just "keysym") (\c -> isAlphaNum c || c == '_')

unicodeKeysymP :: Parser Char
unicodeKeysymP =   char 'U'
               *>  L.hexadecimal
               <&> chr
               <?> "Unicode keysym"

deadKeyP :: Parser DeadKey
deadKeyP = symbolP <&> \case
  SDeadKey dk -> dk
  SChar c     -> case dkFromBaseChar c of
    Nothing -> mkCustomDeadKey c
    Just dk -> dk

resultP :: Parser Result
resultP = stringP <|> resultP'

resultP' :: Parser Result
resultP' = symbolP <&> \case
  SChar c     -> RChar c
  SDeadKey dk -> RDeadKey dk

stringP :: Parser Result
stringP = try $ do
  s <- between (char '"') (char '"') $ some charP
  void . optional . try $ space1 <* symbolP
  case s of
    [c] -> pure $ RChar c
    _   -> pure . RText . fromString $ s

charP :: Parser Char
charP = escapedSequenceP <|> try (satisfy (/= '"'))

escapedSequenceP :: Parser Char
escapedSequenceP = try $
  char '\\' *> escapedCharP <|> escapeHexadecimalP

escapedCharP :: Parser Char
escapedCharP = try $ oneOf ("\"\\nt" :: String) <&> \case
  'n' -> '\n'
  't' -> '\t'
  c   -> c

escapeHexadecimalP :: Parser Char
escapeHexadecimalP = try do
  void $ string "0x"
  chr <$> L.hexadecimal -- [TODO] octal
