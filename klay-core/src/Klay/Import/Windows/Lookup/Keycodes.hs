module Klay.Import.Windows.Lookup.Keycodes
  ( parseKey
  , keycodeToKey
  ) where

import Data.Text qualified as T
import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key.Types (Key)
import Klay.Export.Windows.Lookup.Keycodes (winKeyCodes)

parseKey :: T.Text -> Maybe Key
parseKey = (keycodeToKey Map.!?)

keycodeToKey :: Map.Map T.Text Key
keycodeToKey = Map.fromList . fmap (\(k, c) -> (T.pack c, k)) $ winKeyCodes
