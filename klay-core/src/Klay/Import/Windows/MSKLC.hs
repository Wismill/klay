{-# LANGUAGE OverloadedLists #-}

module Klay.Import.Windows.MSKLC
  ( MSKLC(..)
  , parseMsklc
  , parseMsklcFile
  , parseMsklcLayout
  , parseMsklcLayoutFile
  , msklcToLayout
  , defaultEmptyField
  ) where

import Data.Maybe (fromMaybe, catMaybes)
import Data.Char (chr, isAlphaNum, isPrint, isSpace, isHexDigit)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.List.NonEmpty qualified as NE
import Data.Map.Strict qualified as Map
import Data.Void (Void)
import Data.Functor ((<&>), void)
import Data.Bifunctor (Bifunctor(..))
import Control.Arrow ((&&&))
import Control.Monad (join)
import Control.DeepSeq (NFData(..))
import GHC.Generics (Generic)

import Data.Versions qualified as V
import Data.BCP47 qualified as BCP47
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Export.Windows.Common.Layout (simplifyVirtualKeysDefinition)
import Klay.Export.Windows.Common.KeyNames (keyFromKeyNameRef)
import Klay.Export.Windows.IO (read_utf_16_le)
import Klay.Export.Windows.Lookup.Characters (fromWinEncoding)
import Klay.Export.Windows.Lookup.VirtualKeys (parseVk)
import Klay.Export.Windows.Lookup.Locales (WindowsLocale(..), windowsLanguageIds)
import Klay.Import.Windows.Lookup.Keycodes (parseKey)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  ( Actions, CustomActions(..), TAutoActions(..)
  , custom, mDefaultActions, multiOsBlankActionMap, multiOsActions
  , resolveModifiersOptionsLax
  , mkActions, singleton, toOsRawActionsAlteration, (+>))
import Klay.Keyboard.Layout.Action.Special qualified as A
import Klay.Keyboard.Layout.Action.DeadKey hiding (char)
import Klay.Keyboard.Layout.Action.DeadKey.Predefined (dkFromBaseChar)
import Klay.Keyboard.Layout.Group (GroupLevels, mkRawGroup)
import Klay.Keyboard.Layout.Level qualified as L
import Klay.Keyboard.Layout.Modifier
  ( shift, control, isoLevel1, isoLevel2, isoLevel3, isoLevel4
  , alphabetic, fourLevelAlphabetic, fourLevelSemiAlphabetic
  , fourLevelReverseSemiAlphabetic
  , AutoModifiersOptions(..) )
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.OS (MultiOs(..), windows, toMultiOs)
import Data.Alterable (Alteration(..), (!>))
import Klay.Utils.UserInput (RawLText, fromStrictText)

-- | A MSKLC layout
data MSKLC = MSKLC
  { _kMetadata :: !LayoutMetadata
  , _kWindowsOptions :: !WindowsOptions
  , _kLevels :: !GroupLevels
  , _kSymbols :: !ActionsKeyMap
  , _kLigatures :: !Ligatures
  , _kDeadKeys :: ![RawDeadKeyDefinition]
  } deriving (Generic, NFData, Eq, Show)

type SymbolsEntry = (Key, VK.VirtualKey, ModifiersOptions, Map.Map Level MsklcAction)
type Ligatures = Map.Map (VK.VirtualKey, Level) RawLText
type LigatureEntry = ((VK.VirtualKey, Level), RawLText)
type DeadKeys = Map.Map Char DeadKey

data MsklcAction
  = MAction Action
  | MDeadKey Char
  | MLigature
  deriving (Eq, Show)

parseMsklcLayoutFile :: FilePath -> IO (Either String MultiOsRawLayout)
parseMsklcLayoutFile path = fmap msklcToLayout <$> parseMsklcFile path

parseMsklcFile :: FilePath -> IO (Either String MSKLC)
parseMsklcFile path = parseMsklc path <$> read_utf_16_le path

parseMsklcLayout :: String -> T.Text -> Either String MultiOsRawLayout
parseMsklcLayout file = fmap msklcToLayout . parseMsklc file

parseMsklc :: String -> T.Text -> Either String MSKLC
parseMsklc file = first errorBundlePretty . parse klcP file

msklcToLayout :: MSKLC -> MultiOsRawLayout
msklcToLayout klc = Layout
  { _metadata = metadata
  , _dualFunctionKeys = mempty
  , _deadKeys = mkDeadKeyDefinitions . _kDeadKeys $ klc
  , _groups = groups
  , _options = defaultOptions{ _windows = _kWindowsOptions klc }
  }
  where
    metadata = _kMetadata klc
    groups = OneGroup group
    group = mkRawGroup (_name metadata) levels ams
    -- [TODO] control, control + shift should be windows-specific
    levels = toMultiOs $ _kLevels klc
    ams = (  Map.map (fmap ADefault) multiOsBlankActionMap
          +> Map.map multiOsActions ams' )
          -- Ensure we modify Windows actions (especially space) correctly
          !> Map.map (toOsRawActionsAlteration windows . InsertOrReplace) ams'
    ams' = Map.mapWithKey mkActions' symbols
    symbols = _kSymbols klc
    -- [TODO] check if we need to check vk is unchanged
    mkActions' key as
      | default_ == Just as = ADefault (AutoActions DefaultAutoModifiersOptions as)
      | otherwise           = custom as
      where default_ = resolveModifiersOptionsLax <$> mDefaultActions key windows


type Parser = Parsec Void T.Text

klcP :: Parser MSKLC
klcP = do
  (locale, metadata) <- metadataP
  winOptions <- attributesP <|> pure defaultWindowsOptions
  shiftStates <- shiftStatesP
  let levels = fst <$> shiftStates
  symbols1 <- symbolsP levels
  ligatures <- fromMaybe mempty <$> optional (ligaturesP levels)
  deadKeys <- deadKeysP
  let dk_chars = Map.fromList . fmap ((_dkBaseChar &&& id) . fst) $ deadKeys
  symbols2 <- Map.fromList <$> traverse (finalizeSymbols ligatures dk_chars) symbols1
  let symbols3 = Map.mapKeys fst symbols2
  let vkd0 = Map.fromList . Map.keys $ symbols2
  let vkd = simplifyVirtualKeysDefinition symbols3 vkd0
  let has_altGr = _wAltgr winOptions
  let symbols = fix_altGr has_altGr symbols3
  (key_names, dk_names) <- keyNamesP
  let deadKeys' = finalize_dks dk_names deadKeys
  descriptions <- descriptionsP
  let description = finalize_description locale descriptions
  _languages <- languagesNamesP
  string "ENDKBD" *> skipMany skipP <* eof
  pure $ MSKLC
    { _kMetadata = metadata{_description=description}
    , _kWindowsOptions = winOptions
      { _wVirtualKeyDefinition=vkd
      , _wKeysNames=key_names
      }
    , _kLevels = Map.fromList shiftStates
    , _kSymbols = symbols
    , _kLigatures = ligatures
    , _kDeadKeys = deadKeys'
    }
  where
    localeEN = WindowsLocale "0009" "English" mempty

    finalize_description locale descriptions =
      let localeName = BCP47.toText locale
          winLocale = fromMaybe localeEN $ Map.lookup (T.unpack localeName) windowsLanguageIds
      in fromStrictText <$>
        (   Map.lookup (_wLanguageId winLocale) descriptions
        <|> Map.lookup (_wLanguageId localeEN) descriptions )

    finalizeSymbols
      :: Ligatures
      -> DeadKeys
      -> (Key, VK.VirtualKey, ModifiersOptions, Map.Map Level MsklcAction)
      -> Parser ((Key, VK.VirtualKey), Actions)
    finalizeSymbols ligatures dk_chars (key, vk, opts, as) =
      ((key, vk),) . mkActions opts mempty . Map.toList <$> Map.traverseMaybeWithKey f as
      where
        f :: Level -> MsklcAction -> Parser (Maybe Action)
        f _ (MAction NoAction) = pure Nothing -- Remove action
        f _ (MAction a@(AChar c)) -- Check for KP separators
          | vk == VK.VK_DECIMAL
            = pure . Just $ if
            | c == ','  -> ASpecial A.KP_Separator
            | c == '.'  -> ASpecial A.KP_Decimal
            | otherwise -> a
          | vk == VK.VK_SEPARATOR = pure . Just $ if
            | c == ','  -> ASpecial A.KP_Separator
            | c == '.'  -> ASpecial A.KP_Decimal
            | otherwise -> a
        f _ (MAction a) = pure . Just $ a
        f _ (MDeadKey c) = case Map.lookup c dk_chars of
          Nothing -> fail $ "Missing dead key definition: " <> show c
          Just dk -> pure . Just $ ADeadKey dk
        f level MLigature = case Map.lookup (vk, level) ligatures of
          Nothing -> fail $ "Missing ligature definition: " <> show (vk, level)
          Just t  -> pure . Just $ AText t

    -- Note: we can retrieve dead key's name but we have no way to retrieve its label
    finalize_dks names = fmap (bimap (finalize_dk names) (fmap (finalize_combo names)))
    finalize_combo names (DeadKeyCombo s r) =
      let s' = s <&> \case
            SDeadKey dk -> SDeadKey $ finalize_dk names dk
            c           -> c
          r' = case r of
            RDeadKey dk -> RDeadKey $ finalize_dk names dk
            _           -> r
      in DeadKeyCombo s' r'
    finalize_dk names dk = case Map.lookup (_dkBaseChar dk) names of
      Nothing -> dk
      Just n  -> dk{_dkName=fromStrictText n}

    fix_altGr True  = Map.insert K.RAlternate (singleton M.IsoLevel3Set)
    fix_altGr False = id

separatorP :: Parser Char
separatorP = satisfy (\c -> c == ' ' || c == '\t')

separatorsP :: Parser ()
separatorsP = skipMany separatorP

separators1P :: Parser ()
separators1P = skipSome separatorP

skipP :: Parser ()
skipP = separatorsP <* optional commentP <* eol

systemNameP :: Parser T.Text
systemNameP = takeWhile1P (Just "system name") (\c -> isAlphaNum c || c == '_')

metadataP :: Parser (BCP47.BCP47, LayoutMetadata)
metadataP = do
  system_name <- string "KBD" *> separators1P *> systemNameP <* separators1P
  name <- quotedText1P <* skipSome skipP
  copyright <- join <$> optional (copyrightP <* skipSome skipP)
  company <- join <$> optional (companyP <* skipSome skipP)
  locale <- localeNameP <* skipSome skipP
  _localeId <- localeIdP <* skipSome skipP
  version <- join <$> optional (versionP <* skipSome skipP)
  pure (locale, mempty
    { _name = fromStrictText name
    , _systemName = fromStrictText system_name
    , _locales = mempty
      { _locPrimary = [locale] }
    , _version = version
    , _author = company
    , _license = copyright
    })

copyrightP :: Parser (Maybe RawLText)
copyrightP
  = string "COPYRIGHT" *> separators1P
  *> maybeQuotedTextP <&> fmap fromStrictText

companyP :: Parser (Maybe RawLText)
companyP = string "COMPANY" *> separators1P
  *> maybeQuotedTextP <&> fmap fromStrictText

localeNameP :: Parser BCP47.BCP47
localeNameP
  =  string "LOCALENAME" *> separators1P
  *> quotedTextP >>= \t -> case BCP47.fromText t of
    Left err -> fail $ "Invalid locale name: " <> T.unpack err
    Right l  -> pure l


localeIdP :: Parser Int
localeIdP = string "LOCALEID" *> separators1P *> between (char '"') (char '"') L.hexadecimal

versionP :: Parser (Maybe Version)
versionP
  =  string "VERSION"
  *> optional
    (  try
    $  separators1P
    *> takeWhile1P (Just "version") (\c -> isAlphaNum c || c == '.' || c == '-' || c == '+')
    -- Note: package Versions expose its own Megaparsec parser, but it eats spaces
    >>= \t -> case V.version t of
      Left _  -> fail $ "Invalid version: " <> T.unpack t
      Right v -> pure (Version v)
    )

attributesP :: Parser WindowsOptions
attributesP = do
  string "ATTRIBUTES" *> skipSome skipP
  as <- attributeP `sepEndBy` skipSome skipP
  pure defaultWindowsOptions
    { _wAltgr = "ALTGR" `elem` as
    , _wIsoLevel3IsAltGr = "ALTGR" `elem` as
    , _wShiftDeactivatesCaps = "SHIFTLOCK" `elem` as
    , _wLrmRlm = "LRM_RLM" `elem` as
    }

attributeP :: Parser T.Text
attributeP = choice (
  [ string "ALTGR"
  , string "SHIFTLOCK"
  , string "LRM_RLM"
  ] :: [Parser T.Text])

shiftStatesP :: Parser [(Level, RawLText)]
shiftStatesP = do
  string "SHIFTSTATE" *> skipSome skipP
  shiftStateP `sepEndBy1` skipSome skipP

shiftStateP :: Parser (Level, RawLText)
shiftStateP = choice (
  [ (isoLevel1, L.defaultIsoLevel1) <$ char '0'
  , (isoLevel2, L.defaultIsoLevel2) <$ char '1'
  , (control, L.defaultControlLevel) <$ char '2'
  , (shift <> control, L.defaultControlShiftLevel) <$ char '3'
  , (isoLevel3, L.defaultIsoLevel3) <$ char '6'
  , (isoLevel4, L.defaultIsoLevel4) <$ char '7'
  ] :: [Parser (Level, RawLText)])

symbolsP :: [Level] -> Parser [SymbolsEntry]
symbolsP levels = do
  void $ string "LAYOUT" *> skipSome skipP
  symbolsEntryP levels `sepEndBy1` skipSome skipP

symbolsEntryP :: [Level] -> Parser SymbolsEntry
symbolsEntryP levels = do
  key <- try (keyP 'T') <* separators1P
  vk <- vkP <* separators1P
  raw_opts <- modifierOptionsP <* separators1P
  raw_actions <- actionP `sepEndBy1` separators1P
  let actions = Map.fromList $ zip levels raw_actions
  let opts = mkModifiersOptions levels raw_actions raw_opts
  pure (key, vk, opts, actions)

keyP :: Char -> Parser Key
keyP prefix
  = takeP (Just "key identifier") 2
  >>= parseKey' . T.cons prefix . T.toUpper
  where
    parseKey' key_id = case parseKey key_id of
      Nothing  -> fail $ "Unsupported key:" <> T.unpack key_id
      Just key -> pure key

keyP' :: Char -> Parser (Either T.Text Key)
keyP' prefix
  = takeP (Just "key identifier") 2
  >>= parseKey' . T.cons prefix . T.toUpper
  where
    parseKey' key_id = case keyFromKeyNameRef key_id of
      Nothing  -> fail $ "Unsupported key:" <> T.unpack key_id
      Just key -> pure key

vkP :: Parser VK.VirtualKey
vkP = takeWhile1P (Just "Virtual key") (\c -> isAlphaNum c || c == '_')
    >>= \vk_id -> case parseVk vk_id of
      Nothing -> fail $ "Invalid VK: " <> T.unpack vk_id
      Just vk -> pure vk

-- | Raw Modifier Options
modifierOptionsP :: Parser T.Text
modifierOptionsP = choice (
  [ string "0"
  , string "1"
  , string "4"
  , string "5"
  -- [TODO] other modifiers
  -- , string "SGCap"
  ] :: [Parser T.Text])

-- | Process raw Modifier Options
mkModifiersOptions :: [Level] -> [MsklcAction] -> T.Text -> ModifiersOptions
mkModifiersOptions ls as = \case
  "1" -> semi_alphabetic
  "4" -> fourLevelReverseSemiAlphabetic
  "5" -> alphabetic'
  -- [TODO] other modifiers
  -- Note: the processing of SGCap should depend on the presence of 'Capitals' in the level modifiers
  _   -> mempty
  where
    ls' = fmap fst . filter ((/= MAction NoAction) . snd) $ zip ls as
    hasLevel3 = isoLevel3 `elem` ls' || isoLevel4 `elem` ls'
    semi_alphabetic
      | hasLevel3 = fourLevelSemiAlphabetic
      | otherwise = alphabetic
    alphabetic'
      | hasLevel3 = fourLevelAlphabetic
      | otherwise = alphabetic

actionP :: Parser MsklcAction
actionP = choice (
  [ MAction NoAction <$ string "-1"
  , MAction . AChar <$> try (alphaNumChar <* notFollowedBy alphaNumChar)
  , charOrDeadKeyP
  , MLigature <$ string "%%"
  ] :: [Parser MsklcAction])

hexaCharP :: Parser Char
hexaCharP = chr <$> L.hexadecimal

charOrDeadKeyP :: Parser MsklcAction
charOrDeadKeyP = do
  c <- hexaCharP
  maybe (MAction (AChar c)) (const (MDeadKey c)) <$> optional (char '@')

ligaturesP :: [Level] -> Parser Ligatures
ligaturesP levels = do
  void $ string "LIGATURE" *> skipSome skipP
  let maxLevel = length levels - 1
  Map.fromList <$> (ligatureEntryP levels maxLevel `sepEndBy` skipSome skipP)

ligatureEntryP :: [Level] -> Int -> Parser LigatureEntry
ligatureEntryP levels maxLevel = do
  notFollowedBy (string "DEADKEY")
  notFollowedBy (string "KEYNAME")
  vk <- vkP <* separators1P
  levelIndex <- L.decimal <* separators1P
  level <- if levelIndex > maxLevel
    then fail $ "Invalid index: " <> show levelIndex
    else pure (levels !! levelIndex)
  value <- fromWinEncoding <$> L.hexadecimal `sepEndBy1` separators1P
  pure ((vk, level), fromStrictText value)

deadKeysP :: Parser [RawDeadKeyDefinition]
deadKeysP = many deadKeyP

deadKeyP :: Parser RawDeadKeyDefinition
deadKeyP = do
  void $ string "DEADKEY" *> separators1P
  base_char <- hexaCharP <* skipSome skipP
  let dk = lookupDk base_char
  (dk,) <$> deadKeyComboP `sepEndBy1` skipSome skipP
  where
    lookupDk base_char = case dkFromBaseChar base_char of
      Just dk -> dk
      Nothing -> mkCustomDeadKey base_char

deadKeyComboP :: Parser DeadKeyCombo
deadKeyComboP = do
  notFollowedBy (string "DEADKEY")
  s <- SChar <$> hexaCharP <* separators1P
  r <- RChar <$> hexaCharP
  pure $ mkCombo (s NE.:| []) r

keyNamesP :: Parser (KeyNames, Map.Map Char T.Text)
keyNamesP = do
  string "KEYNAME" *> skipSome skipP
  keynames_t <- catMaybes <$> many (try (pKeyName 'T') <* skipSome skipP)
  string "KEYNAME_EXT" *> skipSome skipP
  keynames_x <- catMaybes <$> many (try (pKeyName 'X') <* skipSome skipP)
  let keynames = Map.map fromStrictText . Map.fromList $ keynames_t <> keynames_x
  dk_names_list <- optional do
    string "KEYNAME_DEAD" *> skipSome skipP
    deadKeyNameP `sepEndBy` skipSome skipP
  let dk_names = maybe mempty Map.fromList dk_names_list
  pure (keynames, dk_names)

pKeyName :: Char -> Parser (Maybe (Key, T.Text))
pKeyName prefix = do
  notFollowedBy
    (   string "KEYNAME_EXT"
    <|> string "KEYNAME_DEAD"
    <|> string "DESCRIPTIONS" )
  key <- keyP' prefix <* separators1P
  name <- textP
  pure case key of
    Left _     -> Nothing
    Right key' -> Just (key', name)

deadKeyNameP :: Parser (Char, T.Text)
deadKeyNameP = do
  notFollowedBy (string "DESCRIPTIONS")
  c <- hexaCharP <* separators1P
  name <- textP
  pure (c, name)

descriptionsP :: Parser (Map.Map String T.Text)
descriptionsP = do
  void $ string "DESCRIPTIONS" *> skipSome skipP
  -- void $ many (try $ L.hexadecimal <* separators1P <* skipSome (satisfy (/= '\n')) <* eol :: Parser Int)
  -- skipSome skipP
  descriptions <- try descriptionP `sepEndBy` skipSome skipP
  pure . Map.fromList . catMaybes $ descriptions

descriptionP :: Parser (Maybe (String, T.Text))
descriptionP = do
  language <- languageIdP
  description <- optional (try $ separators1P *> textP')
  pure $ (T.unpack language,) <$> description

languagesNamesP :: Parser (Map.Map T.Text T.Text)
languagesNamesP = do
  void $ string "LANGUAGENAMES" *> skipSome skipP
  names <- try languageNameP `sepEndBy` skipSome skipP
  pure . Map.fromList . catMaybes $ names

languageNameP :: Parser (Maybe (T.Text, T.Text))
languageNameP = do
  notFollowedBy "ENDKBD"
  language <- languageIdP
  name <- optional (try $ separators1P *> textP')
  pure $ (language,) <$> name

languageIdP :: Parser T.Text
languageIdP = takeWhile1P (Just "language ID") isHexDigit

commentP :: Parser ()
commentP = L.skipLineComment "//" <|> L.skipLineComment ";"

textP :: Parser T.Text
textP = try quotedTextP <|> unquotedTextP

textP' :: Parser T.Text
textP' = try quotedTextP <|> unquotedTextWithSpacesP

quotedText1P :: Parser T.Text
quotedText1P = between (char '"') (char '"') $ takeWhile1P (Just "quoted text") (\c -> c /= '"' && c /= '\n')

quotedTextP :: Parser T.Text
quotedTextP = between (char '"') (char '"') $ takeWhileP (Just "quoted text") (\c -> c /= '"' && c /= '\n')

maybeQuotedTextP :: Parser (Maybe T.Text)
maybeQuotedTextP
  = between (char '"') (char '"')
  $ takeWhileP (Just "quoted text") (\c -> c /= '"' && c /= '\n')
  <&> \case
    ""       -> Nothing
    "(none)" -> Nothing
    t        -> Just t

-- Note: keep synchronised with maybeQuotedTextP
defaultEmptyField :: TL.Text
defaultEmptyField = "(none)"

unquotedTextP :: Parser T.Text
unquotedTextP = takeWhile1P (Just "unquoted text") (\c -> isPrint c && not (isSpace c))

unquotedTextWithSpacesP :: Parser T.Text
unquotedTextWithSpacesP = takeWhile1P (Just "unquoted text") (\c -> isPrint c && c /= '\n')
