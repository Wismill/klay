{-# LANGUAGE OverloadedLists #-}

module Klay.OS
  ( OS(..)
  , OsSpecific(..)
  , MultiOs(..)
  , MultiOsId
  , pattern TOsIndependent
  , pattern TLinux
  , pattern TWindows
  , fromListWithDefault
  , getOs
  , onOs
  , onOss
  , mapWithOs
  , traverseMultiOs
  , foldlWithOs
  , toMultiOs
  , toMultiOsWithDefault
  , toNonEmptyMap
  , toNonEmpty
  , allOsSpecific
  , osIndependent
  , linux
  , windows
  , serializeOsSpecific
  , parseOsSpecific
  ) where

import Data.Void (Void)
import Data.Tuple (swap)
import Data.Text qualified as T
import Data.Semigroup hiding (diff)
import Data.List (intersperse)
import Data.List.NonEmpty qualified as NE
import Data.Map.NonEmpty qualified as NEMap
import Data.HashMap.Strict qualified as HMap
import Data.Functor (($>))
import Data.Foldable (foldl')
import Control.DeepSeq (NFData(..))
import GHC.Exts (IsList(..))
import GHC.Generics (Generic)

import TextShow (TextShow)
import TextShow.Generic (FromGeneric(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..), genericShrink)
import Test.QuickCheck.Gen (chooseEnum, frequency)
import Data.Aeson qualified as Aeson
import Data.Aeson.Types (ToJSON(..), ToJSONKey(..), FromJSON(..), FromJSONKey(..), (.=))
import Data.Aeson.Types qualified as Aeson
import Text.Megaparsec
import Text.Megaparsec.Char

import Data.Alterable (Magma(..), Alterable(..), Differentiable(..))
import Klay.Utils (jsonOptions, jsonKeyOptions, jsonKeyOptions)

-- | Supported OS.
data OS
  = Linux
  | Windows
  deriving (Generic, NFData, Eq, Ord, Bounded, Enum, Show, Read)
  deriving TextShow via (FromGeneric OS)

instance ToJSON OS where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON OS where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance ToJSONKey OS where
  toJSONKey = Aeson.genericToJSONKey (jsonKeyOptions id)

instance FromJSONKey OS where
  fromJSONKey = Aeson.genericFromJSONKey (jsonKeyOptions id)

instance Arbitrary OS where
  arbitrary = chooseEnum (minBound, maxBound)
  shrink = genericShrink

-- | Equivalent to @'Maybe' 'OS'@.
data OsSpecific
  = OsIndependent
  | OsSpecific !OS
  deriving (Generic, NFData, Eq, Ord, Show, Read)
  deriving TextShow via (FromGeneric OsSpecific)

serializeOsSpecific :: OsSpecific -> T.Text
serializeOsSpecific OsIndependent        = "default"
serializeOsSpecific (OsSpecific Linux)   = "linux"
serializeOsSpecific (OsSpecific Windows) = "windows"

parseOsSpecific :: T.Text -> Maybe OsSpecific
parseOsSpecific "default"        = Just OsIndependent
parseOsSpecific "os_independent" = Just OsIndependent
parseOsSpecific "linux"          = Just (OsSpecific Linux)
parseOsSpecific "Linux"          = Just (OsSpecific Linux)
parseOsSpecific "windows"        = Just (OsSpecific Windows)
parseOsSpecific "Windows"        = Just (OsSpecific Windows)
parseOsSpecific _                = Nothing

instance ToJSON OsSpecific where
  toJSON     = Aeson.String . serializeOsSpecific
  toEncoding = toEncoding . serializeOsSpecific

instance FromJSON OsSpecific where
  parseJSON = Aeson.withText "OsSpecific" \t ->
    case parseOsSpecific t of
      Nothing -> fail $ "Invalid OS: " <> T.unpack t
      Just os -> pure os

instance ToJSONKey OsSpecific where
  toJSONKey = Aeson.toJSONKeyText serializeOsSpecific

instance FromJSONKey OsSpecific where
  fromJSONKey = Aeson.FromJSONKeyTextParser \t ->
    case parseOsSpecific t of
      Nothing -> fail $ "Invalid OS: " <> T.unpack t
      Just os -> pure os

instance Arbitrary OsSpecific where
  arbitrary = frequency
    [ (1, pure OsIndependent)
    , (1 + fromEnum (maxBound :: OS), OsSpecific <$> arbitrary)
    ]
  shrink = genericShrink

allOsSpecific :: NE.NonEmpty OsSpecific
allOsSpecific = OsIndependent NE.:| fmap OsSpecific (enumFromTo minBound maxBound)

osIndependent, linux, windows :: OsSpecific
osIndependent = OsIndependent
linux = OsSpecific Linux
windows = OsSpecific Windows


--- MultiOs -------------------------------------------------------------------

type MultiOsId a = MultiOs a a a

-- | A container for values that may vary with the OS
data MultiOs i l w = MultiOs
  { _osIndependent :: !i
  -- ^ OS independent value. May also be interpreted as some default value
  , _linux :: !l
  -- ^ Linux
  , _windows :: !w
  -- ^ Windows
  }
  deriving stock (Generic, Eq, Ord, Show)
  deriving anyclass NFData

instance (Monoid a) => IsList (MultiOsId a) where
  type Item (MultiOsId a) = (OsSpecific, a)

  fromList = fromListWithDefault mempty

  toList xs =
    [ (OsIndependent, _osIndependent xs)
    , (OsSpecific Linux, _linux xs)
    , (OsSpecific Windows, _windows xs)
    ]

fromListWithDefault :: a -> [(OsSpecific, a)] -> MultiOsId a
fromListWithDefault a0 = foldl' go (MultiOs a0 a0 a0)
  where go acc (OsIndependent, a) = acc{_osIndependent=a}
        go acc (OsSpecific Linux, a) = acc{_linux=a}
        go acc (OsSpecific Windows, a) = acc{_windows=a}

pattern TOsIndependent :: a -> (OsSpecific, a)
pattern TOsIndependent a = (OsIndependent, a)

pattern TLinux :: a -> (OsSpecific, a)
pattern TLinux a = (OsSpecific Linux, a)

pattern TWindows :: a -> (OsSpecific, a)
pattern TWindows a = (OsSpecific Windows, a)

instance {-# OVERLAPPABLE #-} (Magma i, Magma l, Magma w) => Magma (MultiOs i l w) where
  MultiOs i1 l1 w1 +> MultiOs i2 l2 w2 = MultiOs (i1 +> i2) (l1 +> l2) (w1 +> w2)

instance (Semigroup i, Semigroup l, Semigroup w) => Semigroup (MultiOs i l w) where
  MultiOs i1 l1 w1 <> MultiOs i2 l2 w2 = MultiOs (i1 <> i2) (l1 <> l2) (w1 <> w2)

instance (Monoid i, Monoid l, Monoid w) => Monoid (MultiOs i l w) where
  mempty = MultiOs mempty mempty mempty

instance {-# OVERLAPPABLE #-} (ToJSON i, ToJSON l, ToJSON w) => ToJSON (MultiOs i l w) where
  toJSON     = Aeson.genericToJSON (jsonOptions multiOsFieldLabelModifier)
  toEncoding = Aeson.genericToEncoding (jsonOptions multiOsFieldLabelModifier)

instance {-# OVERLAPPING #-} (ToJSON a, Ord a) => ToJSON (MultiOsId a) where
  toJSON
    = Aeson.object
    . NE.toList
    . fmap (uncurry (.=))
    . NEMap.toList
    . NEMap.mapKeys serializeOsSpecificList
    . toGroupedOsSpecific
  toEncoding
    = Aeson.pairs . sconcat
    . fmap (uncurry (.=))
    . NEMap.toList
    . NEMap.mapKeys serializeOsSpecificList
    . toGroupedOsSpecific

multiOsFieldLabelModifier :: String -> String
multiOsFieldLabelModifier "_osIndependent" = "os independent"
multiOsFieldLabelModifier s                = tail s

serializeOsSpecificList :: [OsSpecific] -> T.Text
serializeOsSpecificList = mconcat . intersperse "+" . fmap serializeOsSpecific

toGroupedOsSpecific :: (Ord a) => MultiOsId a -> NEMap.NEMap [OsSpecific] a
toGroupedOsSpecific
  = NEMap.fromList
  . fmap swap
  . NEMap.toList
  . NEMap.fromListWith (<>)
  . fmap (\(os, a) -> (a, [os]))
  . toNonEmpty

instance (FromJSON i, FromJSON l, FromJSON w) => FromJSON (MultiOs i l w) where
  parseJSON = Aeson.withObject "MultiOs"
            $ traverseMultiOs (MultiOs (check "default") (check "linux") (check "windows"))
            . fromListWithDefault Nothing
            . mconcat
            . fmap go
            . HMap.toList
    where check :: forall x. (FromJSON x) => String -> Maybe Aeson.Value -> Aeson.Parser x
          check os Nothing  = fail $ "missing key: " <> os
          check _  (Just x) = parseJSON x
          go (oss, x)
            = either fail' (\oss' x' -> fmap (,x') oss') (parseOSs oss)
            . Just $ x
          parseOSs = parse (osSpecificListP <* eof) mempty
          fail' e _ = fail (errorBundlePretty e)

type Parser = Parsec Void T.Text

osSpecificListP :: Parser [OsSpecific]
osSpecificListP = osSpecificP `sepBy1` (space *> pSep *> space)
  where pSep = char ',' <|> char '+' <|> char '&' <|> char '|'

osSpecificP :: Parser OsSpecific
osSpecificP = choice (
  [ string "default" $> osIndependent
  , string "os_independent" $> osIndependent
  , string "os independent" $> osIndependent
  , string "linux" $> linux
  , string "Linux" $> linux
  , string "windows" $> windows
  , string "Windows" $> windows
  ] :: [Parser OsSpecific])

instance (Arbitrary i, Arbitrary l, Arbitrary w) => Arbitrary (MultiOs i l w) where
  arbitrary = MultiOs <$> arbitrary <*> arbitrary <*> arbitrary
  shrink = genericShrink

instance (Differentiable i di, Differentiable l dl, Differentiable w dw) => Differentiable (MultiOs i l w) (MultiOs (Maybe di) (Maybe dl) (Maybe dw)) where
  diff (MultiOs i1 l1 w1) (MultiOs i2 l2 w2) = case MultiOs (diff i1 i2) (diff l1 l2) (diff w1 w2) of
    MultiOs Nothing Nothing Nothing -> Nothing
    d                               -> Just d

instance {-# OVERLAPPING #-} (Alterable i di, Alterable l dl, Alterable w dw) => Alterable (MultiOs i l w) (MultiOs di dl dw) where
  alter (MultiOs di dl dw) (MultiOs i l w) = MultiOs (alter di i) (alter dl l) (alter dw w)

getOs :: OsSpecific -> MultiOsId a -> a
getOs OsIndependent        = _osIndependent
getOs (OsSpecific Linux)   = _linux
getOs (OsSpecific Windows) = _windows

onOs :: OsSpecific -> (a -> a) -> MultiOsId a -> MultiOsId a
onOs OsIndependent        f a = a{_osIndependent = f $ _osIndependent a}
onOs (OsSpecific Linux)   f a = a{_linux = f $ _linux a}
onOs (OsSpecific Windows) f a = a{_windows = f $ _windows a}

onOss :: MultiOs (i -> i') (l -> l') (w -> w') -> MultiOs i l w -> MultiOs i' l' w'
onOss (MultiOs fi fl fw) (MultiOs i l w) = MultiOs (fi i) (fl l) (fw w)

traverseMultiOs :: (Applicative f) => MultiOs (i -> f i') (l -> f l') (w -> f w') -> MultiOs i l w -> f (MultiOs i' l' w')
traverseMultiOs (MultiOs fi fl fw) (MultiOs i l w) = MultiOs <$> fi i <*> fl l <*> fw w

mapWithOs :: (OsSpecific -> a -> a') -> MultiOsId a -> MultiOsId a'
mapWithOs f (MultiOs i l w) = MultiOs (f osIndependent i) (f linux l) (f windows w)

foldlWithOs :: (b -> OsSpecific -> a -> b) -> b -> MultiOsId a -> b
foldlWithOs f b = foldl' (uncurry . f) b . toNonEmpty

toMultiOs :: a -> MultiOsId a
toMultiOs a = MultiOs a a a

toMultiOsWithDefault
  :: a          -- ^ Default value
  -> OsSpecific -- ^ OS with value
  -> a          -- ^ Value for given OS
  -> MultiOsId a
toMultiOsWithDefault a0 os a = onOs os (const a) (toMultiOs a0)

toNonEmptyMap :: MultiOsId a -> NEMap.NEMap OsSpecific a
toNonEmptyMap = NEMap.fromList . toNonEmpty

toNonEmpty :: MultiOsId a -> NE.NonEmpty (OsSpecific, a)
toNonEmpty (MultiOs i l w) =
  [ (osIndependent, i)
  , (linux, l)
  , (windows, w)
  ]
