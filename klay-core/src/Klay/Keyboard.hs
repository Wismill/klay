{-|
Description: Keyboard layout modelling

The layout of a keyboard may refer to:

* its [/physical/](#lphysical) (arrangement of keys) layout,
* its [/visual/](#lvisual) (physical labeling of keys) layout, or
* its [/functional/](#lfunctional) (software response to a key press or release) layout.

= Physical layouts #lphysical#
Physical layout is the actual positioning of keys on a keyboard.

= Visual layouts #lvisual#
Visual layout the arrangement of the legends (labels, markings, engravings) that appear
on the physical keycaps.
Visual layouts vary by language, country, and user preference.

= Functional layouts #lfunctional#
The functional layout of the keyboard refers to the mapping between the physical keys, such as the A key,
and software events, such as the letter "A" appearing on the screen.
Such mapping is called __key mapping__.

-}
-- [TODO] Mobile layouts

module Klay.Keyboard
  ( -- [TODO] choose modules to re-export
  ) where
