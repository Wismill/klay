module Klay.Check
  ( LayoutErrors(..)
  , MappingErrors(..)
  , checkLayout
  , checkLayoutByOs
  , checkGroup
  , checkDeadKeys
  ) where


import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Foldable (foldlM)
import GHC.Generics (Generic)

import Control.Monad.State.Strict hiding (state)

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.Actions (Actions, foldlActions)
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Group
  ( MultiOsRawGroups, FinalGroups, KeyFinalGroup
  , resolveFinalGroups)
import Klay.Keyboard.Layout.VirtualKey (VirtualKey)
import Klay.OS (OsSpecific, allOsSpecific)
import Klay.Utils.UserInput (RawLText)
import Data.Functor ((<&>))

type S = State LayoutErrors

-- | Same as 'checkLayoutByOs' but for all OS
checkLayout :: MultiOsRawLayout -> LayoutErrors
checkLayout = checkLayoutByOs allOsSpecific

{-|
Check for the following errors in a given layout:

* Are all the defined virtual keys mapped to actions?
* Do all the used dead keys have definitions?
-}
checkLayoutByOs :: (Foldable f) => f OsSpecific -> MultiOsRawLayout -> LayoutErrors
checkLayoutByOs oss layout = execState processLayout mempty
  where
    processLayout :: S ()
    processLayout = foldlM processOs mempty oss >>= checkDeadKeys dksDef

    processOs :: Set DeadKey -> OsSpecific -> S (Set DeadKey)
    processOs acc os
      =   processGroups os (_groups layout)
      >>= foldlM checkGroup mempty
      <&> (acc <>)

    processGroups :: OsSpecific -> MultiOsRawGroups -> S FinalGroups
    processGroups os gs =
      let groups = resolveFinalGroups os gs
          -- vkErrors = maybe mempty (foldr (\g -> Set.union (Set.map (_groupName g,) . _groupMapping $ g)) mempty)
          --          . check_unmapped_vks
          --          $ groups
          vkErrors = mempty -- [FIXME] check_unmapped_vks
          errors = MappingErrors{_vkNotMapped=vkErrors}
      in do
        when (errors /= mempty) $ modify (\s -> s{_checkMappingErrors=Map.insert os errors $ _checkMappingErrors s})
        pure groups

    dksDef = _deadKeys layout


checkDeadKeys :: DeadKeyDefinitions -> Set DeadKey -> S ()
checkDeadKeys DeadKeyDefinitions{_dkMergedDefinitions=dksDef,_dkErrors=merge_errors} dks =
  let (_, errors) = resolveMergedDeadKeyDefinitions dks dksDef in updateDkErrors (errors <> merge_errors)
  where updateDkErrors err = modify (mempty{_checkDkErrors=err} <>)

checkGroup :: Set DeadKey -> KeyFinalGroup -> S (Set DeadKey)
checkGroup s = foldlM checkVk s . _groupMapping

checkVk :: Set DeadKey -> Actions -> S (Set DeadKey)
checkVk acc = pure . foldlActions (\acc' _ _ a -> getDks acc' a) acc
  where
    getDks :: Set DeadKey -> Action -> Set DeadKey
    getDks dks (ADeadKey dk) = Set.insert dk dks
    getDks dks _             = dks

-- [TODO] better errors detail by group
-- | Error descriptor for a layout
data LayoutErrors = LayoutErrors
  { _checkMappingErrors :: Map.Map OsSpecific MappingErrors
  , _checkDkErrors :: DeadKeyError
  } deriving (Generic, Eq, Show)

instance Semigroup LayoutErrors where
  (LayoutErrors m1 dks1) <> (LayoutErrors m2 dks2) = LayoutErrors (m1 <> m2) (dks1 <> dks2)

instance Monoid LayoutErrors where
  mempty = LayoutErrors mempty mempty

newtype MappingErrors = MappingErrors
  { _vkNotMapped :: Set (RawLText, VirtualKey)
  } deriving (Generic, Eq, Show)

instance Semigroup MappingErrors where
  (MappingErrors vks1) <> (MappingErrors vks2) = MappingErrors (vks1 <> vks2)

instance Monoid MappingErrors where
  mempty = MappingErrors mempty
