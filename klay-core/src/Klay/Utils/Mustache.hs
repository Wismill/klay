module Klay.Utils.Mustache
  ( generateFileContentFromTemplate
  ) where

import Data.String (IsString(..))
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.IO.Utf8 qualified as Utf8

import Data.Aeson (Value)
import Text.Megaparsec qualified as Mega
import Text.Mustache qualified as Mustache

import Paths_klay_core (getDataFileName)

generateFileContentFromTemplate :: String -> FilePath -> Value -> IO (Maybe TL.Text)
generateFileContentFromTemplate msg template_file vars = do
  template_file' <- getDataFileName template_file
  templateContent <- TL.toStrict <$> Utf8.readFile template_file'
  case Mustache.compileMustacheText (fromString template_file) templateContent of
    Left bundle -> do
      putStrLn . mconcat $ ["[ERROR] ", msg, ": Cannot compile Mustache template: ", template_file]
      putStrLn (Mega.errorBundlePretty bundle)
      pure Nothing
    Right template -> pure . Just $ Mustache.renderMustache template vars
