{-# LANGUAGE FlexibleContexts #-}

module Klay.Utils.Unicode
  ( isShowableChar
  , showHex
  , showHeX
  , showUnicode
  , showHexB, showHeXB
  , showUnicodeB, showUnicodEB
  , padL, padR
  , center
  , safeDeadKeyChars
  , nonCharacters
  , privateUseAreas
  , unicodeOffset
  , unsafeUnicodeOffset
  , showInvisibleChar
  , showCombiningChar )
  where

import Numeric qualified
import Data.Char (chr, ord, toUpper, isAlphaNum, isPunctuation, isSymbol, isMark, intToDigit)
import Data.String (IsString(..))
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB

import TextShow (TextShow(..))
import TextShow.Data.Integral (showbIntAtBase)

-- | Convert a character into its Unicode code point in /upper/ case,
-- prefixed with @U+@ and corresponding @0@ if necessary.
showUnicode :: Char -> String
showUnicode = ("U+" <>) . showHeX 4 . ord

-- | Convert a number into the hexadecimal base in /lower/ case. Prefix with @0@ if necessary.
showHex
  :: Int    -- ^ Minimum length
  -> Int    -- ^ Number to convert
  -> String
showHex k n = pad (Numeric.showHex n mempty)
  where pad s = replicate (k - length s) '0' <> s

-- | Convert a number into the hexadecimal base in /upper/ case. Prefix with @0@ if necessary.
showHeX
  :: Int    -- ^ Minimum length
  -> Int    -- ^ Number to convert
  -> String
showHeX k = fmap toUpper . showHex k

-- | Convert a number into the hexadecimal base in /lower/ case.
showHexB
  :: (Integral n, TextShow n)
  => n          -- ^ Number to convert
  -> TB.Builder -- ^ Unicode code point.
showHexB = showbIntAtBase 16 intToDigit

-- | Convert a number into the hexadecimal base in /upper/ case.
showHeXB
  :: (Integral n, TextShow n)
  => n          -- ^ Number to convert
  -> TB.Builder -- ^ Unicode code point.
showHeXB = showbIntAtBase 16 (toUpper . intToDigit)

-- | Convert a character into its Unicode code point in /lower/ case.
showUnicodeB
  :: Char       -- ^ Character to convert
  -> TB.Builder -- ^ Unicode code point.
showUnicodeB = showHexB . ord

-- | Convert a character into its Unicode code point in /upper/ case.
showUnicodEB
  :: Char       -- ^ Character to convert
  -> TB.Builder -- ^ Unicode code point.
showUnicodEB = showHeXB . ord

-- | Pad a text at the left if necessary.
padL
  :: Int        -- ^ Target length @l@
  -> Char       -- ^ Filling character @c@
  -> TB.Builder -- ^ Text to pad
  -> TB.Builder -- ^ Output text of length @l@ or more completed to the left by repeating @c@ if necessary.
padL n c = TB.fromLazyText . TL.justifyRight (fromIntegral n) c . TB.toLazyText

-- | Pad a text at the right if necessary.
padR
  :: Int        -- ^ Target length @l@
  -> Char       -- ^ Filling character @c@
  -> TB.Builder -- ^ Text to pad
  -> TB.Builder -- ^ Output text of length @l@ or more completed to the right by repeating @c@ if necessary.
padR n c = TB.fromLazyText . TL.justifyLeft (fromIntegral n) c . TB.toLazyText

-- | Center a text at the given length with filling character
center
  :: Int        -- ^ Target length @l@
  -> Char       -- ^ Filling character @c@
  -> TB.Builder -- ^ Text to center
  -> TB.Builder -- ^ Output text of length @l@ or more.
center n c = TB.fromLazyText . TL.center (fromIntegral n) c . TB.toLazyText

-- | Set of characters that can be safely used to encode dead keys.
safeDeadKeyChars :: String
safeDeadKeyChars = nonCharacters <> (mconcat . fmap rangeToChars $ privateUseAreas)
  where rangeToChars (b1, b2) = fmap chr [b1..b2]

{-|
A /noncharacter/ is a code point that is permanently reserved in the Unicode Standard for internal use.

See: http://www.unicode.org/faq/private_use.html, section “Noncharacters”
-}
nonCharacters :: String
nonCharacters = fmap chr $ [0xFDD0..0xFDEF] <>
  [ 0x0FFFE,  0x0FFFF
  , 0x1FFFE,  0x1FFFF
  , 0x2FFFE,  0x2FFFF
  , 0x3FFFE,  0x3FFFF
  , 0x4FFFE,  0x4FFFF
  , 0x5FFFE,  0x5FFFF
  , 0x6FFFE,  0x6FFFF
  , 0x7FFFE,  0x7FFFF
  , 0x8FFFE,  0x8FFFF
  , 0x9FFFE,  0x9FFFF
  , 0xAFFFE,  0xAFFFF
  , 0xBFFFE,  0xBFFFF
  , 0xCFFFE,  0xCFFFF
  , 0xDFFFE,  0xDFFFF
  , 0xEFFFE,  0xEFFFF
  , 0xFFFFE,  0xFFFFF
  , 0x10FFFE, 0x10FFFF
  ]

{-|
/Private-use characters/ are code points whose interpretation is not specified by a character encoding standard and whose use and interpretation may be determined by private agreement among cooperating users. Private-use characters are sometimes also referred to as user-defined characters (UDC) or vendor-defined characters (VDC).

See: http://www.unicode.org/faq/private_use.html, section “Noncharacters”
-}
privateUseAreas :: [(Int, Int)]
privateUseAreas =
  [ (0xE000, 0xF8FF)     -- Private Use Area
  , (0xF0000, 0xFFFFD)   -- Supplementary Private Use Area-A
  , (0x100000, 0x10FFFD) -- Supplementary Private Use Area-B
  ]


unsafeUnicodeOffset :: Int -> Char -> Char
unsafeUnicodeOffset o c = chr (ord c + o)

-- | Offset that outputs only outside reserved ranges of Unicode.
unicodeOffset :: Int -> Char -> Maybe Char
unicodeOffset o c
  | isInvalidValidRange || isPrivateRange = Nothing
  | otherwise = Just . chr $ c'
  where c' = ord c + o
        isInvalidValidRange = c' < 0 || c' > 0x10FFFF
        isPrivateRange = 0xF0000 <= c' && c' <= 0xFFFFD
                      || 0x100000 <= c' && c' <= 0x10FFFD

showInvisibleChar :: IsString t => Char -> t
showInvisibleChar = fromString . showInvisibleChar'
  where
    control_chars = "␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟␠"
    showInvisibleChar' :: Char -> String
    showInvisibleChar' '\x007F' = "⌦"   -- del
    showInvisibleChar' '\x0020' = "␣"   -- normal space
    showInvisibleChar' '\x00A0' = "⍽"   -- no-break space
    -- showInvisibleChar' '\x00AD' = "⧚-⧛" -- soft hyphen
    showInvisibleChar' '\x00AD' = "-͓" -- soft hyphen
    showInvisibleChar' '\x2000' = "␣ ‹EN QUAD›"
    showInvisibleChar' '\x2001' = "␣ ‹EM QUAD›"
    showInvisibleChar' '\x2002' = "␣ ‹EN›"
    showInvisibleChar' '\x2003' = "␣ ‹EM›"
    showInvisibleChar' '\x2004' = "␣ ‹thick›"
    showInvisibleChar' '\x2005' = "␣ ‹mid›"
    showInvisibleChar' '\x2006' = "␣ ‹SIX-PER-EM›"
    showInvisibleChar' '\x2007' = "⍽ ‹123›"
    showInvisibleChar' '\x2008' = "␣ ‹punctuation›"
    showInvisibleChar' '\x2009' = "␣ ‹thin›"
    -- showInvisibleChar' '\x2011' = "⧘-⧙" -- no-break hyphen
    showInvisibleChar' '\x2011' = "-̺" -- no-break hyphen
    showInvisibleChar' '\x200A' = "␣ ‹hair›"
    showInvisibleChar' '\x200B' = "␣ ‹0›"
    showInvisibleChar' '\x200C' = "␣ ‹0 non joiner›"
    showInvisibleChar' '\x200D' = "␣ ‹0 joiner›"
    showInvisibleChar' '\x200E' = "␣ ‹→›"
    showInvisibleChar' '\x200F' = "␣ ‹←›"
    showInvisibleChar' '\x202F' = "⍽ ‹narrow›"
    showInvisibleChar' '\x205F' = "␣ ‹math›"
    showInvisibleChar' '\x2061' = "␣ ‹𝑓›"
    showInvisibleChar' '\x2062' = "␣ ‹×›"
    showInvisibleChar' '\x2063' = "␣ ‹list›"
    showInvisibleChar' '\x2064' = "␣ ‹+›"
    showInvisibleChar' '\xFEFF' = "‹BOM›"
    showInvisibleChar' c
      | c <= '\x20' = [control_chars !! ord c]
      | isMark c    = ['\x25CC', c] -- U+25CC: dotted circle "◌"
      | otherwise   = [c]

showCombiningChar :: Char -> String
showCombiningChar c
  | isMark c  = ['\'', '\x25CC', c, '\''] -- U+25CC: dotted circle "◌"
  | otherwise = show c

isShowableChar :: Char -> Bool
isShowableChar c = isAlphaNum c || isPunctuation c || isSymbol c
