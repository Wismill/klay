module Klay.Utils.UserInput
  ( -- * Types
    Raw(..)
  , RawText(..)
  , RawLText
    -- * Construction
  , fromChar
  , fromStrictText
  , fromShow
  , fromTShow
    -- * Common escape functions
  , escapeControlNewlines
  , escapeLine
  , escapeFilename
  , escapeFilename'
  , escapeSafeFilename
  , escapeSafeFilename'
    -- * Miscellaneous
  , rawTextToString
  ) where

--import Data.Char (GeneralCategory(..), ord, chr, isSpace, generalCategory)
import Data.Char (ord, chr, isSpace)
import Data.String (IsString(..))
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Hashable (Hashable)
import GHC.Generics (Generic)
import Control.DeepSeq (NFData)

import TextShow (TextShow(showtl))
import Test.QuickCheck.Arbitrary (Arbitrary(..))
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Utils (toJsonOptionsNewtype)


-- | A generic container to mark the data as raw user input.
newtype Raw a = Raw { getRaw :: a }
  deriving stock (Show, Functor, Generic)
  deriving newtype (Eq, NFData, Hashable)

instance (ToJSON a) => ToJSON (Raw a) where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance (FromJSON a) => FromJSON (Raw a) where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

type RawLText = RawText TL.Text

-- | A wrapper to mark the contained 'Data.Text.Lazy.Text' as raw user input.
newtype RawText t = RawText { getRawText :: t }
  deriving stock (Show, Functor, Generic)
  deriving newtype (Eq, Ord, Read, NFData, Hashable, Semigroup, Monoid, IsString)

instance ToJSON RawLText where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON RawLText where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

instance Arbitrary RawLText where
  arbitrary = RawText . TL.pack <$> arbitrary

rawTextToString :: RawLText -> String
rawTextToString = TL.unpack . getRawText

fromChar :: Char -> RawLText
fromChar = RawText . TL.singleton

fromStrictText :: T.Text -> RawLText
fromStrictText = RawText . TL.fromStrict

fromShow :: (Show a) => a -> RawLText
fromShow = RawText . fromString . show

fromTShow :: (TextShow a) => a -> RawLText
fromTShow = RawText . showtl

-- | Escape control and newlines characters
escapeControlNewlines :: RawLText -> TL.Text
escapeControlNewlines = TL.concatMap escape . getRawText
  where
    escape '\x007F' = "␡" -- U+007F Delete
    escape '\x0085' = "␤" -- U+0085 Next Line
    escape '\x2028' = "␤" -- U+2028 Line Separator
    escape '\x2029' = "␤" -- U+2029 Paragraph Separator
    escape c
      | c < '\x20' = TL.singleton $ chr (ord c + 0x2400) -- Replace control character by one of: ␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟
      | otherwise = TL.singleton c

-- [TODO] implement escapeIdentifier
-- escapeIdentifier  :: RawLText -> TL.Text
-- escapeIdentifier = undefined

-- | Escape control, newlines characters, ASCII quotes and reverse solidus.
escapeLine :: RawLText -> TL.Text
escapeLine = TL.concatMap escape . getRawText
  where
    escape '\x0022' = "_" -- U+0022 ASCII double quote
    escape '\x0027' = "’" -- U+0027 ASCII single quote
    escape '\x005C' = "_" -- U+005C Reverse solidus
    escape '\x007F' = "␡" -- U+007F Delete
    escape '\x0085' = "␤" -- U+0085 Next Line
    escape '\x2028' = "␤" -- U+2028 Line Separator
    escape '\x2029' = "␤" -- U+2029 Paragraph Separator
    escape c
      | c < '\x20' = TL.singleton $ chr (ord c + 0x2400) -- Replace control character by one of: ␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟
      | otherwise = TL.singleton c

-- | Escape a generic file name to make it a valid and readable on an OS supporting Unicode.
-- It replaces any newline or control character by their representation and /keeps/ spacing characters.
escapeFilename :: RawLText -> TL.Text
escapeFilename = TL.concatMap escape . getRawText
  where
    escape '\x0022' = "_" -- U+0022 ASCII double quote
    escape '\x0027' = "’" -- U+0027 ASCII single quote
    escape '\x002F' = "_" -- U+005C solidus
    escape '\x005C' = "_" -- U+005C Reverse solidus
    escape '\x007F' = "␡" -- U+007F Delete
    escape '\x0085' = "␤" -- U+0085 Next Line
    escape '\x2028' = "␤" -- U+2028 Line Separator
    escape '\x2029' = "␤" -- U+2029 Paragraph Separator
    escape c
      | c < '\x20' = TL.singleton $ chr (ord c + 0x2400) -- Replace control character by one of: ␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟
      | otherwise = TL.singleton c

-- | Same as 'escapeFilename' , but output a 'FilePath
escapeFilename' :: RawLText -> FilePath
escapeFilename' = TL.unpack . escapeControlNewlines

-- | Escape a generic file name to make it a valid and readable on an OS supporting Unicode.
-- It replaces any newline or control character by their representation and replaces spacing characters with underscores.
escapeSafeFilename :: RawLText -> TL.Text
escapeSafeFilename = TL.concatMap escape . getRawText
  where
    escape '\x0022' = "_" -- U+0022 ASCII double quote
    escape '\x0027' = "’" -- U+0027 ASCII single quote
    escape '\x005C' = "_" -- U+005C Reverse solidus
    escape '\x007F' = "␡" -- U+007F Delete
    escape '\x0085' = "␤" -- U+0085 Next Line
    escape '\x2028' = "␤" -- U+2028 Line Separator
    escape '\x2029' = "␤" -- U+2029 Paragraph Separator
    escape c
      | c < '\x20' = TL.singleton $ chr (ord c + 0x2400) -- Replace control character by one of: ␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟
      | isSpace c = "_"
      | otherwise = TL.singleton c

-- | Same as 'escapeSafeFilename', but outputs a 'FilePath'.
escapeSafeFilename' :: RawLText -> FilePath
escapeSafeFilename' = TL.unpack . escapeSafeFilename

-- isNewLine :: Char -> Bool
-- isNewLine c = let cat = generalCategory c in cat == LineSeparator || cat == ParagraphSeparator
