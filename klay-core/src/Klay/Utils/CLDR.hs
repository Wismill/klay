-- [NOTE] Auto-generated. Do not edit manually
{-# LANGUAGE OverloadedLists   #-}

module Klay.Utils.CLDR
  ( LanguageData(..)
  , Exemplars(..)
  , getExemplars
  , database
  ) where

import Data.Text (Text)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import GHC.Generics (Generic)

import Data.Aeson (ToJSON(..))
import Data.Aeson qualified as Aeson


-- See: https://unicode.org/reports/tr35/tr35-general.html#Exemplars
data LanguageData = LanguageData
  { language :: Text
  , script :: Maybe Text
  , territory :: Maybe Text
  , exemplars :: Maybe (Exemplars (Maybe (Set Char)))
  } deriving (Generic, Eq, Show)

data Exemplars a = Exemplars
  { _exMain :: a
  , _exAuxiliary :: a
  , _exNumbers :: a
  , _exPunctuation :: a
  } deriving (Generic, Eq, Functor, Show)

instance (Semigroup a) => Semigroup (Exemplars a) where
  (Exemplars m1 a1 n1 p1) <> (Exemplars m2 a2 n2 p2) =
    Exemplars (m1 <> m2) (a1 <> a2) (n1 <> n2) (p1 <> p2)

instance (Monoid a) => Monoid (Exemplars a) where
  mempty = Exemplars mempty mempty mempty mempty

instance (ToJSON a) => ToJSON (Exemplars a) where
    toEncoding = Aeson.genericToEncoding Aeson.defaultOptions

getExemplars :: Text -> Maybe (Exemplars (Maybe (Set Char)))
getExemplars lang = case Map.lookup lang database of
  Nothing -> Nothing
  Just entry1 -> case Map.lookup (language entry1) database of
    Nothing -> exemplars entry1
    Just entry2 ->
      let examplars1 = exemplars entry1
          examplars2 = exemplars entry2
      in examplars1 <> examplars2

database :: Map Text LanguageData
database =
  [ ("af", LanguageData
      { language = "af"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE1\xE2\xE8\xE9\
             \\xEA\xEB\xEE\xEF\xF4\xF6\xFB"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE3\xE4\xE5\xE6\xE7\xEC\xED\xF2\xF3\
             \\xF9\xFA\xFC\xFD"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("agq", LanguageData
      { language = "agq"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6B\
             \\x6C\x6D\x6E\x6F\x70\x73\x74\x75\x76\x77\
             \\x79\x7A\xE0\xE2\xE8\xEA\xEC\xEE\xF2\xF4\
             \\xF9\xFB\x101\x113\x11B\x12B\x14B\x14D\x16B\x1CE\
             \\x1D0\x1D2\x1D4\x254\x254\x300\x254\x302\x254\x304\x254\x30C\x25B\x25B\x300\
             \\x25B\x302\x25B\x304\x25B\x30C\x268\x268\x300\x268\x302\x268\x304\x268\x30C\x289\x289\x300\
             \\x289\x302\x289\x304\x289\x30C\x294"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x72\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ak", LanguageData
      { language = "ak"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6B\x6C\
             \\x6D\x6E\x6F\x70\x72\x73\x74\x75\x77\x79\
             \\x254\x25B"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x6A\x71\x76\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("am", LanguageData
      { language = "am"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1200\x1201\x1202\x1203\x1204\x1205\x1206\x1208\x1209\x120A\
             \\x120B\x120C\x120D\x120E\x120F\x1210\x1211\x1212\x1213\x1214\
             \\x1215\x1216\x1217\x1218\x1219\x121A\x121B\x121C\x121D\x121E\
             \\x121F\x1220\x1221\x1222\x1223\x1224\x1225\x1226\x1227\x1228\
             \\x1229\x122A\x122B\x122C\x122D\x122E\x122F\x1230\x1231\x1232\
             \\x1233\x1234\x1235\x1236\x1237\x1238\x1239\x123A\x123B\x123C\
             \\x123D\x123E\x123F\x1240\x1241\x1242\x1243\x1244\x1245\x1246\
             \\x1248\x124A\x124B\x124C\x124D\x1260\x1261\x1262\x1263\x1264\
             \\x1265\x1266\x1267\x1268\x1269\x126A\x126B\x126C\x126D\x126E\
             \\x126F\x1270\x1271\x1272\x1273\x1274\x1275\x1276\x1277\x1278\
             \\x1279\x127A\x127B\x127C\x127D\x127E\x127F\x1280\x1281\x1282\
             \\x1283\x1284\x1285\x1286\x1288\x128A\x128B\x128C\x128D\x1290\
             \\x1291\x1292\x1293\x1294\x1295\x1296\x1297\x1298\x1299\x129A\
             \\x129B\x129C\x129D\x129E\x129F\x12A0\x12A1\x12A2\x12A3\x12A4\
             \\x12A5\x12A6\x12A7\x12A8\x12A9\x12AA\x12AB\x12AC\x12AD\x12AE\
             \\x12B0\x12B2\x12B3\x12B4\x12B5\x12B8\x12B9\x12BA\x12BB\x12BC\
             \\x12BD\x12BE\x12C8\x12C9\x12CA\x12CB\x12CC\x12CD\x12CE\x12D0\
             \\x12D1\x12D2\x12D3\x12D4\x12D5\x12D6\x12D8\x12D9\x12DA\x12DB\
             \\x12DC\x12DD\x12DE\x12DF\x12E0\x12E1\x12E2\x12E3\x12E4\x12E5\
             \\x12E6\x12E7\x12E8\x12E9\x12EA\x12EB\x12EC\x12ED\x12EE\x12F0\
             \\x12F1\x12F2\x12F3\x12F4\x12F5\x12F6\x12F7\x1300\x1301\x1302\
             \\x1303\x1304\x1305\x1306\x1307\x1308\x1309\x130A\x130B\x130C\
             \\x130D\x130E\x1310\x1312\x1313\x1314\x1315\x1320\x1321\x1322\
             \\x1323\x1324\x1325\x1326\x1327\x1328\x1329\x132A\x132B\x132C\
             \\x132D\x132E\x132F\x1330\x1331\x1332\x1333\x1334\x1335\x1336\
             \\x1337\x1338\x1339\x133A\x133B\x133C\x133D\x133E\x133F\x1340\
             \\x1341\x1342\x1343\x1344\x1345\x1346\x1348\x1349\x134A\x134B\
             \\x134C\x134D\x134E\x134F\x1350\x1351\x1352\x1353\x1354\x1355\
             \\x1356\x1357"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2E\x3F\x5B\x5D\xAB\xBB\
             \\x1361\x1362\x1363\x1364\x1365\x1366\x2010\x2013\x2039\x203A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ar", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x623\x624\x625\x626\x627\x628\x629\x62A\
             \\x62B\x62C\x62D\x62E\x62F\x630\x631\x632\x633\x634\
             \\x635\x636\x637\x638\x639\x63A\x641\x642\x643\x644\
             \\x645\x646\x647\x648\x649\x64A\x64B\x64C\x64D\x64E\
             \\x64F\x650\x651\x652\x670"
          , _exAuxiliary = Just $ Set.fromList
             "\x640\x200C\x200D\x200E\x200F\x66F\x67E\x686\x698\x69C\x6A2\x6A4\x6A5\x6A7\
             \\x6A8\x6A9\x6AF\x6CC"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2D\x2E\x3A\x5B\x5D\
             \\xAB\xBB\x60C\x61B\x61F\x2010\x2011\x2013\x2014\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x660\x31\x661\x32\x662\x33\x663\x34\x664\
             \\x35\x665\x36\x666\x37\x667\x38\x668\x39\x669\x609\x61C\x200E\x66A\x66B\x66C\
             \\x2011\x2030"
          }
      }
    )
  , ("as", LanguageData
      { language = "as"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x981\x982\x983\x985\x986\x987\x988\x989\x98A\x98B\
             \\x98F\x990\x993\x994\x995\x995\x9CD\x9B7\x996\x997\x998\x999\
             \\x99A\x99B\x99C\x99D\x99E\x99F\x9A0\x9A1\x9A1\x9BC\x9A2\
             \\x9A2\x9BC\x9A3\x9A4\x9A5\x9A6\x9A7\x9A8\x9AA\x9AB\x9AC\
             \\x9AD\x9AE\x9AF\x9AF\x9BC\x9B2\x9B6\x9B7\x9B8\x9B9\x9BC\
             \\x9BE\x9BF\x9C0\x9C1\x9C2\x9C3\x9C7\x9C8\x9CB\x9CC\
             \\x9CD\x9F0\x9F1"
          , _exAuxiliary = Just $ Set.fromList
             "\x9B0\x9CE\x9F2\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x964\
             \\x2010\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\
             \\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x9E6\x31\x9E7\x32\x9E8\x33\x9E9\x34\x9EA\
             \\x35\x9EB\x36\x9EC\x37\x9ED\x38\x9EE\x39\x9EF\x2011\x2030"
          }
      }
    )
  , ("asa", LanguageData
      { language = "asa"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ast", LanguageData
      { language = "ast"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6C\
             \\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\x76\
             \\x78\x79\x7A\xE1\xE9\xED\xF1\xF3\xFA\xFC\
             \\x1E25\x1E37"
          , _exAuxiliary = Just $ Set.fromList
             "\x6A\x6B\x77\xAA\xBA\xE0\xE2\xE3\xE4\xE5\
             \\xE6\xE7\xE8\xEA\xEB\xEC\xEE\xEF\xF2\xF4\
             \\xF6\xF8\xF9\xFB\xFF\x101\x103\x113\x115\x12B\
             \\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\xA1\
             \\xA7\xAB\xBB\xBF\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("az", LanguageData
      { language = "az"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x78\x79\x7A\xE7\xF6\xFC\x11F\x130\
             \\x131\x15F\x259"
          , _exAuxiliary = Just $ Set.fromList
             "\x77"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("bas", LanguageData
      { language = "bas"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x61\x1DC6\x61\x1DC7\x62\x63\x64\x65\x65\x1DC6\x65\x1DC7\x66\x67\x68\
             \\x69\x69\x1DC6\x69\x1DC7\x6A\x6B\x6C\x6D\x6E\x6F\x6F\x1DC6\x6F\x1DC7\x70\
             \\x72\x73\x74\x75\x75\x1DC6\x75\x1DC7\x76\x77\x79\x7A\xE0\
             \\xE1\xE2\xE8\xE9\xEA\xEC\xED\xEE\xF2\xF3\
             \\xF4\xF9\xFA\xFB\x101\x113\x11B\x12B\x144\x14B\
             \\x14D\x16B\x1CE\x1D0\x1D2\x1D4\x1F9\x253\x254\x254\x300\
             \\x254\x301\x254\x302\x254\x304\x254\x30C\x254\x1DC6\x254\x1DC7\x25B\x25B\x300\x25B\x301\x25B\x302\x25B\x304\
             \\x25B\x30C\x25B\x1DC6\x25B\x1DC7"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("be", LanguageData
      { language = "be"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x434\x436\x434\x437\x435\x436\x437\
             \\x439\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\
             \\x443\x444\x445\x446\x447\x448\x44B\x44C\x44D\x44E\
             \\x44F\x451\x456\x45E"
          , _exAuxiliary = Just $ Set.fromList
             "\x430\x301\x435\x301\x43E\x301\x443\x301\x44B\x301\x44D\x301\x44E\x301\x44F\x301\x451\x301\x456\x301"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\xAB\xBB\x2011"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("bem", LanguageData
      { language = "bem"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x65\x66\x67\x69\x6A\x6B\x6C\
             \\x6D\x6E\x6F\x70\x73\x73\x68\x74\x75\x77\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x64\x68\x71\x72\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("bez", LanguageData
      { language = "bez"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("bg", LanguageData
      { language = "bg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x446\x447\x448\x449\x44A\x44C\x44E\x44F"
          , _exAuxiliary = Just $ Set.fromList
             "\x430\x300\x43E\x300\x443\x300\x44A\x300\x44B\x44D\x44E\x300\x44F\x300\x450\x451\
             \\x45D\x463\x46B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\x2011\x2013\
             \\x2014\x2018\x201A\x201C\x201E\x2026\x2033\x2116"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("bm", LanguageData
      { language = "bm"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x77\x79\x7A\x14B\x254\x25B\x272"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x76\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("bn", LanguageData
      { language = "bn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x981\x982\x983\x985\x986\x987\x988\x989\x98A\x98B\
             \\x98C\x98F\x990\x993\x994\x995\x995\x9CD\x9B7\x996\x997\x998\
             \\x999\x99A\x99B\x99C\x99D\x99E\x99F\x9A0\x9A1\x9A1\x9BC\
             \\x9A2\x9A2\x9BC\x9A3\x9A4\x9A5\x9A6\x9A7\x9A8\x9AA\x9AB\
             \\x9AC\x9AD\x9AE\x9AF\x9AF\x9BC\x9B0\x9B2\x9B6\x9B7\x9B8\
             \\x9B9\x9BC\x9BD\x9BE\x9BF\x9C0\x9C1\x9C2\x9C3\x9C4\
             \\x9C7\x9C8\x9CB\x9CC\x9CD\x9CE\x9D7\x9E0\x9E1\x9E2\
             \\x9E3\x9FA"
          , _exAuxiliary = Just $ Set.fromList
             "\x9F0\x9F1\x9F2\x9F3\x9F4\x9F5\x9F6\x9F7\x9F8\x9F9\
             \\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x9E6\x31\x9E7\x32\x9E8\x33\x9E9\x34\x9EA\
             \\x35\x9EB\x36\x9EC\x37\x9ED\x38\x9EE\x39\x9EF\x2011\x2030"
          }
      }
    )
  , ("bo", LanguageData
      { language = "bo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xF40\xF40\xFB5\xF41\xF42\xF42\xFB7\xF44\xF45\xF46\xF47\xF49\
             \\xF4A\xF4B\xF4C\xF4C\xFB7\xF4E\xF4F\xF50\xF51\xF51\xFB7\xF53\
             \\xF54\xF55\xF56\xF56\xFB7\xF58\xF59\xF5A\xF5B\xF5B\xFB7\xF5D\
             \\xF5E\xF5F\xF60\xF61\xF62\xF63\xF64\xF65\xF66\xF67\
             \\xF68\xF6A\xF71\xF72\xF71\xF74\xF71\xF80\xF72\xF74\xF77\xF79\xF7A\
             \\xF7B\xF7C\xF7D\xF7E\xF7F\xF80\xF84\xF90\xF90\xFB5\xF91\
             \\xF92\xF92\xFB7\xF94\xF95\xF96\xF97\xF99\xF9A\xF9B\xF9C\
             \\xF9C\xFB7\xF9E\xF9F\xFA0\xFA1\xFA1\xFB7\xFA3\xFA4\xFA5\xFA6\
             \\xFA6\xFB7\xFA8\xFA9\xFAA\xFAB\xFAB\xFB7\xFAD\xFAE\xFAF\xFB0\
             \\xFB1\xFB2\xFB2\xF80\xFB3\xFB3\xF80\xFB4\xFB5\xFB6\xFB7\xFB8\
             \\xFBA\xFBB\xFBC"
          , _exAuxiliary = Just $ Set.fromList
             "\xF00"
          , _exPunctuation = Just $ Set.fromList
             "\x3A\xF0B\xF0D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xF20\x31\xF21\x32\xF22\x33\xF23\x34\xF24\
             \\x35\xF25\x36\xF26\x37\xF27\x38\xF28\x39\xF29\x2011\x2030"
          }
      }
    )
  , ("br", LanguageData
      { language = "br"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x68\x63\x2BC\x68\x64\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xEA\xF1\xF9"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\
             \\xE8\xE9\xEB\xEC\xED\xEE\xEF\xF2\xF3\xF4\
             \\xF6\xF8\xFA\xFB\xFC\xFF\x101\x103\x113\x115\
             \\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2011"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("brx", LanguageData
      { language = "brx"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x905\x906\x907\x908\x909\x90A\x90D\x90F\
             \\x910\x911\x913\x914\x915\x916\x917\x918\x91A\x91B\
             \\x91C\x91D\x91E\x91F\x920\x921\x921\x93C\x922\x923\x924\
             \\x925\x926\x927\x928\x92A\x92B\x92C\x92D\x92E\x92F\
             \\x930\x932\x933\x935\x936\x937\x938\x939\x93C\x93E\
             \\x93F\x940\x941\x942\x943\x945\x947\x948\x949\x94B\
             \\x94C\x94D"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("bs", LanguageData
      { language = "bs"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x64\x17E\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6C\x6A\x6D\x6E\x6E\x6A\x6F\x70\x72\
             \\x73\x74\x75\x76\x7A\x107\x10D\x111\x161\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2E\x2F\x3A\
             \\x3B\x3F\x40\x5B\x5D\x2010\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ca", LanguageData
      { language = "ca"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xB7\xE0\xE7\xE8\
             \\xE9\xED\xEF\xF2\xF3\xFA\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xBA\xE1\xE2\xE3\xE4\xE5\xE6\xEA\xEB\xEC\
             \\xEE\xF1\xF4\xF6\xF8\xF9\xFB\xFF\x101\x103\
             \\x113\x115\x12B\x12D\x140\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\xA1\
             \\xA7\xAB\xBB\xBF\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ccp", LanguageData
      { language = "ccp"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x11100\x11101\x11102\x11103\x11104\x11105\x11106\x11107\x11108\x11109\
             \\x1110A\x1110B\x1110C\x1110D\x1110E\x1110F\x11110\x11111\x11112\x11113\
             \\x11114\x11115\x11116\x11117\x11118\x11119\x1111A\x1111B\x1111C\x1111D\
             \\x1111E\x1111F\x11120\x11121\x11122\x11123\x11124\x11125\x11126\x11127\
             \\x11128\x11129\x1112A\x1112B\x1112C\x1112D\x1112E\x1112F\x11130\x11131\
             \\x11132\x11133\x11134"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033\x11140\x11141\x11142\x11143"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x2011\x2030\x11136\x11137\x11138\
             \\x11139\x1113A\x1113B\x1113C\x1113D\x1113E\x1113F"
          }
      }
    )
  , ("ce", LanguageData
      { language = "ce"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x430\x44C\x431\x432\x433\x433\x4CF\x434\x435\x436\x437\
             \\x438\x438\x439\x439\x43A\x43A\x43A\x43A\x43A\x445\x43A\x445\x43A\x44C\x43A\x4CF\x43B\
             \\x43C\x43D\x43E\x43E\x432\x43E\x44C\x43F\x43F\x43F\x43F\x4CF\x440\x440\x445\x4CF\
             \\x441\x441\x441\x442\x442\x442\x442\x4CF\x443\x443\x432\x443\x44C\x443\x44C\x439\x444\
             \\x445\x445\x44C\x445\x4CF\x446\x446\x4CF\x447\x447\x4CF\x448\x449\x44A\
             \\x44B\x44C\x44D\x44E\x44E\x44C\x44F\x44F\x44C\x451"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ceb", LanguageData
      { language = "ceb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x67\x68\x69\x6B\x6C\x6D\
             \\x6E\x6F\x70\x72\x73\x74\x75\x77\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x66\x6A\x71\x76\x78\x7A\xF1"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x2011\x2018\
             \\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("cgg", LanguageData
      { language = "cgg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("chr", LanguageData
      { language = "chr"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x13F8\x13F9\x13FA\x13FB\x13FC\xAB70\xAB71\xAB72\xAB73\xAB74\
             \\xAB75\xAB76\xAB77\xAB78\xAB79\xAB7A\xAB7B\xAB7C\xAB7D\xAB7E\
             \\xAB7F\xAB80\xAB81\xAB82\xAB83\xAB84\xAB85\xAB86\xAB87\xAB88\
             \\xAB89\xAB8A\xAB8B\xAB8C\xAB8D\xAB8E\xAB8F\xAB90\xAB91\xAB92\
             \\xAB93\xAB94\xAB95\xAB96\xAB97\xAB98\xAB99\xAB9A\xAB9B\xAB9C\
             \\xAB9D\xAB9E\xAB9F\xABA0\xABA1\xABA2\xABA3\xABA4\xABA5\xABA6\
             \\xABA7\xABA8\xABA9\xABAA\xABAB\xABAC\xABAD\xABAE\xABAF\xABB0\
             \\xABB1\xABB2\xABB3\xABB4\xABB5\xABB6\xABB7\xABB8\xABB9\xABBA\
             \\xABBB\xABBC\xABBD\xABBE\xABBF"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ckb", LanguageData
      { language = "ckb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x626\x627\x628\x62A\x62C\x62D\x62E\x62F\x631\x632\
             \\x633\x634\x639\x63A\x641\x642\x644\x645\x646\x648\
             \\x67E\x686\x695\x698\x6A4\x6A9\x6AF\x6B5\x6BE\x6C6\
             \\x6CC\x6CE\x6D5"
          , _exAuxiliary = Just $ Set.fromList
             "\x621\x622\x623\x624\x625\x629\x62B\x630\x635\x636\
             \\x637\x638\x643\x647\x649\x64A\x64B\x64C\x64D\x64E\
             \\x64F\x650\x651\x652\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x660\x31\x661\x32\x662\x33\x663\x34\x664\
             \\x35\x665\x36\x666\x37\x667\x38\x668\x39\x669\x609\x66A\x66B\x66C\x200E\x200F\
             \\x2011\x2030"
          }
      }
    )
  , ("cs", LanguageData
      { language = "cs"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x68\x64\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\
             \\x74\x75\x76\x77\x78\x79\x7A\xE1\xE9\xED\
             \\xF3\xFA\xFD\x10D\x10F\x11B\x148\x159\x161\x165\
             \\x16F\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xEA\xEB\
             \\xEC\xEE\xEF\xF1\xF2\xF4\xF6\xF8\xF9\xFB\
             \\xFC\xFF\x101\x103\x113\x115\x12B\x12D\x13E\x142\
             \\x14D\x14F\x153\x155\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x26\x28\x29\x2A\x2C\x2D\x2E\x2F\x3A\
             \\x3B\x3F\x40\x5B\x5D\xA7\x2010\x2011\x2013\x2018\
             \\x201A\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("cy", LanguageData
      { language = "cy"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x68\x64\x64\x64\x65\x66\x66\x66\x67\
             \\x68\x69\x6A\x6C\x6C\x6C\x6D\x6E\x6E\x67\x6F\x70\
             \\x70\x68\x72\x72\x68\x73\x74\x74\x68\x75\x77\x79\xE0\
             \\xE1\xE2\xE4\xE8\xE9\xEA\xEB\xEC\xED\xEE\
             \\xEF\xF2\xF3\xF4\xF6\xF9\xFA\xFB\xFC\xFD\
             \\xFF\x175\x177\x1E81\x1E83\x1E85\x1EF3"
          , _exAuxiliary = Just $ Set.fromList
             "\x6B\x71\x76\x78\x7A\xE3\xE5\xE6\xE7\xF1\
             \\xF8\x101\x103\x113\x115\x12B\x12D\x14D\x14F\x153\
             \\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("da", LanguageData
      { language = "da"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE5\xE6\xF8"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE4\xE7\xE8\xE9\xEA\xEB\xED\
             \\xEE\xEF\xF1\xF3\xF4\xF6\xF9\xFA\xFB\xFC\
             \\xFF\x153\x1FF"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2018\x2019\x201C\x201D\x2020\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("dav", LanguageData
      { language = "dav"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("de", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xDF\xE4\xF6\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE5\xE6\xE7\xE8\xE9\xEA\
             \\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF8\
             \\xF9\xFA\xFB\xFF\x101\x103\x113\x115\x11F\x12B\
             \\x12D\x130\x131\x14D\x14F\x153\x15F\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("dje", LanguageData
      { language = "dje"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x77\x78\x79\x7A\xE3\xF5\x14B\x161\x17E\
             \\x272\x1EBD"
          , _exAuxiliary = Just $ Set.fromList
             "\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("doi", LanguageData
      { language = "doi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x903\x905\x906\x907\x908\x909\x90A\x90B\
             \\x90C\x90F\x910\x913\x914\x915\x915\x94D\x937\x916\x917\x918\
             \\x919\x91A\x91B\x91C\x91D\x91E\x91F\x920\x921\x921\x93C\
             \\x922\x922\x93C\x923\x924\x925\x926\x927\x928\x92A\x92B\
             \\x92C\x92D\x92E\x92F\x930\x932\x933\x935\x936\x937\
             \\x938\x939\x93C\x93D\x93E\x93F\x940\x941\x942\x943\
             \\x944\x947\x948\x94B\x94C\x94D\x950\x951\x952\x960\x961\
             \\x962\x963"
          , _exAuxiliary = Just $ Set.fromList
             "\x90D\x911\x945\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2E\
             \\x2F\x3A\x3B\x3F\x40\x5F\xA7\x2013\x2014\x2018\
             \\x2019\x201C\x201D\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("dsb", LanguageData
      { language = "dsb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x68\x64\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\
             \\x74\x75\x76\x77\x78\x79\x7A\xF3\x107\x10D\
             \\x11B\x142\x144\x155\x15B\x161\x17A\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\
             \\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF4\
             \\xF6\xF8\xF9\xFA\xFB\xFC\xFD\xFF\x101\x103\
             \\x105\x10F\x111\x113\x115\x117\x119\x11F\x12B\x12D\
             \\x130\x131\x13A\x13E\x148\x14D\x14F\x151\x153\x159\
             \\x15F\x165\x16B\x16D\x16F\x171\x17C"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x2019\x201A\
             \\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("dua", LanguageData
      { language = "dua"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6E\x79\x6F\x70\x72\x73\x74\x75\
             \\x77\x79\xE1\xE9\xED\xF3\xFA\x14B\x16B\x253\
             \\x254\x254\x301\x257\x25B\x25B\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x68\x71\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("dyo", LanguageData
      { language = "dyo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\xE1\xE9\xED\xF1\xF3\
             \\xFA\x14B"
          , _exAuxiliary = Just $ Set.fromList
             "\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("dz", LanguageData
      { language = "dz"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xF40\xF41\xF42\xF44\xF45\xF46\xF47\xF49\xF4F\xF50\
             \\xF51\xF53\xF54\xF55\xF56\xF58\xF59\xF5A\xF5B\xF5D\
             \\xF5E\xF5F\xF60\xF61\xF62\xF63\xF64\xF66\xF67\xF68\
             \\xF72\xF74\xF7A\xF7C\xF90\xF91\xF92\xF94\xF97\xF99\
             \\xF9F\xFA0\xFA1\xFA3\xFA4\xFA5\xFA6\xFA8\xFA9\xFAA\
             \\xFAB\xFAD\xFB1\xFB2\xFB3\xFB5\xFB6\xFB7"
          , _exAuxiliary = Just $ Set.fromList
             "\xF4A\xF4B\xF4C\xF4E\xF65\xF7B\xF7D\xF7E\xF80\xF84\
             \\xF9A\xF9B\xF9C\xF9E\xFBA\xFBB\xFBC"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\xF04\
             \\xF05\xF06\xF08\xF09\xF0A\xF0C\xF0D\xF0E\xF0F\xF10\
             \\xF11\xF12\xF14\xF34\xF36\xF3C\xF3D\xFBE\xFBF\xFD0\
             \\xFD1\xFD2\xFD3\xFD4\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xF20\x31\xF21\x32\xF22\x33\xF23\x34\xF24\
             \\x35\xF25\x36\xF26\x37\xF27\x38\xF28\x39\xF29\x2011\x2030"
          }
      }
    )
  , ("ebu", LanguageData
      { language = "ebu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\x129\x169"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ee", LanguageData
      { language = "ee"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6B\x6C\
             \\x6D\x6E\x6F\x70\x72\x73\x74\x75\x76\x77\
             \\x78\x79\x7A\xE0\xE1\xE3\xE8\xE9\xEC\xED\
             \\xF2\xF3\xF5\xF9\xFA\x129\x14B\x169\x192\x254\
             \\x254\x300\x254\x301\x254\x303\x256\x25B\x25B\x300\x25B\x301\x25B\x303\x263\x28B\
             \\x1EBD"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x6A\x71\xE2\xE4\xE5\xE6\xE7\xEA\xEB\
             \\xEE\xEF\xF1\xF4\xF6\xF8\xFB\xFC\xFF\x101\
             \\x103\x115\x12D\x14F\x153\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\x2010\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\
             \\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("el", LanguageData
      { language = "el"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x390\x3AC\x3AD\x3AE\x3AF\x3B0\x3B1\x3B2\x3B3\x3B4\
             \\x3B5\x3B6\x3B7\x3B8\x3B9\x3BA\x3BB\x3BC\x3BD\x3BE\
             \\x3BF\x3C0\x3C1\x3C2\x3C3\x3C4\x3C5\x3C6\x3C7\x3C8\
             \\x3C9\x3CA\x3CB\x3CC\x3CD\x3CE"
          , _exAuxiliary = Just $ Set.fromList
             "\x1F00\x1F01\x1F02\x1F03\x1F04\x1F05\x1F06\x1F07\x1F10\x1F11\
             \\x1F12\x1F13\x1F14\x1F15\x1F20\x1F21\x1F22\x1F23\x1F24\x1F25\
             \\x1F26\x1F27\x1F30\x1F31\x1F32\x1F33\x1F34\x1F35\x1F36\x1F37\
             \\x1F42\x1F43\x1F44\x1F50\x1F51\x1F52\x1F53\x1F54\x1F55\x1F56\
             \\x1F57\x1F62\x1F63\x1F64\x1F65\x1F66\x1F67\x1F70\x1F72\x1F74\
             \\x1F76\x1F78\x1F7A\x1F7C\x1FB6\x1FC6\x1FD2\x1FD6\x1FD7\x1FE2\
             \\x1FE6\x1FE7\x1FF6"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x26\x28\x29\x2A\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x40\x5B\x5C\x5D\xA7\xAB\xBB\x2010\
             \\x2011\x2013\x2014\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("en", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xE9\
             \\xEA\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\
             \\xF6\xF8\xF9\xFA\xFB\xFC\xFF\x101\x103\x113\
             \\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("eo", LanguageData
      { language = "eo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x7A\x109\x11D\x125\x135\x15D\x16D"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x2F\x3A\
             \\x3B\x3F\x5B\x5D\x7B\x7D\x2010\x2011\x2013\x2014\
             \\x2018\x2019\x201C\x201D\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("es", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE1\xE9\xED\xF1\
             \\xF3\xFA\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xAA\xBA\xE0\xE2\xE3\xE4\xE5\xE6\xE7\xE8\
             \\xEA\xEB\xEC\xEE\xEF\xF2\xF4\xF6\xF8\xF9\
             \\xFB\xFD\xFF\x101\x103\x113\x115\x12B\x12D\x14D\
             \\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\xA1\
             \\xA7\xAB\xBB\xBF\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("et", LanguageData
      { language = "et"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE4\xF5\xF6\xFC\
             \\x161\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE5\xE6\xE7\xE8\xE9\xEA\
             \\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF8\
             \\xF9\xFA\xFB\x101\x113\x12B\x14D\x14F\x153\x16B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x40\
             \\x5B\x5D\x7B\x7D\x2011\x2013\x201C\x201E"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("eu", LanguageData
      { language = "eu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE7\xF1"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE8\xE9\xEA\
             \\xEB\xEC\xED\xEE\xEF\xF2\xF3\xF4\xF6\xF8\
             \\xF9\xFA\xFB\xFC\xFF\x101\x103\x113\x115\x12B\
             \\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2030\x2212"
          }
      }
    )
  , ("ewo", LanguageData
      { language = "ewo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x64\x7A\x65\x66\x67\x68\x69\x6B\
             \\x6B\x70\x6C\x6D\x6E\x6E\x67\x6E\x6B\x6F\x70\x72\x73\
             \\x74\x74\x73\x75\x76\x77\x79\x7A\xE0\xE1\xE2\
             \\xE8\xE9\xEA\xEC\xED\xEE\xF2\xF3\xF4\xF9\
             \\xFA\xFB\x11B\x144\x14B\x1CE\x1D0\x1D2\x1D4\x1F9\
             \\x254\x254\x300\x254\x301\x254\x302\x254\x30C\x259\x259\x300\x259\x301\x259\x302\x259\x30C\
             \\x25B\x25B\x300\x25B\x301\x25B\x302\x25B\x30C"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x6A\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("fa", LanguageData
      { language = "fa"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x623\x624\x626\x627\x628\x629\x62A\x62B\
             \\x62C\x62D\x62E\x62F\x630\x631\x632\x633\x634\x635\
             \\x636\x637\x638\x639\x63A\x641\x642\x644\x645\x646\
             \\x647\x648\x64B\x64C\x64D\x651\x654\x67E\x686\x698\
             \\x6A9\x6AF\x6CC"
          , _exAuxiliary = Just $ Set.fromList
             "\x625\x640\x200C\x200D\x200E\x200F\x643\x649\x64A\x64E\x64F\x650\x652\x656\
             \\x670"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2A\x2D\x2E\x2F\x3A\x5B\x5D\
             \\x5D\xAB\xBB\x60C\x61B\x61F\x66B\x66C\x2010\x2011\
             \\x2026\x2039\x203A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2E\x30\x6F0\x31\x6F1\x32\x6F2\x33\x6F3\x34\x6F4\x35\x6F5\
             \\x36\x6F6\x37\x6F7\x38\x6F8\x39\x6F9\x609\x66A\x66B\x66C\x200E\x2030\
             \\x2212"
          }
      }
    )
  , ("ff", LanguageData
      { language = "ff"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x77\x79\xF1\x14B\x1B4\x253\x257"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("fi", LanguageData
      { language = "fi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE4\xE5\xF6\x161\
             \\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE0\xE1\xE2\xE3\xE6\xE7\xE8\xE9\xEA\
             \\xEB\xED\xEE\xEF\xF0\xF1\xF2\xF3\xF4\xF5\
             \\xF8\xF9\xFA\xFB\xFC\xFD\xFE\xFF\x101\x103\
             \\x105\x107\x10B\x10D\x10F\x111\x113\x117\x119\x11B\
             \\x11F\x123\x127\x12B\x12F\x130\x131\x137\x13A\x13C\
             \\x13E\x142\x144\x146\x148\x14B\x151\x153\x155\x159\
             \\x15B\x15D\x15F\x163\x165\x167\x16B\x16F\x171\x173\
             \\x17A\x17C\x1E5\x1E7\x1E9\x1EF\x219\x21B\x21F\x292"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x23\x26\x28\x29\x2A\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x3F\x40\x5B\x5C\x5D\xA7\xBB\x2010\
             \\x2011\x2013\x2019\x201D\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\x202F\x2030\x2212"
          }
      }
    )
  , ("fil", LanguageData
      { language = "fil"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6E\x67\x6F\x70\x71\x72\x73\
             \\x74\x75\x76\x77\x78\x79\x7A\xF1"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE8\xE9\xEA\xEC\xED\xEE\xF2\
             \\xF3\xF4\xF9\xFA\xFB"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x5B\x5D\xA7\x2010\x2011\
             \\x2013\x2014\x2018\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("fo", LanguageData
      { language = "fo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x76\
             \\x79\xE1\xE6\xED\xF0\xF3\xF8\xFA\xFD"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x77\x78\x7A"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2018\x2019\x201C\x201D\x2020\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2030\x2212"
          }
      }
    )
  , ("fr", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE2\xE6\xE7\
             \\xE8\xE9\xEA\xEB\xEE\xEF\xF4\xF9\xFB\xFC\
             \\xFF\x153"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE1\xE3\xE4\xE5\xEC\xED\xF1\xF2\xF3\
             \\xF5\xF6\xF8\xFA\x101\x107\x113\x12B\x133\x159\
             \\x161\x17F\x1D4"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x28\x29\x2A\x2C\x2D\x2E\
             \\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\xAB\xBB\
             \\x2010\x2011\x2013\x2014\x2019\x201C\x201D\x2020\x2021\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\xB2\xB3\x2B3\x2E2\x1D48\
             \\x1D49\x2011\x202F\x2030\x2212"
          }
      }
    )
  , ("fur", LanguageData
      { language = "fur"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE2\xE7\xE8\
             \\xEA\xEC\xEE\xF2\xF4\xF9\xFB"
          , _exAuxiliary = Just $ Set.fromList
             "\xE5\xE9\xEB\xEF\xF1\xF3\xFC\x10D\x11F\x161"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("fy", LanguageData
      { language = "fy"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x69\x6A\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\
             \\x75\x76\x77\x79\x7A\xE0\xE1\xE2\xE4\xE8\
             \\xE9\xEA\xEB\xED\xED\x6A\x301\xEF\xF3\xF4\xF6\xFA\
             \\xFB\xFC\xFD"
          , _exAuxiliary = Just $ Set.fromList
             "\xE6\xF2\xF9"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ga", LanguageData
      { language = "ga"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6C\
             \\x6D\x6E\x6F\x70\x72\x73\x74\x75\xE1\xE9\
             \\xED\xF3\xFA"
          , _exAuxiliary = Just $ Set.fromList
             "\x6A\x6B\x71\x76\x77\x78\x79\x7A\xE5\x10B\
             \\x121\x1E03\x1E0B\x1E1F\x1E41\x1E57\x1E61\x1E6B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("gd", LanguageData
      { language = "gd"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6C\
             \\x6D\x6E\x6F\x70\x72\x73\x74\x75\xE0\xE8\
             \\xEC\xF2\xF9"
          , _exAuxiliary = Just $ Set.fromList
             "\x6A\x6B\x71\x76\x77\x78\x79\x7A\xE1\xE2\
             \\xE3\xE4\xE5\xE6\xE7\xE9\xEA\xEB\xED\xEE\
             \\xEF\xF1\xF3\xF4\xF6\xF8\xFA\xFB\xFC\xFF\
             \\x101\x103\x10B\x113\x115\x121\x12B\x12D\x131\x142\
             \\x14D\x14F\x153\x15F\x16B\x16D\x219\x1E0B\x1E1F\x1E41\
             \\x1E57\x1E61\x1E6B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x27\x28\x29\x2A\x2C\
             \\x2D\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\
             \\x7D\xA1\xA7\xA9\xAE\xB0\xB6\xB7\x2010\x2011\
             \\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\x2027\
             \\x204A\x2122"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("gl", LanguageData
      { language = "gl"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE1\xE9\xED\xEF\
             \\xF1\xF3\xFA\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xAA\xBA\xE0\xE2\xE3\xE4\xE5\xE6\xE7\xE8\
             \\xEA\xEB\xEC\xEE\xF2\xF4\xF5\xF6\xF8\xF9\
             \\xFB\x101\x103\x113\x115\x12B\x12D\x14D\x14F\x153\
             \\x16B\x16D\x251"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\xA1\
             \\xA7\xAB\xBB\xBF\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("gsw", LanguageData
      { language = "gsw"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE4\xF6\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE5\xE6\xE7\xE8\xE9\xEA\xEB\
             \\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF8\xF9\
             \\xFA\xFB\xFF\x101\x103\x113\x115\x12B\x12D\x14D\
             \\x14F\x153\x16B\x16D"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2E\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\x2019\x2030\x2212"
          }
      }
    )
  , ("gu", LanguageData
      { language = "gu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xA81\xA82\xA83\xA85\xA86\xA87\xA88\xA89\xA8A\xA8B\
             \\xA8D\xA8F\xA90\xA91\xA93\xA94\xA95\xA96\xA97\xA98\
             \\xA99\xA9A\xA9B\xA9C\xA9D\xA9E\xA9F\xAA0\xAA1\xAA2\
             \\xAA3\xAA4\xAA5\xAA6\xAA7\xAA8\xAAA\xAAB\xAAC\xAAD\
             \\xAAE\xAAF\xAB0\xAB2\xAB3\xAB5\xAB6\xAB7\xAB8\xAB9\
             \\xABC\xABD\xABE\xABF\xAC0\xAC1\xAC2\xAC3\xAC4\xAC5\
             \\xAC7\xAC8\xAC9\xACB\xACC\xACD\xAD0\xAE0"
          , _exAuxiliary = Just $ Set.fromList
             "\xAF0\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xAE6\x31\xAE7\x32\xAE8\x33\xAE9\x34\xAEA\
             \\x35\xAEB\x36\xAEC\x37\xAED\x38\xAEE\x39\xAEF\x2011\x2030"
          }
      }
    )
  , ("guz", LanguageData
      { language = "guz"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("gv", LanguageData
      { language = "gv"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE7"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("ha", LanguageData
      { language = "ha"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x72\x73\x73\x68\x74\x74\x73\
             \\x75\x77\x79\x7A\x199\x1B4\x253\x257\x2BC"
          , _exAuxiliary = Just $ Set.fromList
             "\x70\x71\x72\x303\x76\x78\xE0\xE1\xE2\xE8\xE9\
             \\xEA\xEC\xED\xEE\xF2\xF3\xF4\xF9\xFA\xFB"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x3A\x3B\
             \\x3F\x5B\x5D\x7B\x7D\x2011\x2018\x2019\x201C\x201D\
             \\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("haw", LanguageData
      { language = "haw"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x65\x68\x69\x6B\x6C\x6D\x6E\x6F\x70\
             \\x75\x77\x101\x113\x12B\x14D\x16B\x2BB"
          , _exAuxiliary = Just $ Set.fromList
             "\x62\x63\x64\x66\x67\x6A\x71\x72\x73\x74\
             \\x76\x78\x79\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("he", LanguageData
      { language = "he"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x5D0\x5D1\x5D2\x5D3\x5D4\x5D5\x5D6\x5D7\x5D8\x5D9\
             \\x5DA\x5DB\x5DC\x5DD\x5DE\x5DF\x5E0\x5E1\x5E2\x5E3\
             \\x5E4\x5E5\x5E6\x5E7\x5E8\x5E9\x5EA"
          , _exAuxiliary = Just $ Set.fromList
             "\x5B0\x5B1\x5B2\x5B3\x5B4\x5B5\x5B6\x5B7\x5B8\x5B9\
             \\x5BB\x5BC\x5BD\x5C4\x200E\x200F\x5BF\x5C1\x5C2\x5F4"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x2F\x3A\
             \\x3B\x3F\x5B\x5D\x5BE\x5F3\x5F4\x2010\x2011\x2013\
             \\x2014"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("hi", LanguageData
      { language = "hi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x903\x905\x906\x907\x908\x909\x90A\x90B\
             \\x90C\x90D\x90F\x910\x911\x913\x914\x915\x916\x917\
             \\x918\x919\x91A\x91B\x91C\x91D\x91E\x91F\x920\x921\
             \\x922\x923\x924\x925\x926\x927\x928\x92A\x92B\x92C\
             \\x92D\x92E\x92F\x930\x932\x933\x935\x936\x937\x938\
             \\x939\x93C\x93D\x93E\x93F\x940\x941\x942\x943\x944\
             \\x945\x947\x948\x949\x94B\x94C\x94D\x950"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x970\x2011\x2018\x2019\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x966\x31\x967\x32\x968\x33\x969\x34\x96A\
             \\x35\x96B\x36\x96C\x37\x96D\x38\x96E\x39\x96F\x2011\x2030"
          }
      }
    )
  , ("hr", LanguageData
      { language = "hr"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x64\x17E\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6C\x6A\x6D\x6E\x6E\x6A\x6F\x70\x72\
             \\x73\x74\x75\x76\x7A\x107\x10D\x111\x161\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2E\x2F\x3A\
             \\x3B\x3F\x40\x5B\x5D\x2010\x2013\x2014\x2018\x2019\
             \\x201A\x201C\x201D\x201E\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("hsb", LanguageData
      { language = "hsb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x68\x64\x64\x17A\x65\x66\x67\x68\
             \\x69\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\
             \\x73\x74\x75\x76\x77\x78\x79\x7A\xF3\x107\
             \\x10D\x11B\x142\x144\x159\x161\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\
             \\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF4\
             \\xF6\xF8\xF9\xFA\xFB\xFC\xFD\xFF\x101\x103\
             \\x105\x10F\x111\x113\x115\x117\x119\x11F\x12B\x12D\
             \\x130\x131\x13A\x13E\x148\x14D\x14F\x151\x153\x155\
             \\x15B\x15F\x165\x16B\x16D\x16F\x171\x17A\x17C"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x2019\x201A\
             \\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("hu", LanguageData
      { language = "hu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x63\x73\x63\x73\x64\x64\x64\x7A\x64\x64\x7A\x73\x64\x7A\x64\x7A\x73\
             \\x65\x66\x67\x67\x67\x79\x67\x79\x68\x69\x6A\x6B\x6C\
             \\x6C\x6C\x79\x6C\x79\x6D\x6E\x6E\x6E\x79\x6E\x79\x6F\x70\x72\x73\
             \\x73\x73\x7A\x73\x7A\x74\x74\x74\x79\x74\x79\x75\x76\x7A\x7A\x73\x7A\x7A\x73\
             \\xE1\xE9\xED\xF3\xF6\xFA\xFC\x151\x171"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79\xE0\xE2\xE3\xE4\xE5\xE6\
             \\xE7\xE8\xEA\xEB\xEC\xEE\xEF\xF1\xF2\xF4\
             \\xF8\xF9\xFB\xFF\x101\x103\x113\x115\x12B\x12D\
             \\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\x7E\xA7\xAB\xBB\x2011\x2013\x2019\x201D\x201E\x2026\
             \\x2052\x27E8\x27E9"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("hy", LanguageData
      { language = "hy"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x561\x562\x563\x564\x565\x566\x567\x568\x569\x56A\
             \\x56B\x56C\x56D\x56E\x56F\x570\x571\x572\x573\x574\
             \\x575\x576\x577\x578\x579\x57A\x57B\x57C\x57D\x57E\
             \\x57F\x580\x581\x582\x583\x584\x585\x586"
          , _exAuxiliary = Just $ Set.fromList
             "\x587"
          , _exPunctuation = Just $ Set.fromList
             "\x2C\x2E\x3A\xAB\xBB\x55A\x55B\x55C\x55D\x55E\
             \\x55F\x58A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ia", LanguageData
      { language = "ia"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x68\x64\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x70\x70\x68\x71\x72\
             \\x73\x74\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("id", LanguageData
      { language = "id"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\xE5"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ig", LanguageData
      { language = "ig"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x68\x64\x65\x66\x67\x67\x62\x67\x68\x67\x77\
             \\x68\x69\x6A\x6B\x6B\x70\x6B\x77\x6C\x6D\x6E\x6E\x77\
             \\x6E\x79\x6F\x70\x72\x73\x73\x68\x74\x75\x76\x77\
             \\x79\x7A\x1E45\x1EB9\x1ECB\x1ECD\x1EE5"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x6D\x300\x71\x78\xE0\xE1\xE8\xE9\xEC\xED\
             \\xF2\xF3\xF9\xFA\x101\x113\x12B\x144\x14D\x16B\
             \\x1F9\x1E3F\x1ECB\x300\x1ECB\x301\x1ECD\x300\x1ECD\x301\x1EE5\x300\x1EE5\x301"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2011\x2018\x2019\x201C\x201D"
          , _exNumbers = Nothing
          }
      }
    )
  , ("ii", LanguageData
      { language = "ii"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xA000\xA001\xA002\xA003\xA004\xA005\xA006\xA007\xA008\xA009\
             \\xA00A\xA00B\xA00C\xA00D\xA00E\xA00F\xA010\xA011\xA012\xA013\
             \\xA014\xA015\xA016\xA017\xA018\xA019\xA01A\xA01B\xA01C\xA01D\
             \\xA01E\xA01F\xA020\xA021\xA022\xA023\xA024\xA025\xA026\xA027\
             \\xA028\xA029\xA02A\xA02B\xA02C\xA02D\xA02E\xA02F\xA030\xA031\
             \\xA032\xA033\xA034\xA035\xA036\xA037\xA038\xA039\xA03A\xA03B\
             \\xA03C\xA03D\xA03E\xA03F\xA040\xA041\xA042\xA043\xA044\xA045\
             \\xA046\xA047\xA048\xA049\xA04A\xA04B\xA04C\xA04D\xA04E\xA04F\
             \\xA050\xA051\xA052\xA053\xA054\xA055\xA056\xA057\xA058\xA059\
             \\xA05A\xA05B\xA05C\xA05D\xA05E\xA05F\xA060\xA061\xA062\xA063\
             \\xA064\xA065\xA066\xA067\xA068\xA069\xA06A\xA06B\xA06C\xA06D\
             \\xA06E\xA06F\xA070\xA071\xA072\xA073\xA074\xA075\xA076\xA077\
             \\xA078\xA079\xA07A\xA07B\xA07C\xA07D\xA07E\xA07F\xA080\xA081\
             \\xA082\xA083\xA084\xA085\xA086\xA087\xA088\xA089\xA08A\xA08B\
             \\xA08C\xA08D\xA08E\xA08F\xA090\xA091\xA092\xA093\xA094\xA095\
             \\xA096\xA097\xA098\xA099\xA09A\xA09B\xA09C\xA09D\xA09E\xA09F\
             \\xA0A0\xA0A1\xA0A2\xA0A3\xA0A4\xA0A5\xA0A6\xA0A7\xA0A8\xA0A9\
             \\xA0AA\xA0AB\xA0AC\xA0AD\xA0AE\xA0AF\xA0B0\xA0B1\xA0B2\xA0B3\
             \\xA0B4\xA0B5\xA0B6\xA0B7\xA0B8\xA0B9\xA0BA\xA0BB\xA0BC\xA0BD\
             \\xA0BE\xA0BF\xA0C0\xA0C1\xA0C2\xA0C3\xA0C4\xA0C5\xA0C6\xA0C7\
             \\xA0C8\xA0C9\xA0CA\xA0CB\xA0CC\xA0CD\xA0CE\xA0CF\xA0D0\xA0D1\
             \\xA0D2\xA0D3\xA0D4\xA0D5\xA0D6\xA0D7\xA0D8\xA0D9\xA0DA\xA0DB\
             \\xA0DC\xA0DD\xA0DE\xA0DF\xA0E0\xA0E1\xA0E2\xA0E3\xA0E4\xA0E5\
             \\xA0E6\xA0E7\xA0E8\xA0E9\xA0EA\xA0EB\xA0EC\xA0ED\xA0EE\xA0EF\
             \\xA0F0\xA0F1\xA0F2\xA0F3\xA0F4\xA0F5\xA0F6\xA0F7\xA0F8\xA0F9\
             \\xA0FA\xA0FB\xA0FC\xA0FD\xA0FE\xA0FF\xA100\xA101\xA102\xA103\
             \\xA104\xA105\xA106\xA107\xA108\xA109\xA10A\xA10B\xA10C\xA10D\
             \\xA10E\xA10F\xA110\xA111\xA112\xA113\xA114\xA115\xA116\xA117\
             \\xA118\xA119\xA11A\xA11B\xA11C\xA11D\xA11E\xA11F\xA120\xA121\
             \\xA122\xA123\xA124\xA125\xA126\xA127\xA128\xA129\xA12A\xA12B\
             \\xA12C\xA12D\xA12E\xA12F\xA130\xA131\xA132\xA133\xA134\xA135\
             \\xA136\xA137\xA138\xA139\xA13A\xA13B\xA13C\xA13D\xA13E\xA13F\
             \\xA140\xA141\xA142\xA143\xA144\xA145\xA146\xA147\xA148\xA149\
             \\xA14A\xA14B\xA14C\xA14D\xA14E\xA14F\xA150\xA151\xA152\xA153\
             \\xA154\xA155\xA156\xA157\xA158\xA159\xA15A\xA15B\xA15C\xA15D\
             \\xA15E\xA15F\xA160\xA161\xA162\xA163\xA164\xA165\xA166\xA167\
             \\xA168\xA169\xA16A\xA16B\xA16C\xA16D\xA16E\xA16F\xA170\xA171\
             \\xA172\xA173\xA174\xA175\xA176\xA177\xA178\xA179\xA17A\xA17B\
             \\xA17C\xA17D\xA17E\xA17F\xA180\xA181\xA182\xA183\xA184\xA185\
             \\xA186\xA187\xA188\xA189\xA18A\xA18B\xA18C\xA18D\xA18E\xA18F\
             \\xA190\xA191\xA192\xA193\xA194\xA195\xA196\xA197\xA198\xA199\
             \\xA19A\xA19B\xA19C\xA19D\xA19E\xA19F\xA1A0\xA1A1\xA1A2\xA1A3\
             \\xA1A4\xA1A5\xA1A6\xA1A7\xA1A8\xA1A9\xA1AA\xA1AB\xA1AC\xA1AD\
             \\xA1AE\xA1AF\xA1B0\xA1B1\xA1B2\xA1B3\xA1B4\xA1B5\xA1B6\xA1B7\
             \\xA1B8\xA1B9\xA1BA\xA1BB\xA1BC\xA1BD\xA1BE\xA1BF\xA1C0\xA1C1\
             \\xA1C2\xA1C3\xA1C4\xA1C5\xA1C6\xA1C7\xA1C8\xA1C9\xA1CA\xA1CB\
             \\xA1CC\xA1CD\xA1CE\xA1CF\xA1D0\xA1D1\xA1D2\xA1D3\xA1D4\xA1D5\
             \\xA1D6\xA1D7\xA1D8\xA1D9\xA1DA\xA1DB\xA1DC\xA1DD\xA1DE\xA1DF\
             \\xA1E0\xA1E1\xA1E2\xA1E3\xA1E4\xA1E5\xA1E6\xA1E7\xA1E8\xA1E9\
             \\xA1EA\xA1EB\xA1EC\xA1ED\xA1EE\xA1EF\xA1F0\xA1F1\xA1F2\xA1F3\
             \\xA1F4\xA1F5\xA1F6\xA1F7\xA1F8\xA1F9\xA1FA\xA1FB\xA1FC\xA1FD\
             \\xA1FE\xA1FF\xA200\xA201\xA202\xA203\xA204\xA205\xA206\xA207\
             \\xA208\xA209\xA20A\xA20B\xA20C\xA20D\xA20E\xA20F\xA210\xA211\
             \\xA212\xA213\xA214\xA215\xA216\xA217\xA218\xA219\xA21A\xA21B\
             \\xA21C\xA21D\xA21E\xA21F\xA220\xA221\xA222\xA223\xA224\xA225\
             \\xA226\xA227\xA228\xA229\xA22A\xA22B\xA22C\xA22D\xA22E\xA22F\
             \\xA230\xA231\xA232\xA233\xA234\xA235\xA236\xA237\xA238\xA239\
             \\xA23A\xA23B\xA23C\xA23D\xA23E\xA23F\xA240\xA241\xA242\xA243\
             \\xA244\xA245\xA246\xA247\xA248\xA249\xA24A\xA24B\xA24C\xA24D\
             \\xA24E\xA24F\xA250\xA251\xA252\xA253\xA254\xA255\xA256\xA257\
             \\xA258\xA259\xA25A\xA25B\xA25C\xA25D\xA25E\xA25F\xA260\xA261\
             \\xA262\xA263\xA264\xA265\xA266\xA267\xA268\xA269\xA26A\xA26B\
             \\xA26C\xA26D\xA26E\xA26F\xA270\xA271\xA272\xA273\xA274\xA275\
             \\xA276\xA277\xA278\xA279\xA27A\xA27B\xA27C\xA27D\xA27E\xA27F\
             \\xA280\xA281\xA282\xA283\xA284\xA285\xA286\xA287\xA288\xA289\
             \\xA28A\xA28B\xA28C\xA28D\xA28E\xA28F\xA290\xA291\xA292\xA293\
             \\xA294\xA295\xA296\xA297\xA298\xA299\xA29A\xA29B\xA29C\xA29D\
             \\xA29E\xA29F\xA2A0\xA2A1\xA2A2\xA2A3\xA2A4\xA2A5\xA2A6\xA2A7\
             \\xA2A8\xA2A9\xA2AA\xA2AB\xA2AC\xA2AD\xA2AE\xA2AF\xA2B0\xA2B1\
             \\xA2B2\xA2B3\xA2B4\xA2B5\xA2B6\xA2B7\xA2B8\xA2B9\xA2BA\xA2BB\
             \\xA2BC\xA2BD\xA2BE\xA2BF\xA2C0\xA2C1\xA2C2\xA2C3\xA2C4\xA2C5\
             \\xA2C6\xA2C7\xA2C8\xA2C9\xA2CA\xA2CB\xA2CC\xA2CD\xA2CE\xA2CF\
             \\xA2D0\xA2D1\xA2D2\xA2D3\xA2D4\xA2D5\xA2D6\xA2D7\xA2D8\xA2D9\
             \\xA2DA\xA2DB\xA2DC\xA2DD\xA2DE\xA2DF\xA2E0\xA2E1\xA2E2\xA2E3\
             \\xA2E4\xA2E5\xA2E6\xA2E7\xA2E8\xA2E9\xA2EA\xA2EB\xA2EC\xA2ED\
             \\xA2EE\xA2EF\xA2F0\xA2F1\xA2F2\xA2F3\xA2F4\xA2F5\xA2F6\xA2F7\
             \\xA2F8\xA2F9\xA2FA\xA2FB\xA2FC\xA2FD\xA2FE\xA2FF\xA300\xA301\
             \\xA302\xA303\xA304\xA305\xA306\xA307\xA308\xA309\xA30A\xA30B\
             \\xA30C\xA30D\xA30E\xA30F\xA310\xA311\xA312\xA313\xA314\xA315\
             \\xA316\xA317\xA318\xA319\xA31A\xA31B\xA31C\xA31D\xA31E\xA31F\
             \\xA320\xA321\xA322\xA323\xA324\xA325\xA326\xA327\xA328\xA329\
             \\xA32A\xA32B\xA32C\xA32D\xA32E\xA32F\xA330\xA331\xA332\xA333\
             \\xA334\xA335\xA336\xA337\xA338\xA339\xA33A\xA33B\xA33C\xA33D\
             \\xA33E\xA33F\xA340\xA341\xA342\xA343\xA344\xA345\xA346\xA347\
             \\xA348\xA349\xA34A\xA34B\xA34C\xA34D\xA34E\xA34F\xA350\xA351\
             \\xA352\xA353\xA354\xA355\xA356\xA357\xA358\xA359\xA35A\xA35B\
             \\xA35C\xA35D\xA35E\xA35F\xA360\xA361\xA362\xA363\xA364\xA365\
             \\xA366\xA367\xA368\xA369\xA36A\xA36B\xA36C\xA36D\xA36E\xA36F\
             \\xA370\xA371\xA372\xA373\xA374\xA375\xA376\xA377\xA378\xA379\
             \\xA37A\xA37B\xA37C\xA37D\xA37E\xA37F\xA380\xA381\xA382\xA383\
             \\xA384\xA385\xA386\xA387\xA388\xA389\xA38A\xA38B\xA38C\xA38D\
             \\xA38E\xA38F\xA390\xA391\xA392\xA393\xA394\xA395\xA396\xA397\
             \\xA398\xA399\xA39A\xA39B\xA39C\xA39D\xA39E\xA39F\xA3A0\xA3A1\
             \\xA3A2\xA3A3\xA3A4\xA3A5\xA3A6\xA3A7\xA3A8\xA3A9\xA3AA\xA3AB\
             \\xA3AC\xA3AD\xA3AE\xA3AF\xA3B0\xA3B1\xA3B2\xA3B3\xA3B4\xA3B5\
             \\xA3B6\xA3B7\xA3B8\xA3B9\xA3BA\xA3BB\xA3BC\xA3BD\xA3BE\xA3BF\
             \\xA3C0\xA3C1\xA3C2\xA3C3\xA3C4\xA3C5\xA3C6\xA3C7\xA3C8\xA3C9\
             \\xA3CA\xA3CB\xA3CC\xA3CD\xA3CE\xA3CF\xA3D0\xA3D1\xA3D2\xA3D3\
             \\xA3D4\xA3D5\xA3D6\xA3D7\xA3D8\xA3D9\xA3DA\xA3DB\xA3DC\xA3DD\
             \\xA3DE\xA3DF\xA3E0\xA3E1\xA3E2\xA3E3\xA3E4\xA3E5\xA3E6\xA3E7\
             \\xA3E8\xA3E9\xA3EA\xA3EB\xA3EC\xA3ED\xA3EE\xA3EF\xA3F0\xA3F1\
             \\xA3F2\xA3F3\xA3F4\xA3F5\xA3F6\xA3F7\xA3F8\xA3F9\xA3FA\xA3FB\
             \\xA3FC\xA3FD\xA3FE\xA3FF\xA400\xA401\xA402\xA403\xA404\xA405\
             \\xA406\xA407\xA408\xA409\xA40A\xA40B\xA40C\xA40D\xA40E\xA40F\
             \\xA410\xA411\xA412\xA413\xA414\xA415\xA416\xA417\xA418\xA419\
             \\xA41A\xA41B\xA41C\xA41D\xA41E\xA41F\xA420\xA421\xA422\xA423\
             \\xA424\xA425\xA426\xA427\xA428\xA429\xA42A\xA42B\xA42C\xA42D\
             \\xA42E\xA42F\xA430\xA431\xA432\xA433\xA434\xA435\xA436\xA437\
             \\xA438\xA439\xA43A\xA43B\xA43C\xA43D\xA43E\xA43F\xA440\xA441\
             \\xA442\xA443\xA444\xA445\xA446\xA447\xA448\xA449\xA44A\xA44B\
             \\xA44C\xA44D\xA44E\xA44F\xA450\xA451\xA452\xA453\xA454\xA455\
             \\xA456\xA457\xA458\xA459\xA45A\xA45B\xA45C\xA45D\xA45E\xA45F\
             \\xA460\xA461\xA462\xA463\xA464\xA465\xA466\xA467\xA468\xA469\
             \\xA46A\xA46B\xA46C\xA46D\xA46E\xA46F\xA470\xA471\xA472\xA473\
             \\xA474\xA475\xA476\xA477\xA478\xA479\xA47A\xA47B\xA47C\xA47D\
             \\xA47E\xA47F\xA480\xA481\xA482\xA483\xA484\xA485\xA486\xA487\
             \\xA488\xA489\xA48A\xA48B\xA48C"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("is", LanguageData
      { language = "is"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x76\
             \\x78\x79\xE1\xE6\xE9\xED\xF0\xF3\xF6\xFA\
             \\xFD\xFE"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x77\x7A"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x201A\x201C\x201E\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("it", LanguageData
      { language = "it"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE8\xE9\xEC\
             \\xF2\xF3\xF9"
          , _exAuxiliary = Just $ Set.fromList
             "\xAA\xBA\xDF\xE1\xE2\xE3\xE4\xE5\xE6\xE7\
             \\xEA\xEB\xED\xEE\xEF\xF1\xF4\xF5\xF6\xF8\
             \\xFA\xFB\xFC\xFF\x153"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x2F\x3A\
             \\x3B\x3F\x40\x5B\x5D\x7B\x7D\xAB\xBB\x2011\
             \\x2014\x2019\x201C\x201D\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ja", LanguageData
      { language = "ja"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x3005\x3041\x30A1\x3042\x30A2\x3043\x30A3\x3044\x30A4\x3045\x30A5\x3046\x30A6\x3047\x30A7\x3048\x30A8\x3049\x30A9\
             \\x304A\x30AA\x304B\x30AB\x304C\x30AC\x304D\x30AD\x304E\x30AE\x304F\x30AF\x3050\x30B0\x3051\x30B1\x3052\x30B2\x3053\x30B3\
             \\x3054\x30B4\x3055\x30B5\x3056\x30B6\x3057\x30B7\x3058\x30B8\x3059\x30B9\x305A\x30BA\x305B\x30BB\x305C\x30BC\x305D\x30BD\
             \\x305E\x30BE\x305F\x30BF\x3060\x30C0\x3061\x30C1\x3062\x30C2\x3063\x30C3\x3064\x30C4\x3065\x30C5\x3066\x30C6\x3067\x30C7\
             \\x3068\x30C8\x3069\x30C9\x306A\x30CA\x306B\x30CB\x306C\x30CC\x306D\x30CD\x306E\x30CE\x306F\x30CF\x3070\x30D0\x3071\x30D1\
             \\x3072\x30D2\x3073\x30D3\x3074\x30D4\x3075\x30D5\x3076\x30D6\x3077\x30D7\x3078\x30D8\x3079\x30D9\x307A\x30DA\x307B\x30DB\
             \\x307C\x30DC\x307D\x30DD\x307E\x30DE\x307F\x30DF\x3080\x30E0\x3081\x30E1\x3082\x30E2\x3083\x30E3\x3084\x30E4\x3085\x30E5\
             \\x3086\x30E6\x3087\x30E7\x3088\x30E8\x3089\x30E9\x308A\x30EA\x308B\x30EB\x308C\x30EC\x308D\x30ED\x308E\x30EE\x308F\x30EF\
             \\x3090\x30F0\x3091\x30F1\x3092\x30F2\x3093\x30F3\x309D\x30FD\x309E\x30FE\x30F4\x30F5\x30F6\x30FC\
             \\x4E00\x4E01\x4E03\x4E07\x4E08\x4E09\x4E0A\x4E0B\x4E0D\x4E0E\
             \\x4E14\x4E16\x4E18\x4E19\x4E21\x4E26\x4E2D\x4E32\x4E38\x4E39\
             \\x4E3B\x4E3C\x4E45\x4E4F\x4E57\x4E59\x4E5D\x4E5E\x4E71\x4E73\
             \\x4E7E\x4E80\x4E86\x4E88\x4E89\x4E8B\x4E8C\x4E92\x4E94\x4E95\
             \\x4E9C\x4EA1\x4EA4\x4EAB\x4EAC\x4EAD\x4EBA\x4EC1\x4ECA\x4ECB\
             \\x4ECF\x4ED5\x4ED6\x4ED8\x4ED9\x4EE3\x4EE4\x4EE5\x4EEE\x4EF0\
             \\x4EF2\x4EF6\x4EFB\x4F01\x4F0E\x4F0F\x4F10\x4F11\x4F1A\x4F1D\
             \\x4F2F\x4F34\x4F38\x4F3A\x4F3C\x4F46\x4F4D\x4F4E\x4F4F\x4F50\
             \\x4F53\x4F55\x4F59\x4F5C\x4F73\x4F75\x4F7F\x4F8B\x4F8D\x4F9B\
             \\x4F9D\x4FA1\x4FAE\x4FAF\x4FB5\x4FB6\x4FBF\x4FC2\x4FC3\x4FCA\
             \\x4FD7\x4FDD\x4FE1\x4FEE\x4FF3\x4FF5\x4FF8\x4FFA\x5009\x500B\
             \\x500D\x5012\x5019\x501F\x5023\x5024\x502B\x5039\x5049\x504F\
             \\x505C\x5065\x5074\x5075\x5076\x507D\x508D\x5091\x5098\x5099\
             \\x50AC\x50B2\x50B5\x50B7\x50BE\x50C5\x50CD\x50CF\x50D5\x50DA\
             \\x50E7\x5100\x5104\x5112\x511F\x512A\x5143\x5144\x5145\x5146\
             \\x5148\x5149\x514B\x514D\x5150\x515A\x5165\x5168\x516B\x516C\
             \\x516D\x5171\x5175\x5177\x5178\x517C\x5185\x5186\x518A\x518D\
             \\x5192\x5197\x5199\x51A0\x51A5\x51AC\x51B6\x51B7\x51C4\x51C6\
             \\x51CD\x51DD\x51E1\x51E6\x51F6\x51F8\x51F9\x51FA\x5200\x5203\
             \\x5206\x5207\x5208\x520A\x5211\x5217\x521D\x5224\x5225\x5229\
             \\x5230\x5236\x5237\x5238\x5239\x523A\x523B\x5247\x524A\x524D\
             \\x5256\x525B\x5263\x5264\x5265\x526F\x5270\x5272\x5275\x5287\
             \\x529B\x529F\x52A0\x52A3\x52A9\x52AA\x52B1\x52B4\x52B9\x52BE\
             \\x52C3\x52C5\x52C7\x52C9\x52D5\x52D8\x52D9\x52DD\x52DF\x52E2\
             \\x52E4\x52E7\x52F2\x52FE\x5302\x5305\x5316\x5317\x5320\x5339\
             \\x533A\x533B\x533F\x5341\x5343\x5347\x5348\x534A\x5351\x5352\
             \\x5353\x5354\x5357\x5358\x535A\x5360\x5370\x5371\x5373\x5374\
             \\x5375\x5378\x5384\x5398\x539A\x539F\x53B3\x53BB\x53C2\x53C8\
             \\x53CA\x53CB\x53CC\x53CD\x53CE\x53D4\x53D6\x53D7\x53D9\x53E3\
             \\x53E4\x53E5\x53EB\x53EC\x53EF\x53F0\x53F1\x53F2\x53F3\x53F7\
             \\x53F8\x5404\x5408\x5409\x540C\x540D\x540E\x540F\x5410\x5411\
             \\x541B\x541F\x5426\x542B\x5438\x5439\x5442\x5448\x5449\x544A\
             \\x5468\x546A\x5473\x547C\x547D\x548C\x54B2\x54BD\x54C0\x54C1\
             \\x54E1\x54F2\x54FA\x5504\x5506\x5507\x5510\x552F\x5531\x553E\
             \\x5546\x554F\x5553\x5584\x5589\x559A\x559C\x559D\x55A9\x55AA\
             \\x55AB\x55B6\x55C5\x55E3\x5606\x5631\x5632\x5668\x5674\x5687\
             \\x56DA\x56DB\x56DE\x56E0\x56E3\x56F0\x56F2\x56F3\x56FA\x56FD\
             \\x570F\x5712\x571F\x5727\x5728\x5730\x5742\x5747\x574A\x5751\
             \\x576A\x5782\x578B\x57A3\x57CB\x57CE\x57DF\x57F7\x57F9\x57FA\
             \\x57FC\x5800\x5802\x5805\x5806\x5815\x5824\x582A\x5831\x5834\
             \\x5840\x5841\x584A\x5851\x5854\x5857\x585A\x585E\x5869\x586B\
             \\x587E\x5883\x5893\x5897\x589C\x58A8\x58B3\x58BE\x58C1\x58C7\
             \\x58CA\x58CC\x58EB\x58EE\x58F0\x58F1\x58F2\x5909\x590F\x5915\
             \\x5916\x591A\x591C\x5922\x5927\x5929\x592A\x592B\x592E\x5931\
             \\x5947\x5948\x5949\x594F\x5951\x5954\x5965\x5968\x596A\x596E\
             \\x5973\x5974\x597D\x5982\x5983\x5984\x598A\x5996\x5999\x59A5\
             \\x59A8\x59AC\x59B9\x59BB\x59C9\x59CB\x59D3\x59D4\x59EB\x59FB\
             \\x59FF\x5A01\x5A18\x5A20\x5A2F\x5A46\x5A5A\x5A66\x5A7F\x5A92\
             \\x5A9B\x5AC1\x5AC9\x5ACC\x5AE1\x5B22\x5B50\x5B54\x5B57\x5B58\
             \\x5B5D\x5B63\x5B64\x5B66\x5B6B\x5B85\x5B87\x5B88\x5B89\x5B8C\
             \\x5B97\x5B98\x5B99\x5B9A\x5B9B\x5B9C\x5B9D\x5B9F\x5BA2\x5BA3\
             \\x5BA4\x5BAE\x5BB0\x5BB3\x5BB4\x5BB5\x5BB6\x5BB9\x5BBF\x5BC2\
             \\x5BC4\x5BC6\x5BCC\x5BD2\x5BDB\x5BDD\x5BDF\x5BE1\x5BE7\x5BE9\
             \\x5BEE\x5BF8\x5BFA\x5BFE\x5BFF\x5C01\x5C02\x5C04\x5C06\x5C09\
             \\x5C0A\x5C0B\x5C0E\x5C0F\x5C11\x5C1A\x5C31\x5C3A\x5C3B\x5C3C\
             \\x5C3D\x5C3E\x5C3F\x5C40\x5C45\x5C48\x5C4A\x5C4B\x5C55\x5C5E\
             \\x5C64\x5C65\x5C6F\x5C71\x5C90\x5CA1\x5CA9\x5CAC\x5CB3\x5CB8\
             \\x5CE0\x5CE1\x5CF0\x5CF6\x5D07\x5D0E\x5D16\x5D29\x5D50\x5DDD\
             \\x5DDE\x5DE1\x5DE3\x5DE5\x5DE6\x5DE7\x5DE8\x5DEE\x5DF1\x5DFB\
             \\x5DFE\x5E02\x5E03\x5E06\x5E0C\x5E1D\x5E25\x5E2B\x5E2D\x5E2F\
             \\x5E30\x5E33\x5E38\x5E3D\x5E45\x5E55\x5E63\x5E72\x5E73\x5E74\
             \\x5E78\x5E79\x5E7B\x5E7C\x5E7D\x5E7E\x5E81\x5E83\x5E8A\x5E8F\
             \\x5E95\x5E97\x5E9C\x5EA6\x5EA7\x5EAB\x5EAD\x5EB6\x5EB7\x5EB8\
             \\x5EC3\x5EC9\x5ECA\x5EF6\x5EF7\x5EFA\x5F01\x5F04\x5F0A\x5F0F\
             \\x5F10\x5F13\x5F14\x5F15\x5F1F\x5F25\x5F26\x5F27\x5F31\x5F35\
             \\x5F37\x5F3E\x5F53\x5F59\x5F62\x5F69\x5F6B\x5F70\x5F71\x5F79\
             \\x5F7C\x5F80\x5F81\x5F84\x5F85\x5F8B\x5F8C\x5F90\x5F92\x5F93\
             \\x5F97\x5FA1\x5FA9\x5FAA\x5FAE\x5FB3\x5FB4\x5FB9\x5FC3\x5FC5\
             \\x5FCC\x5FCD\x5FD7\x5FD8\x5FD9\x5FDC\x5FE0\x5FEB\x5FF5\x6012\
             \\x6016\x601D\x6020\x6025\x6027\x6028\x602A\x604B\x6050\x6052\
             \\x6063\x6065\x6068\x6069\x606D\x606F\x6075\x6094\x609F\x60A0\
             \\x60A3\x60A6\x60A9\x60AA\x60B2\x60BC\x60C5\x60D1\x60DC\x60E7\
             \\x60E8\x60F0\x60F3\x6101\x6109\x610F\x611A\x611B\x611F\x6144\
             \\x6148\x614B\x614C\x614E\x6155\x6162\x6163\x6168\x616E\x6170\
             \\x6176\x6182\x618E\x61A4\x61A7\x61A9\x61AC\x61B2\x61B6\x61BE\
             \\x61C7\x61D0\x61F2\x61F8\x6210\x6211\x6212\x621A\x6226\x622F\
             \\x6234\x6238\x623B\x623F\x6240\x6247\x6249\x624B\x624D\x6253\
             \\x6255\x6271\x6276\x6279\x627F\x6280\x6284\x628A\x6291\x6295\
             \\x6297\x6298\x629C\x629E\x62AB\x62B1\x62B5\x62B9\x62BC\x62BD\
             \\x62C5\x62C9\x62CD\x62D0\x62D2\x62D3\x62D8\x62D9\x62DB\x62DD\
             \\x62E0\x62E1\x62EC\x62ED\x62F3\x62F6\x62F7\x62FE\x6301\x6307\
             \\x6311\x6319\x631F\x6328\x632B\x632F\x633F\x6349\x6355\x6357\
             \\x635C\x6368\x636E\x637B\x6383\x6388\x638C\x6392\x6398\x639B\
             \\x63A1\x63A2\x63A5\x63A7\x63A8\x63AA\x63B2\x63CF\x63D0\x63DA\
             \\x63DB\x63E1\x63EE\x63F4\x63FA\x640D\x642C\x642D\x643A\x643E\
             \\x6442\x6458\x6469\x646F\x6483\x64A4\x64AE\x64B2\x64C1\x64CD\
             \\x64E6\x64EC\x652F\x6539\x653B\x653E\x653F\x6545\x654F\x6551\
             \\x6557\x6559\x6562\x6563\x656C\x6570\x6574\x6575\x6577\x6587\
             \\x6589\x658E\x6591\x6597\x6599\x659C\x65A4\x65A5\x65AC\x65AD\
             \\x65B0\x65B9\x65BD\x65C5\x65CB\x65CF\x65D7\x65E2\x65E5\x65E6\
             \\x65E7\x65E8\x65E9\x65EC\x65FA\x6606\x6607\x660E\x6613\x6614\
             \\x661F\x6620\x6625\x6627\x6628\x662D\x662F\x663C\x6642\x6669\
             \\x666E\x666F\x6674\x6676\x6681\x6687\x6691\x6696\x6697\x66A6\
             \\x66AB\x66AE\x66B4\x66C7\x66D6\x66DC\x66F2\x66F4\x66F8\x66F9\
             \\x66FD\x66FF\x6700\x6708\x6709\x670D\x6715\x6717\x671B\x671D\
             \\x671F\x6728\x672A\x672B\x672C\x672D\x6731\x6734\x673A\x673D\
             \\x6749\x6750\x6751\x675F\x6761\x6765\x676F\x6771\x677E\x677F\
             \\x6790\x6795\x6797\x679A\x679C\x679D\x67A0\x67A2\x67AF\x67B6\
             \\x67C4\x67D0\x67D3\x67D4\x67F1\x67F3\x67F5\x67FB\x67FF\x6803\
             \\x6804\x6813\x6821\x682A\x6838\x6839\x683C\x683D\x6841\x6843\
             \\x6848\x6851\x685C\x685F\x6885\x6897\x68A8\x68B0\x68C4\x68CB\
             \\x68D2\x68DA\x68DF\x68EE\x68FA\x6905\x690D\x690E\x691C\x696D\
             \\x6975\x6977\x697C\x697D\x6982\x69CB\x69D8\x69FD\x6A19\x6A21\
             \\x6A29\x6A2A\x6A39\x6A4B\x6A5F\x6B04\x6B20\x6B21\x6B27\x6B32\
             \\x6B3A\x6B3E\x6B4C\x6B53\x6B62\x6B63\x6B66\x6B69\x6B6F\x6B73\
             \\x6B74\x6B7B\x6B89\x6B8A\x6B8B\x6B96\x6BB4\x6BB5\x6BBA\x6BBB\
             \\x6BBF\x6BC0\x6BCD\x6BCE\x6BD2\x6BD4\x6BDB\x6C0F\x6C11\x6C17\
             \\x6C34\x6C37\x6C38\x6C3E\x6C41\x6C42\x6C4E\x6C57\x6C5A\x6C5F\
             \\x6C60\x6C70\x6C7A\x6C7D\x6C83\x6C88\x6C96\x6C99\x6CA1\x6CA2\
             \\x6CB3\x6CB8\x6CB9\x6CBB\x6CBC\x6CBF\x6CC1\x6CC9\x6CCA\x6CCC\
             \\x6CD5\x6CE1\x6CE2\x6CE3\x6CE5\x6CE8\x6CF0\x6CF3\x6D0B\x6D17\
             \\x6D1E\x6D25\x6D2A\x6D3B\x6D3E\x6D41\x6D44\x6D45\x6D5C\x6D66\
             \\x6D6A\x6D6E\x6D74\x6D77\x6D78\x6D88\x6D99\x6DAF\x6DB2\x6DBC\
             \\x6DD1\x6DE1\x6DEB\x6DF1\x6DF7\x6DFB\x6E05\x6E07\x6E08\x6E09\
             \\x6E0B\x6E13\x6E1B\x6E21\x6E26\x6E29\x6E2C\x6E2F\x6E56\x6E67\
             \\x6E6F\x6E7E\x6E7F\x6E80\x6E90\x6E96\x6E9D\x6EB6\x6EBA\x6EC5\
             \\x6ECB\x6ED1\x6EDD\x6EDE\x6EF4\x6F01\x6F02\x6F06\x6F0F\x6F14\
             \\x6F20\x6F22\x6F2B\x6F2C\x6F38\x6F54\x6F5C\x6F5F\x6F64\x6F6E\
             \\x6F70\x6F84\x6FC0\x6FC1\x6FC3\x6FEB\x6FEF\x702C\x706B\x706F\
             \\x7070\x707D\x7089\x708A\x708E\x70AD\x70B9\x70BA\x70C8\x7121\
             \\x7126\x7136\x713C\x714E\x7159\x7167\x7169\x716E\x718A\x719F\
             \\x71B1\x71C3\x71E5\x7206\x722A\x7235\x7236\x723D\x7247\x7248\
             \\x7259\x725B\x7267\x7269\x7272\x7279\x72A0\x72AC\x72AF\x72B6\
             \\x72C2\x72D9\x72E9\x72EC\x72ED\x731B\x731F\x732B\x732E\x7336\
             \\x733F\x7344\x7363\x7372\x7384\x7387\x7389\x738B\x73A9\x73CD\
             \\x73E0\x73ED\x73FE\x7403\x7406\x7434\x7460\x7483\x74A7\x74B0\
             \\x74BD\x74E6\x74F6\x7518\x751A\x751F\x7523\x7528\x7530\x7531\
             \\x7532\x7533\x7537\x753A\x753B\x754C\x754F\x7551\x7554\x7559\
             \\x755C\x755D\x7565\x756A\x7570\x7573\x757F\x758E\x7591\x75AB\
             \\x75B2\x75BE\x75C5\x75C7\x75D5\x75D8\x75DB\x75E2\x75E9\x75F4\
             \\x760D\x7642\x7652\x7656\x767A\x767B\x767D\x767E\x7684\x7686\
             \\x7687\x76AE\x76BF\x76C6\x76CA\x76D7\x76DB\x76DF\x76E3\x76E4\
             \\x76EE\x76F2\x76F4\x76F8\x76FE\x7701\x7709\x770B\x770C\x771F\
             \\x7720\x773A\x773C\x7740\x7761\x7763\x7766\x77AC\x77AD\x77B3\
             \\x77DB\x77E2\x77E5\x77ED\x77EF\x77F3\x7802\x7814\x7815\x7832\
             \\x7834\x785D\x786B\x786C\x7881\x7891\x78BA\x78C1\x78E8\x7901\
             \\x790E\x793A\x793C\x793E\x7948\x7949\x7956\x795D\x795E\x7965\
             \\x7968\x796D\x7981\x7985\x798D\x798F\x79C0\x79C1\x79CB\x79D1\
             \\x79D2\x79D8\x79DF\x79E9\x79F0\x79FB\x7A0B\x7A0E\x7A1A\x7A2E\
             \\x7A32\x7A3C\x7A3D\x7A3F\x7A40\x7A42\x7A4D\x7A4F\x7A6B\x7A74\
             \\x7A76\x7A7A\x7A81\x7A83\x7A92\x7A93\x7A9F\x7AAE\x7AAF\x7ACB\
             \\x7ADC\x7AE0\x7AE5\x7AEF\x7AF6\x7AF9\x7B11\x7B1B\x7B26\x7B2C\
             \\x7B46\x7B49\x7B4B\x7B52\x7B54\x7B56\x7B87\x7B8B\x7B97\x7BA1\
             \\x7BB1\x7BB8\x7BC0\x7BC4\x7BC9\x7BE4\x7C21\x7C3F\x7C4D\x7C60\
             \\x7C73\x7C89\x7C8B\x7C92\x7C97\x7C98\x7C9B\x7CA7\x7CBE\x7CD6\
             \\x7CE7\x7CF8\x7CFB\x7CFE\x7D00\x7D04\x7D05\x7D0B\x7D0D\x7D14\
             \\x7D19\x7D1A\x7D1B\x7D20\x7D21\x7D22\x7D2B\x7D2F\x7D30\x7D33\
             \\x7D39\x7D3A\x7D42\x7D44\x7D4C\x7D50\x7D5E\x7D61\x7D66\x7D71\
             \\x7D75\x7D76\x7D79\x7D99\x7D9A\x7DAD\x7DB1\x7DB2\x7DBB\x7DBF\
             \\x7DCA\x7DCF\x7DD1\x7DD2\x7DDA\x7DE0\x7DE8\x7DE9\x7DEF\x7DF4\
             \\x7DFB\x7E01\x7E04\x7E1B\x7E26\x7E2B\x7E2E\x7E3E\x7E41\x7E4A\
             \\x7E54\x7E55\x7E6D\x7E70\x7F36\x7F6A\x7F6E\x7F70\x7F72\x7F75\
             \\x7F77\x7F85\x7F8A\x7F8E\x7F9E\x7FA4\x7FA8\x7FA9\x7FBD\x7FC1\
             \\x7FCC\x7FD2\x7FFB\x7FFC\x8001\x8003\x8005\x8010\x8015\x8017\
             \\x8033\x8056\x805E\x8074\x8077\x8089\x808C\x8096\x8098\x809D\
             \\x80A1\x80A2\x80A5\x80A9\x80AA\x80AF\x80B2\x80BA\x80C3\x80C6\
             \\x80CC\x80CE\x80DE\x80F4\x80F8\x80FD\x8102\x8105\x8107\x8108\
             \\x810A\x811A\x8131\x8133\x814E\x8150\x8155\x816B\x8170\x8178\
             \\x8179\x817A\x819A\x819C\x819D\x81A8\x81B3\x81C6\x81D3\x81E3\
             \\x81E8\x81EA\x81ED\x81F3\x81F4\x81FC\x8208\x820C\x820E\x8217\
             \\x821E\x821F\x822A\x822C\x8236\x8237\x8239\x8247\x8266\x826F\
             \\x8272\x8276\x828B\x829D\x82AF\x82B1\x82B3\x82B8\x82BD\x82D7\
             \\x82DB\x82E5\x82E6\x82F1\x8302\x830E\x8328\x8336\x8349\x8352\
             \\x8358\x8377\x83CA\x83CC\x83D3\x83DC\x83EF\x840E\x843D\x8449\
             \\x8457\x845B\x846C\x84B8\x84C4\x84CB\x8511\x8535\x853D\x8584\
             \\x85A6\x85AA\x85AB\x85AC\x85CD\x85E4\x85E9\x85FB\x864E\x8650\
             \\x865A\x865C\x865E\x866B\x8679\x868A\x8695\x86C7\x86CD\x86EE\
             \\x8702\x871C\x878D\x8840\x8846\x884C\x8853\x8857\x885B\x885D\
             \\x8861\x8863\x8868\x8870\x8877\x888B\x8896\x88AB\x88C1\x88C2\
             \\x88C5\x88CF\x88D5\x88DC\x88F8\x88FD\x88FE\x8907\x8910\x8912\
             \\x895F\x8972\x897F\x8981\x8986\x8987\x898B\x898F\x8996\x899A\
             \\x89A7\x89AA\x89B3\x89D2\x89E3\x89E6\x8A00\x8A02\x8A03\x8A08\
             \\x8A0E\x8A13\x8A17\x8A18\x8A1F\x8A2A\x8A2D\x8A31\x8A33\x8A34\
             \\x8A3A\x8A3C\x8A50\x8A54\x8A55\x8A5E\x8A60\x8A63\x8A66\x8A69\
             \\x8A6E\x8A70\x8A71\x8A72\x8A73\x8A87\x8A89\x8A8C\x8A8D\x8A93\
             \\x8A95\x8A98\x8A9E\x8AA0\x8AA4\x8AAC\x8AAD\x8AB0\x8AB2\x8ABF\
             \\x8AC7\x8ACB\x8AD6\x8AE6\x8AE7\x8AED\x8AEE\x8AF8\x8AFE\x8B00\
             \\x8B01\x8B04\x8B0E\x8B19\x8B1B\x8B1D\x8B21\x8B39\x8B58\x8B5C\
             \\x8B66\x8B70\x8B72\x8B77\x8C37\x8C46\x8C4A\x8C5A\x8C61\x8C6A\
             \\x8C8C\x8C9D\x8C9E\x8CA0\x8CA1\x8CA2\x8CA7\x8CA8\x8CA9\x8CAA\
             \\x8CAB\x8CAC\x8CAF\x8CB4\x8CB7\x8CB8\x8CBB\x8CBC\x8CBF\x8CC0\
             \\x8CC2\x8CC3\x8CC4\x8CC7\x8CCA\x8CD3\x8CDB\x8CDC\x8CDE\x8CE0\
             \\x8CE2\x8CE6\x8CEA\x8CED\x8CFC\x8D08\x8D64\x8D66\x8D70\x8D74\
             \\x8D77\x8D85\x8D8A\x8DA3\x8DB3\x8DDD\x8DE1\x8DEF\x8DF3\x8DF5\
             \\x8E0A\x8E0F\x8E2A\x8E74\x8E8D\x8EAB\x8ECA\x8ECC\x8ECD\x8ED2\
             \\x8EDF\x8EE2\x8EF8\x8EFD\x8F03\x8F09\x8F1D\x8F29\x8F2A\x8F38\
             \\x8F44\x8F9B\x8F9E\x8FA3\x8FB1\x8FB2\x8FBA\x8FBC\x8FC5\x8FCE\
             \\x8FD1\x8FD4\x8FEB\x8FED\x8FF0\x8FF7\x8FFD\x9000\x9001\x9003\
             \\x9006\x900F\x9010\x9013\x9014\x901A\x901D\x901F\x9020\x9023\
             \\x902E\x9031\x9032\x9038\x9042\x9045\x9047\x904A\x904B\x904D\
             \\x904E\x9053\x9054\x9055\x905C\x9060\x9061\x9063\x9069\x906D\
             \\x906E\x9075\x9077\x9078\x907A\x907F\x9084\x90A3\x90A6\x90AA\
             \\x90B8\x90CA\x90CE\x90E1\x90E8\x90ED\x90F5\x90F7\x90FD\x914C\
             \\x914D\x914E\x9152\x9154\x9162\x916A\x916C\x9175\x9177\x9178\
             \\x9192\x919C\x91B8\x91C7\x91C8\x91CC\x91CD\x91CE\x91CF\x91D1\
             \\x91DC\x91DD\x91E3\x920D\x9234\x9244\x925B\x9262\x9271\x9280\
             \\x9283\x9285\x9298\x92AD\x92ED\x92F3\x92FC\x9320\x9326\x932C\
             \\x932E\x932F\x9332\x934B\x935B\x9375\x938C\x9396\x93AE\x93E1\
             \\x9418\x9451\x9577\x9580\x9589\x958B\x9591\x9593\x95A2\x95A3\
             \\x95A5\x95B2\x95C7\x95D8\x961C\x962A\x9632\x963B\x9644\x964D\
             \\x9650\x965B\x9662\x9663\x9664\x9665\x966A\x9670\x9673\x9675\
             \\x9676\x9678\x967A\x967D\x9685\x9686\x968A\x968E\x968F\x9694\
             \\x9699\x969B\x969C\x96A0\x96A3\x96B7\x96BB\x96C4\x96C5\x96C6\
             \\x96C7\x96CC\x96D1\x96E2\x96E3\x96E8\x96EA\x96F0\x96F2\x96F6\
             \\x96F7\x96FB\x9700\x9707\x970A\x971C\x9727\x9732\x9752\x9759\
             \\x975E\x9762\x9769\x9774\x97D3\x97F3\x97FB\x97FF\x9802\x9803\
             \\x9805\x9806\x9808\x9810\x9811\x9812\x9813\x9818\x982C\x982D\
             \\x983B\x983C\x984C\x984D\x984E\x9854\x9855\x9858\x985E\x9867\
             \\x98A8\x98DB\x98DF\x98E2\x98EF\x98F2\x98FC\x98FD\x98FE\x9905\
             \\x990A\x990C\x9913\x9928\x9996\x9999\x99AC\x99C4\x99C5\x99C6\
             \\x99D0\x99D2\x9A0E\x9A12\x9A13\x9A30\x9A5A\x9AA8\x9AB8\x9AC4\
             \\x9AD8\x9AEA\x9B31\x9B3C\x9B42\x9B45\x9B54\x9B5A\x9BAE\x9BE8\
             \\x9CE5\x9CF4\x9D8F\x9DB4\x9E7F\x9E93\x9E97\x9EA6\x9EBA\x9EBB\
             \\x9EC4\x9ED2\x9ED9\x9F13\x9F3B\x9F62"
          , _exAuxiliary = Just $ Set.fromList
             "\x4E11\x4EA5\x4EA8\x514C\x514E\x51E7\x5243\x536F\x5609\x5614\
             \\x5618\x58EC\x58FA\x5B09\x5BC5\x5DF3\x5E9A\x5EB5\x5F18\x5F57\
             \\x60B6\x6115\x620A\x620C\x62FC\x63C3\x65A7\x660C\x6756\x6876\
             \\x68B5\x6954\x6E58\x711A\x71ED\x722C\x724C\x725D\x7261\x72D0\
             \\x72D7\x72FC\x732A\x7345\x7678\x7791\x7887\x795A\x7984\x798E\
             \\x79E4\x7AFF\x7D46\x7E4D\x7F6B\x818F\x8292\x87C4\x87F9\x880D\
             \\x8823\x8D1B\x8E44\x8FB0\x9149\x92F2\x9304\x9328\x958F\x95A9\
             \\x96C0\x96C9\x9CF3\x9F20\x9F8D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\xFF01\x22\xFF02\x23\xFF03\x25\xFF05\x26\xFF06\x28\xFF08\x29\xFF09\x2A\xFF0A\x2C\xFF0C\x2D\xFF0D\
             \\x2E\xFF0E\x2F\xFF0F\x3A\xFF1A\x3B\xFF1B\x3F\xFF1F\x40\xFF20\x5B\xFF3B\x5D\xFF3D\x5F\xFF3F\x7B\xFF5B\
             \\x7D\xFF5D\xA7\xB6\x2010\x2011\x2014\x2015\x2016\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2025\x2026\x2030\x2032\x2033\x203B\
             \\x203E\x3001\xFF64\x3002\xFF61\x3003\x3008\x3009\x300A\x300B\x300C\xFF62\x300D\xFF63\
             \\x300E\x300F\x3010\x3011\x3014\x3015\x301C\x30FB\xFF07\xFF3C\
             \\xFF65"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("jgo", LanguageData
      { language = "jgo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x66\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6D\x300\x6D\x304\x6E\x6E\x304\x70\x70\x66\x73\x73\x68\
             \\x74\x74\x73\x75\x76\x77\x79\x7A\xE1\xE2\xED\
             \\xEE\xFA\xFB\x144\x14B\x14B\x300\x14B\x301\x14B\x304\x1CE\x1D0\
             \\x1D4\x1F9\x254\x254\x301\x254\x302\x254\x30C\x25B\x25B\x300\x25B\x301\x25B\x302\
             \\x25B\x304\x25B\x30C\x289\x289\x301\x289\x302\x289\x308\x289\x30C\x1E3F\x1E85\xA78C"
          , _exAuxiliary = Just $ Set.fromList
             "\x65\x6F\x71\x72\x78"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x2C\x2D\x2E\x3A\x3B\x3F\xAB\xBB\x2011\
             \\x2039\x203A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("jmc", LanguageData
      { language = "jmc"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("jv", LanguageData
      { language = "jv"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x77\
             \\x79\xE2\xE5\xE8\xE9\xEA\xEC\xF2\xF9"
          , _exAuxiliary = Just $ Set.fromList
             "\x66\x71\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ka", LanguageData
      { language = "ka"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x10D0\x10D1\x10D2\x10D3\x10D4\x10D5\x10D6\x10D7\x10D8\x10D9\
             \\x10DA\x10DB\x10DC\x10DD\x10DE\x10DF\x10E0\x10E1\x10E2\x10E3\
             \\x10E4\x10E5\x10E6\x10E7\x10E8\x10E9\x10EA\x10EB\x10EC\x10ED\
             \\x10EE\x10EF\x10F0"
          , _exAuxiliary = Just $ Set.fromList
             "\x10F1\x10F2\x10F3\x10F4\x10F5\x10F6\x10F7\x10F8\x10F9\x10FA\
             \\x2D00\x2D01\x2D02\x2D03\x2D04\x2D05\x2D06\x2D07\x2D08\x2D09\
             \\x2D0A\x2D0B\x2D0C\x2D0D\x2D0E\x2D0F\x2D10\x2D11\x2D12\x2D13\
             \\x2D14\x2D15\x2D16\x2D17\x2D18\x2D19\x2D1A\x2D1B\x2D1C\x2D1D\
             \\x2D1E\x2D1F\x2D20\x2D21\x2D22\x2D23\x2D24\x2D25"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x23\x26\x27\x28\x29\x2A\x2C\x2D\x2E\
             \\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\xA7\
             \\xAB\xBB\x10FB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2020\x2021\x2026\x2032\x2033\x2116"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("kab", LanguageData
      { language = "kab"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x70\x71\x72\x73\x74\x75\
             \\x77\x78\x79\x7A\x10D\x1E7\x25B\x263\x1E0D\x1E25\
             \\x1E5B\x1E63\x1E6D\x1E93"
          , _exAuxiliary = Just $ Set.fromList
             "\x6F\x76"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("kam", LanguageData
      { language = "kam"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x79\x7A\x129\x169"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("kde", LanguageData
      { language = "kde"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("kea", LanguageData
      { language = "kea"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x64\x6A\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6C\x68\x6D\x6E\x6E\x68\x6F\x70\x72\x73\
             \\x74\x74\x78\x75\x76\x78\x79\x7A\xF1"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x6E\x308\x71\x72\x72\x77\xAA\xBA\xE0\xE1\xE2\
             \\xE3\xE4\xE5\xE6\xE7\xE8\xE9\xEA\xEB\xEC\
             \\xED\xEE\xEF\xF2\xF3\xF4\xF5\xF6\xF8\xF9\
             \\xFA\xFB\xFC\xFF\x101\x103\x113\x115\x129\x12B\
             \\x12D\x14D\x14F\x153\x169\x16B\x16D\x1EBD"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\xAB\
             \\xBB\x2010\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\
             \\x2021\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("khq", LanguageData
      { language = "khq"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x77\x78\x79\x7A\xE3\xF5\x14B\x161\x17E\
             \\x272\x1EBD"
          , _exAuxiliary = Just $ Set.fromList
             "\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ki", LanguageData
      { language = "ki"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x67\x68\x69\x6A\x6B\
             \\x6D\x6E\x6F\x72\x74\x75\x77\x79\x129\x169"
          , _exAuxiliary = Just $ Set.fromList
             "\x66\x6C\x70\x71\x73\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("kk", LanguageData
      { language = "kk"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x446\x447\x448\x449\x44A\x44B\x44C\x44D\
             \\x44E\x44F\x451\x456\x493\x49B\x4A3\x4AF\x4B1\x4BB\
             \\x4D9\x4E9"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x2019\x201C\
             \\x201D\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("kkj", LanguageData
      { language = "kkj"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x61\x327\x62\x63\x64\x65\x66\x67\x67\x62\x67\x77\
             \\x68\x69\x69\x327\x6A\x6B\x6B\x70\x6B\x77\x6C\x6D\x6D\x62\
             \\x6E\x6E\x64\x6E\x79\x6F\x70\x72\x73\x74\x75\x75\x327\
             \\x76\x77\x79\xE0\xE1\xE2\xE8\xE9\xEA\xEC\
             \\xED\xEE\xF2\xF3\xF4\xF9\xFA\xFB\x14B\x14B\x67\
             \\x14B\x67\x62\x14B\x67\x77\x1CC\x253\x254\x254\x300\x254\x301\x254\x302\x254\x327\x257\
             \\x257\x79\x25B\x25B\x300\x25B\x301\x25B\x302\x25B\x327"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78\x7A"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2A\x2C\x2E\x3A\x3F\xAB\xBB\
             \\x2018\x201C\x201D\x2026\x2039\x203A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("kl", LanguageData
      { language = "kl"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE5\xE6\xF8"
          , _exAuxiliary = Just $ Set.fromList
             "\xE1\xE2\xE3\xE9\xEA\xED\xEE\xF4\xF5\xFA\
             \\xFB\x129\x138\x169\x1EBD"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2030\x2212"
          }
      }
    )
  , ("kln", LanguageData
      { language = "kln"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x77\
             \\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x66\x71\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("km", LanguageData
      { language = "km"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1780\x1781\x1782\x1783\x1784\x1785\x1786\x1787\x1788\x1789\
             \\x178A\x178B\x178C\x178D\x178E\x178F\x1790\x1791\x1792\x1793\
             \\x1794\x1795\x1796\x1797\x1798\x1799\x179A\x179B\x179C\x179F\
             \\x17A0\x17A1\x17A2\x17A2\x17B6\x17A5\x17A6\x17A7\x17A7\x1780\x17A9\x17AA\
             \\x17AB\x17AC\x17AD\x17AE\x17AF\x17B0\x17B1\x17B2\x17B3\x17B6\
             \\x17B7\x17B8\x17B9\x17BA\x17BB\x17BC\x17BD\x17BE\x17BF\x17C0\
             \\x17C1\x17C2\x17C3\x17C4\x17C5\x17C6\x17C7\x17C8\x17C9\x17CA\
             \\x17CB\x17CD\x17D0\x17D2"
          , _exAuxiliary = Just $ Set.fromList
             "\x179D\x179E\x17B4\x17B5\x200B\x17CC\x17CE\x17CF\x17D1"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x28\x29\x2C\x2D\x2E\x3F\x5B\x5D\
             \\x7B\x7D\x17D4\x17D5\x17D6\x17D9\x17DA\x2011\x2018\x2019\
             \\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("kn", LanguageData
      { language = "kn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xC82\xC83\xC85\xC86\xC87\xC88\xC89\xC8A\xC8B\xC8C\
             \\xC8E\xC8F\xC90\xC92\xC93\xC94\xC95\xC96\xC97\xC98\
             \\xC99\xC9A\xC9B\xC9C\xC9D\xC9E\xC9F\xCA0\xCA1\xCA2\
             \\xCA3\xCA4\xCA5\xCA6\xCA7\xCA8\xCAA\xCAB\xCAC\xCAD\
             \\xCAE\xCAF\xCB0\xCB1\xCB2\xCB3\xCB5\xCB6\xCB7\xCB8\
             \\xCB9\xCBC\xCBD\xCBE\xCBF\xCC0\xCC1\xCC2\xCC3\xCC4\
             \\xCC6\xCC7\xCC8\xCCA\xCCB\xCCC\xCCD\xCD5\xCD6\xCE0\
             \\xCE1\xCE6\xCE7\xCE8\xCE9\xCEA\xCEB\xCEC\xCED\xCEE\
             \\xCEF"
          , _exAuxiliary = Just $ Set.fromList
             "\xCDE\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x2010\x2011\
             \\x2013\x2014\x2018\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xCE6\x31\xCE7\x32\xCE8\x33\xCE9\x34\xCEA\
             \\x35\xCEB\x36\xCEC\x37\xCED\x38\xCEE\x39\xCEF\x2011\x2030"
          }
      }
    )
  , ("ko", LanguageData
      { language = "ko"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xAC00\xAC01\xAC02\xAC03\xAC04\xAC05\xAC06\xAC07\xAC08\xAC09\
             \\xAC0A\xAC0B\xAC0C\xAC0D\xAC0E\xAC0F\xAC10\xAC11\xAC12\xAC13\
             \\xAC14\xAC15\xAC16\xAC17\xAC18\xAC19\xAC1A\xAC1B\xAC1C\xAC1D\
             \\xAC1E\xAC1F\xAC20\xAC21\xAC22\xAC23\xAC24\xAC25\xAC26\xAC27\
             \\xAC28\xAC29\xAC2A\xAC2B\xAC2C\xAC2D\xAC2E\xAC2F\xAC30\xAC31\
             \\xAC32\xAC33\xAC34\xAC35\xAC36\xAC37\xAC38\xAC39\xAC3A\xAC3B\
             \\xAC3C\xAC3D\xAC3E\xAC3F\xAC40\xAC41\xAC42\xAC43\xAC44\xAC45\
             \\xAC46\xAC47\xAC48\xAC49\xAC4A\xAC4B\xAC4C\xAC4D\xAC4E\xAC4F\
             \\xAC50\xAC51\xAC52\xAC53\xAC54\xAC55\xAC56\xAC57\xAC58\xAC59\
             \\xAC5A\xAC5B\xAC5C\xAC5D\xAC5E\xAC5F\xAC60\xAC61\xAC62\xAC63\
             \\xAC64\xAC65\xAC66\xAC67\xAC68\xAC69\xAC6A\xAC6B\xAC6C\xAC6D\
             \\xAC6E\xAC6F\xAC70\xAC71\xAC72\xAC73\xAC74\xAC75\xAC76\xAC77\
             \\xAC78\xAC79\xAC7A\xAC7B\xAC7C\xAC7D\xAC7E\xAC7F\xAC80\xAC81\
             \\xAC82\xAC83\xAC84\xAC85\xAC86\xAC87\xAC88\xAC89\xAC8A\xAC8B\
             \\xAC8C\xAC8D\xAC8E\xAC8F\xAC90\xAC91\xAC92\xAC93\xAC94\xAC95\
             \\xAC96\xAC97\xAC98\xAC99\xAC9A\xAC9B\xAC9C\xAC9D\xAC9E\xAC9F\
             \\xACA0\xACA1\xACA2\xACA3\xACA4\xACA5\xACA6\xACA7\xACA8\xACA9\
             \\xACAA\xACAB\xACAC\xACAD\xACAE\xACAF\xACB0\xACB1\xACB2\xACB3\
             \\xACB4\xACB5\xACB6\xACB7\xACB8\xACB9\xACBA\xACBB\xACBC\xACBD\
             \\xACBE\xACBF\xACC0\xACC1\xACC2\xACC3\xACC4\xACC5\xACC6\xACC7\
             \\xACC8\xACC9\xACCA\xACCB\xACCC\xACCD\xACCE\xACCF\xACD0\xACD1\
             \\xACD2\xACD3\xACD4\xACD5\xACD6\xACD7\xACD8\xACD9\xACDA\xACDB\
             \\xACDC\xACDD\xACDE\xACDF\xACE0\xACE1\xACE2\xACE3\xACE4\xACE5\
             \\xACE6\xACE7\xACE8\xACE9\xACEA\xACEB\xACEC\xACED\xACEE\xACEF\
             \\xACF0\xACF1\xACF2\xACF3\xACF4\xACF5\xACF6\xACF7\xACF8\xACF9\
             \\xACFA\xACFB\xACFC\xACFD\xACFE\xACFF\xAD00\xAD01\xAD02\xAD03\
             \\xAD04\xAD05\xAD06\xAD07\xAD08\xAD09\xAD0A\xAD0B\xAD0C\xAD0D\
             \\xAD0E\xAD0F\xAD10\xAD11\xAD12\xAD13\xAD14\xAD15\xAD16\xAD17\
             \\xAD18\xAD19\xAD1A\xAD1B\xAD1C\xAD1D\xAD1E\xAD1F\xAD20\xAD21\
             \\xAD22\xAD23\xAD24\xAD25\xAD26\xAD27\xAD28\xAD29\xAD2A\xAD2B\
             \\xAD2C\xAD2D\xAD2E\xAD2F\xAD30\xAD31\xAD32\xAD33\xAD34\xAD35\
             \\xAD36\xAD37\xAD38\xAD39\xAD3A\xAD3B\xAD3C\xAD3D\xAD3E\xAD3F\
             \\xAD40\xAD41\xAD42\xAD43\xAD44\xAD45\xAD46\xAD47\xAD48\xAD49\
             \\xAD4A\xAD4B\xAD4C\xAD4D\xAD4E\xAD4F\xAD50\xAD51\xAD52\xAD53\
             \\xAD54\xAD55\xAD56\xAD57\xAD58\xAD59\xAD5A\xAD5B\xAD5C\xAD5D\
             \\xAD5E\xAD5F\xAD60\xAD61\xAD62\xAD63\xAD64\xAD65\xAD66\xAD67\
             \\xAD68\xAD69\xAD6A\xAD6B\xAD6C\xAD6D\xAD6E\xAD6F\xAD70\xAD71\
             \\xAD72\xAD73\xAD74\xAD75\xAD76\xAD77\xAD78\xAD79\xAD7A\xAD7B\
             \\xAD7C\xAD7D\xAD7E\xAD7F\xAD80\xAD81\xAD82\xAD83\xAD84\xAD85\
             \\xAD86\xAD87\xAD88\xAD89\xAD8A\xAD8B\xAD8C\xAD8D\xAD8E\xAD8F\
             \\xAD90\xAD91\xAD92\xAD93\xAD94\xAD95\xAD96\xAD97\xAD98\xAD99\
             \\xAD9A\xAD9B\xAD9C\xAD9D\xAD9E\xAD9F\xADA0\xADA1\xADA2\xADA3\
             \\xADA4\xADA5\xADA6\xADA7\xADA8\xADA9\xADAA\xADAB\xADAC\xADAD\
             \\xADAE\xADAF\xADB0\xADB1\xADB2\xADB3\xADB4\xADB5\xADB6\xADB7\
             \\xADB8\xADB9\xADBA\xADBB\xADBC\xADBD\xADBE\xADBF\xADC0\xADC1\
             \\xADC2\xADC3\xADC4\xADC5\xADC6\xADC7\xADC8\xADC9\xADCA\xADCB\
             \\xADCC\xADCD\xADCE\xADCF\xADD0\xADD1\xADD2\xADD3\xADD4\xADD5\
             \\xADD6\xADD7\xADD8\xADD9\xADDA\xADDB\xADDC\xADDD\xADDE\xADDF\
             \\xADE0\xADE1\xADE2\xADE3\xADE4\xADE5\xADE6\xADE7\xADE8\xADE9\
             \\xADEA\xADEB\xADEC\xADED\xADEE\xADEF\xADF0\xADF1\xADF2\xADF3\
             \\xADF4\xADF5\xADF6\xADF7\xADF8\xADF9\xADFA\xADFB\xADFC\xADFD\
             \\xADFE\xADFF\xAE00\xAE01\xAE02\xAE03\xAE04\xAE05\xAE06\xAE07\
             \\xAE08\xAE09\xAE0A\xAE0B\xAE0C\xAE0D\xAE0E\xAE0F\xAE10\xAE11\
             \\xAE12\xAE13\xAE14\xAE15\xAE16\xAE17\xAE18\xAE19\xAE1A\xAE1B\
             \\xAE1C\xAE1D\xAE1E\xAE1F\xAE20\xAE21\xAE22\xAE23\xAE24\xAE25\
             \\xAE26\xAE27\xAE28\xAE29\xAE2A\xAE2B\xAE2C\xAE2D\xAE2E\xAE2F\
             \\xAE30\xAE31\xAE32\xAE33\xAE34\xAE35\xAE36\xAE37\xAE38\xAE39\
             \\xAE3A\xAE3B\xAE3C\xAE3D\xAE3E\xAE3F\xAE40\xAE41\xAE42\xAE43\
             \\xAE44\xAE45\xAE46\xAE47\xAE48\xAE49\xAE4A\xAE4B\xAE4C\xAE4D\
             \\xAE4E\xAE4F\xAE50\xAE51\xAE52\xAE53\xAE54\xAE55\xAE56\xAE57\
             \\xAE58\xAE59\xAE5A\xAE5B\xAE5C\xAE5D\xAE5E\xAE5F\xAE60\xAE61\
             \\xAE62\xAE63\xAE64\xAE65\xAE66\xAE67\xAE68\xAE69\xAE6A\xAE6B\
             \\xAE6C\xAE6D\xAE6E\xAE6F\xAE70\xAE71\xAE72\xAE73\xAE74\xAE75\
             \\xAE76\xAE77\xAE78\xAE79\xAE7A\xAE7B\xAE7C\xAE7D\xAE7E\xAE7F\
             \\xAE80\xAE81\xAE82\xAE83\xAE84\xAE85\xAE86\xAE87\xAE88\xAE89\
             \\xAE8A\xAE8B\xAE8C\xAE8D\xAE8E\xAE8F\xAE90\xAE91\xAE92\xAE93\
             \\xAE94\xAE95\xAE96\xAE97\xAE98\xAE99\xAE9A\xAE9B\xAE9C\xAE9D\
             \\xAE9E\xAE9F\xAEA0\xAEA1\xAEA2\xAEA3\xAEA4\xAEA5\xAEA6\xAEA7\
             \\xAEA8\xAEA9\xAEAA\xAEAB\xAEAC\xAEAD\xAEAE\xAEAF\xAEB0\xAEB1\
             \\xAEB2\xAEB3\xAEB4\xAEB5\xAEB6\xAEB7\xAEB8\xAEB9\xAEBA\xAEBB\
             \\xAEBC\xAEBD\xAEBE\xAEBF\xAEC0\xAEC1\xAEC2\xAEC3\xAEC4\xAEC5\
             \\xAEC6\xAEC7\xAEC8\xAEC9\xAECA\xAECB\xAECC\xAECD\xAECE\xAECF\
             \\xAED0\xAED1\xAED2\xAED3\xAED4\xAED5\xAED6\xAED7\xAED8\xAED9\
             \\xAEDA\xAEDB\xAEDC\xAEDD\xAEDE\xAEDF\xAEE0\xAEE1\xAEE2\xAEE3\
             \\xAEE4\xAEE5\xAEE6\xAEE7\xAEE8\xAEE9\xAEEA\xAEEB\xAEEC\xAEED\
             \\xAEEE\xAEEF\xAEF0\xAEF1\xAEF2\xAEF3\xAEF4\xAEF5\xAEF6\xAEF7\
             \\xAEF8\xAEF9\xAEFA\xAEFB\xAEFC\xAEFD\xAEFE\xAEFF\xAF00\xAF01\
             \\xAF02\xAF03\xAF04\xAF05\xAF06\xAF07\xAF08\xAF09\xAF0A\xAF0B\
             \\xAF0C\xAF0D\xAF0E\xAF0F\xAF10\xAF11\xAF12\xAF13\xAF14\xAF15\
             \\xAF16\xAF17\xAF18\xAF19\xAF1A\xAF1B\xAF1C\xAF1D\xAF1E\xAF1F\
             \\xAF20\xAF21\xAF22\xAF23\xAF24\xAF25\xAF26\xAF27\xAF28\xAF29\
             \\xAF2A\xAF2B\xAF2C\xAF2D\xAF2E\xAF2F\xAF30\xAF31\xAF32\xAF33\
             \\xAF34\xAF35\xAF36\xAF37\xAF38\xAF39\xAF3A\xAF3B\xAF3C\xAF3D\
             \\xAF3E\xAF3F\xAF40\xAF41\xAF42\xAF43\xAF44\xAF45\xAF46\xAF47\
             \\xAF48\xAF49\xAF4A\xAF4B\xAF4C\xAF4D\xAF4E\xAF4F\xAF50\xAF51\
             \\xAF52\xAF53\xAF54\xAF55\xAF56\xAF57\xAF58\xAF59\xAF5A\xAF5B\
             \\xAF5C\xAF5D\xAF5E\xAF5F\xAF60\xAF61\xAF62\xAF63\xAF64\xAF65\
             \\xAF66\xAF67\xAF68\xAF69\xAF6A\xAF6B\xAF6C\xAF6D\xAF6E\xAF6F\
             \\xAF70\xAF71\xAF72\xAF73\xAF74\xAF75\xAF76\xAF77\xAF78\xAF79\
             \\xAF7A\xAF7B\xAF7C\xAF7D\xAF7E\xAF7F\xAF80\xAF81\xAF82\xAF83\
             \\xAF84\xAF85\xAF86\xAF87\xAF88\xAF89\xAF8A\xAF8B\xAF8C\xAF8D\
             \\xAF8E\xAF8F\xAF90\xAF91\xAF92\xAF93\xAF94\xAF95\xAF96\xAF97\
             \\xAF98\xAF99\xAF9A\xAF9B\xAF9C\xAF9D\xAF9E\xAF9F\xAFA0\xAFA1\
             \\xAFA2\xAFA3\xAFA4\xAFA5\xAFA6\xAFA7\xAFA8\xAFA9\xAFAA\xAFAB\
             \\xAFAC\xAFAD\xAFAE\xAFAF\xAFB0\xAFB1\xAFB2\xAFB3\xAFB4\xAFB5\
             \\xAFB6\xAFB7\xAFB8\xAFB9\xAFBA\xAFBB\xAFBC\xAFBD\xAFBE\xAFBF\
             \\xAFC0\xAFC1\xAFC2\xAFC3\xAFC4\xAFC5\xAFC6\xAFC7\xAFC8\xAFC9\
             \\xAFCA\xAFCB\xAFCC\xAFCD\xAFCE\xAFCF\xAFD0\xAFD1\xAFD2\xAFD3\
             \\xAFD4\xAFD5\xAFD6\xAFD7\xAFD8\xAFD9\xAFDA\xAFDB\xAFDC\xAFDD\
             \\xAFDE\xAFDF\xAFE0\xAFE1\xAFE2\xAFE3\xAFE4\xAFE5\xAFE6\xAFE7\
             \\xAFE8\xAFE9\xAFEA\xAFEB\xAFEC\xAFED\xAFEE\xAFEF\xAFF0\xAFF1\
             \\xAFF2\xAFF3\xAFF4\xAFF5\xAFF6\xAFF7\xAFF8\xAFF9\xAFFA\xAFFB\
             \\xAFFC\xAFFD\xAFFE\xAFFF\xB000\xB001\xB002\xB003\xB004\xB005\
             \\xB006\xB007\xB008\xB009\xB00A\xB00B\xB00C\xB00D\xB00E\xB00F\
             \\xB010\xB011\xB012\xB013\xB014\xB015\xB016\xB017\xB018\xB019\
             \\xB01A\xB01B\xB01C\xB01D\xB01E\xB01F\xB020\xB021\xB022\xB023\
             \\xB024\xB025\xB026\xB027\xB028\xB029\xB02A\xB02B\xB02C\xB02D\
             \\xB02E\xB02F\xB030\xB031\xB032\xB033\xB034\xB035\xB036\xB037\
             \\xB038\xB039\xB03A\xB03B\xB03C\xB03D\xB03E\xB03F\xB040\xB041\
             \\xB042\xB043\xB044\xB045\xB046\xB047\xB048\xB049\xB04A\xB04B\
             \\xB04C\xB04D\xB04E\xB04F\xB050\xB051\xB052\xB053\xB054\xB055\
             \\xB056\xB057\xB058\xB059\xB05A\xB05B\xB05C\xB05D\xB05E\xB05F\
             \\xB060\xB061\xB062\xB063\xB064\xB065\xB066\xB067\xB068\xB069\
             \\xB06A\xB06B\xB06C\xB06D\xB06E\xB06F\xB070\xB071\xB072\xB073\
             \\xB074\xB075\xB076\xB077\xB078\xB079\xB07A\xB07B\xB07C\xB07D\
             \\xB07E\xB07F\xB080\xB081\xB082\xB083\xB084\xB085\xB086\xB087\
             \\xB088\xB089\xB08A\xB08B\xB08C\xB08D\xB08E\xB08F\xB090\xB091\
             \\xB092\xB093\xB094\xB095\xB096\xB097\xB098\xB099\xB09A\xB09B\
             \\xB09C\xB09D\xB09E\xB09F\xB0A0\xB0A1\xB0A2\xB0A3\xB0A4\xB0A5\
             \\xB0A6\xB0A7\xB0A8\xB0A9\xB0AA\xB0AB\xB0AC\xB0AD\xB0AE\xB0AF\
             \\xB0B0\xB0B1\xB0B2\xB0B3\xB0B4\xB0B5\xB0B6\xB0B7\xB0B8\xB0B9\
             \\xB0BA\xB0BB\xB0BC\xB0BD\xB0BE\xB0BF\xB0C0\xB0C1\xB0C2\xB0C3\
             \\xB0C4\xB0C5\xB0C6\xB0C7\xB0C8\xB0C9\xB0CA\xB0CB\xB0CC\xB0CD\
             \\xB0CE\xB0CF\xB0D0\xB0D1\xB0D2\xB0D3\xB0D4\xB0D5\xB0D6\xB0D7\
             \\xB0D8\xB0D9\xB0DA\xB0DB\xB0DC\xB0DD\xB0DE\xB0DF\xB0E0\xB0E1\
             \\xB0E2\xB0E3\xB0E4\xB0E5\xB0E6\xB0E7\xB0E8\xB0E9\xB0EA\xB0EB\
             \\xB0EC\xB0ED\xB0EE\xB0EF\xB0F0\xB0F1\xB0F2\xB0F3\xB0F4\xB0F5\
             \\xB0F6\xB0F7\xB0F8\xB0F9\xB0FA\xB0FB\xB0FC\xB0FD\xB0FE\xB0FF\
             \\xB100\xB101\xB102\xB103\xB104\xB105\xB106\xB107\xB108\xB109\
             \\xB10A\xB10B\xB10C\xB10D\xB10E\xB10F\xB110\xB111\xB112\xB113\
             \\xB114\xB115\xB116\xB117\xB118\xB119\xB11A\xB11B\xB11C\xB11D\
             \\xB11E\xB11F\xB120\xB121\xB122\xB123\xB124\xB125\xB126\xB127\
             \\xB128\xB129\xB12A\xB12B\xB12C\xB12D\xB12E\xB12F\xB130\xB131\
             \\xB132\xB133\xB134\xB135\xB136\xB137\xB138\xB139\xB13A\xB13B\
             \\xB13C\xB13D\xB13E\xB13F\xB140\xB141\xB142\xB143\xB144\xB145\
             \\xB146\xB147\xB148\xB149\xB14A\xB14B\xB14C\xB14D\xB14E\xB14F\
             \\xB150\xB151\xB152\xB153\xB154\xB155\xB156\xB157\xB158\xB159\
             \\xB15A\xB15B\xB15C\xB15D\xB15E\xB15F\xB160\xB161\xB162\xB163\
             \\xB164\xB165\xB166\xB167\xB168\xB169\xB16A\xB16B\xB16C\xB16D\
             \\xB16E\xB16F\xB170\xB171\xB172\xB173\xB174\xB175\xB176\xB177\
             \\xB178\xB179\xB17A\xB17B\xB17C\xB17D\xB17E\xB17F\xB180\xB181\
             \\xB182\xB183\xB184\xB185\xB186\xB187\xB188\xB189\xB18A\xB18B\
             \\xB18C\xB18D\xB18E\xB18F\xB190\xB191\xB192\xB193\xB194\xB195\
             \\xB196\xB197\xB198\xB199\xB19A\xB19B\xB19C\xB19D\xB19E\xB19F\
             \\xB1A0\xB1A1\xB1A2\xB1A3\xB1A4\xB1A5\xB1A6\xB1A7\xB1A8\xB1A9\
             \\xB1AA\xB1AB\xB1AC\xB1AD\xB1AE\xB1AF\xB1B0\xB1B1\xB1B2\xB1B3\
             \\xB1B4\xB1B5\xB1B6\xB1B7\xB1B8\xB1B9\xB1BA\xB1BB\xB1BC\xB1BD\
             \\xB1BE\xB1BF\xB1C0\xB1C1\xB1C2\xB1C3\xB1C4\xB1C5\xB1C6\xB1C7\
             \\xB1C8\xB1C9\xB1CA\xB1CB\xB1CC\xB1CD\xB1CE\xB1CF\xB1D0\xB1D1\
             \\xB1D2\xB1D3\xB1D4\xB1D5\xB1D6\xB1D7\xB1D8\xB1D9\xB1DA\xB1DB\
             \\xB1DC\xB1DD\xB1DE\xB1DF\xB1E0\xB1E1\xB1E2\xB1E3\xB1E4\xB1E5\
             \\xB1E6\xB1E7\xB1E8\xB1E9\xB1EA\xB1EB\xB1EC\xB1ED\xB1EE\xB1EF\
             \\xB1F0\xB1F1\xB1F2\xB1F3\xB1F4\xB1F5\xB1F6\xB1F7\xB1F8\xB1F9\
             \\xB1FA\xB1FB\xB1FC\xB1FD\xB1FE\xB1FF\xB200\xB201\xB202\xB203\
             \\xB204\xB205\xB206\xB207\xB208\xB209\xB20A\xB20B\xB20C\xB20D\
             \\xB20E\xB20F\xB210\xB211\xB212\xB213\xB214\xB215\xB216\xB217\
             \\xB218\xB219\xB21A\xB21B\xB21C\xB21D\xB21E\xB21F\xB220\xB221\
             \\xB222\xB223\xB224\xB225\xB226\xB227\xB228\xB229\xB22A\xB22B\
             \\xB22C\xB22D\xB22E\xB22F\xB230\xB231\xB232\xB233\xB234\xB235\
             \\xB236\xB237\xB238\xB239\xB23A\xB23B\xB23C\xB23D\xB23E\xB23F\
             \\xB240\xB241\xB242\xB243\xB244\xB245\xB246\xB247\xB248\xB249\
             \\xB24A\xB24B\xB24C\xB24D\xB24E\xB24F\xB250\xB251\xB252\xB253\
             \\xB254\xB255\xB256\xB257\xB258\xB259\xB25A\xB25B\xB25C\xB25D\
             \\xB25E\xB25F\xB260\xB261\xB262\xB263\xB264\xB265\xB266\xB267\
             \\xB268\xB269\xB26A\xB26B\xB26C\xB26D\xB26E\xB26F\xB270\xB271\
             \\xB272\xB273\xB274\xB275\xB276\xB277\xB278\xB279\xB27A\xB27B\
             \\xB27C\xB27D\xB27E\xB27F\xB280\xB281\xB282\xB283\xB284\xB285\
             \\xB286\xB287\xB288\xB289\xB28A\xB28B\xB28C\xB28D\xB28E\xB28F\
             \\xB290\xB291\xB292\xB293\xB294\xB295\xB296\xB297\xB298\xB299\
             \\xB29A\xB29B\xB29C\xB29D\xB29E\xB29F\xB2A0\xB2A1\xB2A2\xB2A3\
             \\xB2A4\xB2A5\xB2A6\xB2A7\xB2A8\xB2A9\xB2AA\xB2AB\xB2AC\xB2AD\
             \\xB2AE\xB2AF\xB2B0\xB2B1\xB2B2\xB2B3\xB2B4\xB2B5\xB2B6\xB2B7\
             \\xB2B8\xB2B9\xB2BA\xB2BB\xB2BC\xB2BD\xB2BE\xB2BF\xB2C0\xB2C1\
             \\xB2C2\xB2C3\xB2C4\xB2C5\xB2C6\xB2C7\xB2C8\xB2C9\xB2CA\xB2CB\
             \\xB2CC\xB2CD\xB2CE\xB2CF\xB2D0\xB2D1\xB2D2\xB2D3\xB2D4\xB2D5\
             \\xB2D6\xB2D7\xB2D8\xB2D9\xB2DA\xB2DB\xB2DC\xB2DD\xB2DE\xB2DF\
             \\xB2E0\xB2E1\xB2E2\xB2E3\xB2E4\xB2E5\xB2E6\xB2E7\xB2E8\xB2E9\
             \\xB2EA\xB2EB\xB2EC\xB2ED\xB2EE\xB2EF\xB2F0\xB2F1\xB2F2\xB2F3\
             \\xB2F4\xB2F5\xB2F6\xB2F7\xB2F8\xB2F9\xB2FA\xB2FB\xB2FC\xB2FD\
             \\xB2FE\xB2FF\xB300\xB301\xB302\xB303\xB304\xB305\xB306\xB307\
             \\xB308\xB309\xB30A\xB30B\xB30C\xB30D\xB30E\xB30F\xB310\xB311\
             \\xB312\xB313\xB314\xB315\xB316\xB317\xB318\xB319\xB31A\xB31B\
             \\xB31C\xB31D\xB31E\xB31F\xB320\xB321\xB322\xB323\xB324\xB325\
             \\xB326\xB327\xB328\xB329\xB32A\xB32B\xB32C\xB32D\xB32E\xB32F\
             \\xB330\xB331\xB332\xB333\xB334\xB335\xB336\xB337\xB338\xB339\
             \\xB33A\xB33B\xB33C\xB33D\xB33E\xB33F\xB340\xB341\xB342\xB343\
             \\xB344\xB345\xB346\xB347\xB348\xB349\xB34A\xB34B\xB34C\xB34D\
             \\xB34E\xB34F\xB350\xB351\xB352\xB353\xB354\xB355\xB356\xB357\
             \\xB358\xB359\xB35A\xB35B\xB35C\xB35D\xB35E\xB35F\xB360\xB361\
             \\xB362\xB363\xB364\xB365\xB366\xB367\xB368\xB369\xB36A\xB36B\
             \\xB36C\xB36D\xB36E\xB36F\xB370\xB371\xB372\xB373\xB374\xB375\
             \\xB376\xB377\xB378\xB379\xB37A\xB37B\xB37C\xB37D\xB37E\xB37F\
             \\xB380\xB381\xB382\xB383\xB384\xB385\xB386\xB387\xB388\xB389\
             \\xB38A\xB38B\xB38C\xB38D\xB38E\xB38F\xB390\xB391\xB392\xB393\
             \\xB394\xB395\xB396\xB397\xB398\xB399\xB39A\xB39B\xB39C\xB39D\
             \\xB39E\xB39F\xB3A0\xB3A1\xB3A2\xB3A3\xB3A4\xB3A5\xB3A6\xB3A7\
             \\xB3A8\xB3A9\xB3AA\xB3AB\xB3AC\xB3AD\xB3AE\xB3AF\xB3B0\xB3B1\
             \\xB3B2\xB3B3\xB3B4\xB3B5\xB3B6\xB3B7\xB3B8\xB3B9\xB3BA\xB3BB\
             \\xB3BC\xB3BD\xB3BE\xB3BF\xB3C0\xB3C1\xB3C2\xB3C3\xB3C4\xB3C5\
             \\xB3C6\xB3C7\xB3C8\xB3C9\xB3CA\xB3CB\xB3CC\xB3CD\xB3CE\xB3CF\
             \\xB3D0\xB3D1\xB3D2\xB3D3\xB3D4\xB3D5\xB3D6\xB3D7\xB3D8\xB3D9\
             \\xB3DA\xB3DB\xB3DC\xB3DD\xB3DE\xB3DF\xB3E0\xB3E1\xB3E2\xB3E3\
             \\xB3E4\xB3E5\xB3E6\xB3E7\xB3E8\xB3E9\xB3EA\xB3EB\xB3EC\xB3ED\
             \\xB3EE\xB3EF\xB3F0\xB3F1\xB3F2\xB3F3\xB3F4\xB3F5\xB3F6\xB3F7\
             \\xB3F8\xB3F9\xB3FA\xB3FB\xB3FC\xB3FD\xB3FE\xB3FF\xB400\xB401\
             \\xB402\xB403\xB404\xB405\xB406\xB407\xB408\xB409\xB40A\xB40B\
             \\xB40C\xB40D\xB40E\xB40F\xB410\xB411\xB412\xB413\xB414\xB415\
             \\xB416\xB417\xB418\xB419\xB41A\xB41B\xB41C\xB41D\xB41E\xB41F\
             \\xB420\xB421\xB422\xB423\xB424\xB425\xB426\xB427\xB428\xB429\
             \\xB42A\xB42B\xB42C\xB42D\xB42E\xB42F\xB430\xB431\xB432\xB433\
             \\xB434\xB435\xB436\xB437\xB438\xB439\xB43A\xB43B\xB43C\xB43D\
             \\xB43E\xB43F\xB440\xB441\xB442\xB443\xB444\xB445\xB446\xB447\
             \\xB448\xB449\xB44A\xB44B\xB44C\xB44D\xB44E\xB44F\xB450\xB451\
             \\xB452\xB453\xB454\xB455\xB456\xB457\xB458\xB459\xB45A\xB45B\
             \\xB45C\xB45D\xB45E\xB45F\xB460\xB461\xB462\xB463\xB464\xB465\
             \\xB466\xB467\xB468\xB469\xB46A\xB46B\xB46C\xB46D\xB46E\xB46F\
             \\xB470\xB471\xB472\xB473\xB474\xB475\xB476\xB477\xB478\xB479\
             \\xB47A\xB47B\xB47C\xB47D\xB47E\xB47F\xB480\xB481\xB482\xB483\
             \\xB484\xB485\xB486\xB487\xB488\xB489\xB48A\xB48B\xB48C\xB48D\
             \\xB48E\xB48F\xB490\xB491\xB492\xB493\xB494\xB495\xB496\xB497\
             \\xB498\xB499\xB49A\xB49B\xB49C\xB49D\xB49E\xB49F\xB4A0\xB4A1\
             \\xB4A2\xB4A3\xB4A4\xB4A5\xB4A6\xB4A7\xB4A8\xB4A9\xB4AA\xB4AB\
             \\xB4AC\xB4AD\xB4AE\xB4AF\xB4B0\xB4B1\xB4B2\xB4B3\xB4B4\xB4B5\
             \\xB4B6\xB4B7\xB4B8\xB4B9\xB4BA\xB4BB\xB4BC\xB4BD\xB4BE\xB4BF\
             \\xB4C0\xB4C1\xB4C2\xB4C3\xB4C4\xB4C5\xB4C6\xB4C7\xB4C8\xB4C9\
             \\xB4CA\xB4CB\xB4CC\xB4CD\xB4CE\xB4CF\xB4D0\xB4D1\xB4D2\xB4D3\
             \\xB4D4\xB4D5\xB4D6\xB4D7\xB4D8\xB4D9\xB4DA\xB4DB\xB4DC\xB4DD\
             \\xB4DE\xB4DF\xB4E0\xB4E1\xB4E2\xB4E3\xB4E4\xB4E5\xB4E6\xB4E7\
             \\xB4E8\xB4E9\xB4EA\xB4EB\xB4EC\xB4ED\xB4EE\xB4EF\xB4F0\xB4F1\
             \\xB4F2\xB4F3\xB4F4\xB4F5\xB4F6\xB4F7\xB4F8\xB4F9\xB4FA\xB4FB\
             \\xB4FC\xB4FD\xB4FE\xB4FF\xB500\xB501\xB502\xB503\xB504\xB505\
             \\xB506\xB507\xB508\xB509\xB50A\xB50B\xB50C\xB50D\xB50E\xB50F\
             \\xB510\xB511\xB512\xB513\xB514\xB515\xB516\xB517\xB518\xB519\
             \\xB51A\xB51B\xB51C\xB51D\xB51E\xB51F\xB520\xB521\xB522\xB523\
             \\xB524\xB525\xB526\xB527\xB528\xB529\xB52A\xB52B\xB52C\xB52D\
             \\xB52E\xB52F\xB530\xB531\xB532\xB533\xB534\xB535\xB536\xB537\
             \\xB538\xB539\xB53A\xB53B\xB53C\xB53D\xB53E\xB53F\xB540\xB541\
             \\xB542\xB543\xB544\xB545\xB546\xB547\xB548\xB549\xB54A\xB54B\
             \\xB54C\xB54D\xB54E\xB54F\xB550\xB551\xB552\xB553\xB554\xB555\
             \\xB556\xB557\xB558\xB559\xB55A\xB55B\xB55C\xB55D\xB55E\xB55F\
             \\xB560\xB561\xB562\xB563\xB564\xB565\xB566\xB567\xB568\xB569\
             \\xB56A\xB56B\xB56C\xB56D\xB56E\xB56F\xB570\xB571\xB572\xB573\
             \\xB574\xB575\xB576\xB577\xB578\xB579\xB57A\xB57B\xB57C\xB57D\
             \\xB57E\xB57F\xB580\xB581\xB582\xB583\xB584\xB585\xB586\xB587\
             \\xB588\xB589\xB58A\xB58B\xB58C\xB58D\xB58E\xB58F\xB590\xB591\
             \\xB592\xB593\xB594\xB595\xB596\xB597\xB598\xB599\xB59A\xB59B\
             \\xB59C\xB59D\xB59E\xB59F\xB5A0\xB5A1\xB5A2\xB5A3\xB5A4\xB5A5\
             \\xB5A6\xB5A7\xB5A8\xB5A9\xB5AA\xB5AB\xB5AC\xB5AD\xB5AE\xB5AF\
             \\xB5B0\xB5B1\xB5B2\xB5B3\xB5B4\xB5B5\xB5B6\xB5B7\xB5B8\xB5B9\
             \\xB5BA\xB5BB\xB5BC\xB5BD\xB5BE\xB5BF\xB5C0\xB5C1\xB5C2\xB5C3\
             \\xB5C4\xB5C5\xB5C6\xB5C7\xB5C8\xB5C9\xB5CA\xB5CB\xB5CC\xB5CD\
             \\xB5CE\xB5CF\xB5D0\xB5D1\xB5D2\xB5D3\xB5D4\xB5D5\xB5D6\xB5D7\
             \\xB5D8\xB5D9\xB5DA\xB5DB\xB5DC\xB5DD\xB5DE\xB5DF\xB5E0\xB5E1\
             \\xB5E2\xB5E3\xB5E4\xB5E5\xB5E6\xB5E7\xB5E8\xB5E9\xB5EA\xB5EB\
             \\xB5EC\xB5ED\xB5EE\xB5EF\xB5F0\xB5F1\xB5F2\xB5F3\xB5F4\xB5F5\
             \\xB5F6\xB5F7\xB5F8\xB5F9\xB5FA\xB5FB\xB5FC\xB5FD\xB5FE\xB5FF\
             \\xB600\xB601\xB602\xB603\xB604\xB605\xB606\xB607\xB608\xB609\
             \\xB60A\xB60B\xB60C\xB60D\xB60E\xB60F\xB610\xB611\xB612\xB613\
             \\xB614\xB615\xB616\xB617\xB618\xB619\xB61A\xB61B\xB61C\xB61D\
             \\xB61E\xB61F\xB620\xB621\xB622\xB623\xB624\xB625\xB626\xB627\
             \\xB628\xB629\xB62A\xB62B\xB62C\xB62D\xB62E\xB62F\xB630\xB631\
             \\xB632\xB633\xB634\xB635\xB636\xB637\xB638\xB639\xB63A\xB63B\
             \\xB63C\xB63D\xB63E\xB63F\xB640\xB641\xB642\xB643\xB644\xB645\
             \\xB646\xB647\xB648\xB649\xB64A\xB64B\xB64C\xB64D\xB64E\xB64F\
             \\xB650\xB651\xB652\xB653\xB654\xB655\xB656\xB657\xB658\xB659\
             \\xB65A\xB65B\xB65C\xB65D\xB65E\xB65F\xB660\xB661\xB662\xB663\
             \\xB664\xB665\xB666\xB667\xB668\xB669\xB66A\xB66B\xB66C\xB66D\
             \\xB66E\xB66F\xB670\xB671\xB672\xB673\xB674\xB675\xB676\xB677\
             \\xB678\xB679\xB67A\xB67B\xB67C\xB67D\xB67E\xB67F\xB680\xB681\
             \\xB682\xB683\xB684\xB685\xB686\xB687\xB688\xB689\xB68A\xB68B\
             \\xB68C\xB68D\xB68E\xB68F\xB690\xB691\xB692\xB693\xB694\xB695\
             \\xB696\xB697\xB698\xB699\xB69A\xB69B\xB69C\xB69D\xB69E\xB69F\
             \\xB6A0\xB6A1\xB6A2\xB6A3\xB6A4\xB6A5\xB6A6\xB6A7\xB6A8\xB6A9\
             \\xB6AA\xB6AB\xB6AC\xB6AD\xB6AE\xB6AF\xB6B0\xB6B1\xB6B2\xB6B3\
             \\xB6B4\xB6B5\xB6B6\xB6B7\xB6B8\xB6B9\xB6BA\xB6BB\xB6BC\xB6BD\
             \\xB6BE\xB6BF\xB6C0\xB6C1\xB6C2\xB6C3\xB6C4\xB6C5\xB6C6\xB6C7\
             \\xB6C8\xB6C9\xB6CA\xB6CB\xB6CC\xB6CD\xB6CE\xB6CF\xB6D0\xB6D1\
             \\xB6D2\xB6D3\xB6D4\xB6D5\xB6D6\xB6D7\xB6D8\xB6D9\xB6DA\xB6DB\
             \\xB6DC\xB6DD\xB6DE\xB6DF\xB6E0\xB6E1\xB6E2\xB6E3\xB6E4\xB6E5\
             \\xB6E6\xB6E7\xB6E8\xB6E9\xB6EA\xB6EB\xB6EC\xB6ED\xB6EE\xB6EF\
             \\xB6F0\xB6F1\xB6F2\xB6F3\xB6F4\xB6F5\xB6F6\xB6F7\xB6F8\xB6F9\
             \\xB6FA\xB6FB\xB6FC\xB6FD\xB6FE\xB6FF\xB700\xB701\xB702\xB703\
             \\xB704\xB705\xB706\xB707\xB708\xB709\xB70A\xB70B\xB70C\xB70D\
             \\xB70E\xB70F\xB710\xB711\xB712\xB713\xB714\xB715\xB716\xB717\
             \\xB718\xB719\xB71A\xB71B\xB71C\xB71D\xB71E\xB71F\xB720\xB721\
             \\xB722\xB723\xB724\xB725\xB726\xB727\xB728\xB729\xB72A\xB72B\
             \\xB72C\xB72D\xB72E\xB72F\xB730\xB731\xB732\xB733\xB734\xB735\
             \\xB736\xB737\xB738\xB739\xB73A\xB73B\xB73C\xB73D\xB73E\xB73F\
             \\xB740\xB741\xB742\xB743\xB744\xB745\xB746\xB747\xB748\xB749\
             \\xB74A\xB74B\xB74C\xB74D\xB74E\xB74F\xB750\xB751\xB752\xB753\
             \\xB754\xB755\xB756\xB757\xB758\xB759\xB75A\xB75B\xB75C\xB75D\
             \\xB75E\xB75F\xB760\xB761\xB762\xB763\xB764\xB765\xB766\xB767\
             \\xB768\xB769\xB76A\xB76B\xB76C\xB76D\xB76E\xB76F\xB770\xB771\
             \\xB772\xB773\xB774\xB775\xB776\xB777\xB778\xB779\xB77A\xB77B\
             \\xB77C\xB77D\xB77E\xB77F\xB780\xB781\xB782\xB783\xB784\xB785\
             \\xB786\xB787\xB788\xB789\xB78A\xB78B\xB78C\xB78D\xB78E\xB78F\
             \\xB790\xB791\xB792\xB793\xB794\xB795\xB796\xB797\xB798\xB799\
             \\xB79A\xB79B\xB79C\xB79D\xB79E\xB79F\xB7A0\xB7A1\xB7A2\xB7A3\
             \\xB7A4\xB7A5\xB7A6\xB7A7\xB7A8\xB7A9\xB7AA\xB7AB\xB7AC\xB7AD\
             \\xB7AE\xB7AF\xB7B0\xB7B1\xB7B2\xB7B3\xB7B4\xB7B5\xB7B6\xB7B7\
             \\xB7B8\xB7B9\xB7BA\xB7BB\xB7BC\xB7BD\xB7BE\xB7BF\xB7C0\xB7C1\
             \\xB7C2\xB7C3\xB7C4\xB7C5\xB7C6\xB7C7\xB7C8\xB7C9\xB7CA\xB7CB\
             \\xB7CC\xB7CD\xB7CE\xB7CF\xB7D0\xB7D1\xB7D2\xB7D3\xB7D4\xB7D5\
             \\xB7D6\xB7D7\xB7D8\xB7D9\xB7DA\xB7DB\xB7DC\xB7DD\xB7DE\xB7DF\
             \\xB7E0\xB7E1\xB7E2\xB7E3\xB7E4\xB7E5\xB7E6\xB7E7\xB7E8\xB7E9\
             \\xB7EA\xB7EB\xB7EC\xB7ED\xB7EE\xB7EF\xB7F0\xB7F1\xB7F2\xB7F3\
             \\xB7F4\xB7F5\xB7F6\xB7F7\xB7F8\xB7F9\xB7FA\xB7FB\xB7FC\xB7FD\
             \\xB7FE\xB7FF\xB800\xB801\xB802\xB803\xB804\xB805\xB806\xB807\
             \\xB808\xB809\xB80A\xB80B\xB80C\xB80D\xB80E\xB80F\xB810\xB811\
             \\xB812\xB813\xB814\xB815\xB816\xB817\xB818\xB819\xB81A\xB81B\
             \\xB81C\xB81D\xB81E\xB81F\xB820\xB821\xB822\xB823\xB824\xB825\
             \\xB826\xB827\xB828\xB829\xB82A\xB82B\xB82C\xB82D\xB82E\xB82F\
             \\xB830\xB831\xB832\xB833\xB834\xB835\xB836\xB837\xB838\xB839\
             \\xB83A\xB83B\xB83C\xB83D\xB83E\xB83F\xB840\xB841\xB842\xB843\
             \\xB844\xB845\xB846\xB847\xB848\xB849\xB84A\xB84B\xB84C\xB84D\
             \\xB84E\xB84F\xB850\xB851\xB852\xB853\xB854\xB855\xB856\xB857\
             \\xB858\xB859\xB85A\xB85B\xB85C\xB85D\xB85E\xB85F\xB860\xB861\
             \\xB862\xB863\xB864\xB865\xB866\xB867\xB868\xB869\xB86A\xB86B\
             \\xB86C\xB86D\xB86E\xB86F\xB870\xB871\xB872\xB873\xB874\xB875\
             \\xB876\xB877\xB878\xB879\xB87A\xB87B\xB87C\xB87D\xB87E\xB87F\
             \\xB880\xB881\xB882\xB883\xB884\xB885\xB886\xB887\xB888\xB889\
             \\xB88A\xB88B\xB88C\xB88D\xB88E\xB88F\xB890\xB891\xB892\xB893\
             \\xB894\xB895\xB896\xB897\xB898\xB899\xB89A\xB89B\xB89C\xB89D\
             \\xB89E\xB89F\xB8A0\xB8A1\xB8A2\xB8A3\xB8A4\xB8A5\xB8A6\xB8A7\
             \\xB8A8\xB8A9\xB8AA\xB8AB\xB8AC\xB8AD\xB8AE\xB8AF\xB8B0\xB8B1\
             \\xB8B2\xB8B3\xB8B4\xB8B5\xB8B6\xB8B7\xB8B8\xB8B9\xB8BA\xB8BB\
             \\xB8BC\xB8BD\xB8BE\xB8BF\xB8C0\xB8C1\xB8C2\xB8C3\xB8C4\xB8C5\
             \\xB8C6\xB8C7\xB8C8\xB8C9\xB8CA\xB8CB\xB8CC\xB8CD\xB8CE\xB8CF\
             \\xB8D0\xB8D1\xB8D2\xB8D3\xB8D4\xB8D5\xB8D6\xB8D7\xB8D8\xB8D9\
             \\xB8DA\xB8DB\xB8DC\xB8DD\xB8DE\xB8DF\xB8E0\xB8E1\xB8E2\xB8E3\
             \\xB8E4\xB8E5\xB8E6\xB8E7\xB8E8\xB8E9\xB8EA\xB8EB\xB8EC\xB8ED\
             \\xB8EE\xB8EF\xB8F0\xB8F1\xB8F2\xB8F3\xB8F4\xB8F5\xB8F6\xB8F7\
             \\xB8F8\xB8F9\xB8FA\xB8FB\xB8FC\xB8FD\xB8FE\xB8FF\xB900\xB901\
             \\xB902\xB903\xB904\xB905\xB906\xB907\xB908\xB909\xB90A\xB90B\
             \\xB90C\xB90D\xB90E\xB90F\xB910\xB911\xB912\xB913\xB914\xB915\
             \\xB916\xB917\xB918\xB919\xB91A\xB91B\xB91C\xB91D\xB91E\xB91F\
             \\xB920\xB921\xB922\xB923\xB924\xB925\xB926\xB927\xB928\xB929\
             \\xB92A\xB92B\xB92C\xB92D\xB92E\xB92F\xB930\xB931\xB932\xB933\
             \\xB934\xB935\xB936\xB937\xB938\xB939\xB93A\xB93B\xB93C\xB93D\
             \\xB93E\xB93F\xB940\xB941\xB942\xB943\xB944\xB945\xB946\xB947\
             \\xB948\xB949\xB94A\xB94B\xB94C\xB94D\xB94E\xB94F\xB950\xB951\
             \\xB952\xB953\xB954\xB955\xB956\xB957\xB958\xB959\xB95A\xB95B\
             \\xB95C\xB95D\xB95E\xB95F\xB960\xB961\xB962\xB963\xB964\xB965\
             \\xB966\xB967\xB968\xB969\xB96A\xB96B\xB96C\xB96D\xB96E\xB96F\
             \\xB970\xB971\xB972\xB973\xB974\xB975\xB976\xB977\xB978\xB979\
             \\xB97A\xB97B\xB97C\xB97D\xB97E\xB97F\xB980\xB981\xB982\xB983\
             \\xB984\xB985\xB986\xB987\xB988\xB989\xB98A\xB98B\xB98C\xB98D\
             \\xB98E\xB98F\xB990\xB991\xB992\xB993\xB994\xB995\xB996\xB997\
             \\xB998\xB999\xB99A\xB99B\xB99C\xB99D\xB99E\xB99F\xB9A0\xB9A1\
             \\xB9A2\xB9A3\xB9A4\xB9A5\xB9A6\xB9A7\xB9A8\xB9A9\xB9AA\xB9AB\
             \\xB9AC\xB9AD\xB9AE\xB9AF\xB9B0\xB9B1\xB9B2\xB9B3\xB9B4\xB9B5\
             \\xB9B6\xB9B7\xB9B8\xB9B9\xB9BA\xB9BB\xB9BC\xB9BD\xB9BE\xB9BF\
             \\xB9C0\xB9C1\xB9C2\xB9C3\xB9C4\xB9C5\xB9C6\xB9C7\xB9C8\xB9C9\
             \\xB9CA\xB9CB\xB9CC\xB9CD\xB9CE\xB9CF\xB9D0\xB9D1\xB9D2\xB9D3\
             \\xB9D4\xB9D5\xB9D6\xB9D7\xB9D8\xB9D9\xB9DA\xB9DB\xB9DC\xB9DD\
             \\xB9DE\xB9DF\xB9E0\xB9E1\xB9E2\xB9E3\xB9E4\xB9E5\xB9E6\xB9E7\
             \\xB9E8\xB9E9\xB9EA\xB9EB\xB9EC\xB9ED\xB9EE\xB9EF\xB9F0\xB9F1\
             \\xB9F2\xB9F3\xB9F4\xB9F5\xB9F6\xB9F7\xB9F8\xB9F9\xB9FA\xB9FB\
             \\xB9FC\xB9FD\xB9FE\xB9FF\xBA00\xBA01\xBA02\xBA03\xBA04\xBA05\
             \\xBA06\xBA07\xBA08\xBA09\xBA0A\xBA0B\xBA0C\xBA0D\xBA0E\xBA0F\
             \\xBA10\xBA11\xBA12\xBA13\xBA14\xBA15\xBA16\xBA17\xBA18\xBA19\
             \\xBA1A\xBA1B\xBA1C\xBA1D\xBA1E\xBA1F\xBA20\xBA21\xBA22\xBA23\
             \\xBA24\xBA25\xBA26\xBA27\xBA28\xBA29\xBA2A\xBA2B\xBA2C\xBA2D\
             \\xBA2E\xBA2F\xBA30\xBA31\xBA32\xBA33\xBA34\xBA35\xBA36\xBA37\
             \\xBA38\xBA39\xBA3A\xBA3B\xBA3C\xBA3D\xBA3E\xBA3F\xBA40\xBA41\
             \\xBA42\xBA43\xBA44\xBA45\xBA46\xBA47\xBA48\xBA49\xBA4A\xBA4B\
             \\xBA4C\xBA4D\xBA4E\xBA4F\xBA50\xBA51\xBA52\xBA53\xBA54\xBA55\
             \\xBA56\xBA57\xBA58\xBA59\xBA5A\xBA5B\xBA5C\xBA5D\xBA5E\xBA5F\
             \\xBA60\xBA61\xBA62\xBA63\xBA64\xBA65\xBA66\xBA67\xBA68\xBA69\
             \\xBA6A\xBA6B\xBA6C\xBA6D\xBA6E\xBA6F\xBA70\xBA71\xBA72\xBA73\
             \\xBA74\xBA75\xBA76\xBA77\xBA78\xBA79\xBA7A\xBA7B\xBA7C\xBA7D\
             \\xBA7E\xBA7F\xBA80\xBA81\xBA82\xBA83\xBA84\xBA85\xBA86\xBA87\
             \\xBA88\xBA89\xBA8A\xBA8B\xBA8C\xBA8D\xBA8E\xBA8F\xBA90\xBA91\
             \\xBA92\xBA93\xBA94\xBA95\xBA96\xBA97\xBA98\xBA99\xBA9A\xBA9B\
             \\xBA9C\xBA9D\xBA9E\xBA9F\xBAA0\xBAA1\xBAA2\xBAA3\xBAA4\xBAA5\
             \\xBAA6\xBAA7\xBAA8\xBAA9\xBAAA\xBAAB\xBAAC\xBAAD\xBAAE\xBAAF\
             \\xBAB0\xBAB1\xBAB2\xBAB3\xBAB4\xBAB5\xBAB6\xBAB7\xBAB8\xBAB9\
             \\xBABA\xBABB\xBABC\xBABD\xBABE\xBABF\xBAC0\xBAC1\xBAC2\xBAC3\
             \\xBAC4\xBAC5\xBAC6\xBAC7\xBAC8\xBAC9\xBACA\xBACB\xBACC\xBACD\
             \\xBACE\xBACF\xBAD0\xBAD1\xBAD2\xBAD3\xBAD4\xBAD5\xBAD6\xBAD7\
             \\xBAD8\xBAD9\xBADA\xBADB\xBADC\xBADD\xBADE\xBADF\xBAE0\xBAE1\
             \\xBAE2\xBAE3\xBAE4\xBAE5\xBAE6\xBAE7\xBAE8\xBAE9\xBAEA\xBAEB\
             \\xBAEC\xBAED\xBAEE\xBAEF\xBAF0\xBAF1\xBAF2\xBAF3\xBAF4\xBAF5\
             \\xBAF6\xBAF7\xBAF8\xBAF9\xBAFA\xBAFB\xBAFC\xBAFD\xBAFE\xBAFF\
             \\xBB00\xBB01\xBB02\xBB03\xBB04\xBB05\xBB06\xBB07\xBB08\xBB09\
             \\xBB0A\xBB0B\xBB0C\xBB0D\xBB0E\xBB0F\xBB10\xBB11\xBB12\xBB13\
             \\xBB14\xBB15\xBB16\xBB17\xBB18\xBB19\xBB1A\xBB1B\xBB1C\xBB1D\
             \\xBB1E\xBB1F\xBB20\xBB21\xBB22\xBB23\xBB24\xBB25\xBB26\xBB27\
             \\xBB28\xBB29\xBB2A\xBB2B\xBB2C\xBB2D\xBB2E\xBB2F\xBB30\xBB31\
             \\xBB32\xBB33\xBB34\xBB35\xBB36\xBB37\xBB38\xBB39\xBB3A\xBB3B\
             \\xBB3C\xBB3D\xBB3E\xBB3F\xBB40\xBB41\xBB42\xBB43\xBB44\xBB45\
             \\xBB46\xBB47\xBB48\xBB49\xBB4A\xBB4B\xBB4C\xBB4D\xBB4E\xBB4F\
             \\xBB50\xBB51\xBB52\xBB53\xBB54\xBB55\xBB56\xBB57\xBB58\xBB59\
             \\xBB5A\xBB5B\xBB5C\xBB5D\xBB5E\xBB5F\xBB60\xBB61\xBB62\xBB63\
             \\xBB64\xBB65\xBB66\xBB67\xBB68\xBB69\xBB6A\xBB6B\xBB6C\xBB6D\
             \\xBB6E\xBB6F\xBB70\xBB71\xBB72\xBB73\xBB74\xBB75\xBB76\xBB77\
             \\xBB78\xBB79\xBB7A\xBB7B\xBB7C\xBB7D\xBB7E\xBB7F\xBB80\xBB81\
             \\xBB82\xBB83\xBB84\xBB85\xBB86\xBB87\xBB88\xBB89\xBB8A\xBB8B\
             \\xBB8C\xBB8D\xBB8E\xBB8F\xBB90\xBB91\xBB92\xBB93\xBB94\xBB95\
             \\xBB96\xBB97\xBB98\xBB99\xBB9A\xBB9B\xBB9C\xBB9D\xBB9E\xBB9F\
             \\xBBA0\xBBA1\xBBA2\xBBA3\xBBA4\xBBA5\xBBA6\xBBA7\xBBA8\xBBA9\
             \\xBBAA\xBBAB\xBBAC\xBBAD\xBBAE\xBBAF\xBBB0\xBBB1\xBBB2\xBBB3\
             \\xBBB4\xBBB5\xBBB6\xBBB7\xBBB8\xBBB9\xBBBA\xBBBB\xBBBC\xBBBD\
             \\xBBBE\xBBBF\xBBC0\xBBC1\xBBC2\xBBC3\xBBC4\xBBC5\xBBC6\xBBC7\
             \\xBBC8\xBBC9\xBBCA\xBBCB\xBBCC\xBBCD\xBBCE\xBBCF\xBBD0\xBBD1\
             \\xBBD2\xBBD3\xBBD4\xBBD5\xBBD6\xBBD7\xBBD8\xBBD9\xBBDA\xBBDB\
             \\xBBDC\xBBDD\xBBDE\xBBDF\xBBE0\xBBE1\xBBE2\xBBE3\xBBE4\xBBE5\
             \\xBBE6\xBBE7\xBBE8\xBBE9\xBBEA\xBBEB\xBBEC\xBBED\xBBEE\xBBEF\
             \\xBBF0\xBBF1\xBBF2\xBBF3\xBBF4\xBBF5\xBBF6\xBBF7\xBBF8\xBBF9\
             \\xBBFA\xBBFB\xBBFC\xBBFD\xBBFE\xBBFF\xBC00\xBC01\xBC02\xBC03\
             \\xBC04\xBC05\xBC06\xBC07\xBC08\xBC09\xBC0A\xBC0B\xBC0C\xBC0D\
             \\xBC0E\xBC0F\xBC10\xBC11\xBC12\xBC13\xBC14\xBC15\xBC16\xBC17\
             \\xBC18\xBC19\xBC1A\xBC1B\xBC1C\xBC1D\xBC1E\xBC1F\xBC20\xBC21\
             \\xBC22\xBC23\xBC24\xBC25\xBC26\xBC27\xBC28\xBC29\xBC2A\xBC2B\
             \\xBC2C\xBC2D\xBC2E\xBC2F\xBC30\xBC31\xBC32\xBC33\xBC34\xBC35\
             \\xBC36\xBC37\xBC38\xBC39\xBC3A\xBC3B\xBC3C\xBC3D\xBC3E\xBC3F\
             \\xBC40\xBC41\xBC42\xBC43\xBC44\xBC45\xBC46\xBC47\xBC48\xBC49\
             \\xBC4A\xBC4B\xBC4C\xBC4D\xBC4E\xBC4F\xBC50\xBC51\xBC52\xBC53\
             \\xBC54\xBC55\xBC56\xBC57\xBC58\xBC59\xBC5A\xBC5B\xBC5C\xBC5D\
             \\xBC5E\xBC5F\xBC60\xBC61\xBC62\xBC63\xBC64\xBC65\xBC66\xBC67\
             \\xBC68\xBC69\xBC6A\xBC6B\xBC6C\xBC6D\xBC6E\xBC6F\xBC70\xBC71\
             \\xBC72\xBC73\xBC74\xBC75\xBC76\xBC77\xBC78\xBC79\xBC7A\xBC7B\
             \\xBC7C\xBC7D\xBC7E\xBC7F\xBC80\xBC81\xBC82\xBC83\xBC84\xBC85\
             \\xBC86\xBC87\xBC88\xBC89\xBC8A\xBC8B\xBC8C\xBC8D\xBC8E\xBC8F\
             \\xBC90\xBC91\xBC92\xBC93\xBC94\xBC95\xBC96\xBC97\xBC98\xBC99\
             \\xBC9A\xBC9B\xBC9C\xBC9D\xBC9E\xBC9F\xBCA0\xBCA1\xBCA2\xBCA3\
             \\xBCA4\xBCA5\xBCA6\xBCA7\xBCA8\xBCA9\xBCAA\xBCAB\xBCAC\xBCAD\
             \\xBCAE\xBCAF\xBCB0\xBCB1\xBCB2\xBCB3\xBCB4\xBCB5\xBCB6\xBCB7\
             \\xBCB8\xBCB9\xBCBA\xBCBB\xBCBC\xBCBD\xBCBE\xBCBF\xBCC0\xBCC1\
             \\xBCC2\xBCC3\xBCC4\xBCC5\xBCC6\xBCC7\xBCC8\xBCC9\xBCCA\xBCCB\
             \\xBCCC\xBCCD\xBCCE\xBCCF\xBCD0\xBCD1\xBCD2\xBCD3\xBCD4\xBCD5\
             \\xBCD6\xBCD7\xBCD8\xBCD9\xBCDA\xBCDB\xBCDC\xBCDD\xBCDE\xBCDF\
             \\xBCE0\xBCE1\xBCE2\xBCE3\xBCE4\xBCE5\xBCE6\xBCE7\xBCE8\xBCE9\
             \\xBCEA\xBCEB\xBCEC\xBCED\xBCEE\xBCEF\xBCF0\xBCF1\xBCF2\xBCF3\
             \\xBCF4\xBCF5\xBCF6\xBCF7\xBCF8\xBCF9\xBCFA\xBCFB\xBCFC\xBCFD\
             \\xBCFE\xBCFF\xBD00\xBD01\xBD02\xBD03\xBD04\xBD05\xBD06\xBD07\
             \\xBD08\xBD09\xBD0A\xBD0B\xBD0C\xBD0D\xBD0E\xBD0F\xBD10\xBD11\
             \\xBD12\xBD13\xBD14\xBD15\xBD16\xBD17\xBD18\xBD19\xBD1A\xBD1B\
             \\xBD1C\xBD1D\xBD1E\xBD1F\xBD20\xBD21\xBD22\xBD23\xBD24\xBD25\
             \\xBD26\xBD27\xBD28\xBD29\xBD2A\xBD2B\xBD2C\xBD2D\xBD2E\xBD2F\
             \\xBD30\xBD31\xBD32\xBD33\xBD34\xBD35\xBD36\xBD37\xBD38\xBD39\
             \\xBD3A\xBD3B\xBD3C\xBD3D\xBD3E\xBD3F\xBD40\xBD41\xBD42\xBD43\
             \\xBD44\xBD45\xBD46\xBD47\xBD48\xBD49\xBD4A\xBD4B\xBD4C\xBD4D\
             \\xBD4E\xBD4F\xBD50\xBD51\xBD52\xBD53\xBD54\xBD55\xBD56\xBD57\
             \\xBD58\xBD59\xBD5A\xBD5B\xBD5C\xBD5D\xBD5E\xBD5F\xBD60\xBD61\
             \\xBD62\xBD63\xBD64\xBD65\xBD66\xBD67\xBD68\xBD69\xBD6A\xBD6B\
             \\xBD6C\xBD6D\xBD6E\xBD6F\xBD70\xBD71\xBD72\xBD73\xBD74\xBD75\
             \\xBD76\xBD77\xBD78\xBD79\xBD7A\xBD7B\xBD7C\xBD7D\xBD7E\xBD7F\
             \\xBD80\xBD81\xBD82\xBD83\xBD84\xBD85\xBD86\xBD87\xBD88\xBD89\
             \\xBD8A\xBD8B\xBD8C\xBD8D\xBD8E\xBD8F\xBD90\xBD91\xBD92\xBD93\
             \\xBD94\xBD95\xBD96\xBD97\xBD98\xBD99\xBD9A\xBD9B\xBD9C\xBD9D\
             \\xBD9E\xBD9F\xBDA0\xBDA1\xBDA2\xBDA3\xBDA4\xBDA5\xBDA6\xBDA7\
             \\xBDA8\xBDA9\xBDAA\xBDAB\xBDAC\xBDAD\xBDAE\xBDAF\xBDB0\xBDB1\
             \\xBDB2\xBDB3\xBDB4\xBDB5\xBDB6\xBDB7\xBDB8\xBDB9\xBDBA\xBDBB\
             \\xBDBC\xBDBD\xBDBE\xBDBF\xBDC0\xBDC1\xBDC2\xBDC3\xBDC4\xBDC5\
             \\xBDC6\xBDC7\xBDC8\xBDC9\xBDCA\xBDCB\xBDCC\xBDCD\xBDCE\xBDCF\
             \\xBDD0\xBDD1\xBDD2\xBDD3\xBDD4\xBDD5\xBDD6\xBDD7\xBDD8\xBDD9\
             \\xBDDA\xBDDB\xBDDC\xBDDD\xBDDE\xBDDF\xBDE0\xBDE1\xBDE2\xBDE3\
             \\xBDE4\xBDE5\xBDE6\xBDE7\xBDE8\xBDE9\xBDEA\xBDEB\xBDEC\xBDED\
             \\xBDEE\xBDEF\xBDF0\xBDF1\xBDF2\xBDF3\xBDF4\xBDF5\xBDF6\xBDF7\
             \\xBDF8\xBDF9\xBDFA\xBDFB\xBDFC\xBDFD\xBDFE\xBDFF\xBE00\xBE01\
             \\xBE02\xBE03\xBE04\xBE05\xBE06\xBE07\xBE08\xBE09\xBE0A\xBE0B\
             \\xBE0C\xBE0D\xBE0E\xBE0F\xBE10\xBE11\xBE12\xBE13\xBE14\xBE15\
             \\xBE16\xBE17\xBE18\xBE19\xBE1A\xBE1B\xBE1C\xBE1D\xBE1E\xBE1F\
             \\xBE20\xBE21\xBE22\xBE23\xBE24\xBE25\xBE26\xBE27\xBE28\xBE29\
             \\xBE2A\xBE2B\xBE2C\xBE2D\xBE2E\xBE2F\xBE30\xBE31\xBE32\xBE33\
             \\xBE34\xBE35\xBE36\xBE37\xBE38\xBE39\xBE3A\xBE3B\xBE3C\xBE3D\
             \\xBE3E\xBE3F\xBE40\xBE41\xBE42\xBE43\xBE44\xBE45\xBE46\xBE47\
             \\xBE48\xBE49\xBE4A\xBE4B\xBE4C\xBE4D\xBE4E\xBE4F\xBE50\xBE51\
             \\xBE52\xBE53\xBE54\xBE55\xBE56\xBE57\xBE58\xBE59\xBE5A\xBE5B\
             \\xBE5C\xBE5D\xBE5E\xBE5F\xBE60\xBE61\xBE62\xBE63\xBE64\xBE65\
             \\xBE66\xBE67\xBE68\xBE69\xBE6A\xBE6B\xBE6C\xBE6D\xBE6E\xBE6F\
             \\xBE70\xBE71\xBE72\xBE73\xBE74\xBE75\xBE76\xBE77\xBE78\xBE79\
             \\xBE7A\xBE7B\xBE7C\xBE7D\xBE7E\xBE7F\xBE80\xBE81\xBE82\xBE83\
             \\xBE84\xBE85\xBE86\xBE87\xBE88\xBE89\xBE8A\xBE8B\xBE8C\xBE8D\
             \\xBE8E\xBE8F\xBE90\xBE91\xBE92\xBE93\xBE94\xBE95\xBE96\xBE97\
             \\xBE98\xBE99\xBE9A\xBE9B\xBE9C\xBE9D\xBE9E\xBE9F\xBEA0\xBEA1\
             \\xBEA2\xBEA3\xBEA4\xBEA5\xBEA6\xBEA7\xBEA8\xBEA9\xBEAA\xBEAB\
             \\xBEAC\xBEAD\xBEAE\xBEAF\xBEB0\xBEB1\xBEB2\xBEB3\xBEB4\xBEB5\
             \\xBEB6\xBEB7\xBEB8\xBEB9\xBEBA\xBEBB\xBEBC\xBEBD\xBEBE\xBEBF\
             \\xBEC0\xBEC1\xBEC2\xBEC3\xBEC4\xBEC5\xBEC6\xBEC7\xBEC8\xBEC9\
             \\xBECA\xBECB\xBECC\xBECD\xBECE\xBECF\xBED0\xBED1\xBED2\xBED3\
             \\xBED4\xBED5\xBED6\xBED7\xBED8\xBED9\xBEDA\xBEDB\xBEDC\xBEDD\
             \\xBEDE\xBEDF\xBEE0\xBEE1\xBEE2\xBEE3\xBEE4\xBEE5\xBEE6\xBEE7\
             \\xBEE8\xBEE9\xBEEA\xBEEB\xBEEC\xBEED\xBEEE\xBEEF\xBEF0\xBEF1\
             \\xBEF2\xBEF3\xBEF4\xBEF5\xBEF6\xBEF7\xBEF8\xBEF9\xBEFA\xBEFB\
             \\xBEFC\xBEFD\xBEFE\xBEFF\xBF00\xBF01\xBF02\xBF03\xBF04\xBF05\
             \\xBF06\xBF07\xBF08\xBF09\xBF0A\xBF0B\xBF0C\xBF0D\xBF0E\xBF0F\
             \\xBF10\xBF11\xBF12\xBF13\xBF14\xBF15\xBF16\xBF17\xBF18\xBF19\
             \\xBF1A\xBF1B\xBF1C\xBF1D\xBF1E\xBF1F\xBF20\xBF21\xBF22\xBF23\
             \\xBF24\xBF25\xBF26\xBF27\xBF28\xBF29\xBF2A\xBF2B\xBF2C\xBF2D\
             \\xBF2E\xBF2F\xBF30\xBF31\xBF32\xBF33\xBF34\xBF35\xBF36\xBF37\
             \\xBF38\xBF39\xBF3A\xBF3B\xBF3C\xBF3D\xBF3E\xBF3F\xBF40\xBF41\
             \\xBF42\xBF43\xBF44\xBF45\xBF46\xBF47\xBF48\xBF49\xBF4A\xBF4B\
             \\xBF4C\xBF4D\xBF4E\xBF4F\xBF50\xBF51\xBF52\xBF53\xBF54\xBF55\
             \\xBF56\xBF57\xBF58\xBF59\xBF5A\xBF5B\xBF5C\xBF5D\xBF5E\xBF5F\
             \\xBF60\xBF61\xBF62\xBF63\xBF64\xBF65\xBF66\xBF67\xBF68\xBF69\
             \\xBF6A\xBF6B\xBF6C\xBF6D\xBF6E\xBF6F\xBF70\xBF71\xBF72\xBF73\
             \\xBF74\xBF75\xBF76\xBF77\xBF78\xBF79\xBF7A\xBF7B\xBF7C\xBF7D\
             \\xBF7E\xBF7F\xBF80\xBF81\xBF82\xBF83\xBF84\xBF85\xBF86\xBF87\
             \\xBF88\xBF89\xBF8A\xBF8B\xBF8C\xBF8D\xBF8E\xBF8F\xBF90\xBF91\
             \\xBF92\xBF93\xBF94\xBF95\xBF96\xBF97\xBF98\xBF99\xBF9A\xBF9B\
             \\xBF9C\xBF9D\xBF9E\xBF9F\xBFA0\xBFA1\xBFA2\xBFA3\xBFA4\xBFA5\
             \\xBFA6\xBFA7\xBFA8\xBFA9\xBFAA\xBFAB\xBFAC\xBFAD\xBFAE\xBFAF\
             \\xBFB0\xBFB1\xBFB2\xBFB3\xBFB4\xBFB5\xBFB6\xBFB7\xBFB8\xBFB9\
             \\xBFBA\xBFBB\xBFBC\xBFBD\xBFBE\xBFBF\xBFC0\xBFC1\xBFC2\xBFC3\
             \\xBFC4\xBFC5\xBFC6\xBFC7\xBFC8\xBFC9\xBFCA\xBFCB\xBFCC\xBFCD\
             \\xBFCE\xBFCF\xBFD0\xBFD1\xBFD2\xBFD3\xBFD4\xBFD5\xBFD6\xBFD7\
             \\xBFD8\xBFD9\xBFDA\xBFDB\xBFDC\xBFDD\xBFDE\xBFDF\xBFE0\xBFE1\
             \\xBFE2\xBFE3\xBFE4\xBFE5\xBFE6\xBFE7\xBFE8\xBFE9\xBFEA\xBFEB\
             \\xBFEC\xBFED\xBFEE\xBFEF\xBFF0\xBFF1\xBFF2\xBFF3\xBFF4\xBFF5\
             \\xBFF6\xBFF7\xBFF8\xBFF9\xBFFA\xBFFB\xBFFC\xBFFD\xBFFE\xBFFF\
             \\xC000\xC001\xC002\xC003\xC004\xC005\xC006\xC007\xC008\xC009\
             \\xC00A\xC00B\xC00C\xC00D\xC00E\xC00F\xC010\xC011\xC012\xC013\
             \\xC014\xC015\xC016\xC017\xC018\xC019\xC01A\xC01B\xC01C\xC01D\
             \\xC01E\xC01F\xC020\xC021\xC022\xC023\xC024\xC025\xC026\xC027\
             \\xC028\xC029\xC02A\xC02B\xC02C\xC02D\xC02E\xC02F\xC030\xC031\
             \\xC032\xC033\xC034\xC035\xC036\xC037\xC038\xC039\xC03A\xC03B\
             \\xC03C\xC03D\xC03E\xC03F\xC040\xC041\xC042\xC043\xC044\xC045\
             \\xC046\xC047\xC048\xC049\xC04A\xC04B\xC04C\xC04D\xC04E\xC04F\
             \\xC050\xC051\xC052\xC053\xC054\xC055\xC056\xC057\xC058\xC059\
             \\xC05A\xC05B\xC05C\xC05D\xC05E\xC05F\xC060\xC061\xC062\xC063\
             \\xC064\xC065\xC066\xC067\xC068\xC069\xC06A\xC06B\xC06C\xC06D\
             \\xC06E\xC06F\xC070\xC071\xC072\xC073\xC074\xC075\xC076\xC077\
             \\xC078\xC079\xC07A\xC07B\xC07C\xC07D\xC07E\xC07F\xC080\xC081\
             \\xC082\xC083\xC084\xC085\xC086\xC087\xC088\xC089\xC08A\xC08B\
             \\xC08C\xC08D\xC08E\xC08F\xC090\xC091\xC092\xC093\xC094\xC095\
             \\xC096\xC097\xC098\xC099\xC09A\xC09B\xC09C\xC09D\xC09E\xC09F\
             \\xC0A0\xC0A1\xC0A2\xC0A3\xC0A4\xC0A5\xC0A6\xC0A7\xC0A8\xC0A9\
             \\xC0AA\xC0AB\xC0AC\xC0AD\xC0AE\xC0AF\xC0B0\xC0B1\xC0B2\xC0B3\
             \\xC0B4\xC0B5\xC0B6\xC0B7\xC0B8\xC0B9\xC0BA\xC0BB\xC0BC\xC0BD\
             \\xC0BE\xC0BF\xC0C0\xC0C1\xC0C2\xC0C3\xC0C4\xC0C5\xC0C6\xC0C7\
             \\xC0C8\xC0C9\xC0CA\xC0CB\xC0CC\xC0CD\xC0CE\xC0CF\xC0D0\xC0D1\
             \\xC0D2\xC0D3\xC0D4\xC0D5\xC0D6\xC0D7\xC0D8\xC0D9\xC0DA\xC0DB\
             \\xC0DC\xC0DD\xC0DE\xC0DF\xC0E0\xC0E1\xC0E2\xC0E3\xC0E4\xC0E5\
             \\xC0E6\xC0E7\xC0E8\xC0E9\xC0EA\xC0EB\xC0EC\xC0ED\xC0EE\xC0EF\
             \\xC0F0\xC0F1\xC0F2\xC0F3\xC0F4\xC0F5\xC0F6\xC0F7\xC0F8\xC0F9\
             \\xC0FA\xC0FB\xC0FC\xC0FD\xC0FE\xC0FF\xC100\xC101\xC102\xC103\
             \\xC104\xC105\xC106\xC107\xC108\xC109\xC10A\xC10B\xC10C\xC10D\
             \\xC10E\xC10F\xC110\xC111\xC112\xC113\xC114\xC115\xC116\xC117\
             \\xC118\xC119\xC11A\xC11B\xC11C\xC11D\xC11E\xC11F\xC120\xC121\
             \\xC122\xC123\xC124\xC125\xC126\xC127\xC128\xC129\xC12A\xC12B\
             \\xC12C\xC12D\xC12E\xC12F\xC130\xC131\xC132\xC133\xC134\xC135\
             \\xC136\xC137\xC138\xC139\xC13A\xC13B\xC13C\xC13D\xC13E\xC13F\
             \\xC140\xC141\xC142\xC143\xC144\xC145\xC146\xC147\xC148\xC149\
             \\xC14A\xC14B\xC14C\xC14D\xC14E\xC14F\xC150\xC151\xC152\xC153\
             \\xC154\xC155\xC156\xC157\xC158\xC159\xC15A\xC15B\xC15C\xC15D\
             \\xC15E\xC15F\xC160\xC161\xC162\xC163\xC164\xC165\xC166\xC167\
             \\xC168\xC169\xC16A\xC16B\xC16C\xC16D\xC16E\xC16F\xC170\xC171\
             \\xC172\xC173\xC174\xC175\xC176\xC177\xC178\xC179\xC17A\xC17B\
             \\xC17C\xC17D\xC17E\xC17F\xC180\xC181\xC182\xC183\xC184\xC185\
             \\xC186\xC187\xC188\xC189\xC18A\xC18B\xC18C\xC18D\xC18E\xC18F\
             \\xC190\xC191\xC192\xC193\xC194\xC195\xC196\xC197\xC198\xC199\
             \\xC19A\xC19B\xC19C\xC19D\xC19E\xC19F\xC1A0\xC1A1\xC1A2\xC1A3\
             \\xC1A4\xC1A5\xC1A6\xC1A7\xC1A8\xC1A9\xC1AA\xC1AB\xC1AC\xC1AD\
             \\xC1AE\xC1AF\xC1B0\xC1B1\xC1B2\xC1B3\xC1B4\xC1B5\xC1B6\xC1B7\
             \\xC1B8\xC1B9\xC1BA\xC1BB\xC1BC\xC1BD\xC1BE\xC1BF\xC1C0\xC1C1\
             \\xC1C2\xC1C3\xC1C4\xC1C5\xC1C6\xC1C7\xC1C8\xC1C9\xC1CA\xC1CB\
             \\xC1CC\xC1CD\xC1CE\xC1CF\xC1D0\xC1D1\xC1D2\xC1D3\xC1D4\xC1D5\
             \\xC1D6\xC1D7\xC1D8\xC1D9\xC1DA\xC1DB\xC1DC\xC1DD\xC1DE\xC1DF\
             \\xC1E0\xC1E1\xC1E2\xC1E3\xC1E4\xC1E5\xC1E6\xC1E7\xC1E8\xC1E9\
             \\xC1EA\xC1EB\xC1EC\xC1ED\xC1EE\xC1EF\xC1F0\xC1F1\xC1F2\xC1F3\
             \\xC1F4\xC1F5\xC1F6\xC1F7\xC1F8\xC1F9\xC1FA\xC1FB\xC1FC\xC1FD\
             \\xC1FE\xC1FF\xC200\xC201\xC202\xC203\xC204\xC205\xC206\xC207\
             \\xC208\xC209\xC20A\xC20B\xC20C\xC20D\xC20E\xC20F\xC210\xC211\
             \\xC212\xC213\xC214\xC215\xC216\xC217\xC218\xC219\xC21A\xC21B\
             \\xC21C\xC21D\xC21E\xC21F\xC220\xC221\xC222\xC223\xC224\xC225\
             \\xC226\xC227\xC228\xC229\xC22A\xC22B\xC22C\xC22D\xC22E\xC22F\
             \\xC230\xC231\xC232\xC233\xC234\xC235\xC236\xC237\xC238\xC239\
             \\xC23A\xC23B\xC23C\xC23D\xC23E\xC23F\xC240\xC241\xC242\xC243\
             \\xC244\xC245\xC246\xC247\xC248\xC249\xC24A\xC24B\xC24C\xC24D\
             \\xC24E\xC24F\xC250\xC251\xC252\xC253\xC254\xC255\xC256\xC257\
             \\xC258\xC259\xC25A\xC25B\xC25C\xC25D\xC25E\xC25F\xC260\xC261\
             \\xC262\xC263\xC264\xC265\xC266\xC267\xC268\xC269\xC26A\xC26B\
             \\xC26C\xC26D\xC26E\xC26F\xC270\xC271\xC272\xC273\xC274\xC275\
             \\xC276\xC277\xC278\xC279\xC27A\xC27B\xC27C\xC27D\xC27E\xC27F\
             \\xC280\xC281\xC282\xC283\xC284\xC285\xC286\xC287\xC288\xC289\
             \\xC28A\xC28B\xC28C\xC28D\xC28E\xC28F\xC290\xC291\xC292\xC293\
             \\xC294\xC295\xC296\xC297\xC298\xC299\xC29A\xC29B\xC29C\xC29D\
             \\xC29E\xC29F\xC2A0\xC2A1\xC2A2\xC2A3\xC2A4\xC2A5\xC2A6\xC2A7\
             \\xC2A8\xC2A9\xC2AA\xC2AB\xC2AC\xC2AD\xC2AE\xC2AF\xC2B0\xC2B1\
             \\xC2B2\xC2B3\xC2B4\xC2B5\xC2B6\xC2B7\xC2B8\xC2B9\xC2BA\xC2BB\
             \\xC2BC\xC2BD\xC2BE\xC2BF\xC2C0\xC2C1\xC2C2\xC2C3\xC2C4\xC2C5\
             \\xC2C6\xC2C7\xC2C8\xC2C9\xC2CA\xC2CB\xC2CC\xC2CD\xC2CE\xC2CF\
             \\xC2D0\xC2D1\xC2D2\xC2D3\xC2D4\xC2D5\xC2D6\xC2D7\xC2D8\xC2D9\
             \\xC2DA\xC2DB\xC2DC\xC2DD\xC2DE\xC2DF\xC2E0\xC2E1\xC2E2\xC2E3\
             \\xC2E4\xC2E5\xC2E6\xC2E7\xC2E8\xC2E9\xC2EA\xC2EB\xC2EC\xC2ED\
             \\xC2EE\xC2EF\xC2F0\xC2F1\xC2F2\xC2F3\xC2F4\xC2F5\xC2F6\xC2F7\
             \\xC2F8\xC2F9\xC2FA\xC2FB\xC2FC\xC2FD\xC2FE\xC2FF\xC300\xC301\
             \\xC302\xC303\xC304\xC305\xC306\xC307\xC308\xC309\xC30A\xC30B\
             \\xC30C\xC30D\xC30E\xC30F\xC310\xC311\xC312\xC313\xC314\xC315\
             \\xC316\xC317\xC318\xC319\xC31A\xC31B\xC31C\xC31D\xC31E\xC31F\
             \\xC320\xC321\xC322\xC323\xC324\xC325\xC326\xC327\xC328\xC329\
             \\xC32A\xC32B\xC32C\xC32D\xC32E\xC32F\xC330\xC331\xC332\xC333\
             \\xC334\xC335\xC336\xC337\xC338\xC339\xC33A\xC33B\xC33C\xC33D\
             \\xC33E\xC33F\xC340\xC341\xC342\xC343\xC344\xC345\xC346\xC347\
             \\xC348\xC349\xC34A\xC34B\xC34C\xC34D\xC34E\xC34F\xC350\xC351\
             \\xC352\xC353\xC354\xC355\xC356\xC357\xC358\xC359\xC35A\xC35B\
             \\xC35C\xC35D\xC35E\xC35F\xC360\xC361\xC362\xC363\xC364\xC365\
             \\xC366\xC367\xC368\xC369\xC36A\xC36B\xC36C\xC36D\xC36E\xC36F\
             \\xC370\xC371\xC372\xC373\xC374\xC375\xC376\xC377\xC378\xC379\
             \\xC37A\xC37B\xC37C\xC37D\xC37E\xC37F\xC380\xC381\xC382\xC383\
             \\xC384\xC385\xC386\xC387\xC388\xC389\xC38A\xC38B\xC38C\xC38D\
             \\xC38E\xC38F\xC390\xC391\xC392\xC393\xC394\xC395\xC396\xC397\
             \\xC398\xC399\xC39A\xC39B\xC39C\xC39D\xC39E\xC39F\xC3A0\xC3A1\
             \\xC3A2\xC3A3\xC3A4\xC3A5\xC3A6\xC3A7\xC3A8\xC3A9\xC3AA\xC3AB\
             \\xC3AC\xC3AD\xC3AE\xC3AF\xC3B0\xC3B1\xC3B2\xC3B3\xC3B4\xC3B5\
             \\xC3B6\xC3B7\xC3B8\xC3B9\xC3BA\xC3BB\xC3BC\xC3BD\xC3BE\xC3BF\
             \\xC3C0\xC3C1\xC3C2\xC3C3\xC3C4\xC3C5\xC3C6\xC3C7\xC3C8\xC3C9\
             \\xC3CA\xC3CB\xC3CC\xC3CD\xC3CE\xC3CF\xC3D0\xC3D1\xC3D2\xC3D3\
             \\xC3D4\xC3D5\xC3D6\xC3D7\xC3D8\xC3D9\xC3DA\xC3DB\xC3DC\xC3DD\
             \\xC3DE\xC3DF\xC3E0\xC3E1\xC3E2\xC3E3\xC3E4\xC3E5\xC3E6\xC3E7\
             \\xC3E8\xC3E9\xC3EA\xC3EB\xC3EC\xC3ED\xC3EE\xC3EF\xC3F0\xC3F1\
             \\xC3F2\xC3F3\xC3F4\xC3F5\xC3F6\xC3F7\xC3F8\xC3F9\xC3FA\xC3FB\
             \\xC3FC\xC3FD\xC3FE\xC3FF\xC400\xC401\xC402\xC403\xC404\xC405\
             \\xC406\xC407\xC408\xC409\xC40A\xC40B\xC40C\xC40D\xC40E\xC40F\
             \\xC410\xC411\xC412\xC413\xC414\xC415\xC416\xC417\xC418\xC419\
             \\xC41A\xC41B\xC41C\xC41D\xC41E\xC41F\xC420\xC421\xC422\xC423\
             \\xC424\xC425\xC426\xC427\xC428\xC429\xC42A\xC42B\xC42C\xC42D\
             \\xC42E\xC42F\xC430\xC431\xC432\xC433\xC434\xC435\xC436\xC437\
             \\xC438\xC439\xC43A\xC43B\xC43C\xC43D\xC43E\xC43F\xC440\xC441\
             \\xC442\xC443\xC444\xC445\xC446\xC447\xC448\xC449\xC44A\xC44B\
             \\xC44C\xC44D\xC44E\xC44F\xC450\xC451\xC452\xC453\xC454\xC455\
             \\xC456\xC457\xC458\xC459\xC45A\xC45B\xC45C\xC45D\xC45E\xC45F\
             \\xC460\xC461\xC462\xC463\xC464\xC465\xC466\xC467\xC468\xC469\
             \\xC46A\xC46B\xC46C\xC46D\xC46E\xC46F\xC470\xC471\xC472\xC473\
             \\xC474\xC475\xC476\xC477\xC478\xC479\xC47A\xC47B\xC47C\xC47D\
             \\xC47E\xC47F\xC480\xC481\xC482\xC483\xC484\xC485\xC486\xC487\
             \\xC488\xC489\xC48A\xC48B\xC48C\xC48D\xC48E\xC48F\xC490\xC491\
             \\xC492\xC493\xC494\xC495\xC496\xC497\xC498\xC499\xC49A\xC49B\
             \\xC49C\xC49D\xC49E\xC49F\xC4A0\xC4A1\xC4A2\xC4A3\xC4A4\xC4A5\
             \\xC4A6\xC4A7\xC4A8\xC4A9\xC4AA\xC4AB\xC4AC\xC4AD\xC4AE\xC4AF\
             \\xC4B0\xC4B1\xC4B2\xC4B3\xC4B4\xC4B5\xC4B6\xC4B7\xC4B8\xC4B9\
             \\xC4BA\xC4BB\xC4BC\xC4BD\xC4BE\xC4BF\xC4C0\xC4C1\xC4C2\xC4C3\
             \\xC4C4\xC4C5\xC4C6\xC4C7\xC4C8\xC4C9\xC4CA\xC4CB\xC4CC\xC4CD\
             \\xC4CE\xC4CF\xC4D0\xC4D1\xC4D2\xC4D3\xC4D4\xC4D5\xC4D6\xC4D7\
             \\xC4D8\xC4D9\xC4DA\xC4DB\xC4DC\xC4DD\xC4DE\xC4DF\xC4E0\xC4E1\
             \\xC4E2\xC4E3\xC4E4\xC4E5\xC4E6\xC4E7\xC4E8\xC4E9\xC4EA\xC4EB\
             \\xC4EC\xC4ED\xC4EE\xC4EF\xC4F0\xC4F1\xC4F2\xC4F3\xC4F4\xC4F5\
             \\xC4F6\xC4F7\xC4F8\xC4F9\xC4FA\xC4FB\xC4FC\xC4FD\xC4FE\xC4FF\
             \\xC500\xC501\xC502\xC503\xC504\xC505\xC506\xC507\xC508\xC509\
             \\xC50A\xC50B\xC50C\xC50D\xC50E\xC50F\xC510\xC511\xC512\xC513\
             \\xC514\xC515\xC516\xC517\xC518\xC519\xC51A\xC51B\xC51C\xC51D\
             \\xC51E\xC51F\xC520\xC521\xC522\xC523\xC524\xC525\xC526\xC527\
             \\xC528\xC529\xC52A\xC52B\xC52C\xC52D\xC52E\xC52F\xC530\xC531\
             \\xC532\xC533\xC534\xC535\xC536\xC537\xC538\xC539\xC53A\xC53B\
             \\xC53C\xC53D\xC53E\xC53F\xC540\xC541\xC542\xC543\xC544\xC545\
             \\xC546\xC547\xC548\xC549\xC54A\xC54B\xC54C\xC54D\xC54E\xC54F\
             \\xC550\xC551\xC552\xC553\xC554\xC555\xC556\xC557\xC558\xC559\
             \\xC55A\xC55B\xC55C\xC55D\xC55E\xC55F\xC560\xC561\xC562\xC563\
             \\xC564\xC565\xC566\xC567\xC568\xC569\xC56A\xC56B\xC56C\xC56D\
             \\xC56E\xC56F\xC570\xC571\xC572\xC573\xC574\xC575\xC576\xC577\
             \\xC578\xC579\xC57A\xC57B\xC57C\xC57D\xC57E\xC57F\xC580\xC581\
             \\xC582\xC583\xC584\xC585\xC586\xC587\xC588\xC589\xC58A\xC58B\
             \\xC58C\xC58D\xC58E\xC58F\xC590\xC591\xC592\xC593\xC594\xC595\
             \\xC596\xC597\xC598\xC599\xC59A\xC59B\xC59C\xC59D\xC59E\xC59F\
             \\xC5A0\xC5A1\xC5A2\xC5A3\xC5A4\xC5A5\xC5A6\xC5A7\xC5A8\xC5A9\
             \\xC5AA\xC5AB\xC5AC\xC5AD\xC5AE\xC5AF\xC5B0\xC5B1\xC5B2\xC5B3\
             \\xC5B4\xC5B5\xC5B6\xC5B7\xC5B8\xC5B9\xC5BA\xC5BB\xC5BC\xC5BD\
             \\xC5BE\xC5BF\xC5C0\xC5C1\xC5C2\xC5C3\xC5C4\xC5C5\xC5C6\xC5C7\
             \\xC5C8\xC5C9\xC5CA\xC5CB\xC5CC\xC5CD\xC5CE\xC5CF\xC5D0\xC5D1\
             \\xC5D2\xC5D3\xC5D4\xC5D5\xC5D6\xC5D7\xC5D8\xC5D9\xC5DA\xC5DB\
             \\xC5DC\xC5DD\xC5DE\xC5DF\xC5E0\xC5E1\xC5E2\xC5E3\xC5E4\xC5E5\
             \\xC5E6\xC5E7\xC5E8\xC5E9\xC5EA\xC5EB\xC5EC\xC5ED\xC5EE\xC5EF\
             \\xC5F0\xC5F1\xC5F2\xC5F3\xC5F4\xC5F5\xC5F6\xC5F7\xC5F8\xC5F9\
             \\xC5FA\xC5FB\xC5FC\xC5FD\xC5FE\xC5FF\xC600\xC601\xC602\xC603\
             \\xC604\xC605\xC606\xC607\xC608\xC609\xC60A\xC60B\xC60C\xC60D\
             \\xC60E\xC60F\xC610\xC611\xC612\xC613\xC614\xC615\xC616\xC617\
             \\xC618\xC619\xC61A\xC61B\xC61C\xC61D\xC61E\xC61F\xC620\xC621\
             \\xC622\xC623\xC624\xC625\xC626\xC627\xC628\xC629\xC62A\xC62B\
             \\xC62C\xC62D\xC62E\xC62F\xC630\xC631\xC632\xC633\xC634\xC635\
             \\xC636\xC637\xC638\xC639\xC63A\xC63B\xC63C\xC63D\xC63E\xC63F\
             \\xC640\xC641\xC642\xC643\xC644\xC645\xC646\xC647\xC648\xC649\
             \\xC64A\xC64B\xC64C\xC64D\xC64E\xC64F\xC650\xC651\xC652\xC653\
             \\xC654\xC655\xC656\xC657\xC658\xC659\xC65A\xC65B\xC65C\xC65D\
             \\xC65E\xC65F\xC660\xC661\xC662\xC663\xC664\xC665\xC666\xC667\
             \\xC668\xC669\xC66A\xC66B\xC66C\xC66D\xC66E\xC66F\xC670\xC671\
             \\xC672\xC673\xC674\xC675\xC676\xC677\xC678\xC679\xC67A\xC67B\
             \\xC67C\xC67D\xC67E\xC67F\xC680\xC681\xC682\xC683\xC684\xC685\
             \\xC686\xC687\xC688\xC689\xC68A\xC68B\xC68C\xC68D\xC68E\xC68F\
             \\xC690\xC691\xC692\xC693\xC694\xC695\xC696\xC697\xC698\xC699\
             \\xC69A\xC69B\xC69C\xC69D\xC69E\xC69F\xC6A0\xC6A1\xC6A2\xC6A3\
             \\xC6A4\xC6A5\xC6A6\xC6A7\xC6A8\xC6A9\xC6AA\xC6AB\xC6AC\xC6AD\
             \\xC6AE\xC6AF\xC6B0\xC6B1\xC6B2\xC6B3\xC6B4\xC6B5\xC6B6\xC6B7\
             \\xC6B8\xC6B9\xC6BA\xC6BB\xC6BC\xC6BD\xC6BE\xC6BF\xC6C0\xC6C1\
             \\xC6C2\xC6C3\xC6C4\xC6C5\xC6C6\xC6C7\xC6C8\xC6C9\xC6CA\xC6CB\
             \\xC6CC\xC6CD\xC6CE\xC6CF\xC6D0\xC6D1\xC6D2\xC6D3\xC6D4\xC6D5\
             \\xC6D6\xC6D7\xC6D8\xC6D9\xC6DA\xC6DB\xC6DC\xC6DD\xC6DE\xC6DF\
             \\xC6E0\xC6E1\xC6E2\xC6E3\xC6E4\xC6E5\xC6E6\xC6E7\xC6E8\xC6E9\
             \\xC6EA\xC6EB\xC6EC\xC6ED\xC6EE\xC6EF\xC6F0\xC6F1\xC6F2\xC6F3\
             \\xC6F4\xC6F5\xC6F6\xC6F7\xC6F8\xC6F9\xC6FA\xC6FB\xC6FC\xC6FD\
             \\xC6FE\xC6FF\xC700\xC701\xC702\xC703\xC704\xC705\xC706\xC707\
             \\xC708\xC709\xC70A\xC70B\xC70C\xC70D\xC70E\xC70F\xC710\xC711\
             \\xC712\xC713\xC714\xC715\xC716\xC717\xC718\xC719\xC71A\xC71B\
             \\xC71C\xC71D\xC71E\xC71F\xC720\xC721\xC722\xC723\xC724\xC725\
             \\xC726\xC727\xC728\xC729\xC72A\xC72B\xC72C\xC72D\xC72E\xC72F\
             \\xC730\xC731\xC732\xC733\xC734\xC735\xC736\xC737\xC738\xC739\
             \\xC73A\xC73B\xC73C\xC73D\xC73E\xC73F\xC740\xC741\xC742\xC743\
             \\xC744\xC745\xC746\xC747\xC748\xC749\xC74A\xC74B\xC74C\xC74D\
             \\xC74E\xC74F\xC750\xC751\xC752\xC753\xC754\xC755\xC756\xC757\
             \\xC758\xC759\xC75A\xC75B\xC75C\xC75D\xC75E\xC75F\xC760\xC761\
             \\xC762\xC763\xC764\xC765\xC766\xC767\xC768\xC769\xC76A\xC76B\
             \\xC76C\xC76D\xC76E\xC76F\xC770\xC771\xC772\xC773\xC774\xC775\
             \\xC776\xC777\xC778\xC779\xC77A\xC77B\xC77C\xC77D\xC77E\xC77F\
             \\xC780\xC781\xC782\xC783\xC784\xC785\xC786\xC787\xC788\xC789\
             \\xC78A\xC78B\xC78C\xC78D\xC78E\xC78F\xC790\xC791\xC792\xC793\
             \\xC794\xC795\xC796\xC797\xC798\xC799\xC79A\xC79B\xC79C\xC79D\
             \\xC79E\xC79F\xC7A0\xC7A1\xC7A2\xC7A3\xC7A4\xC7A5\xC7A6\xC7A7\
             \\xC7A8\xC7A9\xC7AA\xC7AB\xC7AC\xC7AD\xC7AE\xC7AF\xC7B0\xC7B1\
             \\xC7B2\xC7B3\xC7B4\xC7B5\xC7B6\xC7B7\xC7B8\xC7B9\xC7BA\xC7BB\
             \\xC7BC\xC7BD\xC7BE\xC7BF\xC7C0\xC7C1\xC7C2\xC7C3\xC7C4\xC7C5\
             \\xC7C6\xC7C7\xC7C8\xC7C9\xC7CA\xC7CB\xC7CC\xC7CD\xC7CE\xC7CF\
             \\xC7D0\xC7D1\xC7D2\xC7D3\xC7D4\xC7D5\xC7D6\xC7D7\xC7D8\xC7D9\
             \\xC7DA\xC7DB\xC7DC\xC7DD\xC7DE\xC7DF\xC7E0\xC7E1\xC7E2\xC7E3\
             \\xC7E4\xC7E5\xC7E6\xC7E7\xC7E8\xC7E9\xC7EA\xC7EB\xC7EC\xC7ED\
             \\xC7EE\xC7EF\xC7F0\xC7F1\xC7F2\xC7F3\xC7F4\xC7F5\xC7F6\xC7F7\
             \\xC7F8\xC7F9\xC7FA\xC7FB\xC7FC\xC7FD\xC7FE\xC7FF\xC800\xC801\
             \\xC802\xC803\xC804\xC805\xC806\xC807\xC808\xC809\xC80A\xC80B\
             \\xC80C\xC80D\xC80E\xC80F\xC810\xC811\xC812\xC813\xC814\xC815\
             \\xC816\xC817\xC818\xC819\xC81A\xC81B\xC81C\xC81D\xC81E\xC81F\
             \\xC820\xC821\xC822\xC823\xC824\xC825\xC826\xC827\xC828\xC829\
             \\xC82A\xC82B\xC82C\xC82D\xC82E\xC82F\xC830\xC831\xC832\xC833\
             \\xC834\xC835\xC836\xC837\xC838\xC839\xC83A\xC83B\xC83C\xC83D\
             \\xC83E\xC83F\xC840\xC841\xC842\xC843\xC844\xC845\xC846\xC847\
             \\xC848\xC849\xC84A\xC84B\xC84C\xC84D\xC84E\xC84F\xC850\xC851\
             \\xC852\xC853\xC854\xC855\xC856\xC857\xC858\xC859\xC85A\xC85B\
             \\xC85C\xC85D\xC85E\xC85F\xC860\xC861\xC862\xC863\xC864\xC865\
             \\xC866\xC867\xC868\xC869\xC86A\xC86B\xC86C\xC86D\xC86E\xC86F\
             \\xC870\xC871\xC872\xC873\xC874\xC875\xC876\xC877\xC878\xC879\
             \\xC87A\xC87B\xC87C\xC87D\xC87E\xC87F\xC880\xC881\xC882\xC883\
             \\xC884\xC885\xC886\xC887\xC888\xC889\xC88A\xC88B\xC88C\xC88D\
             \\xC88E\xC88F\xC890\xC891\xC892\xC893\xC894\xC895\xC896\xC897\
             \\xC898\xC899\xC89A\xC89B\xC89C\xC89D\xC89E\xC89F\xC8A0\xC8A1\
             \\xC8A2\xC8A3\xC8A4\xC8A5\xC8A6\xC8A7\xC8A8\xC8A9\xC8AA\xC8AB\
             \\xC8AC\xC8AD\xC8AE\xC8AF\xC8B0\xC8B1\xC8B2\xC8B3\xC8B4\xC8B5\
             \\xC8B6\xC8B7\xC8B8\xC8B9\xC8BA\xC8BB\xC8BC\xC8BD\xC8BE\xC8BF\
             \\xC8C0\xC8C1\xC8C2\xC8C3\xC8C4\xC8C5\xC8C6\xC8C7\xC8C8\xC8C9\
             \\xC8CA\xC8CB\xC8CC\xC8CD\xC8CE\xC8CF\xC8D0\xC8D1\xC8D2\xC8D3\
             \\xC8D4\xC8D5\xC8D6\xC8D7\xC8D8\xC8D9\xC8DA\xC8DB\xC8DC\xC8DD\
             \\xC8DE\xC8DF\xC8E0\xC8E1\xC8E2\xC8E3\xC8E4\xC8E5\xC8E6\xC8E7\
             \\xC8E8\xC8E9\xC8EA\xC8EB\xC8EC\xC8ED\xC8EE\xC8EF\xC8F0\xC8F1\
             \\xC8F2\xC8F3\xC8F4\xC8F5\xC8F6\xC8F7\xC8F8\xC8F9\xC8FA\xC8FB\
             \\xC8FC\xC8FD\xC8FE\xC8FF\xC900\xC901\xC902\xC903\xC904\xC905\
             \\xC906\xC907\xC908\xC909\xC90A\xC90B\xC90C\xC90D\xC90E\xC90F\
             \\xC910\xC911\xC912\xC913\xC914\xC915\xC916\xC917\xC918\xC919\
             \\xC91A\xC91B\xC91C\xC91D\xC91E\xC91F\xC920\xC921\xC922\xC923\
             \\xC924\xC925\xC926\xC927\xC928\xC929\xC92A\xC92B\xC92C\xC92D\
             \\xC92E\xC92F\xC930\xC931\xC932\xC933\xC934\xC935\xC936\xC937\
             \\xC938\xC939\xC93A\xC93B\xC93C\xC93D\xC93E\xC93F\xC940\xC941\
             \\xC942\xC943\xC944\xC945\xC946\xC947\xC948\xC949\xC94A\xC94B\
             \\xC94C\xC94D\xC94E\xC94F\xC950\xC951\xC952\xC953\xC954\xC955\
             \\xC956\xC957\xC958\xC959\xC95A\xC95B\xC95C\xC95D\xC95E\xC95F\
             \\xC960\xC961\xC962\xC963\xC964\xC965\xC966\xC967\xC968\xC969\
             \\xC96A\xC96B\xC96C\xC96D\xC96E\xC96F\xC970\xC971\xC972\xC973\
             \\xC974\xC975\xC976\xC977\xC978\xC979\xC97A\xC97B\xC97C\xC97D\
             \\xC97E\xC97F\xC980\xC981\xC982\xC983\xC984\xC985\xC986\xC987\
             \\xC988\xC989\xC98A\xC98B\xC98C\xC98D\xC98E\xC98F\xC990\xC991\
             \\xC992\xC993\xC994\xC995\xC996\xC997\xC998\xC999\xC99A\xC99B\
             \\xC99C\xC99D\xC99E\xC99F\xC9A0\xC9A1\xC9A2\xC9A3\xC9A4\xC9A5\
             \\xC9A6\xC9A7\xC9A8\xC9A9\xC9AA\xC9AB\xC9AC\xC9AD\xC9AE\xC9AF\
             \\xC9B0\xC9B1\xC9B2\xC9B3\xC9B4\xC9B5\xC9B6\xC9B7\xC9B8\xC9B9\
             \\xC9BA\xC9BB\xC9BC\xC9BD\xC9BE\xC9BF\xC9C0\xC9C1\xC9C2\xC9C3\
             \\xC9C4\xC9C5\xC9C6\xC9C7\xC9C8\xC9C9\xC9CA\xC9CB\xC9CC\xC9CD\
             \\xC9CE\xC9CF\xC9D0\xC9D1\xC9D2\xC9D3\xC9D4\xC9D5\xC9D6\xC9D7\
             \\xC9D8\xC9D9\xC9DA\xC9DB\xC9DC\xC9DD\xC9DE\xC9DF\xC9E0\xC9E1\
             \\xC9E2\xC9E3\xC9E4\xC9E5\xC9E6\xC9E7\xC9E8\xC9E9\xC9EA\xC9EB\
             \\xC9EC\xC9ED\xC9EE\xC9EF\xC9F0\xC9F1\xC9F2\xC9F3\xC9F4\xC9F5\
             \\xC9F6\xC9F7\xC9F8\xC9F9\xC9FA\xC9FB\xC9FC\xC9FD\xC9FE\xC9FF\
             \\xCA00\xCA01\xCA02\xCA03\xCA04\xCA05\xCA06\xCA07\xCA08\xCA09\
             \\xCA0A\xCA0B\xCA0C\xCA0D\xCA0E\xCA0F\xCA10\xCA11\xCA12\xCA13\
             \\xCA14\xCA15\xCA16\xCA17\xCA18\xCA19\xCA1A\xCA1B\xCA1C\xCA1D\
             \\xCA1E\xCA1F\xCA20\xCA21\xCA22\xCA23\xCA24\xCA25\xCA26\xCA27\
             \\xCA28\xCA29\xCA2A\xCA2B\xCA2C\xCA2D\xCA2E\xCA2F\xCA30\xCA31\
             \\xCA32\xCA33\xCA34\xCA35\xCA36\xCA37\xCA38\xCA39\xCA3A\xCA3B\
             \\xCA3C\xCA3D\xCA3E\xCA3F\xCA40\xCA41\xCA42\xCA43\xCA44\xCA45\
             \\xCA46\xCA47\xCA48\xCA49\xCA4A\xCA4B\xCA4C\xCA4D\xCA4E\xCA4F\
             \\xCA50\xCA51\xCA52\xCA53\xCA54\xCA55\xCA56\xCA57\xCA58\xCA59\
             \\xCA5A\xCA5B\xCA5C\xCA5D\xCA5E\xCA5F\xCA60\xCA61\xCA62\xCA63\
             \\xCA64\xCA65\xCA66\xCA67\xCA68\xCA69\xCA6A\xCA6B\xCA6C\xCA6D\
             \\xCA6E\xCA6F\xCA70\xCA71\xCA72\xCA73\xCA74\xCA75\xCA76\xCA77\
             \\xCA78\xCA79\xCA7A\xCA7B\xCA7C\xCA7D\xCA7E\xCA7F\xCA80\xCA81\
             \\xCA82\xCA83\xCA84\xCA85\xCA86\xCA87\xCA88\xCA89\xCA8A\xCA8B\
             \\xCA8C\xCA8D\xCA8E\xCA8F\xCA90\xCA91\xCA92\xCA93\xCA94\xCA95\
             \\xCA96\xCA97\xCA98\xCA99\xCA9A\xCA9B\xCA9C\xCA9D\xCA9E\xCA9F\
             \\xCAA0\xCAA1\xCAA2\xCAA3\xCAA4\xCAA5\xCAA6\xCAA7\xCAA8\xCAA9\
             \\xCAAA\xCAAB\xCAAC\xCAAD\xCAAE\xCAAF\xCAB0\xCAB1\xCAB2\xCAB3\
             \\xCAB4\xCAB5\xCAB6\xCAB7\xCAB8\xCAB9\xCABA\xCABB\xCABC\xCABD\
             \\xCABE\xCABF\xCAC0\xCAC1\xCAC2\xCAC3\xCAC4\xCAC5\xCAC6\xCAC7\
             \\xCAC8\xCAC9\xCACA\xCACB\xCACC\xCACD\xCACE\xCACF\xCAD0\xCAD1\
             \\xCAD2\xCAD3\xCAD4\xCAD5\xCAD6\xCAD7\xCAD8\xCAD9\xCADA\xCADB\
             \\xCADC\xCADD\xCADE\xCADF\xCAE0\xCAE1\xCAE2\xCAE3\xCAE4\xCAE5\
             \\xCAE6\xCAE7\xCAE8\xCAE9\xCAEA\xCAEB\xCAEC\xCAED\xCAEE\xCAEF\
             \\xCAF0\xCAF1\xCAF2\xCAF3\xCAF4\xCAF5\xCAF6\xCAF7\xCAF8\xCAF9\
             \\xCAFA\xCAFB\xCAFC\xCAFD\xCAFE\xCAFF\xCB00\xCB01\xCB02\xCB03\
             \\xCB04\xCB05\xCB06\xCB07\xCB08\xCB09\xCB0A\xCB0B\xCB0C\xCB0D\
             \\xCB0E\xCB0F\xCB10\xCB11\xCB12\xCB13\xCB14\xCB15\xCB16\xCB17\
             \\xCB18\xCB19\xCB1A\xCB1B\xCB1C\xCB1D\xCB1E\xCB1F\xCB20\xCB21\
             \\xCB22\xCB23\xCB24\xCB25\xCB26\xCB27\xCB28\xCB29\xCB2A\xCB2B\
             \\xCB2C\xCB2D\xCB2E\xCB2F\xCB30\xCB31\xCB32\xCB33\xCB34\xCB35\
             \\xCB36\xCB37\xCB38\xCB39\xCB3A\xCB3B\xCB3C\xCB3D\xCB3E\xCB3F\
             \\xCB40\xCB41\xCB42\xCB43\xCB44\xCB45\xCB46\xCB47\xCB48\xCB49\
             \\xCB4A\xCB4B\xCB4C\xCB4D\xCB4E\xCB4F\xCB50\xCB51\xCB52\xCB53\
             \\xCB54\xCB55\xCB56\xCB57\xCB58\xCB59\xCB5A\xCB5B\xCB5C\xCB5D\
             \\xCB5E\xCB5F\xCB60\xCB61\xCB62\xCB63\xCB64\xCB65\xCB66\xCB67\
             \\xCB68\xCB69\xCB6A\xCB6B\xCB6C\xCB6D\xCB6E\xCB6F\xCB70\xCB71\
             \\xCB72\xCB73\xCB74\xCB75\xCB76\xCB77\xCB78\xCB79\xCB7A\xCB7B\
             \\xCB7C\xCB7D\xCB7E\xCB7F\xCB80\xCB81\xCB82\xCB83\xCB84\xCB85\
             \\xCB86\xCB87\xCB88\xCB89\xCB8A\xCB8B\xCB8C\xCB8D\xCB8E\xCB8F\
             \\xCB90\xCB91\xCB92\xCB93\xCB94\xCB95\xCB96\xCB97\xCB98\xCB99\
             \\xCB9A\xCB9B\xCB9C\xCB9D\xCB9E\xCB9F\xCBA0\xCBA1\xCBA2\xCBA3\
             \\xCBA4\xCBA5\xCBA6\xCBA7\xCBA8\xCBA9\xCBAA\xCBAB\xCBAC\xCBAD\
             \\xCBAE\xCBAF\xCBB0\xCBB1\xCBB2\xCBB3\xCBB4\xCBB5\xCBB6\xCBB7\
             \\xCBB8\xCBB9\xCBBA\xCBBB\xCBBC\xCBBD\xCBBE\xCBBF\xCBC0\xCBC1\
             \\xCBC2\xCBC3\xCBC4\xCBC5\xCBC6\xCBC7\xCBC8\xCBC9\xCBCA\xCBCB\
             \\xCBCC\xCBCD\xCBCE\xCBCF\xCBD0\xCBD1\xCBD2\xCBD3\xCBD4\xCBD5\
             \\xCBD6\xCBD7\xCBD8\xCBD9\xCBDA\xCBDB\xCBDC\xCBDD\xCBDE\xCBDF\
             \\xCBE0\xCBE1\xCBE2\xCBE3\xCBE4\xCBE5\xCBE6\xCBE7\xCBE8\xCBE9\
             \\xCBEA\xCBEB\xCBEC\xCBED\xCBEE\xCBEF\xCBF0\xCBF1\xCBF2\xCBF3\
             \\xCBF4\xCBF5\xCBF6\xCBF7\xCBF8\xCBF9\xCBFA\xCBFB\xCBFC\xCBFD\
             \\xCBFE\xCBFF\xCC00\xCC01\xCC02\xCC03\xCC04\xCC05\xCC06\xCC07\
             \\xCC08\xCC09\xCC0A\xCC0B\xCC0C\xCC0D\xCC0E\xCC0F\xCC10\xCC11\
             \\xCC12\xCC13\xCC14\xCC15\xCC16\xCC17\xCC18\xCC19\xCC1A\xCC1B\
             \\xCC1C\xCC1D\xCC1E\xCC1F\xCC20\xCC21\xCC22\xCC23\xCC24\xCC25\
             \\xCC26\xCC27\xCC28\xCC29\xCC2A\xCC2B\xCC2C\xCC2D\xCC2E\xCC2F\
             \\xCC30\xCC31\xCC32\xCC33\xCC34\xCC35\xCC36\xCC37\xCC38\xCC39\
             \\xCC3A\xCC3B\xCC3C\xCC3D\xCC3E\xCC3F\xCC40\xCC41\xCC42\xCC43\
             \\xCC44\xCC45\xCC46\xCC47\xCC48\xCC49\xCC4A\xCC4B\xCC4C\xCC4D\
             \\xCC4E\xCC4F\xCC50\xCC51\xCC52\xCC53\xCC54\xCC55\xCC56\xCC57\
             \\xCC58\xCC59\xCC5A\xCC5B\xCC5C\xCC5D\xCC5E\xCC5F\xCC60\xCC61\
             \\xCC62\xCC63\xCC64\xCC65\xCC66\xCC67\xCC68\xCC69\xCC6A\xCC6B\
             \\xCC6C\xCC6D\xCC6E\xCC6F\xCC70\xCC71\xCC72\xCC73\xCC74\xCC75\
             \\xCC76\xCC77\xCC78\xCC79\xCC7A\xCC7B\xCC7C\xCC7D\xCC7E\xCC7F\
             \\xCC80\xCC81\xCC82\xCC83\xCC84\xCC85\xCC86\xCC87\xCC88\xCC89\
             \\xCC8A\xCC8B\xCC8C\xCC8D\xCC8E\xCC8F\xCC90\xCC91\xCC92\xCC93\
             \\xCC94\xCC95\xCC96\xCC97\xCC98\xCC99\xCC9A\xCC9B\xCC9C\xCC9D\
             \\xCC9E\xCC9F\xCCA0\xCCA1\xCCA2\xCCA3\xCCA4\xCCA5\xCCA6\xCCA7\
             \\xCCA8\xCCA9\xCCAA\xCCAB\xCCAC\xCCAD\xCCAE\xCCAF\xCCB0\xCCB1\
             \\xCCB2\xCCB3\xCCB4\xCCB5\xCCB6\xCCB7\xCCB8\xCCB9\xCCBA\xCCBB\
             \\xCCBC\xCCBD\xCCBE\xCCBF\xCCC0\xCCC1\xCCC2\xCCC3\xCCC4\xCCC5\
             \\xCCC6\xCCC7\xCCC8\xCCC9\xCCCA\xCCCB\xCCCC\xCCCD\xCCCE\xCCCF\
             \\xCCD0\xCCD1\xCCD2\xCCD3\xCCD4\xCCD5\xCCD6\xCCD7\xCCD8\xCCD9\
             \\xCCDA\xCCDB\xCCDC\xCCDD\xCCDE\xCCDF\xCCE0\xCCE1\xCCE2\xCCE3\
             \\xCCE4\xCCE5\xCCE6\xCCE7\xCCE8\xCCE9\xCCEA\xCCEB\xCCEC\xCCED\
             \\xCCEE\xCCEF\xCCF0\xCCF1\xCCF2\xCCF3\xCCF4\xCCF5\xCCF6\xCCF7\
             \\xCCF8\xCCF9\xCCFA\xCCFB\xCCFC\xCCFD\xCCFE\xCCFF\xCD00\xCD01\
             \\xCD02\xCD03\xCD04\xCD05\xCD06\xCD07\xCD08\xCD09\xCD0A\xCD0B\
             \\xCD0C\xCD0D\xCD0E\xCD0F\xCD10\xCD11\xCD12\xCD13\xCD14\xCD15\
             \\xCD16\xCD17\xCD18\xCD19\xCD1A\xCD1B\xCD1C\xCD1D\xCD1E\xCD1F\
             \\xCD20\xCD21\xCD22\xCD23\xCD24\xCD25\xCD26\xCD27\xCD28\xCD29\
             \\xCD2A\xCD2B\xCD2C\xCD2D\xCD2E\xCD2F\xCD30\xCD31\xCD32\xCD33\
             \\xCD34\xCD35\xCD36\xCD37\xCD38\xCD39\xCD3A\xCD3B\xCD3C\xCD3D\
             \\xCD3E\xCD3F\xCD40\xCD41\xCD42\xCD43\xCD44\xCD45\xCD46\xCD47\
             \\xCD48\xCD49\xCD4A\xCD4B\xCD4C\xCD4D\xCD4E\xCD4F\xCD50\xCD51\
             \\xCD52\xCD53\xCD54\xCD55\xCD56\xCD57\xCD58\xCD59\xCD5A\xCD5B\
             \\xCD5C\xCD5D\xCD5E\xCD5F\xCD60\xCD61\xCD62\xCD63\xCD64\xCD65\
             \\xCD66\xCD67\xCD68\xCD69\xCD6A\xCD6B\xCD6C\xCD6D\xCD6E\xCD6F\
             \\xCD70\xCD71\xCD72\xCD73\xCD74\xCD75\xCD76\xCD77\xCD78\xCD79\
             \\xCD7A\xCD7B\xCD7C\xCD7D\xCD7E\xCD7F\xCD80\xCD81\xCD82\xCD83\
             \\xCD84\xCD85\xCD86\xCD87\xCD88\xCD89\xCD8A\xCD8B\xCD8C\xCD8D\
             \\xCD8E\xCD8F\xCD90\xCD91\xCD92\xCD93\xCD94\xCD95\xCD96\xCD97\
             \\xCD98\xCD99\xCD9A\xCD9B\xCD9C\xCD9D\xCD9E\xCD9F\xCDA0\xCDA1\
             \\xCDA2\xCDA3\xCDA4\xCDA5\xCDA6\xCDA7\xCDA8\xCDA9\xCDAA\xCDAB\
             \\xCDAC\xCDAD\xCDAE\xCDAF\xCDB0\xCDB1\xCDB2\xCDB3\xCDB4\xCDB5\
             \\xCDB6\xCDB7\xCDB8\xCDB9\xCDBA\xCDBB\xCDBC\xCDBD\xCDBE\xCDBF\
             \\xCDC0\xCDC1\xCDC2\xCDC3\xCDC4\xCDC5\xCDC6\xCDC7\xCDC8\xCDC9\
             \\xCDCA\xCDCB\xCDCC\xCDCD\xCDCE\xCDCF\xCDD0\xCDD1\xCDD2\xCDD3\
             \\xCDD4\xCDD5\xCDD6\xCDD7\xCDD8\xCDD9\xCDDA\xCDDB\xCDDC\xCDDD\
             \\xCDDE\xCDDF\xCDE0\xCDE1\xCDE2\xCDE3\xCDE4\xCDE5\xCDE6\xCDE7\
             \\xCDE8\xCDE9\xCDEA\xCDEB\xCDEC\xCDED\xCDEE\xCDEF\xCDF0\xCDF1\
             \\xCDF2\xCDF3\xCDF4\xCDF5\xCDF6\xCDF7\xCDF8\xCDF9\xCDFA\xCDFB\
             \\xCDFC\xCDFD\xCDFE\xCDFF\xCE00\xCE01\xCE02\xCE03\xCE04\xCE05\
             \\xCE06\xCE07\xCE08\xCE09\xCE0A\xCE0B\xCE0C\xCE0D\xCE0E\xCE0F\
             \\xCE10\xCE11\xCE12\xCE13\xCE14\xCE15\xCE16\xCE17\xCE18\xCE19\
             \\xCE1A\xCE1B\xCE1C\xCE1D\xCE1E\xCE1F\xCE20\xCE21\xCE22\xCE23\
             \\xCE24\xCE25\xCE26\xCE27\xCE28\xCE29\xCE2A\xCE2B\xCE2C\xCE2D\
             \\xCE2E\xCE2F\xCE30\xCE31\xCE32\xCE33\xCE34\xCE35\xCE36\xCE37\
             \\xCE38\xCE39\xCE3A\xCE3B\xCE3C\xCE3D\xCE3E\xCE3F\xCE40\xCE41\
             \\xCE42\xCE43\xCE44\xCE45\xCE46\xCE47\xCE48\xCE49\xCE4A\xCE4B\
             \\xCE4C\xCE4D\xCE4E\xCE4F\xCE50\xCE51\xCE52\xCE53\xCE54\xCE55\
             \\xCE56\xCE57\xCE58\xCE59\xCE5A\xCE5B\xCE5C\xCE5D\xCE5E\xCE5F\
             \\xCE60\xCE61\xCE62\xCE63\xCE64\xCE65\xCE66\xCE67\xCE68\xCE69\
             \\xCE6A\xCE6B\xCE6C\xCE6D\xCE6E\xCE6F\xCE70\xCE71\xCE72\xCE73\
             \\xCE74\xCE75\xCE76\xCE77\xCE78\xCE79\xCE7A\xCE7B\xCE7C\xCE7D\
             \\xCE7E\xCE7F\xCE80\xCE81\xCE82\xCE83\xCE84\xCE85\xCE86\xCE87\
             \\xCE88\xCE89\xCE8A\xCE8B\xCE8C\xCE8D\xCE8E\xCE8F\xCE90\xCE91\
             \\xCE92\xCE93\xCE94\xCE95\xCE96\xCE97\xCE98\xCE99\xCE9A\xCE9B\
             \\xCE9C\xCE9D\xCE9E\xCE9F\xCEA0\xCEA1\xCEA2\xCEA3\xCEA4\xCEA5\
             \\xCEA6\xCEA7\xCEA8\xCEA9\xCEAA\xCEAB\xCEAC\xCEAD\xCEAE\xCEAF\
             \\xCEB0\xCEB1\xCEB2\xCEB3\xCEB4\xCEB5\xCEB6\xCEB7\xCEB8\xCEB9\
             \\xCEBA\xCEBB\xCEBC\xCEBD\xCEBE\xCEBF\xCEC0\xCEC1\xCEC2\xCEC3\
             \\xCEC4\xCEC5\xCEC6\xCEC7\xCEC8\xCEC9\xCECA\xCECB\xCECC\xCECD\
             \\xCECE\xCECF\xCED0\xCED1\xCED2\xCED3\xCED4\xCED5\xCED6\xCED7\
             \\xCED8\xCED9\xCEDA\xCEDB\xCEDC\xCEDD\xCEDE\xCEDF\xCEE0\xCEE1\
             \\xCEE2\xCEE3\xCEE4\xCEE5\xCEE6\xCEE7\xCEE8\xCEE9\xCEEA\xCEEB\
             \\xCEEC\xCEED\xCEEE\xCEEF\xCEF0\xCEF1\xCEF2\xCEF3\xCEF4\xCEF5\
             \\xCEF6\xCEF7\xCEF8\xCEF9\xCEFA\xCEFB\xCEFC\xCEFD\xCEFE\xCEFF\
             \\xCF00\xCF01\xCF02\xCF03\xCF04\xCF05\xCF06\xCF07\xCF08\xCF09\
             \\xCF0A\xCF0B\xCF0C\xCF0D\xCF0E\xCF0F\xCF10\xCF11\xCF12\xCF13\
             \\xCF14\xCF15\xCF16\xCF17\xCF18\xCF19\xCF1A\xCF1B\xCF1C\xCF1D\
             \\xCF1E\xCF1F\xCF20\xCF21\xCF22\xCF23\xCF24\xCF25\xCF26\xCF27\
             \\xCF28\xCF29\xCF2A\xCF2B\xCF2C\xCF2D\xCF2E\xCF2F\xCF30\xCF31\
             \\xCF32\xCF33\xCF34\xCF35\xCF36\xCF37\xCF38\xCF39\xCF3A\xCF3B\
             \\xCF3C\xCF3D\xCF3E\xCF3F\xCF40\xCF41\xCF42\xCF43\xCF44\xCF45\
             \\xCF46\xCF47\xCF48\xCF49\xCF4A\xCF4B\xCF4C\xCF4D\xCF4E\xCF4F\
             \\xCF50\xCF51\xCF52\xCF53\xCF54\xCF55\xCF56\xCF57\xCF58\xCF59\
             \\xCF5A\xCF5B\xCF5C\xCF5D\xCF5E\xCF5F\xCF60\xCF61\xCF62\xCF63\
             \\xCF64\xCF65\xCF66\xCF67\xCF68\xCF69\xCF6A\xCF6B\xCF6C\xCF6D\
             \\xCF6E\xCF6F\xCF70\xCF71\xCF72\xCF73\xCF74\xCF75\xCF76\xCF77\
             \\xCF78\xCF79\xCF7A\xCF7B\xCF7C\xCF7D\xCF7E\xCF7F\xCF80\xCF81\
             \\xCF82\xCF83\xCF84\xCF85\xCF86\xCF87\xCF88\xCF89\xCF8A\xCF8B\
             \\xCF8C\xCF8D\xCF8E\xCF8F\xCF90\xCF91\xCF92\xCF93\xCF94\xCF95\
             \\xCF96\xCF97\xCF98\xCF99\xCF9A\xCF9B\xCF9C\xCF9D\xCF9E\xCF9F\
             \\xCFA0\xCFA1\xCFA2\xCFA3\xCFA4\xCFA5\xCFA6\xCFA7\xCFA8\xCFA9\
             \\xCFAA\xCFAB\xCFAC\xCFAD\xCFAE\xCFAF\xCFB0\xCFB1\xCFB2\xCFB3\
             \\xCFB4\xCFB5\xCFB6\xCFB7\xCFB8\xCFB9\xCFBA\xCFBB\xCFBC\xCFBD\
             \\xCFBE\xCFBF\xCFC0\xCFC1\xCFC2\xCFC3\xCFC4\xCFC5\xCFC6\xCFC7\
             \\xCFC8\xCFC9\xCFCA\xCFCB\xCFCC\xCFCD\xCFCE\xCFCF\xCFD0\xCFD1\
             \\xCFD2\xCFD3\xCFD4\xCFD5\xCFD6\xCFD7\xCFD8\xCFD9\xCFDA\xCFDB\
             \\xCFDC\xCFDD\xCFDE\xCFDF\xCFE0\xCFE1\xCFE2\xCFE3\xCFE4\xCFE5\
             \\xCFE6\xCFE7\xCFE8\xCFE9\xCFEA\xCFEB\xCFEC\xCFED\xCFEE\xCFEF\
             \\xCFF0\xCFF1\xCFF2\xCFF3\xCFF4\xCFF5\xCFF6\xCFF7\xCFF8\xCFF9\
             \\xCFFA\xCFFB\xCFFC\xCFFD\xCFFE\xCFFF\xD000\xD001\xD002\xD003\
             \\xD004\xD005\xD006\xD007\xD008\xD009\xD00A\xD00B\xD00C\xD00D\
             \\xD00E\xD00F\xD010\xD011\xD012\xD013\xD014\xD015\xD016\xD017\
             \\xD018\xD019\xD01A\xD01B\xD01C\xD01D\xD01E\xD01F\xD020\xD021\
             \\xD022\xD023\xD024\xD025\xD026\xD027\xD028\xD029\xD02A\xD02B\
             \\xD02C\xD02D\xD02E\xD02F\xD030\xD031\xD032\xD033\xD034\xD035\
             \\xD036\xD037\xD038\xD039\xD03A\xD03B\xD03C\xD03D\xD03E\xD03F\
             \\xD040\xD041\xD042\xD043\xD044\xD045\xD046\xD047\xD048\xD049\
             \\xD04A\xD04B\xD04C\xD04D\xD04E\xD04F\xD050\xD051\xD052\xD053\
             \\xD054\xD055\xD056\xD057\xD058\xD059\xD05A\xD05B\xD05C\xD05D\
             \\xD05E\xD05F\xD060\xD061\xD062\xD063\xD064\xD065\xD066\xD067\
             \\xD068\xD069\xD06A\xD06B\xD06C\xD06D\xD06E\xD06F\xD070\xD071\
             \\xD072\xD073\xD074\xD075\xD076\xD077\xD078\xD079\xD07A\xD07B\
             \\xD07C\xD07D\xD07E\xD07F\xD080\xD081\xD082\xD083\xD084\xD085\
             \\xD086\xD087\xD088\xD089\xD08A\xD08B\xD08C\xD08D\xD08E\xD08F\
             \\xD090\xD091\xD092\xD093\xD094\xD095\xD096\xD097\xD098\xD099\
             \\xD09A\xD09B\xD09C\xD09D\xD09E\xD09F\xD0A0\xD0A1\xD0A2\xD0A3\
             \\xD0A4\xD0A5\xD0A6\xD0A7\xD0A8\xD0A9\xD0AA\xD0AB\xD0AC\xD0AD\
             \\xD0AE\xD0AF\xD0B0\xD0B1\xD0B2\xD0B3\xD0B4\xD0B5\xD0B6\xD0B7\
             \\xD0B8\xD0B9\xD0BA\xD0BB\xD0BC\xD0BD\xD0BE\xD0BF\xD0C0\xD0C1\
             \\xD0C2\xD0C3\xD0C4\xD0C5\xD0C6\xD0C7\xD0C8\xD0C9\xD0CA\xD0CB\
             \\xD0CC\xD0CD\xD0CE\xD0CF\xD0D0\xD0D1\xD0D2\xD0D3\xD0D4\xD0D5\
             \\xD0D6\xD0D7\xD0D8\xD0D9\xD0DA\xD0DB\xD0DC\xD0DD\xD0DE\xD0DF\
             \\xD0E0\xD0E1\xD0E2\xD0E3\xD0E4\xD0E5\xD0E6\xD0E7\xD0E8\xD0E9\
             \\xD0EA\xD0EB\xD0EC\xD0ED\xD0EE\xD0EF\xD0F0\xD0F1\xD0F2\xD0F3\
             \\xD0F4\xD0F5\xD0F6\xD0F7\xD0F8\xD0F9\xD0FA\xD0FB\xD0FC\xD0FD\
             \\xD0FE\xD0FF\xD100\xD101\xD102\xD103\xD104\xD105\xD106\xD107\
             \\xD108\xD109\xD10A\xD10B\xD10C\xD10D\xD10E\xD10F\xD110\xD111\
             \\xD112\xD113\xD114\xD115\xD116\xD117\xD118\xD119\xD11A\xD11B\
             \\xD11C\xD11D\xD11E\xD11F\xD120\xD121\xD122\xD123\xD124\xD125\
             \\xD126\xD127\xD128\xD129\xD12A\xD12B\xD12C\xD12D\xD12E\xD12F\
             \\xD130\xD131\xD132\xD133\xD134\xD135\xD136\xD137\xD138\xD139\
             \\xD13A\xD13B\xD13C\xD13D\xD13E\xD13F\xD140\xD141\xD142\xD143\
             \\xD144\xD145\xD146\xD147\xD148\xD149\xD14A\xD14B\xD14C\xD14D\
             \\xD14E\xD14F\xD150\xD151\xD152\xD153\xD154\xD155\xD156\xD157\
             \\xD158\xD159\xD15A\xD15B\xD15C\xD15D\xD15E\xD15F\xD160\xD161\
             \\xD162\xD163\xD164\xD165\xD166\xD167\xD168\xD169\xD16A\xD16B\
             \\xD16C\xD16D\xD16E\xD16F\xD170\xD171\xD172\xD173\xD174\xD175\
             \\xD176\xD177\xD178\xD179\xD17A\xD17B\xD17C\xD17D\xD17E\xD17F\
             \\xD180\xD181\xD182\xD183\xD184\xD185\xD186\xD187\xD188\xD189\
             \\xD18A\xD18B\xD18C\xD18D\xD18E\xD18F\xD190\xD191\xD192\xD193\
             \\xD194\xD195\xD196\xD197\xD198\xD199\xD19A\xD19B\xD19C\xD19D\
             \\xD19E\xD19F\xD1A0\xD1A1\xD1A2\xD1A3\xD1A4\xD1A5\xD1A6\xD1A7\
             \\xD1A8\xD1A9\xD1AA\xD1AB\xD1AC\xD1AD\xD1AE\xD1AF\xD1B0\xD1B1\
             \\xD1B2\xD1B3\xD1B4\xD1B5\xD1B6\xD1B7\xD1B8\xD1B9\xD1BA\xD1BB\
             \\xD1BC\xD1BD\xD1BE\xD1BF\xD1C0\xD1C1\xD1C2\xD1C3\xD1C4\xD1C5\
             \\xD1C6\xD1C7\xD1C8\xD1C9\xD1CA\xD1CB\xD1CC\xD1CD\xD1CE\xD1CF\
             \\xD1D0\xD1D1\xD1D2\xD1D3\xD1D4\xD1D5\xD1D6\xD1D7\xD1D8\xD1D9\
             \\xD1DA\xD1DB\xD1DC\xD1DD\xD1DE\xD1DF\xD1E0\xD1E1\xD1E2\xD1E3\
             \\xD1E4\xD1E5\xD1E6\xD1E7\xD1E8\xD1E9\xD1EA\xD1EB\xD1EC\xD1ED\
             \\xD1EE\xD1EF\xD1F0\xD1F1\xD1F2\xD1F3\xD1F4\xD1F5\xD1F6\xD1F7\
             \\xD1F8\xD1F9\xD1FA\xD1FB\xD1FC\xD1FD\xD1FE\xD1FF\xD200\xD201\
             \\xD202\xD203\xD204\xD205\xD206\xD207\xD208\xD209\xD20A\xD20B\
             \\xD20C\xD20D\xD20E\xD20F\xD210\xD211\xD212\xD213\xD214\xD215\
             \\xD216\xD217\xD218\xD219\xD21A\xD21B\xD21C\xD21D\xD21E\xD21F\
             \\xD220\xD221\xD222\xD223\xD224\xD225\xD226\xD227\xD228\xD229\
             \\xD22A\xD22B\xD22C\xD22D\xD22E\xD22F\xD230\xD231\xD232\xD233\
             \\xD234\xD235\xD236\xD237\xD238\xD239\xD23A\xD23B\xD23C\xD23D\
             \\xD23E\xD23F\xD240\xD241\xD242\xD243\xD244\xD245\xD246\xD247\
             \\xD248\xD249\xD24A\xD24B\xD24C\xD24D\xD24E\xD24F\xD250\xD251\
             \\xD252\xD253\xD254\xD255\xD256\xD257\xD258\xD259\xD25A\xD25B\
             \\xD25C\xD25D\xD25E\xD25F\xD260\xD261\xD262\xD263\xD264\xD265\
             \\xD266\xD267\xD268\xD269\xD26A\xD26B\xD26C\xD26D\xD26E\xD26F\
             \\xD270\xD271\xD272\xD273\xD274\xD275\xD276\xD277\xD278\xD279\
             \\xD27A\xD27B\xD27C\xD27D\xD27E\xD27F\xD280\xD281\xD282\xD283\
             \\xD284\xD285\xD286\xD287\xD288\xD289\xD28A\xD28B\xD28C\xD28D\
             \\xD28E\xD28F\xD290\xD291\xD292\xD293\xD294\xD295\xD296\xD297\
             \\xD298\xD299\xD29A\xD29B\xD29C\xD29D\xD29E\xD29F\xD2A0\xD2A1\
             \\xD2A2\xD2A3\xD2A4\xD2A5\xD2A6\xD2A7\xD2A8\xD2A9\xD2AA\xD2AB\
             \\xD2AC\xD2AD\xD2AE\xD2AF\xD2B0\xD2B1\xD2B2\xD2B3\xD2B4\xD2B5\
             \\xD2B6\xD2B7\xD2B8\xD2B9\xD2BA\xD2BB\xD2BC\xD2BD\xD2BE\xD2BF\
             \\xD2C0\xD2C1\xD2C2\xD2C3\xD2C4\xD2C5\xD2C6\xD2C7\xD2C8\xD2C9\
             \\xD2CA\xD2CB\xD2CC\xD2CD\xD2CE\xD2CF\xD2D0\xD2D1\xD2D2\xD2D3\
             \\xD2D4\xD2D5\xD2D6\xD2D7\xD2D8\xD2D9\xD2DA\xD2DB\xD2DC\xD2DD\
             \\xD2DE\xD2DF\xD2E0\xD2E1\xD2E2\xD2E3\xD2E4\xD2E5\xD2E6\xD2E7\
             \\xD2E8\xD2E9\xD2EA\xD2EB\xD2EC\xD2ED\xD2EE\xD2EF\xD2F0\xD2F1\
             \\xD2F2\xD2F3\xD2F4\xD2F5\xD2F6\xD2F7\xD2F8\xD2F9\xD2FA\xD2FB\
             \\xD2FC\xD2FD\xD2FE\xD2FF\xD300\xD301\xD302\xD303\xD304\xD305\
             \\xD306\xD307\xD308\xD309\xD30A\xD30B\xD30C\xD30D\xD30E\xD30F\
             \\xD310\xD311\xD312\xD313\xD314\xD315\xD316\xD317\xD318\xD319\
             \\xD31A\xD31B\xD31C\xD31D\xD31E\xD31F\xD320\xD321\xD322\xD323\
             \\xD324\xD325\xD326\xD327\xD328\xD329\xD32A\xD32B\xD32C\xD32D\
             \\xD32E\xD32F\xD330\xD331\xD332\xD333\xD334\xD335\xD336\xD337\
             \\xD338\xD339\xD33A\xD33B\xD33C\xD33D\xD33E\xD33F\xD340\xD341\
             \\xD342\xD343\xD344\xD345\xD346\xD347\xD348\xD349\xD34A\xD34B\
             \\xD34C\xD34D\xD34E\xD34F\xD350\xD351\xD352\xD353\xD354\xD355\
             \\xD356\xD357\xD358\xD359\xD35A\xD35B\xD35C\xD35D\xD35E\xD35F\
             \\xD360\xD361\xD362\xD363\xD364\xD365\xD366\xD367\xD368\xD369\
             \\xD36A\xD36B\xD36C\xD36D\xD36E\xD36F\xD370\xD371\xD372\xD373\
             \\xD374\xD375\xD376\xD377\xD378\xD379\xD37A\xD37B\xD37C\xD37D\
             \\xD37E\xD37F\xD380\xD381\xD382\xD383\xD384\xD385\xD386\xD387\
             \\xD388\xD389\xD38A\xD38B\xD38C\xD38D\xD38E\xD38F\xD390\xD391\
             \\xD392\xD393\xD394\xD395\xD396\xD397\xD398\xD399\xD39A\xD39B\
             \\xD39C\xD39D\xD39E\xD39F\xD3A0\xD3A1\xD3A2\xD3A3\xD3A4\xD3A5\
             \\xD3A6\xD3A7\xD3A8\xD3A9\xD3AA\xD3AB\xD3AC\xD3AD\xD3AE\xD3AF\
             \\xD3B0\xD3B1\xD3B2\xD3B3\xD3B4\xD3B5\xD3B6\xD3B7\xD3B8\xD3B9\
             \\xD3BA\xD3BB\xD3BC\xD3BD\xD3BE\xD3BF\xD3C0\xD3C1\xD3C2\xD3C3\
             \\xD3C4\xD3C5\xD3C6\xD3C7\xD3C8\xD3C9\xD3CA\xD3CB\xD3CC\xD3CD\
             \\xD3CE\xD3CF\xD3D0\xD3D1\xD3D2\xD3D3\xD3D4\xD3D5\xD3D6\xD3D7\
             \\xD3D8\xD3D9\xD3DA\xD3DB\xD3DC\xD3DD\xD3DE\xD3DF\xD3E0\xD3E1\
             \\xD3E2\xD3E3\xD3E4\xD3E5\xD3E6\xD3E7\xD3E8\xD3E9\xD3EA\xD3EB\
             \\xD3EC\xD3ED\xD3EE\xD3EF\xD3F0\xD3F1\xD3F2\xD3F3\xD3F4\xD3F5\
             \\xD3F6\xD3F7\xD3F8\xD3F9\xD3FA\xD3FB\xD3FC\xD3FD\xD3FE\xD3FF\
             \\xD400\xD401\xD402\xD403\xD404\xD405\xD406\xD407\xD408\xD409\
             \\xD40A\xD40B\xD40C\xD40D\xD40E\xD40F\xD410\xD411\xD412\xD413\
             \\xD414\xD415\xD416\xD417\xD418\xD419\xD41A\xD41B\xD41C\xD41D\
             \\xD41E\xD41F\xD420\xD421\xD422\xD423\xD424\xD425\xD426\xD427\
             \\xD428\xD429\xD42A\xD42B\xD42C\xD42D\xD42E\xD42F\xD430\xD431\
             \\xD432\xD433\xD434\xD435\xD436\xD437\xD438\xD439\xD43A\xD43B\
             \\xD43C\xD43D\xD43E\xD43F\xD440\xD441\xD442\xD443\xD444\xD445\
             \\xD446\xD447\xD448\xD449\xD44A\xD44B\xD44C\xD44D\xD44E\xD44F\
             \\xD450\xD451\xD452\xD453\xD454\xD455\xD456\xD457\xD458\xD459\
             \\xD45A\xD45B\xD45C\xD45D\xD45E\xD45F\xD460\xD461\xD462\xD463\
             \\xD464\xD465\xD466\xD467\xD468\xD469\xD46A\xD46B\xD46C\xD46D\
             \\xD46E\xD46F\xD470\xD471\xD472\xD473\xD474\xD475\xD476\xD477\
             \\xD478\xD479\xD47A\xD47B\xD47C\xD47D\xD47E\xD47F\xD480\xD481\
             \\xD482\xD483\xD484\xD485\xD486\xD487\xD488\xD489\xD48A\xD48B\
             \\xD48C\xD48D\xD48E\xD48F\xD490\xD491\xD492\xD493\xD494\xD495\
             \\xD496\xD497\xD498\xD499\xD49A\xD49B\xD49C\xD49D\xD49E\xD49F\
             \\xD4A0\xD4A1\xD4A2\xD4A3\xD4A4\xD4A5\xD4A6\xD4A7\xD4A8\xD4A9\
             \\xD4AA\xD4AB\xD4AC\xD4AD\xD4AE\xD4AF\xD4B0\xD4B1\xD4B2\xD4B3\
             \\xD4B4\xD4B5\xD4B6\xD4B7\xD4B8\xD4B9\xD4BA\xD4BB\xD4BC\xD4BD\
             \\xD4BE\xD4BF\xD4C0\xD4C1\xD4C2\xD4C3\xD4C4\xD4C5\xD4C6\xD4C7\
             \\xD4C8\xD4C9\xD4CA\xD4CB\xD4CC\xD4CD\xD4CE\xD4CF\xD4D0\xD4D1\
             \\xD4D2\xD4D3\xD4D4\xD4D5\xD4D6\xD4D7\xD4D8\xD4D9\xD4DA\xD4DB\
             \\xD4DC\xD4DD\xD4DE\xD4DF\xD4E0\xD4E1\xD4E2\xD4E3\xD4E4\xD4E5\
             \\xD4E6\xD4E7\xD4E8\xD4E9\xD4EA\xD4EB\xD4EC\xD4ED\xD4EE\xD4EF\
             \\xD4F0\xD4F1\xD4F2\xD4F3\xD4F4\xD4F5\xD4F6\xD4F7\xD4F8\xD4F9\
             \\xD4FA\xD4FB\xD4FC\xD4FD\xD4FE\xD4FF\xD500\xD501\xD502\xD503\
             \\xD504\xD505\xD506\xD507\xD508\xD509\xD50A\xD50B\xD50C\xD50D\
             \\xD50E\xD50F\xD510\xD511\xD512\xD513\xD514\xD515\xD516\xD517\
             \\xD518\xD519\xD51A\xD51B\xD51C\xD51D\xD51E\xD51F\xD520\xD521\
             \\xD522\xD523\xD524\xD525\xD526\xD527\xD528\xD529\xD52A\xD52B\
             \\xD52C\xD52D\xD52E\xD52F\xD530\xD531\xD532\xD533\xD534\xD535\
             \\xD536\xD537\xD538\xD539\xD53A\xD53B\xD53C\xD53D\xD53E\xD53F\
             \\xD540\xD541\xD542\xD543\xD544\xD545\xD546\xD547\xD548\xD549\
             \\xD54A\xD54B\xD54C\xD54D\xD54E\xD54F\xD550\xD551\xD552\xD553\
             \\xD554\xD555\xD556\xD557\xD558\xD559\xD55A\xD55B\xD55C\xD55D\
             \\xD55E\xD55F\xD560\xD561\xD562\xD563\xD564\xD565\xD566\xD567\
             \\xD568\xD569\xD56A\xD56B\xD56C\xD56D\xD56E\xD56F\xD570\xD571\
             \\xD572\xD573\xD574\xD575\xD576\xD577\xD578\xD579\xD57A\xD57B\
             \\xD57C\xD57D\xD57E\xD57F\xD580\xD581\xD582\xD583\xD584\xD585\
             \\xD586\xD587\xD588\xD589\xD58A\xD58B\xD58C\xD58D\xD58E\xD58F\
             \\xD590\xD591\xD592\xD593\xD594\xD595\xD596\xD597\xD598\xD599\
             \\xD59A\xD59B\xD59C\xD59D\xD59E\xD59F\xD5A0\xD5A1\xD5A2\xD5A3\
             \\xD5A4\xD5A5\xD5A6\xD5A7\xD5A8\xD5A9\xD5AA\xD5AB\xD5AC\xD5AD\
             \\xD5AE\xD5AF\xD5B0\xD5B1\xD5B2\xD5B3\xD5B4\xD5B5\xD5B6\xD5B7\
             \\xD5B8\xD5B9\xD5BA\xD5BB\xD5BC\xD5BD\xD5BE\xD5BF\xD5C0\xD5C1\
             \\xD5C2\xD5C3\xD5C4\xD5C5\xD5C6\xD5C7\xD5C8\xD5C9\xD5CA\xD5CB\
             \\xD5CC\xD5CD\xD5CE\xD5CF\xD5D0\xD5D1\xD5D2\xD5D3\xD5D4\xD5D5\
             \\xD5D6\xD5D7\xD5D8\xD5D9\xD5DA\xD5DB\xD5DC\xD5DD\xD5DE\xD5DF\
             \\xD5E0\xD5E1\xD5E2\xD5E3\xD5E4\xD5E5\xD5E6\xD5E7\xD5E8\xD5E9\
             \\xD5EA\xD5EB\xD5EC\xD5ED\xD5EE\xD5EF\xD5F0\xD5F1\xD5F2\xD5F3\
             \\xD5F4\xD5F5\xD5F6\xD5F7\xD5F8\xD5F9\xD5FA\xD5FB\xD5FC\xD5FD\
             \\xD5FE\xD5FF\xD600\xD601\xD602\xD603\xD604\xD605\xD606\xD607\
             \\xD608\xD609\xD60A\xD60B\xD60C\xD60D\xD60E\xD60F\xD610\xD611\
             \\xD612\xD613\xD614\xD615\xD616\xD617\xD618\xD619\xD61A\xD61B\
             \\xD61C\xD61D\xD61E\xD61F\xD620\xD621\xD622\xD623\xD624\xD625\
             \\xD626\xD627\xD628\xD629\xD62A\xD62B\xD62C\xD62D\xD62E\xD62F\
             \\xD630\xD631\xD632\xD633\xD634\xD635\xD636\xD637\xD638\xD639\
             \\xD63A\xD63B\xD63C\xD63D\xD63E\xD63F\xD640\xD641\xD642\xD643\
             \\xD644\xD645\xD646\xD647\xD648\xD649\xD64A\xD64B\xD64C\xD64D\
             \\xD64E\xD64F\xD650\xD651\xD652\xD653\xD654\xD655\xD656\xD657\
             \\xD658\xD659\xD65A\xD65B\xD65C\xD65D\xD65E\xD65F\xD660\xD661\
             \\xD662\xD663\xD664\xD665\xD666\xD667\xD668\xD669\xD66A\xD66B\
             \\xD66C\xD66D\xD66E\xD66F\xD670\xD671\xD672\xD673\xD674\xD675\
             \\xD676\xD677\xD678\xD679\xD67A\xD67B\xD67C\xD67D\xD67E\xD67F\
             \\xD680\xD681\xD682\xD683\xD684\xD685\xD686\xD687\xD688\xD689\
             \\xD68A\xD68B\xD68C\xD68D\xD68E\xD68F\xD690\xD691\xD692\xD693\
             \\xD694\xD695\xD696\xD697\xD698\xD699\xD69A\xD69B\xD69C\xD69D\
             \\xD69E\xD69F\xD6A0\xD6A1\xD6A2\xD6A3\xD6A4\xD6A5\xD6A6\xD6A7\
             \\xD6A8\xD6A9\xD6AA\xD6AB\xD6AC\xD6AD\xD6AE\xD6AF\xD6B0\xD6B1\
             \\xD6B2\xD6B3\xD6B4\xD6B5\xD6B6\xD6B7\xD6B8\xD6B9\xD6BA\xD6BB\
             \\xD6BC\xD6BD\xD6BE\xD6BF\xD6C0\xD6C1\xD6C2\xD6C3\xD6C4\xD6C5\
             \\xD6C6\xD6C7\xD6C8\xD6C9\xD6CA\xD6CB\xD6CC\xD6CD\xD6CE\xD6CF\
             \\xD6D0\xD6D1\xD6D2\xD6D3\xD6D4\xD6D5\xD6D6\xD6D7\xD6D8\xD6D9\
             \\xD6DA\xD6DB\xD6DC\xD6DD\xD6DE\xD6DF\xD6E0\xD6E1\xD6E2\xD6E3\
             \\xD6E4\xD6E5\xD6E6\xD6E7\xD6E8\xD6E9\xD6EA\xD6EB\xD6EC\xD6ED\
             \\xD6EE\xD6EF\xD6F0\xD6F1\xD6F2\xD6F3\xD6F4\xD6F5\xD6F6\xD6F7\
             \\xD6F8\xD6F9\xD6FA\xD6FB\xD6FC\xD6FD\xD6FE\xD6FF\xD700\xD701\
             \\xD702\xD703\xD704\xD705\xD706\xD707\xD708\xD709\xD70A\xD70B\
             \\xD70C\xD70D\xD70E\xD70F\xD710\xD711\xD712\xD713\xD714\xD715\
             \\xD716\xD717\xD718\xD719\xD71A\xD71B\xD71C\xD71D\xD71E\xD71F\
             \\xD720\xD721\xD722\xD723\xD724\xD725\xD726\xD727\xD728\xD729\
             \\xD72A\xD72B\xD72C\xD72D\xD72E\xD72F\xD730\xD731\xD732\xD733\
             \\xD734\xD735\xD736\xD737\xD738\xD739\xD73A\xD73B\xD73C\xD73D\
             \\xD73E\xD73F\xD740\xD741\xD742\xD743\xD744\xD745\xD746\xD747\
             \\xD748\xD749\xD74A\xD74B\xD74C\xD74D\xD74E\xD74F\xD750\xD751\
             \\xD752\xD753\xD754\xD755\xD756\xD757\xD758\xD759\xD75A\xD75B\
             \\xD75C\xD75D\xD75E\xD75F\xD760\xD761\xD762\xD763\xD764\xD765\
             \\xD766\xD767\xD768\xD769\xD76A\xD76B\xD76C\xD76D\xD76E\xD76F\
             \\xD770\xD771\xD772\xD773\xD774\xD775\xD776\xD777\xD778\xD779\
             \\xD77A\xD77B\xD77C\xD77D\xD77E\xD77F\xD780\xD781\xD782\xD783\
             \\xD784\xD785\xD786\xD787\xD788\xD789\xD78A\xD78B\xD78C\xD78D\
             \\xD78E\xD78F\xD790\xD791\xD792\xD793\xD794\xD795\xD796\xD797\
             \\xD798\xD799\xD79A\xD79B\xD79C\xD79D\xD79E\xD79F\xD7A0\xD7A1\
             \\xD7A2\xD7A3"
          , _exAuxiliary = Just $ Set.fromList
             "\x1100\x1101\x1102\x1103\x1104\x1105\x1106\x1107\x1108\x1109\
             \\x110A\x110B\x110C\x110D\x110E\x110F\x1110\x1111\x1112\x1161\
             \\x1162\x1163\x1164\x1165\x1166\x1167\x1168\x1169\x116A\x116B\
             \\x116C\x116D\x116E\x116F\x1170\x1171\x1172\x1173\x1174\x1175\
             \\x11A8\x11A9\x11AA\x11AB\x11AC\x11AD\x11AE\x11AF\x11B0\x11B1\
             \\x11B2\x11B3\x11B4\x11B5\x11B6\x11B7\x11B8\x11B9\x11BA\x11BB\
             \\x11BC\x11BD\x11BE\x11BF\x11C0\x11C1\x11C2\x4E18\x4E32\x4E43\
             \\x4E45\x4E56\x4E5D\x4E5E\x4E6B\x4E7E\x4E82\x4E98\x4EA4\x4EAC\
             \\x4EC7\x4ECA\x4ECB\x4EF6\x4EF7\x4F01\x4F0B\x4F0E\x4F3D\x4F73\
             \\x4F76\x4F83\x4F86\x4F8A\x4F9B\x4FC2\x4FD3\x4FF1\x500B\x501E\
             \\x5026\x5028\x5047\x5048\x5065\x5080\x5091\x50BE\x50C5\x50D1\
             \\x50F9\x5106\x5109\x513A\x5149\x514B\x5162\x5167\x516C\x5171\
             \\x5176\x5177\x517C\x5180\x51A0\x51F1\x520A\x522E\x5238\x523B\
             \\x524B\x525B\x5287\x528D\x5292\x529F\x52A0\x52A4\x52AB\x52C1\
             \\x52CD\x52D8\x52E4\x52F8\x52FB\x52FE\x5321\x5323\x5340\x5357\
             \\x5366\x5374\x5375\x5377\x537F\x53A5\x53BB\x53CA\x53E3\x53E5\
             \\x53E9\x53EB\x53EF\x5404\x5409\x541B\x544A\x5471\x5475\x548E\
             \\x54AC\x54E5\x54ED\x5553\x5580\x5587\x559D\x55AB\x55AC\x55DC\
             \\x5609\x5614\x5668\x56CA\x56F0\x56FA\x5708\x570B\x572D\x573B\
             \\x5747\x574E\x5751\x5764\x5770\x5775\x57A2\x57FA\x57FC\x5800\
             \\x5805\x5808\x582A\x583A\x584A\x584F\x5883\x58BE\x58D9\x58DE\
             \\x5914\x5947\x5948\x594E\x5951\x5978\x5993\x5997\x59D1\x59DC\
             \\x59E6\x5A18\x5A1C\x5AC1\x5B0C\x5B54\x5B63\x5B64\x5B8F\x5B98\
             \\x5BA2\x5BAE\x5BB6\x5BC4\x5BC7\x5BE1\x5BEC\x5C3B\x5C40\x5C45\
             \\x5C46\x5C48\x5C90\x5CA1\x5CAC\x5D0E\x5D11\x5D17\x5D4C\x5D50\
             \\x5D87\x5DA0\x5DE5\x5DE7\x5DE8\x5DF1\x5DFE\x5E72\x5E79\x5E7E\
             \\x5E9A\x5EAB\x5EB7\x5ECA\x5ED0\x5ED3\x5EE3\x5EFA\x5F13\x5F3A\
             \\x5F4A\x5F91\x5FCC\x6025\x602A\x602F\x6050\x605D\x606A\x606D\
             \\x60B8\x6106\x611F\x6127\x6137\x613E\x614A\x6163\x6164\x6168\
             \\x6176\x6177\x61A9\x61AC\x61BE\x61C3\x61C7\x61E6\x61F6\x61FC\
             \\x6208\x6212\x621F\x6221\x6271\x6280\x6289\x62C9\x62CF\x62D0\
             \\x62D2\x62D8\x62EC\x62EE\x62F1\x62F3\x62F7\x62FF\x634F\x636E\
             \\x6372\x637A\x6398\x639B\x63A7\x63C0\x63C6\x63ED\x64CA\x64CE\
             \\x64D2\x64DA\x64E7\x652A\x6537\x6539\x653B\x6545\x654E\x6551\
             \\x6562\x656C\x6572\x659B\x65A4\x65D7\x65E3\x6606\x6611\x666F\
             \\x6677\x6687\x6696\x66A0\x66BB\x66E0\x66F2\x66F4\x66F7\x6717\
             \\x671E\x671F\x673A\x6746\x675E\x6770\x678F\x679C\x67AF\x67B6\
             \\x67B8\x67D1\x67E9\x67EC\x67EF\x6821\x6839\x683C\x6840\x6842\
             \\x6854\x687F\x688F\x6897\x68B0\x68B1\x68C4\x68CB\x68CD\x68D8\
             \\x68E8\x68FA\x6957\x6960\x6975\x69C1\x69CB\x69D0\x69E8\x69EA\
             \\x69FB\x69FF\x6A02\x6A44\x6A4B\x6A58\x6A5F\x6A84\x6A8E\x6AA2\
             \\x6AC3\x6B04\x6B0A\x6B3A\x6B3E\x6B4C\x6B50\x6B78\x6BBC\x6BC6\
             \\x6BEC\x6C23\x6C42\x6C5F\x6C68\x6C72\x6C7A\x6C7D\x6C82\x6CBD\
             \\x6D1B\x6D38\x6D6A\x6D87\x6DC3\x6DC7\x6E1B\x6E20\x6E34\x6E73\
             \\x6E9D\x6EAA\x6ED1\x6EFE\x6F11\x6F54\x6F70\x6F97\x6FC0\x6FEB\
             \\x704C\x7078\x7085\x709A\x70AC\x70D9\x70F1\x7156\x721B\x727D\
             \\x72AC\x72C2\x72D7\x72E1\x72FC\x7357\x7396\x7398\x73C2\x73CF\
             \\x73D6\x73D9\x73DE\x73EA\x7403\x7426\x7428\x742A\x742F\x7434\
             \\x747E\x7482\x749F\x74A3\x74A5\x74CA\x74D8\x74DC\x7504\x7518\
             \\x7532\x7537\x7547\x754C\x7578\x757A\x757F\x7586\x75A5\x75B3\
             \\x75C2\x75D9\x75FC\x764E\x7669\x7678\x7686\x768E\x7690\x76D6\
             \\x76E3\x770B\x7737\x777E\x77B0\x77BC\x77BF\x77DC\x77E9\x77EF\
             \\x7845\x786C\x7881\x78A3\x78CE\x78EC\x78EF\x78F5\x7941\x7947\
             \\x7948\x795B\x797A\x7981\x79BD\x79D1\x7A08\x7A3C\x7A3D\x7A3F\
             \\x7A40\x7A76\x7A79\x7A7A\x7A98\x7A9F\x7AAE\x7ABA\x7AC5\x7ADF\
             \\x7AED\x7AF6\x7AFF\x7B4B\x7B50\x7B60\x7B87\x7B95\x7B9D\x7BA1\
             \\x7C21\x7CB3\x7CE0\x7CFB\x7CFE\x7D00\x7D0D\x7D18\x7D1A\x7D3A\
             \\x7D45\x7D50\x7D5E\x7D66\x7D73\x7D79\x7D7F\x7D93\x7DB1\x7DBA\
             \\x7DCA\x7E6B\x7E6D\x7E7C\x7F3A\x7F50\x7F6B\x7F85\x7F88\x7F8C\
             \\x7F94\x7FA4\x7FB9\x7FF9\x8003\x8006\x8009\x8015\x802D\x803F\
             \\x808C\x809D\x80A1\x80A9\x80AF\x80B1\x80DB\x80F1\x811A\x811B\
             \\x8154\x8171\x8188\x818F\x81A0\x81D8\x81FC\x8205\x820A\x8221\
             \\x826E\x8271\x828E\x82A5\x82A9\x82B9\x82DB\x82DF\x82E6\x82FD\
             \\x8304\x8396\x83C5\x83CA\x83CC\x83D3\x83EB\x83F0\x843D\x845B\
             \\x8475\x84CB\x854E\x8568\x8591\x85C1\x85CD\x85FF\x862D\x863F\
             \\x8654\x86A3\x86DF\x874E\x87BA\x881F\x8831\x8857\x8862\x8872\
             \\x887E\x887F\x8888\x889E\x88B4\x88D9\x88F8\x8910\x8941\x895F\
             \\x8964\x898B\x898F\x89A1\x89B2\x89BA\x89C0\x89D2\x8A08\x8A18\
             \\x8A23\x8A36\x8A6D\x8A87\x8AA1\x8AA5\x8AB2\x8AEB\x8AFE\x8B19\
             \\x8B1B\x8B33\x8B39\x8B4F\x8B66\x8B74\x8C37\x8C3F\x8C48\x8CA2\
             \\x8CAB\x8CB4\x8CC8\x8CFC\x8D73\x8D77\x8DCF\x8DDD\x8DE8\x8E1E\
             \\x8E47\x8E76\x8EAC\x8EC0\x8ECA\x8ECC\x8ECD\x8EFB\x8F03\x8F15\
             \\x8F4E\x8F5F\x8F9C\x8FD1\x8FE6\x8FF2\x9002\x9011\x9015\x9035\
             \\x904E\x9063\x907D\x908F\x90A3\x90AF\x90B1\x90CA\x90CE\x90E1\
             \\x90ED\x916A\x91B5\x91D1\x9210\x921E\x9240\x9245\x9257\x9264\
             \\x92B6\x92F8\x92FC\x9321\x9324\x9326\x932E\x934B\x9375\x938C\
             \\x93A7\x93E1\x9451\x9452\x945B\x958B\x9593\x9598\x95A3\x95A8\
             \\x95D5\x95DC\x964D\x968E\x9694\x9699\x96C7\x96E3\x978F\x97A0\
             \\x97A8\x97AB\x9803\x9838\x9846\x9867\x98E2\x9903\x9928\x9949\
             \\x994B\x9951\x99D2\x99D5\x99F1\x9A0E\x9A0F\x9A2B\x9A45\x9A55\
             \\x9A5A\x9A65\x9AA8\x9AD8\x9B3C\x9B41\x9BAB\x9BE4\x9BE8\x9C47\
             \\x9CE9\x9D51\x9D60\x9DC4\x9DD7\x9E1E\x9E92\x9EB4\x9ED4\x9F13\
             \\x9F95\x9F9C"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\x5F\
             \\x7B\x7D\xA1\xA7\xB6\xB7\xBF\x2010\x2011\x2014\
             \\x2015\x2018\x2019\x201C\x201D\x2020\x2021\x2025\x2026\x2030\
             \\x2032\x2033\x203B\x203E\x3001\x3002\x3003\x3008\x3009\x300A\
             \\x300B\x300C\x300D\x300E\x300F\x3010\x3011\x3014\x3015\x301C\
             \\x30FB\xFF01\xFF02\xFF03\xFF05\xFF06\xFF07\xFF08\xFF09\xFF0A\
             \\xFF0C\xFF0D\xFF0E\xFF0F\xFF1A\xFF1B\xFF1F\xFF20\xFF3B\xFF3C\
             \\xFF3D\xFF3F\xFF5B\xFF5D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("kok", LanguageData
      { language = "kok"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x903\x905\x906\x907\x908\x909\x90A\x90B\
             \\x90C\x90D\x90F\x910\x911\x913\x914\x915\x915\x93C\x916\
             \\x916\x93C\x917\x917\x93C\x918\x919\x91A\x91B\x91C\x91C\x93C\x91D\
             \\x91E\x91F\x920\x921\x921\x93C\x922\x922\x93C\x923\x924\x925\
             \\x926\x927\x928\x92A\x92B\x92B\x93C\x92C\x92D\x92E\x92F\
             \\x92F\x93C\x930\x932\x933\x935\x936\x937\x938\x939\x93C\
             \\x93D\x93E\x93F\x940\x941\x942\x943\x944\x945\x947\
             \\x948\x949\x94B\x94C\x94D\x950\x966\x967\x968\x969\
             \\x96A\x96B\x96C\x96D\x96E\x96F"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x966\x31\x967\x32\x968\x33\x969\x34\x96A\
             \\x35\x96B\x36\x96C\x37\x96D\x38\x96E\x39\x96F\x2011\x2030"
          }
      }
    )
  , ("ks", LanguageData
      { language = "ks"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x620\x621\x622\x623\x624\x627\x628\x62A\x62B\x62C\
             \\x62D\x62E\x62F\x630\x631\x632\x633\x634\x635\x636\
             \\x637\x638\x639\x63A\x641\x642\x644\x645\x646\x648\
             \\x672\x679\x67E\x686\x688\x691\x698\x6A9\x6AF\x6BA\
             \\x6BE\x6C1\x6C4\x6C6\x6CC\x6CD\x6D2"
          , _exAuxiliary = Just $ Set.fromList
             "\x64E\x64F\x650\x654\x655\x656\x657\x65F\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x6F0\x31\x6F1\x32\x6F2\x33\x6F3\x34\x6F4\
             \\x35\x6F5\x36\x6F6\x37\x6F7\x38\x6F8\x39\x6F9\x200E\x2011\x2030"
          }
      }
    )
  , ("ksb", LanguageData
      { language = "ksb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x73\x74\x75\x76\
             \\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x72\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ksf", LanguageData
      { language = "ksf"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A\xE1\xE9\xED\xF3\xFA\x14B\
             \\x1DD\x1DD\x301\x254\x254\x301\x25B\x25B\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ksh", LanguageData
      { language = "ksh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xDF\xE4\xE5\xE6\
             \\xEB\xF6\xFC\x117\x153\x16F"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE7\xE8\xE9\xEA\xEC\xED\
             \\xEE\xEF\xF1\xF2\xF3\xF4\xF8\xF9\xFA\xFB\
             \\xFF\x101\x103\x113\x115\x11F\x12B\x12D\x131\x133\
             \\x142\x14D\x14F\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x27\x28\x29\x2A\x2C\
             \\x2E\x2F\x3A\x3B\x3C\x3D\x3E\x3F\x40\x5B\
             \\x5D\x5F\x7B\x7D\x7E\xA7\xB0\x2010\x2013\x2014\
             \\x2018\x201A\x201C\x201E\x2020\x2021\x2026\x2E17"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("ku", LanguageData
      { language = "ku"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE7\xEA\xEE\xFB\
             \\x15F"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE8\xE9\
             \\xEB\xEC\xED\xEF\xF1\xF2\xF3\xF4\xF8\xF9\
             \\xFA\xFF\x101\x103\x113\x115\x12B\x12D\x14D\x14F\
             \\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("kw", LanguageData
      { language = "kw"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ky", LanguageData
      { language = "ky"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x433\x434\x435\x436\x437\x438\x439\x43A\
             \\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\x445\
             \\x447\x448\x44A\x44B\x44D\x44E\x44F\x451\x4A3\x4AF\
             \\x4E9"
          , _exAuxiliary = Just $ Set.fromList
             "\x432\x444\x446\x449\x44C"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("lag", LanguageData
      { language = "lag"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE1\xE9\xED\xF3\
             \\xFA\x268\x289"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("lb", LanguageData
      { language = "lb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE4\xE9\xEB"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE0\xE1\xE2\xE3\xE5\xE6\xE7\xE8\xEA\
             \\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF6\xF8\
             \\xF9\xFA\xFB\xFC\xFF\x101\x103\x113\x115\x11F\
             \\x12B\x12D\x130\x131\x14D\x14F\x153\x15F\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("lg", LanguageData
      { language = "lg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6E\x79\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A\x14B"
          , _exAuxiliary = Just $ Set.fromList
             "\x68\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("lkt", LanguageData
      { language = "lkt"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x61\x14B\x62\x65\x67\x68\x69\x69\x14B\x6B\x6B\x68\
             \\x6B\x21F\x6B\x2BC\x6C\x6D\x6E\x6F\x70\x70\x68\x70\x21F\x70\x2BC\
             \\x73\x74\x74\x68\x74\x21F\x74\x2BC\x75\x75\x14B\x77\x79\x7A\
             \\xE1\xE9\xED\xF3\xFA\x10D\x10D\x68\x10D\x2BC\x14B\x161\
             \\x17E\x1E7\x21F\x2BC"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x64\x66\x6A\x71\x72\x73\x2BC\x76\x78\x161\x2BC\
             \\x21F\x2BC"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x28\x29\x2A\x2C\x2D\x2E\
             \\x2F\x3A\x3B\x3F\x40\x5B\x5D\x2010\x2011\x2013\
             \\x2014\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ln", LanguageData
      { language = "ln"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x67\x62\x68\x69\
             \\x6B\x6C\x6D\x6D\x62\x6D\x70\x6E\x6E\x64\x6E\x67\x6E\x6B\x6E\x73\
             \\x6E\x74\x6E\x79\x6E\x7A\x6F\x70\x72\x73\x74\x75\x76\
             \\x77\x79\x7A\xE1\xE2\xE9\xEA\xED\xEE\xF3\
             \\xF4\xFA\x11B\x1CE\x1D0\x1D2\x254\x254\x301\x254\x302\x254\x30C\
             \\x25B\x25B\x301\x25B\x302\x25B\x30C"
          , _exAuxiliary = Just $ Set.fromList
             "\x6A\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("lo", LanguageData
      { language = "lo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xE81\xE82\xE84\xE87\xE88\xE8A\xE8D\xE94\xE95\xE96\
             \\xE97\xE99\xE9A\xE9B\xE9C\xE9D\xE9E\xE9F\xEA1\xEA2\
             \\xEA3\xEA5\xEA7\xEAA\xEAB\xEAD\xEAE\xEAF\xEB0\xEB1\
             \\xEB2\xEB3\xEB4\xEB5\xEB6\xEB7\xEB8\xEB9\xEBB\xEBC\
             \\xEBD\xEC0\xEC1\xEC2\xEC3\xEC4\xEC6\xEC8\xEC9\xECA\
             \\xECB\xECC\xECD\xEDC\xEDD"
          , _exAuxiliary = Just $ Set.fromList
             "\xED0\xED1\xED2\xED3\xED4\xED5\xED6\xED7\xED8\xED9\
             \\x200B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("lrc", LanguageData
      { language = "lrc"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x622\x623\x624\x626\x627\x628\x62A\x62B\x62C\x62D\
             \\x62E\x62F\x630\x631\x632\x633\x634\x635\x636\x637\
             \\x638\x639\x63A\x63D\x641\x642\x644\x645\x646\x648\
             \\x659\x65B\x67E\x686\x698\x6A4\x6A9\x6AF\x6BE\x6C9\
             \\x6CA\x6CC\x6D5"
          , _exAuxiliary = Just $ Set.fromList
             "\x625\x629\x643\x647\x649\x64A\x64B\x64C\x64D\x64E\
             \\x64F\x650\x651\x652\x654\x200B\x200C\x200D\x200E\x200F"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2A\x2D\x2E\x2F\x3A\x5B\x5D\
             \\x5D\xAB\xBB\x60C\x61B\x61F\x66B\x66C\x2010\x2011\
             \\x2026\x2039\x203A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("lt", LanguageData
      { language = "lt"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x79\x7A\x105\x10D\x117\x119\x12F\x161\x16B\
             \\x173\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x68\x64\x7A\x64\x17E\x69\x307\x300\x7D\xEC\x69\x307\x301\x7D\xED\x69\x307\x303\x7D\x129\x6A\x303\x6A\x307\x303\x6C\x303\x6D\x303\x71\
             \\x72\x303\x77\x78\xE0\xE1\xE3\xE8\xE9\xF1\xF2\
             \\xF3\xF5\xF9\xFA\x105\x301\x105\x303\x117\x301\x117\x303\x119\x301\x119\x303\
             \\x12F\x301\x12F\x307\x301\x12F\x303\x12F\x307\x303\x169\x16B\x301\x16B\x303\x173\x301\x173\x303\x1EBD"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2010\x2011\x2013\x2014\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("lu", LanguageData
      { language = "lu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6E\x67\x6E\x79\x6F\x70\x70\x68\x71\x73\
             \\x73\x68\x69\x74\x75\x76\x77\x79\x7A\xE0\xE1\xE8\
             \\xE9\xEC\xED\xF2\xF3\xF9\xFA\x254\x254\x300\x254\x301\
             \\x25B\x25B\x300\x25B\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x67\x72\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("luo", LanguageData
      { language = "luo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("luy", LanguageData
      { language = "luy"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("lv", LanguageData
      { language = "lv"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x7A\x101\x10D\x113\x123\x12B\x137\x13C\x146\
             \\x161\x16B\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79\x14D\x157"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201A\x201C\x201D\x201E\x2020\
             \\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("mai", LanguageData
      { language = "mai"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x902\x903\x915\x915\x94D\x937\x916\x917\x918\x91A\x91B\x91C\
             \\x91C\x94D\x91E\x91D\x91E\x91F\x920\x921\x921\x902\x922\x923\x924\
             \\x924\x94D\x930\x925\x926\x927\x928\x92A\x92B\x92C\x92D\x92E\
             \\x92F\x930\x932\x935\x936\x936\x94D\x930\x937\x938\x939\x93C\
             \\x93E\x93F\x940\x941\x942\x947\x948\x94B\x94C"
          , _exAuxiliary = Just $ Set.fromList
             "\x905\x905\x902\x905\x903\x906\x907\x908\x909\x90A\x90B\x90C\
             \\x90F\x910\x913\x914\x961"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2B\x2C\
             \\x2D\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\
             \\x5F\x60\x7B\x7C\x7D\x7E\xA7\x2011\x2013\x2014\
             \\x2018\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("mas", LanguageData
      { language = "mas"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6E\x79\x6F\x70\x72\x72\x72\x73\x73\x68\
             \\x74\x75\x77\x77\x75\x79\x79\x69\xE0\xE1\xE2\xE8\
             \\xE9\xEA\xEC\xED\xEE\xF2\xF3\xF4\xF9\xFA\
             \\xFB\x101\x113\x12B\x14B\x14D\x16B\x254\x25B\x268\
             \\x289\x289\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x66\x71\x76\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("mer", LanguageData
      { language = "mer"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\x129\x169"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("mfe", LanguageData
      { language = "mfe"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("mg", LanguageData
      { language = "mg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x76\x79\
             \\x7A\xE0\xE2\xE8\xE9\xEA\xEB\xEC\xEE\xEF\
             \\xF1\xF4"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x75\x77\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("mgh", LanguageData
      { language = "mgh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("mgo", LanguageData
      { language = "mgo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x68\x64\x65\x66\x67\x67\x68\x69\x6A\
             \\x6B\x6D\x6E\x6F\x70\x72\x73\x74\x75\x77\
             \\x79\x7A\xE0\xE8\xEC\xF2\xF9\x14B\x254\x254\x300\
             \\x259\x259\x300\x2BC"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x68\x6C\x71\x76\x78"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x2C\x2E\x3A\x3B\x3F\x2018\x2019\
             \\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("mi", LanguageData
      { language = "mi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x65\x68\x69\x6B\x6D\x6E\x6E\x67\x6F\x70\
             \\x72\x74\x75\x77\x77\x68\x101\x113\x12B\x14D\x16B"
          , _exAuxiliary = Just $ Set.fromList
             "\x62\x63\x64\x66\x67\x6A\x6C\x71\x73\x76\
             \\x78\x79\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("mk", LanguageData
      { language = "mk"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x43A\
             \\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\x444\
             \\x445\x446\x447\x448\x453\x455\x458\x459\x45A\x45C\
             \\x45F"
          , _exAuxiliary = Just $ Set.fromList
             "\x450\x45D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ml", LanguageData
      { language = "ml"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xD02\xD03\xD05\xD06\xD07\xD08\xD09\xD0A\xD0B\xD0C\
             \\xD0E\xD0F\xD10\xD12\xD13\xD14\xD15\xD16\xD17\xD18\
             \\xD19\xD1A\xD1B\xD1C\xD1D\xD1E\xD1F\xD20\xD21\xD22\
             \\xD23\xD24\xD25\xD26\xD27\xD28\xD2A\xD2B\xD2C\xD2D\
             \\xD2E\xD2F\xD30\xD31\xD32\xD33\xD34\xD35\xD36\xD37\
             \\xD38\xD39\xD3E\xD3F\xD40\xD41\xD42\xD43\xD46\xD47\
             \\xD48\xD4A\xD4B\xD4C\xD4D\xD57\xD60\xD61\xD7A\xD7B\
             \\xD7C\xD7D\xD7E\xD7F\x200C\x200D"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xD66\x31\xD67\x32\xD68\x33\xD69\x34\xD6A\
             \\x35\xD6B\x36\xD6C\x37\xD6D\x38\xD6E\x39\xD6F\x2011\x2030"
          }
      }
    )
  , ("mn", LanguageData
      { language = "mn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x446\x447\x448\x449\x44A\x44B\x44C\x44D\
             \\x44E\x44F\x451\x4AF\x4E9"
          , _exAuxiliary = Just $ Set.fromList
             "\x497\x4BB\x4CA\x4D9"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("mni", LanguageData
      { language = "mni"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x981\x982\x983\x985\x986\x987\x988\x989\x98A\x98B\
             \\x98F\x990\x993\x994\x995\x996\x997\x998\x999\x99A\
             \\x99B\x99C\x99D\x99E\x99F\x9A0\x9A1\x9A1\x9BC\x9A2\x9A2\x9BC\
             \\x9A3\x9A4\x9A5\x9A6\x9A7\x9A8\x9AA\x9AB\x9AC\x9AD\
             \\x9AE\x9AF\x9AF\x9BC\x9B0\x9B2\x9B6\x9B7\x9B8\x9B9\x9BC\
             \\x9BE\x9BF\x9C0\x9C1\x9C2\x9C3\x9C7\x9C8\x9CB\x9CC\
             \\x9CD\x9F1"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x9E6\x31\x9E7\x32\x9E8\x33\x9E9\x34\x9EA\
             \\x35\x9EB\x36\x9EC\x37\x9ED\x38\x9EE\x39\x9EF\x2011\x2030"
          }
      }
    )
  , ("mr", LanguageData
      { language = "mr"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x903\x905\x906\x907\x908\x909\x90A\x90B\
             \\x90C\x90D\x90F\x910\x911\x913\x914\x915\x916\x917\
             \\x918\x919\x91A\x91B\x91C\x91D\x91E\x91F\x920\x921\
             \\x922\x923\x924\x925\x926\x927\x928\x92A\x92B\x92C\
             \\x92D\x92E\x92F\x930\x931\x932\x933\x935\x936\x937\
             \\x938\x939\x93C\x93D\x93E\x93F\x940\x941\x942\x943\
             \\x944\x945\x947\x948\x949\x94B\x94C\x94D\x950"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x2010\x2011\
             \\x2013\x2014\x2018\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x966\x31\x967\x32\x968\x33\x969\x34\x96A\
             \\x35\x96B\x36\x96C\x37\x96D\x38\x96E\x39\x96F\x2011\x2030"
          }
      }
    )
  , ("ms", LanguageData
      { language = "ms"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("mt", LanguageData
      { language = "mt"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x67\x127\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x7A\xE0\xE8\xEC\xF2\xF9\
             \\x10B\x121\x127\x17C"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x79"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x3A\x3B\
             \\x3F\x5B\x5D\x7B\x7D\x2011\x2018\x2019\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("mua", LanguageData
      { language = "mua"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A\xE3\xEB\xF5\x129\x14B\x1DD\
             \\x253\x257\x1E7D"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("my", LanguageData
      { language = "my"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1000\x1001\x1002\x1003\x1004\x1005\x1006\x1007\x1008\x1009\
             \\x100A\x100B\x100C\x100D\x100E\x100F\x1010\x1011\x1012\x1013\
             \\x1014\x1015\x1016\x1017\x1018\x1019\x101A\x101B\x101C\x101D\
             \\x101E\x101F\x1020\x1021\x1023\x1024\x1025\x1026\x1027\x1029\
             \\x102A\x102B\x102C\x102D\x102E\x102F\x1030\x1031\x1032\x1036\
             \\x1037\x1038\x1039\x103A\x103B\x103C\x103D\x103E\x103F\x104F"
          , _exAuxiliary = Just $ Set.fromList
             "\x1022\x1028\x1033\x1034\x1040\x1090\x1041\x1091\x1042\x1092\x1043\x1093\x1044\x1094\x1045\x1095\
             \\x1046\x1096\x1047\x1097\x1048\x1098\x1049\x1099\x1050\x1051\x1052\x1053\x1054\x1055\
             \\x1056\x1057\x1058\x1059\x105A\x1062\x1064\x1065\x1075\x107D\
             \\x107E\x1086\x1088\x108A\x108F"
          , _exPunctuation = Just $ Set.fromList
             "\x104A\x104B\x2018\x2019\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x1040\x31\x1041\x32\x1042\x33\x1043\x34\x1044\
             \\x35\x1045\x36\x1046\x37\x1047\x38\x1048\x39\x1049\x2011\x2030"
          }
      }
    )
  , ("mzn", LanguageData
      { language = "mzn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x623\x624\x626\x627\x628\x629\x62A\x62B\
             \\x62C\x62D\x62E\x62F\x630\x631\x632\x633\x634\x635\
             \\x636\x637\x638\x639\x63A\x641\x642\x644\x645\x646\
             \\x647\x648\x64B\x64C\x64D\x651\x654\x67E\x686\x698\
             \\x6A9\x6AF\x6CC"
          , _exAuxiliary = Just $ Set.fromList
             "\x625\x643\x649\x64A\x64E\x64F\x650\x652\x656\x670\
             \\x200C\x200D\x200E\x200F"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2A\x2D\x2E\x2F\x3A\x5B\x5D\
             \\x5D\xAB\xBB\x60C\x61B\x61F\x66B\x66C\x2010\x2011\
             \\x2026\x2039\x203A"
          , _exNumbers = Nothing
          }
      }
    )
  , ("naq", LanguageData
      { language = "naq"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6B\
             \\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\x77\
             \\x78\x79\x7A\xE2\xEE\xF4\xFB\x1C0\x1C1\x1C2\
             \\x1C3"
          , _exAuxiliary = Just $ Set.fromList
             "\x6A\x6C\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("nb", LanguageData
      { language = "nb"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE5\xE6\xE9\
             \\xF2\xF3\xF4\xF8"
          , _exAuxiliary = Just $ Set.fromList
             "\xE1\xE3\xE4\xE7\xE8\xEA\xED\xF1\xF6\xFA\
             \\xFC\x10D\x111\x144\x14B\x161\x167\x17E\x1CE"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x3F\x40\x5B\x5D\x5D\x7B\x7D\xA7\
             \\xAB\xBB\x2011\x2013"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("nd", LanguageData
      { language = "nd"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x73\x74\x75\
             \\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x72"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("nds", LanguageData
      { language = "nds"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE4\xE5\xF6\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE6\xE7\xE8\xE9\xEA\xEB\xEC\
             \\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF8\xF9\xFA\
             \\xFB\xFF\x101\x103\x113\x115\x119\x12B\x12D\x14D\
             \\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ne", LanguageData
      { language = "ne"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x903\x905\x906\x907\x908\x909\x90A\x90B\
             \\x90C\x90D\x90F\x910\x911\x913\x914\x915\x916\x917\
             \\x918\x919\x91A\x91B\x91C\x91D\x91E\x91F\x920\x921\
             \\x922\x923\x924\x925\x926\x927\x928\x92A\x92B\x92C\
             \\x92D\x92E\x92F\x930\x932\x933\x935\x936\x937\x938\
             \\x939\x93C\x93D\x93E\x93F\x940\x941\x942\x943\x944\
             \\x945\x947\x948\x949\x94B\x94C\x94D\x950"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x964\x2011\x2014\x2018\x2019\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x966\x31\x967\x32\x968\x33\x969\x34\x96A\
             \\x35\x96B\x36\x96C\x37\x96D\x38\x96E\x39\x96F\x2011\x2030"
          }
      }
    )
  , ("nl", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x69\x6A\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\
             \\x74\x75\x76\x77\x78\x79\x7A\xE1\xE4\xE9\
             \\xEB\xED\xED\x6A\x301\xEF\xF3\xF6\xFA\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE2\xE3\xE5\xE6\xE7\xE8\xEA\xEE\xF1\
             \\xF4\xF8\xF9\xFB\xFF\x153"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("nmg", LanguageData
      { language = "nmg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\xE1\xE2\xE4\xE9\xEA\xED\xEE\
             \\xEF\xF3\xF4\xF6\xFA\xFB\x101\x113\x11B\x12B\
             \\x144\x14B\x14D\x155\x16B\x1CE\x1D0\x1D2\x1D4\x1DD\
             \\x1DD\x301\x1DD\x302\x1DD\x304\x1DD\x30C\x253\x254\x254\x301\x254\x302\x254\x304\x254\x30C\
             \\x25B\x25B\x301\x25B\x302\x25B\x304\x25B\x30C"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("nn", LanguageData
      { language = "nn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE5\xE6\xE9\
             \\xF2\xF3\xF4\xF8"
          , _exAuxiliary = Just $ Set.fromList
             "\xE1\xE4\xE7\xE8\xEA\xF1\xF6\xFC\x10D\x111\
             \\x144\x14B\x161\x167\x17E\x1CE"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("nnh", LanguageData
      { language = "nnh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x70\x66\x73\x73\x68\x74\
             \\x74\x73\x75\x76\x77\x79\x7A\xE0\xE1\xE2\xE8\
             \\xE9\xEA\xEC\xED\xF2\xF3\xF4\xF9\xFA\xFB\
             \\xFF\x11B\x144\x14B\x1CE\x1D2\x1D4\x254\x254\x300\x254\x301\
             \\x254\x302\x254\x30C\x25B\x25B\x300\x25B\x301\x25B\x302\x25B\x30C\x289\x289\x300\x289\x301\
             \\x289\x302\x289\x30C\x2BC\x1E3F\x1E85"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x72\x78"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x27\x2C\x2E\x3A\x3B\x3F\xAB\xBB\x2018\
             \\x2019"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("nus", LanguageData
      { language = "nus"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x61\x331\x62\x63\x64\x65\x65\x331\x66\x67\x68\
             \\x69\x69\x331\x6A\x6B\x6C\x6D\x6E\x6F\x6F\x331\x70\
             \\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7A\
             \\xE4\xEB\xEF\xF6\x14B\x254\x254\x308\x254\x331\x25B\x25B\x308\
             \\x25B\x331\x25B\x331\x308\x263"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("nyn", LanguageData
      { language = "nyn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("om", LanguageData
      { language = "om"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("or", LanguageData
      { language = "or"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xB01\xB02\xB03\xB05\xB06\xB07\xB08\xB09\xB0A\xB0B\
             \\xB0F\xB10\xB13\xB14\xB15\xB16\xB17\xB18\xB19\xB1A\
             \\xB1B\xB1C\xB1D\xB1E\xB1F\xB20\xB21\xB21\xB3C\xB22\xB22\xB3C\
             \\xB23\xB24\xB25\xB26\xB27\xB28\xB2A\xB2B\xB2C\xB2D\
             \\xB2E\xB2F\xB30\xB32\xB33\xB35\xB36\xB37\xB38\xB39\
             \\xB3C\xB3E\xB3F\xB40\xB41\xB42\xB43\xB47\xB48\xB4B\
             \\xB4C\xB4D\xB5F\xB71"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xB66\x31\xB67\x32\xB68\x33\xB69\x34\xB6A\
             \\x35\xB6B\x36\xB6C\x37\xB6D\x38\xB6E\x39\xB6F\x2011\x2030"
          }
      }
    )
  , ("os", LanguageData
      { language = "os"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x433\x44A\x434\x434\x436\x434\x437\x435\x436\
             \\x437\x438\x439\x43A\x43A\x44A\x43B\x43C\x43D\x43E\x43F\
             \\x43F\x44A\x440\x441\x442\x442\x44A\x443\x444\x445\x445\x44A\x446\
             \\x446\x44A\x447\x447\x44A\x448\x449\x44A\x44B\x44C\x44D\x44E\
             \\x44F\x451\x4D5"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("pa", LanguageData
      { language = "pa"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xA05\xA06\xA07\xA08\xA09\xA0A\xA0F\xA10\xA13\xA14\
             \\xA15\xA16\xA16\xA3C\xA17\xA17\xA3C\xA18\xA19\xA1A\xA1B\xA1C\
             \\xA1C\xA3C\xA1D\xA1E\xA1F\xA20\xA21\xA22\xA23\xA24\xA25\
             \\xA26\xA27\xA28\xA2A\xA2B\xA2B\xA3C\xA2C\xA2D\xA2E\xA2F\
             \\xA30\xA32\xA35\xA38\xA38\xA3C\xA39\xA3C\xA3E\xA3F\xA40\
             \\xA41\xA42\xA47\xA48\xA4B\xA4C\xA4D\xA5C\xA66\xA67\
             \\xA68\xA69\xA6A\xA6B\xA6C\xA6D\xA6E\xA6F\xA70\xA71\
             \\xA72\xA73\xA74"
          , _exAuxiliary = Just $ Set.fromList
             "\xA01\xA02\xA03\xA32\xA3C\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x26\x27\x28\x29\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x3F\x5B\x5D\x2010\x2011\x2013\x2014\x2018\
             \\x2019\x201C\x201D\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xA66\x31\xA67\x32\xA68\x33\xA69\x34\xA6A\
             \\x35\xA6B\x36\xA6C\x37\xA6D\x38\xA6E\x39\xA6F\x2011\x2030"
          }
      }
    )
  , ("pcm", LanguageData
      { language = "pcm"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x68\x64\x65\x66\x67\x67\x62\x68\x69\
             \\x6A\x6B\x6B\x70\x6C\x6D\x6E\x6F\x70\x72\x73\
             \\x73\x68\x74\x75\x76\x77\x79\x7A\x7A\x68\xE1\xE9\
             \\xED\xF3\xFA\x1EB9\x1EB9\x301\x1ECD\x1ECD\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x78\xE0\xE8\xEC\xF2\xF9\x1EB9\x300\x1ECD\x300"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("pl", LanguageData
      { language = "pl"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x77\x79\x7A\xF3\x105\x107\x119\x142\x144\x15B\
             \\x17A\x17C"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x76\x78\xDF\xE0\xE2\xE4\xE5\xE6\xE7\
             \\xE8\xE9\xEA\xEB\xEE\xEF\xF4\xF6\xF9\xFB\
             \\xFC\xFF\x153"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x27\x28\x29\x2A\x2C\
             \\x2D\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\
             \\x7D\x7E\xA7\xAB\xB0\xBB\x2010\x2011\x2013\x2014\
             \\x201D\x201E\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ps", LanguageData
      { language = "ps"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x623\x624\x626\x627\x628\x629\x62A\x62B\
             \\x62C\x62D\x62E\x62F\x630\x631\x632\x633\x634\x635\
             \\x636\x637\x638\x639\x63A\x641\x642\x644\x645\x646\
             \\x647\x648\x64A\x64B\x64C\x64D\x64E\x64F\x650\x651\
             \\x652\x654\x670\x67C\x67E\x681\x685\x686\x689\x693\
             \\x696\x698\x69A\x6A9\x6AB\x6AF\x6BC\x6CC\x6CD\x6D0"
          , _exAuxiliary = Just $ Set.fromList
             "\x6D2\x200C\x200D\x200E\x200F"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x27\x28\x29\x2F\x3A\x3B\x5B\x5D\x7B\
             \\x7D\x60C\x6D4\x2018"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x6F0\x31\x6F1\x32\x6F2\x33\x6F3\x34\x6F4\
             \\x35\x6F5\x36\x6F6\x37\x6F7\x38\x6F8\x39\x6F9\x609\x66A\x66B\x66C\x200E\
             \\x2011\x2030\x2212"
          }
      }
    )
  , ("pt", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE1\xE2\xE3\
             \\xE7\xE9\xEA\xED\xF2\xF3\xF4\xF5\xFA"
          , _exAuxiliary = Just $ Set.fromList
             "\xAA\xBA\xE4\xE5\xE6\xE8\xEB\xEC\xEE\xEF\
             \\xF1\xF6\xF8\xF9\xFB\xFC\xFF\x101\x103\x113\
             \\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("qu", LanguageData
      { language = "qu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x63\x68\x63\x68\x2BC\x68\x69\x6B\x6B\x2BC\x6C\x6C\x6C\x6D\
             \\x6E\x70\x70\x2BC\x71\x71\x2BC\x73\x74\x74\x2BC\x75\x77\
             \\x79\xF1"
          , _exAuxiliary = Just $ Set.fromList
             "\x62\x63\x64\x65\x66\x67\x6A\x6F\x72\x76\
             \\x78\x7A\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\
             \\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF2\xF3\
             \\xF4\xF6\xF8\xF9\xFA\xFB\xFC\xFF\x101\x103\
             \\x113\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("rm", LanguageData
      { language = "rm"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE8\xE9\xEC\
             \\xF2\xF9"
          , _exAuxiliary = Just $ Set.fromList
             "\xE1\xE2\xE4\xE5\xE6\xE7\xEA\xEB\xED\xEE\
             \\xEF\xF1\xF3\xF4\xF6\xF8\xFA\xFB\xFC\xFF\
             \\x101\x103\x113\x115\x12B\x12D\x14D\x14F\x153\x16B\
             \\x16D"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2E\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\x2019\x2030\x2212"
          }
      }
    )
  , ("rn", LanguageData
      { language = "rn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ro", LanguageData
      { language = "ro"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x78\x79\x7A\xE2\xEE\x103\x219\x21B"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\xE0\xE1\xE4\xE5\xE7\xE8\xE9\xEA\xEB\
             \\xF1\xF6\xFC\x15F\x163"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x3F\x40\x5B\x5D\xAB\xBB\x2010\x2011\
             \\x2013\x2014\x2018\x201C\x201D\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("rof", LanguageData
      { language = "rof"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("root", LanguageData
      { language = "root"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2011"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ru", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x446\x447\x448\x449\x44A\x44B\x44C\x44D\
             \\x44E\x44F\x451"
          , _exAuxiliary = Just $ Set.fromList
             "\x430\x301\x435\x301\x438\x301\x43E\x301\x443\x301\x44B\x301\x44D\x301\x44E\x301\x44F\x301"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7B\x7D\
             \\xA7\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x201A\x201C\
             \\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("rw", LanguageData
      { language = "rw"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("rwk", LanguageData
      { language = "rwk"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("sa", LanguageData
      { language = "sa"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x901\x902\x903\x905\x906\x907\x908\x909\x90A\x90B\
             \\x90C\x90F\x910\x913\x914\x915\x916\x917\x918\x919\
             \\x91A\x91B\x91C\x91D\x91E\x91F\x920\x921\x922\x923\
             \\x924\x925\x926\x927\x928\x92A\x92B\x92C\x92D\x92E\
             \\x92F\x930\x932\x933\x935\x936\x937\x938\x939\x93C\
             \\x93D\x93E\x93F\x940\x941\x942\x943\x944\x947\x948\
             \\x94B\x94C\x94D\x950\x951\x952\x960\x961\x962\x963"
          , _exAuxiliary = Just $ Set.fromList
             "\x90D\x911\x945\x949\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2B\x2C\
             \\x2D\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\
             \\x5F\x60\x7B\x7C\x7D\x7E\xA7\x2011\x2013\x2014\
             \\x2018\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x966\x31\x967\x32\x968\x33\x969\x34\x96A\
             \\x35\x96B\x36\x96C\x37\x96D\x38\x96E\x39\x96F\x2011\x2030"
          }
      }
    )
  , ("sah", LanguageData
      { language = "sah"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x433\x434\x434\x44C\x438\x439\x43A\x43B\x43C\
             \\x43D\x43D\x44C\x43E\x43F\x440\x441\x442\x443\x445\x447\
             \\x44B\x44D\x495\x4A5\x4AF\x4BB\x4E9"
          , _exAuxiliary = Just $ Set.fromList
             "\x432\x435\x436\x437\x444\x446\x448\x449\x44A\x44C\
             \\x44E\x44F\x451"
          , _exPunctuation = Just $ Set.fromList
             "\x3A"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("saq", LanguageData
      { language = "saq"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x76\
             \\x77\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x66\x71\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("sat", LanguageData
      { language = "sat"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1C5A\x1C5B\x1C5C\x1C5D\x1C5E\x1C5F\x1C60\x1C61\x1C62\x1C63\
             \\x1C64\x1C65\x1C66\x1C67\x1C68\x1C69\x1C6A\x1C6B\x1C6C\x1C6D\
             \\x1C6E\x1C6F\x1C70\x1C71\x1C72\x1C73\x1C74\x1C75\x1C76\x1C77\
             \\x1C78\x1C79\x1C7A\x1C7B\x1C7C\x1C7D"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x1C50\x31\x1C51\x32\x1C52\x33\x1C53\x34\x1C54\
             \\x35\x1C55\x36\x1C56\x37\x1C57\x38\x1C58\x39\x1C59\x2011"
          }
      }
    )
  , ("sbp", LanguageData
      { language = "sbp"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x73\x74\x75\x76\
             \\x77\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x72\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("sd", LanguageData
      { language = "sd"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x627\x628\x62A\x62B\x62C\x62C\x6BE\x62D\x62E\
             \\x62F\x630\x631\x632\x633\x634\x635\x636\x637\x638\
             \\x639\x63A\x641\x642\x644\x645\x646\x647\x648\x64A\
             \\x67A\x67B\x67D\x67E\x67F\x680\x683\x684\x686\x687\
             \\x68A\x68C\x68D\x68F\x699\x6A6\x6A9\x6AA\x6AF\x6AF\x6BE\
             \\x6B1\x6B3\x6BB\x6BE"
          , _exAuxiliary = Just $ Set.fromList
             "\x626\x64E\x64F\x650"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2F\x3A\x5B\x5D\x7B\x7D\x6D4\
             \\x2018\x204F\x2E41"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("se", LanguageData
      { language = "se"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x7A\xE1\x10D\x111\x14B\x161\x167\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79\xE0\xE3\xE4\xE5\xE6\xE7\
             \\xE8\xE9\xED\xF1\xF2\xF3\xF6\xF8\xFA\xFC\
             \\x144"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("seh", LanguageData
      { language = "seh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE1\xE2\xE3\
             \\xE7\xE9\xEA\xED\xF2\xF3\xF4\xF5\xFA"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ses", LanguageData
      { language = "ses"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x77\x78\x79\x7A\xE3\xF5\x14B\x161\x17E\
             \\x272\x1EBD"
          , _exAuxiliary = Just $ Set.fromList
             "\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("sg", LanguageData
      { language = "sg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x76\
             \\x77\x79\x7A\xE2\xE4\xEA\xEB\xEE\xEF\xF4\
             \\xF6\xF9\xFB\xFC"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("shi", LanguageData
      { language = "shi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x2D30\x2D31\x2D33\x2D33\x2D6F\x2D37\x2D39\x2D3B\x2D3C\x2D3D\x2D3D\x2D6F\
             \\x2D40\x2D43\x2D44\x2D45\x2D47\x2D49\x2D4A\x2D4D\x2D4E\x2D4F\
             \\x2D53\x2D54\x2D55\x2D56\x2D59\x2D5A\x2D5B\x2D5C\x2D5F\x2D61\
             \\x2D62\x2D63\x2D65"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("si", LanguageData
      { language = "si"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xD82\xD83\xD85\xD86\xD87\xD88\xD89\xD8A\xD8B\xD8C\
             \\xD8D\xD91\xD92\xD93\xD94\xD95\xD96\xD9A\xD9B\xD9C\
             \\xD9D\xD9E\xD9F\xDA0\xDA1\xDA2\xDA3\xDA4\xDA5\xDA7\
             \\xDA8\xDA9\xDAA\xDAB\xDAC\xDAD\xDAE\xDAF\xDB0\xDB1\
             \\xDB3\xDB4\xDB5\xDB6\xDB7\xDB8\xDB9\xDBA\xDBB\xDBD\
             \\xDC0\xDC1\xDC2\xDC3\xDC4\xDC5\xDC6\xDCA\xDCF\xDD0\
             \\xDD1\xDD2\xDD3\xDD4\xDD6\xDD8\xDD9\xDDA\xDDB\xDDC\
             \\xDDD\xDDE\xDDF\xDF2"
          , _exAuxiliary = Just $ Set.fromList
             "\xD8E\xD8F\xD90\xDA6\xDF3\x200B\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("sk", LanguageData
      { language = "sk"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x63\x68\x64\x64\x7A\x64\x17E\x65\x66\x67\
             \\x68\x69\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\
             \\x72\x73\x74\x75\x76\x77\x78\x79\x7A\xE1\
             \\xE4\xE9\xED\xF3\xF4\xFA\xFD\x10D\x10F\x13A\
             \\x13E\x148\x155\x161\x165\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE2\xE5\xE6\xE7\xE8\xEA\xEB\xEC\xEE\
             \\xEF\xF1\xF2\xF6\xF8\xF9\xFB\xFC\xFF\x101\
             \\x103\x113\x115\x12B\x12D\x14D\x14F\x151\x153\x159\
             \\x16B\x16D\x171"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x26\x28\x29\x2A\x2C\x2D\x2E\x2F\x3A\
             \\x3B\x3F\x40\x5B\x5D\xA7\x2010\x2011\x2013\x2018\
             \\x201A\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("sl", LanguageData
      { language = "sl"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x7A\x10D\x161\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79\xE0\xE1\xE2\xE4\xE5\xE6\
             \\xE7\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF1\
             \\xF2\xF3\xF4\xF6\xF8\xF9\xFA\xFB\xFC\xFF\
             \\x101\x103\x107\x111\x113\x115\x12B\x12D\x14D\x14F\
             \\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2D\x2E\x3A\
             \\x3B\x3F\x40\x5B\x5D\x7B\x7D\xAB\xBB\x2011\
             \\x2013\x201E\x201F\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2030\x2212"
          }
      }
    )
  , ("smn", LanguageData
      { language = "smn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x79\x7A\xE1\xE2\xE4\x10D\x111\x14B\x161\
             \\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\xE0\xE3\xE5\xE6\xE7\xE8\xE9\
             \\xED\xF1\xF2\xF3\xF6\xF8\xFA\xFC\x144"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("sn", LanguageData
      { language = "sn"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("so", LanguageData
      { language = "so"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x62\x63\x64\x66\x67\x68\x6A\x6B\x6C\x6D\
             \\x6E\x71\x72\x73\x74\x77\x78\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x61\x65\x69\x6F\x70\x75\x76\x7A"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("sq", LanguageData
      { language = "sq"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x64\x68\x65\x66\x67\x67\x6A\x68\
             \\x69\x6A\x6B\x6C\x6C\x6C\x6D\x6E\x6E\x6A\x6F\x70\
             \\x71\x72\x72\x72\x73\x73\x68\x74\x74\x68\x75\x76\x78\
             \\x78\x68\x79\x7A\x7A\x68\xE7\xEB"
          , _exAuxiliary = Just $ Set.fromList
             "\x77"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\x7E\xA7\
             \\xAB\xBB\x2010\x2011\x2013\x2014\x2018\x2019\x201C\x201D\
             \\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("sr", LanguageData
      { language = "sr"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x43A\
             \\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\x444\
             \\x445\x446\x447\x448\x452\x458\x459\x45A\x45B\x45F"
          , _exAuxiliary = Just $ Set.fromList
             "\x430\x302\x435\x302\x438\x302\x439\x43E\x302\x443\x302\x449\x44A\x44B\x44C\
             \\x44D\x44E\x44F\x451"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x23\x28\x29\x2A\x2C\x2D\x2E\x3A\x3B\
             \\x3F\x5B\x5D\x7B\x7D\x2010\x2011\x2013\x2018\x201A\
             \\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("su", LanguageData
      { language = "su"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE9"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xEA\
             \\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF6\
             \\xF8\xF9\xFA\xFB\xFC\xFF\x101\x103\x113\x115\
             \\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("sv", LanguageData
      { language = "sv"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE4\xE5\xE9\
             \\xF6"
          , _exAuxiliary = Just $ Set.fromList
             "\xE1\xE2\xE3\xE6\xE7\xE8\xEB\xED\xEE\xEF\
             \\xF1\xF3\xF8\xFA\xFC\xFF\x101\x12B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x30\x31\x32\x33\x34\x35\x36\
             \\x37\x38\x39\xA0\x2030\x2212"
          }
      }
    )
  , ("sw", LanguageData
      { language = "sw"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x68\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x78"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x3A\x3B\
             \\x3F\x5B\x5D\x7B\x7D\x2011"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ta", LanguageData
      { language = "ta"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xB83\xB85\xB86\xB87\xB88\xB89\xB8A\xB8E\xB8F\xB90\
             \\xB92\xB93\xB94\xB95\xB99\xB9A\xB9C\xB9E\xB9F\xBA3\
             \\xBA4\xBA8\xBA9\xBAA\xBAE\xBAF\xBB0\xBB1\xBB2\xBB3\
             \\xBB4\xBB5\xBB7\xBB8\xBB9\xBBE\xBBF\xBC0\xBC1\xBC2\
             \\xBC6\xBC7\xBC8\xBCA\xBCB\xBCC\xBCD"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xBE6\x31\xBE7\x32\xBE8\x33\xBE9\x34\xBEA\
             \\x35\xBEB\x36\xBEC\x37\xBED\x38\xBEE\x39\xBEF\x2011\x2030"
          }
      }
    )
  , ("te", LanguageData
      { language = "te"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xC01\xC02\xC03\xC05\xC06\xC07\xC08\xC09\xC0A\xC0B\
             \\xC0C\xC0E\xC0F\xC10\xC12\xC13\xC14\xC15\xC16\xC17\
             \\xC18\xC19\xC1A\xC1B\xC1C\xC1D\xC1E\xC1F\xC20\xC21\
             \\xC22\xC23\xC24\xC25\xC26\xC27\xC28\xC2A\xC2B\xC2C\
             \\xC2D\xC2E\xC2F\xC30\xC31\xC32\xC33\xC35\xC36\xC37\
             \\xC38\xC39\xC3E\xC3F\xC40\xC41\xC42\xC43\xC44\xC46\
             \\xC47\xC48\xC4A\xC4B\xC4C\xC4D\xC55\xC56\xC60\xC61"
          , _exAuxiliary = Just $ Set.fromList
             "\xC66\xC67\xC68\xC69\xC6A\xC6B\xC6C\xC6D\xC6E\xC6F\
             \\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x3A\x3B\
             \\x3F\x5B\x5D\x7B\x7D\x2011\x2018\x2019\x201C\x201D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\xC66\x31\xC67\x32\xC68\x33\xC69\x34\xC6A\
             \\x35\xC6B\x36\xC6C\x37\xC6D\x38\xC6E\x39\xC6F\x2011\x2030"
          }
      }
    )
  , ("teo", LanguageData
      { language = "teo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x76\
             \\x77\x78\x79"
          , _exAuxiliary = Just $ Set.fromList
             "\x66\x71\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("tg", LanguageData
      { language = "tg"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x447\x448\x44A\x44D\x44E\x44F\x451\x493\
             \\x49B\x4B3\x4B7\x4E3\x4EF"
          , _exAuxiliary = Just $ Set.fromList
             "\x446\x449\x44B\x44C"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("th", LanguageData
      { language = "th"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xE01\xE02\xE03\xE04\xE05\xE06\xE07\xE08\xE09\xE0A\
             \\xE0B\xE0C\xE0D\xE0E\xE0F\xE10\xE11\xE12\xE13\xE14\
             \\xE15\xE16\xE17\xE18\xE19\xE1A\xE1B\xE1C\xE1D\xE1E\
             \\xE1F\xE20\xE21\xE22\xE23\xE24\xE25\xE26\xE27\xE28\
             \\xE29\xE2A\xE2B\xE2C\xE2D\xE2E\xE2F\xE30\xE31\xE32\
             \\xE33\xE34\xE35\xE36\xE37\xE38\xE39\xE3A\xE40\xE41\
             \\xE42\xE43\xE44\xE45\xE46\xE47\xE48\xE49\xE4A\xE4B\
             \\xE4C\xE4D\xE4E"
          , _exAuxiliary = Just $ Set.fromList
             "\x200B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x27\x28\x29\x2A\x2C\x2D\x2E\
             \\x2F\x3A\x40\x5B\x5D\x2010\x2011\x2013\x2014\x2018\
             \\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("ti", LanguageData
      { language = "ti"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1200\x1201\x1202\x1203\x1204\x1205\x1206\x1208\x1209\x120A\
             \\x120B\x120C\x120D\x120E\x120F\x1210\x1211\x1212\x1213\x1214\
             \\x1215\x1216\x1217\x1218\x1219\x121A\x121B\x121C\x121D\x121E\
             \\x121F\x1220\x1221\x1222\x1223\x1224\x1225\x1226\x1227\x1228\
             \\x1229\x122A\x122B\x122C\x122D\x122E\x122F\x1230\x1231\x1232\
             \\x1233\x1234\x1235\x1236\x1237\x1238\x1239\x123A\x123B\x123C\
             \\x123D\x123E\x123F\x1240\x1241\x1242\x1243\x1244\x1245\x1246\
             \\x1248\x124A\x124B\x124C\x124D\x1250\x1251\x1252\x1253\x1254\
             \\x1255\x1256\x1258\x125A\x125B\x125C\x125D\x1260\x1261\x1262\
             \\x1263\x1264\x1265\x1266\x1267\x1268\x1269\x126A\x126B\x126C\
             \\x126D\x126E\x126F\x1270\x1271\x1272\x1273\x1274\x1275\x1276\
             \\x1277\x1278\x1279\x127A\x127B\x127C\x127D\x127E\x127F\x1280\
             \\x1281\x1282\x1283\x1284\x1285\x1286\x1288\x128A\x128B\x128C\
             \\x128D\x1290\x1291\x1292\x1293\x1294\x1295\x1296\x1297\x1298\
             \\x1299\x129A\x129B\x129C\x129D\x129E\x129F\x12A0\x12A1\x12A2\
             \\x12A3\x12A4\x12A5\x12A6\x12A7\x12A8\x12A9\x12AA\x12AB\x12AC\
             \\x12AD\x12AE\x12B0\x12B2\x12B3\x12B4\x12B5\x12B8\x12B9\x12BA\
             \\x12BB\x12BC\x12BD\x12BE\x12C0\x12C2\x12C3\x12C4\x12C5\x12C8\
             \\x12C9\x12CA\x12CB\x12CC\x12CD\x12CE\x12D0\x12D1\x12D2\x12D3\
             \\x12D4\x12D5\x12D6\x12D8\x12D9\x12DA\x12DB\x12DC\x12DD\x12DE\
             \\x12DF\x12E0\x12E1\x12E2\x12E3\x12E4\x12E5\x12E6\x12E7\x12E8\
             \\x12E9\x12EA\x12EB\x12EC\x12ED\x12EE\x12F0\x12F1\x12F2\x12F3\
             \\x12F4\x12F5\x12F6\x12F7\x1300\x1301\x1302\x1303\x1304\x1305\
             \\x1306\x1307\x1308\x1309\x130A\x130B\x130C\x130D\x130E\x1310\
             \\x1312\x1313\x1314\x1315\x1320\x1321\x1322\x1323\x1324\x1325\
             \\x1326\x1327\x1328\x1329\x132A\x132B\x132C\x132D\x132E\x132F\
             \\x1330\x1331\x1332\x1333\x1334\x1335\x1336\x1337\x1338\x1339\
             \\x133A\x133B\x133C\x133D\x133E\x133F\x1340\x1341\x1342\x1343\
             \\x1344\x1345\x1346\x1347\x1348\x1349\x134A\x134B\x134C\x134D\
             \\x134E\x134F\x1350\x1351\x1352\x1353\x1354\x1355\x1356\x1357\
             \\x135F"
          , _exAuxiliary = Just $ Set.fromList
             "\x1207\x1247\x1287\x12AF\x12CF\x12EF\x12F8\x12F9\x12FA\x12FB\
             \\x12FC\x12FD\x12FE\x12FF\x130F\x1318\x1319\x131A\x131B\x131C\
             \\x131D\x131E\x131F\x1358\x1359\x135A\x1380\x1381\x1382\x1383\
             \\x1384\x1385\x1386\x1387\x1388\x1389\x138A\x138B\x138C\x138D\
             \\x138E\x138F\x1390\x1391\x1392\x1393\x1394\x1395\x1396\x1397\
             \\x1398\x1399\x2D80\x2D81\x2D82\x2D83\x2D84\x2D85\x2D86\x2D87\
             \\x2D88\x2D89\x2D8A\x2D8B\x2D8C\x2D8D\x2D8E\x2D8F\x2D90\x2D91\
             \\x2D92\x2D93\x2D94\x2D95\x2D96\x2DA0\x2DA1\x2DA2\x2DA3\x2DA4\
             \\x2DA5\x2DA6\x2DA8\x2DA9\x2DAA\x2DAB\x2DAC\x2DAD\x2DAE\x2DB0\
             \\x2DB1\x2DB2\x2DB3\x2DB4\x2DB5\x2DB6\x2DB8\x2DB9\x2DBA\x2DBB\
             \\x2DBC\x2DBD\x2DBE\x2DC0\x2DC1\x2DC2\x2DC3\x2DC4\x2DC5\x2DC6\
             \\x2DC8\x2DC9\x2DCA\x2DCB\x2DCC\x2DCD\x2DCE\x2DD0\x2DD1\x2DD2\
             \\x2DD3\x2DD4\x2DD5\x2DD6\x2DD8\x2DD9\x2DDA\x2DDB\x2DDC\x2DDD\
             \\x2DDE"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("tk", LanguageData
      { language = "tk"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x68\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\x77\
             \\x79\x7A\xE4\xE7\xF6\xFC\xFD\x148\x15F\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x76\x78"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x28\x29\x2A\x2C\x2D\x2E\x3A\
             \\x3B\x3F\x40\x5B\x5D\x7B\x7D\xA7\x2011\x2013\
             \\x2014\x201C\x201D\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("to", LanguageData
      { language = "to"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x65\x66\x68\x69\x6B\x6C\x6D\x6E\x6E\x67\
             \\x6F\x70\x73\x74\x75\x76\xE1\xE9\xED\xF3\
             \\xFA\x101\x113\x12B\x14D\x16B\x2BB"
          , _exAuxiliary = Just $ Set.fromList
             "\x62\x63\x64\x67\x6A\x71\x72\x77\x78\x79\
             \\x7A\xE0\xE2\xE4\xE5\xE6\xE7\xE8\xEA\xEB\
             \\xEC\xEE\xEF\xF1\xF2\xF4\xF6\xF8\xF9\xFB\
             \\xFC\xFF\x103\x115\x12D\x14F\x153\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("tr", LanguageData
      { language = "tr"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x79\x7A\xE7\xF6\xFC\x11F\x130\x131\x15F"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\xDF\xE0\xE1\xE2\xE3\xE4\xE5\
             \\xE6\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF1\
             \\xF2\xF3\xF4\xF8\xF9\xFA\xFB\xFF\x101\x103\
             \\x113\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("tt", LanguageData
      { language = "tt"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x446\x447\x448\x449\x44A\x44B\x44C\x44D\
             \\x44E\x44F\x451\x497\x4A3\x4AF\x4BB\x4D9\x4E9"
          , _exAuxiliary = Just $ Set.fromList
             "\x493\x49B"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2026\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("twq", LanguageData
      { language = "twq"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x77\x78\x79\x7A\xE3\xF5\x14B\x161\x17E\
             \\x272\x1EBD"
          , _exAuxiliary = Just $ Set.fromList
             "\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("tzm", LanguageData
      { language = "tzm"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x67\x2B7\x68\x69\
             \\x6A\x6B\x6B\x2B7\x6C\x6D\x6E\x71\x72\x73\x74\
             \\x75\x77\x78\x79\x7A\x25B\x263\x1E0D\x1E25\x1E5B\
             \\x1E63\x1E6D"
          , _exAuxiliary = Just $ Set.fromList
             "\x6F\x70\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ug", LanguageData
      { language = "ug"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x626\x627\x626\x647\x626\x648\x626\x649\x626\x6C6\x626\x6C7\x626\x6C8\x626\x6D0\x627\x628\
             \\x62A\x62C\x62E\x62F\x631\x632\x633\x634\x63A\x641\
             \\x642\x643\x644\x645\x646\x648\x649\x64A\x67E\x686\
             \\x698\x6AD\x6AF\x6BE\x6C6\x6C7\x6C8\x6CB\x6D0\x6D5"
          , _exAuxiliary = Just $ Set.fromList
             "\x626\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("uk", LanguageData
      { language = "uk"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x2BC\x430\x431\x432\x433\x434\x435\x436\x437\x438\
             \\x439\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\
             \\x443\x444\x445\x446\x447\x448\x449\x44C\x44E\x44F\
             \\x454\x456\x457\x491"
          , _exAuxiliary = Just $ Set.fromList
             "\x430\x301\x435\x301\x438\x301\x43E\x301\x443\x301\x44A\x44B\x44D\x44E\x301\x44F\x301\
             \\x451\x454\x301\x456\x301\x457\x301"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2A\x2C\x2D\x2E\x2F\
             \\x3A\x3B\x3F\x40\x5B\x5C\x5D\x7B\x7D\xA7\
             \\xAB\xBB\x2011\x2013\x2019\x201C\x201E\x2116"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("ur", LanguageData
      { language = "ur"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x627\x628\x62A\x62B\x62C\x62D\x62E\x62F\x630\
             \\x631\x632\x633\x634\x635\x636\x637\x638\x639\x63A\
             \\x641\x642\x644\x645\x646\x648\x679\x67E\x686\x688\
             \\x691\x698\x6A9\x6AF\x6BE\x6C1\x6CC\x6D2"
          , _exAuxiliary = Just $ Set.fromList
             "\x600\x601\x602\x603\x200C\x200D\x200E\x200F\x622\x623\x624\x626\x629\x647\x64A\x64B\x64C\
             \\x64D\x64E\x64F\x650\x651\x652\x654\x656\x657\x658\
             \\x670\x67A\x67B\x67C\x67D\x6BA\x6C2\x6C3"
          , _exPunctuation = Just $ Set.fromList
             "\x28\x29\x2E\x3A\x5B\x5D\x60C\x60D\x61B\x61F\
             \\x66B\x66C\x6D4"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x6F0\x31\x6F1\x32\x6F2\x33\x6F3\x34\x6F4\
             \\x35\x6F5\x36\x6F6\x37\x6F7\x38\x6F8\x39\x6F9\x66B\x66C\x200E\x2011\x2030"
          }
      }
    )
  , ("uz", LanguageData
      { language = "uz"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x68\x64\x65\x66\x67\x67\x2BB\x68\x69\
             \\x6A\x6B\x6C\x6D\x6E\x6F\x6F\x2BB\x70\x71\x72\
             \\x73\x73\x68\x74\x75\x76\x78\x79\x7A\x2BC"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x77\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\
             \\xE8\xE9\xEA\xEB\xEC\xED\xEE\xEF\xF1\xF2\
             \\xF3\xF4\xF6\xF8\xF9\xFA\xFB\xFC\xFF\x101\
             \\x103\x113\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("vai", LanguageData
      { language = "vai"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\xA500\xA501\xA502\xA503\xA504\xA505\xA506\xA507\xA508\xA509\
             \\xA50A\xA50B\xA50C\xA50D\xA50E\xA50F\xA510\xA511\xA512\xA513\
             \\xA514\xA515\xA516\xA517\xA518\xA519\xA51A\xA51B\xA51C\xA51D\
             \\xA51E\xA51F\xA520\xA521\xA522\xA523\xA524\xA525\xA526\xA527\
             \\xA528\xA529\xA52A\xA52B\xA52C\xA52D\xA52E\xA52F\xA530\xA531\
             \\xA532\xA533\xA534\xA535\xA536\xA537\xA538\xA539\xA53A\xA53B\
             \\xA53C\xA53D\xA53E\xA53F\xA540\xA541\xA542\xA543\xA544\xA545\
             \\xA546\xA547\xA548\xA549\xA54A\xA54B\xA54C\xA54D\xA54E\xA54F\
             \\xA550\xA551\xA552\xA553\xA554\xA555\xA556\xA557\xA558\xA559\
             \\xA55A\xA55B\xA55C\xA55D\xA55E\xA55F\xA560\xA561\xA562\xA563\
             \\xA564\xA565\xA566\xA567\xA568\xA569\xA56A\xA56B\xA56C\xA56D\
             \\xA56E\xA56F\xA570\xA571\xA572\xA573\xA574\xA575\xA576\xA577\
             \\xA578\xA579\xA57A\xA57B\xA57C\xA57D\xA57E\xA57F\xA580\xA581\
             \\xA582\xA583\xA584\xA585\xA586\xA587\xA588\xA589\xA58A\xA58B\
             \\xA58C\xA58D\xA58E\xA58F\xA590\xA591\xA592\xA593\xA594\xA595\
             \\xA596\xA597\xA598\xA599\xA59A\xA59B\xA59C\xA59D\xA59E\xA59F\
             \\xA5A0\xA5A1\xA5A2\xA5A3\xA5A4\xA5A5\xA5A6\xA5A7\xA5A8\xA5A9\
             \\xA5AA\xA5AB\xA5AC\xA5AD\xA5AE\xA5AF\xA5B0\xA5B1\xA5B2\xA5B3\
             \\xA5B4\xA5B5\xA5B6\xA5B7\xA5B8\xA5B9\xA5BA\xA5BB\xA5BC\xA5BD\
             \\xA5BE\xA5BF\xA5C0\xA5C1\xA5C2\xA5C3\xA5C4\xA5C5\xA5C6\xA5C7\
             \\xA5C8\xA5C9\xA5CA\xA5CB\xA5CC\xA5CD\xA5CE\xA5CF\xA5D0\xA5D1\
             \\xA5D2\xA5D3\xA5D4\xA5D5\xA5D6\xA5D7\xA5D8\xA5D9\xA5DA\xA5DB\
             \\xA5DC\xA5DD\xA5DE\xA5DF\xA5E0\xA5E1\xA5E2\xA5E3\xA5E4\xA5E5\
             \\xA5E6\xA5E7\xA5E8\xA5E9\xA5EA\xA5EB\xA5EC\xA5ED\xA5EE\xA5EF\
             \\xA5F0\xA5F1\xA5F2\xA5F3\xA5F4\xA5F5\xA5F6\xA5F7\xA5F8\xA5F9\
             \\xA5FA\xA5FB\xA5FC\xA5FD\xA5FE\xA5FF\xA600\xA601\xA602\xA603\
             \\xA604\xA605\xA606\xA607\xA608\xA609\xA60A\xA60B\xA60C\xA610\
             \\xA611\xA612\xA62A\xA62B"
          , _exAuxiliary = Just $ Set.fromList
             "\xA613\xA614\xA615\xA616\xA617\xA618\xA619\xA61A\xA61B\xA61C\
             \\xA61D\xA61E\xA61F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("vi", LanguageData
      { language = "vi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE0\xE1\xE2\xE3\
             \\xE8\xE9\xEA\xEC\xED\xF2\xF3\xF4\xF5\xF9\
             \\xFA\xFD\x103\x111\x129\x169\x1A1\x1B0\x1EA1\x1EA3\
             \\x1EA5\x1EA7\x1EA9\x1EAB\x1EAD\x1EAF\x1EB1\x1EB3\x1EB5\x1EB7\
             \\x1EB9\x1EBB\x1EBD\x1EBF\x1EC1\x1EC3\x1EC5\x1EC7\x1EC9\x1ECB\
             \\x1ECD\x1ECF\x1ED1\x1ED3\x1ED5\x1ED7\x1ED9\x1EDB\x1EDD\x1EDF\
             \\x1EE1\x1EE3\x1EE5\x1EE7\x1EE9\x1EEB\x1EED\x1EEF\x1EF1\x1EF3\
             \\x1EF5\x1EF7\x1EF9"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("vun", LanguageData
      { language = "vun"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("wae", LanguageData
      { language = "wae"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE1\xE3\xE4\xE9\
             \\xED\xF3\xF5\xF6\xFA\xFC\x10D\x161\x169"
          , _exAuxiliary = Just $ Set.fromList
             "\xDF\xE0\xE2\xE5\xE6\xE7\xE8\xEA\xEB\xEC\
             \\xEE\xEF\xF1\xF2\xF4\xF8\xF9\xFB\xFF\x101\
             \\x103\x113\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2011\x2019\x2030"
          }
      }
    )
  , ("wo", LanguageData
      { language = "wo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x69\x6A\x6B\
             \\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\x75\
             \\x77\x78\x79\xE0\xE9\xEB\xF1\xF3\x14B"
          , _exAuxiliary = Just $ Set.fromList
             "\x68\x76\x7A\xE3"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2011"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("xh", LanguageData
      { language = "xh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("xog", LanguageData
      { language = "xog"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("yav", LanguageData
      { language = "yav"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x68\x69\x6B\x6C\
             \\x6D\x6D\x62\x6E\x6E\x79\x6F\x70\x73\x74\x75\x76\
             \\x77\x79\xE0\xE1\xE2\xE8\xE9\xEC\xED\xEE\
             \\xF2\xF3\xF4\xF9\xFA\xFB\x101\x12B\x14B\x14B\x67\
             \\x14D\x16B\x1CE\x1D2\x1D4\x254\x254\x300\x254\x301\x25B\x25B\x300\
             \\x25B\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x67\x6A\x71\x72\x78\x7A"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("yi", LanguageData
      { language = "yi"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x5D0\x5D0\x5B7\x5D0\x5B8\x5D1\x5D1\x5BF\x5D2\x5D3\x5D3\x5D6\x5E9\x5D4\x5D5\
             \\x5D5\x5BC\x5D5\x5D5\x5D5\x5D9\x5D6\x5D6\x5E9\x5D7\x5D8\x5D8\x5E9\x5D9\x5D9\x5B4\
             \\x5D9\x5D9\x5DA\x5DB\x5DB\x5BC\x5DC\x5DD\x5DE\x5DF\x5E0\x5E1\
             \\x5E2\x5E3\x5E4\x5BC\x5E4\x5BF\x5E5\x5E6\x5E7\x5E8\x5E9\x5E9\x5C2\
             \\x5EA\x5EA\x5BC\x5F2\x5B7"
          , _exAuxiliary = Just $ Set.fromList
             "\x200E\x200F"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x27\x28\x29\x2C\x2D\x2E\x2F\x3A\
             \\x3B\x3F\x5B\x5D\x5BE\x5F3\x5F4\x2010\x2011\x2013\
             \\x2014"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("yo", LanguageData
      { language = "yo"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x67\x62\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x77\x79\xE0\xE1\xE8\xE9\xEC\xED\xF2\xF3\
             \\xF9\xFA\x144\x1E63\x1EB9\x1EB9\x300\x1EB9\x301\x1ECD\x1ECD\x300\x1ECD\x301"
          , _exAuxiliary = Just $ Set.fromList
             "\x63\x71\x76\x78\x7A"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x2010\x2011\x2013\x2014\
             \\x2018\x2019\x201C\x201D\x2020\x2021\x2026\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("yue", LanguageData
      { language = "yue"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x4E00\x4E01\x4E03\x4E08\x4E09\x4E0A\x4E0B\x4E0C\x4E0D\x4E11\
             \\x4E14\x4E16\x4E18\x4E19\x4E1F\x4E26\x4E2D\x4E32\x4E38\x4E39\
             \\x4E3B\x4E43\x4E45\x4E48\x4E4B\x4E4E\x4E4F\x4E56\x4E58\x4E59\
             \\x4E5D\x4E5F\x4E7E\x4E82\x4E86\x4E88\x4E8B\x4E8C\x4E8E\x4E91\
             \\x4E92\x4E94\x4E95\x4E9B\x4E9E\x4EA1\x4EA4\x4EA5\x4EA6\x4EA8\
             \\x4EAB\x4EAC\x4EAE\x4EBA\x4EC0\x4EC1\x4EC7\x4ECA\x4ECB\x4ECD\
             \\x4ED4\x4ED6\x4ED8\x4ED9\x4EE3\x4EE4\x4EE5\x4EF0\x4EF2\x4EF6\
             \\x4EFB\x4EFD\x4F01\x4F0A\x4F0D\x4F10\x4F11\x4F19\x4F2F\x4F30\
             \\x4F34\x4F38\x4F3C\x4F3D\x4F46\x4F48\x4F49\x4F4D\x4F4E\x4F4F\
             \\x4F54\x4F55\x4F59\x4F5B\x4F5C\x4F60\x4F69\x4F73\x4F7F\x4F86\
             \\x4F8B\x4F9B\x4F9D\x4FAF\x4FB5\x4FBF\x4FC2\x4FC3\x4FC4\x4FCA\
             \\x4FCF\x4FD7\x4FDD\x4FE0\x4FE1\x4FEE\x4FF1\x4FFE\x5009\x500B\
             \\x500D\x5011\x5012\x5019\x501A\x501F\x502B\x503C\x5047\x5049\
             \\x504F\x505A\x505C\x5065\x5074\x5075\x5076\x5077\x507D\x5085\
             \\x5091\x5098\x5099\x50A2\x50A3\x50B2\x50B3\x50B7\x50BB\x50BE\
             \\x50C5\x50CF\x50D1\x50E7\x50F9\x5100\x5104\x5112\x5118\x512A\
             \\x5141\x5143\x5144\x5145\x5147\x5148\x5149\x514B\x514D\x5152\
             \\x5154\x5165\x5167\x5168\x5169\x516B\x516C\x516D\x516E\x5171\
             \\x5175\x5176\x5177\x5178\x517C\x518A\x518D\x5192\x51A0\x51AC\
             \\x51B0\x51B7\x51C6\x51CB\x51CC\x51CD\x51DD\x51E1\x51F0\x51F1\
             \\x51FA\x51FD\x5200\x5206\x5207\x520A\x5217\x521D\x5224\x5225\
             \\x5228\x5229\x522A\x522E\x5230\x5236\x5237\x523A\x523B\x5243\
             \\x5247\x524C\x524D\x525B\x5269\x526A\x526F\x5272\x5275\x5283\
             \\x5287\x5289\x528D\x529B\x529F\x52A0\x52A9\x52AA\x52AB\x52C1\
             \\x52C7\x52C9\x52D2\x52D5\x52D9\x52DD\x52DE\x52E2\x52E4\x52F5\
             \\x52F8\x52FE\x52FF\x5305\x5308\x5316\x5317\x532F\x5339\x5340\
             \\x5341\x5343\x5347\x5348\x534A\x5352\x5353\x5354\x5357\x535A\
             \\x535C\x5361\x536F\x5370\x5371\x5373\x5377\x5379\x537B\x5384\
             \\x5398\x539A\x539F\x53AD\x53B2\x53BB\x53C3\x53C8\x53CA\x53CB\
             \\x53CD\x53D4\x53D6\x53D7\x53E3\x53E4\x53E5\x53E6\x53EA\x53EB\
             \\x53EC\x53ED\x53EF\x53F0\x53F2\x53F3\x53F8\x5403\x5404\x5408\
             \\x5409\x540A\x540C\x540D\x540E\x5410\x5411\x5412\x541B\x541D\
             \\x541E\x541F\x5420\x5426\x5427\x542B\x5433\x5435\x5438\x5439\
             \\x543E\x5440\x5442\x5446\x544A\x5462\x5468\x5473\x5475\x547C\
             \\x547D\x548C\x5496\x54A6\x54A7\x54AA\x54AC\x54B1\x54C0\x54C1\
             \\x54C7\x54C8\x54C9\x54CE\x54E1\x54E5\x54E6\x54E9\x54EA\x54ED\
             \\x54F2\x5507\x5509\x5510\x5514\x552C\x552E\x552F\x5531\x5535\
             \\x5537\x5538\x5546\x554A\x554F\x555F\x5561\x5564\x5565\x5566\
             \\x556A\x5580\x5582\x5584\x5587\x558A\x5594\x559C\x559D\x55AA\
             \\x55AC\x55AE\x55B2\x55B5\x55CE\x55DA\x55E8\x55EF\x5606\x5609\
             \\x5617\x561B\x5634\x563B\x563F\x5641\x5653\x5668\x5674\x5687\
             \\x568F\x56B4\x56C9\x56DB\x56DE\x56E0\x56F0\x56FA\x5708\x570B\
             \\x570D\x5712\x5713\x5716\x5718\x571C\x571F\x5728\x572D\x5730\
             \\x573E\x5740\x5747\x574E\x5750\x5751\x5761\x5764\x5766\x576A\
             \\x5782\x5783\x578B\x57C3\x57CE\x57D4\x57DF\x57F7\x57F9\x57FA\
             \\x5802\x5805\x5806\x5821\x582A\x5831\x5834\x584A\x5854\x5857\
             \\x585E\x586B\x5875\x5883\x5885\x5893\x589E\x589F\x58A8\x58AE\
             \\x58B3\x58C1\x58C7\x58D3\x58D8\x58DE\x58E2\x58E9\x58EB\x58EC\
             \\x58EF\x58FA\x58FD\x590F\x5915\x5916\x591A\x591C\x5920\x5922\
             \\x5925\x5927\x5929\x592A\x592B\x592E\x5931\x5937\x5938\x593E\
             \\x5947\x5948\x5949\x594E\x594F\x5951\x5954\x5957\x5967\x596A\
             \\x596E\x5973\x5974\x5976\x5979\x597D\x5982\x5999\x599D\x59A5\
             \\x59A8\x59AE\x59B3\x59B9\x59BB\x59C6\x59CA\x59CB\x59D0\x59D1\
             \\x59D3\x59D4\x59FF\x5A01\x5A03\x5A18\x5A1B\x5A41\x5A46\x5A5A\
             \\x5A66\x5A92\x5ABD\x5ACC\x5AE9\x5B50\x5B54\x5B55\x5B57\x5B58\
             \\x5B5D\x5B5F\x5B63\x5B64\x5B69\x5B6B\x5B75\x5B78\x5B83\x5B85\
             \\x5B87\x5B88\x5B89\x5B8B\x5B8C\x5B8F\x5B97\x5B98\x5B99\x5B9A\
             \\x5B9B\x5B9C\x5BA2\x5BA3\x5BA4\x5BAE\x5BB3\x5BB6\x5BB9\x5BBF\
             \\x5BC2\x5BC4\x5BC5\x5BC6\x5BCC\x5BD2\x5BDE\x5BDF\x5BE2\x5BE6\
             \\x5BE7\x5BE8\x5BE9\x5BEB\x5BEC\x5BEE\x5BF5\x5BF6\x5BFA\x5C01\
             \\x5C04\x5C07\x5C08\x5C0A\x5C0B\x5C0D\x5C0E\x5C0F\x5C11\x5C16\
             \\x5C1A\x5C24\x5C31\x5C3A\x5C3C\x5C3E\x5C3F\x5C40\x5C41\x5C45\
             \\x5C46\x5C4B\x5C4D\x5C4F\x5C51\x5C55\x5C60\x5C64\x5C6C\x5C71\
             \\x5CA1\x5CA9\x5CB8\x5CF0\x5CF6\x5CFD\x5D07\x5D19\x5D34\x5D50\
             \\x5DBA\x5DDD\x5DDE\x5DE1\x5DE5\x5DE6\x5DE7\x5DE8\x5DEB\x5DEE\
             \\x5DF1\x5DF2\x5DF3\x5DF4\x5DF7\x5E02\x5E03\x5E0C\x5E15\x5E16\
             \\x5E1A\x5E1B\x5E1D\x5E25\x5E2B\x5E2D\x5E33\x5E36\x5E38\x5E3D\
             \\x5E45\x5E55\x5E5F\x5E63\x5E6B\x5E72\x5E73\x5E74\x5E78\x5E79\
             \\x5E7B\x5E7C\x5E7D\x5E7E\x5E87\x5E8A\x5E8F\x5E95\x5E97\x5E9A\
             \\x5E9C\x5EA6\x5EA7\x5EAB\x5EAD\x5EB7\x5EB8\x5EC8\x5EC9\x5ED6\
             \\x5EDF\x5EE0\x5EE2\x5EE3\x5EF3\x5EF6\x5EF7\x5EFA\x5F04\x5F0F\
             \\x5F15\x5F17\x5F18\x5F1F\x5F26\x5F31\x5F35\x5F37\x5F48\x5F4A\
             \\x5F4C\x5F4E\x5F5D\x5F5E\x5F62\x5F65\x5F69\x5F6C\x5F6D\x5F70\
             \\x5F71\x5F79\x5F7C\x5F80\x5F81\x5F85\x5F88\x5F8B\x5F8C\x5F90\
             \\x5F91\x5F92\x5F97\x5F9E\x5FA9\x5FAE\x5FB5\x5FB7\x5FB9\x5FC3\
             \\x5FC5\x5FCC\x5FCD\x5FD7\x5FD8\x5FD9\x5FE0\x5FE1\x5FEB\x5FF5\
             \\x5FFD\x600E\x6012\x6015\x6016\x601D\x6021\x6025\x6027\x6028\
             \\x602A\x6046\x6050\x6062\x6065\x6068\x6069\x606D\x606F\x6070\
             \\x6085\x6089\x6094\x609F\x60A0\x60A8\x60B2\x60B6\x60C5\x60D1\
             \\x60DC\x60E0\x60E1\x60F1\x60F3\x60F9\x6101\x6108\x6109\x610F\
             \\x611A\x611B\x611F\x6148\x614B\x6155\x6158\x6162\x6163\x6167\
             \\x616E\x6170\x6176\x617E\x6182\x618A\x6190\x6191\x61B2\x61B6\
             \\x61BE\x61C2\x61C9\x61E8\x61F6\x61F7\x61FC\x6200\x6208\x620A\
             \\x620C\x6210\x6211\x6212\x6216\x622A\x6230\x6232\x6234\x6236\
             \\x623F\x6240\x6241\x6247\x624B\x624D\x624E\x6253\x6258\x6263\
             \\x6265\x626D\x626E\x626F\x6279\x627E\x627F\x6280\x6284\x628A\
             \\x6293\x6295\x6297\x6298\x62AB\x62AC\x62B1\x62B5\x62B9\x62BD\
             \\x62C6\x62C9\x62CB\x62CD\x62CF\x62D2\x62D4\x62D6\x62DB\x62DC\
             \\x62EC\x62F3\x62FC\x62FE\x62FF\x6301\x6307\x6309\x6311\x6316\
             \\x632A\x632F\x633A\x634F\x6350\x6355\x6367\x6368\x6372\x6377\
             \\x6383\x6388\x6389\x638C\x6392\x639B\x63A0\x63A1\x63A2\x63A5\
             \\x63A7\x63A8\x63AA\x63B0\x63CF\x63D0\x63D2\x63DA\x63DB\x63E1\
             \\x63EE\x63F4\x63F9\x640D\x640F\x6416\x641C\x641E\x642C\x642D\
             \\x6436\x6440\x6458\x6469\x6478\x6490\x6492\x6495\x649E\x64A3\
             \\x64A5\x64AD\x64B2\x64BE\x64BF\x64C1\x64C7\x64CA\x64CB\x64CD\
             \\x64CE\x64D4\x64DA\x64E0\x64E6\x64EC\x64F4\x64FA\x64FE\x6500\
             \\x651D\x6524\x652F\x6536\x6539\x653B\x653E\x653F\x6545\x6548\
             \\x654D\x654F\x6551\x6557\x6558\x6559\x655D\x655E\x6562\x6563\
             \\x6566\x656C\x6574\x6575\x6578\x6587\x6590\x6591\x6597\x6599\
             \\x659C\x65A7\x65AF\x65B0\x65B7\x65B9\x65BC\x65BD\x65C1\x65C5\
             \\x65CB\x65CF\x65D7\x65E2\x65E5\x65E6\x65E9\x65ED\x65FA\x6602\
             \\x6606\x6607\x660C\x660E\x660F\x6613\x661F\x6620\x6625\x6628\
             \\x662D\x662F\x6642\x6649\x6652\x665A\x6668\x666E\x666F\x6674\
             \\x6676\x667A\x6691\x6696\x6697\x66AB\x66AE\x66B4\x66C6\x66C7\
             \\x66C9\x66EC\x66F0\x66F2\x66F3\x66F4\x66F8\x66FC\x66FE\x66FF\
             \\x6700\x6703\x6708\x6709\x670B\x670D\x6714\x6717\x671B\x671D\
             \\x671F\x6728\x672A\x672B\x672C\x672D\x6731\x6735\x6749\x674E\
             \\x6750\x6751\x6756\x675C\x675F\x676F\x6770\x6771\x677E\x677F\
             \\x6790\x6797\x679C\x679D\x67AF\x67B6\x67CF\x67D0\x67D3\x67D4\
             \\x67E5\x67EC\x67EF\x67F3\x67F4\x6813\x6821\x6838\x6839\x683C\
             \\x683D\x6843\x6848\x684C\x6851\x6881\x6885\x689D\x68A8\x68AF\
             \\x68B0\x68B5\x68C4\x68C9\x68CB\x68CD\x68D2\x68D5\x68DA\x68EE\
             \\x68FA\x6905\x690D\x6912\x6930\x694A\x6953\x695A\x696D\x6975\
             \\x6982\x699C\x69AE\x69CB\x69CC\x69CD\x6A02\x6A13\x6A19\x6A1E\
             \\x6A21\x6A23\x6A39\x6A44\x6A47\x6A4B\x6A58\x6A59\x6A5F\x6A6B\
             \\x6A80\x6A94\x6AA2\x6AAC\x6AB8\x6ADA\x6AFB\x6B04\x6B0A\x6B16\
             \\x6B20\x6B21\x6B23\x6B32\x6B3A\x6B3D\x6B3E\x6B49\x6B4C\x6B50\
             \\x6B61\x6B62\x6B63\x6B64\x6B65\x6B66\x6B72\x6B77\x6B78\x6B7B\
             \\x6B8A\x6B8B\x6B98\x6BAD\x6BB5\x6BBA\x6BBC\x6BC0\x6BC5\x6BCD\
             \\x6BCF\x6BD2\x6BD4\x6BDB\x6BEB\x6C0F\x6C11\x6C23\x6C34\x6C38\
             \\x6C41\x6C42\x6C57\x6C5D\x6C5F\x6C60\x6C61\x6C6A\x6C76\x6C7A\
             \\x6C7D\x6C83\x6C88\x6C89\x6C92\x6C96\x6C99\x6CAB\x6CAE\x6CB3\
             \\x6CB9\x6CBB\x6CBF\x6CC1\x6CC9\x6CCA\x6CD5\x6CE1\x6CE2\x6CE3\
             \\x6CE5\x6CE8\x6CF0\x6CF3\x6D0B\x6D17\x6D1B\x6D1E\x6D29\x6D2A\
             \\x6D32\x6D3B\x6D3D\x6D3E\x6D41\x6D63\x6D66\x6D69\x6D6A\x6D6E\
             \\x6D74\x6D77\x6D87\x6D88\x6D89\x6D8E\x6DAE\x6DAF\x6DB2\x6DB5\
             \\x6DBC\x6DC7\x6DCB\x6DD1\x6DDA\x6DE1\x6DE8\x6DF1\x6DF7\x6DFA\
             \\x6E05\x6E1B\x6E21\x6E2C\x6E2F\x6E38\x6E56\x6E58\x6E6F\x6E90\
             \\x6E96\x6E9C\x6E9D\x6EAA\x6EAB\x6EC4\x6EC5\x6ECB\x6ED1\x6EF4\
             \\x6EFE\x6EFF\x6F02\x6F0F\x6F14\x6F20\x6F22\x6F2B\x6F32\x6F38\
             \\x6F3F\x6F54\x6F58\x6F5B\x6F6E\x6FA1\x6FA4\x6FB3\x6FC0\x6FC3\
             \\x6FD5\x6FDF\x6FE4\x6FEB\x6FF1\x700F\x704C\x7063\x706B\x7070\
             \\x707D\x708E\x70AE\x70B8\x70BA\x70C8\x70CF\x70D8\x70E4\x70F9\
             \\x710A\x7119\x7121\x7126\x7130\x7136\x7159\x715E\x7167\x7169\
             \\x716E\x718A\x719F\x71B1\x71C3\x71C8\x71D2\x71D9\x71DF\x7206\
             \\x720D\x7210\x721B\x722A\x722C\x722D\x7235\x7236\x7238\x723A\
             \\x723D\x723E\x7246\x7247\x7248\x724C\x7259\x725B\x7260\x7267\
             \\x7269\x7272\x7279\x727D\x7280\x72A7\x72AC\x72AF\x72C0\x72C2\
             \\x72D0\x72D7\x72E0\x72E1\x72F8\x72FC\x731B\x731C\x7329\x7334\
             \\x7336\x733E\x733F\x7344\x7345\x734E\x7368\x7372\x7378\x737A\
             \\x737B\x737E\x7384\x7387\x7389\x738B\x73A9\x73AB\x73B2\x73BB\
             \\x73CA\x73CD\x73E0\x73E5\x73ED\x73FE\x7403\x7406\x7409\x742A\
             \\x7434\x7459\x745C\x745E\x745F\x7464\x746A\x7470\x74B0\x74DC\
             \\x74E2\x74E6\x74F6\x7515\x7518\x751A\x751C\x751F\x7522\x7528\
             \\x7530\x7531\x7532\x7533\x7537\x7538\x754C\x7559\x7562\x7565\
             \\x756A\x756B\x7570\x7576\x7586\x758F\x7591\x75B2\x75BC\x75BE\
             \\x75C5\x75D5\x75DB\x75F4\x760B\x7626\x7627\x7642\x7661\x7678\
             \\x767B\x767C\x767D\x767E\x7682\x7684\x7686\x7687\x76AE\x76BF\
             \\x76C3\x76C6\x76C8\x76CA\x76D4\x76DB\x76DC\x76DF\x76E1\x76E3\
             \\x76E4\x76E5\x76E7\x76EE\x76F2\x76F4\x76F8\x76FC\x76FE\x7701\
             \\x7709\x770B\x771F\x7720\x773C\x773E\x774F\x775B\x7761\x7763\
             \\x7787\x778C\x77A7\x77AA\x77AD\x77DB\x77E3\x77E5\x77ED\x77F3\
             \\x7802\x780D\x7814\x7832\x7834\x786C\x788E\x7897\x789F\x78A7\
             \\x78A9\x78B0\x78BA\x78BC\x78C1\x78DA\x78E8\x78EF\x790E\x7919\
             \\x792B\x793A\x793E\x7948\x7955\x7956\x795A\x795B\x795D\x795E\
             \\x7965\x7968\x797F\x7981\x798D\x798E\x798F\x79AA\x79AE\x79B1\
             \\x79BF\x79C0\x79C1\x79CB\x79D1\x79D2\x79D8\x79DF\x79E4\x79E6\
             \\x79FB\x7A05\x7A0B\x7A0D\x7A2E\x7A31\x7A3B\x7A3F\x7A40\x7A46\
             \\x7A4C\x7A4D\x7A69\x7A76\x7A79\x7A7A\x7A7F\x7A81\x7A84\x7A97\
             \\x7AA9\x7AAE\x7AB6\x7ACB\x7AD9\x7ADF\x7AE0\x7AE5\x7AEF\x7AF6\
             \\x7AF9\x7AFF\x7B11\x7B1B\x7B26\x7B28\x7B2C\x7B46\x7B49\x7B4B\
             \\x7B54\x7B56\x7B77\x7B80\x7B8F\x7B94\x7B97\x7BA1\x7BAD\x7BB1\
             \\x7BC0\x7BC4\x7BC7\x7BC9\x7BF7\x7C21\x7C2B\x7C3D\x7C3F\x7C43\
             \\x7C4C\x7C4D\x7C60\x7C64\x7C73\x7C89\x7C97\x7CB5\x7CBE\x7CCA\
             \\x7CD5\x7CD6\x7CDF\x7CE5\x7CFB\x7CFE\x7D00\x7D04\x7D05\x7D09\
             \\x7D0D\x7D10\x7D14\x7D19\x7D1A\x7D1B\x7D20\x7D22\x7D2B\x7D2E\
             \\x7D2F\x7D30\x7D33\x7D39\x7D42\x7D44\x7D50\x7D55\x7D61\x7D66\
             \\x7D71\x7D72\x7D93\x7D9C\x7DA0\x7DAD\x7DB1\x7DB2\x7DBD\x7DBF\
             \\x7DCA\x7DD2\x7DDA\x7DE3\x7DE8\x7DE9\x7DEC\x7DEF\x7DF4\x7E1B\
             \\x7E23\x7E2B\x7E2E\x7E31\x7E3D\x7E3E\x7E41\x7E43\x7E46\x7E54\
             \\x7E5E\x7E61\x7E69\x7E6A\x7E73\x7E7C\x7E8C\x7E96\x7F38\x7F3A\
             \\x7F48\x7F50\x7F55\x7F69\x7F6A\x7F6E\x7F70\x7F72\x7F75\x7F77\
             \\x7F85\x7F8A\x7F8E\x7F9E\x7FA4\x7FA9\x7FBD\x7FC1\x7FD2\x7FD4\
             \\x7FF0\x7FF9\x7FFB\x7FFC\x8000\x8001\x8003\x8005\x800C\x800D\
             \\x8010\x8017\x8033\x8036\x804A\x8056\x805A\x805E\x806F\x8070\
             \\x8072\x8077\x807D\x807E\x8089\x808C\x809A\x80A1\x80A5\x80A9\
             \\x80AF\x80B2\x80BA\x80CC\x80CE\x80D6\x80DE\x80E1\x80F8\x80FD\
             \\x8106\x8108\x8116\x812B\x8150\x8153\x8154\x8166\x8170\x8173\
             \\x8179\x817F\x819A\x81A0\x81BD\x81C2\x81C9\x81D8\x81DF\x81E3\
             \\x81E5\x81E8\x81EA\x81ED\x81F3\x81F4\x81FA\x8207\x8208\x8209\
             \\x820A\x820C\x820D\x8212\x821E\x821F\x822A\x822C\x8239\x8266\
             \\x826F\x8272\x827E\x8299\x829D\x82AC\x82AD\x82B1\x82B3\x82BD\
             \\x82E3\x82E5\x82E6\x82F1\x8304\x8305\x832B\x8332\x8335\x8336\
             \\x8338\x8349\x8352\x8377\x837C\x8389\x838A\x838E\x8393\x8396\
             \\x83AB\x83C7\x83CC\x83DC\x83E9\x83EF\x83F2\x8404\x840A\x840E\
             \\x842C\x8435\x843D\x8449\x8457\x845B\x8461\x8475\x8482\x8499\
             \\x849C\x84B2\x84B8\x84BC\x84C4\x84C9\x84CB\x84EE\x8514\x8515\
             \\x8521\x8523\x8525\x852C\x8549\x856D\x857E\x8584\x8591\x85A6\
             \\x85A9\x85AA\x85AF\x85C9\x85CD\x85CF\x85DD\x85E4\x85E5\x8606\
             \\x8607\x860B\x8611\x862D\x863F\x864E\x8655\x865B\x865F\x8667\
             \\x868A\x8693\x86AF\x86C7\x86CB\x86D9\x8702\x871C\x8725\x8734\
             \\x8759\x875F\x8760\x8766\x8776\x8782\x8783\x878D\x879E\x87A2\
             \\x87BA\x87C0\x87C4\x87CB\x87D1\x87F2\x87F3\x87F9\x87FB\x8805\
             \\x880D\x8815\x8823\x883B\x8840\x884C\x8853\x8857\x885B\x885D\
             \\x8861\x8863\x8868\x886B\x888B\x888D\x88AB\x88C1\x88C2\x88CF\
             \\x88D5\x88DC\x88DD\x88E1\x88F1\x88F9\x88FD\x8907\x8910\x8932\
             \\x896A\x896F\x897F\x8981\x8986\x898B\x898F\x8996\x89AA\x89BA\
             \\x89BD\x89C0\x89D2\x89E3\x89F8\x8A00\x8A02\x8A08\x8A0A\x8A0E\
             \\x8A13\x8A17\x8A18\x8A1D\x8A25\x8A2A\x8A2D\x8A31\x8A34\x8A3A\
             \\x8A3B\x8A3C\x8A55\x8A5E\x8A62\x8A66\x8A69\x8A71\x8A72\x8A73\
             \\x8A87\x8A8C\x8A8D\x8A93\x8A95\x8A9E\x8AA0\x8AA4\x8AAA\x8AB0\
             \\x8AB2\x8ABC\x8ABF\x8AC7\x8ACB\x8AD2\x8AD6\x8AF8\x8AFA\x8AFE\
             \\x8B00\x8B02\x8B0E\x8B1B\x8B1D\x8B49\x8B58\x8B5C\x8B66\x8B6F\
             \\x8B70\x8B77\x8B7D\x8B80\x8B8A\x8B93\x8B9A\x8C37\x8C46\x8C48\
             \\x8C4E\x8C50\x8C54\x8C61\x8C6A\x8C6C\x8C79\x8C8C\x8C93\x8C9D\
             \\x8C9E\x8CA0\x8CA1\x8CA2\x8CA8\x8CAA\x8CAB\x8CAC\x8CB4\x8CB7\
             \\x8CBB\x8CBC\x8CC0\x8CC7\x8CC8\x8CD3\x8CDC\x8CDE\x8CE2\x8CE3\
             \\x8CE4\x8CE6\x8CEA\x8CED\x8CF4\x8CFA\x8CFC\x8CFD\x8D08\x8D0A\
             \\x8D0F\x8D1B\x8D64\x8D6B\x8D70\x8D77\x8D85\x8D8A\x8D95\x8D99\
             \\x8DA3\x8DA8\x8DB3\x8DC6\x8DCC\x8DCE\x8DD1\x8DDD\x8DDF\x8DE1\
             \\x8DEA\x8DEF\x8DF3\x8E0F\x8E22\x8E29\x8E5F\x8E64\x8E8D\x8EAB\
             \\x8EB2\x8ECA\x8ECC\x8ECD\x8ED2\x8EDF\x8EF8\x8F03\x8F09\x8F14\
             \\x8F15\x8F1B\x8F1D\x8F29\x8F2A\x8F2F\x8F38\x8F49\x8F4E\x8F5F\
             \\x8F9B\x8F9C\x8FA3\x8FA6\x8FA8\x8FAD\x8FAF\x8FB0\x8FB1\x8FB2\
             \\x8FC5\x8FCE\x8FD1\x8FD4\x8FE6\x8FEA\x8FEB\x8FF0\x8FF4\x8FF7\
             \\x8FFD\x9000\x9001\x9003\x9006\x900F\x9010\x9014\x9019\x901A\
             \\x901B\x901D\x901F\x9020\x9022\x9023\x9031\x9032\x9038\x903C\
             \\x9047\x904A\x904B\x904D\x904E\x9053\x9054\x9055\x9059\x905C\
             \\x9060\x9069\x906D\x906E\x9072\x9077\x9078\x907A\x907F\x9080\
             \\x9081\x9084\x908A\x908F\x90A3\x90A6\x90AA\x90B1\x90CE\x90E8\
             \\x90ED\x90F5\x90FD\x9102\x9109\x9119\x912D\x9130\x9149\x914D\
             \\x9152\x916A\x9177\x9178\x9189\x9192\x919C\x91AB\x91AC\x91C7\
             \\x91CB\x91CC\x91CD\x91CE\x91CF\x91D1\x91DD\x91E3\x9234\x9245\
             \\x9262\x9264\x9280\x9285\x9296\x9298\x92B3\x92B7\x92C1\x92D2\
             \\x92FC\x9304\x9322\x9326\x9328\x932B\x932F\x9336\x934A\x934B\
             \\x9375\x937E\x938A\x9396\x93AE\x93C8\x93E1\x93E2\x9418\x9421\
             \\x9435\x9451\x947F\x9577\x9580\x9583\x9589\x958B\x958F\x9592\
             \\x9593\x95A3\x95A9\x95B1\x95C6\x95CA\x95CD\x95D0\x95DC\x95E1\
             \\x9631\x9632\x963B\x963F\x9640\x9644\x964D\x9650\x9662\x9663\
             \\x9664\x966A\x9670\x9673\x9675\x9676\x9677\x9678\x967D\x9686\
             \\x968A\x968E\x9694\x969B\x969C\x96A8\x96AA\x96B1\x96BB\x96C4\
             \\x96C5\x96C6\x96C9\x96CC\x96D6\x96D9\x96DC\x96DE\x96E2\x96E3\
             \\x96E8\x96EA\x96F2\x96F6\x96F7\x96FB\x9700\x9707\x970D\x971C\
             \\x9727\x9732\x9738\x9739\x9742\x9748\x9752\x9756\x9759\x975C\
             \\x975E\x9760\x9762\x9769\x9774\x977C\x978B\x97AD\x97C3\x97CB\
             \\x97D3\x97F3\x97FB\x97FF\x9801\x9802\x9805\x9806\x9808\x980C\
             \\x9810\x9811\x9813\x9817\x9818\x981E\x982D\x9838\x983B\x9846\
             \\x984C\x984D\x984F\x9858\x985B\x985E\x9867\x986F\x98A8\x98B1\
             \\x98C4\x98C6\x98DB\x98DF\x98EA\x98EF\x98F2\x98FD\x98FE\x9903\
             \\x9905\x990A\x990C\x9910\x9918\x991A\x9928\x993E\x9996\x9999\
             \\x99AC\x99D0\x99D5\x99DB\x99DD\x99F1\x9A0E\x9A19\x9A37\x9A45\
             \\x9A55\x9A57\x9A5A\x9AA8\x9AD4\x9AD8\x9AEE\x9B06\x9B0D\x9B25\
             \\x9B27\x9B31\x9B3C\x9B41\x9B42\x9B45\x9B54\x9B5A\x9B6F\x9B77\
             \\x9B91\x9BAE\x9BCA\x9BE8\x9C77\x9CE5\x9CE9\x9CF3\x9CF4\x9D28\
             \\x9D3B\x9D5D\x9D61\x9DB4\x9DF9\x9E1A\x9E7D\x9E7F\x9E97\x9EA5\
             \\x9EB5\x9EBB\x9EBC\x9EC3\x9ECE\x9ED1\x9ED8\x9EDB\x9EDE\x9EE8\
             \\x9F13\x9F20\x9F2C\x9F3B\x9F4A\x9F4B\x9F52\x9F61\x9F8D\x9F90\
             \\x9F9C"
          , _exAuxiliary = Just $ Set.fromList
             "\x4E4D\x4EC2\x4F0F\x4F50\x4FB6\x50F3\x5146\x514C\x5179\x51F8\
             \\x522B\x5238\x52F3\x5351\x535E\x5360\x53F6\x5605\x5824\x588E\
             \\x58E4\x5965\x5B5C\x5CC7\x5DBC\x5DFD\x6817\x6954\x6D85\x6E3E\
             \\x6F8E\x7058\x71E6\x72C4\x7433\x745A\x752B\x7891\x7901\x7E9C\
             \\x8247\x8292\x82D7\x8328\x84EC\x86A9\x8700\x88D8\x8B2C\x914B\
             \\x96B4\x96C0\x9AEA"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\x5F\
             \\x7B\x7D\xA7\xB7\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2025\x2026\x2027\x2030\x2032\x2033\
             \\x2035\x203B\x203E\x3001\x3002\x3003\x3008\x3009\x300A\x300B\
             \\x300C\x300D\x300E\x300F\x3010\x3011\x3014\x3015\x301D\x301E\
             \\xFE30\xFE31\xFE32\xFE33\xFE34\xFE35\xFE36\xFE37\xFE38\xFE39\xFE3A\
             \\xFE3B\xFE3C\xFE3D\xFE3E\xFE3F\xFE40\xFE41\xFE42\xFE43\xFE44\
             \\xFE49\xFE4A\xFE4B\xFE4C\xFE4D\xFE4E\xFE4F\xFE50\xFE51\xFE52\xFE54\xFE55\xFE56\xFE57\xFE58\
             \\xFE59\xFE5A\xFE5B\xFE5C\xFE5D\xFE5E\xFE5F\xFE60\xFE61\xFE63\
             \\xFE68\xFE6A\xFE6B\xFF01\xFF02\xFF03\xFF05\xFF06\xFF07\xFF08\
             \\xFF09\xFF0A\xFF0C\xFF0D\xFF0E\xFF0F\xFF1A\xFF1B\xFF1F\xFF20\
             \\xFF3B\xFF3C\xFF3D\xFF3F\xFF5B\xFF5D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030\x3007\x4E00\x4E03\
             \\x4E09\x4E5D\x4E8C\x4E94\x516B\x516D\x56DB"
          }
      }
    )
  , ("zgh", LanguageData
      { language = "zgh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x2D30\x2D31\x2D33\x2D33\x2D6F\x2D37\x2D39\x2D3B\x2D3C\x2D3D\x2D3D\x2D6F\
             \\x2D40\x2D43\x2D44\x2D45\x2D47\x2D49\x2D4A\x2D4D\x2D4E\x2D4F\
             \\x2D53\x2D54\x2D55\x2D56\x2D59\x2D5A\x2D5B\x2D5C\x2D5F\x2D61\
             \\x2D62\x2D63\x2D65"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("zh", LanguageData
      { language = "zh"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x4E00\x4E01\x4E03\x4E07\x4E08\x4E09\x4E0A\x4E0B\x4E0C\x4E0D\
             \\x4E0E\x4E11\x4E13\x4E14\x4E16\x4E18\x4E19\x4E1A\x4E1C\x4E1D\
             \\x4E22\x4E24\x4E25\x4E27\x4E2A\x4E2D\x4E30\x4E32\x4E34\x4E38\
             \\x4E39\x4E3A\x4E3B\x4E3D\x4E3E\x4E43\x4E45\x4E48\x4E49\x4E4B\
             \\x4E4C\x4E4D\x4E4E\x4E4F\x4E50\x4E54\x4E56\x4E58\x4E59\x4E5D\
             \\x4E5F\x4E60\x4E61\x4E66\x4E70\x4E71\x4E7E\x4E86\x4E88\x4E89\
             \\x4E8B\x4E8C\x4E8E\x4E8F\x4E91\x4E92\x4E94\x4E95\x4E9A\x4E9B\
             \\x4EA1\x4EA4\x4EA5\x4EA6\x4EA7\x4EA8\x4EAB\x4EAC\x4EAE\x4EB2\
             \\x4EBA\x4EBF\x4EC0\x4EC1\x4EC5\x4EC7\x4ECA\x4ECB\x4ECD\x4ECE\
             \\x4ED4\x4ED6\x4ED8\x4ED9\x4EE3\x4EE4\x4EE5\x4EEA\x4EEC\x4EF0\
             \\x4EF2\x4EF6\x4EF7\x4EFB\x4EFD\x4EFF\x4F01\x4F0A\x4F0D\x4F0F\
             \\x4F10\x4F11\x4F17\x4F18\x4F19\x4F1A\x4F1F\x4F20\x4F24\x4F26\
             \\x4F2F\x4F30\x4F34\x4F38\x4F3C\x4F3D\x4F46\x4F4D\x4F4E\x4F4F\
             \\x4F50\x4F51\x4F53\x4F55\x4F59\x4F5B\x4F5C\x4F60\x4F64\x4F69\
             \\x4F73\x4F7F\x4F8B\x4F9B\x4F9D\x4FA0\x4FA6\x4FA7\x4FA8\x4FAC\
             \\x4FAF\x4FB5\x4FBF\x4FC3\x4FC4\x4FCA\x4FD7\x4FDD\x4FE1\x4FE9\
             \\x4FEE\x4FF1\x4FFE\x500D\x5012\x5019\x501A\x501F\x5026\x503C\
             \\x503E\x5047\x504C\x504F\x505A\x505C\x5065\x5076\x5077\x50A8\
             \\x50AC\x50B2\x50BB\x50CF\x50E7\x5112\x513F\x5141\x5143\x5144\
             \\x5145\x5146\x5148\x5149\x514B\x514D\x5151\x5154\x515A\x5165\
             \\x5168\x516B\x516C\x516D\x516E\x5170\x5171\x5173\x5174\x5175\
             \\x5176\x5177\x5178\x5179\x517B\x517C\x517D\x5185\x5188\x518C\
             \\x518D\x5192\x5199\x519B\x519C\x51A0\x51AC\x51B0\x51B2\x51B3\
             \\x51B5\x51B7\x51C6\x51CC\x51CF\x51DD\x51E0\x51E1\x51E4\x51ED\
             \\x51EF\x51F0\x51FA\x51FB\x51FD\x5200\x5206\x5207\x520A\x5211\
             \\x5212\x5217\x5218\x5219\x521A\x521B\x521D\x5224\x5229\x522B\
             \\x5230\x5236\x5237\x5238\x523A\x523B\x5242\x524D\x5251\x5267\
             \\x5269\x526A\x526F\x5272\x529B\x529D\x529E\x529F\x52A0\x52A1\
             \\x52A3\x52A8\x52A9\x52AA\x52AB\x52B1\x52B2\x52B3\x52BF\x52C7\
             \\x52C9\x52CB\x52D2\x52E4\x52FE\x52FF\x5305\x5306\x5308\x5316\
             \\x5317\x5319\x5339\x533A\x533B\x5341\x5343\x5347\x5348\x534A\
             \\x534E\x534F\x5352\x5353\x5355\x5356\x5357\x535A\x5360\x5361\
             \\x5362\x536B\x536F\x5370\x5371\x5373\x5374\x5377\x5382\x5384\
             \\x5385\x5386\x5389\x538B\x538C\x538D\x539A\x539F\x53BB\x53BF\
             \\x53C2\x53C8\x53C9\x53CA\x53CB\x53CC\x53CD\x53D1\x53D4\x53D6\
             \\x53D7\x53D8\x53D9\x53E3\x53E4\x53E5\x53E6\x53EA\x53EB\x53EC\
             \\x53ED\x53EF\x53F0\x53F2\x53F3\x53F6\x53F7\x53F8\x53F9\x5403\
             \\x5404\x5408\x5409\x540A\x540C\x540D\x540E\x5410\x5411\x5413\
             \\x5417\x541B\x541D\x541F\x5426\x5427\x542B\x542C\x542F\x5435\
             \\x5438\x5439\x543B\x543E\x5440\x5446\x5448\x544A\x5450\x5458\
             \\x545C\x5462\x5466\x5468\x5473\x5475\x547C\x547D\x548C\x5496\
             \\x54A6\x54A7\x54A8\x54AA\x54AC\x54AF\x54B1\x54C0\x54C1\x54C7\
             \\x54C8\x54C9\x54CD\x54CE\x54DF\x54E5\x54E6\x54E9\x54EA\x54ED\
             \\x54F2\x5509\x5510\x5524\x552C\x552E\x552F\x5531\x5537\x5546\
             \\x554A\x5561\x5565\x5566\x556A\x5580\x5582\x5584\x5587\x558A\
             \\x558F\x5594\x559C\x559D\x55B5\x55B7\x55BB\x55D2\x55E8\x55EF\
             \\x5609\x561B\x5634\x563B\x563F\x5668\x56DB\x56DE\x56E0\x56E2\
             \\x56ED\x56F0\x56F4\x56FA\x56FD\x56FE\x5706\x5708\x571F\x5723\
             \\x5728\x572D\x5730\x5733\x573A\x573E\x5740\x5747\x574E\x5750\
             \\x5751\x5757\x575A\x575B\x575C\x5761\x5764\x5766\x576A\x5782\
             \\x5783\x578B\x5792\x57C3\x57CB\x57CE\x57D4\x57DF\x57F9\x57FA\
             \\x5802\x5806\x5815\x5821\x582A\x5851\x5854\x585E\x586B\x5883\
             \\x589E\x58A8\x58C1\x58E4\x58EB\x58EC\x58EE\x58F0\x5904\x5907\
             \\x590D\x590F\x5915\x5916\x591A\x591C\x591F\x5925\x5927\x5929\
             \\x592A\x592B\x592E\x5931\x5934\x5937\x5938\x5939\x593A\x5947\
             \\x5948\x5949\x594B\x594F\x5951\x5954\x5956\x5957\x5965\x5973\
             \\x5974\x5976\x5979\x597D\x5982\x5987\x5988\x5996\x5999\x59A5\
             \\x59A8\x59AE\x59B9\x59BB\x59C6\x59CA\x59CB\x59D0\x59D1\x59D3\
             \\x59D4\x59FF\x5A01\x5A03\x5A04\x5A18\x5A1C\x5A1F\x5A31\x5A46\
             \\x5A5A\x5A92\x5AC1\x5ACC\x5AE9\x5B50\x5B54\x5B55\x5B57\x5B58\
             \\x5B59\x5B5C\x5B5D\x5B5F\x5B63\x5B64\x5B66\x5B69\x5B81\x5B83\
             \\x5B87\x5B88\x5B89\x5B8B\x5B8C\x5B8F\x5B97\x5B98\x5B99\x5B9A\
             \\x5B9B\x5B9C\x5B9D\x5B9E\x5BA1\x5BA2\x5BA3\x5BA4\x5BAA\x5BB3\
             \\x5BB4\x5BB6\x5BB9\x5BBD\x5BBE\x5BBF\x5BC2\x5BC4\x5BC5\x5BC6\
             \\x5BC7\x5BCC\x5BD2\x5BDD\x5BDE\x5BDF\x5BE1\x5BE8\x5BF8\x5BF9\
             \\x5BFB\x5BFC\x5BFF\x5C01\x5C04\x5C06\x5C0A\x5C0F\x5C11\x5C14\
             \\x5C16\x5C18\x5C1A\x5C1D\x5C24\x5C31\x5C3A\x5C3C\x5C3D\x5C3E\
             \\x5C40\x5C41\x5C42\x5C45\x5C4B\x5C4F\x5C55\x5C5E\x5C60\x5C71\
             \\x5C81\x5C82\x5C97\x5C98\x5C9A\x5C9B\x5CB3\x5CB8\x5CE1\x5CF0\
             \\x5D07\x5D29\x5D34\x5DDD\x5DDE\x5DE1\x5DE5\x5DE6\x5DE7\x5DE8\
             \\x5DEB\x5DEE\x5DF1\x5DF2\x5DF3\x5DF4\x5DF7\x5E01\x5E02\x5E03\
             \\x5E05\x5E08\x5E0C\x5E10\x5E15\x5E16\x5E1D\x5E26\x5E2D\x5E2E\
             \\x5E38\x5E3D\x5E45\x5E55\x5E72\x5E73\x5E74\x5E76\x5E78\x5E7B\
             \\x5E7C\x5E7D\x5E7F\x5E86\x5E8A\x5E8F\x5E93\x5E94\x5E95\x5E97\
             \\x5E99\x5E9A\x5E9C\x5E9E\x5E9F\x5EA6\x5EA7\x5EAD\x5EB7\x5EB8\
             \\x5EC9\x5ED6\x5EF6\x5EF7\x5EFA\x5F00\x5F02\x5F03\x5F04\x5F0A\
             \\x5F0F\x5F15\x5F17\x5F18\x5F1F\x5F20\x5F25\x5F26\x5F2F\x5F31\
             \\x5F39\x5F3A\x5F52\x5F53\x5F55\x5F5D\x5F62\x5F69\x5F6C\x5F6D\
             \\x5F70\x5F71\x5F77\x5F79\x5F7B\x5F7C\x5F80\x5F81\x5F84\x5F85\
             \\x5F88\x5F8B\x5F8C\x5F90\x5F92\x5F97\x5FAA\x5FAE\x5FB5\x5FB7\
             \\x5FC3\x5FC5\x5FC6\x5FCC\x5FCD\x5FD7\x5FD8\x5FD9\x5FE0\x5FE7\
             \\x5FEB\x5FF5\x5FFD\x6000\x6001\x600E\x6012\x6015\x6016\x601D\
             \\x6021\x6025\x6027\x6028\x602A\x603B\x604B\x6050\x6062\x6068\
             \\x6069\x606D\x606F\x6070\x6076\x607C\x6084\x6089\x6094\x609F\
             \\x60A0\x60A3\x60A8\x60B2\x60C5\x60D1\x60DC\x60E0\x60E7\x60E8\
             \\x60EF\x60F3\x60F9\x6101\x6108\x6109\x610F\x611A\x611F\x6127\
             \\x6148\x614E\x6155\x6162\x6167\x6170\x61BE\x61C2\x61D2\x6208\
             \\x620A\x620C\x620F\x6210\x6211\x6212\x6216\x6218\x622A\x6234\
             \\x6237\x623F\x6240\x6241\x6247\x624B\x624D\x624E\x6251\x6253\
             \\x6258\x6263\x6267\x6269\x626B\x626C\x626D\x626E\x626F\x6279\
             \\x627E\x627F\x6280\x6284\x628A\x6291\x6293\x6295\x6297\x6298\
             \\x62A2\x62A4\x62A5\x62AB\x62AC\x62B1\x62B5\x62B9\x62BD\x62C5\
             \\x62C6\x62C9\x62CD\x62D2\x62D4\x62D6\x62D8\x62DB\x62DC\x62DF\
             \\x62E5\x62E6\x62E8\x62E9\x62EC\x62F3\x62F7\x62FC\x62FE\x62FF\
             \\x6301\x6307\x6309\x6311\x6316\x631D\x6321\x6324\x6325\x632A\
             \\x632F\x633A\x6349\x6350\x6355\x635F\x6361\x6362\x636E\x6377\
             \\x6388\x6389\x638C\x6392\x63A2\x63A5\x63A7\x63A8\x63A9\x63AA\
             \\x63B8\x63CF\x63D0\x63D2\x63E1\x63F4\x641C\x641E\x642C\x642D\
             \\x6444\x6446\x644A\x6454\x6458\x6469\x6478\x6492\x649E\x64AD\
             \\x64CD\x64CE\x64E6\x652F\x6536\x6539\x653B\x653E\x653F\x6545\
             \\x6548\x654C\x654F\x6551\x6559\x655D\x6562\x6563\x6566\x656C\
             \\x6570\x6572\x6574\x6587\x658B\x6590\x6597\x6599\x659C\x65A5\
             \\x65AD\x65AF\x65B0\x65B9\x65BC\x65BD\x65C1\x65C5\x65CB\x65CF\
             \\x65D7\x65E0\x65E2\x65E5\x65E6\x65E7\x65E8\x65E9\x65ED\x65F6\
             \\x65FA\x6602\x6606\x660C\x660E\x660F\x6613\x661F\x6620\x6625\
             \\x6628\x662D\x662F\x663E\x6643\x664B\x6652\x6653\x665A\x6668\
             \\x666E\x666F\x6674\x6676\x667A\x6682\x6691\x6696\x6697\x66AE\
             \\x66B4\x66F0\x66F2\x66F4\x66F9\x66FC\x66FE\x66FF\x6700\x6708\
             \\x6709\x670B\x670D\x6717\x671B\x671D\x671F\x6728\x672A\x672B\
             \\x672C\x672D\x672F\x6731\x6735\x673A\x6740\x6742\x6743\x6749\
             \\x674E\x6750\x6751\x675C\x675F\x6761\x6765\x6768\x676F\x6770\
             \\x677E\x677F\x6781\x6784\x6790\x6797\x679C\x679D\x67A2\x67AA\
             \\x67AB\x67B6\x67CF\x67D0\x67D3\x67D4\x67E5\x67EC\x67EF\x67F3\
             \\x67F4\x6807\x680B\x680F\x6811\x6821\x6837\x6838\x6839\x683C\
             \\x6843\x6846\x6848\x684C\x6851\x6863\x6865\x6881\x6885\x68A6\
             \\x68AF\x68B0\x68B5\x68C0\x68C9\x68CB\x68D2\x68DA\x68EE\x6905\
             \\x690D\x6930\x695A\x697C\x6982\x699C\x6A21\x6A31\x6A80\x6B20\
             \\x6B21\x6B22\x6B23\x6B27\x6B32\x6B3A\x6B3E\x6B49\x6B4C\x6B62\
             \\x6B63\x6B64\x6B65\x6B66\x6B6A\x6B7B\x6B8A\x6B8B\x6BB5\x6BC5\
             \\x6BCD\x6BCF\x6BD2\x6BD4\x6BD5\x6BDB\x6BEB\x6C0F\x6C11\x6C14\
             \\x6C1B\x6C34\x6C38\x6C42\x6C47\x6C49\x6C57\x6C5D\x6C5F\x6C60\
             \\x6C61\x6C64\x6C6A\x6C76\x6C7D\x6C83\x6C88\x6C89\x6C99\x6C9F\
             \\x6CA1\x6CA7\x6CB3\x6CB9\x6CBB\x6CBF\x6CC9\x6CCA\x6CD5\x6CDB\
             \\x6CE1\x6CE2\x6CE3\x6CE5\x6CE8\x6CF0\x6CF3\x6CFD\x6D0B\x6D17\
             \\x6D1B\x6D1E\x6D25\x6D2A\x6D32\x6D3B\x6D3D\x6D3E\x6D41\x6D45\
             \\x6D4B\x6D4E\x6D4F\x6D51\x6D53\x6D59\x6D66\x6D69\x6D6A\x6D6E\
             \\x6D74\x6D77\x6D85\x6D88\x6D89\x6D9B\x6DA8\x6DAF\x6DB2\x6DB5\
             \\x6DCB\x6DD1\x6DD8\x6DE1\x6DF1\x6DF7\x6DFB\x6E05\x6E10\x6E21\
             \\x6E23\x6E29\x6E2F\x6E34\x6E38\x6E56\x6E7E\x6E90\x6E9C\x6EAA\
             \\x6ECB\x6ED1\x6EE1\x6EE5\x6EE8\x6EF4\x6F02\x6F0F\x6F14\x6F20\
             \\x6F2B\x6F58\x6F5C\x6F6E\x6F8E\x6FB3\x6FC0\x704C\x706B\x706D\
             \\x706F\x7070\x7075\x707F\x7089\x708E\x70AE\x70B8\x70B9\x70C2\
             \\x70C8\x70E4\x70E6\x70E7\x70ED\x7126\x7136\x714C\x715E\x7167\
             \\x716E\x718A\x719F\x71C3\x71D5\x7206\x722A\x722C\x7231\x7235\
             \\x7236\x7237\x7238\x723D\x7247\x7248\x724C\x7259\x725B\x7261\
             \\x7262\x7267\x7269\x7272\x7275\x7279\x727A\x72AF\x72B6\x72B9\
             \\x72C2\x72D0\x72D7\x72E0\x72EC\x72EE\x72F1\x72FC\x731B\x731C\
             \\x732A\x732E\x7334\x7384\x7387\x7389\x738B\x739B\x73A9\x73AB\
             \\x73AF\x73B0\x73B2\x73BB\x73C0\x73CA\x73CD\x73E0\x73ED\x7403\
             \\x7406\x740A\x742A\x7433\x7434\x743C\x7459\x745C\x745E\x745F\
             \\x7470\x7476\x7483\x74DC\x74E6\x74F6\x7518\x751A\x751C\x751F\
             \\x7528\x7530\x7531\x7532\x7533\x7535\x7537\x7538\x753B\x7545\
             \\x754C\x7559\x7565\x756A\x7586\x758F\x7591\x7597\x75AF\x75B2\
             \\x75BC\x75BE\x75C5\x75D5\x75DB\x75F4\x7678\x767B\x767D\x767E\
             \\x7684\x7686\x7687\x76AE\x76C8\x76CA\x76D1\x76D2\x76D6\x76D8\
             \\x76DB\x76DF\x76EE\x76F4\x76F8\x76FC\x76FE\x7701\x7709\x770B\
             \\x771F\x7720\x773C\x7740\x775B\x7761\x7763\x77A7\x77DB\x77E3\
             \\x77E5\x77ED\x77F3\x77F6\x7801\x7802\x780D\x7814\x7834\x7840\
             \\x7855\x786C\x786E\x788D\x788E\x7897\x789F\x78A7\x78B0\x78C1\
             \\x78C5\x78E8\x793A\x793C\x793E\x7956\x795A\x795D\x795E\x7965\
             \\x7968\x796F\x7978\x7981\x7985\x798F\x79BB\x79C0\x79C1\x79CB\
             \\x79CD\x79D1\x79D2\x79D8\x79DF\x79E4\x79E6\x79E9\x79EF\x79F0\
             \\x79FB\x7A00\x7A0B\x7A0D\x7A0E\x7A23\x7A33\x7A3F\x7A46\x7A76\
             \\x7A77\x7A79\x7A7A\x7A7F\x7A81\x7A97\x7A9D\x7ACB\x7AD9\x7ADE\
             \\x7ADF\x7AE0\x7AE5\x7AEF\x7AF9\x7B11\x7B14\x7B1B\x7B26\x7B28\
             \\x7B2C\x7B49\x7B4B\x7B51\x7B54\x7B56\x7B79\x7B7E\x7B80\x7B97\
             \\x7BA1\x7BAD\x7BB1\x7BC7\x7BEE\x7C3F\x7C4D\x7C73\x7C7B\x7C89\
             \\x7C92\x7C97\x7CA4\x7CB9\x7CBE\x7CCA\x7CD5\x7CD6\x7CDF\x7CFB\
             \\x7D20\x7D22\x7D27\x7D2B\x7D2F\x7E41\x7EA2\x7EA6\x7EA7\x7EAA\
             \\x7EAF\x7EB2\x7EB3\x7EB5\x7EB7\x7EB8\x7EBD\x7EBF\x7EC3\x7EC4\
             \\x7EC6\x7EC7\x7EC8\x7ECD\x7ECF\x7ED3\x7ED5\x7ED8\x7ED9\x7EDC\
             \\x7EDD\x7EDF\x7EE7\x7EE9\x7EEA\x7EED\x7EF4\x7EF5\x7EFC\x7EFF\
             \\x7F05\x7F13\x7F16\x7F18\x7F20\x7F29\x7F34\x7F36\x7F38\x7F3A\
             \\x7F50\x7F51\x7F55\x7F57\x7F5A\x7F62\x7F6A\x7F6E\x7F72\x7F8A\
             \\x7F8E\x7F9E\x7FA4\x7FAF\x7FBD\x7FC1\x7FC5\x7FD4\x7FD8\x7FE0\
             \\x7FF0\x7FFB\x7FFC\x8000\x8001\x8003\x8005\x800C\x800D\x8010\
             \\x8017\x8033\x8036\x804A\x804C\x8054\x8058\x805A\x806A\x8089\
             \\x8096\x809A\x80A1\x80A4\x80A5\x80A9\x80AF\x80B2\x80C1\x80C6\
             \\x80CC\x80CE\x80D6\x80DC\x80DE\x80E1\x80F6\x80F8\x80FD\x8106\
             \\x8111\x8131\x8138\x814A\x8150\x8153\x8170\x8179\x817E\x817F\
             \\x81C2\x81E3\x81EA\x81ED\x81F3\x81F4\x820C\x820D\x8212\x821E\
             \\x821F\x822A\x822C\x8230\x8239\x826F\x8272\x827A\x827E\x8282\
             \\x8292\x829D\x82A6\x82AC\x82AD\x82B1\x82B3\x82CD\x82CF\x82D7\
             \\x82E5\x82E6\x82F1\x8302\x8303\x8328\x832B\x8336\x8349\x8350\
             \\x8352\x8363\x836F\x8377\x8389\x838E\x83AA\x83AB\x83B1\x83B2\
             \\x83B7\x83DC\x83E9\x83F2\x8404\x840D\x8424\x8425\x8427\x8428\
             \\x843D\x8457\x845B\x8461\x8482\x848B\x8499\x84C9\x84DD\x84EC\
             \\x8511\x8521\x8584\x85AA\x85C9\x85CF\x85E4\x864E\x8651\x866B\
             \\x8679\x867D\x867E\x8681\x86C7\x86CB\x86D9\x86EE\x8702\x871C\
             \\x8776\x878D\x87F9\x8822\x8840\x884C\x8857\x8861\x8863\x8865\
             \\x8868\x888B\x88AB\x88AD\x88C1\x88C2\x88C5\x88D5\x88E4\x897F\
             \\x8981\x8986\x89C1\x89C2\x89C4\x89C6\x89C8\x89C9\x89D2\x89E3\
             \\x8A00\x8A89\x8A93\x8B66\x8BA1\x8BA2\x8BA4\x8BA8\x8BA9\x8BAD\
             \\x8BAE\x8BAF\x8BB0\x8BB2\x8BB7\x8BB8\x8BBA\x8BBE\x8BBF\x8BC1\
             \\x8BC4\x8BC6\x8BC9\x8BCD\x8BD1\x8BD5\x8BD7\x8BDA\x8BDD\x8BDE\
             \\x8BE2\x8BE5\x8BE6\x8BED\x8BEF\x8BF4\x8BF7\x8BF8\x8BFA\x8BFB\
             \\x8BFE\x8C01\x8C03\x8C05\x8C08\x8C0A\x8C0B\x8C13\x8C1C\x8C22\
             \\x8C28\x8C31\x8C37\x8C46\x8C61\x8C6A\x8C8C\x8D1D\x8D1E\x8D1F\
             \\x8D21\x8D22\x8D23\x8D24\x8D25\x8D27\x8D28\x8D29\x8D2A\x8D2D\
             \\x8D2F\x8D31\x8D34\x8D35\x8D38\x8D39\x8D3A\x8D3C\x8D3E\x8D44\
             \\x8D4B\x8D4C\x8D4F\x8D50\x8D54\x8D56\x8D5A\x8D5B\x8D5E\x8D60\
             \\x8D62\x8D64\x8D6B\x8D70\x8D75\x8D77\x8D81\x8D85\x8D8A\x8D8B\
             \\x8DA3\x8DB3\x8DC3\x8DCC\x8DD1\x8DDD\x8DDF\x8DEF\x8DF3\x8E0F\
             \\x8E22\x8E29\x8EAB\x8EB2\x8F66\x8F68\x8F69\x8F6C\x8F6E\x8F6F\
             \\x8F70\x8F7B\x8F7D\x8F83\x8F85\x8F86\x8F88\x8F89\x8F91\x8F93\
             \\x8F9B\x8F9E\x8FA8\x8FA9\x8FB0\x8FB1\x8FB9\x8FBE\x8FC1\x8FC5\
             \\x8FC7\x8FC8\x8FCE\x8FD0\x8FD1\x8FD4\x8FD8\x8FD9\x8FDB\x8FDC\
             \\x8FDD\x8FDE\x8FDF\x8FE6\x8FEA\x8FEB\x8FF0\x8FF7\x8FFD\x9000\
             \\x9001\x9002\x9003\x9006\x9009\x900A\x900F\x9010\x9012\x9014\
             \\x901A\x901B\x901D\x901F\x9020\x9022\x9038\x903B\x903C\x9047\
             \\x904D\x9053\x9057\x906D\x906E\x9075\x907F\x9080\x9093\x90A3\
             \\x90A6\x90AA\x90AE\x90B1\x90BB\x90CE\x90D1\x90E8\x90ED\x90FD\
             \\x9102\x9149\x914B\x914D\x9152\x9177\x9178\x9189\x9192\x91C7\
             \\x91CA\x91CC\x91CD\x91CE\x91CF\x91D1\x9488\x9493\x949F\x94A2\
             \\x94A6\x94B1\x94BB\x94C1\x94C3\x94DC\x94E2\x94ED\x94F6\x94FA\
             \\x94FE\x9500\x9501\x9505\x950B\x9519\x9521\x9526\x952E\x953A\
             \\x9547\x955C\x956D\x957F\x95E8\x95EA\x95ED\x95EE\x95F0\x95F2\
             \\x95F4\x95F7\x95F9\x95FB\x9601\x9605\x9610\x9614\x961F\x962E\
             \\x9632\x9633\x9634\x9635\x9636\x963B\x963F\x9640\x9644\x9645\
             \\x9646\x9648\x964D\x9650\x9662\x9664\x9669\x966A\x9675\x9676\
             \\x9677\x9686\x968F\x9690\x9694\x969C\x96BE\x96C4\x96C5\x96C6\
             \\x96C9\x96E8\x96EA\x96EF\x96F3\x96F6\x96F7\x96FE\x9700\x9707\
             \\x970D\x9716\x9732\x9738\x9739\x9752\x9756\x9759\x975E\x9760\
             \\x9762\x9769\x977C\x978B\x9791\x97E6\x97E9\x97F3\x9875\x9876\
             \\x9879\x987A\x987B\x987D\x987E\x987F\x9884\x9886\x9887\x9891\
             \\x9897\x9898\x989D\x98CE\x98D8\x98D9\x98DE\x98DF\x9910\x996D\
             \\x996E\x9970\x9971\x997C\x9986\x9996\x9999\x99A8\x9A6C\x9A71\
             \\x9A76\x9A7B\x9A7E\x9A8C\x9A91\x9A97\x9A9A\x9AA4\x9AA8\x9AD8\
             \\x9B3C\x9B42\x9B45\x9B54\x9C7C\x9C81\x9C9C\x9E1F\x9E21\x9E23\
             \\x9E2D\x9E3F\x9E45\x9E64\x9E70\x9E7F\x9EA6\x9EBB\x9EC4\x9ECE\
             \\x9ED1\x9ED8\x9F13\x9F20\x9F3B\x9F50\x9F7F\x9F84\x9F99\x9F9F"
          , _exAuxiliary = Just $ Set.fromList
             "\x4E52\x4E53\x4E73\x4EC2\x4ED3\x4F1E\x4F2A\x4FA3\x5088\x50A3\
             \\x50F3\x50F5\x51A5\x51BB\x51C0\x51C9\x51F8\x5228\x522E\x5243\
             \\x527D\x5288\x52FA\x5315\x5320\x532E\x5351\x535C\x535E\x5366\
             \\x5395\x5398\x53A6\x53A8\x53EE\x5415\x5434\x5455\x5463\x5492\
             \\x54E8\x54FA\x5507\x5564\x556E\x55B1\x55C5\x5618\x561F\x5658\
             \\x565C\x5662\x568F\x574F\x575D\x57AB\x5824\x5835\x5885\x5893\
             \\x5899\x589F\x58F3\x58F6\x594E\x5978\x5986\x59DC\x5A25\x5A74\
             \\x5A9A\x5B6A\x5B85\x5BA0\x5BAB\x5BFA\x5C2C\x5C34\x5C38\x5C48\
             \\x5C4E\x5C51\x5C61\x5C65\x5C7F\x5CA9\x5DFD\x5DFE\x5E06\x5E1A\
             \\x5E1C\x5E27\x5E87\x5EB5\x5F13\x5F57\x5FA1\x5FBD\x601C\x6052\
             \\x6064\x6073\x60AC\x60AF\x60CA\x60EB\x6124\x621F\x6254\x6270\
             \\x6273\x6296\x629B\x62C4\x62C7\x62D0\x6302\x630E\x6342\x634F\
             \\x6398\x63A0\x63ED\x640F\x643A\x6447\x6485\x6495\x64A4\x6500\
             \\x655E\x6591\x65A7\x6619\x6655\x6714\x6746\x6756\x6760\x67AF\
             \\x67C4\x67D1\x67DC\x67E0\x67E9\x67F1\x67FF\x6813\x6817\x683D\
             \\x6854\x6876\x68A8\x68CD\x68D5\x68FA\x6912\x6954\x6960\x6984\
             \\x6988\x69CC\x69DF\x69FF\x6A2A\x6A44\x6A47\x6A58\x6A59\x6A61\
             \\x6AAC\x6C41\x6C90\x6CAB\x6CEA\x6CF5\x6D01\x6D12\x6D46\x6D63\
             \\x6D82\x6D8C\x6D8E\x6D95\x6DA9\x6DC7\x6E17\x6E58\x6ED5\x6EDA\
             \\x6EE9\x6FA1\x707E\x70BD\x70D8\x70D9\x70DB\x70DF\x70F9\x710A\
             \\x7119\x7130\x714E\x718F\x71E5\x7280\x7284\x72AC\x72B8\x72E1\
             \\x72F8\x730E\x7315\x7329\x732B\x732C\x733E\x733F\x736D\x737E\
             \\x745A\x74E2\x74EE\x752B\x755C\x759F\x7682\x76B1\x76BF\x76C6\
             \\x76D0\x76D4\x76D7\x76E5\x76F2\x7728\x7729\x772F\x777F\x778C\
             \\x7792\x77AA\x77FF\x7816\x7838\x7891\x7948\x796D\x7977\x7984\
             \\x79BD\x79C3\x7A3B\x7A3D\x7A83\x7A84\x7AA5\x7AD6\x7AFF\x7B06\
             \\x7B3A\x7B3C\x7B50\x7B52\x7B5D\x7B77\x7BB8\x7BD3\x7BF1\x7BF7\
             \\x7C91\x7C9F\x7CA5\x7CAE\x7CBD\x7D6E\x7EA0\x7EAB\x7EAC\x7EB1\
             \\x7EB9\x7ED2\x7EE3\x7EF3\x7EF7\x7F04\x7F06\x7F0E\x7F1D\x7F69\
             \\x7FB9\x7FF1\x8038\x804B\x808C\x80A2\x80BA\x810F\x811A\x814B\
             \\x8155\x816E\x8180\x818F\x8214\x8235\x8247\x826E\x8273\x8299\
             \\x82BD\x82DC\x82DE\x82E3\x82F9\x8304\x8393\x83B4\x83C7\x83CC\
             \\x83E0\x83F1\x840E\x841D\x8469\x846B\x846C\x8471\x8475\x849C\
             \\x84B2\x84B8\x84C4\x84FF\x852C\x853D\x8549\x857E\x85AF\x8611\
             \\x8682\x868A\x8693\x869D\x86AF\x86C6\x86CE\x86D0\x86DB\x86F0\
             \\x86FE\x8717\x8718\x8721\x8725\x8734\x8747\x874E\x8759\x8760\
             \\x8774\x8782\x8783\x87BA\x87C0\x87CB\x87D1\x8815\x886B\x886C\
             \\x889C\x88D9\x88F9\x8902\x8BB6\x8BBD\x8BCA\x8BF1\x8C0D\x8C0E\
             \\x8C1A\x8C2C\x8C5A\x8C79\x8D26\x8D37\x8D63\x8DBE\x8DC6\x8DDB\
             \\x8DE4\x8DE8\x8DEA\x8E2A\x8E44\x8E48\x8E66\x8E6C\x8EAC\x8EBA\
             \\x8F74\x8F7F\x8F90\x8FA3\x8FF9\x90C1\x9119\x9162\x9165\x916A\
             \\x9171\x91BA\x9489\x949E\x94A5\x94A9\x94AE\x94AF\x94B3\x94C2\
             \\x94C5\x94DB\x94F0\x9504\x9511\x951A\x9524\x952F\x9550\x9551\
             \\x9556\x95FA\x95FD\x9631\x96A7\x96C0\x96CC\x96D5\x971C\x973E\
             \\x9774\x9776\x97A0\x9888\x989C\x98A0\x98A4\x98D3\x996A\x9975\
             \\x997A\x998D\x998F\x9A70\x9A7C\x9A82\x9A84\x9A86\x9AB0\x9AB7\
             \\x9ABC\x9AC5\x9AE6\x9B08\x9B41\x9C7F\x9CA4\x9CA8\x9CB8\x9CC4\
             \\x9E22\x9E35\x9E3D\x9E49\x9E66\x9ECF\x9EDB\x9F2C\x9F87"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\x5F\
             \\x7B\x7D\xA7\xB7\x2010\x2011\x2013\x2014\x2015\x2016\
             \\x2018\x2019\x201C\x201D\x2025\x2026\x2030\x2032\x2033\x2035\
             \\x203B\x3001\x3002\x3003\x3008\x3009\x300A\x300B\x300C\x300D\
             \\x300E\x300F\x3010\x3011\x3014\x3015\x3016\x3017\x301D\x301E\
             \\xFE30\xFE31\xFE33\xFE34\xFE35\xFE36\xFE37\xFE38\xFE39\xFE3A\xFE3B\
             \\xFE3C\xFE3D\xFE3E\xFE3F\xFE40\xFE41\xFE42\xFE43\xFE44\xFE49\xFE4A\xFE4B\xFE4C\
             \\xFE4D\xFE4E\xFE4F\xFE50\xFE51\xFE52\xFE54\xFE55\xFE56\xFE57\xFE59\xFE5A\
             \\xFE5B\xFE5C\xFE5D\xFE5E\xFE5F\xFE60\xFE61\xFE63\xFE68\xFE6A\
             \\xFE6B\xFF01\xFF02\xFF03\xFF05\xFF06\xFF07\xFF08\xFF09\xFF0A\
             \\xFF0C\xFF0D\xFF0E\xFF0F\xFF1A\xFF1B\xFF1F\xFF20\xFF3B\xFF3C\
             \\xFF3D\xFF3F\xFF5B\xFF5D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030\x3007\x4E00\x4E03\
             \\x4E09\x4E5D\x4E8C\x4E94\x516B\x516D\x56DB"
          }
      }
    )
  , ("zu", LanguageData
      { language = "zu"
      , script = Nothing
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x62\x68\x63\x63\x68\x64\x64\x6C\x64\x79\x65\x66\
             \\x67\x67\x63\x67\x71\x67\x78\x68\x68\x68\x68\x6C\x69\x6A\x6B\
             \\x6B\x68\x6B\x6C\x6B\x70\x6C\x6D\x6E\x6E\x63\x6E\x67\x63\x6E\x67\x71\x6E\x67\x78\
             \\x6E\x68\x6C\x6E\x6B\x6E\x6B\x63\x6E\x6B\x71\x6E\x6B\x78\x6E\x71\x6E\x74\x73\x68\x6E\x78\x6E\x79\x6F\
             \\x70\x70\x68\x71\x71\x68\x72\x72\x68\x73\x73\x68\x74\x74\x68\
             \\x74\x6C\x74\x73\x74\x73\x68\x75\x76\x77\x78\x78\x68\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xE9\
             \\xEA\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\
             \\xF6\xF8\xF9\xFA\xFB\xFC\xFF\x101\x103\x113\
             \\x115\x12B\x12D\x14D\x14F\x153\x16B\x16D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x28\x29\x2C\x2D\x2E\x3A\x3B\x3F\x5B\
             \\x5D\x7B\x7D\x2011"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("af-NA", LanguageData
      { language = "af"
      , script = Nothing
      , territory = Just "NA"
      , exemplars = Nothing
      }
    )
  , ("af-ZA", LanguageData
      { language = "af"
      , script = Nothing
      , territory = Just "ZA"
      , exemplars = Nothing
      }
    )
  , ("agq-CM", LanguageData
      { language = "agq"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("ak-GH", LanguageData
      { language = "ak"
      , script = Nothing
      , territory = Just "GH"
      , exemplars = Nothing
      }
    )
  , ("am-ET", LanguageData
      { language = "am"
      , script = Nothing
      , territory = Just "ET"
      , exemplars = Nothing
      }
    )
  , ("ar-001", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "001"
      , exemplars = Nothing
      }
    )
  , ("ar-AE", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "AE"
      , exemplars = Nothing
      }
    )
  , ("ar-BH", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "BH"
      , exemplars = Nothing
      }
    )
  , ("ar-DJ", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "DJ"
      , exemplars = Nothing
      }
    )
  , ("ar-DZ", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "DZ"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-EG", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "EG"
      , exemplars = Nothing
      }
    )
  , ("ar-EH", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "EH"
      , exemplars = Nothing
      }
    )
  , ("ar-ER", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "ER"
      , exemplars = Nothing
      }
    )
  , ("ar-IL", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "IL"
      , exemplars = Nothing
      }
    )
  , ("ar-IQ", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "IQ"
      , exemplars = Nothing
      }
    )
  , ("ar-JO", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "JO"
      , exemplars = Nothing
      }
    )
  , ("ar-KM", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "KM"
      , exemplars = Nothing
      }
    )
  , ("ar-KW", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "KW"
      , exemplars = Nothing
      }
    )
  , ("ar-LB", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "LB"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-LY", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "LY"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-MA", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "MA"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Just $ Set.fromList
             "\x66F\x67E\x686\x698\x69C\x6A2\x6A4\x6A5\x6A7\x6A8\
             \\x6A9\x6AD\x6AF\x6CC\x763\x200C\x200D\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-MR", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "MR"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-OM", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "OM"
      , exemplars = Nothing
      }
    )
  , ("ar-PS", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "PS"
      , exemplars = Nothing
      }
    )
  , ("ar-QA", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "QA"
      , exemplars = Nothing
      }
    )
  , ("ar-SA", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "SA"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x66A\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-SD", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "SD"
      , exemplars = Nothing
      }
    )
  , ("ar-SO", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "SO"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x66A\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-SS", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "SS"
      , exemplars = Nothing
      }
    )
  , ("ar-SY", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "SY"
      , exemplars = Nothing
      }
    )
  , ("ar-TD", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "TD"
      , exemplars = Nothing
      }
    )
  , ("ar-TN", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "TN"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("ar-YE", LanguageData
      { language = "ar"
      , script = Nothing
      , territory = Just "YE"
      , exemplars = Nothing
      }
    )
  , ("as-IN", LanguageData
      { language = "as"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("asa-TZ", LanguageData
      { language = "asa"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("ast-ES", LanguageData
      { language = "ast"
      , script = Nothing
      , territory = Just "ES"
      , exemplars = Nothing
      }
    )
  , ("az-Cyrl", LanguageData
      { language = "az"
      , script = Just "Cyrl"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x447\x448\x44B\x458\x493\x49D\x4AF\x4B9\
             \\x4BB\x4D9\x4E9"
          , _exAuxiliary = Just $ Set.fromList
             "\x446\x449\x44A\x44C\x44D\x44E\x44F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("az-Latn", LanguageData
      { language = "az"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("bas-CM", LanguageData
      { language = "bas"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("be-BY", LanguageData
      { language = "be"
      , script = Nothing
      , territory = Just "BY"
      , exemplars = Nothing
      }
    )
  , ("bem-ZM", LanguageData
      { language = "bem"
      , script = Nothing
      , territory = Just "ZM"
      , exemplars = Nothing
      }
    )
  , ("bez-TZ", LanguageData
      { language = "bez"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("bg-BG", LanguageData
      { language = "bg"
      , script = Nothing
      , territory = Just "BG"
      , exemplars = Nothing
      }
    )
  , ("bm-ML", LanguageData
      { language = "bm"
      , script = Nothing
      , territory = Just "ML"
      , exemplars = Nothing
      }
    )
  , ("bn-BD", LanguageData
      { language = "bn"
      , script = Nothing
      , territory = Just "BD"
      , exemplars = Nothing
      }
    )
  , ("bn-IN", LanguageData
      { language = "bn"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("bo-CN", LanguageData
      { language = "bo"
      , script = Nothing
      , territory = Just "CN"
      , exemplars = Nothing
      }
    )
  , ("bo-IN", LanguageData
      { language = "bo"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("br-FR", LanguageData
      { language = "br"
      , script = Nothing
      , territory = Just "FR"
      , exemplars = Nothing
      }
    )
  , ("brx-IN", LanguageData
      { language = "brx"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("bs-Cyrl", LanguageData
      { language = "bs"
      , script = Just "Cyrl"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x43A\
             \\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\x444\
             \\x445\x446\x447\x448\x452\x458\x459\x45A\x45B\x45F"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("bs-Latn", LanguageData
      { language = "bs"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("ca-AD", LanguageData
      { language = "ca"
      , script = Nothing
      , territory = Just "AD"
      , exemplars = Nothing
      }
    )
  , ("ca-ES", LanguageData
      { language = "ca"
      , script = Nothing
      , territory = Just "ES"
      , exemplars = Nothing
      }
    )
  , ("ca-FR", LanguageData
      { language = "ca"
      , script = Nothing
      , territory = Just "FR"
      , exemplars = Nothing
      }
    )
  , ("ca-IT", LanguageData
      { language = "ca"
      , script = Nothing
      , territory = Just "IT"
      , exemplars = Nothing
      }
    )
  , ("ccp-BD", LanguageData
      { language = "ccp"
      , script = Nothing
      , territory = Just "BD"
      , exemplars = Nothing
      }
    )
  , ("ccp-IN", LanguageData
      { language = "ccp"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ce-RU", LanguageData
      { language = "ce"
      , script = Nothing
      , territory = Just "RU"
      , exemplars = Nothing
      }
    )
  , ("ceb-PH", LanguageData
      { language = "ceb"
      , script = Nothing
      , territory = Just "PH"
      , exemplars = Nothing
      }
    )
  , ("cgg-UG", LanguageData
      { language = "cgg"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("chr-US", LanguageData
      { language = "chr"
      , script = Nothing
      , territory = Just "US"
      , exemplars = Nothing
      }
    )
  , ("ckb-IQ", LanguageData
      { language = "ckb"
      , script = Nothing
      , territory = Just "IQ"
      , exemplars = Nothing
      }
    )
  , ("ckb-IR", LanguageData
      { language = "ckb"
      , script = Nothing
      , territory = Just "IR"
      , exemplars = Nothing
      }
    )
  , ("cs-CZ", LanguageData
      { language = "cs"
      , script = Nothing
      , territory = Just "CZ"
      , exemplars = Nothing
      }
    )
  , ("cy-GB", LanguageData
      { language = "cy"
      , script = Nothing
      , territory = Just "GB"
      , exemplars = Nothing
      }
    )
  , ("da-DK", LanguageData
      { language = "da"
      , script = Nothing
      , territory = Just "DK"
      , exemplars = Nothing
      }
    )
  , ("da-GL", LanguageData
      { language = "da"
      , script = Nothing
      , territory = Just "GL"
      , exemplars = Nothing
      }
    )
  , ("dav-KE", LanguageData
      { language = "dav"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("de-AT", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "AT"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("de-BE", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "BE"
      , exemplars = Nothing
      }
    )
  , ("de-CH", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE4\xF6\xFC"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2011\x2019\x2030"
          }
      }
    )
  , ("de-DE", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "DE"
      , exemplars = Nothing
      }
    )
  , ("de-IT", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "IT"
      , exemplars = Nothing
      }
    )
  , ("de-LI", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "LI"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2011\x2019\x2030"
          }
      }
    )
  , ("de-LU", LanguageData
      { language = "de"
      , script = Nothing
      , territory = Just "LU"
      , exemplars = Nothing
      }
    )
  , ("dje-NE", LanguageData
      { language = "dje"
      , script = Nothing
      , territory = Just "NE"
      , exemplars = Nothing
      }
    )
  , ("doi-IN", LanguageData
      { language = "doi"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("dsb-DE", LanguageData
      { language = "dsb"
      , script = Nothing
      , territory = Just "DE"
      , exemplars = Nothing
      }
    )
  , ("dua-CM", LanguageData
      { language = "dua"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("dyo-SN", LanguageData
      { language = "dyo"
      , script = Nothing
      , territory = Just "SN"
      , exemplars = Nothing
      }
    )
  , ("dz-BT", LanguageData
      { language = "dz"
      , script = Nothing
      , territory = Just "BT"
      , exemplars = Nothing
      }
    )
  , ("ebu-KE", LanguageData
      { language = "ebu"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("ee-GH", LanguageData
      { language = "ee"
      , script = Nothing
      , territory = Just "GH"
      , exemplars = Nothing
      }
    )
  , ("ee-TG", LanguageData
      { language = "ee"
      , script = Nothing
      , territory = Just "TG"
      , exemplars = Nothing
      }
    )
  , ("el-CY", LanguageData
      { language = "el"
      , script = Nothing
      , territory = Just "CY"
      , exemplars = Nothing
      }
    )
  , ("el-GR", LanguageData
      { language = "el"
      , script = Nothing
      , territory = Just "GR"
      , exemplars = Nothing
      }
    )
  , ("en-001", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "001"
      , exemplars = Nothing
      }
    )
  , ("en-150", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "150"
      , exemplars = Nothing
      }
    )
  , ("en-AE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "AE"
      , exemplars = Nothing
      }
    )
  , ("en-AG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "AG"
      , exemplars = Nothing
      }
    )
  , ("en-AI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "AI"
      , exemplars = Nothing
      }
    )
  , ("en-AS", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "AS"
      , exemplars = Nothing
      }
    )
  , ("en-AT", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "AT"
      , exemplars = Nothing
      }
    )
  , ("en-AU", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "AU"
      , exemplars = Nothing
      }
    )
  , ("en-BB", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BB"
      , exemplars = Nothing
      }
    )
  , ("en-BE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BE"
      , exemplars = Nothing
      }
    )
  , ("en-BI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BI"
      , exemplars = Nothing
      }
    )
  , ("en-BM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BM"
      , exemplars = Nothing
      }
    )
  , ("en-BS", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BS"
      , exemplars = Nothing
      }
    )
  , ("en-BW", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BW"
      , exemplars = Nothing
      }
    )
  , ("en-BZ", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "BZ"
      , exemplars = Nothing
      }
    )
  , ("en-CA", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CA"
      , exemplars = Nothing
      }
    )
  , ("en-CC", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CC"
      , exemplars = Nothing
      }
    )
  , ("en-CH", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Nothing
      }
    )
  , ("en-CK", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CK"
      , exemplars = Nothing
      }
    )
  , ("en-CM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("en-CX", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CX"
      , exemplars = Nothing
      }
    )
  , ("en-CY", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "CY"
      , exemplars = Nothing
      }
    )
  , ("en-DE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "DE"
      , exemplars = Nothing
      }
    )
  , ("en-DG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "DG"
      , exemplars = Nothing
      }
    )
  , ("en-DK", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "DK"
      , exemplars = Nothing
      }
    )
  , ("en-DM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "DM"
      , exemplars = Nothing
      }
    )
  , ("en-ER", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "ER"
      , exemplars = Nothing
      }
    )
  , ("en-FI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "FI"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("en-FJ", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "FJ"
      , exemplars = Nothing
      }
    )
  , ("en-FK", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "FK"
      , exemplars = Nothing
      }
    )
  , ("en-FM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "FM"
      , exemplars = Nothing
      }
    )
  , ("en-GB", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GB"
      , exemplars = Nothing
      }
    )
  , ("en-GD", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GD"
      , exemplars = Nothing
      }
    )
  , ("en-GG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GG"
      , exemplars = Nothing
      }
    )
  , ("en-GH", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GH"
      , exemplars = Nothing
      }
    )
  , ("en-GI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GI"
      , exemplars = Nothing
      }
    )
  , ("en-GM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GM"
      , exemplars = Nothing
      }
    )
  , ("en-GU", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GU"
      , exemplars = Nothing
      }
    )
  , ("en-GY", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "GY"
      , exemplars = Nothing
      }
    )
  , ("en-HK", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "HK"
      , exemplars = Nothing
      }
    )
  , ("en-IE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "IE"
      , exemplars = Nothing
      }
    )
  , ("en-IL", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "IL"
      , exemplars = Nothing
      }
    )
  , ("en-IM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "IM"
      , exemplars = Nothing
      }
    )
  , ("en-IN", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("en-IO", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "IO"
      , exemplars = Nothing
      }
    )
  , ("en-JE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "JE"
      , exemplars = Nothing
      }
    )
  , ("en-JM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "JM"
      , exemplars = Nothing
      }
    )
  , ("en-KE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("en-KI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "KI"
      , exemplars = Nothing
      }
    )
  , ("en-KN", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "KN"
      , exemplars = Nothing
      }
    )
  , ("en-KY", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "KY"
      , exemplars = Nothing
      }
    )
  , ("en-LC", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "LC"
      , exemplars = Nothing
      }
    )
  , ("en-LR", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "LR"
      , exemplars = Nothing
      }
    )
  , ("en-LS", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "LS"
      , exemplars = Nothing
      }
    )
  , ("en-MG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MG"
      , exemplars = Nothing
      }
    )
  , ("en-MH", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MH"
      , exemplars = Nothing
      }
    )
  , ("en-MO", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MO"
      , exemplars = Nothing
      }
    )
  , ("en-MP", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MP"
      , exemplars = Nothing
      }
    )
  , ("en-MS", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MS"
      , exemplars = Nothing
      }
    )
  , ("en-MT", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MT"
      , exemplars = Nothing
      }
    )
  , ("en-MU", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MU"
      , exemplars = Nothing
      }
    )
  , ("en-MW", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MW"
      , exemplars = Nothing
      }
    )
  , ("en-MY", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "MY"
      , exemplars = Nothing
      }
    )
  , ("en-NA", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NA"
      , exemplars = Nothing
      }
    )
  , ("en-NF", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NF"
      , exemplars = Nothing
      }
    )
  , ("en-NG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("en-NL", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NL"
      , exemplars = Nothing
      }
    )
  , ("en-NR", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NR"
      , exemplars = Nothing
      }
    )
  , ("en-NU", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NU"
      , exemplars = Nothing
      }
    )
  , ("en-NZ", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "NZ"
      , exemplars = Nothing
      }
    )
  , ("en-PG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "PG"
      , exemplars = Nothing
      }
    )
  , ("en-PH", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "PH"
      , exemplars = Nothing
      }
    )
  , ("en-PK", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "PK"
      , exemplars = Nothing
      }
    )
  , ("en-PN", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "PN"
      , exemplars = Nothing
      }
    )
  , ("en-PR", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "PR"
      , exemplars = Nothing
      }
    )
  , ("en-PW", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "PW"
      , exemplars = Nothing
      }
    )
  , ("en-RW", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "RW"
      , exemplars = Nothing
      }
    )
  , ("en-SB", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SB"
      , exemplars = Nothing
      }
    )
  , ("en-SC", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SC"
      , exemplars = Nothing
      }
    )
  , ("en-SD", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SD"
      , exemplars = Nothing
      }
    )
  , ("en-SE", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SE"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("en-SG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SG"
      , exemplars = Nothing
      }
    )
  , ("en-SH", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SH"
      , exemplars = Nothing
      }
    )
  , ("en-SI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SI"
      , exemplars = Nothing
      }
    )
  , ("en-SL", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SL"
      , exemplars = Nothing
      }
    )
  , ("en-SS", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SS"
      , exemplars = Nothing
      }
    )
  , ("en-SX", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SX"
      , exemplars = Nothing
      }
    )
  , ("en-SZ", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "SZ"
      , exemplars = Nothing
      }
    )
  , ("en-TC", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "TC"
      , exemplars = Nothing
      }
    )
  , ("en-TK", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "TK"
      , exemplars = Nothing
      }
    )
  , ("en-TO", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "TO"
      , exemplars = Nothing
      }
    )
  , ("en-TT", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "TT"
      , exemplars = Nothing
      }
    )
  , ("en-TV", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "TV"
      , exemplars = Nothing
      }
    )
  , ("en-TZ", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("en-UG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("en-UM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "UM"
      , exemplars = Nothing
      }
    )
  , ("en-US", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "US"
      , exemplars = Nothing
      }
    )
  , ("en-VC", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "VC"
      , exemplars = Nothing
      }
    )
  , ("en-VG", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "VG"
      , exemplars = Nothing
      }
    )
  , ("en-VI", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "VI"
      , exemplars = Nothing
      }
    )
  , ("en-VU", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "VU"
      , exemplars = Nothing
      }
    )
  , ("en-WS", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "WS"
      , exemplars = Nothing
      }
    )
  , ("en-ZA", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "ZA"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE4\xE5\xE6\xE7\xE8\xE9\xEA\
             \\xEB\xEC\xED\xEE\xEF\xF1\xF2\xF3\xF4\xF6\
             \\xF8\xF9\xFA\xFB\xFC\xFF\x101\x103\x113\x115\
             \\x12B\x12D\x14D\x14F\x153\x161\x16B\x16D\x1E13\x1E3D\
             \\x1E45\x1E4B\x1E71"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("en-ZM", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "ZM"
      , exemplars = Nothing
      }
    )
  , ("en-ZW", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "ZW"
      , exemplars = Nothing
      }
    )
  , ("eo-001", LanguageData
      { language = "eo"
      , script = Nothing
      , territory = Just "001"
      , exemplars = Nothing
      }
    )
  , ("es-419", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "419"
      , exemplars = Nothing
      }
    )
  , ("es-AR", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "AR"
      , exemplars = Nothing
      }
    )
  , ("es-BO", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "BO"
      , exemplars = Nothing
      }
    )
  , ("es-BR", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "BR"
      , exemplars = Nothing
      }
    )
  , ("es-BZ", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "BZ"
      , exemplars = Nothing
      }
    )
  , ("es-CL", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "CL"
      , exemplars = Nothing
      }
    )
  , ("es-CO", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "CO"
      , exemplars = Nothing
      }
    )
  , ("es-CR", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "CR"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("es-CU", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "CU"
      , exemplars = Nothing
      }
    )
  , ("es-DO", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "DO"
      , exemplars = Nothing
      }
    )
  , ("es-EA", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "EA"
      , exemplars = Nothing
      }
    )
  , ("es-EC", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "EC"
      , exemplars = Nothing
      }
    )
  , ("es-ES", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "ES"
      , exemplars = Nothing
      }
    )
  , ("es-GQ", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "GQ"
      , exemplars = Nothing
      }
    )
  , ("es-GT", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "GT"
      , exemplars = Nothing
      }
    )
  , ("es-HN", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "HN"
      , exemplars = Nothing
      }
    )
  , ("es-IC", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "IC"
      , exemplars = Nothing
      }
    )
  , ("es-MX", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "MX"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x2E\x2E\x2E"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE2\xE3\xE4\xE5\xE6\xE7\xE8\xEA\xEB\
             \\xEC\xEE\xEF\xF2\xF4\xF6\xF8\xF9\xFB\xFF\
             \\x101\x103\x113\x115\x12B\x12D\x14D\x14F\x153\x16B\
             \\x16D"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("es-NI", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "NI"
      , exemplars = Nothing
      }
    )
  , ("es-PA", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "PA"
      , exemplars = Nothing
      }
    )
  , ("es-PE", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "PE"
      , exemplars = Nothing
      }
    )
  , ("es-PH", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "PH"
      , exemplars = Nothing
      }
    )
  , ("es-PR", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "PR"
      , exemplars = Nothing
      }
    )
  , ("es-PY", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "PY"
      , exemplars = Nothing
      }
    )
  , ("es-SV", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "SV"
      , exemplars = Nothing
      }
    )
  , ("es-US", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "US"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x2E\x2E\x2E"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("es-UY", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "UY"
      , exemplars = Nothing
      }
    )
  , ("es-VE", LanguageData
      { language = "es"
      , script = Nothing
      , territory = Just "VE"
      , exemplars = Nothing
      }
    )
  , ("et-EE", LanguageData
      { language = "et"
      , script = Nothing
      , territory = Just "EE"
      , exemplars = Nothing
      }
    )
  , ("eu-ES", LanguageData
      { language = "eu"
      , script = Nothing
      , territory = Just "ES"
      , exemplars = Nothing
      }
    )
  , ("ewo-CM", LanguageData
      { language = "ewo"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("fa-AF", LanguageData
      { language = "fa"
      , script = Nothing
      , territory = Just "AF"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Just $ Set.fromList
             "\x625\x643\x649\x64A\x64E\x64F\x650\x652\x656\x670\
             \\x67C\x681\x685\x689\x693\x696\x69A\x6AB\x6BC\x200C\
             \\x200D\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("fa-IR", LanguageData
      { language = "fa"
      , script = Nothing
      , territory = Just "IR"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1E922\x1E923\x1E924\x1E925\x1E926\x1E927\x1E928\x1E929\x1E92A\x1E92B\
             \\x1E92C\x1E92D\x1E92E\x1E92F\x1E930\x1E931\x1E932\x1E933\x1E934\x1E935\
             \\x1E936\x1E937\x1E938\x1E939\x1E93A\x1E93B\x1E93C\x1E93D\x1E944\x1E945\x1E946\x1E94B"
          , _exAuxiliary = Just $ Set.fromList
             "\x1E93E\x1E93F\x1E940\x1E941\x1E942\x1E943"
          , _exPunctuation = Just $ Set.fromList
             "\x25\x2D\x2E\x2011\x2030\x1E95E\x1E95F"
          , _exNumbers = Just $ Set.fromList
             "\x1E950\x1E951\x1E952\x1E953\x1E954\x1E955\x1E956\x1E957\x1E958\x1E959"
          }
      }
    )
  , ("ff-Latn", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("fi-FI", LanguageData
      { language = "fi"
      , script = Nothing
      , territory = Just "FI"
      , exemplars = Nothing
      }
    )
  , ("fil-PH", LanguageData
      { language = "fil"
      , script = Nothing
      , territory = Just "PH"
      , exemplars = Nothing
      }
    )
  , ("fo-DK", LanguageData
      { language = "fo"
      , script = Nothing
      , territory = Just "DK"
      , exemplars = Nothing
      }
    )
  , ("fo-FO", LanguageData
      { language = "fo"
      , script = Nothing
      , territory = Just "FO"
      , exemplars = Nothing
      }
    )
  , ("fr-BE", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "BE"
      , exemplars = Nothing
      }
    )
  , ("fr-BF", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "BF"
      , exemplars = Nothing
      }
    )
  , ("fr-BI", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "BI"
      , exemplars = Nothing
      }
    )
  , ("fr-BJ", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "BJ"
      , exemplars = Nothing
      }
    )
  , ("fr-BL", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "BL"
      , exemplars = Nothing
      }
    )
  , ("fr-CA", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CA"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Just $ Set.fromList
             "\xE1\xE3\xE4\xE5\xEC\xED\xF1\xF2\xF3\xF6\
             \\xF8\xFA\x101\x113\x12B\x1D4"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("fr-CD", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CD"
      , exemplars = Nothing
      }
    )
  , ("fr-CF", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CF"
      , exemplars = Nothing
      }
    )
  , ("fr-CG", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CG"
      , exemplars = Nothing
      }
    )
  , ("fr-CH", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Nothing
      }
    )
  , ("fr-CI", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CI"
      , exemplars = Nothing
      }
    )
  , ("fr-CM", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("fr-DJ", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "DJ"
      , exemplars = Nothing
      }
    )
  , ("fr-DZ", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "DZ"
      , exemplars = Nothing
      }
    )
  , ("fr-FR", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "FR"
      , exemplars = Nothing
      }
    )
  , ("fr-GA", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "GA"
      , exemplars = Nothing
      }
    )
  , ("fr-GF", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "GF"
      , exemplars = Nothing
      }
    )
  , ("fr-GN", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "GN"
      , exemplars = Nothing
      }
    )
  , ("fr-GP", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "GP"
      , exemplars = Nothing
      }
    )
  , ("fr-GQ", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "GQ"
      , exemplars = Nothing
      }
    )
  , ("fr-HT", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "HT"
      , exemplars = Nothing
      }
    )
  , ("fr-KM", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "KM"
      , exemplars = Nothing
      }
    )
  , ("fr-LU", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "LU"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("fr-MA", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MA"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("fr-MC", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MC"
      , exemplars = Nothing
      }
    )
  , ("fr-MF", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MF"
      , exemplars = Nothing
      }
    )
  , ("fr-MG", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MG"
      , exemplars = Nothing
      }
    )
  , ("fr-ML", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "ML"
      , exemplars = Nothing
      }
    )
  , ("fr-MQ", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MQ"
      , exemplars = Nothing
      }
    )
  , ("fr-MR", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MR"
      , exemplars = Nothing
      }
    )
  , ("fr-MU", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "MU"
      , exemplars = Nothing
      }
    )
  , ("fr-NC", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "NC"
      , exemplars = Nothing
      }
    )
  , ("fr-NE", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "NE"
      , exemplars = Nothing
      }
    )
  , ("fr-PF", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "PF"
      , exemplars = Nothing
      }
    )
  , ("fr-PM", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "PM"
      , exemplars = Nothing
      }
    )
  , ("fr-RE", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "RE"
      , exemplars = Nothing
      }
    )
  , ("fr-RW", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "RW"
      , exemplars = Nothing
      }
    )
  , ("fr-SC", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "SC"
      , exemplars = Nothing
      }
    )
  , ("fr-SN", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "SN"
      , exemplars = Nothing
      }
    )
  , ("fr-SY", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "SY"
      , exemplars = Nothing
      }
    )
  , ("fr-TD", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "TD"
      , exemplars = Nothing
      }
    )
  , ("fr-TG", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "TG"
      , exemplars = Nothing
      }
    )
  , ("fr-TN", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "TN"
      , exemplars = Nothing
      }
    )
  , ("fr-VU", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "VU"
      , exemplars = Nothing
      }
    )
  , ("fr-WF", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "WF"
      , exemplars = Nothing
      }
    )
  , ("fr-YT", LanguageData
      { language = "fr"
      , script = Nothing
      , territory = Just "YT"
      , exemplars = Nothing
      }
    )
  , ("fur-IT", LanguageData
      { language = "fur"
      , script = Nothing
      , territory = Just "IT"
      , exemplars = Nothing
      }
    )
  , ("fy-NL", LanguageData
      { language = "fy"
      , script = Nothing
      , territory = Just "NL"
      , exemplars = Nothing
      }
    )
  , ("ga-GB", LanguageData
      { language = "ga"
      , script = Nothing
      , territory = Just "GB"
      , exemplars = Nothing
      }
    )
  , ("ga-IE", LanguageData
      { language = "ga"
      , script = Nothing
      , territory = Just "IE"
      , exemplars = Nothing
      }
    )
  , ("gd-GB", LanguageData
      { language = "gd"
      , script = Nothing
      , territory = Just "GB"
      , exemplars = Nothing
      }
    )
  , ("gl-ES", LanguageData
      { language = "gl"
      , script = Nothing
      , territory = Just "ES"
      , exemplars = Nothing
      }
    )
  , ("gsw-CH", LanguageData
      { language = "gsw"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Nothing
      }
    )
  , ("gsw-FR", LanguageData
      { language = "gsw"
      , script = Nothing
      , territory = Just "FR"
      , exemplars = Nothing
      }
    )
  , ("gsw-LI", LanguageData
      { language = "gsw"
      , script = Nothing
      , territory = Just "LI"
      , exemplars = Nothing
      }
    )
  , ("gu-IN", LanguageData
      { language = "gu"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("guz-KE", LanguageData
      { language = "guz"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("gv-IM", LanguageData
      { language = "gv"
      , script = Nothing
      , territory = Just "IM"
      , exemplars = Nothing
      }
    )
  , ("ha-GH", LanguageData
      { language = "ha"
      , script = Nothing
      , territory = Just "GH"
      , exemplars = Nothing
      }
    )
  , ("ha-NE", LanguageData
      { language = "ha"
      , script = Nothing
      , territory = Just "NE"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Just $ Set.fromList
             "\x70\x71\x72\x303\x76\x78\xE0\xE1\xE2\xE8\xE9\
             \\xEA\xEC\xED\xEE\xF2\xF3\xF4\xF9\xFA\xFB\
             \\x2BC\x79"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ha-NG", LanguageData
      { language = "ha"
      , script = Nothing
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("haw-US", LanguageData
      { language = "haw"
      , script = Nothing
      , territory = Just "US"
      , exemplars = Nothing
      }
    )
  , ("he-IL", LanguageData
      { language = "he"
      , script = Nothing
      , territory = Just "IL"
      , exemplars = Nothing
      }
    )
  , ("hi-IN", LanguageData
      { language = "hi"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("hr-BA", LanguageData
      { language = "hr"
      , script = Nothing
      , territory = Just "BA"
      , exemplars = Nothing
      }
    )
  , ("hr-HR", LanguageData
      { language = "hr"
      , script = Nothing
      , territory = Just "HR"
      , exemplars = Nothing
      }
    )
  , ("hsb-DE", LanguageData
      { language = "hsb"
      , script = Nothing
      , territory = Just "DE"
      , exemplars = Nothing
      }
    )
  , ("hu-HU", LanguageData
      { language = "hu"
      , script = Nothing
      , territory = Just "HU"
      , exemplars = Nothing
      }
    )
  , ("hy-AM", LanguageData
      { language = "hy"
      , script = Nothing
      , territory = Just "AM"
      , exemplars = Nothing
      }
    )
  , ("ia-001", LanguageData
      { language = "ia"
      , script = Nothing
      , territory = Just "001"
      , exemplars = Nothing
      }
    )
  , ("id-ID", LanguageData
      { language = "id"
      , script = Nothing
      , territory = Just "ID"
      , exemplars = Nothing
      }
    )
  , ("ig-NG", LanguageData
      { language = "ig"
      , script = Nothing
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("ii-CN", LanguageData
      { language = "ii"
      , script = Nothing
      , territory = Just "CN"
      , exemplars = Nothing
      }
    )
  , ("is-IS", LanguageData
      { language = "is"
      , script = Nothing
      , territory = Just "IS"
      , exemplars = Nothing
      }
    )
  , ("it-CH", LanguageData
      { language = "it"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2D\x2E\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\x2011\x2019\x2030"
          }
      }
    )
  , ("it-IT", LanguageData
      { language = "it"
      , script = Nothing
      , territory = Just "IT"
      , exemplars = Nothing
      }
    )
  , ("it-SM", LanguageData
      { language = "it"
      , script = Nothing
      , territory = Just "SM"
      , exemplars = Nothing
      }
    )
  , ("it-VA", LanguageData
      { language = "it"
      , script = Nothing
      , territory = Just "VA"
      , exemplars = Nothing
      }
    )
  , ("ja-JP", LanguageData
      { language = "ja"
      , script = Nothing
      , territory = Just "JP"
      , exemplars = Nothing
      }
    )
  , ("jgo-CM", LanguageData
      { language = "jgo"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("jmc-TZ", LanguageData
      { language = "jmc"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("jv-ID", LanguageData
      { language = "jv"
      , script = Nothing
      , territory = Just "ID"
      , exemplars = Nothing
      }
    )
  , ("ka-GE", LanguageData
      { language = "ka"
      , script = Nothing
      , territory = Just "GE"
      , exemplars = Nothing
      }
    )
  , ("kab-DZ", LanguageData
      { language = "kab"
      , script = Nothing
      , territory = Just "DZ"
      , exemplars = Nothing
      }
    )
  , ("kam-KE", LanguageData
      { language = "kam"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("kde-TZ", LanguageData
      { language = "kde"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("kea-CV", LanguageData
      { language = "kea"
      , script = Nothing
      , territory = Just "CV"
      , exemplars = Nothing
      }
    )
  , ("khq-ML", LanguageData
      { language = "khq"
      , script = Nothing
      , territory = Just "ML"
      , exemplars = Nothing
      }
    )
  , ("ki-KE", LanguageData
      { language = "ki"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("kk-KZ", LanguageData
      { language = "kk"
      , script = Nothing
      , territory = Just "KZ"
      , exemplars = Nothing
      }
    )
  , ("kkj-CM", LanguageData
      { language = "kkj"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("kl-GL", LanguageData
      { language = "kl"
      , script = Nothing
      , territory = Just "GL"
      , exemplars = Nothing
      }
    )
  , ("kln-KE", LanguageData
      { language = "kln"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("km-KH", LanguageData
      { language = "km"
      , script = Nothing
      , territory = Just "KH"
      , exemplars = Nothing
      }
    )
  , ("kn-IN", LanguageData
      { language = "kn"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ko-KP", LanguageData
      { language = "ko"
      , script = Nothing
      , territory = Just "KP"
      , exemplars = Nothing
      }
    )
  , ("ko-KR", LanguageData
      { language = "ko"
      , script = Nothing
      , territory = Just "KR"
      , exemplars = Nothing
      }
    )
  , ("kok-IN", LanguageData
      { language = "kok"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ks-Arab", LanguageData
      { language = "ks"
      , script = Just "Arab"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("ksb-TZ", LanguageData
      { language = "ksb"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("ksf-CM", LanguageData
      { language = "ksf"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("ksh-DE", LanguageData
      { language = "ksh"
      , script = Nothing
      , territory = Just "DE"
      , exemplars = Nothing
      }
    )
  , ("ku-TR", LanguageData
      { language = "ku"
      , script = Nothing
      , territory = Just "TR"
      , exemplars = Nothing
      }
    )
  , ("kw-GB", LanguageData
      { language = "kw"
      , script = Nothing
      , territory = Just "GB"
      , exemplars = Nothing
      }
    )
  , ("ky-KG", LanguageData
      { language = "ky"
      , script = Nothing
      , territory = Just "KG"
      , exemplars = Nothing
      }
    )
  , ("lag-TZ", LanguageData
      { language = "lag"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("lb-LU", LanguageData
      { language = "lb"
      , script = Nothing
      , territory = Just "LU"
      , exemplars = Nothing
      }
    )
  , ("lg-UG", LanguageData
      { language = "lg"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("lkt-US", LanguageData
      { language = "lkt"
      , script = Nothing
      , territory = Just "US"
      , exemplars = Nothing
      }
    )
  , ("ln-AO", LanguageData
      { language = "ln"
      , script = Nothing
      , territory = Just "AO"
      , exemplars = Nothing
      }
    )
  , ("ln-CD", LanguageData
      { language = "ln"
      , script = Nothing
      , territory = Just "CD"
      , exemplars = Nothing
      }
    )
  , ("ln-CF", LanguageData
      { language = "ln"
      , script = Nothing
      , territory = Just "CF"
      , exemplars = Nothing
      }
    )
  , ("ln-CG", LanguageData
      { language = "ln"
      , script = Nothing
      , territory = Just "CG"
      , exemplars = Nothing
      }
    )
  , ("lo-LA", LanguageData
      { language = "lo"
      , script = Nothing
      , territory = Just "LA"
      , exemplars = Nothing
      }
    )
  , ("lrc-IQ", LanguageData
      { language = "lrc"
      , script = Nothing
      , territory = Just "IQ"
      , exemplars = Nothing
      }
    )
  , ("lrc-IR", LanguageData
      { language = "lrc"
      , script = Nothing
      , territory = Just "IR"
      , exemplars = Nothing
      }
    )
  , ("lt-LT", LanguageData
      { language = "lt"
      , script = Nothing
      , territory = Just "LT"
      , exemplars = Nothing
      }
    )
  , ("lu-CD", LanguageData
      { language = "lu"
      , script = Nothing
      , territory = Just "CD"
      , exemplars = Nothing
      }
    )
  , ("luo-KE", LanguageData
      { language = "luo"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("luy-KE", LanguageData
      { language = "luy"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("lv-LV", LanguageData
      { language = "lv"
      , script = Nothing
      , territory = Just "LV"
      , exemplars = Nothing
      }
    )
  , ("mai-IN", LanguageData
      { language = "mai"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("mas-KE", LanguageData
      { language = "mas"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("mas-TZ", LanguageData
      { language = "mas"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("mer-KE", LanguageData
      { language = "mer"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("mfe-MU", LanguageData
      { language = "mfe"
      , script = Nothing
      , territory = Just "MU"
      , exemplars = Nothing
      }
    )
  , ("mg-MG", LanguageData
      { language = "mg"
      , script = Nothing
      , territory = Just "MG"
      , exemplars = Nothing
      }
    )
  , ("mgh-MZ", LanguageData
      { language = "mgh"
      , script = Nothing
      , territory = Just "MZ"
      , exemplars = Nothing
      }
    )
  , ("mgo-CM", LanguageData
      { language = "mgo"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("mi-NZ", LanguageData
      { language = "mi"
      , script = Nothing
      , territory = Just "NZ"
      , exemplars = Nothing
      }
    )
  , ("mk-MK", LanguageData
      { language = "mk"
      , script = Nothing
      , territory = Just "MK"
      , exemplars = Nothing
      }
    )
  , ("ml-IN", LanguageData
      { language = "ml"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("mn-MN", LanguageData
      { language = "mn"
      , script = Nothing
      , territory = Just "MN"
      , exemplars = Nothing
      }
    )
  , ("mni-Beng", LanguageData
      { language = "mni"
      , script = Just "Beng"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("mr-IN", LanguageData
      { language = "mr"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ms-BN", LanguageData
      { language = "ms"
      , script = Nothing
      , territory = Just "BN"
      , exemplars = Nothing
      }
    )
  , ("ms-ID", LanguageData
      { language = "ms"
      , script = Nothing
      , territory = Just "ID"
      , exemplars = Nothing
      }
    )
  , ("ms-MY", LanguageData
      { language = "ms"
      , script = Nothing
      , territory = Just "MY"
      , exemplars = Nothing
      }
    )
  , ("ms-SG", LanguageData
      { language = "ms"
      , script = Nothing
      , territory = Just "SG"
      , exemplars = Nothing
      }
    )
  , ("mt-MT", LanguageData
      { language = "mt"
      , script = Nothing
      , territory = Just "MT"
      , exemplars = Nothing
      }
    )
  , ("mua-CM", LanguageData
      { language = "mua"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("my-MM", LanguageData
      { language = "my"
      , script = Nothing
      , territory = Just "MM"
      , exemplars = Nothing
      }
    )
  , ("mzn-IR", LanguageData
      { language = "mzn"
      , script = Nothing
      , territory = Just "IR"
      , exemplars = Nothing
      }
    )
  , ("naq-NA", LanguageData
      { language = "naq"
      , script = Nothing
      , territory = Just "NA"
      , exemplars = Nothing
      }
    )
  , ("nb-NO", LanguageData
      { language = "nb"
      , script = Nothing
      , territory = Just "NO"
      , exemplars = Nothing
      }
    )
  , ("nb-SJ", LanguageData
      { language = "nb"
      , script = Nothing
      , territory = Just "SJ"
      , exemplars = Nothing
      }
    )
  , ("nd-ZW", LanguageData
      { language = "nd"
      , script = Nothing
      , territory = Just "ZW"
      , exemplars = Nothing
      }
    )
  , ("nds-DE", LanguageData
      { language = "nds"
      , script = Nothing
      , territory = Just "DE"
      , exemplars = Nothing
      }
    )
  , ("nds-NL", LanguageData
      { language = "nds"
      , script = Nothing
      , territory = Just "NL"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\xE0\xE1\xE2\xE4\xE8\xE9\xEA\xEB\xEC\xED\
             \\xEE\xEF\xF2\xF3\xF4\xF6\xF9\xFA\xFB\xFC"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Nothing
          }
      }
    )
  , ("ne-IN", LanguageData
      { language = "ne"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ne-NP", LanguageData
      { language = "ne"
      , script = Nothing
      , territory = Just "NP"
      , exemplars = Nothing
      }
    )
  , ("nl-AW", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "AW"
      , exemplars = Nothing
      }
    )
  , ("nl-BE", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "BE"
      , exemplars = Nothing
      }
    )
  , ("nl-BQ", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "BQ"
      , exemplars = Nothing
      }
    )
  , ("nl-CW", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "CW"
      , exemplars = Nothing
      }
    )
  , ("nl-NL", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "NL"
      , exemplars = Nothing
      }
    )
  , ("nl-SR", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "SR"
      , exemplars = Nothing
      }
    )
  , ("nl-SX", LanguageData
      { language = "nl"
      , script = Nothing
      , territory = Just "SX"
      , exemplars = Nothing
      }
    )
  , ("nmg-CM", LanguageData
      { language = "nmg"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("nn-NO", LanguageData
      { language = "nn"
      , script = Nothing
      , territory = Just "NO"
      , exemplars = Nothing
      }
    )
  , ("nnh-CM", LanguageData
      { language = "nnh"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("nus-SS", LanguageData
      { language = "nus"
      , script = Nothing
      , territory = Just "SS"
      , exemplars = Nothing
      }
    )
  , ("nyn-UG", LanguageData
      { language = "nyn"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("om-ET", LanguageData
      { language = "om"
      , script = Nothing
      , territory = Just "ET"
      , exemplars = Nothing
      }
    )
  , ("om-KE", LanguageData
      { language = "om"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("or-IN", LanguageData
      { language = "or"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("os-GE", LanguageData
      { language = "os"
      , script = Nothing
      , territory = Just "GE"
      , exemplars = Nothing
      }
    )
  , ("os-RU", LanguageData
      { language = "os"
      , script = Nothing
      , territory = Just "RU"
      , exemplars = Nothing
      }
    )
  , ("pa-Arab", LanguageData
      { language = "pa"
      , script = Just "Arab"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x624\x626\x627\x628\x62A\x62B\x62C\x62D\
             \\x62E\x62F\x630\x631\x632\x633\x634\x635\x636\x637\
             \\x638\x639\x63A\x641\x642\x644\x645\x646\x647\x648\
             \\x64F\x679\x67E\x686\x688\x691\x698\x6A9\x6AF\x6BA\
             \\x6BE\x6C1\x6CC\x6D2"
          , _exAuxiliary = Just $ Set.fromList
             "\x623\x629\x67A\x67B\x67C\x67D\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x200E\x2011\x2030"
          }
      }
    )
  , ("pa-Guru", LanguageData
      { language = "pa"
      , script = Just "Guru"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("pcm-NG", LanguageData
      { language = "pcm"
      , script = Nothing
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("pl-PL", LanguageData
      { language = "pl"
      , script = Nothing
      , territory = Just "PL"
      , exemplars = Nothing
      }
    )
  , ("ps-AF", LanguageData
      { language = "ps"
      , script = Nothing
      , territory = Just "AF"
      , exemplars = Nothing
      }
    )
  , ("ps-PK", LanguageData
      { language = "ps"
      , script = Nothing
      , territory = Just "PK"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x623\x624\x626\x627\x628\x629\x62A\x62B\
             \\x62C\x62D\x62E\x62F\x630\x631\x632\x633\x634\x635\
             \\x636\x637\x638\x639\x63A\x641\x642\x644\x645\x646\
             \\x647\x648\x64A\x64B\x64C\x64D\x64E\x64F\x650\x651\
             \\x652\x654\x670\x67C\x67E\x681\x685\x686\x689\x693\
             \\x696\x698\x69A\x6A9\x6AB\x6AF\x6BC\x6CC\x6CD\x6D0\
             \\x6D2"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("pt-AO", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "AO"
      , exemplars = Nothing
      }
    )
  , ("pt-BR", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "BR"
      , exemplars = Nothing
      }
    )
  , ("pt-CH", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Nothing
      }
    )
  , ("pt-CV", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "CV"
      , exemplars = Nothing
      }
    )
  , ("pt-GQ", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "GQ"
      , exemplars = Nothing
      }
    )
  , ("pt-GW", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "GW"
      , exemplars = Nothing
      }
    )
  , ("pt-LU", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "LU"
      , exemplars = Nothing
      }
    )
  , ("pt-MO", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "MO"
      , exemplars = Nothing
      }
    )
  , ("pt-MZ", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "MZ"
      , exemplars = Nothing
      }
    )
  , ("pt-PT", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "PT"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x3F"
          , _exAuxiliary = Nothing
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\xAB\
             \\xBB\x2010\x2011\x2013\x2014\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("pt-ST", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "ST"
      , exemplars = Nothing
      }
    )
  , ("pt-TL", LanguageData
      { language = "pt"
      , script = Nothing
      , territory = Just "TL"
      , exemplars = Nothing
      }
    )
  , ("qu-BO", LanguageData
      { language = "qu"
      , script = Nothing
      , territory = Just "BO"
      , exemplars = Nothing
      }
    )
  , ("qu-EC", LanguageData
      { language = "qu"
      , script = Nothing
      , territory = Just "EC"
      , exemplars = Nothing
      }
    )
  , ("qu-PE", LanguageData
      { language = "qu"
      , script = Nothing
      , territory = Just "PE"
      , exemplars = Nothing
      }
    )
  , ("rm-CH", LanguageData
      { language = "rm"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Nothing
      }
    )
  , ("rn-BI", LanguageData
      { language = "rn"
      , script = Nothing
      , territory = Just "BI"
      , exemplars = Nothing
      }
    )
  , ("ro-MD", LanguageData
      { language = "ro"
      , script = Nothing
      , territory = Just "MD"
      , exemplars = Nothing
      }
    )
  , ("ro-RO", LanguageData
      { language = "ro"
      , script = Nothing
      , territory = Just "RO"
      , exemplars = Nothing
      }
    )
  , ("rof-TZ", LanguageData
      { language = "rof"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("ru-BY", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Just "BY"
      , exemplars = Nothing
      }
    )
  , ("ru-KG", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Just "KG"
      , exemplars = Nothing
      }
    )
  , ("ru-KZ", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Just "KZ"
      , exemplars = Nothing
      }
    )
  , ("ru-MD", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Just "MD"
      , exemplars = Nothing
      }
    )
  , ("ru-RU", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Just "RU"
      , exemplars = Nothing
      }
    )
  , ("ru-UA", LanguageData
      { language = "ru"
      , script = Nothing
      , territory = Just "UA"
      , exemplars = Nothing
      }
    )
  , ("rw-RW", LanguageData
      { language = "rw"
      , script = Nothing
      , territory = Just "RW"
      , exemplars = Nothing
      }
    )
  , ("rwk-TZ", LanguageData
      { language = "rwk"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("sa-IN", LanguageData
      { language = "sa"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("sah-RU", LanguageData
      { language = "sah"
      , script = Nothing
      , territory = Just "RU"
      , exemplars = Nothing
      }
    )
  , ("saq-KE", LanguageData
      { language = "saq"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("sat-Olck", LanguageData
      { language = "sat"
      , script = Just "Olck"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("sbp-TZ", LanguageData
      { language = "sbp"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("sd-Arab", LanguageData
      { language = "sd"
      , script = Just "Arab"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("sd-Deva", LanguageData
      { language = "sd"
      , script = Just "Deva"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x902\x905\x906\x907\x908\x909\x90A\x90F\x910\x913\
             \\x914\x915\x916\x917\x918\x919\x91A\x91B\x91C\x91D\
             \\x91E\x91F\x920\x921\x922\x923\x924\x925\x926\x927\
             \\x928\x92A\x92B\x92C\x92D\x92E\x92F\x930\x932\x935\
             \\x936\x937\x938\x939\x93C\x93E\x93F\x940\x941\x942\
             \\x943\x944\x945\x947\x948\x949\x94B\x94C\x94D\x97B\
             \\x97C\x97E\x97F"
          , _exAuxiliary = Just $ Set.fromList
             "\x200C\x200D"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x26\x27\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5D\xA7\x2010\
             \\x2011\x2013\x2014\x2018\x2019\x201C\x201D\x2020\x2021\x2026\
             \\x2032\x2033"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("se-FI", LanguageData
      { language = "se"
      , script = Nothing
      , territory = Just "FI"
      , exemplars = Nothing
      }
    )
  , ("se-NO", LanguageData
      { language = "se"
      , script = Nothing
      , territory = Just "NO"
      , exemplars = Nothing
      }
    )
  , ("se-SE", LanguageData
      { language = "se"
      , script = Nothing
      , territory = Just "SE"
      , exemplars = Nothing
      }
    )
  , ("seh-MZ", LanguageData
      { language = "seh"
      , script = Nothing
      , territory = Just "MZ"
      , exemplars = Nothing
      }
    )
  , ("ses-ML", LanguageData
      { language = "ses"
      , script = Nothing
      , territory = Just "ML"
      , exemplars = Nothing
      }
    )
  , ("sg-CF", LanguageData
      { language = "sg"
      , script = Nothing
      , territory = Just "CF"
      , exemplars = Nothing
      }
    )
  , ("shi-Latn", LanguageData
      { language = "shi"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x67\x2B7\x68\x69\
             \\x6A\x6B\x6B\x2B7\x6C\x6D\x6E\x71\x72\x73\x74\
             \\x75\x77\x78\x79\x7A\x25B\x263\x1E0D\x1E25\x1E5B\
             \\x1E63\x1E6D"
          , _exAuxiliary = Just $ Set.fromList
             "\x6F\x70\x76"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("shi-Tfng", LanguageData
      { language = "shi"
      , script = Just "Tfng"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("si-LK", LanguageData
      { language = "si"
      , script = Nothing
      , territory = Just "LK"
      , exemplars = Nothing
      }
    )
  , ("sk-SK", LanguageData
      { language = "sk"
      , script = Nothing
      , territory = Just "SK"
      , exemplars = Nothing
      }
    )
  , ("sl-SI", LanguageData
      { language = "sl"
      , script = Nothing
      , territory = Just "SI"
      , exemplars = Nothing
      }
    )
  , ("smn-FI", LanguageData
      { language = "smn"
      , script = Nothing
      , territory = Just "FI"
      , exemplars = Nothing
      }
    )
  , ("sn-ZW", LanguageData
      { language = "sn"
      , script = Nothing
      , territory = Just "ZW"
      , exemplars = Nothing
      }
    )
  , ("so-DJ", LanguageData
      { language = "so"
      , script = Nothing
      , territory = Just "DJ"
      , exemplars = Nothing
      }
    )
  , ("so-ET", LanguageData
      { language = "so"
      , script = Nothing
      , territory = Just "ET"
      , exemplars = Nothing
      }
    )
  , ("so-KE", LanguageData
      { language = "so"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("so-SO", LanguageData
      { language = "so"
      , script = Nothing
      , territory = Just "SO"
      , exemplars = Nothing
      }
    )
  , ("sq-AL", LanguageData
      { language = "sq"
      , script = Nothing
      , territory = Just "AL"
      , exemplars = Nothing
      }
    )
  , ("sq-MK", LanguageData
      { language = "sq"
      , script = Nothing
      , territory = Just "MK"
      , exemplars = Nothing
      }
    )
  , ("sq-XK", LanguageData
      { language = "sq"
      , script = Nothing
      , territory = Just "XK"
      , exemplars = Nothing
      }
    )
  , ("sr-Cyrl", LanguageData
      { language = "sr"
      , script = Just "Cyrl"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("sr-Latn", LanguageData
      { language = "sr"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x64\x17E\x65\x66\x67\x68\x69\
             \\x6A\x6B\x6C\x6C\x6A\x6D\x6E\x6E\x6A\x6F\x70\x72\
             \\x73\x74\x75\x76\x7A\x107\x10D\x111\x161\x17E"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x77\x78\x79\xE5"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x23\x28\x29\x2A\x2C\x2D\x2E\x3A\x3B\
             \\x3F\x5B\x5D\x7B\x7D\x2010\x2011\x2013\x2018\x201A\
             \\x201C\x201E\x2026"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("su-Latn", LanguageData
      { language = "su"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("sv-AX", LanguageData
      { language = "sv"
      , script = Nothing
      , territory = Just "AX"
      , exemplars = Nothing
      }
    )
  , ("sv-FI", LanguageData
      { language = "sv"
      , script = Nothing
      , territory = Just "FI"
      , exemplars = Nothing
      }
    )
  , ("sv-SE", LanguageData
      { language = "sv"
      , script = Nothing
      , territory = Just "SE"
      , exemplars = Nothing
      }
    )
  , ("sw-CD", LanguageData
      { language = "sw"
      , script = Nothing
      , territory = Just "CD"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x74\x75\
             \\x76\x77\x79\x7A"
          , _exAuxiliary = Just $ Set.fromList
             "\x71\x78"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("sw-KE", LanguageData
      { language = "sw"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("sw-TZ", LanguageData
      { language = "sw"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("sw-UG", LanguageData
      { language = "sw"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("ta-IN", LanguageData
      { language = "ta"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ta-LK", LanguageData
      { language = "ta"
      , script = Nothing
      , territory = Just "LK"
      , exemplars = Nothing
      }
    )
  , ("ta-MY", LanguageData
      { language = "ta"
      , script = Nothing
      , territory = Just "MY"
      , exemplars = Nothing
      }
    )
  , ("ta-SG", LanguageData
      { language = "ta"
      , script = Nothing
      , territory = Just "SG"
      , exemplars = Nothing
      }
    )
  , ("te-IN", LanguageData
      { language = "te"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("teo-KE", LanguageData
      { language = "teo"
      , script = Nothing
      , territory = Just "KE"
      , exemplars = Nothing
      }
    )
  , ("teo-UG", LanguageData
      { language = "teo"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("tg-TJ", LanguageData
      { language = "tg"
      , script = Nothing
      , territory = Just "TJ"
      , exemplars = Nothing
      }
    )
  , ("th-TH", LanguageData
      { language = "th"
      , script = Nothing
      , territory = Just "TH"
      , exemplars = Nothing
      }
    )
  , ("ti-ER", LanguageData
      { language = "ti"
      , script = Nothing
      , territory = Just "ER"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x1200\x1201\x1202\x1203\x1204\x1205\x1206\x1208\x1209\x120A\
             \\x120B\x120C\x120D\x120E\x120F\x1210\x1211\x1212\x1213\x1214\
             \\x1215\x1216\x1217\x1218\x1219\x121A\x121B\x121C\x121D\x121E\
             \\x121F\x1228\x1229\x122A\x122B\x122C\x122D\x122E\x122F\x1230\
             \\x1231\x1232\x1233\x1234\x1235\x1236\x1237\x1238\x1239\x123A\
             \\x123B\x123C\x123D\x123E\x123F\x1240\x1241\x1242\x1243\x1244\
             \\x1245\x1246\x1248\x124A\x124B\x124C\x124D\x1250\x1251\x1252\
             \\x1253\x1254\x1255\x1256\x1258\x125A\x125B\x125C\x125D\x1260\
             \\x1261\x1262\x1263\x1264\x1265\x1266\x1267\x1268\x1269\x126A\
             \\x126B\x126C\x126D\x126E\x126F\x1270\x1271\x1272\x1273\x1274\
             \\x1275\x1276\x1277\x1278\x1279\x127A\x127B\x127C\x127D\x127E\
             \\x127F\x1280\x1281\x1282\x1283\x1284\x1285\x1286\x1288\x128A\
             \\x128B\x128C\x128D\x1290\x1291\x1292\x1293\x1294\x1295\x1296\
             \\x1297\x1298\x1299\x129A\x129B\x129C\x129D\x129E\x129F\x12A0\
             \\x12A1\x12A2\x12A3\x12A4\x12A5\x12A6\x12A7\x12A8\x12A9\x12AA\
             \\x12AB\x12AC\x12AD\x12AE\x12B0\x12B2\x12B3\x12B4\x12B5\x12B8\
             \\x12B9\x12BA\x12BB\x12BC\x12BD\x12BE\x12C0\x12C2\x12C3\x12C4\
             \\x12C5\x12C8\x12C9\x12CA\x12CB\x12CC\x12CD\x12CE\x12D0\x12D1\
             \\x12D2\x12D3\x12D4\x12D5\x12D6\x12D8\x12D9\x12DA\x12DB\x12DC\
             \\x12DD\x12DE\x12DF\x12E0\x12E1\x12E2\x12E3\x12E4\x12E5\x12E6\
             \\x12E7\x12E8\x12E9\x12EA\x12EB\x12EC\x12ED\x12EE\x12F0\x12F1\
             \\x12F2\x12F3\x12F4\x12F5\x12F6\x12F7\x1300\x1301\x1302\x1303\
             \\x1304\x1305\x1306\x1307\x1308\x1309\x130A\x130B\x130C\x130D\
             \\x130E\x1310\x1312\x1313\x1314\x1315\x1320\x1321\x1322\x1323\
             \\x1324\x1325\x1326\x1327\x1328\x1329\x132A\x132B\x132C\x132D\
             \\x132E\x132F\x1338\x1339\x133A\x133B\x133C\x133D\x133E\x133F\
             \\x1348\x1349\x134A\x134B\x134C\x134D\x134E\x134F\x1350\x1351\
             \\x1352\x1353\x1354\x1355\x1356\x1357\x135F\x1360\x1361\x1362\
             \\x1363\x1364\x1365\x1366\x1367\x1368\x1369\x136A\x136B\x136C\
             \\x136D\x136E\x136F\x1370\x1371\x1372\x1373\x1374\x1375\x1376\
             \\x1377\x1378\x1379\x137A\x137B\x137C"
          , _exAuxiliary = Just $ Set.fromList
             "\x1207\x1220\x1221\x1222\x1223\x1224\x1225\x1226\x1227\x1247\
             \\x1287\x12AF\x12CF\x12EF\x12F8\x12F9\x12FA\x12FB\x12FC\x12FD\
             \\x12FE\x12FF\x130F\x1318\x1319\x131A\x131B\x131C\x131D\x131E\
             \\x131F\x1340\x1341\x1342\x1343\x1344\x1345\x1346\x1347\x1358\
             \\x1359\x135A\x1380\x1381\x1382\x1383\x1384\x1385\x1386\x1387\
             \\x1388\x1389\x138A\x138B\x138C\x138D\x138E\x138F\x1390\x1391\
             \\x1392\x1393\x1394\x1395\x1396\x1397\x1398\x1399\x2D80\x2D81\
             \\x2D82\x2D83\x2D84\x2D85\x2D86\x2D87\x2D88\x2D89\x2D8A\x2D8B\
             \\x2D8C\x2D8D\x2D8E\x2D8F\x2D90\x2D91\x2D92\x2D93\x2D94\x2D95\
             \\x2D96\x2DA0\x2DA1\x2DA2\x2DA3\x2DA4\x2DA5\x2DA6\x2DA8\x2DA9\
             \\x2DAA\x2DAB\x2DAC\x2DAD\x2DAE\x2DB0\x2DB1\x2DB2\x2DB3\x2DB4\
             \\x2DB5\x2DB6\x2DB8\x2DB9\x2DBA\x2DBB\x2DBC\x2DBD\x2DBE\x2DC0\
             \\x2DC1\x2DC2\x2DC3\x2DC4\x2DC5\x2DC6\x2DC8\x2DC9\x2DCA\x2DCB\
             \\x2DCC\x2DCD\x2DCE\x2DD0\x2DD1\x2DD2\x2DD3\x2DD4\x2DD5\x2DD6\
             \\x2DD8\x2DD9\x2DDA\x2DDB\x2DDC\x2DDD\x2DDE"
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("ti-ET", LanguageData
      { language = "ti"
      , script = Nothing
      , territory = Just "ET"
      , exemplars = Nothing
      }
    )
  , ("tk-TM", LanguageData
      { language = "tk"
      , script = Nothing
      , territory = Just "TM"
      , exemplars = Nothing
      }
    )
  , ("to-TO", LanguageData
      { language = "to"
      , script = Nothing
      , territory = Just "TO"
      , exemplars = Nothing
      }
    )
  , ("tr-CY", LanguageData
      { language = "tr"
      , script = Nothing
      , territory = Just "CY"
      , exemplars = Nothing
      }
    )
  , ("tr-TR", LanguageData
      { language = "tr"
      , script = Nothing
      , territory = Just "TR"
      , exemplars = Nothing
      }
    )
  , ("tt-RU", LanguageData
      { language = "tt"
      , script = Nothing
      , territory = Just "RU"
      , exemplars = Nothing
      }
    )
  , ("twq-NE", LanguageData
      { language = "twq"
      , script = Nothing
      , territory = Just "NE"
      , exemplars = Nothing
      }
    )
  , ("tzm-MA", LanguageData
      { language = "tzm"
      , script = Nothing
      , territory = Just "MA"
      , exemplars = Nothing
      }
    )
  , ("ug-CN", LanguageData
      { language = "ug"
      , script = Nothing
      , territory = Just "CN"
      , exemplars = Nothing
      }
    )
  , ("uk-UA", LanguageData
      { language = "uk"
      , script = Nothing
      , territory = Just "UA"
      , exemplars = Nothing
      }
    )
  , ("ur-IN", LanguageData
      { language = "ur"
      , script = Nothing
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("ur-PK", LanguageData
      { language = "ur"
      , script = Nothing
      , territory = Just "PK"
      , exemplars = Nothing
      }
    )
  , ("uz-Arab", LanguageData
      { language = "uz"
      , script = Just "Arab"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x621\x622\x623\x624\x626\x627\x628\x629\x62A\x62B\
             \\x62C\x62D\x62E\x62F\x630\x631\x632\x633\x634\x635\
             \\x636\x637\x638\x639\x63A\x641\x642\x644\x645\x646\
             \\x647\x648\x64B\x64C\x64D\x64E\x64F\x650\x651\x652\
             \\x654\x670\x67E\x686\x698\x6A9\x6AF\x6C7\x6C9\x6CC"
          , _exAuxiliary = Just $ Set.fromList
             "\x64A\x67C\x681\x685\x689\x693\x696\x69A\x6AB\x6BC\
             \\x6CD\x6D0\x200C\x200D\x200E\x200F"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x6F0\x31\x6F1\x32\x6F2\x33\x6F3\x34\x6F4\
             \\x35\x6F5\x36\x6F6\x37\x6F7\x38\x6F8\x39\x6F9\x609\x66A\x66B\x66C\x200E\
             \\x2011\x2030\x2212"
          }
      }
    )
  , ("uz-Cyrl", LanguageData
      { language = "uz"
      , script = Just "Cyrl"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x430\x431\x432\x433\x434\x435\x436\x437\x438\x439\
             \\x43A\x43B\x43C\x43D\x43E\x43F\x440\x441\x442\x443\
             \\x444\x445\x447\x448\x44A\x44D\x44E\x44F\x451\x45E\
             \\x493\x49B\x4B3"
          , _exAuxiliary = Just $ Set.fromList
             "\x446\x449\x44B\x44C"
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x30\x31\x32\x33\x34\x35\
             \\x36\x37\x38\x39\xA0\x2011\x2030"
          }
      }
    )
  , ("uz-Latn", LanguageData
      { language = "uz"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("vai-Latn", LanguageData
      { language = "vai"
      , script = Just "Latn"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x73\x74\
             \\x75\x76\x77\x78\x79\x7A\xE1\xE3\xE9\xED\
             \\xF3\xF5\xFA\x129\x14B\x169\x253\x254\x254\x301\x254\x303\
             \\x257\x25B\x25B\x301\x25B\x303\x1EBD"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030"
          }
      }
    )
  , ("vai-Vaii", LanguageData
      { language = "vai"
      , script = Just "Vaii"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("vi-VN", LanguageData
      { language = "vi"
      , script = Nothing
      , territory = Just "VN"
      , exemplars = Nothing
      }
    )
  , ("vun-TZ", LanguageData
      { language = "vun"
      , script = Nothing
      , territory = Just "TZ"
      , exemplars = Nothing
      }
    )
  , ("wae-CH", LanguageData
      { language = "wae"
      , script = Nothing
      , territory = Just "CH"
      , exemplars = Nothing
      }
    )
  , ("wo-SN", LanguageData
      { language = "wo"
      , script = Nothing
      , territory = Just "SN"
      , exemplars = Nothing
      }
    )
  , ("xh-ZA", LanguageData
      { language = "xh"
      , script = Nothing
      , territory = Just "ZA"
      , exemplars = Nothing
      }
    )
  , ("xog-UG", LanguageData
      { language = "xog"
      , script = Nothing
      , territory = Just "UG"
      , exemplars = Nothing
      }
    )
  , ("yav-CM", LanguageData
      { language = "yav"
      , script = Nothing
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("yi-001", LanguageData
      { language = "yi"
      , script = Nothing
      , territory = Just "001"
      , exemplars = Nothing
      }
    )
  , ("yo-BJ", LanguageData
      { language = "yo"
      , script = Nothing
      , territory = Just "BJ"
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x61\x62\x64\x65\x66\x67\x67\x62\x68\x69\x6A\
             \\x6B\x6C\x6D\x6E\x6F\x70\x72\x73\x73\x68\x74\
             \\x75\x77\x79\xE0\xE1\xE8\xE9\xEC\xED\xF2\
             \\xF3\xF9\xFA\x254\x254\x300\x254\x301\x25B\x25B\x300\x25B\x301"
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Nothing
          }
      }
    )
  , ("yo-NG", LanguageData
      { language = "yo"
      , script = Nothing
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("yue-Hans", LanguageData
      { language = "yue"
      , script = Just "Hans"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x4E00\x4E01\x4E03\x4E07\x4E08\x4E09\x4E0A\x4E0B\x4E0C\x4E0D\
             \\x4E0E\x4E11\x4E13\x4E14\x4E16\x4E18\x4E19\x4E1A\x4E1C\x4E1D\
             \\x4E22\x4E24\x4E25\x4E2A\x4E2D\x4E30\x4E32\x4E34\x4E38\x4E39\
             \\x4E3A\x4E3B\x4E3D\x4E3E\x4E43\x4E45\x4E48\x4E49\x4E4B\x4E4C\
             \\x4E4E\x4E4F\x4E50\x4E54\x4E56\x4E58\x4E59\x4E5D\x4E5F\x4E60\
             \\x4E61\x4E66\x4E70\x4E71\x4E86\x4E88\x4E89\x4E8B\x4E8C\x4E8E\
             \\x4E8F\x4E91\x4E92\x4E94\x4E95\x4E9A\x4E9B\x4EA1\x4EA4\x4EA5\
             \\x4EA6\x4EA7\x4EA8\x4EAB\x4EAC\x4EAE\x4EB2\x4EBA\x4EBF\x4EC0\
             \\x4EC1\x4EC5\x4EC7\x4ECA\x4ECB\x4ECD\x4ECE\x4ED1\x4ED4\x4ED6\
             \\x4ED8\x4ED9\x4EE3\x4EE4\x4EE5\x4EEA\x4EEC\x4EF0\x4EF2\x4EF6\
             \\x4EF7\x4EFB\x4EFD\x4F01\x4F0A\x4F0D\x4F10\x4F11\x4F17\x4F18\
             \\x4F19\x4F1A\x4F1F\x4F20\x4F24\x4F26\x4F2F\x4F30\x4F34\x4F38\
             \\x4F3C\x4F3D\x4F46\x4F49\x4F4D\x4F4E\x4F4F\x4F53\x4F55\x4F59\
             \\x4F5B\x4F5C\x4F60\x4F69\x4F73\x4F7F\x4F8B\x4F9B\x4F9D\x4FA0\
             \\x4FA6\x4FA7\x4FA8\x4FAF\x4FB5\x4FBF\x4FC3\x4FC4\x4FCA\x4FD7\
             \\x4FDD\x4FE1\x4FEE\x4FFE\x500D\x5012\x5019\x501A\x501F\x503C\
             \\x503E\x5047\x504F\x505A\x505C\x5065\x5076\x5077\x50A3\x50B2\
             \\x50BB\x50CF\x50E7\x5112\x513F\x5141\x5143\x5144\x5145\x5148\
             \\x5149\x514B\x514D\x5154\x515A\x5165\x5168\x516B\x516C\x516D\
             \\x516E\x5170\x5171\x5173\x5174\x5175\x5176\x5177\x5178\x5179\
             \\x517B\x517C\x517D\x5185\x5188\x518C\x518D\x5192\x5199\x519B\
             \\x519C\x51A0\x51AC\x51B0\x51B2\x51B3\x51B5\x51B7\x51C0\x51C6\
             \\x51C9\x51CC\x51CF\x51DD\x51E0\x51E1\x51E4\x51ED\x51EF\x51F0\
             \\x51F6\x51FA\x51FB\x51FD\x5200\x5206\x5207\x520A\x5212\x5217\
             \\x5218\x5219\x521A\x521B\x521D\x5220\x5224\x5229\x522B\x5230\
             \\x5236\x5237\x523A\x523B\x524C\x524D\x5251\x5267\x5269\x526A\
             \\x526F\x5272\x529B\x529D\x529E\x529F\x52A0\x52A1\x52A8\x52A9\
             \\x52AA\x52AB\x52B1\x52B2\x52B3\x52BF\x52C7\x52C9\x52D2\x52E4\
             \\x52FF\x5305\x5308\x5316\x5317\x5339\x533A\x533B\x5341\x5343\
             \\x5347\x5348\x534A\x534E\x534F\x5352\x5353\x5355\x5356\x5357\
             \\x535A\x535C\x5360\x5361\x5362\x5367\x536B\x536F\x5370\x5371\
             \\x5373\x5374\x5377\x5382\x5384\x5385\x5386\x5389\x538B\x538C\
             \\x5398\x539A\x539F\x53BB\x53BF\x53C2\x53C8\x53CA\x53CB\x53CC\
             \\x53CD\x53D1\x53D4\x53D6\x53D7\x53D8\x53D9\x53E3\x53E4\x53E5\
             \\x53E6\x53EA\x53EB\x53EC\x53ED\x53EF\x53F0\x53F2\x53F3\x53F6\
             \\x53F7\x53F8\x53F9\x5403\x5404\x5408\x5409\x540A\x540C\x540D\
             \\x540E\x5410\x5411\x5413\x5415\x5417\x541B\x541D\x541E\x541F\
             \\x5420\x5426\x5427\x542B\x542C\x542F\x5434\x5435\x5438\x5439\
             \\x543E\x5440\x5446\x544A\x5458\x545C\x5462\x5468\x5473\x5475\
             \\x547C\x547D\x548C\x5496\x54A4\x54A6\x54A7\x54AA\x54AC\x54B1\
             \\x54C0\x54C1\x54C7\x54C8\x54C9\x54CD\x54CE\x54E5\x54E6\x54E9\
             \\x54EA\x54ED\x54F2\x5509\x5510\x5514\x552C\x552E\x552F\x5531\
             \\x5537\x5546\x554A\x5561\x5565\x5566\x556A\x5570\x5580\x5582\
             \\x5584\x5587\x558A\x5594\x559C\x559D\x55B5\x55B7\x55E8\x55EF\
             \\x5609\x561B\x5634\x563B\x563F\x5668\x56DB\x56DE\x56E0\x56E2\
             \\x56ED\x56F0\x56F4\x56FA\x56FD\x56FE\x5706\x5708\x571C\x571F\
             \\x5723\x5728\x572D\x5730\x573A\x573E\x5740\x5747\x574E\x574F\
             \\x5750\x5757\x575A\x575B\x575C\x5761\x5764\x5766\x576A\x5782\
             \\x5783\x578B\x5792\x57C3\x57CE\x57D4\x57DF\x57F9\x57FA\x5802\
             \\x5806\x5815\x5821\x582A\x5854\x585E\x586B\x5883\x5899\x589E\
             \\x58A8\x58C1\x58EB\x58EC\x58EE\x58F0\x58F3\x5904\x5907\x590D\
             \\x590F\x5915\x5916\x591A\x591C\x591F\x5927\x5929\x592A\x592B\
             \\x592E\x5931\x5934\x5937\x5938\x5939\x593A\x5947\x5948\x5949\
             \\x594B\x594E\x594F\x5951\x5954\x5956\x5957\x5965\x5973\x5974\
             \\x5976\x5979\x597D\x5982\x5986\x5987\x5988\x5999\x59A5\x59A8\
             \\x59AE\x59B3\x59B9\x59BB\x59C6\x59CB\x59D0\x59D1\x59D3\x59D4\
             \\x59FF\x5A01\x5A03\x5A04\x5A18\x5A31\x5A46\x5A5A\x5A92\x5ACC\
             \\x5AE9\x5B50\x5B54\x5B57\x5B58\x5B59\x5B5D\x5B5F\x5B63\x5B64\
             \\x5B66\x5B69\x5B81\x5B83\x5B85\x5B87\x5B88\x5B89\x5B8B\x5B8C\
             \\x5B8F\x5B97\x5B98\x5B99\x5B9A\x5B9B\x5B9C\x5B9D\x5B9E\x5BA0\
             \\x5BA1\x5BA2\x5BA3\x5BA4\x5BAA\x5BAB\x5BB3\x5BB6\x5BB9\x5BBD\
             \\x5BBE\x5BBF\x5BC2\x5BC4\x5BC5\x5BC6\x5BCC\x5BD2\x5BDD\x5BDE\
             \\x5BDF\x5BE8\x5BEE\x5BF9\x5BFB\x5BFC\x5BFF\x5C01\x5C04\x5C06\
             \\x5C0A\x5C0F\x5C11\x5C14\x5C16\x5C18\x5C1A\x5C1D\x5C24\x5C31\
             \\x5C3A\x5C3C\x5C3D\x5C3E\x5C40\x5C41\x5C42\x5C45\x5C4A\x5C4B\
             \\x5C4F\x5C55\x5C5E\x5C60\x5C71\x5C81\x5C82\x5C9A\x5C9B\x5CA9\
             \\x5CAD\x5CB8\x5CE1\x5CF0\x5D07\x5D34\x5DDD\x5DDE\x5DE1\x5DE5\
             \\x5DE6\x5DE7\x5DE8\x5DEB\x5DEE\x5DF1\x5DF2\x5DF3\x5DF4\x5DF7\
             \\x5E01\x5E02\x5E03\x5E05\x5E08\x5E0C\x5E10\x5E15\x5E16\x5E1B\
             \\x5E1D\x5E26\x5E2D\x5E2E\x5E38\x5E3D\x5E45\x5E55\x5E72\x5E73\
             \\x5E74\x5E76\x5E78\x5E7B\x5E7C\x5E7D\x5E7F\x5E84\x5E86\x5E87\
             \\x5E8A\x5E8F\x5E93\x5E94\x5E95\x5E97\x5E9A\x5E9C\x5E9F\x5EA6\
             \\x5EA7\x5EAD\x5EB7\x5EB8\x5EC9\x5ED6\x5EF6\x5EF7\x5EFA\x5F00\
             \\x5F02\x5F03\x5F04\x5F0F\x5F15\x5F17\x5F18\x5F1F\x5F20\x5F25\
             \\x5F26\x5F2F\x5F31\x5F39\x5F3A\x5F4A\x5F52\x5F53\x5F55\x5F5D\
             \\x5F62\x5F66\x5F69\x5F6C\x5F6D\x5F70\x5F71\x5F79\x5F7B\x5F7C\
             \\x5F80\x5F81\x5F84\x5F85\x5F88\x5F8B\x5F90\x5F92\x5F97\x5FAE\
             \\x5FB7\x5FC3\x5FC5\x5FC6\x5FCC\x5FCD\x5FD7\x5FD8\x5FD9\x5FE0\
             \\x5FE7\x5FEB\x5FF5\x5FFD\x6000\x6001\x600E\x6012\x6015\x6016\
             \\x601C\x601D\x6021\x6025\x6027\x6028\x602A\x603B\x604B\x6050\
             \\x6052\x6062\x6068\x6069\x606D\x606F\x6070\x6076\x607C\x6089\
             \\x6094\x609F\x60A0\x60A6\x60A8\x60B2\x60C5\x60CA\x60D1\x60DC\
             \\x60E0\x60E7\x60E8\x60EF\x60F3\x60F9\x6101\x6108\x6109\x610F\
             \\x611A\x611F\x613F\x6148\x6155\x6162\x6167\x6170\x61BE\x61C2\
             \\x61D2\x6208\x620A\x620C\x620F\x6210\x6211\x6212\x6216\x6218\
             \\x622A\x6234\x6237\x623F\x6240\x6241\x6247\x624B\x624D\x624E\
             \\x6253\x6258\x6263\x6265\x6267\x6269\x626B\x626C\x626D\x626F\
             \\x6270\x6279\x627E\x627F\x6280\x6284\x628A\x6293\x6295\x6297\
             \\x6298\x629B\x62A2\x62A4\x62A5\x62AB\x62AC\x62B1\x62B5\x62B9\
             \\x62BD\x62C5\x62C6\x62C9\x62CD\x62CF\x62D2\x62D4\x62D6\x62DB\
             \\x62DC\x62DF\x62E5\x62E8\x62E9\x62EC\x62F3\x62FC\x62FE\x62FF\
             \\x6301\x6302\x6307\x6309\x6311\x6316\x631D\x6321\x6324\x6325\
             \\x632A\x632F\x633A\x6350\x6355\x635F\x6361\x6362\x636E\x6377\
             \\x6388\x6389\x638C\x6392\x63A2\x63A5\x63A7\x63A8\x63AA\x63B8\
             \\x63CF\x63D0\x63D2\x63E1\x63F4\x641C\x641E\x642C\x642D\x6444\
             \\x6446\x6447\x6458\x6469\x6478\x6491\x6492\x649E\x64AD\x64CD\
             \\x64CE\x64E6\x652F\x6536\x6539\x653B\x653E\x653F\x6545\x6548\
             \\x654C\x654D\x654F\x6551\x6559\x655D\x6562\x6563\x6566\x656C\
             \\x6570\x6574\x6587\x658B\x6590\x6597\x6599\x65AD\x65AF\x65B0\
             \\x65B9\x65BD\x65C1\x65C5\x65CB\x65CF\x65D7\x65E0\x65E2\x65E5\
             \\x65E6\x65E7\x65E9\x65ED\x65F6\x65FA\x6602\x6606\x660C\x660E\
             \\x660F\x6613\x661F\x6620\x6625\x6628\x662D\x662F\x663E\x664B\
             \\x6652\x6653\x665A\x6668\x666E\x666F\x6674\x6676\x667A\x6682\
             \\x6691\x6696\x6697\x66B4\x66F0\x66F2\x66F4\x66FC\x66FE\x66FF\
             \\x6700\x6708\x6709\x670B\x670D\x6717\x671B\x671D\x671F\x6728\
             \\x672A\x672B\x672C\x672D\x672F\x6731\x6735\x673A\x6740\x6742\
             \\x6743\x6749\x674E\x6750\x6751\x675C\x675F\x6761\x6765\x6768\
             \\x676F\x6770\x677E\x677F\x6781\x6784\x6790\x6797\x679C\x679D\
             \\x67A2\x67AA\x67AB\x67B6\x67CF\x67D0\x67D3\x67D4\x67E5\x67EC\
             \\x67EF\x67F3\x67F4\x6807\x680F\x6811\x6821\x6837\x6838\x6839\
             \\x683C\x6843\x6848\x684C\x6851\x6863\x6865\x6881\x6885\x68A6\
             \\x68A8\x68AF\x68B0\x68B5\x68C0\x68C9\x68CB\x68D2\x68DA\x68EE\
             \\x6905\x690D\x6930\x695A\x697C\x6982\x699C\x6A21\x6A2A\x6A80\
             \\x6B21\x6B22\x6B23\x6B27\x6B32\x6B3A\x6B3E\x6B49\x6B4C\x6B62\
             \\x6B63\x6B64\x6B65\x6B66\x6B7B\x6B8A\x6B8B\x6BB5\x6BC1\x6BC5\
             \\x6BCD\x6BCF\x6BD2\x6BD4\x6BD5\x6BDB\x6BEB\x6C0F\x6C11\x6C14\
             \\x6C34\x6C38\x6C42\x6C49\x6C57\x6C5D\x6C5F\x6C60\x6C61\x6C64\
             \\x6C6A\x6C76\x6C7D\x6C83\x6C88\x6C89\x6C99\x6C9F\x6CA1\x6CA7\
             \\x6CB3\x6CB9\x6CBB\x6CBF\x6CC4\x6CC9\x6CCA\x6CD5\x6CE1\x6CE2\
             \\x6CE5\x6CE8\x6CEA\x6CF0\x6CF3\x6CFD\x6CFE\x6D01\x6D0B\x6D17\
             \\x6D1B\x6D1E\x6D2A\x6D32\x6D3B\x6D3D\x6D3E\x6D41\x6D45\x6D4B\
             \\x6D4E\x6D4F\x6D53\x6D66\x6D69\x6D6A\x6D6E\x6D77\x6D82\x6D88\
             \\x6D89\x6D9B\x6DA8\x6DAF\x6DB2\x6DB5\x6DD1\x6DE1\x6DF1\x6DF7\
             \\x6E05\x6E10\x6E21\x6E29\x6E2F\x6E38\x6E56\x6E7E\x6E90\x6EAA\
             \\x6ECB\x6ED1\x6EDA\x6EE1\x6EE5\x6EE8\x6EF4\x6F02\x6F0F\x6F14\
             \\x6F20\x6F2B\x6F58\x6F5C\x6F6E\x6FB3\x6FC0\x704C\x706B\x706D\
             \\x706F\x7070\x7075\x707E\x7089\x708E\x70AE\x70B8\x70B9\x70C2\
             \\x70C8\x70DF\x70E4\x70E6\x70E7\x70ED\x7126\x7136\x715E\x7167\
             \\x718A\x719F\x71C3\x7206\x722A\x722C\x7231\x7235\x7236\x7237\
             \\x7238\x723D\x7247\x7248\x724C\x7259\x725B\x7260\x7267\x7269\
             \\x7272\x7275\x7279\x727A\x72AF\x72B6\x72B9\x72C2\x72D0\x72D7\
             \\x72E0\x72EC\x72EE\x72F1\x72FC\x731B\x731C\x732A\x732B\x732E\
             \\x7334\x7384\x7387\x7389\x738B\x739B\x73A9\x73AB\x73AF\x73B0\
             \\x73B2\x73BB\x73CA\x73CD\x73E0\x73E5\x73ED\x7403\x7406\x7409\
             \\x742A\x7434\x7459\x745C\x745E\x745F\x7470\x7476\x74DC\x74E6\
             \\x74F6\x7518\x751A\x751C\x751F\x7528\x7530\x7531\x7532\x7533\
             \\x7535\x7537\x7538\x753B\x754C\x7559\x7565\x756A\x7586\x758F\
             \\x7591\x7597\x75AF\x75BC\x75C5\x75D5\x75DB\x75F4\x7678\x767B\
             \\x767D\x767E\x7684\x7686\x7687\x76AE\x76CA\x76D1\x76D6\x76D7\
             \\x76D8\x76DB\x76DF\x76EE\x76F2\x76F4\x76F8\x76FC\x76FE\x7701\
             \\x7709\x770B\x771F\x7720\x773C\x7740\x775B\x7761\x7763\x77A7\
             \\x77DB\x77E3\x77E5\x77ED\x77F3\x77F6\x7801\x7802\x780D\x7814\
             \\x7834\x7840\x7855\x786C\x786E\x788D\x788E\x7897\x789F\x78A7\
             \\x78B0\x78C1\x78E8\x793A\x793C\x793E\x7956\x795A\x795B\x795D\
             \\x795E\x7965\x7968\x796F\x7978\x7981\x7984\x7985\x798F\x79BB\
             \\x79C0\x79C1\x79CB\x79CD\x79D1\x79D2\x79D8\x79DF\x79E4\x79E6\
             \\x79EF\x79F0\x79FB\x7A0B\x7A0D\x7A0E\x7A23\x7A33\x7A3F\x7A46\
             \\x7A76\x7A77\x7A79\x7A7A\x7A7F\x7A81\x7A97\x7A9D\x7AAD\x7ACB\
             \\x7AD9\x7ADE\x7ADF\x7AE0\x7AE5\x7AEF\x7AF9\x7B11\x7B14\x7B1B\
             \\x7B26\x7B28\x7B2C\x7B49\x7B4B\x7B51\x7B54\x7B56\x7B79\x7B7E\
             \\x7B80\x7B97\x7BA1\x7BAB\x7BAD\x7BB1\x7BC7\x7BEE\x7C3F\x7C4D\
             \\x7C73\x7C7B\x7C89\x7C97\x7CA4\x7CBE\x7CCA\x7CD5\x7CDF\x7CFB\
             \\x7D20\x7D22\x7D27\x7D2B\x7D2F\x7E41\x7EA0\x7EA2\x7EA6\x7EA7\
             \\x7EAA\x7EAC\x7EAF\x7EB2\x7EB3\x7EB5\x7EB7\x7EB8\x7EBD\x7EBF\
             \\x7EC3\x7EC4\x7EC6\x7EC7\x7EC8\x7ECD\x7ECF\x7ED3\x7ED5\x7ED8\
             \\x7ED9\x7EDC\x7EDD\x7EDF\x7EE7\x7EE9\x7EEA\x7EED\x7EF4\x7EFC\
             \\x7EFF\x7F05\x7F13\x7F16\x7F18\x7F1A\x7F29\x7F2A\x7F34\x7F38\
             \\x7F3A\x7F51\x7F55\x7F57\x7F5A\x7F62\x7F6A\x7F6E\x7F72\x7F8A\
             \\x7F8E\x7F9E\x7FA4\x7FBD\x7FC1\x7FD4\x7FD8\x7FF0\x7FFB\x7FFC\
             \\x8000\x8001\x8003\x8005\x800C\x800D\x8010\x8017\x8033\x8036\
             \\x803B\x804A\x804C\x8054\x805A\x806A\x8089\x809A\x80A1\x80A5\
             \\x80A9\x80AF\x80B2\x80C6\x80CC\x80CE\x80D6\x80DC\x80DE\x80E1\
             \\x80F8\x80FD\x8106\x8111\x811A\x8131\x8138\x814A\x8153\x8154\
             \\x8170\x817F\x81E3\x81EA\x81ED\x81F3\x81F4\x820C\x820D\x8212\
             \\x821E\x821F\x822A\x822C\x8230\x8239\x826F\x8272\x827A\x827E\
             \\x8282\x829D\x82A6\x82AC\x82B1\x82B3\x82CD\x82CF\x82E5\x82E6\
             \\x82F1\x8303\x8305\x832B\x8336\x8349\x8350\x8352\x8363\x836F\
             \\x8377\x837C\x8389\x838E\x83AB\x83B1\x83B2\x83B7\x83DC\x83E9\
             \\x83F2\x8404\x8424\x8425\x8427\x8428\x843D\x845B\x8461\x8482\
             \\x848B\x8499\x84B2\x84DD\x8515\x8521\x8584\x85AA\x85CF\x85E4\
             \\x864E\x8651\x865A\x866B\x867D\x86C7\x86CB\x86D9\x86EE\x8702\
             \\x871C\x874E\x8776\x878D\x87F9\x8840\x884C\x8857\x8861\x8863\
             \\x8865\x8868\x888B\x88AB\x88C1\x88C2\x88C5\x88D5\x88E4\x897F\
             \\x8981\x8986\x89C1\x89C2\x89C4\x89C6\x89C8\x89C9\x89D2\x89E3\
             \\x89E6\x8A00\x8A89\x8A93\x8B66\x8BA1\x8BA2\x8BA4\x8BA8\x8BA9\
             \\x8BAD\x8BAE\x8BAF\x8BB0\x8BB2\x8BB7\x8BB8\x8BBA\x8BBE\x8BBF\
             \\x8BC1\x8BC4\x8BC6\x8BC9\x8BCD\x8BD1\x8BD5\x8BD7\x8BDA\x8BDD\
             \\x8BDE\x8BE2\x8BE5\x8BE6\x8BED\x8BEF\x8BF4\x8BF7\x8BF8\x8BFA\
             \\x8BFB\x8BFE\x8C01\x8C03\x8C05\x8C08\x8C0A\x8C0B\x8C13\x8C1A\
             \\x8C22\x8C31\x8C37\x8C46\x8C61\x8C6A\x8C8C\x8D1D\x8D1E\x8D1F\
             \\x8D21\x8D22\x8D23\x8D24\x8D25\x8D27\x8D28\x8D2A\x8D2D\x8D2F\
             \\x8D31\x8D34\x8D35\x8D39\x8D3A\x8D3E\x8D44\x8D4B\x8D4C\x8D4F\
             \\x8D50\x8D56\x8D5A\x8D5B\x8D5E\x8D60\x8D62\x8D64\x8D6B\x8D70\
             \\x8D75\x8D76\x8D77\x8D85\x8D8A\x8D8B\x8DA3\x8DB3\x8DC3\x8DCC\
             \\x8DCE\x8DD1\x8DDD\x8DDF\x8DEF\x8DF3\x8E0F\x8E22\x8E2A\x8EAB\
             \\x8EB2\x8F66\x8F68\x8F69\x8F6C\x8F6E\x8F6F\x8F70\x8F7B\x8F7D\
             \\x8F83\x8F85\x8F86\x8F88\x8F89\x8F91\x8F93\x8F9B\x8F9E\x8FA8\
             \\x8FA9\x8FB0\x8FB1\x8FB9\x8FBE\x8FC1\x8FC5\x8FC7\x8FC8\x8FCE\
             \\x8FD0\x8FD1\x8FD4\x8FD8\x8FD9\x8FDB\x8FDC\x8FDD\x8FDE\x8FDF\
             \\x8FE6\x8FEA\x8FEB\x8FF0\x8FF7\x8FF9\x8FFD\x9000\x9001\x9002\
             \\x9003\x9006\x9009\x900A\x900F\x9010\x9014\x901A\x901B\x901D\
             \\x901F\x9020\x9022\x9038\x903B\x903C\x9047\x904D\x9053\x9057\
             \\x9065\x906D\x906E\x907F\x9080\x90A3\x90A6\x90AA\x90AE\x90B1\
             \\x90BB\x90C1\x90CE\x90D1\x90E8\x90ED\x90FD\x9102\x9149\x914D\
             \\x9152\x9177\x9178\x9189\x9192\x91C7\x91CA\x91CC\x91CD\x91CE\
             \\x91CF\x91D1\x9274\x9488\x9493\x949F\x94A2\x94A6\x94B1\x94B5\
             \\x94C1\x94C3\x94DC\x94E2\x94ED\x94F6\x9500\x9501\x9505\x950B\
             \\x9510\x9519\x9521\x9526\x952E\x9547\x9551\x955C\x957F\x95E8\
             \\x95EA\x95ED\x95EE\x95F0\x95F2\x95F4\x95F7\x95F9\x95FB\x9601\
             \\x9605\x9607\x9610\x9614\x9617\x961F\x9632\x9633\x9634\x9635\
             \\x9636\x963B\x963F\x9640\x9644\x9645\x9646\x9648\x964D\x9650\
             \\x9662\x9664\x9669\x966A\x9675\x9676\x9677\x9686\x968F\x9690\
             \\x9694\x969C\x96BE\x96C4\x96C5\x96C6\x96C9\x96E8\x96EA\x96F3\
             \\x96F6\x96F7\x96FE\x9700\x9707\x970D\x9732\x9738\x9739\x9752\
             \\x9756\x9759\x975E\x9760\x9762\x9769\x977C\x978B\x9791\x97E6\
             \\x97E9\x97F3\x97F5\x981E\x9875\x9876\x9879\x987A\x987B\x987D\
             \\x987E\x987F\x9884\x9886\x9887\x9891\x9897\x9898\x989C\x989D\
             \\x98CE\x98D8\x98DE\x98DF\x9910\x996D\x996E\x9970\x9971\x997C\
             \\x9986\x9996\x9999\x9A6C\x9A71\x9A76\x9A7B\x9A7E\x9A82\x9A8C\
             \\x9A91\x9A97\x9A9A\x9AA8\x9AD8\x9B3C\x9B41\x9B42\x9B45\x9B54\
             \\x9C7C\x9C81\x9C9C\x9E1F\x9E21\x9E23\x9E3F\x9E45\x9E70\x9E7F\
             \\x9EA6\x9EBB\x9EC4\x9ECE\x9ED1\x9ED8\x9F13\x9F20\x9F3B\x9F50\
             \\x9F7F\x9F84\x9F99\x9F9F"
          , _exAuxiliary = Just $ Set.fromList
             "\x4E4D\x4EC2\x4F0F\x4F50\x4FA3\x50F3\x5146\x5151\x5238\x52CB\
             \\x5351\x535E\x5480\x5605\x5824\x588E\x58E4\x5B5C\x5C7F\x5CC7\
             \\x5DFD\x659C\x6619\x663C\x6817\x6954\x6D51\x6D85\x6E58\x6F8E\
             \\x707F\x72C4\x7433\x745A\x752B\x7891\x7901\x7EF0\x8292\x82D7\
             \\x8328\x8335\x84EC\x86A9\x86F0\x8700\x88D8\x8C2C\x8D63\x914B\
             \\x95FD\x9647\x971C"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\x5F\
             \\x7B\x7D\xA7\xB7\x2010\x2011\x2013\x2014\x2015\x2016\
             \\x2018\x2019\x201C\x201D\x2025\x2026\x2030\x2032\x2033\x2035\
             \\x203B\x3001\x3002\x3003\x3008\x3009\x300A\x300B\x300C\x300D\
             \\x300E\x300F\x3010\x3011\x3014\x3015\x3016\x3017\x301D\x301E\
             \\xFE30\xFE31\xFE33\xFE34\xFE35\xFE36\xFE37\xFE38\xFE39\xFE3A\xFE3B\
             \\xFE3C\xFE3D\xFE3E\xFE3F\xFE40\xFE41\xFE42\xFE43\xFE44\xFE49\xFE4A\xFE4B\xFE4C\
             \\xFE4D\xFE4E\xFE4F\xFE50\xFE51\xFE52\xFE54\xFE55\xFE56\xFE57\xFE59\xFE5A\
             \\xFE5B\xFE5C\xFE5D\xFE5E\xFE5F\xFE60\xFE61\xFE63\xFE68\xFE6A\
             \\xFE6B\xFF01\xFF02\xFF03\xFF05\xFF06\xFF07\xFF08\xFF09\xFF0A\
             \\xFF0C\xFF0D\xFF0E\xFF0F\xFF1A\xFF1B\xFF1F\xFF20\xFF3B\xFF3C\
             \\xFF3D\xFF3F\xFF5B\xFF5D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030\x3007\x4E00\x4E03\
             \\x4E09\x4E5D\x4E8C\x4E94\x516B\x516D\x56DB"
          }
      }
    )
  , ("yue-Hant", LanguageData
      { language = "yue"
      , script = Just "Hant"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("zgh-MA", LanguageData
      { language = "zgh"
      , script = Nothing
      , territory = Just "MA"
      , exemplars = Nothing
      }
    )
  , ("zh-Hans", LanguageData
      { language = "zh"
      , script = Just "Hans"
      , territory = Nothing
      , exemplars = Nothing
      }
    )
  , ("zh-Hant", LanguageData
      { language = "zh"
      , script = Just "Hant"
      , territory = Nothing
      , exemplars = Just Exemplars
          { _exMain = Just $ Set.fromList
             "\x4E00\x4E01\x4E03\x4E08\x4E09\x4E0A\x4E0B\x4E0C\x4E0D\x4E11\
             \\x4E14\x4E16\x4E18\x4E19\x4E1F\x4E26\x4E2D\x4E32\x4E38\x4E39\
             \\x4E3B\x4E43\x4E45\x4E48\x4E4B\x4E4E\x4E4F\x4E56\x4E58\x4E59\
             \\x4E5D\x4E5F\x4E7E\x4E82\x4E86\x4E88\x4E8B\x4E8C\x4E8E\x4E91\
             \\x4E92\x4E94\x4E95\x4E9B\x4E9E\x4EA1\x4EA4\x4EA5\x4EA6\x4EA8\
             \\x4EAB\x4EAC\x4EAE\x4EBA\x4EC0\x4EC1\x4EC7\x4ECA\x4ECB\x4ECD\
             \\x4ED4\x4ED6\x4ED8\x4ED9\x4EE3\x4EE4\x4EE5\x4EF0\x4EF2\x4EF6\
             \\x4EFB\x4EFD\x4F01\x4F0A\x4F0D\x4F10\x4F11\x4F19\x4F2F\x4F30\
             \\x4F34\x4F38\x4F3C\x4F3D\x4F46\x4F48\x4F49\x4F4D\x4F4E\x4F4F\
             \\x4F54\x4F55\x4F59\x4F5B\x4F5C\x4F60\x4F69\x4F73\x4F7F\x4F86\
             \\x4F8B\x4F9B\x4F9D\x4FAF\x4FB5\x4FBF\x4FC2\x4FC3\x4FC4\x4FCA\
             \\x4FD7\x4FDD\x4FE0\x4FE1\x4FEE\x4FF1\x4FFE\x500B\x500D\x5011\
             \\x5012\x5019\x501A\x501F\x502B\x503C\x5047\x5049\x504F\x505A\
             \\x505C\x5065\x5074\x5075\x5076\x5077\x5091\x5099\x50A2\x50A3\
             \\x50B2\x50B3\x50B7\x50BB\x50BE\x50C5\x50CF\x50D1\x50E7\x50F9\
             \\x5100\x5104\x5112\x5118\x512A\x5141\x5143\x5144\x5145\x5147\
             \\x5148\x5149\x514B\x514D\x5152\x5154\x5165\x5167\x5168\x5169\
             \\x516B\x516C\x516D\x516E\x5171\x5175\x5176\x5177\x5178\x517C\
             \\x518A\x518D\x5192\x51A0\x51AC\x51B0\x51B7\x51C6\x51CC\x51DD\
             \\x51E1\x51F0\x51F1\x51FA\x51FD\x5200\x5206\x5207\x520A\x5217\
             \\x521D\x5224\x5225\x5229\x522A\x5230\x5236\x5237\x523A\x523B\
             \\x5247\x524C\x524D\x525B\x5269\x526A\x526F\x5272\x5275\x5283\
             \\x5287\x5289\x528D\x529B\x529F\x52A0\x52A9\x52AA\x52AB\x52C1\
             \\x52C7\x52C9\x52D2\x52D5\x52D9\x52DD\x52DE\x52E2\x52E4\x52F5\
             \\x52F8\x52FF\x5305\x5308\x5316\x5317\x5339\x5340\x5341\x5343\
             \\x5347\x5348\x534A\x5352\x5353\x5354\x5357\x535A\x535C\x5361\
             \\x536F\x5370\x5371\x5373\x5377\x537B\x5384\x5398\x539A\x539F\
             \\x53AD\x53B2\x53BB\x53C3\x53C8\x53CA\x53CB\x53CD\x53D4\x53D6\
             \\x53D7\x53E3\x53E4\x53E5\x53E6\x53EA\x53EB\x53EC\x53ED\x53EF\
             \\x53F0\x53F2\x53F3\x53F8\x5403\x5404\x5408\x5409\x540A\x540C\
             \\x540D\x540E\x5410\x5411\x5412\x541B\x541D\x541E\x541F\x5420\
             \\x5426\x5427\x542B\x5433\x5435\x5438\x5439\x543E\x5440\x5442\
             \\x5446\x544A\x5462\x5468\x5473\x5475\x547C\x547D\x548C\x5496\
             \\x54A6\x54A7\x54AA\x54AC\x54B1\x54C0\x54C1\x54C7\x54C8\x54C9\
             \\x54CE\x54E1\x54E5\x54E6\x54E9\x54EA\x54ED\x54F2\x5509\x5510\
             \\x5514\x552C\x552E\x552F\x5531\x5537\x5538\x5546\x554A\x554F\
             \\x555F\x5561\x5565\x5566\x556A\x5580\x5582\x5584\x5587\x558A\
             \\x5594\x559C\x559D\x55AC\x55AE\x55B5\x55CE\x55DA\x55E8\x55EF\
             \\x5606\x5609\x5617\x561B\x5634\x563B\x563F\x5668\x5674\x5687\
             \\x56B4\x56C9\x56DB\x56DE\x56E0\x56F0\x56FA\x5708\x570B\x570D\
             \\x5712\x5713\x5716\x5718\x571C\x571F\x5728\x572D\x5730\x573E\
             \\x5740\x5747\x574E\x5750\x5761\x5764\x5766\x576A\x5782\x5783\
             \\x578B\x57C3\x57CE\x57D4\x57DF\x57F7\x57F9\x57FA\x5802\x5805\
             \\x5806\x5821\x582A\x5831\x5834\x584A\x5854\x5857\x585E\x586B\
             \\x5875\x5883\x589E\x58A8\x58AE\x58C1\x58C7\x58D3\x58D8\x58DE\
             \\x58E2\x58EB\x58EC\x58EF\x58FD\x590F\x5915\x5916\x591A\x591C\
             \\x5920\x5922\x5925\x5927\x5929\x592A\x592B\x592E\x5931\x5937\
             \\x5938\x593E\x5947\x5948\x5949\x594E\x594F\x5951\x5954\x5957\
             \\x5967\x596A\x596E\x5973\x5974\x5976\x5979\x597D\x5982\x5999\
             \\x599D\x59A5\x59A8\x59AE\x59B3\x59B9\x59BB\x59C6\x59CA\x59CB\
             \\x59D0\x59D1\x59D3\x59D4\x59FF\x5A01\x5A03\x5A18\x5A1B\x5A41\
             \\x5A46\x5A5A\x5A66\x5A92\x5ABD\x5ACC\x5AE9\x5B50\x5B54\x5B57\
             \\x5B58\x5B5D\x5B5F\x5B63\x5B64\x5B69\x5B6B\x5B78\x5B83\x5B85\
             \\x5B87\x5B88\x5B89\x5B8B\x5B8C\x5B8F\x5B97\x5B98\x5B99\x5B9A\
             \\x5B9B\x5B9C\x5BA2\x5BA3\x5BA4\x5BAE\x5BB3\x5BB6\x5BB9\x5BBF\
             \\x5BC2\x5BC4\x5BC5\x5BC6\x5BCC\x5BD2\x5BDE\x5BDF\x5BE2\x5BE6\
             \\x5BE7\x5BE8\x5BE9\x5BEB\x5BEC\x5BEE\x5BF5\x5BF6\x5C01\x5C04\
             \\x5C07\x5C08\x5C0A\x5C0B\x5C0D\x5C0E\x5C0F\x5C11\x5C16\x5C1A\
             \\x5C24\x5C31\x5C3A\x5C3C\x5C3E\x5C40\x5C41\x5C45\x5C46\x5C4B\
             \\x5C4F\x5C55\x5C60\x5C64\x5C6C\x5C71\x5CA1\x5CA9\x5CB8\x5CF0\
             \\x5CF6\x5CFD\x5D07\x5D19\x5D34\x5D50\x5DBA\x5DDD\x5DDE\x5DE1\
             \\x5DE5\x5DE6\x5DE7\x5DE8\x5DEB\x5DEE\x5DF1\x5DF2\x5DF3\x5DF4\
             \\x5DF7\x5E02\x5E03\x5E0C\x5E15\x5E16\x5E1B\x5E1D\x5E25\x5E2B\
             \\x5E2D\x5E33\x5E36\x5E38\x5E3D\x5E45\x5E55\x5E63\x5E6B\x5E72\
             \\x5E73\x5E74\x5E78\x5E79\x5E7B\x5E7C\x5E7D\x5E7E\x5E87\x5E8A\
             \\x5E8F\x5E95\x5E97\x5E9A\x5E9C\x5EA6\x5EA7\x5EAB\x5EAD\x5EB7\
             \\x5EB8\x5EC9\x5ED6\x5EE0\x5EE2\x5EE3\x5EF3\x5EF6\x5EF7\x5EFA\
             \\x5F04\x5F0F\x5F15\x5F17\x5F18\x5F1F\x5F26\x5F31\x5F35\x5F37\
             \\x5F48\x5F4A\x5F4C\x5F4E\x5F5D\x5F5E\x5F62\x5F65\x5F69\x5F6C\
             \\x5F6D\x5F70\x5F71\x5F79\x5F7C\x5F80\x5F81\x5F85\x5F88\x5F8B\
             \\x5F8C\x5F90\x5F91\x5F92\x5F97\x5F9E\x5FA9\x5FAE\x5FB5\x5FB7\
             \\x5FB9\x5FC3\x5FC5\x5FCC\x5FCD\x5FD7\x5FD8\x5FD9\x5FE0\x5FEB\
             \\x5FF5\x5FFD\x600E\x6012\x6015\x6016\x601D\x6021\x6025\x6027\
             \\x6028\x602A\x6046\x6050\x6062\x6065\x6068\x6069\x606D\x606F\
             \\x6070\x6085\x6089\x6094\x609F\x60A0\x60A8\x60B2\x60B6\x60C5\
             \\x60D1\x60DC\x60E0\x60E1\x60F1\x60F3\x60F9\x6101\x6108\x6109\
             \\x610F\x611A\x611B\x611F\x6148\x614B\x6155\x6158\x6162\x6163\
             \\x6167\x616E\x6170\x6176\x617E\x6182\x6190\x6191\x61B2\x61B6\
             \\x61BE\x61C2\x61C9\x61F6\x61F7\x61FC\x6200\x6208\x620A\x620C\
             \\x6210\x6211\x6212\x6216\x622A\x6230\x6232\x6234\x6236\x623F\
             \\x6240\x6241\x6247\x624B\x624D\x624E\x6253\x6258\x6263\x6265\
             \\x626D\x626F\x6279\x627E\x627F\x6280\x6284\x628A\x6293\x6295\
             \\x6297\x6298\x62AB\x62AC\x62B1\x62B5\x62B9\x62BD\x62C6\x62C9\
             \\x62CB\x62CD\x62CF\x62D2\x62D4\x62D6\x62DB\x62DC\x62EC\x62F3\
             \\x62FC\x62FE\x62FF\x6301\x6307\x6309\x6311\x6316\x632A\x632F\
             \\x633A\x6350\x6355\x6368\x6372\x6377\x6383\x6388\x6389\x638C\
             \\x6392\x639B\x63A1\x63A2\x63A5\x63A7\x63A8\x63AA\x63CF\x63D0\
             \\x63D2\x63DA\x63DB\x63E1\x63EE\x63F4\x640D\x6416\x641C\x641E\
             \\x642C\x642D\x6436\x6458\x6469\x6478\x6490\x6492\x649E\x64A3\
             \\x64A5\x64AD\x64BE\x64BF\x64C1\x64C7\x64CA\x64CB\x64CD\x64CE\
             \\x64D4\x64DA\x64E0\x64E6\x64EC\x64F4\x64FA\x64FE\x651D\x652F\
             \\x6536\x6539\x653B\x653E\x653F\x6545\x6548\x654D\x654F\x6551\
             \\x6557\x6558\x6559\x655D\x6562\x6563\x6566\x656C\x6574\x6575\
             \\x6578\x6587\x6590\x6597\x6599\x65AF\x65B0\x65B7\x65B9\x65BC\
             \\x65BD\x65C1\x65C5\x65CB\x65CF\x65D7\x65E2\x65E5\x65E6\x65E9\
             \\x65ED\x65FA\x6602\x6606\x6607\x660C\x660E\x660F\x6613\x661F\
             \\x6620\x6625\x6628\x662D\x662F\x6642\x6649\x6652\x665A\x6668\
             \\x666E\x666F\x6674\x6676\x667A\x6691\x6696\x6697\x66AB\x66B4\
             \\x66C6\x66C9\x66F0\x66F2\x66F4\x66F8\x66FC\x66FE\x66FF\x6700\
             \\x6703\x6708\x6709\x670B\x670D\x6717\x671B\x671D\x671F\x6728\
             \\x672A\x672B\x672C\x672D\x6731\x6735\x6749\x674E\x6750\x6751\
             \\x675C\x675F\x676F\x6770\x6771\x677E\x677F\x6790\x6797\x679C\
             \\x679D\x67B6\x67CF\x67D0\x67D3\x67D4\x67E5\x67EC\x67EF\x67F3\
             \\x67F4\x6821\x6838\x6839\x683C\x6843\x6848\x684C\x6851\x6881\
             \\x6885\x689D\x68A8\x68AF\x68B0\x68B5\x68C4\x68C9\x68CB\x68D2\
             \\x68DA\x68EE\x6905\x690D\x6930\x694A\x6953\x695A\x696D\x6975\
             \\x6982\x699C\x69AE\x69CB\x69CD\x6A02\x6A13\x6A19\x6A1E\x6A21\
             \\x6A23\x6A39\x6A4B\x6A5F\x6A6B\x6A80\x6A94\x6AA2\x6B04\x6B0A\
             \\x6B21\x6B23\x6B32\x6B3A\x6B3D\x6B3E\x6B49\x6B4C\x6B50\x6B61\
             \\x6B62\x6B63\x6B64\x6B65\x6B66\x6B72\x6B77\x6B78\x6B7B\x6B8A\
             \\x6B98\x6BB5\x6BBA\x6BBC\x6BC0\x6BC5\x6BCD\x6BCF\x6BD2\x6BD4\
             \\x6BDB\x6BEB\x6C0F\x6C11\x6C23\x6C34\x6C38\x6C42\x6C57\x6C5D\
             \\x6C5F\x6C60\x6C61\x6C6A\x6C76\x6C7A\x6C7D\x6C83\x6C88\x6C89\
             \\x6C92\x6C96\x6C99\x6CB3\x6CB9\x6CBB\x6CBF\x6CC1\x6CC9\x6CCA\
             \\x6CD5\x6CE1\x6CE2\x6CE5\x6CE8\x6CF0\x6CF3\x6D0B\x6D17\x6D1B\
             \\x6D1E\x6D29\x6D2A\x6D32\x6D3B\x6D3D\x6D3E\x6D41\x6D66\x6D69\
             \\x6D6A\x6D6E\x6D77\x6D87\x6D88\x6D89\x6DAF\x6DB2\x6DB5\x6DBC\
             \\x6DD1\x6DDA\x6DE1\x6DE8\x6DF1\x6DF7\x6DFA\x6E05\x6E1B\x6E21\
             \\x6E2C\x6E2F\x6E38\x6E56\x6E6F\x6E90\x6E96\x6E9D\x6EAA\x6EAB\
             \\x6EC4\x6EC5\x6ECB\x6ED1\x6EF4\x6EFE\x6EFF\x6F02\x6F0F\x6F14\
             \\x6F20\x6F22\x6F2B\x6F32\x6F38\x6F54\x6F58\x6F5B\x6F6E\x6FA4\
             \\x6FB3\x6FC0\x6FC3\x6FDF\x6FE4\x6FEB\x6FF1\x700F\x704C\x7063\
             \\x706B\x7070\x707D\x708E\x70AE\x70B8\x70BA\x70C8\x70CF\x70E4\
             \\x7121\x7126\x7136\x7159\x715E\x7167\x7169\x718A\x719F\x71B1\
             \\x71C3\x71C8\x71D2\x71DF\x7206\x7210\x721B\x722A\x722C\x722D\
             \\x7235\x7236\x7238\x723A\x723D\x723E\x7246\x7247\x7248\x724C\
             \\x7259\x725B\x7260\x7267\x7269\x7272\x7279\x727D\x72A7\x72AF\
             \\x72C0\x72C2\x72D0\x72D7\x72E0\x72FC\x731B\x731C\x7334\x7336\
             \\x7344\x7345\x734E\x7368\x7372\x7378\x737B\x7384\x7387\x7389\
             \\x738B\x73A9\x73AB\x73B2\x73BB\x73CA\x73CD\x73E0\x73E5\x73ED\
             \\x73FE\x7403\x7406\x7409\x742A\x7434\x7459\x745C\x745E\x745F\
             \\x7464\x746A\x7470\x74B0\x74DC\x74E6\x74F6\x7518\x751A\x751C\
             \\x751F\x7522\x7528\x7530\x7531\x7532\x7533\x7537\x7538\x754C\
             \\x7559\x7562\x7565\x756A\x756B\x7570\x7576\x7586\x758F\x7591\
             \\x75BC\x75C5\x75D5\x75DB\x75F4\x760B\x7642\x7661\x7678\x767B\
             \\x767C\x767D\x767E\x7684\x7686\x7687\x76AE\x76C3\x76CA\x76DB\
             \\x76DC\x76DF\x76E1\x76E3\x76E4\x76E7\x76EE\x76F2\x76F4\x76F8\
             \\x76FC\x76FE\x7701\x7709\x770B\x771F\x7720\x773C\x773E\x775B\
             \\x7761\x7763\x77A7\x77AD\x77DB\x77E3\x77E5\x77ED\x77F3\x7802\
             \\x780D\x7814\x7832\x7834\x786C\x788E\x7897\x789F\x78A7\x78A9\
             \\x78B0\x78BA\x78BC\x78C1\x78E8\x78EF\x790E\x7919\x793A\x793E\
             \\x7955\x7956\x795A\x795B\x795D\x795E\x7965\x7968\x797F\x7981\
             \\x798D\x798E\x798F\x79AA\x79AE\x79C0\x79C1\x79CB\x79D1\x79D2\
             \\x79D8\x79DF\x79E4\x79E6\x79FB\x7A05\x7A0B\x7A0D\x7A2E\x7A31\
             \\x7A3F\x7A46\x7A4C\x7A4D\x7A69\x7A76\x7A79\x7A7A\x7A7F\x7A81\
             \\x7A97\x7AA9\x7AAE\x7AB6\x7ACB\x7AD9\x7ADF\x7AE0\x7AE5\x7AEF\
             \\x7AF6\x7AF9\x7B11\x7B1B\x7B26\x7B28\x7B2C\x7B46\x7B49\x7B4B\
             \\x7B54\x7B56\x7B80\x7B97\x7BA1\x7BAD\x7BB1\x7BC0\x7BC4\x7BC7\
             \\x7BC9\x7C21\x7C2B\x7C3D\x7C3F\x7C43\x7C4C\x7C4D\x7C64\x7C73\
             \\x7C89\x7C97\x7CB5\x7CBE\x7CCA\x7CD5\x7CDF\x7CFB\x7CFE\x7D00\
             \\x7D04\x7D05\x7D0D\x7D10\x7D14\x7D19\x7D1A\x7D1B\x7D20\x7D22\
             \\x7D2B\x7D2F\x7D30\x7D39\x7D42\x7D44\x7D50\x7D55\x7D61\x7D66\
             \\x7D71\x7D72\x7D93\x7D9C\x7DA0\x7DAD\x7DB1\x7DB2\x7DCA\x7DD2\
             \\x7DDA\x7DE3\x7DE8\x7DE9\x7DEC\x7DEF\x7DF4\x7E1B\x7E23\x7E2E\
             \\x7E31\x7E3D\x7E3E\x7E41\x7E46\x7E54\x7E5E\x7E6A\x7E73\x7E7C\
             \\x7E8C\x7F38\x7F3A\x7F55\x7F6A\x7F6E\x7F70\x7F72\x7F75\x7F77\
             \\x7F85\x7F8A\x7F8E\x7F9E\x7FA4\x7FA9\x7FBD\x7FC1\x7FD2\x7FD4\
             \\x7FF0\x7FF9\x7FFB\x7FFC\x8000\x8001\x8003\x8005\x800C\x800D\
             \\x8010\x8017\x8033\x8036\x804A\x8056\x805A\x805E\x806F\x8070\
             \\x8072\x8077\x807D\x8089\x809A\x80A1\x80A5\x80A9\x80AF\x80B2\
             \\x80CC\x80CE\x80D6\x80DE\x80E1\x80F8\x80FD\x8106\x812B\x8153\
             \\x8154\x8166\x8170\x8173\x817F\x81BD\x81C9\x81D8\x81E3\x81E5\
             \\x81E8\x81EA\x81ED\x81F3\x81F4\x81FA\x8207\x8208\x8209\x820A\
             \\x820C\x820D\x8212\x821E\x821F\x822A\x822C\x8239\x8266\x826F\
             \\x8272\x827E\x829D\x82AC\x82B1\x82B3\x82E5\x82E6\x82F1\x8305\
             \\x832B\x8332\x8336\x8349\x8352\x8377\x837C\x8389\x838A\x838E\
             \\x83AB\x83DC\x83E9\x83EF\x83F2\x8404\x840A\x842C\x843D\x8449\
             \\x8457\x845B\x8461\x8482\x8499\x84B2\x84BC\x84CB\x84EE\x8515\
             \\x8521\x8523\x856D\x8584\x85A6\x85A9\x85AA\x85C9\x85CD\x85CF\
             \\x85DD\x85E4\x85E5\x8606\x8607\x862D\x864E\x8655\x865B\x865F\
             \\x8667\x86C7\x86CB\x86D9\x8702\x871C\x8776\x878D\x87A2\x87F2\
             \\x87F9\x880D\x883B\x8840\x884C\x8853\x8857\x885B\x885D\x8861\
             \\x8863\x8868\x888B\x88AB\x88C1\x88C2\x88D5\x88DC\x88DD\x88E1\
             \\x88FD\x8907\x8932\x897F\x8981\x8986\x898B\x898F\x8996\x89AA\
             \\x89BA\x89BD\x89C0\x89D2\x89E3\x89F8\x8A00\x8A02\x8A08\x8A0A\
             \\x8A0E\x8A13\x8A17\x8A18\x8A25\x8A2A\x8A2D\x8A31\x8A34\x8A3B\
             \\x8A3C\x8A55\x8A5E\x8A62\x8A66\x8A69\x8A71\x8A72\x8A73\x8A87\
             \\x8A8C\x8A8D\x8A93\x8A95\x8A9E\x8AA0\x8AA4\x8AAA\x8AB0\x8AB2\
             \\x8ABC\x8ABF\x8AC7\x8ACB\x8AD2\x8AD6\x8AF8\x8AFA\x8AFE\x8B00\
             \\x8B02\x8B1B\x8B1D\x8B49\x8B58\x8B5C\x8B66\x8B6F\x8B70\x8B77\
             \\x8B7D\x8B80\x8B8A\x8B93\x8B9A\x8C37\x8C46\x8C48\x8C50\x8C61\
             \\x8C6A\x8C6C\x8C8C\x8C93\x8C9D\x8C9E\x8CA0\x8CA1\x8CA2\x8CA8\
             \\x8CAA\x8CAB\x8CAC\x8CB4\x8CB7\x8CBB\x8CBC\x8CC0\x8CC7\x8CC8\
             \\x8CD3\x8CDC\x8CDE\x8CE2\x8CE3\x8CE4\x8CE6\x8CEA\x8CED\x8CF4\
             \\x8CFA\x8CFC\x8CFD\x8D08\x8D0A\x8D0F\x8D64\x8D6B\x8D70\x8D77\
             \\x8D85\x8D8A\x8D95\x8D99\x8DA3\x8DA8\x8DB3\x8DCC\x8DCE\x8DD1\
             \\x8DDD\x8DDF\x8DE1\x8DEF\x8DF3\x8E0F\x8E22\x8E5F\x8E64\x8E8D\
             \\x8EAB\x8EB2\x8ECA\x8ECC\x8ECD\x8ED2\x8EDF\x8F03\x8F09\x8F14\
             \\x8F15\x8F1B\x8F1D\x8F29\x8F2A\x8F2F\x8F38\x8F49\x8F5F\x8F9B\
             \\x8FA6\x8FA8\x8FAD\x8FAF\x8FB0\x8FB1\x8FB2\x8FC5\x8FCE\x8FD1\
             \\x8FD4\x8FE6\x8FEA\x8FEB\x8FF0\x8FF4\x8FF7\x8FFD\x9000\x9001\
             \\x9003\x9006\x900F\x9010\x9014\x9019\x901A\x901B\x901D\x901F\
             \\x9020\x9022\x9023\x9031\x9032\x9038\x903C\x9047\x904A\x904B\
             \\x904D\x904E\x9053\x9054\x9055\x9059\x905C\x9060\x9069\x906D\
             \\x906E\x9072\x9077\x9078\x907A\x907F\x9080\x9081\x9084\x908A\
             \\x908F\x90A3\x90A6\x90AA\x90B1\x90CE\x90E8\x90ED\x90F5\x90FD\
             \\x9102\x9109\x912D\x9130\x9149\x914D\x9152\x9177\x9178\x9189\
             \\x9192\x919C\x91AB\x91C7\x91CB\x91CC\x91CD\x91CE\x91CF\x91D1\
             \\x91DD\x91E3\x9234\x9262\x9280\x9285\x9296\x9298\x92B3\x92B7\
             \\x92D2\x92FC\x9304\x9322\x9326\x932B\x932F\x934B\x9375\x937E\
             \\x938A\x9396\x93AE\x93E1\x9418\x9435\x9451\x9577\x9580\x9583\
             \\x9589\x958B\x958F\x9592\x9593\x95A3\x95B1\x95C6\x95CA\x95CD\
             \\x95D0\x95DC\x95E1\x9632\x963B\x963F\x9640\x9644\x964D\x9650\
             \\x9662\x9663\x9664\x966A\x9670\x9673\x9675\x9676\x9677\x9678\
             \\x967D\x9686\x968A\x968E\x9694\x969B\x969C\x96A8\x96AA\x96B1\
             \\x96BB\x96C4\x96C5\x96C6\x96C9\x96D6\x96D9\x96DC\x96DE\x96E2\
             \\x96E3\x96E8\x96EA\x96F2\x96F6\x96F7\x96FB\x9700\x9707\x970D\
             \\x9727\x9732\x9738\x9739\x9742\x9748\x9752\x9756\x975C\x975E\
             \\x9760\x9762\x9769\x977C\x978B\x97C3\x97CB\x97D3\x97F3\x97FB\
             \\x97FF\x9801\x9802\x9805\x9806\x9808\x9810\x9811\x9813\x9817\
             \\x9818\x981E\x982D\x983B\x9846\x984C\x984D\x984F\x9858\x985E\
             \\x9867\x986F\x98A8\x98C4\x98DB\x98DF\x98EF\x98F2\x98FD\x98FE\
             \\x9905\x990A\x9910\x9918\x9928\x9996\x9999\x99AC\x99D0\x99D5\
             \\x99DB\x9A0E\x9A19\x9A37\x9A45\x9A57\x9A5A\x9AA8\x9AD4\x9AD8\
             \\x9AEE\x9B06\x9B25\x9B27\x9B31\x9B3C\x9B41\x9B42\x9B45\x9B54\
             \\x9B5A\x9B6F\x9BAE\x9CE5\x9CF3\x9CF4\x9D3B\x9D5D\x9DF9\x9E7F\
             \\x9E97\x9EA5\x9EB5\x9EBB\x9EBC\x9EC3\x9ECE\x9ED1\x9ED8\x9EDE\
             \\x9EE8\x9F13\x9F20\x9F3B\x9F4A\x9F4B\x9F52\x9F61\x9F8D\x9F9C"
          , _exAuxiliary = Just $ Set.fromList
             "\x4E4D\x4E73\x4EC2\x4F0F\x4F50\x4FB6\x4FCF\x5009\x507D\x5085\
             \\x5098\x50F3\x5146\x514C\x5179\x51CB\x51CD\x51F8\x5212\x5228\
             \\x522B\x522E\x5238\x5243\x52F3\x52FE\x5315\x5319\x5323\x532F\
             \\x5351\x535E\x5360\x5379\x53C9\x53F6\x543B\x54FA\x5507\x5535\
             \\x5564\x55AA\x55B2\x561F\x5641\x5653\x5658\x568F\x5751\x5824\
             \\x5885\x588E\x5893\x589F\x58B3\x58E4\x58E9\x58FA\x5965\x5996\
             \\x5B30\x5B55\x5B5C\x5B75\x5BFA\x5C3F\x5C4D\x5C51\x5CC7\x5DBC\
             \\x5DFD\x5DFE\x5E06\x5E1A\x5E5F\x5EC1\x5EC8\x5EDA\x5EDF\x5F0B\
             \\x5F13\x5FE1\x618A\x61E8\x61F8\x621F\x626E\x6273\x6342\x634F\
             \\x6367\x63A0\x63B0\x63F9\x640F\x6440\x6454\x6495\x64B2\x6500\
             \\x6524\x655E\x6591\x659C\x65A7\x6688\x66AE\x66C7\x66EC\x66F3\
             \\x6714\x6756\x67AF\x6813\x6817\x683D\x6846\x6876\x687F\x68CD\
             \\x68D5\x68FA\x6912\x6954\x69CC\x6A44\x6A47\x6A58\x6A59\x6AAC\
             \\x6AB8\x6AC3\x6ADA\x6AFB\x6B16\x6B20\x6B8B\x6BAD\x6C41\x6CAB\
             \\x6CAE\x6CE3\x6D63\x6D74\x6D85\x6D8E\x6DAE\x6DC7\x6DCB\x6E3E\
             \\x6E58\x6E9C\x6F3F\x6F8E\x6FA1\x6FD5\x7058\x70D8\x70F9\x710A\
             \\x7119\x7130\x714E\x716E\x71D5\x71D9\x71E6\x71ED\x720D\x7261\
             \\x7280\x72AC\x72C4\x72E1\x72F8\x7329\x733E\x733F\x737A\x737E\
             \\x7433\x745A\x74E2\x7515\x752B\x758A\x75B2\x75BE\x7626\x7627\
             \\x7682\x76BA\x76BF\x76C6\x76C8\x76D2\x76D4\x76E5\x7728\x7729\
             \\x774F\x7787\x778C\x77AA\x7891\x78DA\x7901\x792B\x7948\x79B1\
             \\x79BF\x7A3B\x7A40\x7A84\x7AFF\x7B52\x7B77\x7B8F\x7B94\x7BF7\
             \\x7C0D\x7C60\x7CD6\x7CF0\x7D09\x7D0B\x7D17\x7D2E\x7D33\x7DBD\
             \\x7DBF\x7E2B\x7E43\x7E61\x7E69\x7E8F\x7E96\x7E9C\x7F48\x7F50\
             \\x7F69\x7FAF\x8073\x807E\x808C\x8096\x80BA\x8108\x8116\x8150\
             \\x8179\x819A\x81A0\x81C2\x81DF\x8247\x8292\x8299\x82AD\x82BD\
             \\x82D7\x82E3\x8304\x8328\x8335\x8338\x8393\x8396\x83C7\x83CC\
             \\x83F1\x840E\x8435\x8475\x849C\x84B8\x84C4\x84C9\x84EC\x8514\
             \\x8525\x852C\x8549\x857E\x8591\x85AF\x860B\x8611\x863F\x8679\
             \\x868A\x8693\x86A9\x86AF\x86DB\x8700\x8718\x8725\x8734\x8759\
             \\x875F\x8760\x8766\x8774\x8778\x8782\x8783\x879E\x87BA\x87C0\
             \\x87C4\x87CB\x87D1\x87F3\x87FB\x8805\x8815\x881F\x8823\x886B\
             \\x888D\x88CF\x88D8\x88D9\x88F1\x88F9\x8910\x896A\x896F\x8A1D\
             \\x8A3A\x8B0E\x8B2C\x8C4E\x8C54\x8C5A\x8C79\x8D1B\x8DC6\x8DE8\
             \\x8DEA\x8E29\x8EAC\x8EF8\x8F4E\x8F9C\x8FA3\x905E\x9119\x914B\
             \\x916A\x91AC\x91D8\x9214\x9215\x9245\x925B\x9264\x92C1\x9328\
             \\x9336\x934A\x939A\x93AC\x93C8\x93E2\x943A\x9470\x947D\x947F\
             \\x95A9\x9631\x96B4\x96C0\x96CC\x9704\x971C\x9759\x9774\x97A0\
             \\x97AD\x980C\x9838\x985B\x98B1\x98C6\x98EA\x9903\x990C\x991A\
             \\x9935\x993E\x99DD\x99F1\x9A55\x9AB0\x9AB7\x9ACF\x9B0D\x9B77\
             \\x9B91\x9BC9\x9BCA\x9BE8\x9C77\x9CE9\x9CF6\x9D28\x9D61\x9DB4\
             \\x9E1A\x9E7D\x9EDB\x9F2C\x9F90"
          , _exPunctuation = Just $ Set.fromList
             "\x21\x22\x23\x25\x26\x28\x29\x2A\x2C\x2D\
             \\x2E\x2F\x3A\x3B\x3F\x40\x5B\x5C\x5D\x5F\
             \\x7B\x7D\xA7\xB7\x2010\x2011\x2013\x2014\x2018\x2019\
             \\x201C\x201D\x2020\x2021\x2025\x2026\x2027\x2030\x2032\x2033\
             \\x2035\x203B\x203E\x3001\x3002\x3003\x3008\x3009\x300A\x300B\
             \\x300C\x300D\x300E\x300F\x3010\x3011\x3014\x3015\x301D\x301E\
             \\xFE30\xFE31\xFE32\xFE33\xFE34\xFE35\xFE36\xFE37\xFE38\xFE39\xFE3A\
             \\xFE3B\xFE3C\xFE3D\xFE3E\xFE3F\xFE40\xFE41\xFE42\xFE43\xFE44\
             \\xFE49\xFE4A\xFE4B\xFE4C\xFE4D\xFE4E\xFE4F\xFE50\xFE51\xFE52\xFE54\xFE55\xFE56\xFE57\xFE58\
             \\xFE59\xFE5A\xFE5B\xFE5C\xFE5D\xFE5E\xFE5F\xFE60\xFE61\xFE63\
             \\xFE68\xFE6A\xFE6B\xFF01\xFF02\xFF03\xFF05\xFF06\xFF07\xFF08\
             \\xFF09\xFF0A\xFF0C\xFF0D\xFF0E\xFF0F\xFF1A\xFF1B\xFF1F\xFF20\
             \\xFF3B\xFF3C\xFF3D\xFF3F\xFF5B\xFF5D"
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x30\x31\x32\x33\x34\
             \\x35\x36\x37\x38\x39\x2011\x2030\x3007\x4E00\x4E03\
             \\x4E09\x4E5D\x4E8C\x4E94\x516B\x516D\x56DB"
          }
      }
    )
  , ("zu-ZA", LanguageData
      { language = "zu"
      , script = Nothing
      , territory = Just "ZA"
      , exemplars = Nothing
      }
    )
  , ("az-Cyrl-AZ", LanguageData
      { language = "az"
      , script = Just "Cyrl"
      , territory = Just "AZ"
      , exemplars = Nothing
      }
    )
  , ("az-Latn-AZ", LanguageData
      { language = "az"
      , script = Just "Latn"
      , territory = Just "AZ"
      , exemplars = Nothing
      }
    )
  , ("bs-Cyrl-BA", LanguageData
      { language = "bs"
      , script = Just "Cyrl"
      , territory = Just "BA"
      , exemplars = Nothing
      }
    )
  , ("bs-Latn-BA", LanguageData
      { language = "bs"
      , script = Just "Latn"
      , territory = Just "BA"
      , exemplars = Nothing
      }
    )
  , ("ca-ES-VALENCIA", LanguageData
      { language = "ca"
      , script = Nothing
      , territory = Just "ES"
      , exemplars = Nothing
      }
    )
  , ("en-US-POSIX", LanguageData
      { language = "en"
      , script = Nothing
      , territory = Just "US"
      , exemplars = Just Exemplars
          { _exMain = Nothing
          , _exAuxiliary = Nothing
          , _exPunctuation = Nothing
          , _exNumbers = Just $ Set.fromList
             "\x25\x2B\x2C\x2D\x2E\x2F\x30\x31\x32\x33\
             \\x34\x35\x36\x37\x38\x39\x2011"
          }
      }
    )
  , ("ff-Adlm-BF", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "BF"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-CM", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-GH", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "GH"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-GM", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "GM"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-GN", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "GN"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-GW", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "GW"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-LR", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "LR"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-MR", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "MR"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-NE", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "NE"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-NG", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-SL", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "SL"
      , exemplars = Nothing
      }
    )
  , ("ff-Adlm-SN", LanguageData
      { language = "ff"
      , script = Just "Adlm"
      , territory = Just "SN"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-BF", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "BF"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-CM", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "CM"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-GH", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "GH"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-GM", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "GM"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-GN", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "GN"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-GW", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "GW"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-LR", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "LR"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-MR", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "MR"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-NE", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "NE"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-NG", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "NG"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-SL", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "SL"
      , exemplars = Nothing
      }
    )
  , ("ff-Latn-SN", LanguageData
      { language = "ff"
      , script = Just "Latn"
      , territory = Just "SN"
      , exemplars = Nothing
      }
    )
  , ("ks-Arab-IN", LanguageData
      { language = "ks"
      , script = Just "Arab"
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("mni-Beng-IN", LanguageData
      { language = "mni"
      , script = Just "Beng"
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("pa-Arab-PK", LanguageData
      { language = "pa"
      , script = Just "Arab"
      , territory = Just "PK"
      , exemplars = Nothing
      }
    )
  , ("pa-Guru-IN", LanguageData
      { language = "pa"
      , script = Just "Guru"
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("sat-Olck-IN", LanguageData
      { language = "sat"
      , script = Just "Olck"
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("sd-Arab-PK", LanguageData
      { language = "sd"
      , script = Just "Arab"
      , territory = Just "PK"
      , exemplars = Nothing
      }
    )
  , ("sd-Deva-IN", LanguageData
      { language = "sd"
      , script = Just "Deva"
      , territory = Just "IN"
      , exemplars = Nothing
      }
    )
  , ("shi-Latn-MA", LanguageData
      { language = "shi"
      , script = Just "Latn"
      , territory = Just "MA"
      , exemplars = Nothing
      }
    )
  , ("shi-Tfng-MA", LanguageData
      { language = "shi"
      , script = Just "Tfng"
      , territory = Just "MA"
      , exemplars = Nothing
      }
    )
  , ("sr-Cyrl-BA", LanguageData
      { language = "sr"
      , script = Just "Cyrl"
      , territory = Just "BA"
      , exemplars = Nothing
      }
    )
  , ("sr-Cyrl-ME", LanguageData
      { language = "sr"
      , script = Just "Cyrl"
      , territory = Just "ME"
      , exemplars = Nothing
      }
    )
  , ("sr-Cyrl-RS", LanguageData
      { language = "sr"
      , script = Just "Cyrl"
      , territory = Just "RS"
      , exemplars = Nothing
      }
    )
  , ("sr-Cyrl-XK", LanguageData
      { language = "sr"
      , script = Just "Cyrl"
      , territory = Just "XK"
      , exemplars = Nothing
      }
    )
  , ("sr-Latn-BA", LanguageData
      { language = "sr"
      , script = Just "Latn"
      , territory = Just "BA"
      , exemplars = Nothing
      }
    )
  , ("sr-Latn-ME", LanguageData
      { language = "sr"
      , script = Just "Latn"
      , territory = Just "ME"
      , exemplars = Nothing
      }
    )
  , ("sr-Latn-RS", LanguageData
      { language = "sr"
      , script = Just "Latn"
      , territory = Just "RS"
      , exemplars = Nothing
      }
    )
  , ("sr-Latn-XK", LanguageData
      { language = "sr"
      , script = Just "Latn"
      , territory = Just "XK"
      , exemplars = Nothing
      }
    )
  , ("su-Latn-ID", LanguageData
      { language = "su"
      , script = Just "Latn"
      , territory = Just "ID"
      , exemplars = Nothing
      }
    )
  , ("uz-Arab-AF", LanguageData
      { language = "uz"
      , script = Just "Arab"
      , territory = Just "AF"
      , exemplars = Nothing
      }
    )
  , ("uz-Cyrl-UZ", LanguageData
      { language = "uz"
      , script = Just "Cyrl"
      , territory = Just "UZ"
      , exemplars = Nothing
      }
    )
  , ("uz-Latn-UZ", LanguageData
      { language = "uz"
      , script = Just "Latn"
      , territory = Just "UZ"
      , exemplars = Nothing
      }
    )
  , ("vai-Latn-LR", LanguageData
      { language = "vai"
      , script = Just "Latn"
      , territory = Just "LR"
      , exemplars = Nothing
      }
    )
  , ("vai-Vaii-LR", LanguageData
      { language = "vai"
      , script = Just "Vaii"
      , territory = Just "LR"
      , exemplars = Nothing
      }
    )
  , ("yue-Hans-CN", LanguageData
      { language = "yue"
      , script = Just "Hans"
      , territory = Just "CN"
      , exemplars = Nothing
      }
    )
  , ("yue-Hant-HK", LanguageData
      { language = "yue"
      , script = Just "Hant"
      , territory = Just "HK"
      , exemplars = Nothing
      }
    )
  , ("zh-Hans-CN", LanguageData
      { language = "zh"
      , script = Just "Hans"
      , territory = Just "CN"
      , exemplars = Nothing
      }
    )
  , ("zh-Hans-HK", LanguageData
      { language = "zh"
      , script = Just "Hans"
      , territory = Just "HK"
      , exemplars = Nothing
      }
    )
  , ("zh-Hans-MO", LanguageData
      { language = "zh"
      , script = Just "Hans"
      , territory = Just "MO"
      , exemplars = Nothing
      }
    )
  , ("zh-Hans-SG", LanguageData
      { language = "zh"
      , script = Just "Hans"
      , territory = Just "SG"
      , exemplars = Nothing
      }
    )
  , ("zh-Hant-HK", LanguageData
      { language = "zh"
      , script = Just "Hant"
      , territory = Just "HK"
      , exemplars = Nothing
      }
    )
  , ("zh-Hant-MO", LanguageData
      { language = "zh"
      , script = Just "Hant"
      , territory = Just "MO"
      , exemplars = Nothing
      }
    )
  , ("zh-Hant-TW", LanguageData
      { language = "zh"
      , script = Just "Hant"
      , territory = Just "TW"
      , exemplars = Nothing
      }
    )

  ]
