module Klay.Utils.Unicode.Latin
  ( latinSmallCapital
  ) where

-- [TODO] ᴣᴕᴐᴙᴚⱻʁꟺ
-- | Convert a small latin letter into its small capital.
latinSmallCapital :: Char -> Maybe Char
latinSmallCapital 'a' = Just 'ᴀ' -- U1D00 # LATIN LETTER SMALL CAPITAL A
latinSmallCapital 'æ' = Just 'ᴁ' -- U1D01 # LATIN LETTER SMALL CAPITAL AE
latinSmallCapital 'b' = Just 'ʙ' -- U0299 # LATIN LETTER SMALL CAPITAL B
latinSmallCapital 'c' = Just 'ᴄ' -- U1D04 # LATIN LETTER SMALL CAPITAL C
latinSmallCapital 'd' = Just 'ᴅ' -- U1D05 # LATIN LETTER SMALL CAPITAL D
latinSmallCapital 'e' = Just 'ᴇ' -- U1D07 # LATIN LETTER SMALL CAPITAL E
latinSmallCapital 'f' = Just 'ꜰ' -- UA730 # LATIN LETTER SMALL CAPITAL F
latinSmallCapital 'g' = Just 'ɢ' -- U0262 # LATIN LETTER SMALL CAPITAL G
latinSmallCapital 'h' = Just 'ʜ' -- U029C # LATIN LETTER SMALL CAPITAL H
latinSmallCapital 'i' = Just 'ɪ' -- U026A # LATIN LETTER SMALL CAPITAL I
latinSmallCapital 'j' = Just 'ᴊ' -- U1D0A # LATIN LETTER SMALL CAPITAL J
latinSmallCapital 'k' = Just 'ᴋ' -- U1D0B # LATIN LETTER SMALL CAPITAL K
latinSmallCapital 'l' = Just 'ʟ' -- U029F # LATIN LETTER SMALL CAPITAL L
latinSmallCapital 'm' = Just 'ᴍ' -- U1D0D # LATIN LETTER SMALL CAPITAL M
latinSmallCapital 'n' = Just 'ɴ' -- U0274 # LATIN LETTER SMALL CAPITAL N
latinSmallCapital 'o' = Just 'ᴏ' -- U1D0F # LATIN LETTER SMALL CAPITAL O
latinSmallCapital 'œ' = Just 'ɶ' -- U0276 # LATIN LETTER SMALL CAPITAL OE
latinSmallCapital 'p' = Just 'ᴘ' -- U1D18 # LATIN LETTER SMALL CAPITAL P
latinSmallCapital 'q' = Just 'ꞯ' -- UA7AF # LATIN LETTER SMALL CAPITAL Q
latinSmallCapital 'r' = Just 'ʀ' -- U0280 # LATIN LETTER SMALL CAPITAL R
latinSmallCapital 's' = Just 'ꜱ' -- UA731 # LATIN LETTER SMALL CAPITAL S
latinSmallCapital 't' = Just 'ᴛ' -- U1D1B # LATIN LETTER SMALL CAPITAL T
latinSmallCapital 'u' = Just 'ᴜ' -- U1D1C # LATIN LETTER SMALL CAPITAL U
latinSmallCapital 'v' = Just 'ᴠ' -- U1D20 # LATIN LETTER SMALL CAPITAL V
latinSmallCapital 'w' = Just 'ᴡ' -- U1D21 # LATIN LETTER SMALL CAPITAL W
-- There is no SMALL CAPITAL X (yet)
latinSmallCapital 'y' = Just 'ʏ' -- U028F # LATIN LETTER SMALL CAPITAL Y
latinSmallCapital 'z' = Just 'ᴢ' -- U1D22 # LATIN LETTER SMALL CAPITAL Z
latinSmallCapital _   = Nothing
