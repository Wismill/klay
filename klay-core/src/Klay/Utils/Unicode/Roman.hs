module Klay.Utils.Unicode.Roman
  ( toLowerRoman
  , toLowerSingleRoman
  , toUpperRoman
  , toUpperSingleRoman
  , toRoman
  , lowerRomanNumerals
  , lowerSingleRomanNumerals
  , upperRomanNumerals
  , upperSingleRomanNumerals
  ) where

import Data.String (IsString(..))

-- |Converts a number to a lower Roman numeral.
toLowerRoman :: (Num n, Ord n, Monoid t, IsString t) => n -> Maybe t
toLowerRoman = toRoman lowerRomanNumerals 3999

-- |Converts a number to a lower single Roman numeral.
toLowerSingleRoman :: (Num n, Ord n, Monoid t, IsString t) => n -> Maybe t
toLowerSingleRoman = toRoman lowerSingleRomanNumerals 12

-- |Converts a number to a upper Roman numeral.
toUpperRoman :: (Num n, Ord n, Monoid t, IsString t) => n -> Maybe t
toUpperRoman = toRoman upperRomanNumerals 3999

-- |Converts a number to a upper Roman numeral.
toUpperSingleRoman :: (Num n, Ord n, Monoid t, IsString t) => n -> Maybe t
toUpperSingleRoman = toRoman upperSingleRomanNumerals 12

-- |Converts a number to a Roman numeral according to the given configuration.
toRoman
  :: (Num n, Ord n, Monoid t)
  => [(t, n)]                 -- ^ Descending ordered list of symbols
  -> n                        -- ^ Maximum supported number
  -> n                        -- ^ Number to convert
  -> Maybe t
toRoman _  m n | n <= 0 || n > m = Nothing
toRoman ns _ n = go n ns
  where go _ [] = Nothing
        go k ts0@(~(symbols, value) : ts)
            | k <= 0     = Just mempty
            | k >= value = Just symbols <> go (k - value) ts0
            | otherwise  = go k ts

lowerRomanNumerals :: (Num n, IsString t) => [(t, n)]
lowerRomanNumerals = reverse
  [ ("ⅰ",     1)
  , ("ⅰⅴ",    4)
  , ("ⅴ",     5)
  , ("ⅰⅹ",    9)
  , ("ⅹ",    10)
  , ("ⅹⅼ",   40)
  , ("ⅼ",    50)
  , ("ⅹⅽ",   90)
  , ("ⅽ",   100)
  , ("ⅽⅾ",  400)
  , ("ⅾ",   500)
  , ("ⅽⅿ",  900)
  , ("ⅿ",  1000)
  ]

lowerSingleRomanNumerals :: (Num n, IsString t) => [(t, n)]
lowerSingleRomanNumerals = reverse
  [ ("ⅰ",     1)
  , ("ⅱ",     2)
  , ("ⅲ",     3)
  , ("ⅳ",     4)
  , ("ⅴ",     5)
  , ("ⅵ",     6)
  , ("ⅶ",     7)
  , ("ⅷ",     8)
  , ("ⅸ",     9)
  , ("ⅹ",    10)
  , ("ⅺ",    11)
  , ("ⅻ",    12)
  ]

upperRomanNumerals :: (Num n, IsString t) => [(t, n)]
upperRomanNumerals = reverse
  [ ("Ⅰ",     1)
  , ("ⅠⅤ",    4)
  , ("Ⅴ",     5)
  , ("ⅠⅩ",    9)
  , ("Ⅹ",    10)
  , ("ⅩⅬ",   40)
  , ("Ⅼ",    50)
  , ("ⅩⅭ",   90)
  , ("Ⅽ",   100)
  , ("ⅭⅮ",  400)
  , ("Ⅾ",   500)
  , ("ⅭⅯ",  900)
  , ("Ⅿ",  1000)
  ]

upperSingleRomanNumerals :: (Num n, IsString t) => [(t, n)]
upperSingleRomanNumerals = reverse
  [ ("Ⅰ",     1)
  , ("Ⅱ",     2)
  , ("Ⅲ",     3)
  , ("Ⅳ",     4)
  , ("Ⅴ",     5)
  , ("Ⅵ",     6)
  , ("Ⅶ",     7)
  , ("Ⅷ",     8)
  , ("Ⅸ",     9)
  , ("Ⅹ",    10)
  , ("Ⅺ",    11)
  , ("Ⅻ",    12)
  ]
