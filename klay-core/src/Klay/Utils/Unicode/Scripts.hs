-- [NOTE] Auto-generated. Do not edit manually

{-# LANGUAGE OverloadedLists #-}
{- HLINT ignore "Use camelCase" -}

module Klay.Utils.Unicode.Scripts
  ( UnicodeScript(..)
  , UnicodeRange(..)
  , unicodeScript
  , unicodeScripts
  ) where

import Data.Map.Strict (Map)

import TextShow (TextShow, FromStringShow(..))
import Data.Csv


-- | Unicode Script property values. See: [Report TR24](https://www.unicode.org/reports/tr24/)
data UnicodeScript
  = Adlam
  | Ahom
  | Anatolian_Hieroglyphs
  | Arabic
  | Armenian
  | Avestan
  | Balinese
  | Bamum
  | Bassa_Vah
  | Batak
  | Bengali
  | Bhaiksuki
  | Bopomofo
  | Brahmi
  | Braille
  | Buginese
  | Buhid
  | Canadian_Aboriginal
  | Carian
  | Caucasian_Albanian
  | Chakma
  | Cham
  | Cherokee
  | Chorasmian
  | Common
  | Coptic
  | Cuneiform
  | Cypriot
  | Cyrillic
  | Deseret
  | Devanagari
  | Dives_Akuru
  | Dogra
  | Duployan
  | Egyptian_Hieroglyphs
  | Elbasan
  | Elymaic
  | Ethiopic
  | Georgian
  | Glagolitic
  | Gothic
  | Grantha
  | Greek
  | Gujarati
  | Gunjala_Gondi
  | Gurmukhi
  | Han
  | Hangul
  | Hanifi_Rohingya
  | Hanunoo
  | Hatran
  | Hebrew
  | Hiragana
  | Imperial_Aramaic
  | Inherited
  | Inscriptional_Pahlavi
  | Inscriptional_Parthian
  | Javanese
  | Kaithi
  | Kannada
  | Katakana
  | Kayah_Li
  | Kharoshthi
  | Khitan_Small_Script
  | Khmer
  | Khojki
  | Khudawadi
  | Lao
  | Latin
  | Lepcha
  | Limbu
  | Linear_A
  | Linear_B
  | Lisu
  | Lycian
  | Lydian
  | Mahajani
  | Makasar
  | Malayalam
  | Mandaic
  | Manichaean
  | Marchen
  | Masaram_Gondi
  | Medefaidrin
  | Meetei_Mayek
  | Mende_Kikakui
  | Meroitic_Cursive
  | Meroitic_Hieroglyphs
  | Miao
  | Modi
  | Mongolian
  | Mro
  | Multani
  | Myanmar
  | Nabataean
  | Nandinagari
  | New_Tai_Lue
  | Newa
  | Nko
  | Nushu
  | Nyiakeng_Puachue_Hmong
  | Ogham
  | Ol_Chiki
  | Old_Hungarian
  | Old_Italic
  | Old_North_Arabian
  | Old_Permic
  | Old_Persian
  | Old_Sogdian
  | Old_South_Arabian
  | Old_Turkic
  | Oriya
  | Osage
  | Osmanya
  | Pahawh_Hmong
  | Palmyrene
  | Pau_Cin_Hau
  | Phags_Pa
  | Phoenician
  | Psalter_Pahlavi
  | Rejang
  | Runic
  | Samaritan
  | Saurashtra
  | Sharada
  | Shavian
  | Siddham
  | SignWriting
  | Sinhala
  | Sogdian
  | Sora_Sompeng
  | Soyombo
  | Sundanese
  | Syloti_Nagri
  | Syriac
  | Tagalog
  | Tagbanwa
  | Tai_Le
  | Tai_Tham
  | Tai_Viet
  | Takri
  | Tamil
  | Tangut
  | Telugu
  | Thaana
  | Thai
  | Tibetan
  | Tifinagh
  | Tirhuta
  | Ugaritic
  | Unknown
  | Vai
  | Wancho
  | Warang_Citi
  | Yezidi
  | Yi
  | Zanabazar_Square
  deriving (Eq, Ord, Enum, Bounded, Show, Read)
  deriving TextShow via (FromStringShow UnicodeScript)

instance ToField UnicodeScript where
  toField = toField . show

instance FromField UnicodeScript where
  parseField = fmap read . parseField

data UnicodeRange
  = UnicodeCodePoint !Int  -- ^ Single code point
  | UnicodeRange !Int !Int -- ^ A range

unicodeScript :: Char -> UnicodeScript
unicodeScript c
  | c <= '\x0040' = Common
  | c <= '\x005A' = Latin
  | c <= '\x0060' = Common
  | c <= '\x007A' = Latin
  | c <= '\x00A9' = Common
  | c == '\x00AA' = Latin
  | c <= '\x00B9' = Common
  | c == '\x00BA' = Latin
  | c <= '\x00BF' = Common
  | c <= '\x00D6' = Latin
  | c == '\x00D7' = Common
  | c <= '\x00F6' = Latin
  | c == '\x00F7' = Common
  | c <= '\x02B8' = Latin
  | c <= '\x02DF' = Common
  | c <= '\x02E4' = Latin
  | c <= '\x02E9' = Common
  | c <= '\x02EB' = Bopomofo
  | c <= '\x02FF' = Common
  | c <= '\x036F' = Inherited
  | c <= '\x0373' = Greek
  | c == '\x0374' = Common
  | c <= '\x0377' = Greek
  | c <= '\x0379' = Unknown
  | c <= '\x037D' = Greek
  | c == '\x037E' = Common
  | c == '\x037F' = Greek
  | c <= '\x0383' = Unknown
  | c == '\x0384' = Greek
  | c == '\x0385' = Common
  | c == '\x0386' = Greek
  | c == '\x0387' = Common
  | c <= '\x038A' = Greek
  | c == '\x038B' = Unknown
  | c == '\x038C' = Greek
  | c == '\x038D' = Unknown
  | c <= '\x03A1' = Greek
  | c == '\x03A2' = Unknown
  | c <= '\x03E1' = Greek
  | c <= '\x03EF' = Coptic
  | c <= '\x03FF' = Greek
  | c <= '\x0484' = Cyrillic
  | c <= '\x0486' = Inherited
  | c <= '\x052F' = Cyrillic
  | c == '\x0530' = Unknown
  | c <= '\x0556' = Armenian
  | c <= '\x0558' = Unknown
  | c <= '\x058A' = Armenian
  | c <= '\x058C' = Unknown
  | c <= '\x058F' = Armenian
  | c == '\x0590' = Unknown
  | c <= '\x05C7' = Hebrew
  | c <= '\x05CF' = Unknown
  | c <= '\x05EA' = Hebrew
  | c <= '\x05EE' = Unknown
  | c <= '\x05F4' = Hebrew
  | c <= '\x05FF' = Unknown
  | c <= '\x0604' = Arabic
  | c == '\x0605' = Common
  | c <= '\x060B' = Arabic
  | c == '\x060C' = Common
  | c <= '\x061A' = Arabic
  | c == '\x061B' = Common
  | c == '\x061C' = Arabic
  | c == '\x061D' = Unknown
  | c == '\x061E' = Arabic
  | c == '\x061F' = Common
  | c <= '\x063F' = Arabic
  | c == '\x0640' = Common
  | c <= '\x064A' = Arabic
  | c <= '\x0655' = Inherited
  | c <= '\x066F' = Arabic
  | c == '\x0670' = Inherited
  | c <= '\x06DC' = Arabic
  | c == '\x06DD' = Common
  | c <= '\x06FF' = Arabic
  | c <= '\x070D' = Syriac
  | c == '\x070E' = Unknown
  | c <= '\x074A' = Syriac
  | c <= '\x074C' = Unknown
  | c <= '\x074F' = Syriac
  | c <= '\x077F' = Arabic
  | c <= '\x07B1' = Thaana
  | c <= '\x07BF' = Unknown
  | c <= '\x07FA' = Nko
  | c <= '\x07FC' = Unknown
  | c <= '\x07FF' = Nko
  | c <= '\x082D' = Samaritan
  | c <= '\x082F' = Unknown
  | c <= '\x083E' = Samaritan
  | c == '\x083F' = Unknown
  | c <= '\x085B' = Mandaic
  | c <= '\x085D' = Unknown
  | c == '\x085E' = Mandaic
  | c == '\x085F' = Unknown
  | c <= '\x086A' = Syriac
  | c <= '\x089F' = Unknown
  | c <= '\x08B4' = Arabic
  | c == '\x08B5' = Unknown
  | c <= '\x08C7' = Arabic
  | c <= '\x08D2' = Unknown
  | c <= '\x08E1' = Arabic
  | c == '\x08E2' = Common
  | c <= '\x08FF' = Arabic
  | c <= '\x0950' = Devanagari
  | c <= '\x0954' = Inherited
  | c <= '\x0963' = Devanagari
  | c <= '\x0965' = Common
  | c <= '\x097F' = Devanagari
  | c <= '\x0983' = Bengali
  | c == '\x0984' = Unknown
  | c <= '\x098C' = Bengali
  | c <= '\x098E' = Unknown
  | c <= '\x0990' = Bengali
  | c <= '\x0992' = Unknown
  | c <= '\x09A8' = Bengali
  | c == '\x09A9' = Unknown
  | c <= '\x09B0' = Bengali
  | c == '\x09B1' = Unknown
  | c == '\x09B2' = Bengali
  | c <= '\x09B5' = Unknown
  | c <= '\x09B9' = Bengali
  | c <= '\x09BB' = Unknown
  | c <= '\x09C4' = Bengali
  | c <= '\x09C6' = Unknown
  | c <= '\x09C8' = Bengali
  | c <= '\x09CA' = Unknown
  | c <= '\x09CE' = Bengali
  | c <= '\x09D6' = Unknown
  | c == '\x09D7' = Bengali
  | c <= '\x09DB' = Unknown
  | c <= '\x09DD' = Bengali
  | c == '\x09DE' = Unknown
  | c <= '\x09E3' = Bengali
  | c <= '\x09E5' = Unknown
  | c <= '\x09FE' = Bengali
  | c <= '\x0A00' = Unknown
  | c <= '\x0A03' = Gurmukhi
  | c == '\x0A04' = Unknown
  | c <= '\x0A0A' = Gurmukhi
  | c <= '\x0A0E' = Unknown
  | c <= '\x0A10' = Gurmukhi
  | c <= '\x0A12' = Unknown
  | c <= '\x0A28' = Gurmukhi
  | c == '\x0A29' = Unknown
  | c <= '\x0A30' = Gurmukhi
  | c == '\x0A31' = Unknown
  | c <= '\x0A33' = Gurmukhi
  | c == '\x0A34' = Unknown
  | c <= '\x0A36' = Gurmukhi
  | c == '\x0A37' = Unknown
  | c <= '\x0A39' = Gurmukhi
  | c <= '\x0A3B' = Unknown
  | c == '\x0A3C' = Gurmukhi
  | c == '\x0A3D' = Unknown
  | c <= '\x0A42' = Gurmukhi
  | c <= '\x0A46' = Unknown
  | c <= '\x0A48' = Gurmukhi
  | c <= '\x0A4A' = Unknown
  | c <= '\x0A4D' = Gurmukhi
  | c <= '\x0A50' = Unknown
  | c == '\x0A51' = Gurmukhi
  | c <= '\x0A58' = Unknown
  | c <= '\x0A5C' = Gurmukhi
  | c == '\x0A5D' = Unknown
  | c == '\x0A5E' = Gurmukhi
  | c <= '\x0A65' = Unknown
  | c <= '\x0A76' = Gurmukhi
  | c <= '\x0A80' = Unknown
  | c <= '\x0A83' = Gujarati
  | c == '\x0A84' = Unknown
  | c <= '\x0A8D' = Gujarati
  | c == '\x0A8E' = Unknown
  | c <= '\x0A91' = Gujarati
  | c == '\x0A92' = Unknown
  | c <= '\x0AA8' = Gujarati
  | c == '\x0AA9' = Unknown
  | c <= '\x0AB0' = Gujarati
  | c == '\x0AB1' = Unknown
  | c <= '\x0AB3' = Gujarati
  | c == '\x0AB4' = Unknown
  | c <= '\x0AB9' = Gujarati
  | c <= '\x0ABB' = Unknown
  | c <= '\x0AC5' = Gujarati
  | c == '\x0AC6' = Unknown
  | c <= '\x0AC9' = Gujarati
  | c == '\x0ACA' = Unknown
  | c <= '\x0ACD' = Gujarati
  | c <= '\x0ACF' = Unknown
  | c == '\x0AD0' = Gujarati
  | c <= '\x0ADF' = Unknown
  | c <= '\x0AE3' = Gujarati
  | c <= '\x0AE5' = Unknown
  | c <= '\x0AF1' = Gujarati
  | c <= '\x0AF8' = Unknown
  | c <= '\x0AFF' = Gujarati
  | c == '\x0B00' = Unknown
  | c <= '\x0B03' = Oriya
  | c == '\x0B04' = Unknown
  | c <= '\x0B0C' = Oriya
  | c <= '\x0B0E' = Unknown
  | c <= '\x0B10' = Oriya
  | c <= '\x0B12' = Unknown
  | c <= '\x0B28' = Oriya
  | c == '\x0B29' = Unknown
  | c <= '\x0B30' = Oriya
  | c == '\x0B31' = Unknown
  | c <= '\x0B33' = Oriya
  | c == '\x0B34' = Unknown
  | c <= '\x0B39' = Oriya
  | c <= '\x0B3B' = Unknown
  | c <= '\x0B44' = Oriya
  | c <= '\x0B46' = Unknown
  | c <= '\x0B48' = Oriya
  | c <= '\x0B4A' = Unknown
  | c <= '\x0B4D' = Oriya
  | c <= '\x0B54' = Unknown
  | c <= '\x0B57' = Oriya
  | c <= '\x0B5B' = Unknown
  | c <= '\x0B5D' = Oriya
  | c == '\x0B5E' = Unknown
  | c <= '\x0B63' = Oriya
  | c <= '\x0B65' = Unknown
  | c <= '\x0B77' = Oriya
  | c <= '\x0B81' = Unknown
  | c <= '\x0B83' = Tamil
  | c == '\x0B84' = Unknown
  | c <= '\x0B8A' = Tamil
  | c <= '\x0B8D' = Unknown
  | c <= '\x0B90' = Tamil
  | c == '\x0B91' = Unknown
  | c <= '\x0B95' = Tamil
  | c <= '\x0B98' = Unknown
  | c <= '\x0B9A' = Tamil
  | c == '\x0B9B' = Unknown
  | c == '\x0B9C' = Tamil
  | c == '\x0B9D' = Unknown
  | c <= '\x0B9F' = Tamil
  | c <= '\x0BA2' = Unknown
  | c <= '\x0BA4' = Tamil
  | c <= '\x0BA7' = Unknown
  | c <= '\x0BAA' = Tamil
  | c <= '\x0BAD' = Unknown
  | c <= '\x0BB9' = Tamil
  | c <= '\x0BBD' = Unknown
  | c <= '\x0BC2' = Tamil
  | c <= '\x0BC5' = Unknown
  | c <= '\x0BC8' = Tamil
  | c == '\x0BC9' = Unknown
  | c <= '\x0BCD' = Tamil
  | c <= '\x0BCF' = Unknown
  | c == '\x0BD0' = Tamil
  | c <= '\x0BD6' = Unknown
  | c == '\x0BD7' = Tamil
  | c <= '\x0BE5' = Unknown
  | c <= '\x0BFA' = Tamil
  | c <= '\x0BFF' = Unknown
  | c <= '\x0C0C' = Telugu
  | c == '\x0C0D' = Unknown
  | c <= '\x0C10' = Telugu
  | c == '\x0C11' = Unknown
  | c <= '\x0C28' = Telugu
  | c == '\x0C29' = Unknown
  | c <= '\x0C39' = Telugu
  | c <= '\x0C3C' = Unknown
  | c <= '\x0C44' = Telugu
  | c == '\x0C45' = Unknown
  | c <= '\x0C48' = Telugu
  | c == '\x0C49' = Unknown
  | c <= '\x0C4D' = Telugu
  | c <= '\x0C54' = Unknown
  | c <= '\x0C56' = Telugu
  | c == '\x0C57' = Unknown
  | c <= '\x0C5A' = Telugu
  | c <= '\x0C5F' = Unknown
  | c <= '\x0C63' = Telugu
  | c <= '\x0C65' = Unknown
  | c <= '\x0C6F' = Telugu
  | c <= '\x0C76' = Unknown
  | c <= '\x0C7F' = Telugu
  | c <= '\x0C8C' = Kannada
  | c == '\x0C8D' = Unknown
  | c <= '\x0C90' = Kannada
  | c == '\x0C91' = Unknown
  | c <= '\x0CA8' = Kannada
  | c == '\x0CA9' = Unknown
  | c <= '\x0CB3' = Kannada
  | c == '\x0CB4' = Unknown
  | c <= '\x0CB9' = Kannada
  | c <= '\x0CBB' = Unknown
  | c <= '\x0CC4' = Kannada
  | c == '\x0CC5' = Unknown
  | c <= '\x0CC8' = Kannada
  | c == '\x0CC9' = Unknown
  | c <= '\x0CCD' = Kannada
  | c <= '\x0CD4' = Unknown
  | c <= '\x0CD6' = Kannada
  | c <= '\x0CDD' = Unknown
  | c == '\x0CDE' = Kannada
  | c == '\x0CDF' = Unknown
  | c <= '\x0CE3' = Kannada
  | c <= '\x0CE5' = Unknown
  | c <= '\x0CEF' = Kannada
  | c == '\x0CF0' = Unknown
  | c <= '\x0CF2' = Kannada
  | c <= '\x0CFF' = Unknown
  | c <= '\x0D0C' = Malayalam
  | c == '\x0D0D' = Unknown
  | c <= '\x0D10' = Malayalam
  | c == '\x0D11' = Unknown
  | c <= '\x0D44' = Malayalam
  | c == '\x0D45' = Unknown
  | c <= '\x0D48' = Malayalam
  | c == '\x0D49' = Unknown
  | c <= '\x0D4F' = Malayalam
  | c <= '\x0D53' = Unknown
  | c <= '\x0D63' = Malayalam
  | c <= '\x0D65' = Unknown
  | c <= '\x0D7F' = Malayalam
  | c == '\x0D80' = Unknown
  | c <= '\x0D83' = Sinhala
  | c == '\x0D84' = Unknown
  | c <= '\x0D96' = Sinhala
  | c <= '\x0D99' = Unknown
  | c <= '\x0DB1' = Sinhala
  | c == '\x0DB2' = Unknown
  | c <= '\x0DBB' = Sinhala
  | c == '\x0DBC' = Unknown
  | c == '\x0DBD' = Sinhala
  | c <= '\x0DBF' = Unknown
  | c <= '\x0DC6' = Sinhala
  | c <= '\x0DC9' = Unknown
  | c == '\x0DCA' = Sinhala
  | c <= '\x0DCE' = Unknown
  | c <= '\x0DD4' = Sinhala
  | c == '\x0DD5' = Unknown
  | c == '\x0DD6' = Sinhala
  | c == '\x0DD7' = Unknown
  | c <= '\x0DDF' = Sinhala
  | c <= '\x0DE5' = Unknown
  | c <= '\x0DEF' = Sinhala
  | c <= '\x0DF1' = Unknown
  | c <= '\x0DF4' = Sinhala
  | c <= '\x0E00' = Unknown
  | c <= '\x0E3A' = Thai
  | c <= '\x0E3E' = Unknown
  | c == '\x0E3F' = Common
  | c <= '\x0E5B' = Thai
  | c <= '\x0E80' = Unknown
  | c <= '\x0E82' = Lao
  | c == '\x0E83' = Unknown
  | c == '\x0E84' = Lao
  | c == '\x0E85' = Unknown
  | c <= '\x0E8A' = Lao
  | c == '\x0E8B' = Unknown
  | c <= '\x0EA3' = Lao
  | c == '\x0EA4' = Unknown
  | c == '\x0EA5' = Lao
  | c == '\x0EA6' = Unknown
  | c <= '\x0EBD' = Lao
  | c <= '\x0EBF' = Unknown
  | c <= '\x0EC4' = Lao
  | c == '\x0EC5' = Unknown
  | c == '\x0EC6' = Lao
  | c == '\x0EC7' = Unknown
  | c <= '\x0ECD' = Lao
  | c <= '\x0ECF' = Unknown
  | c <= '\x0ED9' = Lao
  | c <= '\x0EDB' = Unknown
  | c <= '\x0EDF' = Lao
  | c <= '\x0EFF' = Unknown
  | c <= '\x0F47' = Tibetan
  | c == '\x0F48' = Unknown
  | c <= '\x0F6C' = Tibetan
  | c <= '\x0F70' = Unknown
  | c <= '\x0F97' = Tibetan
  | c == '\x0F98' = Unknown
  | c <= '\x0FBC' = Tibetan
  | c == '\x0FBD' = Unknown
  | c <= '\x0FCC' = Tibetan
  | c == '\x0FCD' = Unknown
  | c <= '\x0FD4' = Tibetan
  | c <= '\x0FD8' = Common
  | c <= '\x0FDA' = Tibetan
  | c <= '\x0FFF' = Unknown
  | c <= '\x109F' = Myanmar
  | c <= '\x10C5' = Georgian
  | c == '\x10C6' = Unknown
  | c == '\x10C7' = Georgian
  | c <= '\x10CC' = Unknown
  | c == '\x10CD' = Georgian
  | c <= '\x10CF' = Unknown
  | c <= '\x10FA' = Georgian
  | c == '\x10FB' = Common
  | c <= '\x10FF' = Georgian
  | c <= '\x11FF' = Hangul
  | c <= '\x1248' = Ethiopic
  | c == '\x1249' = Unknown
  | c <= '\x124D' = Ethiopic
  | c <= '\x124F' = Unknown
  | c <= '\x1256' = Ethiopic
  | c == '\x1257' = Unknown
  | c == '\x1258' = Ethiopic
  | c == '\x1259' = Unknown
  | c <= '\x125D' = Ethiopic
  | c <= '\x125F' = Unknown
  | c <= '\x1288' = Ethiopic
  | c == '\x1289' = Unknown
  | c <= '\x128D' = Ethiopic
  | c <= '\x128F' = Unknown
  | c <= '\x12B0' = Ethiopic
  | c == '\x12B1' = Unknown
  | c <= '\x12B5' = Ethiopic
  | c <= '\x12B7' = Unknown
  | c <= '\x12BE' = Ethiopic
  | c == '\x12BF' = Unknown
  | c == '\x12C0' = Ethiopic
  | c == '\x12C1' = Unknown
  | c <= '\x12C5' = Ethiopic
  | c <= '\x12C7' = Unknown
  | c <= '\x12D6' = Ethiopic
  | c == '\x12D7' = Unknown
  | c <= '\x1310' = Ethiopic
  | c == '\x1311' = Unknown
  | c <= '\x1315' = Ethiopic
  | c <= '\x1317' = Unknown
  | c <= '\x135A' = Ethiopic
  | c <= '\x135C' = Unknown
  | c <= '\x137C' = Ethiopic
  | c <= '\x137F' = Unknown
  | c <= '\x1399' = Ethiopic
  | c <= '\x139F' = Unknown
  | c <= '\x13F5' = Cherokee
  | c <= '\x13F7' = Unknown
  | c <= '\x13FD' = Cherokee
  | c <= '\x13FF' = Unknown
  | c <= '\x167F' = Canadian_Aboriginal
  | c <= '\x169C' = Ogham
  | c <= '\x169F' = Unknown
  | c <= '\x16EA' = Runic
  | c <= '\x16ED' = Common
  | c <= '\x16F8' = Runic
  | c <= '\x16FF' = Unknown
  | c <= '\x170C' = Tagalog
  | c == '\x170D' = Unknown
  | c <= '\x1714' = Tagalog
  | c <= '\x171F' = Unknown
  | c <= '\x1734' = Hanunoo
  | c <= '\x1736' = Common
  | c <= '\x173F' = Unknown
  | c <= '\x1753' = Buhid
  | c <= '\x175F' = Unknown
  | c <= '\x176C' = Tagbanwa
  | c == '\x176D' = Unknown
  | c <= '\x1770' = Tagbanwa
  | c == '\x1771' = Unknown
  | c <= '\x1773' = Tagbanwa
  | c <= '\x177F' = Unknown
  | c <= '\x17DD' = Khmer
  | c <= '\x17DF' = Unknown
  | c <= '\x17E9' = Khmer
  | c <= '\x17EF' = Unknown
  | c <= '\x17F9' = Khmer
  | c <= '\x17FF' = Unknown
  | c <= '\x1801' = Mongolian
  | c <= '\x1803' = Common
  | c == '\x1804' = Mongolian
  | c == '\x1805' = Common
  | c <= '\x180E' = Mongolian
  | c == '\x180F' = Unknown
  | c <= '\x1819' = Mongolian
  | c <= '\x181F' = Unknown
  | c <= '\x1878' = Mongolian
  | c <= '\x187F' = Unknown
  | c <= '\x18AA' = Mongolian
  | c <= '\x18AF' = Unknown
  | c <= '\x18F5' = Canadian_Aboriginal
  | c <= '\x18FF' = Unknown
  | c <= '\x191E' = Limbu
  | c == '\x191F' = Unknown
  | c <= '\x192B' = Limbu
  | c <= '\x192F' = Unknown
  | c <= '\x193B' = Limbu
  | c <= '\x193F' = Unknown
  | c == '\x1940' = Limbu
  | c <= '\x1943' = Unknown
  | c <= '\x194F' = Limbu
  | c <= '\x196D' = Tai_Le
  | c <= '\x196F' = Unknown
  | c <= '\x1974' = Tai_Le
  | c <= '\x197F' = Unknown
  | c <= '\x19AB' = New_Tai_Lue
  | c <= '\x19AF' = Unknown
  | c <= '\x19C9' = New_Tai_Lue
  | c <= '\x19CF' = Unknown
  | c <= '\x19DA' = New_Tai_Lue
  | c <= '\x19DD' = Unknown
  | c <= '\x19DF' = New_Tai_Lue
  | c <= '\x19FF' = Khmer
  | c <= '\x1A1B' = Buginese
  | c <= '\x1A1D' = Unknown
  | c <= '\x1A1F' = Buginese
  | c <= '\x1A5E' = Tai_Tham
  | c == '\x1A5F' = Unknown
  | c <= '\x1A7C' = Tai_Tham
  | c <= '\x1A7E' = Unknown
  | c <= '\x1A89' = Tai_Tham
  | c <= '\x1A8F' = Unknown
  | c <= '\x1A99' = Tai_Tham
  | c <= '\x1A9F' = Unknown
  | c <= '\x1AAD' = Tai_Tham
  | c <= '\x1AAF' = Unknown
  | c <= '\x1AC0' = Inherited
  | c <= '\x1AFF' = Unknown
  | c <= '\x1B4B' = Balinese
  | c <= '\x1B4F' = Unknown
  | c <= '\x1B7C' = Balinese
  | c <= '\x1B7F' = Unknown
  | c <= '\x1BBF' = Sundanese
  | c <= '\x1BF3' = Batak
  | c <= '\x1BFB' = Unknown
  | c <= '\x1BFF' = Batak
  | c <= '\x1C37' = Lepcha
  | c <= '\x1C3A' = Unknown
  | c <= '\x1C49' = Lepcha
  | c <= '\x1C4C' = Unknown
  | c <= '\x1C4F' = Lepcha
  | c <= '\x1C7F' = Ol_Chiki
  | c <= '\x1C88' = Cyrillic
  | c <= '\x1C8F' = Unknown
  | c <= '\x1CBA' = Georgian
  | c <= '\x1CBC' = Unknown
  | c <= '\x1CBF' = Georgian
  | c <= '\x1CC7' = Sundanese
  | c <= '\x1CCF' = Unknown
  | c <= '\x1CD2' = Inherited
  | c == '\x1CD3' = Common
  | c <= '\x1CE0' = Inherited
  | c == '\x1CE1' = Common
  | c <= '\x1CE8' = Inherited
  | c <= '\x1CEC' = Common
  | c == '\x1CED' = Inherited
  | c <= '\x1CF3' = Common
  | c == '\x1CF4' = Inherited
  | c <= '\x1CF7' = Common
  | c <= '\x1CF9' = Inherited
  | c == '\x1CFA' = Common
  | c <= '\x1CFF' = Unknown
  | c <= '\x1D25' = Latin
  | c <= '\x1D2A' = Greek
  | c == '\x1D2B' = Cyrillic
  | c <= '\x1D5C' = Latin
  | c <= '\x1D61' = Greek
  | c <= '\x1D65' = Latin
  | c <= '\x1D6A' = Greek
  | c <= '\x1D77' = Latin
  | c == '\x1D78' = Cyrillic
  | c <= '\x1DBE' = Latin
  | c == '\x1DBF' = Greek
  | c <= '\x1DF9' = Inherited
  | c == '\x1DFA' = Unknown
  | c <= '\x1DFF' = Inherited
  | c <= '\x1EFF' = Latin
  | c <= '\x1F15' = Greek
  | c <= '\x1F17' = Unknown
  | c <= '\x1F1D' = Greek
  | c <= '\x1F1F' = Unknown
  | c <= '\x1F45' = Greek
  | c <= '\x1F47' = Unknown
  | c <= '\x1F4D' = Greek
  | c <= '\x1F4F' = Unknown
  | c <= '\x1F57' = Greek
  | c == '\x1F58' = Unknown
  | c == '\x1F59' = Greek
  | c == '\x1F5A' = Unknown
  | c == '\x1F5B' = Greek
  | c == '\x1F5C' = Unknown
  | c == '\x1F5D' = Greek
  | c == '\x1F5E' = Unknown
  | c <= '\x1F7D' = Greek
  | c <= '\x1F7F' = Unknown
  | c <= '\x1FB4' = Greek
  | c == '\x1FB5' = Unknown
  | c <= '\x1FC4' = Greek
  | c == '\x1FC5' = Unknown
  | c <= '\x1FD3' = Greek
  | c <= '\x1FD5' = Unknown
  | c <= '\x1FDB' = Greek
  | c == '\x1FDC' = Unknown
  | c <= '\x1FEF' = Greek
  | c <= '\x1FF1' = Unknown
  | c <= '\x1FF4' = Greek
  | c == '\x1FF5' = Unknown
  | c <= '\x1FFE' = Greek
  | c == '\x1FFF' = Unknown
  | c <= '\x200B' = Common
  | c <= '\x200D' = Inherited
  | c <= '\x2064' = Common
  | c == '\x2065' = Unknown
  | c <= '\x2070' = Common
  | c == '\x2071' = Latin
  | c <= '\x2073' = Unknown
  | c <= '\x207E' = Common
  | c == '\x207F' = Latin
  | c <= '\x208E' = Common
  | c == '\x208F' = Unknown
  | c <= '\x209C' = Latin
  | c <= '\x209F' = Unknown
  | c <= '\x20BF' = Common
  | c <= '\x20CF' = Unknown
  | c <= '\x20F0' = Inherited
  | c <= '\x20FF' = Unknown
  | c <= '\x2125' = Common
  | c == '\x2126' = Greek
  | c <= '\x2129' = Common
  | c <= '\x212B' = Latin
  | c <= '\x2131' = Common
  | c == '\x2132' = Latin
  | c <= '\x214D' = Common
  | c == '\x214E' = Latin
  | c <= '\x215F' = Common
  | c <= '\x2188' = Latin
  | c <= '\x218B' = Common
  | c <= '\x218F' = Unknown
  | c <= '\x2426' = Common
  | c <= '\x243F' = Unknown
  | c <= '\x244A' = Common
  | c <= '\x245F' = Unknown
  | c <= '\x27FF' = Common
  | c <= '\x28FF' = Braille
  | c <= '\x2B73' = Common
  | c <= '\x2B75' = Unknown
  | c <= '\x2B95' = Common
  | c == '\x2B96' = Unknown
  | c <= '\x2BFF' = Common
  | c <= '\x2C2E' = Glagolitic
  | c == '\x2C2F' = Unknown
  | c <= '\x2C5E' = Glagolitic
  | c == '\x2C5F' = Unknown
  | c <= '\x2C7F' = Latin
  | c <= '\x2CF3' = Coptic
  | c <= '\x2CF8' = Unknown
  | c <= '\x2CFF' = Coptic
  | c <= '\x2D25' = Georgian
  | c == '\x2D26' = Unknown
  | c == '\x2D27' = Georgian
  | c <= '\x2D2C' = Unknown
  | c == '\x2D2D' = Georgian
  | c <= '\x2D2F' = Unknown
  | c <= '\x2D67' = Tifinagh
  | c <= '\x2D6E' = Unknown
  | c <= '\x2D70' = Tifinagh
  | c <= '\x2D7E' = Unknown
  | c == '\x2D7F' = Tifinagh
  | c <= '\x2D96' = Ethiopic
  | c <= '\x2D9F' = Unknown
  | c <= '\x2DA6' = Ethiopic
  | c == '\x2DA7' = Unknown
  | c <= '\x2DAE' = Ethiopic
  | c == '\x2DAF' = Unknown
  | c <= '\x2DB6' = Ethiopic
  | c == '\x2DB7' = Unknown
  | c <= '\x2DBE' = Ethiopic
  | c == '\x2DBF' = Unknown
  | c <= '\x2DC6' = Ethiopic
  | c == '\x2DC7' = Unknown
  | c <= '\x2DCE' = Ethiopic
  | c == '\x2DCF' = Unknown
  | c <= '\x2DD6' = Ethiopic
  | c == '\x2DD7' = Unknown
  | c <= '\x2DDE' = Ethiopic
  | c == '\x2DDF' = Unknown
  | c <= '\x2DFF' = Cyrillic
  | c <= '\x2E52' = Common
  | c <= '\x2E7F' = Unknown
  | c <= '\x2E99' = Han
  | c == '\x2E9A' = Unknown
  | c <= '\x2EF3' = Han
  | c <= '\x2EFF' = Unknown
  | c <= '\x2FD5' = Han
  | c <= '\x2FEF' = Unknown
  | c <= '\x2FFB' = Common
  | c <= '\x2FFF' = Unknown
  | c <= '\x3004' = Common
  | c == '\x3005' = Han
  | c == '\x3006' = Common
  | c == '\x3007' = Han
  | c <= '\x3020' = Common
  | c <= '\x3029' = Han
  | c <= '\x302D' = Inherited
  | c <= '\x302F' = Hangul
  | c <= '\x3037' = Common
  | c <= '\x303B' = Han
  | c <= '\x303F' = Common
  | c == '\x3040' = Unknown
  | c <= '\x3096' = Hiragana
  | c <= '\x3098' = Unknown
  | c <= '\x309A' = Inherited
  | c <= '\x309C' = Common
  | c <= '\x309F' = Hiragana
  | c == '\x30A0' = Common
  | c <= '\x30FA' = Katakana
  | c <= '\x30FC' = Common
  | c <= '\x30FF' = Katakana
  | c <= '\x3104' = Unknown
  | c <= '\x312F' = Bopomofo
  | c == '\x3130' = Unknown
  | c <= '\x318E' = Hangul
  | c == '\x318F' = Unknown
  | c <= '\x319F' = Common
  | c <= '\x31BF' = Bopomofo
  | c <= '\x31E3' = Common
  | c <= '\x31EF' = Unknown
  | c <= '\x31FF' = Katakana
  | c <= '\x321E' = Hangul
  | c == '\x321F' = Unknown
  | c <= '\x325F' = Common
  | c <= '\x327E' = Hangul
  | c <= '\x32CF' = Common
  | c <= '\x32FE' = Katakana
  | c == '\x32FF' = Common
  | c <= '\x3357' = Katakana
  | c <= '\x33FF' = Common
  | c <= '\x4DBF' = Han
  | c <= '\x4DFF' = Common
  | c <= '\x9FFC' = Han
  | c <= '\x9FFF' = Unknown
  | c <= '\xA48C' = Yi
  | c <= '\xA48F' = Unknown
  | c <= '\xA4C6' = Yi
  | c <= '\xA4CF' = Unknown
  | c <= '\xA4FF' = Lisu
  | c <= '\xA62B' = Vai
  | c <= '\xA63F' = Unknown
  | c <= '\xA69F' = Cyrillic
  | c <= '\xA6F7' = Bamum
  | c <= '\xA6FF' = Unknown
  | c <= '\xA721' = Common
  | c <= '\xA787' = Latin
  | c <= '\xA78A' = Common
  | c <= '\xA7BF' = Latin
  | c <= '\xA7C1' = Unknown
  | c <= '\xA7CA' = Latin
  | c <= '\xA7F4' = Unknown
  | c <= '\xA7FF' = Latin
  | c <= '\xA82C' = Syloti_Nagri
  | c <= '\xA82F' = Unknown
  | c <= '\xA839' = Common
  | c <= '\xA83F' = Unknown
  | c <= '\xA877' = Phags_Pa
  | c <= '\xA87F' = Unknown
  | c <= '\xA8C5' = Saurashtra
  | c <= '\xA8CD' = Unknown
  | c <= '\xA8D9' = Saurashtra
  | c <= '\xA8DF' = Unknown
  | c <= '\xA8FF' = Devanagari
  | c <= '\xA92D' = Kayah_Li
  | c == '\xA92E' = Common
  | c == '\xA92F' = Kayah_Li
  | c <= '\xA953' = Rejang
  | c <= '\xA95E' = Unknown
  | c == '\xA95F' = Rejang
  | c <= '\xA97C' = Hangul
  | c <= '\xA97F' = Unknown
  | c <= '\xA9CD' = Javanese
  | c == '\xA9CE' = Unknown
  | c == '\xA9CF' = Common
  | c <= '\xA9D9' = Javanese
  | c <= '\xA9DD' = Unknown
  | c <= '\xA9DF' = Javanese
  | c <= '\xA9FE' = Myanmar
  | c == '\xA9FF' = Unknown
  | c <= '\xAA36' = Cham
  | c <= '\xAA3F' = Unknown
  | c <= '\xAA4D' = Cham
  | c <= '\xAA4F' = Unknown
  | c <= '\xAA59' = Cham
  | c <= '\xAA5B' = Unknown
  | c <= '\xAA5F' = Cham
  | c <= '\xAA7F' = Myanmar
  | c <= '\xAAC2' = Tai_Viet
  | c <= '\xAADA' = Unknown
  | c <= '\xAADF' = Tai_Viet
  | c <= '\xAAF6' = Meetei_Mayek
  | c <= '\xAB00' = Unknown
  | c <= '\xAB06' = Ethiopic
  | c <= '\xAB08' = Unknown
  | c <= '\xAB0E' = Ethiopic
  | c <= '\xAB10' = Unknown
  | c <= '\xAB16' = Ethiopic
  | c <= '\xAB1F' = Unknown
  | c <= '\xAB26' = Ethiopic
  | c == '\xAB27' = Unknown
  | c <= '\xAB2E' = Ethiopic
  | c == '\xAB2F' = Unknown
  | c <= '\xAB5A' = Latin
  | c == '\xAB5B' = Common
  | c <= '\xAB64' = Latin
  | c == '\xAB65' = Greek
  | c <= '\xAB69' = Latin
  | c <= '\xAB6B' = Common
  | c <= '\xAB6F' = Unknown
  | c <= '\xABBF' = Cherokee
  | c <= '\xABED' = Meetei_Mayek
  | c <= '\xABEF' = Unknown
  | c <= '\xABF9' = Meetei_Mayek
  | c <= '\xABFF' = Unknown
  | c <= '\xD7A3' = Hangul
  | c <= '\xD7AF' = Unknown
  | c <= '\xD7C6' = Hangul
  | c <= '\xD7CA' = Unknown
  | c <= '\xD7FB' = Hangul
  | c <= '\xF8FF' = Unknown
  | c <= '\xFA6D' = Han
  | c <= '\xFA6F' = Unknown
  | c <= '\xFAD9' = Han
  | c <= '\xFAFF' = Unknown
  | c <= '\xFB06' = Latin
  | c <= '\xFB12' = Unknown
  | c <= '\xFB17' = Armenian
  | c <= '\xFB1C' = Unknown
  | c <= '\xFB36' = Hebrew
  | c == '\xFB37' = Unknown
  | c <= '\xFB3C' = Hebrew
  | c == '\xFB3D' = Unknown
  | c == '\xFB3E' = Hebrew
  | c == '\xFB3F' = Unknown
  | c <= '\xFB41' = Hebrew
  | c == '\xFB42' = Unknown
  | c <= '\xFB44' = Hebrew
  | c == '\xFB45' = Unknown
  | c <= '\xFB4F' = Hebrew
  | c <= '\xFBC1' = Arabic
  | c <= '\xFBD2' = Unknown
  | c <= '\xFD3D' = Arabic
  | c <= '\xFD3F' = Common
  | c <= '\xFD4F' = Unknown
  | c <= '\xFD8F' = Arabic
  | c <= '\xFD91' = Unknown
  | c <= '\xFDC7' = Arabic
  | c <= '\xFDEF' = Unknown
  | c <= '\xFDFD' = Arabic
  | c <= '\xFDFF' = Unknown
  | c <= '\xFE0F' = Inherited
  | c <= '\xFE19' = Common
  | c <= '\xFE1F' = Unknown
  | c <= '\xFE2D' = Inherited
  | c <= '\xFE2F' = Cyrillic
  | c <= '\xFE52' = Common
  | c == '\xFE53' = Unknown
  | c <= '\xFE66' = Common
  | c == '\xFE67' = Unknown
  | c <= '\xFE6B' = Common
  | c <= '\xFE6F' = Unknown
  | c <= '\xFE74' = Arabic
  | c == '\xFE75' = Unknown
  | c <= '\xFEFC' = Arabic
  | c <= '\xFEFE' = Unknown
  | c == '\xFEFF' = Common
  | c == '\xFF00' = Unknown
  | c <= '\xFF20' = Common
  | c <= '\xFF3A' = Latin
  | c <= '\xFF40' = Common
  | c <= '\xFF5A' = Latin
  | c <= '\xFF65' = Common
  | c <= '\xFF6F' = Katakana
  | c == '\xFF70' = Common
  | c <= '\xFF9D' = Katakana
  | c <= '\xFF9F' = Common
  | c <= '\xFFBE' = Hangul
  | c <= '\xFFC1' = Unknown
  | c <= '\xFFC7' = Hangul
  | c <= '\xFFC9' = Unknown
  | c <= '\xFFCF' = Hangul
  | c <= '\xFFD1' = Unknown
  | c <= '\xFFD7' = Hangul
  | c <= '\xFFD9' = Unknown
  | c <= '\xFFDC' = Hangul
  | c <= '\xFFDF' = Unknown
  | c <= '\xFFE6' = Common
  | c == '\xFFE7' = Unknown
  | c <= '\xFFEE' = Common
  | c <= '\xFFF8' = Unknown
  | c <= '\xFFFD' = Common
  | c <= '\xFFFF' = Unknown
  | c <= '\x1000B' = Linear_B
  | c == '\x1000C' = Unknown
  | c <= '\x10026' = Linear_B
  | c == '\x10027' = Unknown
  | c <= '\x1003A' = Linear_B
  | c == '\x1003B' = Unknown
  | c <= '\x1003D' = Linear_B
  | c == '\x1003E' = Unknown
  | c <= '\x1004D' = Linear_B
  | c <= '\x1004F' = Unknown
  | c <= '\x1005D' = Linear_B
  | c <= '\x1007F' = Unknown
  | c <= '\x100FA' = Linear_B
  | c <= '\x100FF' = Unknown
  | c <= '\x10102' = Common
  | c <= '\x10106' = Unknown
  | c <= '\x10133' = Common
  | c <= '\x10136' = Unknown
  | c <= '\x1013F' = Common
  | c <= '\x1018E' = Greek
  | c == '\x1018F' = Unknown
  | c <= '\x1019C' = Common
  | c <= '\x1019F' = Unknown
  | c == '\x101A0' = Greek
  | c <= '\x101CF' = Unknown
  | c <= '\x101FC' = Common
  | c == '\x101FD' = Inherited
  | c <= '\x1027F' = Unknown
  | c <= '\x1029C' = Lycian
  | c <= '\x1029F' = Unknown
  | c <= '\x102D0' = Carian
  | c <= '\x102DF' = Unknown
  | c == '\x102E0' = Inherited
  | c <= '\x102FB' = Common
  | c <= '\x102FF' = Unknown
  | c <= '\x10323' = Old_Italic
  | c <= '\x1032C' = Unknown
  | c <= '\x1032F' = Old_Italic
  | c <= '\x1034A' = Gothic
  | c <= '\x1034F' = Unknown
  | c <= '\x1037A' = Old_Permic
  | c <= '\x1037F' = Unknown
  | c <= '\x1039D' = Ugaritic
  | c == '\x1039E' = Unknown
  | c == '\x1039F' = Ugaritic
  | c <= '\x103C3' = Old_Persian
  | c <= '\x103C7' = Unknown
  | c <= '\x103D5' = Old_Persian
  | c <= '\x103FF' = Unknown
  | c <= '\x1044F' = Deseret
  | c <= '\x1047F' = Shavian
  | c <= '\x1049D' = Osmanya
  | c <= '\x1049F' = Unknown
  | c <= '\x104A9' = Osmanya
  | c <= '\x104AF' = Unknown
  | c <= '\x104D3' = Osage
  | c <= '\x104D7' = Unknown
  | c <= '\x104FB' = Osage
  | c <= '\x104FF' = Unknown
  | c <= '\x10527' = Elbasan
  | c <= '\x1052F' = Unknown
  | c <= '\x10563' = Caucasian_Albanian
  | c <= '\x1056E' = Unknown
  | c == '\x1056F' = Caucasian_Albanian
  | c <= '\x105FF' = Unknown
  | c <= '\x10736' = Linear_A
  | c <= '\x1073F' = Unknown
  | c <= '\x10755' = Linear_A
  | c <= '\x1075F' = Unknown
  | c <= '\x10767' = Linear_A
  | c <= '\x107FF' = Unknown
  | c <= '\x10805' = Cypriot
  | c <= '\x10807' = Unknown
  | c == '\x10808' = Cypriot
  | c == '\x10809' = Unknown
  | c <= '\x10835' = Cypriot
  | c == '\x10836' = Unknown
  | c <= '\x10838' = Cypriot
  | c <= '\x1083B' = Unknown
  | c == '\x1083C' = Cypriot
  | c <= '\x1083E' = Unknown
  | c == '\x1083F' = Cypriot
  | c <= '\x10855' = Imperial_Aramaic
  | c == '\x10856' = Unknown
  | c <= '\x1085F' = Imperial_Aramaic
  | c <= '\x1087F' = Palmyrene
  | c <= '\x1089E' = Nabataean
  | c <= '\x108A6' = Unknown
  | c <= '\x108AF' = Nabataean
  | c <= '\x108DF' = Unknown
  | c <= '\x108F2' = Hatran
  | c == '\x108F3' = Unknown
  | c <= '\x108F5' = Hatran
  | c <= '\x108FA' = Unknown
  | c <= '\x108FF' = Hatran
  | c <= '\x1091B' = Phoenician
  | c <= '\x1091E' = Unknown
  | c == '\x1091F' = Phoenician
  | c <= '\x10939' = Lydian
  | c <= '\x1093E' = Unknown
  | c == '\x1093F' = Lydian
  | c <= '\x1097F' = Unknown
  | c <= '\x1099F' = Meroitic_Hieroglyphs
  | c <= '\x109B7' = Meroitic_Cursive
  | c <= '\x109BB' = Unknown
  | c <= '\x109CF' = Meroitic_Cursive
  | c <= '\x109D1' = Unknown
  | c <= '\x109FF' = Meroitic_Cursive
  | c <= '\x10A03' = Kharoshthi
  | c == '\x10A04' = Unknown
  | c <= '\x10A06' = Kharoshthi
  | c <= '\x10A0B' = Unknown
  | c <= '\x10A13' = Kharoshthi
  | c == '\x10A14' = Unknown
  | c <= '\x10A17' = Kharoshthi
  | c == '\x10A18' = Unknown
  | c <= '\x10A35' = Kharoshthi
  | c <= '\x10A37' = Unknown
  | c <= '\x10A3A' = Kharoshthi
  | c <= '\x10A3E' = Unknown
  | c <= '\x10A48' = Kharoshthi
  | c <= '\x10A4F' = Unknown
  | c <= '\x10A58' = Kharoshthi
  | c <= '\x10A5F' = Unknown
  | c <= '\x10A7F' = Old_South_Arabian
  | c <= '\x10A9F' = Old_North_Arabian
  | c <= '\x10ABF' = Unknown
  | c <= '\x10AE6' = Manichaean
  | c <= '\x10AEA' = Unknown
  | c <= '\x10AF6' = Manichaean
  | c <= '\x10AFF' = Unknown
  | c <= '\x10B35' = Avestan
  | c <= '\x10B38' = Unknown
  | c <= '\x10B3F' = Avestan
  | c <= '\x10B55' = Inscriptional_Parthian
  | c <= '\x10B57' = Unknown
  | c <= '\x10B5F' = Inscriptional_Parthian
  | c <= '\x10B72' = Inscriptional_Pahlavi
  | c <= '\x10B77' = Unknown
  | c <= '\x10B7F' = Inscriptional_Pahlavi
  | c <= '\x10B91' = Psalter_Pahlavi
  | c <= '\x10B98' = Unknown
  | c <= '\x10B9C' = Psalter_Pahlavi
  | c <= '\x10BA8' = Unknown
  | c <= '\x10BAF' = Psalter_Pahlavi
  | c <= '\x10BFF' = Unknown
  | c <= '\x10C48' = Old_Turkic
  | c <= '\x10C7F' = Unknown
  | c <= '\x10CB2' = Old_Hungarian
  | c <= '\x10CBF' = Unknown
  | c <= '\x10CF2' = Old_Hungarian
  | c <= '\x10CF9' = Unknown
  | c <= '\x10CFF' = Old_Hungarian
  | c <= '\x10D27' = Hanifi_Rohingya
  | c <= '\x10D2F' = Unknown
  | c <= '\x10D39' = Hanifi_Rohingya
  | c <= '\x10E5F' = Unknown
  | c <= '\x10E7E' = Arabic
  | c == '\x10E7F' = Unknown
  | c <= '\x10EA9' = Yezidi
  | c == '\x10EAA' = Unknown
  | c <= '\x10EAD' = Yezidi
  | c <= '\x10EAF' = Unknown
  | c <= '\x10EB1' = Yezidi
  | c <= '\x10EFF' = Unknown
  | c <= '\x10F27' = Old_Sogdian
  | c <= '\x10F2F' = Unknown
  | c <= '\x10F59' = Sogdian
  | c <= '\x10FAF' = Unknown
  | c <= '\x10FCB' = Chorasmian
  | c <= '\x10FDF' = Unknown
  | c <= '\x10FF6' = Elymaic
  | c <= '\x10FFF' = Unknown
  | c <= '\x1104D' = Brahmi
  | c <= '\x11051' = Unknown
  | c <= '\x1106F' = Brahmi
  | c <= '\x1107E' = Unknown
  | c == '\x1107F' = Brahmi
  | c <= '\x110C1' = Kaithi
  | c <= '\x110CC' = Unknown
  | c == '\x110CD' = Kaithi
  | c <= '\x110CF' = Unknown
  | c <= '\x110E8' = Sora_Sompeng
  | c <= '\x110EF' = Unknown
  | c <= '\x110F9' = Sora_Sompeng
  | c <= '\x110FF' = Unknown
  | c <= '\x11134' = Chakma
  | c == '\x11135' = Unknown
  | c <= '\x11147' = Chakma
  | c <= '\x1114F' = Unknown
  | c <= '\x11176' = Mahajani
  | c <= '\x1117F' = Unknown
  | c <= '\x111DF' = Sharada
  | c == '\x111E0' = Unknown
  | c <= '\x111F4' = Sinhala
  | c <= '\x111FF' = Unknown
  | c <= '\x11211' = Khojki
  | c == '\x11212' = Unknown
  | c <= '\x1123E' = Khojki
  | c <= '\x1127F' = Unknown
  | c <= '\x11286' = Multani
  | c == '\x11287' = Unknown
  | c == '\x11288' = Multani
  | c == '\x11289' = Unknown
  | c <= '\x1128D' = Multani
  | c == '\x1128E' = Unknown
  | c <= '\x1129D' = Multani
  | c == '\x1129E' = Unknown
  | c <= '\x112A9' = Multani
  | c <= '\x112AF' = Unknown
  | c <= '\x112EA' = Khudawadi
  | c <= '\x112EF' = Unknown
  | c <= '\x112F9' = Khudawadi
  | c <= '\x112FF' = Unknown
  | c <= '\x11303' = Grantha
  | c == '\x11304' = Unknown
  | c <= '\x1130C' = Grantha
  | c <= '\x1130E' = Unknown
  | c <= '\x11310' = Grantha
  | c <= '\x11312' = Unknown
  | c <= '\x11328' = Grantha
  | c == '\x11329' = Unknown
  | c <= '\x11330' = Grantha
  | c == '\x11331' = Unknown
  | c <= '\x11333' = Grantha
  | c == '\x11334' = Unknown
  | c <= '\x11339' = Grantha
  | c == '\x1133A' = Unknown
  | c == '\x1133B' = Inherited
  | c <= '\x11344' = Grantha
  | c <= '\x11346' = Unknown
  | c <= '\x11348' = Grantha
  | c <= '\x1134A' = Unknown
  | c <= '\x1134D' = Grantha
  | c <= '\x1134F' = Unknown
  | c == '\x11350' = Grantha
  | c <= '\x11356' = Unknown
  | c == '\x11357' = Grantha
  | c <= '\x1135C' = Unknown
  | c <= '\x11363' = Grantha
  | c <= '\x11365' = Unknown
  | c <= '\x1136C' = Grantha
  | c <= '\x1136F' = Unknown
  | c <= '\x11374' = Grantha
  | c <= '\x113FF' = Unknown
  | c <= '\x1145B' = Newa
  | c == '\x1145C' = Unknown
  | c <= '\x11461' = Newa
  | c <= '\x1147F' = Unknown
  | c <= '\x114C7' = Tirhuta
  | c <= '\x114CF' = Unknown
  | c <= '\x114D9' = Tirhuta
  | c <= '\x1157F' = Unknown
  | c <= '\x115B5' = Siddham
  | c <= '\x115B7' = Unknown
  | c <= '\x115DD' = Siddham
  | c <= '\x115FF' = Unknown
  | c <= '\x11644' = Modi
  | c <= '\x1164F' = Unknown
  | c <= '\x11659' = Modi
  | c <= '\x1165F' = Unknown
  | c <= '\x1166C' = Mongolian
  | c <= '\x1167F' = Unknown
  | c <= '\x116B8' = Takri
  | c <= '\x116BF' = Unknown
  | c <= '\x116C9' = Takri
  | c <= '\x116FF' = Unknown
  | c <= '\x1171A' = Ahom
  | c <= '\x1171C' = Unknown
  | c <= '\x1172B' = Ahom
  | c <= '\x1172F' = Unknown
  | c <= '\x1173F' = Ahom
  | c <= '\x117FF' = Unknown
  | c <= '\x1183B' = Dogra
  | c <= '\x1189F' = Unknown
  | c <= '\x118F2' = Warang_Citi
  | c <= '\x118FE' = Unknown
  | c == '\x118FF' = Warang_Citi
  | c <= '\x11906' = Dives_Akuru
  | c <= '\x11908' = Unknown
  | c == '\x11909' = Dives_Akuru
  | c <= '\x1190B' = Unknown
  | c <= '\x11913' = Dives_Akuru
  | c == '\x11914' = Unknown
  | c <= '\x11916' = Dives_Akuru
  | c == '\x11917' = Unknown
  | c <= '\x11935' = Dives_Akuru
  | c == '\x11936' = Unknown
  | c <= '\x11938' = Dives_Akuru
  | c <= '\x1193A' = Unknown
  | c <= '\x11946' = Dives_Akuru
  | c <= '\x1194F' = Unknown
  | c <= '\x11959' = Dives_Akuru
  | c <= '\x1199F' = Unknown
  | c <= '\x119A7' = Nandinagari
  | c <= '\x119A9' = Unknown
  | c <= '\x119D7' = Nandinagari
  | c <= '\x119D9' = Unknown
  | c <= '\x119E4' = Nandinagari
  | c <= '\x119FF' = Unknown
  | c <= '\x11A47' = Zanabazar_Square
  | c <= '\x11A4F' = Unknown
  | c <= '\x11AA2' = Soyombo
  | c <= '\x11ABF' = Unknown
  | c <= '\x11AF8' = Pau_Cin_Hau
  | c <= '\x11BFF' = Unknown
  | c <= '\x11C08' = Bhaiksuki
  | c == '\x11C09' = Unknown
  | c <= '\x11C36' = Bhaiksuki
  | c == '\x11C37' = Unknown
  | c <= '\x11C45' = Bhaiksuki
  | c <= '\x11C4F' = Unknown
  | c <= '\x11C6C' = Bhaiksuki
  | c <= '\x11C6F' = Unknown
  | c <= '\x11C8F' = Marchen
  | c <= '\x11C91' = Unknown
  | c <= '\x11CA7' = Marchen
  | c == '\x11CA8' = Unknown
  | c <= '\x11CB6' = Marchen
  | c <= '\x11CFF' = Unknown
  | c <= '\x11D06' = Masaram_Gondi
  | c == '\x11D07' = Unknown
  | c <= '\x11D09' = Masaram_Gondi
  | c == '\x11D0A' = Unknown
  | c <= '\x11D36' = Masaram_Gondi
  | c <= '\x11D39' = Unknown
  | c == '\x11D3A' = Masaram_Gondi
  | c == '\x11D3B' = Unknown
  | c <= '\x11D3D' = Masaram_Gondi
  | c == '\x11D3E' = Unknown
  | c <= '\x11D47' = Masaram_Gondi
  | c <= '\x11D4F' = Unknown
  | c <= '\x11D59' = Masaram_Gondi
  | c <= '\x11D5F' = Unknown
  | c <= '\x11D65' = Gunjala_Gondi
  | c == '\x11D66' = Unknown
  | c <= '\x11D68' = Gunjala_Gondi
  | c == '\x11D69' = Unknown
  | c <= '\x11D8E' = Gunjala_Gondi
  | c == '\x11D8F' = Unknown
  | c <= '\x11D91' = Gunjala_Gondi
  | c == '\x11D92' = Unknown
  | c <= '\x11D98' = Gunjala_Gondi
  | c <= '\x11D9F' = Unknown
  | c <= '\x11DA9' = Gunjala_Gondi
  | c <= '\x11EDF' = Unknown
  | c <= '\x11EF8' = Makasar
  | c <= '\x11FAF' = Unknown
  | c == '\x11FB0' = Lisu
  | c <= '\x11FBF' = Unknown
  | c <= '\x11FF1' = Tamil
  | c <= '\x11FFE' = Unknown
  | c == '\x11FFF' = Tamil
  | c <= '\x12399' = Cuneiform
  | c <= '\x123FF' = Unknown
  | c <= '\x1246E' = Cuneiform
  | c == '\x1246F' = Unknown
  | c <= '\x12474' = Cuneiform
  | c <= '\x1247F' = Unknown
  | c <= '\x12543' = Cuneiform
  | c <= '\x12FFF' = Unknown
  | c <= '\x1342E' = Egyptian_Hieroglyphs
  | c == '\x1342F' = Unknown
  | c <= '\x13438' = Egyptian_Hieroglyphs
  | c <= '\x143FF' = Unknown
  | c <= '\x14646' = Anatolian_Hieroglyphs
  | c <= '\x167FF' = Unknown
  | c <= '\x16A38' = Bamum
  | c <= '\x16A3F' = Unknown
  | c <= '\x16A5E' = Mro
  | c == '\x16A5F' = Unknown
  | c <= '\x16A69' = Mro
  | c <= '\x16A6D' = Unknown
  | c <= '\x16A6F' = Mro
  | c <= '\x16ACF' = Unknown
  | c <= '\x16AED' = Bassa_Vah
  | c <= '\x16AEF' = Unknown
  | c <= '\x16AF5' = Bassa_Vah
  | c <= '\x16AFF' = Unknown
  | c <= '\x16B45' = Pahawh_Hmong
  | c <= '\x16B4F' = Unknown
  | c <= '\x16B59' = Pahawh_Hmong
  | c == '\x16B5A' = Unknown
  | c <= '\x16B61' = Pahawh_Hmong
  | c == '\x16B62' = Unknown
  | c <= '\x16B77' = Pahawh_Hmong
  | c <= '\x16B7C' = Unknown
  | c <= '\x16B8F' = Pahawh_Hmong
  | c <= '\x16E3F' = Unknown
  | c <= '\x16E9A' = Medefaidrin
  | c <= '\x16EFF' = Unknown
  | c <= '\x16F4A' = Miao
  | c <= '\x16F4E' = Unknown
  | c <= '\x16F87' = Miao
  | c <= '\x16F8E' = Unknown
  | c <= '\x16F9F' = Miao
  | c <= '\x16FDF' = Unknown
  | c == '\x16FE0' = Tangut
  | c == '\x16FE1' = Nushu
  | c <= '\x16FE3' = Common
  | c == '\x16FE4' = Khitan_Small_Script
  | c <= '\x16FEF' = Unknown
  | c <= '\x16FF1' = Han
  | c <= '\x16FFF' = Unknown
  | c <= '\x187F7' = Tangut
  | c <= '\x187FF' = Unknown
  | c <= '\x18AFF' = Tangut
  | c <= '\x18CD5' = Khitan_Small_Script
  | c <= '\x18CFF' = Unknown
  | c <= '\x18D08' = Tangut
  | c <= '\x1AFFF' = Unknown
  | c == '\x1B000' = Katakana
  | c <= '\x1B11E' = Hiragana
  | c <= '\x1B14F' = Unknown
  | c <= '\x1B152' = Hiragana
  | c <= '\x1B163' = Unknown
  | c <= '\x1B167' = Katakana
  | c <= '\x1B16F' = Unknown
  | c <= '\x1B2FB' = Nushu
  | c <= '\x1BBFF' = Unknown
  | c <= '\x1BC6A' = Duployan
  | c <= '\x1BC6F' = Unknown
  | c <= '\x1BC7C' = Duployan
  | c <= '\x1BC7F' = Unknown
  | c <= '\x1BC88' = Duployan
  | c <= '\x1BC8F' = Unknown
  | c <= '\x1BC99' = Duployan
  | c <= '\x1BC9B' = Unknown
  | c <= '\x1BC9F' = Duployan
  | c <= '\x1BCA3' = Common
  | c <= '\x1CFFF' = Unknown
  | c <= '\x1D0F5' = Common
  | c <= '\x1D0FF' = Unknown
  | c <= '\x1D126' = Common
  | c <= '\x1D128' = Unknown
  | c <= '\x1D166' = Common
  | c <= '\x1D169' = Inherited
  | c <= '\x1D17A' = Common
  | c <= '\x1D182' = Inherited
  | c <= '\x1D184' = Common
  | c <= '\x1D18B' = Inherited
  | c <= '\x1D1A9' = Common
  | c <= '\x1D1AD' = Inherited
  | c <= '\x1D1E8' = Common
  | c <= '\x1D1FF' = Unknown
  | c <= '\x1D245' = Greek
  | c <= '\x1D2DF' = Unknown
  | c <= '\x1D2F3' = Common
  | c <= '\x1D2FF' = Unknown
  | c <= '\x1D356' = Common
  | c <= '\x1D35F' = Unknown
  | c <= '\x1D378' = Common
  | c <= '\x1D3FF' = Unknown
  | c <= '\x1D454' = Common
  | c == '\x1D455' = Unknown
  | c <= '\x1D49C' = Common
  | c == '\x1D49D' = Unknown
  | c <= '\x1D49F' = Common
  | c <= '\x1D4A1' = Unknown
  | c == '\x1D4A2' = Common
  | c <= '\x1D4A4' = Unknown
  | c <= '\x1D4A6' = Common
  | c <= '\x1D4A8' = Unknown
  | c <= '\x1D4AC' = Common
  | c == '\x1D4AD' = Unknown
  | c <= '\x1D4B9' = Common
  | c == '\x1D4BA' = Unknown
  | c == '\x1D4BB' = Common
  | c == '\x1D4BC' = Unknown
  | c <= '\x1D4C3' = Common
  | c == '\x1D4C4' = Unknown
  | c <= '\x1D505' = Common
  | c == '\x1D506' = Unknown
  | c <= '\x1D50A' = Common
  | c <= '\x1D50C' = Unknown
  | c <= '\x1D514' = Common
  | c == '\x1D515' = Unknown
  | c <= '\x1D51C' = Common
  | c == '\x1D51D' = Unknown
  | c <= '\x1D539' = Common
  | c == '\x1D53A' = Unknown
  | c <= '\x1D53E' = Common
  | c == '\x1D53F' = Unknown
  | c <= '\x1D544' = Common
  | c == '\x1D545' = Unknown
  | c == '\x1D546' = Common
  | c <= '\x1D549' = Unknown
  | c <= '\x1D550' = Common
  | c == '\x1D551' = Unknown
  | c <= '\x1D6A5' = Common
  | c <= '\x1D6A7' = Unknown
  | c <= '\x1D7CB' = Common
  | c <= '\x1D7CD' = Unknown
  | c <= '\x1D7FF' = Common
  | c <= '\x1DA8B' = SignWriting
  | c <= '\x1DA9A' = Unknown
  | c <= '\x1DA9F' = SignWriting
  | c == '\x1DAA0' = Unknown
  | c <= '\x1DAAF' = SignWriting
  | c <= '\x1DFFF' = Unknown
  | c <= '\x1E006' = Glagolitic
  | c == '\x1E007' = Unknown
  | c <= '\x1E018' = Glagolitic
  | c <= '\x1E01A' = Unknown
  | c <= '\x1E021' = Glagolitic
  | c == '\x1E022' = Unknown
  | c <= '\x1E024' = Glagolitic
  | c == '\x1E025' = Unknown
  | c <= '\x1E02A' = Glagolitic
  | c <= '\x1E0FF' = Unknown
  | c <= '\x1E12C' = Nyiakeng_Puachue_Hmong
  | c <= '\x1E12F' = Unknown
  | c <= '\x1E13D' = Nyiakeng_Puachue_Hmong
  | c <= '\x1E13F' = Unknown
  | c <= '\x1E149' = Nyiakeng_Puachue_Hmong
  | c <= '\x1E14D' = Unknown
  | c <= '\x1E14F' = Nyiakeng_Puachue_Hmong
  | c <= '\x1E2BF' = Unknown
  | c <= '\x1E2F9' = Wancho
  | c <= '\x1E2FE' = Unknown
  | c == '\x1E2FF' = Wancho
  | c <= '\x1E7FF' = Unknown
  | c <= '\x1E8C4' = Mende_Kikakui
  | c <= '\x1E8C6' = Unknown
  | c <= '\x1E8D6' = Mende_Kikakui
  | c <= '\x1E8FF' = Unknown
  | c <= '\x1E94B' = Adlam
  | c <= '\x1E94F' = Unknown
  | c <= '\x1E959' = Adlam
  | c <= '\x1E95D' = Unknown
  | c <= '\x1E95F' = Adlam
  | c <= '\x1EC70' = Unknown
  | c <= '\x1ECB4' = Common
  | c <= '\x1ED00' = Unknown
  | c <= '\x1ED3D' = Common
  | c <= '\x1EDFF' = Unknown
  | c <= '\x1EE03' = Arabic
  | c == '\x1EE04' = Unknown
  | c <= '\x1EE1F' = Arabic
  | c == '\x1EE20' = Unknown
  | c <= '\x1EE22' = Arabic
  | c == '\x1EE23' = Unknown
  | c == '\x1EE24' = Arabic
  | c <= '\x1EE26' = Unknown
  | c == '\x1EE27' = Arabic
  | c == '\x1EE28' = Unknown
  | c <= '\x1EE32' = Arabic
  | c == '\x1EE33' = Unknown
  | c <= '\x1EE37' = Arabic
  | c == '\x1EE38' = Unknown
  | c == '\x1EE39' = Arabic
  | c == '\x1EE3A' = Unknown
  | c == '\x1EE3B' = Arabic
  | c <= '\x1EE41' = Unknown
  | c == '\x1EE42' = Arabic
  | c <= '\x1EE46' = Unknown
  | c == '\x1EE47' = Arabic
  | c == '\x1EE48' = Unknown
  | c == '\x1EE49' = Arabic
  | c == '\x1EE4A' = Unknown
  | c == '\x1EE4B' = Arabic
  | c == '\x1EE4C' = Unknown
  | c <= '\x1EE4F' = Arabic
  | c == '\x1EE50' = Unknown
  | c <= '\x1EE52' = Arabic
  | c == '\x1EE53' = Unknown
  | c == '\x1EE54' = Arabic
  | c <= '\x1EE56' = Unknown
  | c == '\x1EE57' = Arabic
  | c == '\x1EE58' = Unknown
  | c == '\x1EE59' = Arabic
  | c == '\x1EE5A' = Unknown
  | c == '\x1EE5B' = Arabic
  | c == '\x1EE5C' = Unknown
  | c == '\x1EE5D' = Arabic
  | c == '\x1EE5E' = Unknown
  | c == '\x1EE5F' = Arabic
  | c == '\x1EE60' = Unknown
  | c <= '\x1EE62' = Arabic
  | c == '\x1EE63' = Unknown
  | c == '\x1EE64' = Arabic
  | c <= '\x1EE66' = Unknown
  | c <= '\x1EE6A' = Arabic
  | c == '\x1EE6B' = Unknown
  | c <= '\x1EE72' = Arabic
  | c == '\x1EE73' = Unknown
  | c <= '\x1EE77' = Arabic
  | c == '\x1EE78' = Unknown
  | c <= '\x1EE7C' = Arabic
  | c == '\x1EE7D' = Unknown
  | c == '\x1EE7E' = Arabic
  | c == '\x1EE7F' = Unknown
  | c <= '\x1EE89' = Arabic
  | c == '\x1EE8A' = Unknown
  | c <= '\x1EE9B' = Arabic
  | c <= '\x1EEA0' = Unknown
  | c <= '\x1EEA3' = Arabic
  | c == '\x1EEA4' = Unknown
  | c <= '\x1EEA9' = Arabic
  | c == '\x1EEAA' = Unknown
  | c <= '\x1EEBB' = Arabic
  | c <= '\x1EEEF' = Unknown
  | c <= '\x1EEF1' = Arabic
  | c <= '\x1EFFF' = Unknown
  | c <= '\x1F02B' = Common
  | c <= '\x1F02F' = Unknown
  | c <= '\x1F093' = Common
  | c <= '\x1F09F' = Unknown
  | c <= '\x1F0AE' = Common
  | c <= '\x1F0B0' = Unknown
  | c <= '\x1F0BF' = Common
  | c == '\x1F0C0' = Unknown
  | c <= '\x1F0CF' = Common
  | c == '\x1F0D0' = Unknown
  | c <= '\x1F0F5' = Common
  | c <= '\x1F0FF' = Unknown
  | c <= '\x1F1AD' = Common
  | c <= '\x1F1E5' = Unknown
  | c <= '\x1F1FF' = Common
  | c == '\x1F200' = Hiragana
  | c <= '\x1F202' = Common
  | c <= '\x1F20F' = Unknown
  | c <= '\x1F23B' = Common
  | c <= '\x1F23F' = Unknown
  | c <= '\x1F248' = Common
  | c <= '\x1F24F' = Unknown
  | c <= '\x1F251' = Common
  | c <= '\x1F25F' = Unknown
  | c <= '\x1F265' = Common
  | c <= '\x1F2FF' = Unknown
  | c <= '\x1F6D7' = Common
  | c <= '\x1F6DF' = Unknown
  | c <= '\x1F6EC' = Common
  | c <= '\x1F6EF' = Unknown
  | c <= '\x1F6FC' = Common
  | c <= '\x1F6FF' = Unknown
  | c <= '\x1F773' = Common
  | c <= '\x1F77F' = Unknown
  | c <= '\x1F7D8' = Common
  | c <= '\x1F7DF' = Unknown
  | c <= '\x1F7EB' = Common
  | c <= '\x1F7FF' = Unknown
  | c <= '\x1F80B' = Common
  | c <= '\x1F80F' = Unknown
  | c <= '\x1F847' = Common
  | c <= '\x1F84F' = Unknown
  | c <= '\x1F859' = Common
  | c <= '\x1F85F' = Unknown
  | c <= '\x1F887' = Common
  | c <= '\x1F88F' = Unknown
  | c <= '\x1F8AD' = Common
  | c <= '\x1F8AF' = Unknown
  | c <= '\x1F8B1' = Common
  | c <= '\x1F8FF' = Unknown
  | c <= '\x1F978' = Common
  | c == '\x1F979' = Unknown
  | c <= '\x1F9CB' = Common
  | c == '\x1F9CC' = Unknown
  | c <= '\x1FA53' = Common
  | c <= '\x1FA5F' = Unknown
  | c <= '\x1FA6D' = Common
  | c <= '\x1FA6F' = Unknown
  | c <= '\x1FA74' = Common
  | c <= '\x1FA77' = Unknown
  | c <= '\x1FA7A' = Common
  | c <= '\x1FA7F' = Unknown
  | c <= '\x1FA86' = Common
  | c <= '\x1FA8F' = Unknown
  | c <= '\x1FAA8' = Common
  | c <= '\x1FAAF' = Unknown
  | c <= '\x1FAB6' = Common
  | c <= '\x1FABF' = Unknown
  | c <= '\x1FAC2' = Common
  | c <= '\x1FACF' = Unknown
  | c <= '\x1FAD6' = Common
  | c <= '\x1FAFF' = Unknown
  | c <= '\x1FB92' = Common
  | c == '\x1FB93' = Unknown
  | c <= '\x1FBCA' = Common
  | c <= '\x1FBEF' = Unknown
  | c <= '\x1FBF9' = Common
  | c <= '\x1FFFF' = Unknown
  | c <= '\x2A6DD' = Han
  | c <= '\x2A6FF' = Unknown
  | c <= '\x2B734' = Han
  | c <= '\x2B73F' = Unknown
  | c <= '\x2B81D' = Han
  | c <= '\x2B81F' = Unknown
  | c <= '\x2CEA1' = Han
  | c <= '\x2CEAF' = Unknown
  | c <= '\x2EBE0' = Han
  | c <= '\x2F7FF' = Unknown
  | c <= '\x2FA1D' = Han
  | c <= '\x2FFFF' = Unknown
  | c <= '\x3134A' = Han
  | c <= '\xE0000' = Unknown
  | c == '\xE0001' = Common
  | c <= '\xE001F' = Unknown
  | c <= '\xE007F' = Common
  | c <= '\xE00FF' = Unknown
  | c <= '\xE01EF' = Inherited
unicodeScript _ = Unknown

unicodeScripts :: Map UnicodeScript [UnicodeRange]
unicodeScripts =
  [ ( Adlam
    , [ UnicodeRange 0x1E900 0x1E900
      , UnicodeRange 0x1E950 0x1E950
      , UnicodeRange 0x1E95E 0x1E95E ] )
  , ( Ahom
    , [ UnicodeRange 0x11700 0x11700
      , UnicodeRange 0x1171D 0x1171D
      , UnicodeRange 0x11730 0x11730 ] )
  , ( Anatolian_Hieroglyphs
    , [ UnicodeRange 0x14400 0x14400 ] )
  , ( Arabic
    , [ UnicodeRange 0x0600 0x0600
      , UnicodeRange 0x0606 0x0606
      , UnicodeRange 0x060D 0x060D
      , UnicodeCodePoint 0x061C
      , UnicodeCodePoint 0x061E
      , UnicodeRange 0x0620 0x0620
      , UnicodeRange 0x0641 0x0641
      , UnicodeRange 0x0656 0x0656
      , UnicodeRange 0x0671 0x0671
      , UnicodeRange 0x06DE 0x06DE
      , UnicodeRange 0x0750 0x0750
      , UnicodeRange 0x08A0 0x08A0
      , UnicodeRange 0x08B6 0x08B6
      , UnicodeRange 0x08D3 0x08D3
      , UnicodeRange 0x08E3 0x08E3
      , UnicodeRange 0xFB50 0xFB50
      , UnicodeRange 0xFBD3 0xFBD3
      , UnicodeRange 0xFD50 0xFD50
      , UnicodeRange 0xFD92 0xFD92
      , UnicodeRange 0xFDF0 0xFDF0
      , UnicodeRange 0xFE70 0xFE70
      , UnicodeRange 0xFE76 0xFE76
      , UnicodeRange 0x10E60 0x10E60
      , UnicodeRange 0x1EE00 0x1EE00
      , UnicodeRange 0x1EE05 0x1EE05
      , UnicodeRange 0x1EE21 0x1EE21
      , UnicodeCodePoint 0x1EE24
      , UnicodeCodePoint 0x1EE27
      , UnicodeRange 0x1EE29 0x1EE29
      , UnicodeRange 0x1EE34 0x1EE34
      , UnicodeCodePoint 0x1EE39
      , UnicodeCodePoint 0x1EE3B
      , UnicodeCodePoint 0x1EE42
      , UnicodeCodePoint 0x1EE47
      , UnicodeCodePoint 0x1EE49
      , UnicodeCodePoint 0x1EE4B
      , UnicodeRange 0x1EE4D 0x1EE4D
      , UnicodeRange 0x1EE51 0x1EE51
      , UnicodeCodePoint 0x1EE54
      , UnicodeCodePoint 0x1EE57
      , UnicodeCodePoint 0x1EE59
      , UnicodeCodePoint 0x1EE5B
      , UnicodeCodePoint 0x1EE5D
      , UnicodeCodePoint 0x1EE5F
      , UnicodeRange 0x1EE61 0x1EE61
      , UnicodeCodePoint 0x1EE64
      , UnicodeRange 0x1EE67 0x1EE67
      , UnicodeRange 0x1EE6C 0x1EE6C
      , UnicodeRange 0x1EE74 0x1EE74
      , UnicodeRange 0x1EE79 0x1EE79
      , UnicodeCodePoint 0x1EE7E
      , UnicodeRange 0x1EE80 0x1EE80
      , UnicodeRange 0x1EE8B 0x1EE8B
      , UnicodeRange 0x1EEA1 0x1EEA1
      , UnicodeRange 0x1EEA5 0x1EEA5
      , UnicodeRange 0x1EEAB 0x1EEAB
      , UnicodeRange 0x1EEF0 0x1EEF0 ] )
  , ( Armenian
    , [ UnicodeRange 0x0531 0x0531
      , UnicodeRange 0x0559 0x0559
      , UnicodeRange 0x058D 0x058D
      , UnicodeRange 0xFB13 0xFB13 ] )
  , ( Avestan
    , [ UnicodeRange 0x10B00 0x10B00
      , UnicodeRange 0x10B39 0x10B39 ] )
  , ( Balinese
    , [ UnicodeRange 0x1B00 0x1B00
      , UnicodeRange 0x1B50 0x1B50 ] )
  , ( Bamum
    , [ UnicodeRange 0xA6A0 0xA6A0
      , UnicodeRange 0x16800 0x16800 ] )
  , ( Bassa_Vah
    , [ UnicodeRange 0x16AD0 0x16AD0
      , UnicodeRange 0x16AF0 0x16AF0 ] )
  , ( Batak
    , [ UnicodeRange 0x1BC0 0x1BC0
      , UnicodeRange 0x1BFC 0x1BFC ] )
  , ( Bengali
    , [ UnicodeRange 0x0980 0x0980
      , UnicodeRange 0x0985 0x0985
      , UnicodeRange 0x098F 0x098F
      , UnicodeRange 0x0993 0x0993
      , UnicodeRange 0x09AA 0x09AA
      , UnicodeCodePoint 0x09B2
      , UnicodeRange 0x09B6 0x09B6
      , UnicodeRange 0x09BC 0x09BC
      , UnicodeRange 0x09C7 0x09C7
      , UnicodeRange 0x09CB 0x09CB
      , UnicodeCodePoint 0x09D7
      , UnicodeRange 0x09DC 0x09DC
      , UnicodeRange 0x09DF 0x09DF
      , UnicodeRange 0x09E6 0x09E6 ] )
  , ( Bhaiksuki
    , [ UnicodeRange 0x11C00 0x11C00
      , UnicodeRange 0x11C0A 0x11C0A
      , UnicodeRange 0x11C38 0x11C38
      , UnicodeRange 0x11C50 0x11C50 ] )
  , ( Bopomofo
    , [ UnicodeRange 0x02EA 0x02EA
      , UnicodeRange 0x3105 0x3105
      , UnicodeRange 0x31A0 0x31A0 ] )
  , ( Brahmi
    , [ UnicodeRange 0x11000 0x11000
      , UnicodeRange 0x11052 0x11052
      , UnicodeCodePoint 0x1107F ] )
  , ( Braille
    , [ UnicodeRange 0x2800 0x2800 ] )
  , ( Buginese
    , [ UnicodeRange 0x1A00 0x1A00
      , UnicodeRange 0x1A1E 0x1A1E ] )
  , ( Buhid
    , [ UnicodeRange 0x1740 0x1740 ] )
  , ( Canadian_Aboriginal
    , [ UnicodeRange 0x1400 0x1400
      , UnicodeRange 0x18B0 0x18B0 ] )
  , ( Carian
    , [ UnicodeRange 0x102A0 0x102A0 ] )
  , ( Caucasian_Albanian
    , [ UnicodeRange 0x10530 0x10530
      , UnicodeCodePoint 0x1056F ] )
  , ( Chakma
    , [ UnicodeRange 0x11100 0x11100
      , UnicodeRange 0x11136 0x11136 ] )
  , ( Cham
    , [ UnicodeRange 0xAA00 0xAA00
      , UnicodeRange 0xAA40 0xAA40
      , UnicodeRange 0xAA50 0xAA50
      , UnicodeRange 0xAA5C 0xAA5C ] )
  , ( Cherokee
    , [ UnicodeRange 0x13A0 0x13A0
      , UnicodeRange 0x13F8 0x13F8
      , UnicodeRange 0xAB70 0xAB70 ] )
  , ( Chorasmian
    , [ UnicodeRange 0x10FB0 0x10FB0 ] )
  , ( Common
    , [ UnicodeRange 0x0000 0x0000
      , UnicodeRange 0x005B 0x005B
      , UnicodeRange 0x007B 0x007B
      , UnicodeRange 0x00AB 0x00AB
      , UnicodeRange 0x00BB 0x00BB
      , UnicodeCodePoint 0x00D7
      , UnicodeCodePoint 0x00F7
      , UnicodeRange 0x02B9 0x02B9
      , UnicodeRange 0x02E5 0x02E5
      , UnicodeRange 0x02EC 0x02EC
      , UnicodeCodePoint 0x0374
      , UnicodeCodePoint 0x037E
      , UnicodeCodePoint 0x0385
      , UnicodeCodePoint 0x0387
      , UnicodeCodePoint 0x0605
      , UnicodeCodePoint 0x060C
      , UnicodeCodePoint 0x061B
      , UnicodeCodePoint 0x061F
      , UnicodeCodePoint 0x0640
      , UnicodeCodePoint 0x06DD
      , UnicodeCodePoint 0x08E2
      , UnicodeRange 0x0964 0x0964
      , UnicodeCodePoint 0x0E3F
      , UnicodeRange 0x0FD5 0x0FD5
      , UnicodeCodePoint 0x10FB
      , UnicodeRange 0x16EB 0x16EB
      , UnicodeRange 0x1735 0x1735
      , UnicodeRange 0x1802 0x1802
      , UnicodeCodePoint 0x1805
      , UnicodeCodePoint 0x1CD3
      , UnicodeCodePoint 0x1CE1
      , UnicodeRange 0x1CE9 0x1CE9
      , UnicodeRange 0x1CEE 0x1CEE
      , UnicodeRange 0x1CF5 0x1CF5
      , UnicodeCodePoint 0x1CFA
      , UnicodeRange 0x2000 0x2000
      , UnicodeRange 0x200E 0x200E
      , UnicodeRange 0x2066 0x2066
      , UnicodeRange 0x2074 0x2074
      , UnicodeRange 0x2080 0x2080
      , UnicodeRange 0x20A0 0x20A0
      , UnicodeRange 0x2100 0x2100
      , UnicodeRange 0x2127 0x2127
      , UnicodeRange 0x212C 0x212C
      , UnicodeRange 0x2133 0x2133
      , UnicodeRange 0x214F 0x214F
      , UnicodeRange 0x2189 0x2189
      , UnicodeRange 0x2190 0x2190
      , UnicodeRange 0x2440 0x2440
      , UnicodeRange 0x2460 0x2460
      , UnicodeRange 0x2900 0x2900
      , UnicodeRange 0x2B76 0x2B76
      , UnicodeRange 0x2B97 0x2B97
      , UnicodeRange 0x2E00 0x2E00
      , UnicodeRange 0x2FF0 0x2FF0
      , UnicodeRange 0x3000 0x3000
      , UnicodeCodePoint 0x3006
      , UnicodeRange 0x3008 0x3008
      , UnicodeRange 0x3030 0x3030
      , UnicodeRange 0x303C 0x303C
      , UnicodeRange 0x309B 0x309B
      , UnicodeCodePoint 0x30A0
      , UnicodeRange 0x30FB 0x30FB
      , UnicodeRange 0x3190 0x3190
      , UnicodeRange 0x31C0 0x31C0
      , UnicodeRange 0x3220 0x3220
      , UnicodeRange 0x327F 0x327F
      , UnicodeCodePoint 0x32FF
      , UnicodeRange 0x3358 0x3358
      , UnicodeRange 0x4DC0 0x4DC0
      , UnicodeRange 0xA700 0xA700
      , UnicodeRange 0xA788 0xA788
      , UnicodeRange 0xA830 0xA830
      , UnicodeCodePoint 0xA92E
      , UnicodeCodePoint 0xA9CF
      , UnicodeCodePoint 0xAB5B
      , UnicodeRange 0xAB6A 0xAB6A
      , UnicodeRange 0xFD3E 0xFD3E
      , UnicodeRange 0xFE10 0xFE10
      , UnicodeRange 0xFE30 0xFE30
      , UnicodeRange 0xFE54 0xFE54
      , UnicodeRange 0xFE68 0xFE68
      , UnicodeCodePoint 0xFEFF
      , UnicodeRange 0xFF01 0xFF01
      , UnicodeRange 0xFF3B 0xFF3B
      , UnicodeRange 0xFF5B 0xFF5B
      , UnicodeCodePoint 0xFF70
      , UnicodeRange 0xFF9E 0xFF9E
      , UnicodeRange 0xFFE0 0xFFE0
      , UnicodeRange 0xFFE8 0xFFE8
      , UnicodeRange 0xFFF9 0xFFF9
      , UnicodeRange 0x10100 0x10100
      , UnicodeRange 0x10107 0x10107
      , UnicodeRange 0x10137 0x10137
      , UnicodeRange 0x10190 0x10190
      , UnicodeRange 0x101D0 0x101D0
      , UnicodeRange 0x102E1 0x102E1
      , UnicodeRange 0x16FE2 0x16FE2
      , UnicodeRange 0x1BCA0 0x1BCA0
      , UnicodeRange 0x1D000 0x1D000
      , UnicodeRange 0x1D100 0x1D100
      , UnicodeRange 0x1D129 0x1D129
      , UnicodeRange 0x1D16A 0x1D16A
      , UnicodeRange 0x1D183 0x1D183
      , UnicodeRange 0x1D18C 0x1D18C
      , UnicodeRange 0x1D1AE 0x1D1AE
      , UnicodeRange 0x1D2E0 0x1D2E0
      , UnicodeRange 0x1D300 0x1D300
      , UnicodeRange 0x1D360 0x1D360
      , UnicodeRange 0x1D400 0x1D400
      , UnicodeRange 0x1D456 0x1D456
      , UnicodeRange 0x1D49E 0x1D49E
      , UnicodeCodePoint 0x1D4A2
      , UnicodeRange 0x1D4A5 0x1D4A5
      , UnicodeRange 0x1D4A9 0x1D4A9
      , UnicodeRange 0x1D4AE 0x1D4AE
      , UnicodeCodePoint 0x1D4BB
      , UnicodeRange 0x1D4BD 0x1D4BD
      , UnicodeRange 0x1D4C5 0x1D4C5
      , UnicodeRange 0x1D507 0x1D507
      , UnicodeRange 0x1D50D 0x1D50D
      , UnicodeRange 0x1D516 0x1D516
      , UnicodeRange 0x1D51E 0x1D51E
      , UnicodeRange 0x1D53B 0x1D53B
      , UnicodeRange 0x1D540 0x1D540
      , UnicodeCodePoint 0x1D546
      , UnicodeRange 0x1D54A 0x1D54A
      , UnicodeRange 0x1D552 0x1D552
      , UnicodeRange 0x1D6A8 0x1D6A8
      , UnicodeRange 0x1D7CE 0x1D7CE
      , UnicodeRange 0x1EC71 0x1EC71
      , UnicodeRange 0x1ED01 0x1ED01
      , UnicodeRange 0x1F000 0x1F000
      , UnicodeRange 0x1F030 0x1F030
      , UnicodeRange 0x1F0A0 0x1F0A0
      , UnicodeRange 0x1F0B1 0x1F0B1
      , UnicodeRange 0x1F0C1 0x1F0C1
      , UnicodeRange 0x1F0D1 0x1F0D1
      , UnicodeRange 0x1F100 0x1F100
      , UnicodeRange 0x1F1E6 0x1F1E6
      , UnicodeRange 0x1F201 0x1F201
      , UnicodeRange 0x1F210 0x1F210
      , UnicodeRange 0x1F240 0x1F240
      , UnicodeRange 0x1F250 0x1F250
      , UnicodeRange 0x1F260 0x1F260
      , UnicodeRange 0x1F300 0x1F300
      , UnicodeRange 0x1F6E0 0x1F6E0
      , UnicodeRange 0x1F6F0 0x1F6F0
      , UnicodeRange 0x1F700 0x1F700
      , UnicodeRange 0x1F780 0x1F780
      , UnicodeRange 0x1F7E0 0x1F7E0
      , UnicodeRange 0x1F800 0x1F800
      , UnicodeRange 0x1F810 0x1F810
      , UnicodeRange 0x1F850 0x1F850
      , UnicodeRange 0x1F860 0x1F860
      , UnicodeRange 0x1F890 0x1F890
      , UnicodeRange 0x1F8B0 0x1F8B0
      , UnicodeRange 0x1F900 0x1F900
      , UnicodeRange 0x1F97A 0x1F97A
      , UnicodeRange 0x1F9CD 0x1F9CD
      , UnicodeRange 0x1FA60 0x1FA60
      , UnicodeRange 0x1FA70 0x1FA70
      , UnicodeRange 0x1FA78 0x1FA78
      , UnicodeRange 0x1FA80 0x1FA80
      , UnicodeRange 0x1FA90 0x1FA90
      , UnicodeRange 0x1FAB0 0x1FAB0
      , UnicodeRange 0x1FAC0 0x1FAC0
      , UnicodeRange 0x1FAD0 0x1FAD0
      , UnicodeRange 0x1FB00 0x1FB00
      , UnicodeRange 0x1FB94 0x1FB94
      , UnicodeRange 0x1FBF0 0x1FBF0
      , UnicodeCodePoint 0xE0001
      , UnicodeRange 0xE0020 0xE0020 ] )
  , ( Coptic
    , [ UnicodeRange 0x03E2 0x03E2
      , UnicodeRange 0x2C80 0x2C80
      , UnicodeRange 0x2CF9 0x2CF9 ] )
  , ( Cuneiform
    , [ UnicodeRange 0x12000 0x12000
      , UnicodeRange 0x12400 0x12400
      , UnicodeRange 0x12470 0x12470
      , UnicodeRange 0x12480 0x12480 ] )
  , ( Cypriot
    , [ UnicodeRange 0x10800 0x10800
      , UnicodeCodePoint 0x10808
      , UnicodeRange 0x1080A 0x1080A
      , UnicodeRange 0x10837 0x10837
      , UnicodeCodePoint 0x1083C
      , UnicodeCodePoint 0x1083F ] )
  , ( Cyrillic
    , [ UnicodeRange 0x0400 0x0400
      , UnicodeRange 0x0487 0x0487
      , UnicodeRange 0x1C80 0x1C80
      , UnicodeCodePoint 0x1D2B
      , UnicodeCodePoint 0x1D78
      , UnicodeRange 0x2DE0 0x2DE0
      , UnicodeRange 0xA640 0xA640
      , UnicodeRange 0xFE2E 0xFE2E ] )
  , ( Deseret
    , [ UnicodeRange 0x10400 0x10400 ] )
  , ( Devanagari
    , [ UnicodeRange 0x0900 0x0900
      , UnicodeRange 0x0955 0x0955
      , UnicodeRange 0x0966 0x0966
      , UnicodeRange 0xA8E0 0xA8E0 ] )
  , ( Dives_Akuru
    , [ UnicodeRange 0x11900 0x11900
      , UnicodeCodePoint 0x11909
      , UnicodeRange 0x1190C 0x1190C
      , UnicodeRange 0x11915 0x11915
      , UnicodeRange 0x11918 0x11918
      , UnicodeRange 0x11937 0x11937
      , UnicodeRange 0x1193B 0x1193B
      , UnicodeRange 0x11950 0x11950 ] )
  , ( Dogra
    , [ UnicodeRange 0x11800 0x11800 ] )
  , ( Duployan
    , [ UnicodeRange 0x1BC00 0x1BC00
      , UnicodeRange 0x1BC70 0x1BC70
      , UnicodeRange 0x1BC80 0x1BC80
      , UnicodeRange 0x1BC90 0x1BC90
      , UnicodeRange 0x1BC9C 0x1BC9C ] )
  , ( Egyptian_Hieroglyphs
    , [ UnicodeRange 0x13000 0x13000
      , UnicodeRange 0x13430 0x13430 ] )
  , ( Elbasan
    , [ UnicodeRange 0x10500 0x10500 ] )
  , ( Elymaic
    , [ UnicodeRange 0x10FE0 0x10FE0 ] )
  , ( Ethiopic
    , [ UnicodeRange 0x1200 0x1200
      , UnicodeRange 0x124A 0x124A
      , UnicodeRange 0x1250 0x1250
      , UnicodeCodePoint 0x1258
      , UnicodeRange 0x125A 0x125A
      , UnicodeRange 0x1260 0x1260
      , UnicodeRange 0x128A 0x128A
      , UnicodeRange 0x1290 0x1290
      , UnicodeRange 0x12B2 0x12B2
      , UnicodeRange 0x12B8 0x12B8
      , UnicodeCodePoint 0x12C0
      , UnicodeRange 0x12C2 0x12C2
      , UnicodeRange 0x12C8 0x12C8
      , UnicodeRange 0x12D8 0x12D8
      , UnicodeRange 0x1312 0x1312
      , UnicodeRange 0x1318 0x1318
      , UnicodeRange 0x135D 0x135D
      , UnicodeRange 0x1380 0x1380
      , UnicodeRange 0x2D80 0x2D80
      , UnicodeRange 0x2DA0 0x2DA0
      , UnicodeRange 0x2DA8 0x2DA8
      , UnicodeRange 0x2DB0 0x2DB0
      , UnicodeRange 0x2DB8 0x2DB8
      , UnicodeRange 0x2DC0 0x2DC0
      , UnicodeRange 0x2DC8 0x2DC8
      , UnicodeRange 0x2DD0 0x2DD0
      , UnicodeRange 0x2DD8 0x2DD8
      , UnicodeRange 0xAB01 0xAB01
      , UnicodeRange 0xAB09 0xAB09
      , UnicodeRange 0xAB11 0xAB11
      , UnicodeRange 0xAB20 0xAB20
      , UnicodeRange 0xAB28 0xAB28 ] )
  , ( Georgian
    , [ UnicodeRange 0x10A0 0x10A0
      , UnicodeCodePoint 0x10C7
      , UnicodeCodePoint 0x10CD
      , UnicodeRange 0x10D0 0x10D0
      , UnicodeRange 0x10FC 0x10FC
      , UnicodeRange 0x1C90 0x1C90
      , UnicodeRange 0x1CBD 0x1CBD
      , UnicodeRange 0x2D00 0x2D00
      , UnicodeCodePoint 0x2D27
      , UnicodeCodePoint 0x2D2D ] )
  , ( Glagolitic
    , [ UnicodeRange 0x2C00 0x2C00
      , UnicodeRange 0x2C30 0x2C30
      , UnicodeRange 0x1E000 0x1E000
      , UnicodeRange 0x1E008 0x1E008
      , UnicodeRange 0x1E01B 0x1E01B
      , UnicodeRange 0x1E023 0x1E023
      , UnicodeRange 0x1E026 0x1E026 ] )
  , ( Gothic
    , [ UnicodeRange 0x10330 0x10330 ] )
  , ( Grantha
    , [ UnicodeRange 0x11300 0x11300
      , UnicodeRange 0x11305 0x11305
      , UnicodeRange 0x1130F 0x1130F
      , UnicodeRange 0x11313 0x11313
      , UnicodeRange 0x1132A 0x1132A
      , UnicodeRange 0x11332 0x11332
      , UnicodeRange 0x11335 0x11335
      , UnicodeRange 0x1133C 0x1133C
      , UnicodeRange 0x11347 0x11347
      , UnicodeRange 0x1134B 0x1134B
      , UnicodeCodePoint 0x11350
      , UnicodeCodePoint 0x11357
      , UnicodeRange 0x1135D 0x1135D
      , UnicodeRange 0x11366 0x11366
      , UnicodeRange 0x11370 0x11370 ] )
  , ( Greek
    , [ UnicodeRange 0x0370 0x0370
      , UnicodeRange 0x0375 0x0375
      , UnicodeRange 0x037A 0x037A
      , UnicodeCodePoint 0x037F
      , UnicodeCodePoint 0x0384
      , UnicodeCodePoint 0x0386
      , UnicodeRange 0x0388 0x0388
      , UnicodeCodePoint 0x038C
      , UnicodeRange 0x038E 0x038E
      , UnicodeRange 0x03A3 0x03A3
      , UnicodeRange 0x03F0 0x03F0
      , UnicodeRange 0x1D26 0x1D26
      , UnicodeRange 0x1D5D 0x1D5D
      , UnicodeRange 0x1D66 0x1D66
      , UnicodeCodePoint 0x1DBF
      , UnicodeRange 0x1F00 0x1F00
      , UnicodeRange 0x1F18 0x1F18
      , UnicodeRange 0x1F20 0x1F20
      , UnicodeRange 0x1F48 0x1F48
      , UnicodeRange 0x1F50 0x1F50
      , UnicodeCodePoint 0x1F59
      , UnicodeCodePoint 0x1F5B
      , UnicodeCodePoint 0x1F5D
      , UnicodeRange 0x1F5F 0x1F5F
      , UnicodeRange 0x1F80 0x1F80
      , UnicodeRange 0x1FB6 0x1FB6
      , UnicodeRange 0x1FC6 0x1FC6
      , UnicodeRange 0x1FD6 0x1FD6
      , UnicodeRange 0x1FDD 0x1FDD
      , UnicodeRange 0x1FF2 0x1FF2
      , UnicodeRange 0x1FF6 0x1FF6
      , UnicodeCodePoint 0x2126
      , UnicodeCodePoint 0xAB65
      , UnicodeRange 0x10140 0x10140
      , UnicodeCodePoint 0x101A0
      , UnicodeRange 0x1D200 0x1D200 ] )
  , ( Gujarati
    , [ UnicodeRange 0x0A81 0x0A81
      , UnicodeRange 0x0A85 0x0A85
      , UnicodeRange 0x0A8F 0x0A8F
      , UnicodeRange 0x0A93 0x0A93
      , UnicodeRange 0x0AAA 0x0AAA
      , UnicodeRange 0x0AB2 0x0AB2
      , UnicodeRange 0x0AB5 0x0AB5
      , UnicodeRange 0x0ABC 0x0ABC
      , UnicodeRange 0x0AC7 0x0AC7
      , UnicodeRange 0x0ACB 0x0ACB
      , UnicodeCodePoint 0x0AD0
      , UnicodeRange 0x0AE0 0x0AE0
      , UnicodeRange 0x0AE6 0x0AE6
      , UnicodeRange 0x0AF9 0x0AF9 ] )
  , ( Gunjala_Gondi
    , [ UnicodeRange 0x11D60 0x11D60
      , UnicodeRange 0x11D67 0x11D67
      , UnicodeRange 0x11D6A 0x11D6A
      , UnicodeRange 0x11D90 0x11D90
      , UnicodeRange 0x11D93 0x11D93
      , UnicodeRange 0x11DA0 0x11DA0 ] )
  , ( Gurmukhi
    , [ UnicodeRange 0x0A01 0x0A01
      , UnicodeRange 0x0A05 0x0A05
      , UnicodeRange 0x0A0F 0x0A0F
      , UnicodeRange 0x0A13 0x0A13
      , UnicodeRange 0x0A2A 0x0A2A
      , UnicodeRange 0x0A32 0x0A32
      , UnicodeRange 0x0A35 0x0A35
      , UnicodeRange 0x0A38 0x0A38
      , UnicodeCodePoint 0x0A3C
      , UnicodeRange 0x0A3E 0x0A3E
      , UnicodeRange 0x0A47 0x0A47
      , UnicodeRange 0x0A4B 0x0A4B
      , UnicodeCodePoint 0x0A51
      , UnicodeRange 0x0A59 0x0A59
      , UnicodeCodePoint 0x0A5E
      , UnicodeRange 0x0A66 0x0A66 ] )
  , ( Han
    , [ UnicodeRange 0x2E80 0x2E80
      , UnicodeRange 0x2E9B 0x2E9B
      , UnicodeRange 0x2F00 0x2F00
      , UnicodeCodePoint 0x3005
      , UnicodeCodePoint 0x3007
      , UnicodeRange 0x3021 0x3021
      , UnicodeRange 0x3038 0x3038
      , UnicodeRange 0x3400 0x3400
      , UnicodeRange 0x4E00 0x4E00
      , UnicodeRange 0xF900 0xF900
      , UnicodeRange 0xFA70 0xFA70
      , UnicodeRange 0x16FF0 0x16FF0
      , UnicodeRange 0x20000 0x20000
      , UnicodeRange 0x2A700 0x2A700
      , UnicodeRange 0x2B740 0x2B740
      , UnicodeRange 0x2B820 0x2B820
      , UnicodeRange 0x2CEB0 0x2CEB0
      , UnicodeRange 0x2F800 0x2F800
      , UnicodeRange 0x30000 0x30000 ] )
  , ( Hangul
    , [ UnicodeRange 0x1100 0x1100
      , UnicodeRange 0x302E 0x302E
      , UnicodeRange 0x3131 0x3131
      , UnicodeRange 0x3200 0x3200
      , UnicodeRange 0x3260 0x3260
      , UnicodeRange 0xA960 0xA960
      , UnicodeRange 0xAC00 0xAC00
      , UnicodeRange 0xD7B0 0xD7B0
      , UnicodeRange 0xD7CB 0xD7CB
      , UnicodeRange 0xFFA0 0xFFA0
      , UnicodeRange 0xFFC2 0xFFC2
      , UnicodeRange 0xFFCA 0xFFCA
      , UnicodeRange 0xFFD2 0xFFD2
      , UnicodeRange 0xFFDA 0xFFDA ] )
  , ( Hanifi_Rohingya
    , [ UnicodeRange 0x10D00 0x10D00
      , UnicodeRange 0x10D30 0x10D30 ] )
  , ( Hanunoo
    , [ UnicodeRange 0x1720 0x1720 ] )
  , ( Hatran
    , [ UnicodeRange 0x108E0 0x108E0
      , UnicodeRange 0x108F4 0x108F4
      , UnicodeRange 0x108FB 0x108FB ] )
  , ( Hebrew
    , [ UnicodeRange 0x0591 0x0591
      , UnicodeRange 0x05D0 0x05D0
      , UnicodeRange 0x05EF 0x05EF
      , UnicodeRange 0xFB1D 0xFB1D
      , UnicodeRange 0xFB38 0xFB38
      , UnicodeCodePoint 0xFB3E
      , UnicodeRange 0xFB40 0xFB40
      , UnicodeRange 0xFB43 0xFB43
      , UnicodeRange 0xFB46 0xFB46 ] )
  , ( Hiragana
    , [ UnicodeRange 0x3041 0x3041
      , UnicodeRange 0x309D 0x309D
      , UnicodeRange 0x1B001 0x1B001
      , UnicodeRange 0x1B150 0x1B150
      , UnicodeCodePoint 0x1F200 ] )
  , ( Imperial_Aramaic
    , [ UnicodeRange 0x10840 0x10840
      , UnicodeRange 0x10857 0x10857 ] )
  , ( Inherited
    , [ UnicodeRange 0x0300 0x0300
      , UnicodeRange 0x0485 0x0485
      , UnicodeRange 0x064B 0x064B
      , UnicodeCodePoint 0x0670
      , UnicodeRange 0x0951 0x0951
      , UnicodeRange 0x1AB0 0x1AB0
      , UnicodeRange 0x1CD0 0x1CD0
      , UnicodeRange 0x1CD4 0x1CD4
      , UnicodeRange 0x1CE2 0x1CE2
      , UnicodeCodePoint 0x1CED
      , UnicodeCodePoint 0x1CF4
      , UnicodeRange 0x1CF8 0x1CF8
      , UnicodeRange 0x1DC0 0x1DC0
      , UnicodeRange 0x1DFB 0x1DFB
      , UnicodeRange 0x200C 0x200C
      , UnicodeRange 0x20D0 0x20D0
      , UnicodeRange 0x302A 0x302A
      , UnicodeRange 0x3099 0x3099
      , UnicodeRange 0xFE00 0xFE00
      , UnicodeRange 0xFE20 0xFE20
      , UnicodeCodePoint 0x101FD
      , UnicodeCodePoint 0x102E0
      , UnicodeCodePoint 0x1133B
      , UnicodeRange 0x1D167 0x1D167
      , UnicodeRange 0x1D17B 0x1D17B
      , UnicodeRange 0x1D185 0x1D185
      , UnicodeRange 0x1D1AA 0x1D1AA
      , UnicodeRange 0xE0100 0xE0100 ] )
  , ( Inscriptional_Pahlavi
    , [ UnicodeRange 0x10B60 0x10B60
      , UnicodeRange 0x10B78 0x10B78 ] )
  , ( Inscriptional_Parthian
    , [ UnicodeRange 0x10B40 0x10B40
      , UnicodeRange 0x10B58 0x10B58 ] )
  , ( Javanese
    , [ UnicodeRange 0xA980 0xA980
      , UnicodeRange 0xA9D0 0xA9D0
      , UnicodeRange 0xA9DE 0xA9DE ] )
  , ( Kaithi
    , [ UnicodeRange 0x11080 0x11080
      , UnicodeCodePoint 0x110CD ] )
  , ( Kannada
    , [ UnicodeRange 0x0C80 0x0C80
      , UnicodeRange 0x0C8E 0x0C8E
      , UnicodeRange 0x0C92 0x0C92
      , UnicodeRange 0x0CAA 0x0CAA
      , UnicodeRange 0x0CB5 0x0CB5
      , UnicodeRange 0x0CBC 0x0CBC
      , UnicodeRange 0x0CC6 0x0CC6
      , UnicodeRange 0x0CCA 0x0CCA
      , UnicodeRange 0x0CD5 0x0CD5
      , UnicodeCodePoint 0x0CDE
      , UnicodeRange 0x0CE0 0x0CE0
      , UnicodeRange 0x0CE6 0x0CE6
      , UnicodeRange 0x0CF1 0x0CF1 ] )
  , ( Katakana
    , [ UnicodeRange 0x30A1 0x30A1
      , UnicodeRange 0x30FD 0x30FD
      , UnicodeRange 0x31F0 0x31F0
      , UnicodeRange 0x32D0 0x32D0
      , UnicodeRange 0x3300 0x3300
      , UnicodeRange 0xFF66 0xFF66
      , UnicodeRange 0xFF71 0xFF71
      , UnicodeCodePoint 0x1B000
      , UnicodeRange 0x1B164 0x1B164 ] )
  , ( Kayah_Li
    , [ UnicodeRange 0xA900 0xA900
      , UnicodeCodePoint 0xA92F ] )
  , ( Kharoshthi
    , [ UnicodeRange 0x10A00 0x10A00
      , UnicodeRange 0x10A05 0x10A05
      , UnicodeRange 0x10A0C 0x10A0C
      , UnicodeRange 0x10A15 0x10A15
      , UnicodeRange 0x10A19 0x10A19
      , UnicodeRange 0x10A38 0x10A38
      , UnicodeRange 0x10A3F 0x10A3F
      , UnicodeRange 0x10A50 0x10A50 ] )
  , ( Khitan_Small_Script
    , [ UnicodeCodePoint 0x16FE4
      , UnicodeRange 0x18B00 0x18B00 ] )
  , ( Khmer
    , [ UnicodeRange 0x1780 0x1780
      , UnicodeRange 0x17E0 0x17E0
      , UnicodeRange 0x17F0 0x17F0
      , UnicodeRange 0x19E0 0x19E0 ] )
  , ( Khojki
    , [ UnicodeRange 0x11200 0x11200
      , UnicodeRange 0x11213 0x11213 ] )
  , ( Khudawadi
    , [ UnicodeRange 0x112B0 0x112B0
      , UnicodeRange 0x112F0 0x112F0 ] )
  , ( Lao
    , [ UnicodeRange 0x0E81 0x0E81
      , UnicodeCodePoint 0x0E84
      , UnicodeRange 0x0E86 0x0E86
      , UnicodeRange 0x0E8C 0x0E8C
      , UnicodeCodePoint 0x0EA5
      , UnicodeRange 0x0EA7 0x0EA7
      , UnicodeRange 0x0EC0 0x0EC0
      , UnicodeCodePoint 0x0EC6
      , UnicodeRange 0x0EC8 0x0EC8
      , UnicodeRange 0x0ED0 0x0ED0
      , UnicodeRange 0x0EDC 0x0EDC ] )
  , ( Latin
    , [ UnicodeRange 0x0041 0x0041
      , UnicodeRange 0x0061 0x0061
      , UnicodeCodePoint 0x00AA
      , UnicodeCodePoint 0x00BA
      , UnicodeRange 0x00C0 0x00C0
      , UnicodeRange 0x00D8 0x00D8
      , UnicodeRange 0x00F8 0x00F8
      , UnicodeRange 0x02E0 0x02E0
      , UnicodeRange 0x1D00 0x1D00
      , UnicodeRange 0x1D2C 0x1D2C
      , UnicodeRange 0x1D62 0x1D62
      , UnicodeRange 0x1D6B 0x1D6B
      , UnicodeRange 0x1D79 0x1D79
      , UnicodeRange 0x1E00 0x1E00
      , UnicodeCodePoint 0x2071
      , UnicodeCodePoint 0x207F
      , UnicodeRange 0x2090 0x2090
      , UnicodeRange 0x212A 0x212A
      , UnicodeCodePoint 0x2132
      , UnicodeCodePoint 0x214E
      , UnicodeRange 0x2160 0x2160
      , UnicodeRange 0x2C60 0x2C60
      , UnicodeRange 0xA722 0xA722
      , UnicodeRange 0xA78B 0xA78B
      , UnicodeRange 0xA7C2 0xA7C2
      , UnicodeRange 0xA7F5 0xA7F5
      , UnicodeRange 0xAB30 0xAB30
      , UnicodeRange 0xAB5C 0xAB5C
      , UnicodeRange 0xAB66 0xAB66
      , UnicodeRange 0xFB00 0xFB00
      , UnicodeRange 0xFF21 0xFF21
      , UnicodeRange 0xFF41 0xFF41 ] )
  , ( Lepcha
    , [ UnicodeRange 0x1C00 0x1C00
      , UnicodeRange 0x1C3B 0x1C3B
      , UnicodeRange 0x1C4D 0x1C4D ] )
  , ( Limbu
    , [ UnicodeRange 0x1900 0x1900
      , UnicodeRange 0x1920 0x1920
      , UnicodeRange 0x1930 0x1930
      , UnicodeCodePoint 0x1940
      , UnicodeRange 0x1944 0x1944 ] )
  , ( Linear_A
    , [ UnicodeRange 0x10600 0x10600
      , UnicodeRange 0x10740 0x10740
      , UnicodeRange 0x10760 0x10760 ] )
  , ( Linear_B
    , [ UnicodeRange 0x10000 0x10000
      , UnicodeRange 0x1000D 0x1000D
      , UnicodeRange 0x10028 0x10028
      , UnicodeRange 0x1003C 0x1003C
      , UnicodeRange 0x1003F 0x1003F
      , UnicodeRange 0x10050 0x10050
      , UnicodeRange 0x10080 0x10080 ] )
  , ( Lisu
    , [ UnicodeRange 0xA4D0 0xA4D0
      , UnicodeCodePoint 0x11FB0 ] )
  , ( Lycian
    , [ UnicodeRange 0x10280 0x10280 ] )
  , ( Lydian
    , [ UnicodeRange 0x10920 0x10920
      , UnicodeCodePoint 0x1093F ] )
  , ( Mahajani
    , [ UnicodeRange 0x11150 0x11150 ] )
  , ( Makasar
    , [ UnicodeRange 0x11EE0 0x11EE0 ] )
  , ( Malayalam
    , [ UnicodeRange 0x0D00 0x0D00
      , UnicodeRange 0x0D0E 0x0D0E
      , UnicodeRange 0x0D12 0x0D12
      , UnicodeRange 0x0D46 0x0D46
      , UnicodeRange 0x0D4A 0x0D4A
      , UnicodeRange 0x0D54 0x0D54
      , UnicodeRange 0x0D66 0x0D66 ] )
  , ( Mandaic
    , [ UnicodeRange 0x0840 0x0840
      , UnicodeCodePoint 0x085E ] )
  , ( Manichaean
    , [ UnicodeRange 0x10AC0 0x10AC0
      , UnicodeRange 0x10AEB 0x10AEB ] )
  , ( Marchen
    , [ UnicodeRange 0x11C70 0x11C70
      , UnicodeRange 0x11C92 0x11C92
      , UnicodeRange 0x11CA9 0x11CA9 ] )
  , ( Masaram_Gondi
    , [ UnicodeRange 0x11D00 0x11D00
      , UnicodeRange 0x11D08 0x11D08
      , UnicodeRange 0x11D0B 0x11D0B
      , UnicodeCodePoint 0x11D3A
      , UnicodeRange 0x11D3C 0x11D3C
      , UnicodeRange 0x11D3F 0x11D3F
      , UnicodeRange 0x11D50 0x11D50 ] )
  , ( Medefaidrin
    , [ UnicodeRange 0x16E40 0x16E40 ] )
  , ( Meetei_Mayek
    , [ UnicodeRange 0xAAE0 0xAAE0
      , UnicodeRange 0xABC0 0xABC0
      , UnicodeRange 0xABF0 0xABF0 ] )
  , ( Mende_Kikakui
    , [ UnicodeRange 0x1E800 0x1E800
      , UnicodeRange 0x1E8C7 0x1E8C7 ] )
  , ( Meroitic_Cursive
    , [ UnicodeRange 0x109A0 0x109A0
      , UnicodeRange 0x109BC 0x109BC
      , UnicodeRange 0x109D2 0x109D2 ] )
  , ( Meroitic_Hieroglyphs
    , [ UnicodeRange 0x10980 0x10980 ] )
  , ( Miao
    , [ UnicodeRange 0x16F00 0x16F00
      , UnicodeRange 0x16F4F 0x16F4F
      , UnicodeRange 0x16F8F 0x16F8F ] )
  , ( Modi
    , [ UnicodeRange 0x11600 0x11600
      , UnicodeRange 0x11650 0x11650 ] )
  , ( Mongolian
    , [ UnicodeRange 0x1800 0x1800
      , UnicodeCodePoint 0x1804
      , UnicodeRange 0x1806 0x1806
      , UnicodeRange 0x1810 0x1810
      , UnicodeRange 0x1820 0x1820
      , UnicodeRange 0x1880 0x1880
      , UnicodeRange 0x11660 0x11660 ] )
  , ( Mro
    , [ UnicodeRange 0x16A40 0x16A40
      , UnicodeRange 0x16A60 0x16A60
      , UnicodeRange 0x16A6E 0x16A6E ] )
  , ( Multani
    , [ UnicodeRange 0x11280 0x11280
      , UnicodeCodePoint 0x11288
      , UnicodeRange 0x1128A 0x1128A
      , UnicodeRange 0x1128F 0x1128F
      , UnicodeRange 0x1129F 0x1129F ] )
  , ( Myanmar
    , [ UnicodeRange 0x1000 0x1000
      , UnicodeRange 0xA9E0 0xA9E0
      , UnicodeRange 0xAA60 0xAA60 ] )
  , ( Nabataean
    , [ UnicodeRange 0x10880 0x10880
      , UnicodeRange 0x108A7 0x108A7 ] )
  , ( Nandinagari
    , [ UnicodeRange 0x119A0 0x119A0
      , UnicodeRange 0x119AA 0x119AA
      , UnicodeRange 0x119DA 0x119DA ] )
  , ( New_Tai_Lue
    , [ UnicodeRange 0x1980 0x1980
      , UnicodeRange 0x19B0 0x19B0
      , UnicodeRange 0x19D0 0x19D0
      , UnicodeRange 0x19DE 0x19DE ] )
  , ( Newa
    , [ UnicodeRange 0x11400 0x11400
      , UnicodeRange 0x1145D 0x1145D ] )
  , ( Nko
    , [ UnicodeRange 0x07C0 0x07C0
      , UnicodeRange 0x07FD 0x07FD ] )
  , ( Nushu
    , [ UnicodeCodePoint 0x16FE1
      , UnicodeRange 0x1B170 0x1B170 ] )
  , ( Nyiakeng_Puachue_Hmong
    , [ UnicodeRange 0x1E100 0x1E100
      , UnicodeRange 0x1E130 0x1E130
      , UnicodeRange 0x1E140 0x1E140
      , UnicodeRange 0x1E14E 0x1E14E ] )
  , ( Ogham
    , [ UnicodeRange 0x1680 0x1680 ] )
  , ( Ol_Chiki
    , [ UnicodeRange 0x1C50 0x1C50 ] )
  , ( Old_Hungarian
    , [ UnicodeRange 0x10C80 0x10C80
      , UnicodeRange 0x10CC0 0x10CC0
      , UnicodeRange 0x10CFA 0x10CFA ] )
  , ( Old_Italic
    , [ UnicodeRange 0x10300 0x10300
      , UnicodeRange 0x1032D 0x1032D ] )
  , ( Old_North_Arabian
    , [ UnicodeRange 0x10A80 0x10A80 ] )
  , ( Old_Permic
    , [ UnicodeRange 0x10350 0x10350 ] )
  , ( Old_Persian
    , [ UnicodeRange 0x103A0 0x103A0
      , UnicodeRange 0x103C8 0x103C8 ] )
  , ( Old_Sogdian
    , [ UnicodeRange 0x10F00 0x10F00 ] )
  , ( Old_South_Arabian
    , [ UnicodeRange 0x10A60 0x10A60 ] )
  , ( Old_Turkic
    , [ UnicodeRange 0x10C00 0x10C00 ] )
  , ( Oriya
    , [ UnicodeRange 0x0B01 0x0B01
      , UnicodeRange 0x0B05 0x0B05
      , UnicodeRange 0x0B0F 0x0B0F
      , UnicodeRange 0x0B13 0x0B13
      , UnicodeRange 0x0B2A 0x0B2A
      , UnicodeRange 0x0B32 0x0B32
      , UnicodeRange 0x0B35 0x0B35
      , UnicodeRange 0x0B3C 0x0B3C
      , UnicodeRange 0x0B47 0x0B47
      , UnicodeRange 0x0B4B 0x0B4B
      , UnicodeRange 0x0B55 0x0B55
      , UnicodeRange 0x0B5C 0x0B5C
      , UnicodeRange 0x0B5F 0x0B5F
      , UnicodeRange 0x0B66 0x0B66 ] )
  , ( Osage
    , [ UnicodeRange 0x104B0 0x104B0
      , UnicodeRange 0x104D8 0x104D8 ] )
  , ( Osmanya
    , [ UnicodeRange 0x10480 0x10480
      , UnicodeRange 0x104A0 0x104A0 ] )
  , ( Pahawh_Hmong
    , [ UnicodeRange 0x16B00 0x16B00
      , UnicodeRange 0x16B50 0x16B50
      , UnicodeRange 0x16B5B 0x16B5B
      , UnicodeRange 0x16B63 0x16B63
      , UnicodeRange 0x16B7D 0x16B7D ] )
  , ( Palmyrene
    , [ UnicodeRange 0x10860 0x10860 ] )
  , ( Pau_Cin_Hau
    , [ UnicodeRange 0x11AC0 0x11AC0 ] )
  , ( Phags_Pa
    , [ UnicodeRange 0xA840 0xA840 ] )
  , ( Phoenician
    , [ UnicodeRange 0x10900 0x10900
      , UnicodeCodePoint 0x1091F ] )
  , ( Psalter_Pahlavi
    , [ UnicodeRange 0x10B80 0x10B80
      , UnicodeRange 0x10B99 0x10B99
      , UnicodeRange 0x10BA9 0x10BA9 ] )
  , ( Rejang
    , [ UnicodeRange 0xA930 0xA930
      , UnicodeCodePoint 0xA95F ] )
  , ( Runic
    , [ UnicodeRange 0x16A0 0x16A0
      , UnicodeRange 0x16EE 0x16EE ] )
  , ( Samaritan
    , [ UnicodeRange 0x0800 0x0800
      , UnicodeRange 0x0830 0x0830 ] )
  , ( Saurashtra
    , [ UnicodeRange 0xA880 0xA880
      , UnicodeRange 0xA8CE 0xA8CE ] )
  , ( Sharada
    , [ UnicodeRange 0x11180 0x11180 ] )
  , ( Shavian
    , [ UnicodeRange 0x10450 0x10450 ] )
  , ( Siddham
    , [ UnicodeRange 0x11580 0x11580
      , UnicodeRange 0x115B8 0x115B8 ] )
  , ( SignWriting
    , [ UnicodeRange 0x1D800 0x1D800
      , UnicodeRange 0x1DA9B 0x1DA9B
      , UnicodeRange 0x1DAA1 0x1DAA1 ] )
  , ( Sinhala
    , [ UnicodeRange 0x0D81 0x0D81
      , UnicodeRange 0x0D85 0x0D85
      , UnicodeRange 0x0D9A 0x0D9A
      , UnicodeRange 0x0DB3 0x0DB3
      , UnicodeCodePoint 0x0DBD
      , UnicodeRange 0x0DC0 0x0DC0
      , UnicodeCodePoint 0x0DCA
      , UnicodeRange 0x0DCF 0x0DCF
      , UnicodeCodePoint 0x0DD6
      , UnicodeRange 0x0DD8 0x0DD8
      , UnicodeRange 0x0DE6 0x0DE6
      , UnicodeRange 0x0DF2 0x0DF2
      , UnicodeRange 0x111E1 0x111E1 ] )
  , ( Sogdian
    , [ UnicodeRange 0x10F30 0x10F30 ] )
  , ( Sora_Sompeng
    , [ UnicodeRange 0x110D0 0x110D0
      , UnicodeRange 0x110F0 0x110F0 ] )
  , ( Soyombo
    , [ UnicodeRange 0x11A50 0x11A50 ] )
  , ( Sundanese
    , [ UnicodeRange 0x1B80 0x1B80
      , UnicodeRange 0x1CC0 0x1CC0 ] )
  , ( Syloti_Nagri
    , [ UnicodeRange 0xA800 0xA800 ] )
  , ( Syriac
    , [ UnicodeRange 0x0700 0x0700
      , UnicodeRange 0x070F 0x070F
      , UnicodeRange 0x074D 0x074D
      , UnicodeRange 0x0860 0x0860 ] )
  , ( Tagalog
    , [ UnicodeRange 0x1700 0x1700
      , UnicodeRange 0x170E 0x170E ] )
  , ( Tagbanwa
    , [ UnicodeRange 0x1760 0x1760
      , UnicodeRange 0x176E 0x176E
      , UnicodeRange 0x1772 0x1772 ] )
  , ( Tai_Le
    , [ UnicodeRange 0x1950 0x1950
      , UnicodeRange 0x1970 0x1970 ] )
  , ( Tai_Tham
    , [ UnicodeRange 0x1A20 0x1A20
      , UnicodeRange 0x1A60 0x1A60
      , UnicodeRange 0x1A7F 0x1A7F
      , UnicodeRange 0x1A90 0x1A90
      , UnicodeRange 0x1AA0 0x1AA0 ] )
  , ( Tai_Viet
    , [ UnicodeRange 0xAA80 0xAA80
      , UnicodeRange 0xAADB 0xAADB ] )
  , ( Takri
    , [ UnicodeRange 0x11680 0x11680
      , UnicodeRange 0x116C0 0x116C0 ] )
  , ( Tamil
    , [ UnicodeRange 0x0B82 0x0B82
      , UnicodeRange 0x0B85 0x0B85
      , UnicodeRange 0x0B8E 0x0B8E
      , UnicodeRange 0x0B92 0x0B92
      , UnicodeRange 0x0B99 0x0B99
      , UnicodeCodePoint 0x0B9C
      , UnicodeRange 0x0B9E 0x0B9E
      , UnicodeRange 0x0BA3 0x0BA3
      , UnicodeRange 0x0BA8 0x0BA8
      , UnicodeRange 0x0BAE 0x0BAE
      , UnicodeRange 0x0BBE 0x0BBE
      , UnicodeRange 0x0BC6 0x0BC6
      , UnicodeRange 0x0BCA 0x0BCA
      , UnicodeCodePoint 0x0BD0
      , UnicodeCodePoint 0x0BD7
      , UnicodeRange 0x0BE6 0x0BE6
      , UnicodeRange 0x11FC0 0x11FC0
      , UnicodeCodePoint 0x11FFF ] )
  , ( Tangut
    , [ UnicodeCodePoint 0x16FE0
      , UnicodeRange 0x17000 0x17000
      , UnicodeRange 0x18800 0x18800
      , UnicodeRange 0x18D00 0x18D00 ] )
  , ( Telugu
    , [ UnicodeRange 0x0C00 0x0C00
      , UnicodeRange 0x0C0E 0x0C0E
      , UnicodeRange 0x0C12 0x0C12
      , UnicodeRange 0x0C2A 0x0C2A
      , UnicodeRange 0x0C3D 0x0C3D
      , UnicodeRange 0x0C46 0x0C46
      , UnicodeRange 0x0C4A 0x0C4A
      , UnicodeRange 0x0C55 0x0C55
      , UnicodeRange 0x0C58 0x0C58
      , UnicodeRange 0x0C60 0x0C60
      , UnicodeRange 0x0C66 0x0C66
      , UnicodeRange 0x0C77 0x0C77 ] )
  , ( Thaana
    , [ UnicodeRange 0x0780 0x0780 ] )
  , ( Thai
    , [ UnicodeRange 0x0E01 0x0E01
      , UnicodeRange 0x0E40 0x0E40 ] )
  , ( Tibetan
    , [ UnicodeRange 0x0F00 0x0F00
      , UnicodeRange 0x0F49 0x0F49
      , UnicodeRange 0x0F71 0x0F71
      , UnicodeRange 0x0F99 0x0F99
      , UnicodeRange 0x0FBE 0x0FBE
      , UnicodeRange 0x0FCE 0x0FCE
      , UnicodeRange 0x0FD9 0x0FD9 ] )
  , ( Tifinagh
    , [ UnicodeRange 0x2D30 0x2D30
      , UnicodeRange 0x2D6F 0x2D6F
      , UnicodeCodePoint 0x2D7F ] )
  , ( Tirhuta
    , [ UnicodeRange 0x11480 0x11480
      , UnicodeRange 0x114D0 0x114D0 ] )
  , ( Ugaritic
    , [ UnicodeRange 0x10380 0x10380
      , UnicodeCodePoint 0x1039F ] )
  , ( Vai
    , [ UnicodeRange 0xA500 0xA500 ] )
  , ( Wancho
    , [ UnicodeRange 0x1E2C0 0x1E2C0
      , UnicodeCodePoint 0x1E2FF ] )
  , ( Warang_Citi
    , [ UnicodeRange 0x118A0 0x118A0
      , UnicodeCodePoint 0x118FF ] )
  , ( Yezidi
    , [ UnicodeRange 0x10E80 0x10E80
      , UnicodeRange 0x10EAB 0x10EAB
      , UnicodeRange 0x10EB0 0x10EB0 ] )
  , ( Yi
    , [ UnicodeRange 0xA000 0xA000
      , UnicodeRange 0xA490 0xA490 ] )
  , ( Zanabazar_Square
    , [ UnicodeRange 0x11A00 0x11A00 ] ) ]
