module Klay.Utils.Unicode.Circled
  ( circled
  , circledNumber
  , negativeCircledNumber )
  where

import Data.Char (ord, chr)

import Klay.Utils.Unicode (unicodeOffset)

circled :: Char -> Maybe Char
circled c
  | 'A' <= c && c <= 'Z' = unicodeOffset 0x2475 c
  | 'a' <= c && c <= 'z' = unicodeOffset 0x246f c
  | '0' <= c && c <= '9' = circledNumber (ord c - ord '0')
circled _ = Nothing

circledNumber :: Int -> Maybe Char
circledNumber 0 = Just '⓪'
circledNumber n
  | 1 <= n && n <= 20 = Just $ chr (0x245F + n)
circledNumber _ = Nothing

negativeCircledNumber :: Int -> Maybe Char
negativeCircledNumber 0 = Just '⓿'
negativeCircledNumber n
  | 1 <= n && n <= 10 = Just $ chr (0x2775 + n)
  | 11 <= n && n <= 20 = Just $ chr (0x24e0 + n)
negativeCircledNumber _ = Nothing
