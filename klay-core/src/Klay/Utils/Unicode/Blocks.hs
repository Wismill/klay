-- [NOTE] Auto-generated. Do not edit manually

{-# LANGUAGE OverloadedLists   #-}
{- HLINT ignore "Use camelCase" -}

module Klay.Utils.Unicode.Blocks
  ( UnicodeBlock(..)
  , UnicodeBlockRange(..)
  , unicodeBlock
  , unicodeBlocks
  , unicodeBlockRange
  , prettyUnicodeBlock
  ) where

import Numeric (showHex)
import Data.Char (toUpper)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map

import TextShow (TextShow, FromStringShow(..))
import Data.Csv


-- | Unicode Block property values. See [Report TR44](http://www.unicode.org/reports/tr44/).
data UnicodeBlock
  = Adlam
  | Aegean_Numbers
  | Ahom
  | Alchemical_Symbols
  | Alphabetic_Presentation_Forms
  | Anatolian_Hieroglyphs
  | Ancient_Greek_Musical_Notation
  | Ancient_Greek_Numbers
  | Ancient_Symbols
  | Arabic
  | Arabic_Extended_A
  | Arabic_Mathematical_Alphabetic_Symbols
  | Arabic_Presentation_Forms_A
  | Arabic_Presentation_Forms_B
  | Arabic_Supplement
  | Armenian
  | Arrows
  | Avestan
  | Balinese
  | Bamum
  | Bamum_Supplement
  | Basic_Latin
  | Bassa_Vah
  | Batak
  | Bengali
  | Bhaiksuki
  | Block_Elements
  | Bopomofo
  | Bopomofo_Extended
  | Box_Drawing
  | Brahmi
  | Braille_Patterns
  | Buginese
  | Buhid
  | Byzantine_Musical_Symbols
  | CJK_Compatibility
  | CJK_Compatibility_Forms
  | CJK_Compatibility_Ideographs
  | CJK_Compatibility_Ideographs_Supplement
  | CJK_Radicals_Supplement
  | CJK_Strokes
  | CJK_Symbols_and_Punctuation
  | CJK_Unified_Ideographs
  | CJK_Unified_Ideographs_Extension_A
  | CJK_Unified_Ideographs_Extension_B
  | CJK_Unified_Ideographs_Extension_C
  | CJK_Unified_Ideographs_Extension_D
  | CJK_Unified_Ideographs_Extension_E
  | CJK_Unified_Ideographs_Extension_F
  | CJK_Unified_Ideographs_Extension_G
  | Carian
  | Caucasian_Albanian
  | Chakma
  | Cham
  | Cherokee
  | Cherokee_Supplement
  | Chess_Symbols
  | Chorasmian
  | Combining_Diacritical_Marks
  | Combining_Diacritical_Marks_Extended
  | Combining_Diacritical_Marks_Supplement
  | Combining_Diacritical_Marks_for_Symbols
  | Combining_Half_Marks
  | Common_Indic_Number_Forms
  | Control_Pictures
  | Coptic
  | Coptic_Epact_Numbers
  | Counting_Rod_Numerals
  | Cuneiform
  | Cuneiform_Numbers_and_Punctuation
  | Currency_Symbols
  | Cypriot_Syllabary
  | Cyrillic
  | Cyrillic_Extended_A
  | Cyrillic_Extended_B
  | Cyrillic_Extended_C
  | Cyrillic_Supplement
  | Deseret
  | Devanagari
  | Devanagari_Extended
  | Dingbats
  | Dives_Akuru
  | Dogra
  | Domino_Tiles
  | Duployan
  | Early_Dynastic_Cuneiform
  | Egyptian_Hieroglyph_Format_Controls
  | Egyptian_Hieroglyphs
  | Elbasan
  | Elymaic
  | Emoticons
  | Enclosed_Alphanumeric_Supplement
  | Enclosed_Alphanumerics
  | Enclosed_CJK_Letters_and_Months
  | Enclosed_Ideographic_Supplement
  | Ethiopic
  | Ethiopic_Extended
  | Ethiopic_Extended_A
  | Ethiopic_Supplement
  | General_Punctuation
  | Geometric_Shapes
  | Geometric_Shapes_Extended
  | Georgian
  | Georgian_Extended
  | Georgian_Supplement
  | Glagolitic
  | Glagolitic_Supplement
  | Gothic
  | Grantha
  | Greek_Extended
  | Greek_and_Coptic
  | Gujarati
  | Gunjala_Gondi
  | Gurmukhi
  | Halfwidth_and_Fullwidth_Forms
  | Hangul_Compatibility_Jamo
  | Hangul_Jamo
  | Hangul_Jamo_Extended_A
  | Hangul_Jamo_Extended_B
  | Hangul_Syllables
  | Hanifi_Rohingya
  | Hanunoo
  | Hatran
  | Hebrew
  | High_Private_Use_Surrogates
  | High_Surrogates
  | Hiragana
  | IPA_Extensions
  | Ideographic_Description_Characters
  | Ideographic_Symbols_and_Punctuation
  | Imperial_Aramaic
  | Indic_Siyaq_Numbers
  | Inscriptional_Pahlavi
  | Inscriptional_Parthian
  | Javanese
  | Kaithi
  | Kana_Extended_A
  | Kana_Supplement
  | Kanbun
  | Kangxi_Radicals
  | Kannada
  | Katakana
  | Katakana_Phonetic_Extensions
  | Kayah_Li
  | Kharoshthi
  | Khitan_Small_Script
  | Khmer
  | Khmer_Symbols
  | Khojki
  | Khudawadi
  | Lao
  | Latin_1_Supplement
  | Latin_Extended_A
  | Latin_Extended_Additional
  | Latin_Extended_B
  | Latin_Extended_C
  | Latin_Extended_D
  | Latin_Extended_E
  | Lepcha
  | Letterlike_Symbols
  | Limbu
  | Linear_A
  | Linear_B_Ideograms
  | Linear_B_Syllabary
  | Lisu
  | Lisu_Supplement
  | Low_Surrogates
  | Lycian
  | Lydian
  | Mahajani
  | Mahjong_Tiles
  | Makasar
  | Malayalam
  | Mandaic
  | Manichaean
  | Marchen
  | Masaram_Gondi
  | Mathematical_Alphanumeric_Symbols
  | Mathematical_Operators
  | Mayan_Numerals
  | Medefaidrin
  | Meetei_Mayek
  | Meetei_Mayek_Extensions
  | Mende_Kikakui
  | Meroitic_Cursive
  | Meroitic_Hieroglyphs
  | Miao
  | Miscellaneous_Mathematical_Symbols_A
  | Miscellaneous_Mathematical_Symbols_B
  | Miscellaneous_Symbols
  | Miscellaneous_Symbols_and_Arrows
  | Miscellaneous_Symbols_and_Pictographs
  | Miscellaneous_Technical
  | Modi
  | Modifier_Tone_Letters
  | Mongolian
  | Mongolian_Supplement
  | Mro
  | Multani
  | Musical_Symbols
  | Myanmar
  | Myanmar_Extended_A
  | Myanmar_Extended_B
  | NKo
  | Nabataean
  | Nandinagari
  | New_Tai_Lue
  | Newa
  | No_Block
  | Number_Forms
  | Nushu
  | Nyiakeng_Puachue_Hmong
  | Ogham
  | Ol_Chiki
  | Old_Hungarian
  | Old_Italic
  | Old_North_Arabian
  | Old_Permic
  | Old_Persian
  | Old_Sogdian
  | Old_South_Arabian
  | Old_Turkic
  | Optical_Character_Recognition
  | Oriya
  | Ornamental_Dingbats
  | Osage
  | Osmanya
  | Ottoman_Siyaq_Numbers
  | Pahawh_Hmong
  | Palmyrene
  | Pau_Cin_Hau
  | Phags_pa
  | Phaistos_Disc
  | Phoenician
  | Phonetic_Extensions
  | Phonetic_Extensions_Supplement
  | Playing_Cards
  | Private_Use_Area
  | Psalter_Pahlavi
  | Rejang
  | Rumi_Numeral_Symbols
  | Runic
  | Samaritan
  | Saurashtra
  | Sharada
  | Shavian
  | Shorthand_Format_Controls
  | Siddham
  | Sinhala
  | Sinhala_Archaic_Numbers
  | Small_Form_Variants
  | Small_Kana_Extension
  | Sogdian
  | Sora_Sompeng
  | Soyombo
  | Spacing_Modifier_Letters
  | Specials
  | Sundanese
  | Sundanese_Supplement
  | Superscripts_and_Subscripts
  | Supplemental_Arrows_A
  | Supplemental_Arrows_B
  | Supplemental_Arrows_C
  | Supplemental_Mathematical_Operators
  | Supplemental_Punctuation
  | Supplemental_Symbols_and_Pictographs
  | Supplementary_Private_Use_Area_A
  | Supplementary_Private_Use_Area_B
  | Sutton_SignWriting
  | Syloti_Nagri
  | Symbols_and_Pictographs_Extended_A
  | Symbols_for_Legacy_Computing
  | Syriac
  | Syriac_Supplement
  | Tagalog
  | Tagbanwa
  | Tags
  | Tai_Le
  | Tai_Tham
  | Tai_Viet
  | Tai_Xuan_Jing_Symbols
  | Takri
  | Tamil
  | Tamil_Supplement
  | Tangut
  | Tangut_Components
  | Tangut_Supplement
  | Telugu
  | Thaana
  | Thai
  | Tibetan
  | Tifinagh
  | Tirhuta
  | Transport_and_Map_Symbols
  | Ugaritic
  | Unified_Canadian_Aboriginal_Syllabics
  | Unified_Canadian_Aboriginal_Syllabics_Extended
  | Vai
  | Variation_Selectors
  | Variation_Selectors_Supplement
  | Vedic_Extensions
  | Vertical_Forms
  | Wancho
  | Warang_Citi
  | Yezidi
  | Yi_Radicals
  | Yi_Syllables
  | Yijing_Hexagram_Symbols
  | Zanabazar_Square
  deriving (Eq, Ord, Enum, Bounded, Show, Read)
  deriving TextShow via (FromStringShow UnicodeBlock)

instance ToField UnicodeBlock where
  toField = toField . show

instance FromField UnicodeBlock where
  parseField = fmap read . parseField

data UnicodeBlockRange = UnicodeBlockRange
  { _blockStart :: !Int   -- ^ First code point of the range
  , _blockEnd :: !Int     -- ^ Last code point of the range
  , _blockName :: !String -- ^ Block name
  } deriving (Eq, Show)

instance Ord UnicodeBlockRange where
  UnicodeBlockRange{_blockStart=s1} `compare` UnicodeBlockRange{_blockStart=s2} = compare s1 s2

prettyUnicodeBlock :: UnicodeBlock -> String
prettyUnicodeBlock block = case Map.lookup block unicodeBlocks of
  Nothing -> "No Block"
  Just (UnicodeBlockRange start end name) -> mconcat
    [ name, " (0x", toUpper <$> showHex start mempty, ", 0x", toUpper <$> showHex end mempty, ")"]

unicodeBlock :: Char -> UnicodeBlock
unicodeBlock c
  | c <= '\x007F' = Basic_Latin
  | c <= '\x00FF' = Latin_1_Supplement
  | c <= '\x017F' = Latin_Extended_A
  | c <= '\x024F' = Latin_Extended_B
  | c <= '\x02AF' = IPA_Extensions
  | c <= '\x02FF' = Spacing_Modifier_Letters
  | c <= '\x036F' = Combining_Diacritical_Marks
  | c <= '\x03FF' = Greek_and_Coptic
  | c <= '\x04FF' = Cyrillic
  | c <= '\x052F' = Cyrillic_Supplement
  | c <= '\x058F' = Armenian
  | c <= '\x05FF' = Hebrew
  | c <= '\x06FF' = Arabic
  | c <= '\x074F' = Syriac
  | c <= '\x077F' = Arabic_Supplement
  | c <= '\x07BF' = Thaana
  | c <= '\x07FF' = NKo
  | c <= '\x083F' = Samaritan
  | c <= '\x085F' = Mandaic
  | c <= '\x086F' = Syriac_Supplement
  | c <= '\x089F' = No_Block
  | c <= '\x08FF' = Arabic_Extended_A
  | c <= '\x097F' = Devanagari
  | c <= '\x09FF' = Bengali
  | c <= '\x0A7F' = Gurmukhi
  | c <= '\x0AFF' = Gujarati
  | c <= '\x0B7F' = Oriya
  | c <= '\x0BFF' = Tamil
  | c <= '\x0C7F' = Telugu
  | c <= '\x0CFF' = Kannada
  | c <= '\x0D7F' = Malayalam
  | c <= '\x0DFF' = Sinhala
  | c <= '\x0E7F' = Thai
  | c <= '\x0EFF' = Lao
  | c <= '\x0FFF' = Tibetan
  | c <= '\x109F' = Myanmar
  | c <= '\x10FF' = Georgian
  | c <= '\x11FF' = Hangul_Jamo
  | c <= '\x137F' = Ethiopic
  | c <= '\x139F' = Ethiopic_Supplement
  | c <= '\x13FF' = Cherokee
  | c <= '\x167F' = Unified_Canadian_Aboriginal_Syllabics
  | c <= '\x169F' = Ogham
  | c <= '\x16FF' = Runic
  | c <= '\x171F' = Tagalog
  | c <= '\x173F' = Hanunoo
  | c <= '\x175F' = Buhid
  | c <= '\x177F' = Tagbanwa
  | c <= '\x17FF' = Khmer
  | c <= '\x18AF' = Mongolian
  | c <= '\x18FF' = Unified_Canadian_Aboriginal_Syllabics_Extended
  | c <= '\x194F' = Limbu
  | c <= '\x197F' = Tai_Le
  | c <= '\x19DF' = New_Tai_Lue
  | c <= '\x19FF' = Khmer_Symbols
  | c <= '\x1A1F' = Buginese
  | c <= '\x1AAF' = Tai_Tham
  | c <= '\x1AFF' = Combining_Diacritical_Marks_Extended
  | c <= '\x1B7F' = Balinese
  | c <= '\x1BBF' = Sundanese
  | c <= '\x1BFF' = Batak
  | c <= '\x1C4F' = Lepcha
  | c <= '\x1C7F' = Ol_Chiki
  | c <= '\x1C8F' = Cyrillic_Extended_C
  | c <= '\x1CBF' = Georgian_Extended
  | c <= '\x1CCF' = Sundanese_Supplement
  | c <= '\x1CFF' = Vedic_Extensions
  | c <= '\x1D7F' = Phonetic_Extensions
  | c <= '\x1DBF' = Phonetic_Extensions_Supplement
  | c <= '\x1DFF' = Combining_Diacritical_Marks_Supplement
  | c <= '\x1EFF' = Latin_Extended_Additional
  | c <= '\x1FFF' = Greek_Extended
  | c <= '\x206F' = General_Punctuation
  | c <= '\x209F' = Superscripts_and_Subscripts
  | c <= '\x20CF' = Currency_Symbols
  | c <= '\x20FF' = Combining_Diacritical_Marks_for_Symbols
  | c <= '\x214F' = Letterlike_Symbols
  | c <= '\x218F' = Number_Forms
  | c <= '\x21FF' = Arrows
  | c <= '\x22FF' = Mathematical_Operators
  | c <= '\x23FF' = Miscellaneous_Technical
  | c <= '\x243F' = Control_Pictures
  | c <= '\x245F' = Optical_Character_Recognition
  | c <= '\x24FF' = Enclosed_Alphanumerics
  | c <= '\x257F' = Box_Drawing
  | c <= '\x259F' = Block_Elements
  | c <= '\x25FF' = Geometric_Shapes
  | c <= '\x26FF' = Miscellaneous_Symbols
  | c <= '\x27BF' = Dingbats
  | c <= '\x27EF' = Miscellaneous_Mathematical_Symbols_A
  | c <= '\x27FF' = Supplemental_Arrows_A
  | c <= '\x28FF' = Braille_Patterns
  | c <= '\x297F' = Supplemental_Arrows_B
  | c <= '\x29FF' = Miscellaneous_Mathematical_Symbols_B
  | c <= '\x2AFF' = Supplemental_Mathematical_Operators
  | c <= '\x2BFF' = Miscellaneous_Symbols_and_Arrows
  | c <= '\x2C5F' = Glagolitic
  | c <= '\x2C7F' = Latin_Extended_C
  | c <= '\x2CFF' = Coptic
  | c <= '\x2D2F' = Georgian_Supplement
  | c <= '\x2D7F' = Tifinagh
  | c <= '\x2DDF' = Ethiopic_Extended
  | c <= '\x2DFF' = Cyrillic_Extended_A
  | c <= '\x2E7F' = Supplemental_Punctuation
  | c <= '\x2EFF' = CJK_Radicals_Supplement
  | c <= '\x2FDF' = Kangxi_Radicals
  | c <= '\x2FEF' = No_Block
  | c <= '\x2FFF' = Ideographic_Description_Characters
  | c <= '\x303F' = CJK_Symbols_and_Punctuation
  | c <= '\x309F' = Hiragana
  | c <= '\x30FF' = Katakana
  | c <= '\x312F' = Bopomofo
  | c <= '\x318F' = Hangul_Compatibility_Jamo
  | c <= '\x319F' = Kanbun
  | c <= '\x31BF' = Bopomofo_Extended
  | c <= '\x31EF' = CJK_Strokes
  | c <= '\x31FF' = Katakana_Phonetic_Extensions
  | c <= '\x32FF' = Enclosed_CJK_Letters_and_Months
  | c <= '\x33FF' = CJK_Compatibility
  | c <= '\x4DBF' = CJK_Unified_Ideographs_Extension_A
  | c <= '\x4DFF' = Yijing_Hexagram_Symbols
  | c <= '\x9FFF' = CJK_Unified_Ideographs
  | c <= '\xA48F' = Yi_Syllables
  | c <= '\xA4CF' = Yi_Radicals
  | c <= '\xA4FF' = Lisu
  | c <= '\xA63F' = Vai
  | c <= '\xA69F' = Cyrillic_Extended_B
  | c <= '\xA6FF' = Bamum
  | c <= '\xA71F' = Modifier_Tone_Letters
  | c <= '\xA7FF' = Latin_Extended_D
  | c <= '\xA82F' = Syloti_Nagri
  | c <= '\xA83F' = Common_Indic_Number_Forms
  | c <= '\xA87F' = Phags_pa
  | c <= '\xA8DF' = Saurashtra
  | c <= '\xA8FF' = Devanagari_Extended
  | c <= '\xA92F' = Kayah_Li
  | c <= '\xA95F' = Rejang
  | c <= '\xA97F' = Hangul_Jamo_Extended_A
  | c <= '\xA9DF' = Javanese
  | c <= '\xA9FF' = Myanmar_Extended_B
  | c <= '\xAA5F' = Cham
  | c <= '\xAA7F' = Myanmar_Extended_A
  | c <= '\xAADF' = Tai_Viet
  | c <= '\xAAFF' = Meetei_Mayek_Extensions
  | c <= '\xAB2F' = Ethiopic_Extended_A
  | c <= '\xAB6F' = Latin_Extended_E
  | c <= '\xABBF' = Cherokee_Supplement
  | c <= '\xABFF' = Meetei_Mayek
  | c <= '\xD7AF' = Hangul_Syllables
  | c <= '\xD7FF' = Hangul_Jamo_Extended_B
  | c <= '\xDB7F' = High_Surrogates
  | c <= '\xDBFF' = High_Private_Use_Surrogates
  | c <= '\xDFFF' = Low_Surrogates
  | c <= '\xF8FF' = Private_Use_Area
  | c <= '\xFAFF' = CJK_Compatibility_Ideographs
  | c <= '\xFB4F' = Alphabetic_Presentation_Forms
  | c <= '\xFDFF' = Arabic_Presentation_Forms_A
  | c <= '\xFE0F' = Variation_Selectors
  | c <= '\xFE1F' = Vertical_Forms
  | c <= '\xFE2F' = Combining_Half_Marks
  | c <= '\xFE4F' = CJK_Compatibility_Forms
  | c <= '\xFE6F' = Small_Form_Variants
  | c <= '\xFEFF' = Arabic_Presentation_Forms_B
  | c <= '\xFFEF' = Halfwidth_and_Fullwidth_Forms
  | c <= '\xFFFF' = Specials
  | c <= '\x1007F' = Linear_B_Syllabary
  | c <= '\x100FF' = Linear_B_Ideograms
  | c <= '\x1013F' = Aegean_Numbers
  | c <= '\x1018F' = Ancient_Greek_Numbers
  | c <= '\x101CF' = Ancient_Symbols
  | c <= '\x101FF' = Phaistos_Disc
  | c <= '\x1027F' = No_Block
  | c <= '\x1029F' = Lycian
  | c <= '\x102DF' = Carian
  | c <= '\x102FF' = Coptic_Epact_Numbers
  | c <= '\x1032F' = Old_Italic
  | c <= '\x1034F' = Gothic
  | c <= '\x1037F' = Old_Permic
  | c <= '\x1039F' = Ugaritic
  | c <= '\x103DF' = Old_Persian
  | c <= '\x103FF' = No_Block
  | c <= '\x1044F' = Deseret
  | c <= '\x1047F' = Shavian
  | c <= '\x104AF' = Osmanya
  | c <= '\x104FF' = Osage
  | c <= '\x1052F' = Elbasan
  | c <= '\x1056F' = Caucasian_Albanian
  | c <= '\x105FF' = No_Block
  | c <= '\x1077F' = Linear_A
  | c <= '\x107FF' = No_Block
  | c <= '\x1083F' = Cypriot_Syllabary
  | c <= '\x1085F' = Imperial_Aramaic
  | c <= '\x1087F' = Palmyrene
  | c <= '\x108AF' = Nabataean
  | c <= '\x108DF' = No_Block
  | c <= '\x108FF' = Hatran
  | c <= '\x1091F' = Phoenician
  | c <= '\x1093F' = Lydian
  | c <= '\x1097F' = No_Block
  | c <= '\x1099F' = Meroitic_Hieroglyphs
  | c <= '\x109FF' = Meroitic_Cursive
  | c <= '\x10A5F' = Kharoshthi
  | c <= '\x10A7F' = Old_South_Arabian
  | c <= '\x10A9F' = Old_North_Arabian
  | c <= '\x10ABF' = No_Block
  | c <= '\x10AFF' = Manichaean
  | c <= '\x10B3F' = Avestan
  | c <= '\x10B5F' = Inscriptional_Parthian
  | c <= '\x10B7F' = Inscriptional_Pahlavi
  | c <= '\x10BAF' = Psalter_Pahlavi
  | c <= '\x10BFF' = No_Block
  | c <= '\x10C4F' = Old_Turkic
  | c <= '\x10C7F' = No_Block
  | c <= '\x10CFF' = Old_Hungarian
  | c <= '\x10D3F' = Hanifi_Rohingya
  | c <= '\x10E5F' = No_Block
  | c <= '\x10E7F' = Rumi_Numeral_Symbols
  | c <= '\x10EBF' = Yezidi
  | c <= '\x10EFF' = No_Block
  | c <= '\x10F2F' = Old_Sogdian
  | c <= '\x10F6F' = Sogdian
  | c <= '\x10FAF' = No_Block
  | c <= '\x10FDF' = Chorasmian
  | c <= '\x10FFF' = Elymaic
  | c <= '\x1107F' = Brahmi
  | c <= '\x110CF' = Kaithi
  | c <= '\x110FF' = Sora_Sompeng
  | c <= '\x1114F' = Chakma
  | c <= '\x1117F' = Mahajani
  | c <= '\x111DF' = Sharada
  | c <= '\x111FF' = Sinhala_Archaic_Numbers
  | c <= '\x1124F' = Khojki
  | c <= '\x1127F' = No_Block
  | c <= '\x112AF' = Multani
  | c <= '\x112FF' = Khudawadi
  | c <= '\x1137F' = Grantha
  | c <= '\x113FF' = No_Block
  | c <= '\x1147F' = Newa
  | c <= '\x114DF' = Tirhuta
  | c <= '\x1157F' = No_Block
  | c <= '\x115FF' = Siddham
  | c <= '\x1165F' = Modi
  | c <= '\x1167F' = Mongolian_Supplement
  | c <= '\x116CF' = Takri
  | c <= '\x116FF' = No_Block
  | c <= '\x1173F' = Ahom
  | c <= '\x117FF' = No_Block
  | c <= '\x1184F' = Dogra
  | c <= '\x1189F' = No_Block
  | c <= '\x118FF' = Warang_Citi
  | c <= '\x1195F' = Dives_Akuru
  | c <= '\x1199F' = No_Block
  | c <= '\x119FF' = Nandinagari
  | c <= '\x11A4F' = Zanabazar_Square
  | c <= '\x11AAF' = Soyombo
  | c <= '\x11ABF' = No_Block
  | c <= '\x11AFF' = Pau_Cin_Hau
  | c <= '\x11BFF' = No_Block
  | c <= '\x11C6F' = Bhaiksuki
  | c <= '\x11CBF' = Marchen
  | c <= '\x11CFF' = No_Block
  | c <= '\x11D5F' = Masaram_Gondi
  | c <= '\x11DAF' = Gunjala_Gondi
  | c <= '\x11EDF' = No_Block
  | c <= '\x11EFF' = Makasar
  | c <= '\x11FAF' = No_Block
  | c <= '\x11FBF' = Lisu_Supplement
  | c <= '\x11FFF' = Tamil_Supplement
  | c <= '\x123FF' = Cuneiform
  | c <= '\x1247F' = Cuneiform_Numbers_and_Punctuation
  | c <= '\x1254F' = Early_Dynastic_Cuneiform
  | c <= '\x12FFF' = No_Block
  | c <= '\x1342F' = Egyptian_Hieroglyphs
  | c <= '\x1343F' = Egyptian_Hieroglyph_Format_Controls
  | c <= '\x143FF' = No_Block
  | c <= '\x1467F' = Anatolian_Hieroglyphs
  | c <= '\x167FF' = No_Block
  | c <= '\x16A3F' = Bamum_Supplement
  | c <= '\x16A6F' = Mro
  | c <= '\x16ACF' = No_Block
  | c <= '\x16AFF' = Bassa_Vah
  | c <= '\x16B8F' = Pahawh_Hmong
  | c <= '\x16E3F' = No_Block
  | c <= '\x16E9F' = Medefaidrin
  | c <= '\x16EFF' = No_Block
  | c <= '\x16F9F' = Miao
  | c <= '\x16FDF' = No_Block
  | c <= '\x16FFF' = Ideographic_Symbols_and_Punctuation
  | c <= '\x187FF' = Tangut
  | c <= '\x18AFF' = Tangut_Components
  | c <= '\x18CFF' = Khitan_Small_Script
  | c <= '\x18D8F' = Tangut_Supplement
  | c <= '\x1AFFF' = No_Block
  | c <= '\x1B0FF' = Kana_Supplement
  | c <= '\x1B12F' = Kana_Extended_A
  | c <= '\x1B16F' = Small_Kana_Extension
  | c <= '\x1B2FF' = Nushu
  | c <= '\x1BBFF' = No_Block
  | c <= '\x1BC9F' = Duployan
  | c <= '\x1BCAF' = Shorthand_Format_Controls
  | c <= '\x1CFFF' = No_Block
  | c <= '\x1D0FF' = Byzantine_Musical_Symbols
  | c <= '\x1D1FF' = Musical_Symbols
  | c <= '\x1D24F' = Ancient_Greek_Musical_Notation
  | c <= '\x1D2DF' = No_Block
  | c <= '\x1D2FF' = Mayan_Numerals
  | c <= '\x1D35F' = Tai_Xuan_Jing_Symbols
  | c <= '\x1D37F' = Counting_Rod_Numerals
  | c <= '\x1D3FF' = No_Block
  | c <= '\x1D7FF' = Mathematical_Alphanumeric_Symbols
  | c <= '\x1DAAF' = Sutton_SignWriting
  | c <= '\x1DFFF' = No_Block
  | c <= '\x1E02F' = Glagolitic_Supplement
  | c <= '\x1E0FF' = No_Block
  | c <= '\x1E14F' = Nyiakeng_Puachue_Hmong
  | c <= '\x1E2BF' = No_Block
  | c <= '\x1E2FF' = Wancho
  | c <= '\x1E7FF' = No_Block
  | c <= '\x1E8DF' = Mende_Kikakui
  | c <= '\x1E8FF' = No_Block
  | c <= '\x1E95F' = Adlam
  | c <= '\x1EC6F' = No_Block
  | c <= '\x1ECBF' = Indic_Siyaq_Numbers
  | c <= '\x1ECFF' = No_Block
  | c <= '\x1ED4F' = Ottoman_Siyaq_Numbers
  | c <= '\x1EDFF' = No_Block
  | c <= '\x1EEFF' = Arabic_Mathematical_Alphabetic_Symbols
  | c <= '\x1EFFF' = No_Block
  | c <= '\x1F02F' = Mahjong_Tiles
  | c <= '\x1F09F' = Domino_Tiles
  | c <= '\x1F0FF' = Playing_Cards
  | c <= '\x1F1FF' = Enclosed_Alphanumeric_Supplement
  | c <= '\x1F2FF' = Enclosed_Ideographic_Supplement
  | c <= '\x1F5FF' = Miscellaneous_Symbols_and_Pictographs
  | c <= '\x1F64F' = Emoticons
  | c <= '\x1F67F' = Ornamental_Dingbats
  | c <= '\x1F6FF' = Transport_and_Map_Symbols
  | c <= '\x1F77F' = Alchemical_Symbols
  | c <= '\x1F7FF' = Geometric_Shapes_Extended
  | c <= '\x1F8FF' = Supplemental_Arrows_C
  | c <= '\x1F9FF' = Supplemental_Symbols_and_Pictographs
  | c <= '\x1FA6F' = Chess_Symbols
  | c <= '\x1FAFF' = Symbols_and_Pictographs_Extended_A
  | c <= '\x1FBFF' = Symbols_for_Legacy_Computing
  | c <= '\x1FFFF' = No_Block
  | c <= '\x2A6DF' = CJK_Unified_Ideographs_Extension_B
  | c <= '\x2A6FF' = No_Block
  | c <= '\x2B73F' = CJK_Unified_Ideographs_Extension_C
  | c <= '\x2B81F' = CJK_Unified_Ideographs_Extension_D
  | c <= '\x2CEAF' = CJK_Unified_Ideographs_Extension_E
  | c <= '\x2EBEF' = CJK_Unified_Ideographs_Extension_F
  | c <= '\x2F7FF' = No_Block
  | c <= '\x2FA1F' = CJK_Compatibility_Ideographs_Supplement
  | c <= '\x2FFFF' = No_Block
  | c <= '\x3134F' = CJK_Unified_Ideographs_Extension_G
  | c <= '\xDFFFF' = No_Block
  | c <= '\xE007F' = Tags
  | c <= '\xE00FF' = No_Block
  | c <= '\xE01EF' = Variation_Selectors_Supplement
  | c <= '\xEFFFF' = No_Block
  | c <= '\xFFFFF' = Supplementary_Private_Use_Area_A
  | c <= '\x10FFFF' = Supplementary_Private_Use_Area_B
unicodeBlock _ = No_Block

unicodeBlocks :: Map UnicodeBlock UnicodeBlockRange
unicodeBlocks =
  [ (Basic_Latin, UnicodeBlockRange 0x0000 0x007F "Basic Latin")
  , (Latin_1_Supplement, UnicodeBlockRange 0x0080 0x00FF "Latin-1 Supplement")
  , (Latin_Extended_A, UnicodeBlockRange 0x0100 0x017F "Latin Extended-A")
  , (Latin_Extended_B, UnicodeBlockRange 0x0180 0x024F "Latin Extended-B")
  , (IPA_Extensions, UnicodeBlockRange 0x0250 0x02AF "IPA Extensions")
  , (Spacing_Modifier_Letters, UnicodeBlockRange 0x02B0 0x02FF "Spacing Modifier Letters")
  , (Combining_Diacritical_Marks, UnicodeBlockRange 0x0300 0x036F "Combining Diacritical Marks")
  , (Greek_and_Coptic, UnicodeBlockRange 0x0370 0x03FF "Greek and Coptic")
  , (Cyrillic, UnicodeBlockRange 0x0400 0x04FF "Cyrillic")
  , (Cyrillic_Supplement, UnicodeBlockRange 0x0500 0x052F "Cyrillic Supplement")
  , (Armenian, UnicodeBlockRange 0x0530 0x058F "Armenian")
  , (Hebrew, UnicodeBlockRange 0x0590 0x05FF "Hebrew")
  , (Arabic, UnicodeBlockRange 0x0600 0x06FF "Arabic")
  , (Syriac, UnicodeBlockRange 0x0700 0x074F "Syriac")
  , (Arabic_Supplement, UnicodeBlockRange 0x0750 0x077F "Arabic Supplement")
  , (Thaana, UnicodeBlockRange 0x0780 0x07BF "Thaana")
  , (NKo, UnicodeBlockRange 0x07C0 0x07FF "NKo")
  , (Samaritan, UnicodeBlockRange 0x0800 0x083F "Samaritan")
  , (Mandaic, UnicodeBlockRange 0x0840 0x085F "Mandaic")
  , (Syriac_Supplement, UnicodeBlockRange 0x0860 0x086F "Syriac Supplement")
  , (Arabic_Extended_A, UnicodeBlockRange 0x08A0 0x08FF "Arabic Extended-A")
  , (Devanagari, UnicodeBlockRange 0x0900 0x097F "Devanagari")
  , (Bengali, UnicodeBlockRange 0x0980 0x09FF "Bengali")
  , (Gurmukhi, UnicodeBlockRange 0x0A00 0x0A7F "Gurmukhi")
  , (Gujarati, UnicodeBlockRange 0x0A80 0x0AFF "Gujarati")
  , (Oriya, UnicodeBlockRange 0x0B00 0x0B7F "Oriya")
  , (Tamil, UnicodeBlockRange 0x0B80 0x0BFF "Tamil")
  , (Telugu, UnicodeBlockRange 0x0C00 0x0C7F "Telugu")
  , (Kannada, UnicodeBlockRange 0x0C80 0x0CFF "Kannada")
  , (Malayalam, UnicodeBlockRange 0x0D00 0x0D7F "Malayalam")
  , (Sinhala, UnicodeBlockRange 0x0D80 0x0DFF "Sinhala")
  , (Thai, UnicodeBlockRange 0x0E00 0x0E7F "Thai")
  , (Lao, UnicodeBlockRange 0x0E80 0x0EFF "Lao")
  , (Tibetan, UnicodeBlockRange 0x0F00 0x0FFF "Tibetan")
  , (Myanmar, UnicodeBlockRange 0x1000 0x109F "Myanmar")
  , (Georgian, UnicodeBlockRange 0x10A0 0x10FF "Georgian")
  , (Hangul_Jamo, UnicodeBlockRange 0x1100 0x11FF "Hangul Jamo")
  , (Ethiopic, UnicodeBlockRange 0x1200 0x137F "Ethiopic")
  , (Ethiopic_Supplement, UnicodeBlockRange 0x1380 0x139F "Ethiopic Supplement")
  , (Cherokee, UnicodeBlockRange 0x13A0 0x13FF "Cherokee")
  , (Unified_Canadian_Aboriginal_Syllabics, UnicodeBlockRange 0x1400 0x167F "Unified Canadian Aboriginal Syllabics")
  , (Ogham, UnicodeBlockRange 0x1680 0x169F "Ogham")
  , (Runic, UnicodeBlockRange 0x16A0 0x16FF "Runic")
  , (Tagalog, UnicodeBlockRange 0x1700 0x171F "Tagalog")
  , (Hanunoo, UnicodeBlockRange 0x1720 0x173F "Hanunoo")
  , (Buhid, UnicodeBlockRange 0x1740 0x175F "Buhid")
  , (Tagbanwa, UnicodeBlockRange 0x1760 0x177F "Tagbanwa")
  , (Khmer, UnicodeBlockRange 0x1780 0x17FF "Khmer")
  , (Mongolian, UnicodeBlockRange 0x1800 0x18AF "Mongolian")
  , (Unified_Canadian_Aboriginal_Syllabics_Extended, UnicodeBlockRange 0x18B0 0x18FF "Unified Canadian Aboriginal Syllabics Extended")
  , (Limbu, UnicodeBlockRange 0x1900 0x194F "Limbu")
  , (Tai_Le, UnicodeBlockRange 0x1950 0x197F "Tai Le")
  , (New_Tai_Lue, UnicodeBlockRange 0x1980 0x19DF "New Tai Lue")
  , (Khmer_Symbols, UnicodeBlockRange 0x19E0 0x19FF "Khmer Symbols")
  , (Buginese, UnicodeBlockRange 0x1A00 0x1A1F "Buginese")
  , (Tai_Tham, UnicodeBlockRange 0x1A20 0x1AAF "Tai Tham")
  , (Combining_Diacritical_Marks_Extended, UnicodeBlockRange 0x1AB0 0x1AFF "Combining Diacritical Marks Extended")
  , (Balinese, UnicodeBlockRange 0x1B00 0x1B7F "Balinese")
  , (Sundanese, UnicodeBlockRange 0x1B80 0x1BBF "Sundanese")
  , (Batak, UnicodeBlockRange 0x1BC0 0x1BFF "Batak")
  , (Lepcha, UnicodeBlockRange 0x1C00 0x1C4F "Lepcha")
  , (Ol_Chiki, UnicodeBlockRange 0x1C50 0x1C7F "Ol Chiki")
  , (Cyrillic_Extended_C, UnicodeBlockRange 0x1C80 0x1C8F "Cyrillic Extended-C")
  , (Georgian_Extended, UnicodeBlockRange 0x1C90 0x1CBF "Georgian Extended")
  , (Sundanese_Supplement, UnicodeBlockRange 0x1CC0 0x1CCF "Sundanese Supplement")
  , (Vedic_Extensions, UnicodeBlockRange 0x1CD0 0x1CFF "Vedic Extensions")
  , (Phonetic_Extensions, UnicodeBlockRange 0x1D00 0x1D7F "Phonetic Extensions")
  , (Phonetic_Extensions_Supplement, UnicodeBlockRange 0x1D80 0x1DBF "Phonetic Extensions Supplement")
  , (Combining_Diacritical_Marks_Supplement, UnicodeBlockRange 0x1DC0 0x1DFF "Combining Diacritical Marks Supplement")
  , (Latin_Extended_Additional, UnicodeBlockRange 0x1E00 0x1EFF "Latin Extended Additional")
  , (Greek_Extended, UnicodeBlockRange 0x1F00 0x1FFF "Greek Extended")
  , (General_Punctuation, UnicodeBlockRange 0x2000 0x206F "General Punctuation")
  , (Superscripts_and_Subscripts, UnicodeBlockRange 0x2070 0x209F "Superscripts and Subscripts")
  , (Currency_Symbols, UnicodeBlockRange 0x20A0 0x20CF "Currency Symbols")
  , (Combining_Diacritical_Marks_for_Symbols, UnicodeBlockRange 0x20D0 0x20FF "Combining Diacritical Marks for Symbols")
  , (Letterlike_Symbols, UnicodeBlockRange 0x2100 0x214F "Letterlike Symbols")
  , (Number_Forms, UnicodeBlockRange 0x2150 0x218F "Number Forms")
  , (Arrows, UnicodeBlockRange 0x2190 0x21FF "Arrows")
  , (Mathematical_Operators, UnicodeBlockRange 0x2200 0x22FF "Mathematical Operators")
  , (Miscellaneous_Technical, UnicodeBlockRange 0x2300 0x23FF "Miscellaneous Technical")
  , (Control_Pictures, UnicodeBlockRange 0x2400 0x243F "Control Pictures")
  , (Optical_Character_Recognition, UnicodeBlockRange 0x2440 0x245F "Optical Character Recognition")
  , (Enclosed_Alphanumerics, UnicodeBlockRange 0x2460 0x24FF "Enclosed Alphanumerics")
  , (Box_Drawing, UnicodeBlockRange 0x2500 0x257F "Box Drawing")
  , (Block_Elements, UnicodeBlockRange 0x2580 0x259F "Block Elements")
  , (Geometric_Shapes, UnicodeBlockRange 0x25A0 0x25FF "Geometric Shapes")
  , (Miscellaneous_Symbols, UnicodeBlockRange 0x2600 0x26FF "Miscellaneous Symbols")
  , (Dingbats, UnicodeBlockRange 0x2700 0x27BF "Dingbats")
  , (Miscellaneous_Mathematical_Symbols_A, UnicodeBlockRange 0x27C0 0x27EF "Miscellaneous Mathematical Symbols-A")
  , (Supplemental_Arrows_A, UnicodeBlockRange 0x27F0 0x27FF "Supplemental Arrows-A")
  , (Braille_Patterns, UnicodeBlockRange 0x2800 0x28FF "Braille Patterns")
  , (Supplemental_Arrows_B, UnicodeBlockRange 0x2900 0x297F "Supplemental Arrows-B")
  , (Miscellaneous_Mathematical_Symbols_B, UnicodeBlockRange 0x2980 0x29FF "Miscellaneous Mathematical Symbols-B")
  , (Supplemental_Mathematical_Operators, UnicodeBlockRange 0x2A00 0x2AFF "Supplemental Mathematical Operators")
  , (Miscellaneous_Symbols_and_Arrows, UnicodeBlockRange 0x2B00 0x2BFF "Miscellaneous Symbols and Arrows")
  , (Glagolitic, UnicodeBlockRange 0x2C00 0x2C5F "Glagolitic")
  , (Latin_Extended_C, UnicodeBlockRange 0x2C60 0x2C7F "Latin Extended-C")
  , (Coptic, UnicodeBlockRange 0x2C80 0x2CFF "Coptic")
  , (Georgian_Supplement, UnicodeBlockRange 0x2D00 0x2D2F "Georgian Supplement")
  , (Tifinagh, UnicodeBlockRange 0x2D30 0x2D7F "Tifinagh")
  , (Ethiopic_Extended, UnicodeBlockRange 0x2D80 0x2DDF "Ethiopic Extended")
  , (Cyrillic_Extended_A, UnicodeBlockRange 0x2DE0 0x2DFF "Cyrillic Extended-A")
  , (Supplemental_Punctuation, UnicodeBlockRange 0x2E00 0x2E7F "Supplemental Punctuation")
  , (CJK_Radicals_Supplement, UnicodeBlockRange 0x2E80 0x2EFF "CJK Radicals Supplement")
  , (Kangxi_Radicals, UnicodeBlockRange 0x2F00 0x2FDF "Kangxi Radicals")
  , (Ideographic_Description_Characters, UnicodeBlockRange 0x2FF0 0x2FFF "Ideographic Description Characters")
  , (CJK_Symbols_and_Punctuation, UnicodeBlockRange 0x3000 0x303F "CJK Symbols and Punctuation")
  , (Hiragana, UnicodeBlockRange 0x3040 0x309F "Hiragana")
  , (Katakana, UnicodeBlockRange 0x30A0 0x30FF "Katakana")
  , (Bopomofo, UnicodeBlockRange 0x3100 0x312F "Bopomofo")
  , (Hangul_Compatibility_Jamo, UnicodeBlockRange 0x3130 0x318F "Hangul Compatibility Jamo")
  , (Kanbun, UnicodeBlockRange 0x3190 0x319F "Kanbun")
  , (Bopomofo_Extended, UnicodeBlockRange 0x31A0 0x31BF "Bopomofo Extended")
  , (CJK_Strokes, UnicodeBlockRange 0x31C0 0x31EF "CJK Strokes")
  , (Katakana_Phonetic_Extensions, UnicodeBlockRange 0x31F0 0x31FF "Katakana Phonetic Extensions")
  , (Enclosed_CJK_Letters_and_Months, UnicodeBlockRange 0x3200 0x32FF "Enclosed CJK Letters and Months")
  , (CJK_Compatibility, UnicodeBlockRange 0x3300 0x33FF "CJK Compatibility")
  , (CJK_Unified_Ideographs_Extension_A, UnicodeBlockRange 0x3400 0x4DBF "CJK Unified Ideographs Extension A")
  , (Yijing_Hexagram_Symbols, UnicodeBlockRange 0x4DC0 0x4DFF "Yijing Hexagram Symbols")
  , (CJK_Unified_Ideographs, UnicodeBlockRange 0x4E00 0x9FFF "CJK Unified Ideographs")
  , (Yi_Syllables, UnicodeBlockRange 0xA000 0xA48F "Yi Syllables")
  , (Yi_Radicals, UnicodeBlockRange 0xA490 0xA4CF "Yi Radicals")
  , (Lisu, UnicodeBlockRange 0xA4D0 0xA4FF "Lisu")
  , (Vai, UnicodeBlockRange 0xA500 0xA63F "Vai")
  , (Cyrillic_Extended_B, UnicodeBlockRange 0xA640 0xA69F "Cyrillic Extended-B")
  , (Bamum, UnicodeBlockRange 0xA6A0 0xA6FF "Bamum")
  , (Modifier_Tone_Letters, UnicodeBlockRange 0xA700 0xA71F "Modifier Tone Letters")
  , (Latin_Extended_D, UnicodeBlockRange 0xA720 0xA7FF "Latin Extended-D")
  , (Syloti_Nagri, UnicodeBlockRange 0xA800 0xA82F "Syloti Nagri")
  , (Common_Indic_Number_Forms, UnicodeBlockRange 0xA830 0xA83F "Common Indic Number Forms")
  , (Phags_pa, UnicodeBlockRange 0xA840 0xA87F "Phags-pa")
  , (Saurashtra, UnicodeBlockRange 0xA880 0xA8DF "Saurashtra")
  , (Devanagari_Extended, UnicodeBlockRange 0xA8E0 0xA8FF "Devanagari Extended")
  , (Kayah_Li, UnicodeBlockRange 0xA900 0xA92F "Kayah Li")
  , (Rejang, UnicodeBlockRange 0xA930 0xA95F "Rejang")
  , (Hangul_Jamo_Extended_A, UnicodeBlockRange 0xA960 0xA97F "Hangul Jamo Extended-A")
  , (Javanese, UnicodeBlockRange 0xA980 0xA9DF "Javanese")
  , (Myanmar_Extended_B, UnicodeBlockRange 0xA9E0 0xA9FF "Myanmar Extended-B")
  , (Cham, UnicodeBlockRange 0xAA00 0xAA5F "Cham")
  , (Myanmar_Extended_A, UnicodeBlockRange 0xAA60 0xAA7F "Myanmar Extended-A")
  , (Tai_Viet, UnicodeBlockRange 0xAA80 0xAADF "Tai Viet")
  , (Meetei_Mayek_Extensions, UnicodeBlockRange 0xAAE0 0xAAFF "Meetei Mayek Extensions")
  , (Ethiopic_Extended_A, UnicodeBlockRange 0xAB00 0xAB2F "Ethiopic Extended-A")
  , (Latin_Extended_E, UnicodeBlockRange 0xAB30 0xAB6F "Latin Extended-E")
  , (Cherokee_Supplement, UnicodeBlockRange 0xAB70 0xABBF "Cherokee Supplement")
  , (Meetei_Mayek, UnicodeBlockRange 0xABC0 0xABFF "Meetei Mayek")
  , (Hangul_Syllables, UnicodeBlockRange 0xAC00 0xD7AF "Hangul Syllables")
  , (Hangul_Jamo_Extended_B, UnicodeBlockRange 0xD7B0 0xD7FF "Hangul Jamo Extended-B")
  , (High_Surrogates, UnicodeBlockRange 0xD800 0xDB7F "High Surrogates")
  , (High_Private_Use_Surrogates, UnicodeBlockRange 0xDB80 0xDBFF "High Private Use Surrogates")
  , (Low_Surrogates, UnicodeBlockRange 0xDC00 0xDFFF "Low Surrogates")
  , (Private_Use_Area, UnicodeBlockRange 0xE000 0xF8FF "Private Use Area")
  , (CJK_Compatibility_Ideographs, UnicodeBlockRange 0xF900 0xFAFF "CJK Compatibility Ideographs")
  , (Alphabetic_Presentation_Forms, UnicodeBlockRange 0xFB00 0xFB4F "Alphabetic Presentation Forms")
  , (Arabic_Presentation_Forms_A, UnicodeBlockRange 0xFB50 0xFDFF "Arabic Presentation Forms-A")
  , (Variation_Selectors, UnicodeBlockRange 0xFE00 0xFE0F "Variation Selectors")
  , (Vertical_Forms, UnicodeBlockRange 0xFE10 0xFE1F "Vertical Forms")
  , (Combining_Half_Marks, UnicodeBlockRange 0xFE20 0xFE2F "Combining Half Marks")
  , (CJK_Compatibility_Forms, UnicodeBlockRange 0xFE30 0xFE4F "CJK Compatibility Forms")
  , (Small_Form_Variants, UnicodeBlockRange 0xFE50 0xFE6F "Small Form Variants")
  , (Arabic_Presentation_Forms_B, UnicodeBlockRange 0xFE70 0xFEFF "Arabic Presentation Forms-B")
  , (Halfwidth_and_Fullwidth_Forms, UnicodeBlockRange 0xFF00 0xFFEF "Halfwidth and Fullwidth Forms")
  , (Specials, UnicodeBlockRange 0xFFF0 0xFFFF "Specials")
  , (Linear_B_Syllabary, UnicodeBlockRange 0x10000 0x1007F "Linear B Syllabary")
  , (Linear_B_Ideograms, UnicodeBlockRange 0x10080 0x100FF "Linear B Ideograms")
  , (Aegean_Numbers, UnicodeBlockRange 0x10100 0x1013F "Aegean Numbers")
  , (Ancient_Greek_Numbers, UnicodeBlockRange 0x10140 0x1018F "Ancient Greek Numbers")
  , (Ancient_Symbols, UnicodeBlockRange 0x10190 0x101CF "Ancient Symbols")
  , (Phaistos_Disc, UnicodeBlockRange 0x101D0 0x101FF "Phaistos Disc")
  , (Lycian, UnicodeBlockRange 0x10280 0x1029F "Lycian")
  , (Carian, UnicodeBlockRange 0x102A0 0x102DF "Carian")
  , (Coptic_Epact_Numbers, UnicodeBlockRange 0x102E0 0x102FF "Coptic Epact Numbers")
  , (Old_Italic, UnicodeBlockRange 0x10300 0x1032F "Old Italic")
  , (Gothic, UnicodeBlockRange 0x10330 0x1034F "Gothic")
  , (Old_Permic, UnicodeBlockRange 0x10350 0x1037F "Old Permic")
  , (Ugaritic, UnicodeBlockRange 0x10380 0x1039F "Ugaritic")
  , (Old_Persian, UnicodeBlockRange 0x103A0 0x103DF "Old Persian")
  , (Deseret, UnicodeBlockRange 0x10400 0x1044F "Deseret")
  , (Shavian, UnicodeBlockRange 0x10450 0x1047F "Shavian")
  , (Osmanya, UnicodeBlockRange 0x10480 0x104AF "Osmanya")
  , (Osage, UnicodeBlockRange 0x104B0 0x104FF "Osage")
  , (Elbasan, UnicodeBlockRange 0x10500 0x1052F "Elbasan")
  , (Caucasian_Albanian, UnicodeBlockRange 0x10530 0x1056F "Caucasian Albanian")
  , (Linear_A, UnicodeBlockRange 0x10600 0x1077F "Linear A")
  , (Cypriot_Syllabary, UnicodeBlockRange 0x10800 0x1083F "Cypriot Syllabary")
  , (Imperial_Aramaic, UnicodeBlockRange 0x10840 0x1085F "Imperial Aramaic")
  , (Palmyrene, UnicodeBlockRange 0x10860 0x1087F "Palmyrene")
  , (Nabataean, UnicodeBlockRange 0x10880 0x108AF "Nabataean")
  , (Hatran, UnicodeBlockRange 0x108E0 0x108FF "Hatran")
  , (Phoenician, UnicodeBlockRange 0x10900 0x1091F "Phoenician")
  , (Lydian, UnicodeBlockRange 0x10920 0x1093F "Lydian")
  , (Meroitic_Hieroglyphs, UnicodeBlockRange 0x10980 0x1099F "Meroitic Hieroglyphs")
  , (Meroitic_Cursive, UnicodeBlockRange 0x109A0 0x109FF "Meroitic Cursive")
  , (Kharoshthi, UnicodeBlockRange 0x10A00 0x10A5F "Kharoshthi")
  , (Old_South_Arabian, UnicodeBlockRange 0x10A60 0x10A7F "Old South Arabian")
  , (Old_North_Arabian, UnicodeBlockRange 0x10A80 0x10A9F "Old North Arabian")
  , (Manichaean, UnicodeBlockRange 0x10AC0 0x10AFF "Manichaean")
  , (Avestan, UnicodeBlockRange 0x10B00 0x10B3F "Avestan")
  , (Inscriptional_Parthian, UnicodeBlockRange 0x10B40 0x10B5F "Inscriptional Parthian")
  , (Inscriptional_Pahlavi, UnicodeBlockRange 0x10B60 0x10B7F "Inscriptional Pahlavi")
  , (Psalter_Pahlavi, UnicodeBlockRange 0x10B80 0x10BAF "Psalter Pahlavi")
  , (Old_Turkic, UnicodeBlockRange 0x10C00 0x10C4F "Old Turkic")
  , (Old_Hungarian, UnicodeBlockRange 0x10C80 0x10CFF "Old Hungarian")
  , (Hanifi_Rohingya, UnicodeBlockRange 0x10D00 0x10D3F "Hanifi Rohingya")
  , (Rumi_Numeral_Symbols, UnicodeBlockRange 0x10E60 0x10E7F "Rumi Numeral Symbols")
  , (Yezidi, UnicodeBlockRange 0x10E80 0x10EBF "Yezidi")
  , (Old_Sogdian, UnicodeBlockRange 0x10F00 0x10F2F "Old Sogdian")
  , (Sogdian, UnicodeBlockRange 0x10F30 0x10F6F "Sogdian")
  , (Chorasmian, UnicodeBlockRange 0x10FB0 0x10FDF "Chorasmian")
  , (Elymaic, UnicodeBlockRange 0x10FE0 0x10FFF "Elymaic")
  , (Brahmi, UnicodeBlockRange 0x11000 0x1107F "Brahmi")
  , (Kaithi, UnicodeBlockRange 0x11080 0x110CF "Kaithi")
  , (Sora_Sompeng, UnicodeBlockRange 0x110D0 0x110FF "Sora Sompeng")
  , (Chakma, UnicodeBlockRange 0x11100 0x1114F "Chakma")
  , (Mahajani, UnicodeBlockRange 0x11150 0x1117F "Mahajani")
  , (Sharada, UnicodeBlockRange 0x11180 0x111DF "Sharada")
  , (Sinhala_Archaic_Numbers, UnicodeBlockRange 0x111E0 0x111FF "Sinhala Archaic Numbers")
  , (Khojki, UnicodeBlockRange 0x11200 0x1124F "Khojki")
  , (Multani, UnicodeBlockRange 0x11280 0x112AF "Multani")
  , (Khudawadi, UnicodeBlockRange 0x112B0 0x112FF "Khudawadi")
  , (Grantha, UnicodeBlockRange 0x11300 0x1137F "Grantha")
  , (Newa, UnicodeBlockRange 0x11400 0x1147F "Newa")
  , (Tirhuta, UnicodeBlockRange 0x11480 0x114DF "Tirhuta")
  , (Siddham, UnicodeBlockRange 0x11580 0x115FF "Siddham")
  , (Modi, UnicodeBlockRange 0x11600 0x1165F "Modi")
  , (Mongolian_Supplement, UnicodeBlockRange 0x11660 0x1167F "Mongolian Supplement")
  , (Takri, UnicodeBlockRange 0x11680 0x116CF "Takri")
  , (Ahom, UnicodeBlockRange 0x11700 0x1173F "Ahom")
  , (Dogra, UnicodeBlockRange 0x11800 0x1184F "Dogra")
  , (Warang_Citi, UnicodeBlockRange 0x118A0 0x118FF "Warang Citi")
  , (Dives_Akuru, UnicodeBlockRange 0x11900 0x1195F "Dives Akuru")
  , (Nandinagari, UnicodeBlockRange 0x119A0 0x119FF "Nandinagari")
  , (Zanabazar_Square, UnicodeBlockRange 0x11A00 0x11A4F "Zanabazar Square")
  , (Soyombo, UnicodeBlockRange 0x11A50 0x11AAF "Soyombo")
  , (Pau_Cin_Hau, UnicodeBlockRange 0x11AC0 0x11AFF "Pau Cin Hau")
  , (Bhaiksuki, UnicodeBlockRange 0x11C00 0x11C6F "Bhaiksuki")
  , (Marchen, UnicodeBlockRange 0x11C70 0x11CBF "Marchen")
  , (Masaram_Gondi, UnicodeBlockRange 0x11D00 0x11D5F "Masaram Gondi")
  , (Gunjala_Gondi, UnicodeBlockRange 0x11D60 0x11DAF "Gunjala Gondi")
  , (Makasar, UnicodeBlockRange 0x11EE0 0x11EFF "Makasar")
  , (Lisu_Supplement, UnicodeBlockRange 0x11FB0 0x11FBF "Lisu Supplement")
  , (Tamil_Supplement, UnicodeBlockRange 0x11FC0 0x11FFF "Tamil Supplement")
  , (Cuneiform, UnicodeBlockRange 0x12000 0x123FF "Cuneiform")
  , (Cuneiform_Numbers_and_Punctuation, UnicodeBlockRange 0x12400 0x1247F "Cuneiform Numbers and Punctuation")
  , (Early_Dynastic_Cuneiform, UnicodeBlockRange 0x12480 0x1254F "Early Dynastic Cuneiform")
  , (Egyptian_Hieroglyphs, UnicodeBlockRange 0x13000 0x1342F "Egyptian Hieroglyphs")
  , (Egyptian_Hieroglyph_Format_Controls, UnicodeBlockRange 0x13430 0x1343F "Egyptian Hieroglyph Format Controls")
  , (Anatolian_Hieroglyphs, UnicodeBlockRange 0x14400 0x1467F "Anatolian Hieroglyphs")
  , (Bamum_Supplement, UnicodeBlockRange 0x16800 0x16A3F "Bamum Supplement")
  , (Mro, UnicodeBlockRange 0x16A40 0x16A6F "Mro")
  , (Bassa_Vah, UnicodeBlockRange 0x16AD0 0x16AFF "Bassa Vah")
  , (Pahawh_Hmong, UnicodeBlockRange 0x16B00 0x16B8F "Pahawh Hmong")
  , (Medefaidrin, UnicodeBlockRange 0x16E40 0x16E9F "Medefaidrin")
  , (Miao, UnicodeBlockRange 0x16F00 0x16F9F "Miao")
  , (Ideographic_Symbols_and_Punctuation, UnicodeBlockRange 0x16FE0 0x16FFF "Ideographic Symbols and Punctuation")
  , (Tangut, UnicodeBlockRange 0x17000 0x187FF "Tangut")
  , (Tangut_Components, UnicodeBlockRange 0x18800 0x18AFF "Tangut Components")
  , (Khitan_Small_Script, UnicodeBlockRange 0x18B00 0x18CFF "Khitan Small Script")
  , (Tangut_Supplement, UnicodeBlockRange 0x18D00 0x18D8F "Tangut Supplement")
  , (Kana_Supplement, UnicodeBlockRange 0x1B000 0x1B0FF "Kana Supplement")
  , (Kana_Extended_A, UnicodeBlockRange 0x1B100 0x1B12F "Kana Extended-A")
  , (Small_Kana_Extension, UnicodeBlockRange 0x1B130 0x1B16F "Small Kana Extension")
  , (Nushu, UnicodeBlockRange 0x1B170 0x1B2FF "Nushu")
  , (Duployan, UnicodeBlockRange 0x1BC00 0x1BC9F "Duployan")
  , (Shorthand_Format_Controls, UnicodeBlockRange 0x1BCA0 0x1BCAF "Shorthand Format Controls")
  , (Byzantine_Musical_Symbols, UnicodeBlockRange 0x1D000 0x1D0FF "Byzantine Musical Symbols")
  , (Musical_Symbols, UnicodeBlockRange 0x1D100 0x1D1FF "Musical Symbols")
  , (Ancient_Greek_Musical_Notation, UnicodeBlockRange 0x1D200 0x1D24F "Ancient Greek Musical Notation")
  , (Mayan_Numerals, UnicodeBlockRange 0x1D2E0 0x1D2FF "Mayan Numerals")
  , (Tai_Xuan_Jing_Symbols, UnicodeBlockRange 0x1D300 0x1D35F "Tai Xuan Jing Symbols")
  , (Counting_Rod_Numerals, UnicodeBlockRange 0x1D360 0x1D37F "Counting Rod Numerals")
  , (Mathematical_Alphanumeric_Symbols, UnicodeBlockRange 0x1D400 0x1D7FF "Mathematical Alphanumeric Symbols")
  , (Sutton_SignWriting, UnicodeBlockRange 0x1D800 0x1DAAF "Sutton SignWriting")
  , (Glagolitic_Supplement, UnicodeBlockRange 0x1E000 0x1E02F "Glagolitic Supplement")
  , (Nyiakeng_Puachue_Hmong, UnicodeBlockRange 0x1E100 0x1E14F "Nyiakeng Puachue Hmong")
  , (Wancho, UnicodeBlockRange 0x1E2C0 0x1E2FF "Wancho")
  , (Mende_Kikakui, UnicodeBlockRange 0x1E800 0x1E8DF "Mende Kikakui")
  , (Adlam, UnicodeBlockRange 0x1E900 0x1E95F "Adlam")
  , (Indic_Siyaq_Numbers, UnicodeBlockRange 0x1EC70 0x1ECBF "Indic Siyaq Numbers")
  , (Ottoman_Siyaq_Numbers, UnicodeBlockRange 0x1ED00 0x1ED4F "Ottoman Siyaq Numbers")
  , (Arabic_Mathematical_Alphabetic_Symbols, UnicodeBlockRange 0x1EE00 0x1EEFF "Arabic Mathematical Alphabetic Symbols")
  , (Mahjong_Tiles, UnicodeBlockRange 0x1F000 0x1F02F "Mahjong Tiles")
  , (Domino_Tiles, UnicodeBlockRange 0x1F030 0x1F09F "Domino Tiles")
  , (Playing_Cards, UnicodeBlockRange 0x1F0A0 0x1F0FF "Playing Cards")
  , (Enclosed_Alphanumeric_Supplement, UnicodeBlockRange 0x1F100 0x1F1FF "Enclosed Alphanumeric Supplement")
  , (Enclosed_Ideographic_Supplement, UnicodeBlockRange 0x1F200 0x1F2FF "Enclosed Ideographic Supplement")
  , (Miscellaneous_Symbols_and_Pictographs, UnicodeBlockRange 0x1F300 0x1F5FF "Miscellaneous Symbols and Pictographs")
  , (Emoticons, UnicodeBlockRange 0x1F600 0x1F64F "Emoticons")
  , (Ornamental_Dingbats, UnicodeBlockRange 0x1F650 0x1F67F "Ornamental Dingbats")
  , (Transport_and_Map_Symbols, UnicodeBlockRange 0x1F680 0x1F6FF "Transport and Map Symbols")
  , (Alchemical_Symbols, UnicodeBlockRange 0x1F700 0x1F77F "Alchemical Symbols")
  , (Geometric_Shapes_Extended, UnicodeBlockRange 0x1F780 0x1F7FF "Geometric Shapes Extended")
  , (Supplemental_Arrows_C, UnicodeBlockRange 0x1F800 0x1F8FF "Supplemental Arrows-C")
  , (Supplemental_Symbols_and_Pictographs, UnicodeBlockRange 0x1F900 0x1F9FF "Supplemental Symbols and Pictographs")
  , (Chess_Symbols, UnicodeBlockRange 0x1FA00 0x1FA6F "Chess Symbols")
  , (Symbols_and_Pictographs_Extended_A, UnicodeBlockRange 0x1FA70 0x1FAFF "Symbols and Pictographs Extended-A")
  , (Symbols_for_Legacy_Computing, UnicodeBlockRange 0x1FB00 0x1FBFF "Symbols for Legacy Computing")
  , (CJK_Unified_Ideographs_Extension_B, UnicodeBlockRange 0x20000 0x2A6DF "CJK Unified Ideographs Extension B")
  , (CJK_Unified_Ideographs_Extension_C, UnicodeBlockRange 0x2A700 0x2B73F "CJK Unified Ideographs Extension C")
  , (CJK_Unified_Ideographs_Extension_D, UnicodeBlockRange 0x2B740 0x2B81F "CJK Unified Ideographs Extension D")
  , (CJK_Unified_Ideographs_Extension_E, UnicodeBlockRange 0x2B820 0x2CEAF "CJK Unified Ideographs Extension E")
  , (CJK_Unified_Ideographs_Extension_F, UnicodeBlockRange 0x2CEB0 0x2EBEF "CJK Unified Ideographs Extension F")
  , (CJK_Compatibility_Ideographs_Supplement, UnicodeBlockRange 0x2F800 0x2FA1F "CJK Compatibility Ideographs Supplement")
  , (CJK_Unified_Ideographs_Extension_G, UnicodeBlockRange 0x30000 0x3134F "CJK Unified Ideographs Extension G")
  , (Tags, UnicodeBlockRange 0xE0000 0xE007F "Tags")
  , (Variation_Selectors_Supplement, UnicodeBlockRange 0xE0100 0xE01EF "Variation Selectors Supplement")
  , (Supplementary_Private_Use_Area_A, UnicodeBlockRange 0xF0000 0xFFFFF "Supplementary Private Use Area-A")
  , (Supplementary_Private_Use_Area_B, UnicodeBlockRange 0x100000 0x10FFFF "Supplementary Private Use Area-B") ]

unicodeBlockRange :: Char -> UnicodeBlockRange
unicodeBlockRange c
  | c <= '\x007F' = UnicodeBlockRange 0x0000 0x007F "Basic Latin"
  | c <= '\x00FF' = UnicodeBlockRange 0x0080 0x00FF "Latin-1 Supplement"
  | c <= '\x017F' = UnicodeBlockRange 0x0100 0x017F "Latin Extended-A"
  | c <= '\x024F' = UnicodeBlockRange 0x0180 0x024F "Latin Extended-B"
  | c <= '\x02AF' = UnicodeBlockRange 0x0250 0x02AF "IPA Extensions"
  | c <= '\x02FF' = UnicodeBlockRange 0x02B0 0x02FF "Spacing Modifier Letters"
  | c <= '\x036F' = UnicodeBlockRange 0x0300 0x036F "Combining Diacritical Marks"
  | c <= '\x03FF' = UnicodeBlockRange 0x0370 0x03FF "Greek and Coptic"
  | c <= '\x04FF' = UnicodeBlockRange 0x0400 0x04FF "Cyrillic"
  | c <= '\x052F' = UnicodeBlockRange 0x0500 0x052F "Cyrillic Supplement"
  | c <= '\x058F' = UnicodeBlockRange 0x0530 0x058F "Armenian"
  | c <= '\x05FF' = UnicodeBlockRange 0x0590 0x05FF "Hebrew"
  | c <= '\x06FF' = UnicodeBlockRange 0x0600 0x06FF "Arabic"
  | c <= '\x074F' = UnicodeBlockRange 0x0700 0x074F "Syriac"
  | c <= '\x077F' = UnicodeBlockRange 0x0750 0x077F "Arabic Supplement"
  | c <= '\x07BF' = UnicodeBlockRange 0x0780 0x07BF "Thaana"
  | c <= '\x07FF' = UnicodeBlockRange 0x07C0 0x07FF "NKo"
  | c <= '\x083F' = UnicodeBlockRange 0x0800 0x083F "Samaritan"
  | c <= '\x085F' = UnicodeBlockRange 0x0840 0x085F "Mandaic"
  | c <= '\x086F' = UnicodeBlockRange 0x0860 0x086F "Syriac Supplement"
  | c <= '\x089F' = UnicodeBlockRange 0x0870 0x089F "None"
  | c <= '\x08FF' = UnicodeBlockRange 0x08A0 0x08FF "Arabic Extended-A"
  | c <= '\x097F' = UnicodeBlockRange 0x0900 0x097F "Devanagari"
  | c <= '\x09FF' = UnicodeBlockRange 0x0980 0x09FF "Bengali"
  | c <= '\x0A7F' = UnicodeBlockRange 0x0A00 0x0A7F "Gurmukhi"
  | c <= '\x0AFF' = UnicodeBlockRange 0x0A80 0x0AFF "Gujarati"
  | c <= '\x0B7F' = UnicodeBlockRange 0x0B00 0x0B7F "Oriya"
  | c <= '\x0BFF' = UnicodeBlockRange 0x0B80 0x0BFF "Tamil"
  | c <= '\x0C7F' = UnicodeBlockRange 0x0C00 0x0C7F "Telugu"
  | c <= '\x0CFF' = UnicodeBlockRange 0x0C80 0x0CFF "Kannada"
  | c <= '\x0D7F' = UnicodeBlockRange 0x0D00 0x0D7F "Malayalam"
  | c <= '\x0DFF' = UnicodeBlockRange 0x0D80 0x0DFF "Sinhala"
  | c <= '\x0E7F' = UnicodeBlockRange 0x0E00 0x0E7F "Thai"
  | c <= '\x0EFF' = UnicodeBlockRange 0x0E80 0x0EFF "Lao"
  | c <= '\x0FFF' = UnicodeBlockRange 0x0F00 0x0FFF "Tibetan"
  | c <= '\x109F' = UnicodeBlockRange 0x1000 0x109F "Myanmar"
  | c <= '\x10FF' = UnicodeBlockRange 0x10A0 0x10FF "Georgian"
  | c <= '\x11FF' = UnicodeBlockRange 0x1100 0x11FF "Hangul Jamo"
  | c <= '\x137F' = UnicodeBlockRange 0x1200 0x137F "Ethiopic"
  | c <= '\x139F' = UnicodeBlockRange 0x1380 0x139F "Ethiopic Supplement"
  | c <= '\x13FF' = UnicodeBlockRange 0x13A0 0x13FF "Cherokee"
  | c <= '\x167F' = UnicodeBlockRange 0x1400 0x167F "Unified Canadian Aboriginal Syllabics"
  | c <= '\x169F' = UnicodeBlockRange 0x1680 0x169F "Ogham"
  | c <= '\x16FF' = UnicodeBlockRange 0x16A0 0x16FF "Runic"
  | c <= '\x171F' = UnicodeBlockRange 0x1700 0x171F "Tagalog"
  | c <= '\x173F' = UnicodeBlockRange 0x1720 0x173F "Hanunoo"
  | c <= '\x175F' = UnicodeBlockRange 0x1740 0x175F "Buhid"
  | c <= '\x177F' = UnicodeBlockRange 0x1760 0x177F "Tagbanwa"
  | c <= '\x17FF' = UnicodeBlockRange 0x1780 0x17FF "Khmer"
  | c <= '\x18AF' = UnicodeBlockRange 0x1800 0x18AF "Mongolian"
  | c <= '\x18FF' = UnicodeBlockRange 0x18B0 0x18FF "Unified Canadian Aboriginal Syllabics Extended"
  | c <= '\x194F' = UnicodeBlockRange 0x1900 0x194F "Limbu"
  | c <= '\x197F' = UnicodeBlockRange 0x1950 0x197F "Tai Le"
  | c <= '\x19DF' = UnicodeBlockRange 0x1980 0x19DF "New Tai Lue"
  | c <= '\x19FF' = UnicodeBlockRange 0x19E0 0x19FF "Khmer Symbols"
  | c <= '\x1A1F' = UnicodeBlockRange 0x1A00 0x1A1F "Buginese"
  | c <= '\x1AAF' = UnicodeBlockRange 0x1A20 0x1AAF "Tai Tham"
  | c <= '\x1AFF' = UnicodeBlockRange 0x1AB0 0x1AFF "Combining Diacritical Marks Extended"
  | c <= '\x1B7F' = UnicodeBlockRange 0x1B00 0x1B7F "Balinese"
  | c <= '\x1BBF' = UnicodeBlockRange 0x1B80 0x1BBF "Sundanese"
  | c <= '\x1BFF' = UnicodeBlockRange 0x1BC0 0x1BFF "Batak"
  | c <= '\x1C4F' = UnicodeBlockRange 0x1C00 0x1C4F "Lepcha"
  | c <= '\x1C7F' = UnicodeBlockRange 0x1C50 0x1C7F "Ol Chiki"
  | c <= '\x1C8F' = UnicodeBlockRange 0x1C80 0x1C8F "Cyrillic Extended-C"
  | c <= '\x1CBF' = UnicodeBlockRange 0x1C90 0x1CBF "Georgian Extended"
  | c <= '\x1CCF' = UnicodeBlockRange 0x1CC0 0x1CCF "Sundanese Supplement"
  | c <= '\x1CFF' = UnicodeBlockRange 0x1CD0 0x1CFF "Vedic Extensions"
  | c <= '\x1D7F' = UnicodeBlockRange 0x1D00 0x1D7F "Phonetic Extensions"
  | c <= '\x1DBF' = UnicodeBlockRange 0x1D80 0x1DBF "Phonetic Extensions Supplement"
  | c <= '\x1DFF' = UnicodeBlockRange 0x1DC0 0x1DFF "Combining Diacritical Marks Supplement"
  | c <= '\x1EFF' = UnicodeBlockRange 0x1E00 0x1EFF "Latin Extended Additional"
  | c <= '\x1FFF' = UnicodeBlockRange 0x1F00 0x1FFF "Greek Extended"
  | c <= '\x206F' = UnicodeBlockRange 0x2000 0x206F "General Punctuation"
  | c <= '\x209F' = UnicodeBlockRange 0x2070 0x209F "Superscripts and Subscripts"
  | c <= '\x20CF' = UnicodeBlockRange 0x20A0 0x20CF "Currency Symbols"
  | c <= '\x20FF' = UnicodeBlockRange 0x20D0 0x20FF "Combining Diacritical Marks for Symbols"
  | c <= '\x214F' = UnicodeBlockRange 0x2100 0x214F "Letterlike Symbols"
  | c <= '\x218F' = UnicodeBlockRange 0x2150 0x218F "Number Forms"
  | c <= '\x21FF' = UnicodeBlockRange 0x2190 0x21FF "Arrows"
  | c <= '\x22FF' = UnicodeBlockRange 0x2200 0x22FF "Mathematical Operators"
  | c <= '\x23FF' = UnicodeBlockRange 0x2300 0x23FF "Miscellaneous Technical"
  | c <= '\x243F' = UnicodeBlockRange 0x2400 0x243F "Control Pictures"
  | c <= '\x245F' = UnicodeBlockRange 0x2440 0x245F "Optical Character Recognition"
  | c <= '\x24FF' = UnicodeBlockRange 0x2460 0x24FF "Enclosed Alphanumerics"
  | c <= '\x257F' = UnicodeBlockRange 0x2500 0x257F "Box Drawing"
  | c <= '\x259F' = UnicodeBlockRange 0x2580 0x259F "Block Elements"
  | c <= '\x25FF' = UnicodeBlockRange 0x25A0 0x25FF "Geometric Shapes"
  | c <= '\x26FF' = UnicodeBlockRange 0x2600 0x26FF "Miscellaneous Symbols"
  | c <= '\x27BF' = UnicodeBlockRange 0x2700 0x27BF "Dingbats"
  | c <= '\x27EF' = UnicodeBlockRange 0x27C0 0x27EF "Miscellaneous Mathematical Symbols-A"
  | c <= '\x27FF' = UnicodeBlockRange 0x27F0 0x27FF "Supplemental Arrows-A"
  | c <= '\x28FF' = UnicodeBlockRange 0x2800 0x28FF "Braille Patterns"
  | c <= '\x297F' = UnicodeBlockRange 0x2900 0x297F "Supplemental Arrows-B"
  | c <= '\x29FF' = UnicodeBlockRange 0x2980 0x29FF "Miscellaneous Mathematical Symbols-B"
  | c <= '\x2AFF' = UnicodeBlockRange 0x2A00 0x2AFF "Supplemental Mathematical Operators"
  | c <= '\x2BFF' = UnicodeBlockRange 0x2B00 0x2BFF "Miscellaneous Symbols and Arrows"
  | c <= '\x2C5F' = UnicodeBlockRange 0x2C00 0x2C5F "Glagolitic"
  | c <= '\x2C7F' = UnicodeBlockRange 0x2C60 0x2C7F "Latin Extended-C"
  | c <= '\x2CFF' = UnicodeBlockRange 0x2C80 0x2CFF "Coptic"
  | c <= '\x2D2F' = UnicodeBlockRange 0x2D00 0x2D2F "Georgian Supplement"
  | c <= '\x2D7F' = UnicodeBlockRange 0x2D30 0x2D7F "Tifinagh"
  | c <= '\x2DDF' = UnicodeBlockRange 0x2D80 0x2DDF "Ethiopic Extended"
  | c <= '\x2DFF' = UnicodeBlockRange 0x2DE0 0x2DFF "Cyrillic Extended-A"
  | c <= '\x2E7F' = UnicodeBlockRange 0x2E00 0x2E7F "Supplemental Punctuation"
  | c <= '\x2EFF' = UnicodeBlockRange 0x2E80 0x2EFF "CJK Radicals Supplement"
  | c <= '\x2FDF' = UnicodeBlockRange 0x2F00 0x2FDF "Kangxi Radicals"
  | c <= '\x2FEF' = UnicodeBlockRange 0x2FE0 0x2FEF "None"
  | c <= '\x2FFF' = UnicodeBlockRange 0x2FF0 0x2FFF "Ideographic Description Characters"
  | c <= '\x303F' = UnicodeBlockRange 0x3000 0x303F "CJK Symbols and Punctuation"
  | c <= '\x309F' = UnicodeBlockRange 0x3040 0x309F "Hiragana"
  | c <= '\x30FF' = UnicodeBlockRange 0x30A0 0x30FF "Katakana"
  | c <= '\x312F' = UnicodeBlockRange 0x3100 0x312F "Bopomofo"
  | c <= '\x318F' = UnicodeBlockRange 0x3130 0x318F "Hangul Compatibility Jamo"
  | c <= '\x319F' = UnicodeBlockRange 0x3190 0x319F "Kanbun"
  | c <= '\x31BF' = UnicodeBlockRange 0x31A0 0x31BF "Bopomofo Extended"
  | c <= '\x31EF' = UnicodeBlockRange 0x31C0 0x31EF "CJK Strokes"
  | c <= '\x31FF' = UnicodeBlockRange 0x31F0 0x31FF "Katakana Phonetic Extensions"
  | c <= '\x32FF' = UnicodeBlockRange 0x3200 0x32FF "Enclosed CJK Letters and Months"
  | c <= '\x33FF' = UnicodeBlockRange 0x3300 0x33FF "CJK Compatibility"
  | c <= '\x4DBF' = UnicodeBlockRange 0x3400 0x4DBF "CJK Unified Ideographs Extension A"
  | c <= '\x4DFF' = UnicodeBlockRange 0x4DC0 0x4DFF "Yijing Hexagram Symbols"
  | c <= '\x9FFF' = UnicodeBlockRange 0x4E00 0x9FFF "CJK Unified Ideographs"
  | c <= '\xA48F' = UnicodeBlockRange 0xA000 0xA48F "Yi Syllables"
  | c <= '\xA4CF' = UnicodeBlockRange 0xA490 0xA4CF "Yi Radicals"
  | c <= '\xA4FF' = UnicodeBlockRange 0xA4D0 0xA4FF "Lisu"
  | c <= '\xA63F' = UnicodeBlockRange 0xA500 0xA63F "Vai"
  | c <= '\xA69F' = UnicodeBlockRange 0xA640 0xA69F "Cyrillic Extended-B"
  | c <= '\xA6FF' = UnicodeBlockRange 0xA6A0 0xA6FF "Bamum"
  | c <= '\xA71F' = UnicodeBlockRange 0xA700 0xA71F "Modifier Tone Letters"
  | c <= '\xA7FF' = UnicodeBlockRange 0xA720 0xA7FF "Latin Extended-D"
  | c <= '\xA82F' = UnicodeBlockRange 0xA800 0xA82F "Syloti Nagri"
  | c <= '\xA83F' = UnicodeBlockRange 0xA830 0xA83F "Common Indic Number Forms"
  | c <= '\xA87F' = UnicodeBlockRange 0xA840 0xA87F "Phags-pa"
  | c <= '\xA8DF' = UnicodeBlockRange 0xA880 0xA8DF "Saurashtra"
  | c <= '\xA8FF' = UnicodeBlockRange 0xA8E0 0xA8FF "Devanagari Extended"
  | c <= '\xA92F' = UnicodeBlockRange 0xA900 0xA92F "Kayah Li"
  | c <= '\xA95F' = UnicodeBlockRange 0xA930 0xA95F "Rejang"
  | c <= '\xA97F' = UnicodeBlockRange 0xA960 0xA97F "Hangul Jamo Extended-A"
  | c <= '\xA9DF' = UnicodeBlockRange 0xA980 0xA9DF "Javanese"
  | c <= '\xA9FF' = UnicodeBlockRange 0xA9E0 0xA9FF "Myanmar Extended-B"
  | c <= '\xAA5F' = UnicodeBlockRange 0xAA00 0xAA5F "Cham"
  | c <= '\xAA7F' = UnicodeBlockRange 0xAA60 0xAA7F "Myanmar Extended-A"
  | c <= '\xAADF' = UnicodeBlockRange 0xAA80 0xAADF "Tai Viet"
  | c <= '\xAAFF' = UnicodeBlockRange 0xAAE0 0xAAFF "Meetei Mayek Extensions"
  | c <= '\xAB2F' = UnicodeBlockRange 0xAB00 0xAB2F "Ethiopic Extended-A"
  | c <= '\xAB6F' = UnicodeBlockRange 0xAB30 0xAB6F "Latin Extended-E"
  | c <= '\xABBF' = UnicodeBlockRange 0xAB70 0xABBF "Cherokee Supplement"
  | c <= '\xABFF' = UnicodeBlockRange 0xABC0 0xABFF "Meetei Mayek"
  | c <= '\xD7AF' = UnicodeBlockRange 0xAC00 0xD7AF "Hangul Syllables"
  | c <= '\xD7FF' = UnicodeBlockRange 0xD7B0 0xD7FF "Hangul Jamo Extended-B"
  | c <= '\xDB7F' = UnicodeBlockRange 0xD800 0xDB7F "High Surrogates"
  | c <= '\xDBFF' = UnicodeBlockRange 0xDB80 0xDBFF "High Private Use Surrogates"
  | c <= '\xDFFF' = UnicodeBlockRange 0xDC00 0xDFFF "Low Surrogates"
  | c <= '\xF8FF' = UnicodeBlockRange 0xE000 0xF8FF "Private Use Area"
  | c <= '\xFAFF' = UnicodeBlockRange 0xF900 0xFAFF "CJK Compatibility Ideographs"
  | c <= '\xFB4F' = UnicodeBlockRange 0xFB00 0xFB4F "Alphabetic Presentation Forms"
  | c <= '\xFDFF' = UnicodeBlockRange 0xFB50 0xFDFF "Arabic Presentation Forms-A"
  | c <= '\xFE0F' = UnicodeBlockRange 0xFE00 0xFE0F "Variation Selectors"
  | c <= '\xFE1F' = UnicodeBlockRange 0xFE10 0xFE1F "Vertical Forms"
  | c <= '\xFE2F' = UnicodeBlockRange 0xFE20 0xFE2F "Combining Half Marks"
  | c <= '\xFE4F' = UnicodeBlockRange 0xFE30 0xFE4F "CJK Compatibility Forms"
  | c <= '\xFE6F' = UnicodeBlockRange 0xFE50 0xFE6F "Small Form Variants"
  | c <= '\xFEFF' = UnicodeBlockRange 0xFE70 0xFEFF "Arabic Presentation Forms-B"
  | c <= '\xFFEF' = UnicodeBlockRange 0xFF00 0xFFEF "Halfwidth and Fullwidth Forms"
  | c <= '\xFFFF' = UnicodeBlockRange 0xFFF0 0xFFFF "Specials"
  | c <= '\x1007F' = UnicodeBlockRange 0x10000 0x1007F "Linear B Syllabary"
  | c <= '\x100FF' = UnicodeBlockRange 0x10080 0x100FF "Linear B Ideograms"
  | c <= '\x1013F' = UnicodeBlockRange 0x10100 0x1013F "Aegean Numbers"
  | c <= '\x1018F' = UnicodeBlockRange 0x10140 0x1018F "Ancient Greek Numbers"
  | c <= '\x101CF' = UnicodeBlockRange 0x10190 0x101CF "Ancient Symbols"
  | c <= '\x101FF' = UnicodeBlockRange 0x101D0 0x101FF "Phaistos Disc"
  | c <= '\x1027F' = UnicodeBlockRange 0x10200 0x1027F "None"
  | c <= '\x1029F' = UnicodeBlockRange 0x10280 0x1029F "Lycian"
  | c <= '\x102DF' = UnicodeBlockRange 0x102A0 0x102DF "Carian"
  | c <= '\x102FF' = UnicodeBlockRange 0x102E0 0x102FF "Coptic Epact Numbers"
  | c <= '\x1032F' = UnicodeBlockRange 0x10300 0x1032F "Old Italic"
  | c <= '\x1034F' = UnicodeBlockRange 0x10330 0x1034F "Gothic"
  | c <= '\x1037F' = UnicodeBlockRange 0x10350 0x1037F "Old Permic"
  | c <= '\x1039F' = UnicodeBlockRange 0x10380 0x1039F "Ugaritic"
  | c <= '\x103DF' = UnicodeBlockRange 0x103A0 0x103DF "Old Persian"
  | c <= '\x103FF' = UnicodeBlockRange 0x103E0 0x103FF "None"
  | c <= '\x1044F' = UnicodeBlockRange 0x10400 0x1044F "Deseret"
  | c <= '\x1047F' = UnicodeBlockRange 0x10450 0x1047F "Shavian"
  | c <= '\x104AF' = UnicodeBlockRange 0x10480 0x104AF "Osmanya"
  | c <= '\x104FF' = UnicodeBlockRange 0x104B0 0x104FF "Osage"
  | c <= '\x1052F' = UnicodeBlockRange 0x10500 0x1052F "Elbasan"
  | c <= '\x1056F' = UnicodeBlockRange 0x10530 0x1056F "Caucasian Albanian"
  | c <= '\x105FF' = UnicodeBlockRange 0x10570 0x105FF "None"
  | c <= '\x1077F' = UnicodeBlockRange 0x10600 0x1077F "Linear A"
  | c <= '\x107FF' = UnicodeBlockRange 0x10780 0x107FF "None"
  | c <= '\x1083F' = UnicodeBlockRange 0x10800 0x1083F "Cypriot Syllabary"
  | c <= '\x1085F' = UnicodeBlockRange 0x10840 0x1085F "Imperial Aramaic"
  | c <= '\x1087F' = UnicodeBlockRange 0x10860 0x1087F "Palmyrene"
  | c <= '\x108AF' = UnicodeBlockRange 0x10880 0x108AF "Nabataean"
  | c <= '\x108DF' = UnicodeBlockRange 0x108B0 0x108DF "None"
  | c <= '\x108FF' = UnicodeBlockRange 0x108E0 0x108FF "Hatran"
  | c <= '\x1091F' = UnicodeBlockRange 0x10900 0x1091F "Phoenician"
  | c <= '\x1093F' = UnicodeBlockRange 0x10920 0x1093F "Lydian"
  | c <= '\x1097F' = UnicodeBlockRange 0x10940 0x1097F "None"
  | c <= '\x1099F' = UnicodeBlockRange 0x10980 0x1099F "Meroitic Hieroglyphs"
  | c <= '\x109FF' = UnicodeBlockRange 0x109A0 0x109FF "Meroitic Cursive"
  | c <= '\x10A5F' = UnicodeBlockRange 0x10A00 0x10A5F "Kharoshthi"
  | c <= '\x10A7F' = UnicodeBlockRange 0x10A60 0x10A7F "Old South Arabian"
  | c <= '\x10A9F' = UnicodeBlockRange 0x10A80 0x10A9F "Old North Arabian"
  | c <= '\x10ABF' = UnicodeBlockRange 0x10AA0 0x10ABF "None"
  | c <= '\x10AFF' = UnicodeBlockRange 0x10AC0 0x10AFF "Manichaean"
  | c <= '\x10B3F' = UnicodeBlockRange 0x10B00 0x10B3F "Avestan"
  | c <= '\x10B5F' = UnicodeBlockRange 0x10B40 0x10B5F "Inscriptional Parthian"
  | c <= '\x10B7F' = UnicodeBlockRange 0x10B60 0x10B7F "Inscriptional Pahlavi"
  | c <= '\x10BAF' = UnicodeBlockRange 0x10B80 0x10BAF "Psalter Pahlavi"
  | c <= '\x10BFF' = UnicodeBlockRange 0x10BB0 0x10BFF "None"
  | c <= '\x10C4F' = UnicodeBlockRange 0x10C00 0x10C4F "Old Turkic"
  | c <= '\x10C7F' = UnicodeBlockRange 0x10C50 0x10C7F "None"
  | c <= '\x10CFF' = UnicodeBlockRange 0x10C80 0x10CFF "Old Hungarian"
  | c <= '\x10D3F' = UnicodeBlockRange 0x10D00 0x10D3F "Hanifi Rohingya"
  | c <= '\x10E5F' = UnicodeBlockRange 0x10D40 0x10E5F "None"
  | c <= '\x10E7F' = UnicodeBlockRange 0x10E60 0x10E7F "Rumi Numeral Symbols"
  | c <= '\x10EBF' = UnicodeBlockRange 0x10E80 0x10EBF "Yezidi"
  | c <= '\x10EFF' = UnicodeBlockRange 0x10EC0 0x10EFF "None"
  | c <= '\x10F2F' = UnicodeBlockRange 0x10F00 0x10F2F "Old Sogdian"
  | c <= '\x10F6F' = UnicodeBlockRange 0x10F30 0x10F6F "Sogdian"
  | c <= '\x10FAF' = UnicodeBlockRange 0x10F70 0x10FAF "None"
  | c <= '\x10FDF' = UnicodeBlockRange 0x10FB0 0x10FDF "Chorasmian"
  | c <= '\x10FFF' = UnicodeBlockRange 0x10FE0 0x10FFF "Elymaic"
  | c <= '\x1107F' = UnicodeBlockRange 0x11000 0x1107F "Brahmi"
  | c <= '\x110CF' = UnicodeBlockRange 0x11080 0x110CF "Kaithi"
  | c <= '\x110FF' = UnicodeBlockRange 0x110D0 0x110FF "Sora Sompeng"
  | c <= '\x1114F' = UnicodeBlockRange 0x11100 0x1114F "Chakma"
  | c <= '\x1117F' = UnicodeBlockRange 0x11150 0x1117F "Mahajani"
  | c <= '\x111DF' = UnicodeBlockRange 0x11180 0x111DF "Sharada"
  | c <= '\x111FF' = UnicodeBlockRange 0x111E0 0x111FF "Sinhala Archaic Numbers"
  | c <= '\x1124F' = UnicodeBlockRange 0x11200 0x1124F "Khojki"
  | c <= '\x1127F' = UnicodeBlockRange 0x11250 0x1127F "None"
  | c <= '\x112AF' = UnicodeBlockRange 0x11280 0x112AF "Multani"
  | c <= '\x112FF' = UnicodeBlockRange 0x112B0 0x112FF "Khudawadi"
  | c <= '\x1137F' = UnicodeBlockRange 0x11300 0x1137F "Grantha"
  | c <= '\x113FF' = UnicodeBlockRange 0x11380 0x113FF "None"
  | c <= '\x1147F' = UnicodeBlockRange 0x11400 0x1147F "Newa"
  | c <= '\x114DF' = UnicodeBlockRange 0x11480 0x114DF "Tirhuta"
  | c <= '\x1157F' = UnicodeBlockRange 0x114E0 0x1157F "None"
  | c <= '\x115FF' = UnicodeBlockRange 0x11580 0x115FF "Siddham"
  | c <= '\x1165F' = UnicodeBlockRange 0x11600 0x1165F "Modi"
  | c <= '\x1167F' = UnicodeBlockRange 0x11660 0x1167F "Mongolian Supplement"
  | c <= '\x116CF' = UnicodeBlockRange 0x11680 0x116CF "Takri"
  | c <= '\x116FF' = UnicodeBlockRange 0x116D0 0x116FF "None"
  | c <= '\x1173F' = UnicodeBlockRange 0x11700 0x1173F "Ahom"
  | c <= '\x117FF' = UnicodeBlockRange 0x11740 0x117FF "None"
  | c <= '\x1184F' = UnicodeBlockRange 0x11800 0x1184F "Dogra"
  | c <= '\x1189F' = UnicodeBlockRange 0x11850 0x1189F "None"
  | c <= '\x118FF' = UnicodeBlockRange 0x118A0 0x118FF "Warang Citi"
  | c <= '\x1195F' = UnicodeBlockRange 0x11900 0x1195F "Dives Akuru"
  | c <= '\x1199F' = UnicodeBlockRange 0x11960 0x1199F "None"
  | c <= '\x119FF' = UnicodeBlockRange 0x119A0 0x119FF "Nandinagari"
  | c <= '\x11A4F' = UnicodeBlockRange 0x11A00 0x11A4F "Zanabazar Square"
  | c <= '\x11AAF' = UnicodeBlockRange 0x11A50 0x11AAF "Soyombo"
  | c <= '\x11ABF' = UnicodeBlockRange 0x11AB0 0x11ABF "None"
  | c <= '\x11AFF' = UnicodeBlockRange 0x11AC0 0x11AFF "Pau Cin Hau"
  | c <= '\x11BFF' = UnicodeBlockRange 0x11B00 0x11BFF "None"
  | c <= '\x11C6F' = UnicodeBlockRange 0x11C00 0x11C6F "Bhaiksuki"
  | c <= '\x11CBF' = UnicodeBlockRange 0x11C70 0x11CBF "Marchen"
  | c <= '\x11CFF' = UnicodeBlockRange 0x11CC0 0x11CFF "None"
  | c <= '\x11D5F' = UnicodeBlockRange 0x11D00 0x11D5F "Masaram Gondi"
  | c <= '\x11DAF' = UnicodeBlockRange 0x11D60 0x11DAF "Gunjala Gondi"
  | c <= '\x11EDF' = UnicodeBlockRange 0x11DB0 0x11EDF "None"
  | c <= '\x11EFF' = UnicodeBlockRange 0x11EE0 0x11EFF "Makasar"
  | c <= '\x11FAF' = UnicodeBlockRange 0x11F00 0x11FAF "None"
  | c <= '\x11FBF' = UnicodeBlockRange 0x11FB0 0x11FBF "Lisu Supplement"
  | c <= '\x11FFF' = UnicodeBlockRange 0x11FC0 0x11FFF "Tamil Supplement"
  | c <= '\x123FF' = UnicodeBlockRange 0x12000 0x123FF "Cuneiform"
  | c <= '\x1247F' = UnicodeBlockRange 0x12400 0x1247F "Cuneiform Numbers and Punctuation"
  | c <= '\x1254F' = UnicodeBlockRange 0x12480 0x1254F "Early Dynastic Cuneiform"
  | c <= '\x12FFF' = UnicodeBlockRange 0x12550 0x12FFF "None"
  | c <= '\x1342F' = UnicodeBlockRange 0x13000 0x1342F "Egyptian Hieroglyphs"
  | c <= '\x1343F' = UnicodeBlockRange 0x13430 0x1343F "Egyptian Hieroglyph Format Controls"
  | c <= '\x143FF' = UnicodeBlockRange 0x13440 0x143FF "None"
  | c <= '\x1467F' = UnicodeBlockRange 0x14400 0x1467F "Anatolian Hieroglyphs"
  | c <= '\x167FF' = UnicodeBlockRange 0x14680 0x167FF "None"
  | c <= '\x16A3F' = UnicodeBlockRange 0x16800 0x16A3F "Bamum Supplement"
  | c <= '\x16A6F' = UnicodeBlockRange 0x16A40 0x16A6F "Mro"
  | c <= '\x16ACF' = UnicodeBlockRange 0x16A70 0x16ACF "None"
  | c <= '\x16AFF' = UnicodeBlockRange 0x16AD0 0x16AFF "Bassa Vah"
  | c <= '\x16B8F' = UnicodeBlockRange 0x16B00 0x16B8F "Pahawh Hmong"
  | c <= '\x16E3F' = UnicodeBlockRange 0x16B90 0x16E3F "None"
  | c <= '\x16E9F' = UnicodeBlockRange 0x16E40 0x16E9F "Medefaidrin"
  | c <= '\x16EFF' = UnicodeBlockRange 0x16EA0 0x16EFF "None"
  | c <= '\x16F9F' = UnicodeBlockRange 0x16F00 0x16F9F "Miao"
  | c <= '\x16FDF' = UnicodeBlockRange 0x16FA0 0x16FDF "None"
  | c <= '\x16FFF' = UnicodeBlockRange 0x16FE0 0x16FFF "Ideographic Symbols and Punctuation"
  | c <= '\x187FF' = UnicodeBlockRange 0x17000 0x187FF "Tangut"
  | c <= '\x18AFF' = UnicodeBlockRange 0x18800 0x18AFF "Tangut Components"
  | c <= '\x18CFF' = UnicodeBlockRange 0x18B00 0x18CFF "Khitan Small Script"
  | c <= '\x18D8F' = UnicodeBlockRange 0x18D00 0x18D8F "Tangut Supplement"
  | c <= '\x1AFFF' = UnicodeBlockRange 0x18D90 0x1AFFF "None"
  | c <= '\x1B0FF' = UnicodeBlockRange 0x1B000 0x1B0FF "Kana Supplement"
  | c <= '\x1B12F' = UnicodeBlockRange 0x1B100 0x1B12F "Kana Extended-A"
  | c <= '\x1B16F' = UnicodeBlockRange 0x1B130 0x1B16F "Small Kana Extension"
  | c <= '\x1B2FF' = UnicodeBlockRange 0x1B170 0x1B2FF "Nushu"
  | c <= '\x1BBFF' = UnicodeBlockRange 0x1B300 0x1BBFF "None"
  | c <= '\x1BC9F' = UnicodeBlockRange 0x1BC00 0x1BC9F "Duployan"
  | c <= '\x1BCAF' = UnicodeBlockRange 0x1BCA0 0x1BCAF "Shorthand Format Controls"
  | c <= '\x1CFFF' = UnicodeBlockRange 0x1BCB0 0x1CFFF "None"
  | c <= '\x1D0FF' = UnicodeBlockRange 0x1D000 0x1D0FF "Byzantine Musical Symbols"
  | c <= '\x1D1FF' = UnicodeBlockRange 0x1D100 0x1D1FF "Musical Symbols"
  | c <= '\x1D24F' = UnicodeBlockRange 0x1D200 0x1D24F "Ancient Greek Musical Notation"
  | c <= '\x1D2DF' = UnicodeBlockRange 0x1D250 0x1D2DF "None"
  | c <= '\x1D2FF' = UnicodeBlockRange 0x1D2E0 0x1D2FF "Mayan Numerals"
  | c <= '\x1D35F' = UnicodeBlockRange 0x1D300 0x1D35F "Tai Xuan Jing Symbols"
  | c <= '\x1D37F' = UnicodeBlockRange 0x1D360 0x1D37F "Counting Rod Numerals"
  | c <= '\x1D3FF' = UnicodeBlockRange 0x1D380 0x1D3FF "None"
  | c <= '\x1D7FF' = UnicodeBlockRange 0x1D400 0x1D7FF "Mathematical Alphanumeric Symbols"
  | c <= '\x1DAAF' = UnicodeBlockRange 0x1D800 0x1DAAF "Sutton SignWriting"
  | c <= '\x1DFFF' = UnicodeBlockRange 0x1DAB0 0x1DFFF "None"
  | c <= '\x1E02F' = UnicodeBlockRange 0x1E000 0x1E02F "Glagolitic Supplement"
  | c <= '\x1E0FF' = UnicodeBlockRange 0x1E030 0x1E0FF "None"
  | c <= '\x1E14F' = UnicodeBlockRange 0x1E100 0x1E14F "Nyiakeng Puachue Hmong"
  | c <= '\x1E2BF' = UnicodeBlockRange 0x1E150 0x1E2BF "None"
  | c <= '\x1E2FF' = UnicodeBlockRange 0x1E2C0 0x1E2FF "Wancho"
  | c <= '\x1E7FF' = UnicodeBlockRange 0x1E300 0x1E7FF "None"
  | c <= '\x1E8DF' = UnicodeBlockRange 0x1E800 0x1E8DF "Mende Kikakui"
  | c <= '\x1E8FF' = UnicodeBlockRange 0x1E8E0 0x1E8FF "None"
  | c <= '\x1E95F' = UnicodeBlockRange 0x1E900 0x1E95F "Adlam"
  | c <= '\x1EC6F' = UnicodeBlockRange 0x1E960 0x1EC6F "None"
  | c <= '\x1ECBF' = UnicodeBlockRange 0x1EC70 0x1ECBF "Indic Siyaq Numbers"
  | c <= '\x1ECFF' = UnicodeBlockRange 0x1ECC0 0x1ECFF "None"
  | c <= '\x1ED4F' = UnicodeBlockRange 0x1ED00 0x1ED4F "Ottoman Siyaq Numbers"
  | c <= '\x1EDFF' = UnicodeBlockRange 0x1ED50 0x1EDFF "None"
  | c <= '\x1EEFF' = UnicodeBlockRange 0x1EE00 0x1EEFF "Arabic Mathematical Alphabetic Symbols"
  | c <= '\x1EFFF' = UnicodeBlockRange 0x1EF00 0x1EFFF "None"
  | c <= '\x1F02F' = UnicodeBlockRange 0x1F000 0x1F02F "Mahjong Tiles"
  | c <= '\x1F09F' = UnicodeBlockRange 0x1F030 0x1F09F "Domino Tiles"
  | c <= '\x1F0FF' = UnicodeBlockRange 0x1F0A0 0x1F0FF "Playing Cards"
  | c <= '\x1F1FF' = UnicodeBlockRange 0x1F100 0x1F1FF "Enclosed Alphanumeric Supplement"
  | c <= '\x1F2FF' = UnicodeBlockRange 0x1F200 0x1F2FF "Enclosed Ideographic Supplement"
  | c <= '\x1F5FF' = UnicodeBlockRange 0x1F300 0x1F5FF "Miscellaneous Symbols and Pictographs"
  | c <= '\x1F64F' = UnicodeBlockRange 0x1F600 0x1F64F "Emoticons"
  | c <= '\x1F67F' = UnicodeBlockRange 0x1F650 0x1F67F "Ornamental Dingbats"
  | c <= '\x1F6FF' = UnicodeBlockRange 0x1F680 0x1F6FF "Transport and Map Symbols"
  | c <= '\x1F77F' = UnicodeBlockRange 0x1F700 0x1F77F "Alchemical Symbols"
  | c <= '\x1F7FF' = UnicodeBlockRange 0x1F780 0x1F7FF "Geometric Shapes Extended"
  | c <= '\x1F8FF' = UnicodeBlockRange 0x1F800 0x1F8FF "Supplemental Arrows-C"
  | c <= '\x1F9FF' = UnicodeBlockRange 0x1F900 0x1F9FF "Supplemental Symbols and Pictographs"
  | c <= '\x1FA6F' = UnicodeBlockRange 0x1FA00 0x1FA6F "Chess Symbols"
  | c <= '\x1FAFF' = UnicodeBlockRange 0x1FA70 0x1FAFF "Symbols and Pictographs Extended-A"
  | c <= '\x1FBFF' = UnicodeBlockRange 0x1FB00 0x1FBFF "Symbols for Legacy Computing"
  | c <= '\x1FFFF' = UnicodeBlockRange 0x1FC00 0x1FFFF "None"
  | c <= '\x2A6DF' = UnicodeBlockRange 0x20000 0x2A6DF "CJK Unified Ideographs Extension B"
  | c <= '\x2A6FF' = UnicodeBlockRange 0x2A6E0 0x2A6FF "None"
  | c <= '\x2B73F' = UnicodeBlockRange 0x2A700 0x2B73F "CJK Unified Ideographs Extension C"
  | c <= '\x2B81F' = UnicodeBlockRange 0x2B740 0x2B81F "CJK Unified Ideographs Extension D"
  | c <= '\x2CEAF' = UnicodeBlockRange 0x2B820 0x2CEAF "CJK Unified Ideographs Extension E"
  | c <= '\x2EBEF' = UnicodeBlockRange 0x2CEB0 0x2EBEF "CJK Unified Ideographs Extension F"
  | c <= '\x2F7FF' = UnicodeBlockRange 0x2EBF0 0x2F7FF "None"
  | c <= '\x2FA1F' = UnicodeBlockRange 0x2F800 0x2FA1F "CJK Compatibility Ideographs Supplement"
  | c <= '\x2FFFF' = UnicodeBlockRange 0x2FA20 0x2FFFF "None"
  | c <= '\x3134F' = UnicodeBlockRange 0x30000 0x3134F "CJK Unified Ideographs Extension G"
  | c <= '\xDFFFF' = UnicodeBlockRange 0x31350 0xDFFFF "None"
  | c <= '\xE007F' = UnicodeBlockRange 0xE0000 0xE007F "Tags"
  | c <= '\xE00FF' = UnicodeBlockRange 0xE0080 0xE00FF "None"
  | c <= '\xE01EF' = UnicodeBlockRange 0xE0100 0xE01EF "Variation Selectors Supplement"
  | c <= '\xEFFFF' = UnicodeBlockRange 0xE01F0 0xEFFFF "None"
  | c <= '\xFFFFF' = UnicodeBlockRange 0xF0000 0xFFFFF "Supplementary Private Use Area-A"
  | c <= '\x10FFFF' = UnicodeBlockRange 0x100000 0x10FFFF "Supplementary Private Use Area-B"
unicodeBlockRange _ = UnicodeBlockRange 0 0 "No Block"
