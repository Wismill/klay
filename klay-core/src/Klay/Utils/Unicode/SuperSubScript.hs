{-# LANGUAGE OverloadedLists   #-}

module Klay.Utils.Unicode.SuperSubScript
  ( -- * Superscript
    toSuperscript
  , superscriptMap
    -- * Subscript
  , toSubscript
  , subscriptMap )
  where


import Data.Map.Strict qualified as Map

-- [TODO] Add other super/subscripts such as Greek, etc.

toSuperscript :: Char -> Maybe Char
toSuperscript = flip Map.lookup superscriptMap

superscriptMap :: Map.Map Char Char
superscriptMap =
    [ ('A', 'ᴬ')
    , ('B', 'ᴮ')
    , ('D', 'ᴰ')
    , ('E', 'ᴱ')
    , ('G', 'ᴳ')
    , ('H', 'ᴴ')
    , ('I', 'ᴵ')
    , ('J', 'ᴶ')
    , ('K', 'ᴷ')
    , ('L', 'ᴸ')
    , ('M', 'ᴹ')
    , ('N', 'ᴺ')
    , ('O', 'ᴼ')
    , ('P', 'ᴾ')
    , ('R', 'ᴿ')
    , ('T', 'ᵀ')
    , ('U', 'ᵁ')
    , ('W', 'ᵂ')
    , ('a', 'ᵃ')
    , ('b', 'ᵇ')
    , ('c', 'ᶜ')
    , ('d', 'ᵈ')
    , ('e', 'ᵉ')
    , ('f', 'ᶠ')
    , ('g', 'ᵍ')
    , ('h', 'ʰ')
    , ('i', 'ⁱ')
    , ('j', 'ʲ')
    , ('k', 'ᵏ')
    , ('l', 'ˡ')
    , ('m', 'ᵐ')
    , ('n', 'ⁿ')
    , ('o', 'ᵒ')
    , ('p', 'ᵖ')
    , ('r', 'ʳ')
    , ('s', 'ˢ')
    , ('t', 'ᵗ')
    , ('u', 'ᵘ')
    , ('v', 'ᵛ')
    , ('w', 'ʷ')
    , ('x', 'ˣ')
    , ('y', 'ʸ')
    , ('z', 'ᶻ')
    , ('ə', 'ᵊ')
    -- [TODO] ˀˁˤ, etc.
    -- [TODO] ʴʵʶᴭᴲᴽᵋᵌᵑᵓᵝᵞᵟᵠᵡᶿ
    , ('0', '⁰')
    , ('1', '¹')
    , ('2', '²')
    , ('3', '³')
    , ('4', '⁴')
    , ('5', '⁵')
    , ('6', '⁶')
    , ('7', '⁷')
    , ('8', '⁸')
    , ('9', '⁹')
    , ('+', '⁺')
    , ('-', '⁻')
    , ('=', '⁼')
    , ('(', '⁽')
    , (')', '⁾') ]


toSubscript :: Char -> Maybe Char
toSubscript = flip Map.lookup subscriptMap

subscriptMap :: Map.Map Char Char
subscriptMap =
    [ ('a', 'ₐ')
    , ('e', 'ₑ')
    , ('h', 'ₕ')
    , ('i', 'ᵢ')
    , ('j', 'ⱼ')
    , ('k', 'ₖ')
    , ('l', 'ₗ')
    , ('m', 'ₘ')
    , ('n', 'ₙ')
    , ('o', 'ₒ')
    , ('p', 'ₚ')
    , ('r', 'ᵣ')
    , ('s', 'ₛ')
    , ('t', 'ₜ')
    , ('u', 'ᵤ')
    , ('v', 'ᵥ')
    , ('x', 'ₓ')
    , ('ə', 'ₔ')
    , ('ɩ', 'ͺ')
    , ('ι', 'ͺ')
    -- [TODO] ᵦᵧᵨᵩᵪ
    , ('0', '₀')
    , ('1', '₁')
    , ('2', '₂')
    , ('3', '₃')
    , ('4', '₄')
    , ('5', '₅')
    , ('6', '₆')
    , ('7', '₇')
    , ('8', '₈')
    , ('9', '₉')
    , ('+', '₊')
    , ('-', '₋')
    , ('=', '₌')
    , ('(', '₍')
    , (')', '₎') ]
