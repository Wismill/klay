{-# OPTIONS_GHC -Wno-orphans #-}

module Klay.Utils.Orphans.BCP47 () where

import Data.BCP47 (BCP47)
import Data.BCP47 qualified as BCP47
import Data.Aeson (ToJSONKey(..))
import Data.Aeson.Types qualified as Aeson

instance ToJSONKey BCP47 where
  toJSONKey = Aeson.toJSONKeyText BCP47.toText
