 {-# OPTIONS_GHC -fno-warn-orphans #-}

 module Klay.Utils.Orphans.Versions () where

import Data.Versions
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Utils (toJsonOptionsNewtype)

instance ToJSON SemVer where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON SemVer where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

instance ToJSON Version where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON Version where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

instance ToJSON VUnit where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON VUnit where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype
