{-# OPTIONS_GHC -Wno-orphans #-}

{-|
Description : Orphan instances
-}

module Klay.Utils.Orphans.IntMap.NonEmpty () where

import Data.List.NonEmpty qualified as NE
import GHC.Generics (Generic, Generic1)

import Data.IntMap.NonEmpty qualified as NEIntMap
import Data.IntMap.NonEmpty.Internal qualified as NEIntMap
import Data.Semialign (Semialign(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..), Arbitrary1(..), genericShrink)
import TextShow (TextShow, FromStringShow(..))

deriving instance Generic1 NEIntMap.NEIntMap
deriving instance Generic (NEIntMap.NEIntMap a)

instance Semialign NEIntMap.NEIntMap where
  align a b = NEIntMap.unsafeFromMap $ align (NEIntMap.toMap a) (NEIntMap.toMap b)

instance (Arbitrary a) => Arbitrary (NEIntMap.NEIntMap a) where
  arbitrary = NEIntMap.fromList <$> ((NE.:|) <$> arbitrary <*> arbitrary)
  shrink = genericShrink

instance Arbitrary1 NEIntMap.NEIntMap where
  liftArbitrary g = NEIntMap.fromList <$> ((NE.:|) <$> liftArbitrary g <*> liftArbitrary (liftArbitrary g))
  liftShrink f a =
    let x NE.:| xs = NEIntMap.toList a
    in NEIntMap.fromList <$> [y NE.:| ys | y <- liftShrink f x, ys <- liftShrink (liftShrink f) xs]

deriving via (FromStringShow (NEIntMap.NEIntMap a)) instance (Show a) => TextShow (NEIntMap.NEIntMap a)
