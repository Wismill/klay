{-# OPTIONS_GHC -Wno-orphans #-}

module Klay.Utils.Orphans.Set.NonEmpty () where

import Data.List.NonEmpty qualified as NE
import Data.Hashable (Hashable(..))
import Data.Hashable.Lifted (Hashable1(..))
import GHC.Exts qualified as Exts
import GHC.Generics (Generic(..), Generic1(..))

import Data.Set.NonEmpty qualified as NESet
import Data.Set.NonEmpty.Internal qualified as NESet

import Test.QuickCheck.Arbitrary (Arbitrary(..), genericShrink)

deriving instance Generic1 NESet.NESet
deriving instance Generic (NESet.NESet a)
instance Hashable1 NESet.NESet where
  liftHashWithSalt f s = NESet.foldl' f s
instance (Hashable a) => Hashable (NESet.NESet a) where
  hashWithSalt s = liftHashWithSalt hashWithSalt s

instance (Ord a) => Exts.IsList (NESet.NESet a) where
  type (Item (NESet.NESet a)) = a
  fromList = NESet.fromList . Exts.fromList
  toList = NE.toList . NESet.toList

instance (Ord a, Arbitrary a) => Arbitrary (NESet.NESet a) where
  arbitrary = NESet.fromList <$> ((NE.:|) <$> arbitrary <*> arbitrary)
  shrink = genericShrink
