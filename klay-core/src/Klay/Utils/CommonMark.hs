module Klay.Utils.CommonMark
  ( escapeChar
  , escapeText
  ) where

import Data.Text.Lazy qualified as TL

escapeChar :: Char -> TL.Text
escapeChar = \case
  '\\'  -> "\\\\"
  '`'  -> "\\`"
  '*'  -> "\\*"
  '_'  -> "\\_"
  '{'  -> "\\{"
  '}'  -> "\\}"
  '['  -> "\\["
  ']'  -> "\\]"
  '('  -> "\\("
  ')'  -> "\\)"
  '#'  -> "\\#"
  '-'  -> "\\-"
  '.'  -> "\\."
  '!'  -> "\\!"
  '|'  -> "\\|"
  '<'  -> "&lt;"
  '>'  -> "&gt;"
  '&'  -> "&amp;"
  '"'  -> "&quot;"
  '\'' -> "&#39;"
  c    -> TL.singleton c

escapeText :: TL.Text -> TL.Text
escapeText = TL.concatMap escapeChar
