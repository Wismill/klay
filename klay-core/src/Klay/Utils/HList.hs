{-# LANGUAGE PolyKinds, TypeOperators #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}

module Klay.Utils.HList
  ( HList(..)
  , DList(..)
  , All(..)
  , dtail
  , ofType
  , hfoldl
  , hfoldr
  , htraverse_
  ) where

import Data.Kind
import Data.Proxy
import Data.Typeable
import Data.Maybe (maybeToList)

-- Source: https://www.howtobuildsoftware.com/index.php/how-do/dem/haskell-type-level-computation-hlist-fold-over-a-heterogeneous-compile-time-list

-- | HList is a fairly standard definition of a heterogeneous list
data HList :: [Type] -> Type where
  HNil :: HList '[]
  HCons :: x -> HList xs -> HList (x ': xs)

{-|
notion of All things in a list of types satisfying some constraint.
We will store the dictionaries for satisfied constraints in a GADT
that either captures both the head constraint dictionary and constraints for All of the the tail
or proves that the list is empty.
A type list satisfies a constraint for All it's items if we can capture the dictionaries for it.
-}
class All (c :: Type -> Constraint) (l :: [Type]) where
  allDict :: p1 c -> p2 l -> DList c l

-- An empty list of types trivially satisfies any constraint applied to all of its items
instance All c '[] where
  allDict _ _ = DNil

-- If the head of a list satisfies a constraint and all of the tail does too,
-- then everything in the list satisfies the constraint.
instance (c x, All c xs) => All c (x ': xs) where
  allDict _ _ = DCons

{-|
DList really is a list of dictionaries. DCons captures the dictionary for the constraint applied
to the head item (ctx h) and all the dictionaries for the remainder of the list (All ctx t).
We can't get the dictionaries for the tail directly from the constructor, but we can write
a function 'dtail' that extracts them from the dictionary for All ctx t.
-}
data DList (ctx :: Type -> Constraint) (l :: [Type]) where
  DCons :: (ctx h, All ctx t) => DList ctx (h ': t)
  DNil  ::                       DList ctx '[]

dtail :: forall ctx h t. DList ctx (h ': t) -> DList ctx t
dtail DCons = allDict (Proxy :: Proxy ctx) (Proxy :: Proxy t)

zipDHWith :: forall c w l p. (All c l, Monoid w) => (forall a. (c a) => a -> w) -> p c -> HList l -> w
zipDHWith f p l = zipDHWith' (allDict p l) l
  where
    zipDHWith' :: forall l'. DList c l' -> HList l' -> w
    zipDHWith' d@DCons (HCons x t) = f x <> zipDHWith' (dtail d) t
    zipDHWith' DNil    HNil        = mempty

ofType :: (All Typeable l, Typeable a) => HList l -> [a]
ofType = zipDHWith (maybeToList . cast) (Proxy :: Proxy Typeable)

hfoldr :: forall c xs a. (All c xs) => (forall x. (c x) => x -> a -> a) -> a -> HList xs -> a
hfoldr f a xs = hfoldr' a (allDict (Proxy :: Proxy c) xs) xs
  where
    hfoldr' :: forall ys. a -> DList c ys -> HList ys -> a
    hfoldr' acc d@DCons (HCons y ys) = f y (hfoldr' acc (dtail d) ys)
    hfoldr' acc DNil    HNil         = acc

hfoldl :: forall c xs a. (All c xs) => (forall x. (c x) => a -> x -> a) -> a -> HList xs -> a
hfoldl f a xs = hfoldl' a (allDict (Proxy :: Proxy c) xs) xs
  where
    hfoldl' :: forall ys. a -> DList c ys -> HList ys -> a
    hfoldl' acc d@DCons (HCons y ys) = hfoldl' (f acc y) (dtail d) ys
    hfoldl' acc DNil    HNil         = acc

htraverse_ :: forall c xs a f. (Applicative f, All c xs) => (forall x. (c x) => x -> f a) -> HList xs -> f ()
htraverse_ f xs = htraverse_' (allDict (Proxy :: Proxy c) xs) xs
  where
    htraverse_' :: forall ys. DList c ys -> HList ys -> f ()
    htraverse_' d@DCons (HCons y ys) = f y *> htraverse_' (dtail d) ys
    htraverse_' DNil    HNil         = pure ()
