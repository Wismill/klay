{-# LANGUAGE OverloadedLists #-}

module Klay.Utils.ISO_9995_9
  ( latinToGreekISO_9995_9
  , latinToCyrillicISO_9995_9 )
  where


import Data.Map.Strict (Map)


-- [NOTE] Source: https://bepo.fr/wiki/Lettres_grecques
latinToGreekISO_9995_9 :: Map Char Char
latinToGreekISO_9995_9 =
  [ ('A', 'Α')
  , ('B', 'Β')
  , ('C', 'Ψ')
  , ('D', 'Δ')
  , ('E', 'Ε')
  , ('F', 'Φ')
  , ('G', 'Γ')
  , ('H', 'Η')
  , ('I', 'Ι')
  , ('J', 'Ξ')
  , ('K', 'Κ')
  , ('L', 'Λ')
  , ('M', 'Μ')
  , ('N', 'Ν')
  , ('O', 'Ο')
  , ('P', 'Π')
  -- [NOTE] Q not defined
  , ('R', 'Ρ')
  , ('S', 'Σ')
  , ('T', 'Τ')
  , ('U', 'Θ')
  , ('V', 'Ω')
  -- [NOTE] W not defined [TODO] ς
  , ('X', 'Χ')
  , ('Y', 'Υ')
  , ('Z', 'Ζ') ]

-- [NOTE] Source: https://bepo.fr/wiki/Version_1.1rc1/Touches_mortes/Cyrillique
latinToCyrillicISO_9995_9 :: Map Char Char
latinToCyrillicISO_9995_9 =
  [ ('a', 'а')
  , ('b', 'б')
  , ('c', 'ц')
  , ('d', 'д')
  , ('e', 'е')
  , ('f', 'ф')
  , ('g', 'г')
  , ('h', 'х')
  , ('i', 'и')
  , ('j', 'э')
  , ('k', 'к')
  , ('l', 'л')
  , ('m', 'м')
  , ('n', 'н')
  , ('o', 'о')
  , ('p', 'п')
  , ('q', 'ж')
  , ('r', 'р')
  , ('s', 'с')
  , ('t', 'т')
  , ('u', 'у')
  , ('v', 'в')
  , ('w', 'ш')
  , ('x', 'ч')
  , ('y', 'ы')
  , ('z', 'з') ]
