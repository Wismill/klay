module Klay.Keyboard.Hardware.ScanCode
  ( ScanCode(..) )
  where

import Numeric.Natural
import GHC.Generics
import Control.DeepSeq

-- | Scan codes should be from set 1.
newtype ScanCode = ScanCode Natural
  deriving stock (Show, Eq, Ord, Generic)
  deriving newtype (Num, NFData)
