{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Hardware.Divisions.ANSI
  ( keyboardSections
  , keyboardZones
  , keySet
  , keyList
  -- Re-export
  , Sections(..)
  , Zones(..)
  ) where


import Data.Set qualified as Set

import Klay.Keyboard.Hardware.Divisions
import Klay.Keyboard.Hardware.Key qualified as K

-- [NOTE] See ISO/IEC 9995-1, section 7.3

keyboardSections :: Sections
keyboardSections = sectionsFromZones keyboardZones

keyboardZones :: Zones
keyboardZones = Zones
  { _alphanumericZone = mconcat
      [ [K.Tilde, K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Plus]
      , [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket, K.Backslash]
      , [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote]
      , [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash]
      , [K.Space]
      ]
  , _alphanumericLeftFunctionZone =
      [ K.LControl, K.LSuper, K.LAlternate
      , K.LShift, K.CapsLock, K.Tabulator ]
  , _alphanumericRightFunctionZone =
      [ K.RControl, K.RSuper, K.RAlternate, K.Menu
      , K.RShift, K.Return, K.Backspace ]
  , _numericZone =
      [ K.KP7, K.KP8, K.KP9
      , K.KP4, K.KP5, K.KP6
      , K.KP1, K.KP2, K.KP3
      , K.KP0, K.KPDecimal
      ]
  , _numericFunctionZone =
      [ K.NumLock, K.KPEnter
      , K.KPDivide, K.KPMultiply, K.KPSubtract, K.KPAdd
      ]
  , _cursorKeyZone =
      [ K.CursorLeft, K.CursorRight, K.CursorUp, K.CursorDown ]
  , _editingFunctionZone = mconcat
      [ [ K.Escape ]
      , Set.fromList $ enumFromTo K.F1 K.F12
      , [ K.Insert, K.Delete, K.Home, K.End, K.PageUp, K.PageDown ]
      , [ K.PrintScreen, K.ScrollLock, K.Pause ]
      ]
  }

keySet :: Set.Set K.Key
keySet = sectionsKeys keyboardSections

keyList :: [K.Key]
keyList = Set.toList keySet
