{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Hardware.Divisions.TypeMatrix
  ( keyboardSections, keyboardZones
  -- Re-export
  , Sections(..)
  , Zones(..)
  ) where


import Data.Set qualified as Set

import Klay.Keyboard.Hardware.Divisions
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.Divisions.ISO qualified as ISO

-- [NOTE] See ISO/IEC 9995-1, section 7.3

keyboardSections :: Sections
keyboardSections = sectionsFromZones keyboardZones

keyboardZones :: Zones
keyboardZones = ISO.keyboardZones
  { _alphanumericZone = _alphanumericZone ISO.keyboardZones <> otherKeys
  }

otherKeys :: Set.Set K.Key
otherKeys =
  [ K.JPBackSlash
  , K.Yen
  , K.HiraganaKatakana
  , K.Muhenkan
  , K.Henkan
  ]

-- [TODO] Add all missing keys
-- Calculator Email1 Browser MediaPlayPause
