module Klay.Keyboard.Hardware.Divisions.ISO
  ( keyboardSections
  , keyboardZones
  , keySet
  , keyList
  -- Re-export
  , Sections(..)
  , Zones(..)
  ) where


import Data.Set qualified as Set

import Klay.Keyboard.Hardware.Divisions
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.Divisions.ANSI qualified as ANSI

-- [NOTE] See ISO/IEC 9995-1, section 7.3

keyboardSections :: Sections
keyboardSections = sectionsFromZones keyboardZones

keyboardZones :: Zones
keyboardZones = ANSI.keyboardZones
  { _alphanumericZone = Set.insert K.Iso102 (_alphanumericZone ANSI.keyboardZones)
  }

keySet :: Set.Set K.Key
keySet = sectionsKeys keyboardSections

keyList :: [K.Key]
keyList = Set.toList keySet
