{-|
Description: Divisions of the keyboard from ISO/IEC&#xA0;9995-1

The module implements the physical divisions of keyboard defined
in ISO/IEC&#xA0;9995-1, section&#xA0;5.2:

* __'Section's:__ block of keys, mostly with some functional relationship
* __'Zone's:__ part of a keyboard section
-}

module Klay.Keyboard.Hardware.Divisions
  ( Section(..), Sections(..)
  , Zone(..), Zones(..)
  , sectionsFromZones
  , sectionsKeys
  , zonesKeys
  ) where

import Data.Set (Set)

import Klay.Keyboard.Hardware.Key (Key(..))


-- | A block of keys, mostly with some functional relationship
data Section
  = AlphanumericSection
  -- ^ Alphanumeric section
  | NumericSection
  -- ^ Numeric section
  | EditAndFunctionSection
  -- ^ Edit-and-function section
  deriving (Show, Eq, Ord, Enum, Bounded)

-- | Blocks of keys, mostly with some functional relationship
data Sections = Sections
  { _alphanumericSection :: Set Key
  -- ^ Alphanumeric section keys
  , _numericSection :: Set Key
  -- ^ Numeric section keys
  , _editAndFunctionSection :: Set Key
  -- ^ Edit-and-function section keys
  } deriving (Show, Eq)

-- | Part of a keyboard section defined in ISO/IEC&#xA0;9995
data Zone
  = AlphanumericZone
  -- ^ Alphanumeric zone
  | AlphanumericFunctionLeftZone
  -- ^ Alphanumeric left function zone
  | AlphanumericFunctionRigthZone
  -- ^ Alphanumeric right function zone
  | NumericZone
  -- ^ Numeric zone
  | NumericFunctionZone
  -- ^ Numeric function zone
  | CursorKeyZone
  -- ^ Cursor key zone
  | EditingFunctionZone
  -- ^ Editing function zone
  deriving (Show, Eq, Ord, Enum, Bounded)

-- | Blocks of keys from keyboard zones
data Zones = Zones
  { _alphanumericZone :: Set Key
  -- ^ Alphanumeric zone keys
  , _alphanumericLeftFunctionZone :: Set Key
  -- ^ Alphanumeric left function zone keys
  , _alphanumericRightFunctionZone :: Set Key
  -- ^ Alphanumeric right function zone keys
  , _numericZone :: Set Key
  -- ^ Numeric zone keys
  , _numericFunctionZone :: Set Key
  -- ^ Numeric function zone keys
  , _cursorKeyZone :: Set Key
  -- ^ Cursor key zone keys
  , _editingFunctionZone :: Set Key
  -- ^ Editing function zone keys
  } deriving (Show, Eq)

-- | Convert key groupings from zones to sections
sectionsFromZones :: Zones -> Sections
sectionsFromZones zs = Sections
  { _alphanumericSection = mconcat
      [ _alphanumericZone zs
      , _alphanumericLeftFunctionZone zs
      , _alphanumericRightFunctionZone zs
      ]
  , _numericSection = mconcat
      [ _numericZone zs
      , _numericFunctionZone zs
      ]
  , _editAndFunctionSection = mconcat
      [ _cursorKeyZone zs
      , _editingFunctionZone zs
      ]
  }

-- | Get the key set from all sections
sectionsKeys :: Sections -> Set Key
sectionsKeys (Sections a n e) = mconcat [a, n, e]

-- | Get the key set from all zones
zonesKeys :: Zones -> Set Key
zonesKeys (Zones a b c d e f g) = mconcat [a, b, c, d, e, f, g]
