{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Hardware.KeyPosition.ANSI
  ( commonKeyPositions
  , keyPositions, keysMap, keyPositionsMap
  , fromKeyPosition, toKeyPosition
  , escape, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12
  , insert, delete, home, end, pageUp, pageDown
  , left, right, up, down
  , printScreen, scrollLock, pause
  , tilde, n1, n2, n3, n4, n5, n6, n7, n8, n9, n0, minus, equal, backspace
  , tabulator, q, w, e, r, t, y , u, i, o, p, openingSquareBracket, closingSquareBracket, enter
  , caps, a, s, d, f, g, h, j, k, l, semicolon, apostrophe, backslash
  , leftShift, z, x, c, v, b, n, m, comma, period, slash, rightShift
  , controlLeft, superLeft, altLeft, space, altRight, superRight, menu, controlRight
  , kp0, kp1, kp2, kp3, kp4, kp5, kp6, kp7, kp8, kp9, kpDecimalSep
  , kpAdd, kpSubtract, kpDivide, kpMultiply, kpEnter, kpNumLock
  ) where

import Data.Tuple (swap)
import Data.Bifunctor (first)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.KeyPosition

toKeyPosition :: Key -> Maybe KeyPosition
toKeyPosition = flip Map.lookup keyPositionsMap

fromKeyPosition :: KeyPosition -> Maybe Key
fromKeyPosition = flip Map.lookup keysMap

keysMap :: Map KeyPosition Key
keysMap = Map.fromList keyPositions

keyPositionsMap :: Map Key KeyPosition
keyPositionsMap = Map.fromList (fmap swap keyPositions)

keyPositions :: [(KeyPosition, Key)]
keyPositions = commonKeyPositions <>
  [ (KeyPosition D 13, K.Return)
  , (KeyPosition C 12, K.Backslash)
  , (KeyPosition B 0, K.Iso102) ]

mkRow :: Row -> Column -> [Key] -> [(KeyPosition, Key)]
mkRow row c0 = fmap (first (KeyPosition row)) . zip [c0..]

commonKeyPositions :: [(KeyPosition, Key)]
commonKeyPositions = mconcat
  [ -- Function keys
    mkRow K 1 [K.F1, K.F2, K.F3, K.F4, K.F5, K.F6, K.F7, K.F8, K.F9, K.F10, K.F11, K.F12]
    -- Number row
  , mkRow E 0 [K.Tilde, K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Plus, K.Backspace]
    -- Alphabetic: row 3
  , mkRow D 0 [K.Tabulator, K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket] -- K.Backslash
    -- Alphabetic: row 2
  , mkRow C 0 [K.CapsLock, K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote] -- K.Return
    -- Alphabetic: row 1
  , [(KeyPosition B (-1), K.LShift)]
  , mkRow B 1 [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash, K.RShift] -- K.Iso102
    -- System key
  , [(KeyPosition A (-1), K.LControl)]
  , mkRow A 1 [K.LSuper, K.LAlternate, K.Space]
  , mkRow A 8 [K.RAlternate, K.RSuper, K.Menu, K.RControl]
  ]

-- Section: Editing-and-function -----------------------------------------------
-- Function keys
escape, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12 :: KeyPosition
escape = KeyPosition K 0
f1  = KeyPosition K 1
f2  = KeyPosition K 2
f3  = KeyPosition K 3
f4  = KeyPosition K 4
f5  = KeyPosition K 5
f6  = KeyPosition K 6
f7  = KeyPosition K 7
f8  = KeyPosition K 8
f9  = KeyPosition K 9
f10 = KeyPosition K 10
f11 = KeyPosition K 11
f12 = KeyPosition K 12

-- Movements keys
left, right, up, down :: KeyPosition
left = KeyPosition A 30
right = KeyPosition A 32
up = KeyPosition B 31
down = KeyPosition A 31

-- Other functions
insert, delete, home, end, pageUp, pageDown :: KeyPosition
insert = KeyPosition E 30
home = KeyPosition E 31
pageUp = KeyPosition E 32

delete = KeyPosition D 30
end = KeyPosition D 31
pageDown = KeyPosition D 32

printScreen, scrollLock, pause :: KeyPosition
printScreen = KeyPosition K 30
scrollLock = KeyPosition K 31
pause = KeyPosition K 32

-- Section: alphanumeric -------------------------------------------------------

-- 5th row: numbers
tilde, n1, n2, n3, n4, n5, n6, n7, n8, n9, n0, minus, equal, backspace :: KeyPosition
tilde = KeyPosition E 0
n1 = KeyPosition E 1
n2 = KeyPosition E 2
n3 = KeyPosition E 3
n4 = KeyPosition E 4
n5 = KeyPosition E 5
n6 = KeyPosition E 6
n7 = KeyPosition E 7
n8 = KeyPosition E 8
n9 = KeyPosition E 9
n0 = KeyPosition E 10
minus = KeyPosition E 11
equal = KeyPosition E 12
backspace = KeyPosition E 13

-- 4th row: letters
tabulator, q, w, e, r, t, y , u, i, o, p :: KeyPosition
tabulator = KeyPosition D 0
q = KeyPosition D 1
w = KeyPosition D 2
e = KeyPosition D 3
r = KeyPosition D 4
t = KeyPosition D 5
y = KeyPosition D 6
u = KeyPosition D 7
i = KeyPosition D 8
o = KeyPosition D 9
p = KeyPosition D 10
openingSquareBracket, closingSquareBracket, enter :: KeyPosition
openingSquareBracket = KeyPosition D 11 -- OEM_4
closingSquareBracket = KeyPosition D 12 -- OEM_6
backslash = KeyPosition D 13 -- OEM_5

-- 3rd row: letters
caps, a, s, d, f, g, h, j, k, l, semicolon, apostrophe, backslash :: KeyPosition
caps = KeyPosition C 0
a = KeyPosition C 1
s = KeyPosition C 2
d = KeyPosition C 3
f = KeyPosition C 4
g = KeyPosition C 5
h = KeyPosition C 6
j = KeyPosition C 7
k = KeyPosition C 8
l = KeyPosition C 9
semicolon = KeyPosition C 10 -- OEM_1
apostrophe = KeyPosition C 11 -- OEM_7
enter = KeyPosition C 12

-- 2nd row: letters
leftShift, z, x, c, v, b, n, m, comma, period, slash, rightShift :: KeyPosition
leftShift = KeyPosition B (-1)
z = KeyPosition B 1
x = KeyPosition B 2
c = KeyPosition B 3
v = KeyPosition B 4
b = KeyPosition B 5
n = KeyPosition B 6
m = KeyPosition B 7
comma = KeyPosition B 8 -- OEM_COMMA
period = KeyPosition B 9 -- OEM_PERIOD
slash = KeyPosition B 10 -- OEM_2
rightShift = KeyPosition B 11

-- 1st row: system
controlLeft, superLeft, altLeft, space, altRight, superRight, menu, controlRight :: KeyPosition
controlLeft = KeyPosition A (-1)
superLeft = KeyPosition A 1
altLeft = KeyPosition A 2
space = KeyPosition A 3
altRight = KeyPosition A 8
superRight = KeyPosition A 9
menu = KeyPosition A 10
controlRight = KeyPosition A 11

-- Section: numeric ------------------------------------------------------------
kp0, kpDecimalSep, kpEnter :: KeyPosition
kp0 = KeyPosition A 51
kpDecimalSep = KeyPosition A 53
kpEnter = KeyPosition A 54

kp1, kp2, kp3 :: KeyPosition
kp1 = KeyPosition B 51
kp2 = KeyPosition B 52
kp3 = KeyPosition B 53

kp4, kp5, kp6, kpAdd :: KeyPosition
kp4 = KeyPosition C 51
kp5 = KeyPosition C 52
kp6 = KeyPosition C 53
kpAdd = KeyPosition C 54

kp7, kp8, kp9 :: KeyPosition
kp7 = KeyPosition D 51
kp8 = KeyPosition D 52
kp9 = KeyPosition D 53

kpNumLock, kpDivide, kpMultiply, kpSubtract :: KeyPosition
kpNumLock = KeyPosition E 51
kpDivide   = KeyPosition E 52
kpMultiply = KeyPosition E 53
kpSubtract = KeyPosition E 54
