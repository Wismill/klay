module Klay.Keyboard.Hardware.KeyPosition.Generic where

import Klay.Keyboard.Hardware.KeyPosition (KeyPosition(..), Row(..))


-- Alphanumeric section
e00, e01, e02, e03, e04, e05, e06, e07, e08, e09, e10, e11, e12, e13, e14 :: KeyPosition
e00 = KeyPosition E 0
e01 = KeyPosition E 1
e02 = KeyPosition E 2
e03 = KeyPosition E 3
e04 = KeyPosition E 4
e05 = KeyPosition E 5
e06 = KeyPosition E 6
e07 = KeyPosition E 7
e08 = KeyPosition E 8
e09 = KeyPosition E 9
e10 = KeyPosition E 10
e11 = KeyPosition E 11
e12 = KeyPosition E 12
e13 = KeyPosition E 13
e14 = KeyPosition E 14

d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14 :: KeyPosition
d00 = KeyPosition D 0
d01 = KeyPosition D 1
d02 = KeyPosition D 2
d03 = KeyPosition D 3
d04 = KeyPosition D 4
d05 = KeyPosition D 5
d06 = KeyPosition D 6
d07 = KeyPosition D 7
d08 = KeyPosition D 8
d09 = KeyPosition D 9
d10 = KeyPosition D 10
d11 = KeyPosition D 11
d12 = KeyPosition D 12
d13 = KeyPosition D 13
d14 = KeyPosition D 14

c00, c01, c02, c03, c04, c05, c06, c07, c08, c09, c10, c11, c12, c13, c14 :: KeyPosition
c00 = KeyPosition C 0
c01 = KeyPosition C 1
c02 = KeyPosition C 2
c03 = KeyPosition C 3
c04 = KeyPosition C 4
c05 = KeyPosition C 5
c06 = KeyPosition C 6
c07 = KeyPosition C 7
c08 = KeyPosition C 8
c09 = KeyPosition C 9
c10 = KeyPosition C 10
c11 = KeyPosition C 11
c12 = KeyPosition C 12
c13 = KeyPosition C 13
c14 = KeyPosition C 14

b99, b00, b01, b02, b03, b04, b05, b06, b07, b08, b09, b10, b11, b12, b13, b14 :: KeyPosition
b99 = KeyPosition B (-1)
b00 = KeyPosition B 0
b01 = KeyPosition B 1
b02 = KeyPosition B 2
b03 = KeyPosition B 3
b04 = KeyPosition B 4
b05 = KeyPosition B 5
b06 = KeyPosition B 6
b07 = KeyPosition B 7
b08 = KeyPosition B 8
b09 = KeyPosition B 9
b10 = KeyPosition B 10
b11 = KeyPosition B 11
b12 = KeyPosition B 12
b13 = KeyPosition B 13
b14 = KeyPosition B 14

a99, a00, a01, a02, a03, a04, a05, a06, a07, a08, a09, a10, a11, a12, a13, a14 :: KeyPosition
a99 = KeyPosition A (-1)
a00 = KeyPosition A 0
a01 = KeyPosition A 1
a02 = KeyPosition A 2
a03 = KeyPosition A 3
a04 = KeyPosition A 4
a05 = KeyPosition A 5
a06 = KeyPosition A 6
a07 = KeyPosition A 7
a08 = KeyPosition A 8
a09 = KeyPosition A 9
a10 = KeyPosition A 10
a11 = KeyPosition A 11
a12 = KeyPosition A 12
a13 = KeyPosition A 13
a14 = KeyPosition A 14

-- Numeric section
e51, e52, e53, e54 :: KeyPosition
e51 = KeyPosition E 51
e52 = KeyPosition E 52
e53 = KeyPosition E 53
e54 = KeyPosition E 54

d51, d52, d53, d54 :: KeyPosition
d51 = KeyPosition D 51
d52 = KeyPosition D 52
d53 = KeyPosition D 53
d54 = KeyPosition D 54

c51, c52, c53, c54 :: KeyPosition
c51 = KeyPosition C 51
c52 = KeyPosition C 52
c53 = KeyPosition C 53
c54 = KeyPosition C 54

b51, b52, b53, b54 :: KeyPosition
b51 = KeyPosition B 51
b52 = KeyPosition B 52
b53 = KeyPosition B 53
b54 = KeyPosition B 54

a51, a52, a53, a54 :: KeyPosition
a51 = KeyPosition A 51
a52 = KeyPosition A 52
a53 = KeyPosition A 53
a54 = KeyPosition A 54
