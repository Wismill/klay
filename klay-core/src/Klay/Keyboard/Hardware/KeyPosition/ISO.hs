{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Hardware.KeyPosition.ISO
  ( commonKeyPositions
  , keyPositions, keysMap, keyPositionsMap
  , fromKeyPosition, toKeyPosition
  , escape, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12
  , insert, delete, home, end, pageUp, pageDown
  , left, right, up, down
  , printScreen, scrollLock, pause
  , tilde, n1, n2, n3, n4, n5, n6, n7, n8, n9, n0, minus, equal, backspace
  , tabulator, q, w, e, r, t, y , u, i, o, p, openingSquareBracket, closingSquareBracket, enter
  , caps, a, s, d, f, g, h, j, k, l, semicolon, apostrophe, backslash
  , leftShift, iso_102, z, x, c, v, b, n, m, comma, period, slash, rightShift
  , controlLeft, superLeft, altLeft, space, altRight, superRight, menu, controlRight
  , kp0, kp1, kp2, kp3, kp4, kp5, kp6, kp7, kp8, kp9, kpDecimalSep
  , kpAdd, kpSubtract, kpDivide, kpMultiply, kpEnter, kpNumLock
  ) where

import Data.Tuple (swap)
import Data.Bifunctor (first)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.KeyPosition
import Klay.Keyboard.Hardware.KeyPosition.ANSI hiding (backslash, enter, toKeyPosition, fromKeyPosition, keysMap, keyPositionsMap, keyPositions, commonKeyPositions)

toKeyPosition :: Key -> Maybe KeyPosition
toKeyPosition = flip Map.lookup keyPositionsMap

fromKeyPosition :: KeyPosition -> Maybe Key
fromKeyPosition = flip Map.lookup keysMap

keysMap :: Map KeyPosition Key
keysMap = Map.fromList keyPositions

keyPositionsMap :: Map Key KeyPosition
keyPositionsMap = Map.fromList (fmap swap keyPositions)

keyPositions :: [(KeyPosition, Key)]
keyPositions = commonKeyPositions <>
  [ (KeyPosition D 13, K.Return)
  , (KeyPosition C 12, K.Backslash)
  , (KeyPosition B 0, K.Iso102) ]

mkRow :: Row -> Column -> [Key] -> [(KeyPosition, Key)]
mkRow row c0 = fmap (first (KeyPosition row)) . zip [c0..]

commonKeyPositions :: [(KeyPosition, Key)]
commonKeyPositions = mconcat
  [ -- Function keys
    mkRow K 1 [K.F1, K.F2, K.F3, K.F4, K.F5, K.F6, K.F7, K.F8, K.F9, K.F10, K.F11, K.F12]
    -- Number row
  , mkRow E 0 [K.Tilde, K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Plus, K.Backspace]
    -- Alphabetic: row 3
  , mkRow D 0 [K.Tabulator, K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket] -- K.Backslash
    -- Alphabetic: row 2
  , mkRow C 0 [K.CapsLock, K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote] -- K.Return
    -- Alphabetic: row 1
  , [(KeyPosition B (-1), K.LShift)]
  , mkRow B 1 [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash, K.RShift] -- K.Iso102
    -- System key
  , [(KeyPosition A (-1), K.LControl)]
  , mkRow A 1 [K.LSuper, K.LAlternate, K.Space]
  , mkRow A 8 [K.RAlternate, K.RSuper, K.Menu, K.RControl]
  ]

-- Section: alphanumeric ------------------------------------------------------

-- 4th row: letters
enter :: KeyPosition
enter = KeyPosition D 13

-- 3rd row: letters
backslash :: KeyPosition
backslash = KeyPosition C 12

-- 2nd row: letters
iso_102 :: KeyPosition
iso_102 = KeyPosition B 0
