{-|
Description: Keyboard physical layout
-}

module Klay.Keyboard.Hardware.Geometry
  ( BaseGeometry
  , Geometry(..)
  , Model(..)
  , FormFactor(..)
  , TypeMatrixMode(..)
  -- , ErgodoxMode(..)
  , toBaseGeometry
  , baseGeometryToString
  , geometryToString
  , typeMatrixModeToString
  ) where

import GHC.Generics (Generic)

import TextShow (TextShow)
import TextShow.Generic (FromGeneric(..))

data BaseGeometry
  = GABNT
  | GANSI
  | GISO
  | GJIS
  | GTypeMatrix
  -- GErgodox
  deriving (Generic, Eq, Enum, Bounded, Show, Read)

toBaseGeometry :: Geometry -> BaseGeometry
toBaseGeometry (ABNT       _) = GABNT
toBaseGeometry (ANSI       _) = GANSI
toBaseGeometry (ISO        _) = GISO
toBaseGeometry (JIS        _) = GJIS
toBaseGeometry (TypeMatrix _) = GTypeMatrix

baseGeometryToString :: BaseGeometry -> String
baseGeometryToString GANSI       = "ANSI"
baseGeometryToString GISO        = "ISO"
baseGeometryToString GJIS        = "JIS"
baseGeometryToString GABNT       = "ABNT"
baseGeometryToString GTypeMatrix = "TypeMatrix 2030"

-- | Keyboard models and their variations
data Geometry
  = ABNT FormFactor           -- ^ Brazilian keyboard with 107 keys (ABNT NBR&#xA0;10346 variant&#xA0;2 and 10347)
  | ANSI FormFactor           -- ^ US keyboard with 104 keys (ANSI-INCITS&#xA0;154-1988)
  | ISO  FormFactor           -- ^ European keyboard with 105 keys (ISO/IEC&#xA0;9995-2)
  | JIS  FormFactor           -- ^ Japanese keyboard with 109 keys (JIS&#xA0;X&#xA0;6002-1980)
  | TypeMatrix TypeMatrixMode -- ^ TypeMatrix 2030
  deriving (Generic, Eq, Show, Read)
  deriving TextShow via (FromGeneric Geometry)

instance Enum Geometry where
  toEnum n
    | n >= nAbntMin       && n <= nAbntMax       = ABNT (toEnum (n - nAbntMin))
    | n >= nAnsiMin       && n <= nAnsiMax       = ANSI (toEnum (n - nAnsiMin))
    | n >= nIsoMin        && n <= nIsoMax        = ISO (toEnum (n - nIsoMin))
    | n >= nJisMin        && n <= nJisMax        = JIS (toEnum (n - nJisMin))
    | n >= nTypematrixMin && n <= nTypematrixMax = TypeMatrix (toEnum (n - nTypematrixMin))
  toEnum n = error ("Out of bound toEnum Geometry: " <> show n)

  fromEnum (ABNT f)       = nAbntMin + fromEnum f
  fromEnum (ANSI f)       = nAnsiMin + fromEnum f
  fromEnum (ISO f)        = nIsoMin + fromEnum f
  fromEnum (JIS f)        = nJisMin + fromEnum f
  fromEnum (TypeMatrix m) = nTypematrixMin + fromEnum m

  enumFrom     x   = enumFromTo     x maxBound
  enumFromThen x y = enumFromThenTo x y bound
    where
      bound | fromEnum y >= fromEnum x = maxBound
            | otherwise                = minBound

nAnsiMin, nAnsiMax, nIsoMin, nIsoMax, nJisMin, nJisMax, nAbntMin, nAbntMax, nTypematrixMin, nTypematrixMax :: Int
nAbntMin = 0
nAbntMax = nAbntMin + fromEnum (maxBound :: FormFactor)
nAnsiMin = nAbntMax + 1
nAnsiMax = nAnsiMin + fromEnum (maxBound :: FormFactor)
nIsoMin = nAnsiMax + 1
nIsoMax = nIsoMin + fromEnum (maxBound :: FormFactor)
nJisMin = nIsoMax + 1
nJisMax = nJisMin + fromEnum (maxBound :: FormFactor)
nTypematrixMin = nJisMax + 1
nTypematrixMax = nTypematrixMin + fromEnum (maxBound :: TypeMatrixMode)

instance Bounded Geometry where
  minBound = ABNT minBound
  maxBound = TypeMatrix maxBound

-- [TODO] implement this parameter
-- | Model for standard keyboards
data Model
  = ModelM     -- ^ Standard [IBM Model M keyboard](https://en.wikipedia.org/wiki/Model_M_keyboard)
  -- [TODO] Laptops
  | AppleMagic -- ^ [Apple Magic keyboard](https://en.wikipedia.org/wiki/Magic_Keyboard)
  deriving (Generic, Eq, Enum, Bounded, Show, Read)
  deriving TextShow via (FromGeneric Model)

-- | Form factor for ISO&#xA0;105/ANSI&#xA0;104 keyboard.
data FormFactor
  = F60  -- ^ 60%: The alphanumeric section (62 keys for ISO 105)
  | F70  -- ^ 70%: The alphanumeric section and function keys (75 for ISO 105)
  | F80  -- ^ 80%: The alphanumeric section, function keys, editing and cursor keys (75 for ISO 105)
  | F100 -- ^ 100%: The complete keyboard
  deriving (Generic, Eq, Enum, Bounded, Show, Read)
  deriving TextShow via (FromGeneric FormFactor)

geometryToString :: Geometry -> String
geometryToString = \case
    ABNT       _ -> "ABNT"
    ANSI       _ -> "ANSI"
    ISO        _ -> "ISO"
    JIS        _ -> "JIS"
    TypeMatrix m -> "TypeMatrix 2030 " <> typeMatrixModeToString m

-- | The mode of the TypeMatrix 2030
data TypeMatrixMode
  = TM101     -- ^ Mode 101 (ANSI, default mode)
  | TMDvorak  -- ^ Mode US Dvorak
  | TM102     -- ^ Mode 102 (ISO)
  | TM106     -- ^ Mode 106 (Japanese)
  | TMMac     -- ^ Mode Mac
  | TMColemak -- ^ Mode US Colemak
  deriving (Generic, Eq, Enum, Bounded, Show, Read)
  deriving TextShow via (FromGeneric TypeMatrixMode)

typeMatrixModeToString :: TypeMatrixMode -> String
typeMatrixModeToString = drop 2 . show
