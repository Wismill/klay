{-|
Description: 'Key's aliases for /ISO&#xA0;105/ keyboards using ISO key position

This modules provides 'Key's aliases for /ISO&#xA0;10/5 keyboards
using key position numbering system from ISO/IEC&#xA0;9995-1, section&#xA0;7.
-}

module Klay.Keyboard.Hardware.Key.ISO
  ( Key
    ( -- Section: Editing-and-function
      K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K10, K11, K12
    , K30, K31, K32
    , E30, E31, E32
    , D30, D31, D32
    , B31
    , A30, A31, A32
      -- Section: alphanumeric
    , E00, E01, E02, E03, E04, E05, E06, E07, E08, E09, E10, E11, E12, E13
    , D00, D01, D02, D03, D04, D05, D06, D07, D08, D09, D10, D11, D12, D13
    , C00, C01, C02, C03, C04, C05, C06, C07, C08, C09, C10, C11, C12
    , B99, B00, B01, B02, B03, B04, B05, B06, B07, B08, B09, B10, B11
    , A99, A00, A01, A02, A08, A09, A10, A11
    -- Section: numeric
    , E51, E52, E53, E54
    , D51, D52, D53, D54
    , C51, C52, C53
    , B51, B52, B53, B54
    , A51, A53
    )
  , IsoKey(..)
  ) where

import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key.Types
import Klay.Keyboard.Hardware.Key.ANSI hiding (D13, C12)
import Klay.Keyboard.Hardware.KeyPosition
import Klay.Keyboard.Hardware.KeyPosition.ISO (keyPositionsMap)

-- | Alias for 'Iso102'
pattern B00 :: Key
pattern B00 = Iso102

-- | Alias for 'Return'
pattern D13 :: Key
pattern D13 = Return

-- | Alias for 'Backslash'
pattern C12 :: Key
pattern C12 = Backslash

newtype IsoKey = IsoKey {unIsoKey :: Key}
  deriving stock (Eq)

instance Ord IsoKey where
  (IsoKey k1) `compare` (IsoKey k2) =
    case (Map.lookup k1 keyPositionsMap, Map.lookup k2 keyPositionsMap) of
      (Nothing, Nothing) -> compare k1 k2
      (Just _ , Nothing) -> GT
      (Nothing, Just _ ) -> LT
      (Just (KeyPosition r1 c1), Just (KeyPosition r2 c2)) -> case compare r1 r2 of
        LT -> GT
        GT -> LT
        EQ -> compare c1 c2

