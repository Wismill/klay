module Klay.Keyboard.Hardware.Key.Types
  ( -- * Physical key action
    PhysicalKey, APhysicalKey(..)
  , HardwareAction, AHardwareAction(..)
  , BaseHardwareAction(..)
  , IsPhysicalKey(..)
  , IsHardwareAction(..)
    -- * Physical key identifier
  , Key
    ( .., FirstAlphanumericKey, SysReq, Tilde, HankakuZenkaku, Plus
    , Eisū, JPBackSlash, BrSlash, Hangul
    , KPBrDot, KPSeparator, Clear
    , LWindows, RWindows, LCommand, RCommand
    , AltGr, LOption, ROption)
  , KeyModCombo(..)
  , KeySequence
  , KeyNames
  , toBaseFunction
  ) where

-- [TODO] Re-enable Generic deriving of Key when the following issues are solved:
-- • https://gitlab.haskell.org/ghc/ghc/-/issues/5642
-- • https://gitlab.haskell.org/ghc/ghc/-/issues/16577

import Data.Ix (Ix(..))
import Data.Text qualified as T
import Data.List.NonEmpty (NonEmpty)
import Data.Map qualified as Map
import GHC.Generics (Generic)
import Control.DeepSeq (NFData(..))

import TextShow (TextShow(..), FromStringShow(..))
-- import TextShow.Generic (FromGeneric(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..)) --, genericShrink)
import Test.QuickCheck.Gen (chooseEnum)
import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as Aeson
import Data.Aeson.Types (ToJSON(..), ToJSONKey(..), FromJSON(..), FromJSONKey(..))

import Data.List.NonEmpty2 (NonEmpty2)
import Klay.Keyboard.Layout.Modifier (ModifiersCombo)
-- import Klay.Utils (jsonOptions, jsonKeyOptions)
import Klay.Utils.UserInput (RawLText)

-- | A physical key of a keyboard, that may be used in combination with the Fn meta-modifier.
type PhysicalKey = APhysicalKey HardwareAction

-- | A container for 'PhysicalKey'
data APhysicalKey a
  = SingleKey !a
  -- ^ A normal key, with no meta-mapping such as @Fn@.
  | FnDualKey
  -- ^ A key that can be used in combination with the meta-modifier @Fn@ key.
    !a -- ^ Base function
    !a -- ^ Action obtained with @Fn@ pressed
  deriving (Generic, NFData, Eq, Ord, Functor, Show)

-- | Action of a physical key of a keyboard
type HardwareAction = AHardwareAction BaseHardwareAction

-- | Container for 'HardwareAction'
data AHardwareAction a
  = OneKey !a
  -- ^ Normal key
  | KeySequence !(NonEmpty2 a)
  -- ^ A sequence of at least two keys
  | KeyCombo !a !a
  -- ^ Simultaneous keys
  deriving (Generic, NFData, Eq, Ord, Functor, Show)

-- | Base action of a physical key of a keyboard, that is either mappable or not by the OS.
data BaseHardwareAction
  = Mappable !Key
  -- ^ Normal key
  | NonMappable !T.Text
  -- ^ Special key that cannot be mapped, such as the @Fn@ key. The parameter is its label.
  deriving (Generic, NFData, Eq, Ord, Show)

-- | Class to facilitate the conversion to 'APhysicalKey'
class IsPhysicalKey k a | k -> a where
  mkPhysicalKey :: k -> APhysicalKey a

instance IsPhysicalKey (APhysicalKey HardwareAction) HardwareAction where
  mkPhysicalKey = id

instance (IsBaseHardwareAction k) => IsPhysicalKey (AHardwareAction k) HardwareAction where
  mkPhysicalKey = SingleKey . fmap mkBaseHardwareAction

instance IsPhysicalKey BaseHardwareAction HardwareAction where
  mkPhysicalKey = SingleKey . OneKey

instance (IsHardwareAction k1 BaseHardwareAction, IsHardwareAction k2 BaseHardwareAction) => IsPhysicalKey (k1, k2) HardwareAction where
  mkPhysicalKey (k1, k2) = FnDualKey (mkHardwareAction k1) (mkHardwareAction k2)

instance IsPhysicalKey T.Text HardwareAction where
  mkPhysicalKey = SingleKey . OneKey . mkBaseHardwareAction

instance IsPhysicalKey Key HardwareAction where
  mkPhysicalKey = SingleKey . OneKey . mkBaseHardwareAction

-- | Class to facilitate the conversion to 'AHardwareAction'.
class IsHardwareAction k a | k -> a where
  mkHardwareAction :: k -> AHardwareAction a

instance (IsBaseHardwareAction k) => IsHardwareAction (AHardwareAction k) BaseHardwareAction where
  mkHardwareAction = fmap mkBaseHardwareAction

instance IsHardwareAction BaseHardwareAction BaseHardwareAction where
  mkHardwareAction = OneKey

instance IsHardwareAction T.Text BaseHardwareAction where
  mkHardwareAction = OneKey . mkBaseHardwareAction

instance IsHardwareAction Key BaseHardwareAction where
  mkHardwareAction = OneKey . mkBaseHardwareAction

instance (IsBaseHardwareAction k1, IsBaseHardwareAction k2) => IsHardwareAction (k1, k2) BaseHardwareAction where
  mkHardwareAction (k1, k2) = KeyCombo (mkBaseHardwareAction k1) (mkBaseHardwareAction k2)

instance (IsBaseHardwareAction k) => IsHardwareAction (NonEmpty2 k) BaseHardwareAction where
  mkHardwareAction = KeySequence . fmap mkBaseHardwareAction

-- | Class to facilitate the conversion to 'BaseHardwareAction'.
class IsBaseHardwareAction a where
  mkBaseHardwareAction :: a -> BaseHardwareAction

instance IsBaseHardwareAction BaseHardwareAction where
  mkBaseHardwareAction = id

instance IsBaseHardwareAction T.Text where
  mkBaseHardwareAction = NonMappable

instance IsBaseHardwareAction Key where
  mkBaseHardwareAction = Mappable

toBaseFunction :: APhysicalKey a -> a
toBaseFunction (SingleKey a)   = a
toBaseFunction (FnDualKey a _) = a

-- [TODO] Missing keys:
-- Print (KEY_PRINT, 210)
-- Select (KEY_SELECT, 0x161)
-- Zoom (KEY_FULL_SCREEN/KEY_ZOOM, 0x174)

{-|
Identifier of a /supported/ mappable keyboard key. See the precise definition in "Klay.Keyboard.Hardware.Key".

The reference is the ISO&#xA0;105 layout and variants (see [IBM model&#xA0;M](https://en.wikipedia.org/wiki/IBM_PC_keyboard)).

The naming corresponds to the US ANSI layout when applicable.
Japanese keys from JIS&#xA0;106 are prefixed with \"JP\".
Brazilian keys from ABNT keyboard are prefixed with \"Br\".

The order of the keys is /loosely based/ on: ISO\/IEC&#xA0;9995-1, section&#xA0;5.2.
-}
data Key
  -- ================== Section: Editing-and-function =========================

  -- Zone: Editing function ---------------------------------------------------

  -- System keys
  = PrintScreen -- ^ __Print Screen \/ SysReq__
  | ScrollLock  -- ^ __Scroll lock__
  | Pause       -- ^ __Pause \/ Break__

  -- Function keys
  | F1 | F2 | F3 | F4 | F5 | F6 | F7 | F8 | F9 | F10 | F11 | F12
  | F13 | F14 | F15 | F16 | F17 | F18 | F19 | F20 | F21 | F22 | F23 | F24

  -- Movements
  | Home     -- ^ @⇱@
  | End      -- ^ @⇲@
  | PageUp   -- ^ @⎗@ \/ @⇞@
  | PageDown -- ^ @⎘@ \/ @⇟@

  -- Miscellaneous
  | Escape   -- ^ @⎋@

  -- Zone: Cursor key ---------------------------------------------------------
  | CursorLeft  -- ^ Left arrow key @◀@
  | CursorRight -- ^ Right arrrow key @▶@
  | CursorUp    -- ^ Up arrrow key @▲@
  | CursorDown  -- ^ Down arrrow key @▼@

  -- Zone: Optional -----------------------------------------------------------

  | ScreenLock
  | Stop | Cancel
  | Save

  -- Media control
  | MediaPlayCD | MediaPlay | MediaPause | MediaPlayPause | MediaStop | MediaEject
  | MediaPrevious | MediaNext | MediaRewind | MediaForward
  | VolumeMute | VolumeDown | VolumeUp

  -- Browser control
  | BrowserBack | BrowserForward | BrowserRefresh
  | BrowserSearch | BrowserFavorites | BrowserHomePage

  -- Applications
  | Calculator | MediaPlayer
  | Browser | Email1 | Email2
  | Find | Explorer
  | MyComputer | Documents | Help
  | Launch1 | Launch2 | Launch3 | Launch4

  -- Power keys
  | Power | Suspend | Sleep | WakeUp
  | BrightnessDown | BrightnessUp

  -- ================== Section: numeric keypad =============================
  -- [NOTE] Typical physical layout:
  -- NumLock KPDivide KPMultiply KPSubtract
  -- KP7     KP8      KP9        KPAdd
  -- KP4     KP5      KP6        KPComma
  -- KP1     KP2      KP3        KPEnter
  --         KP0      KPDecimal  KPEquals

  -- Zone: numeric zone -------------------------------------------------------
  | KP0         -- ^ Keypad number @0@
  | KP1         -- ^ Keypad number @1@
  | KP2         -- ^ Keypad number @2@
  | KP3         -- ^ Keypad number @3@
  | KP4         -- ^ Keypad number @4@
  | KP5         -- ^ Keypad number @5@
  | KP6         -- ^ Keypad number @6@
  | KP7         -- ^ Keypad number @7@
  | KP8         -- ^ Keypad number @8@
  | KP9         -- ^ Keypad number @9@
  | KPDecimal   -- ^ Keypad decimal separator @⎖@: @.@ on US keyboard and @,@ on European keyboards
  | KPComma     -- ^ Keypad @.@ on Brazilian ABNT2 keyboard (despite the name contains "comma")
  | KPJPComma   -- ^ Keypad @,@ on Japanese (Apple) keyboard
  | KPPlusMinus -- ^ Keypad @—@ on LK411 keyboard, which corresponds to 'KPAdd')

  -- Zone: function -----------------------------------------------------------
  -- [NOTE] NumLock is defined with the other modifiers
  | KPDivide   -- ^ Keypad @∕@
  | KPMultiply -- ^ Keypad @×@
  | KPSubtract -- ^ keypad @−@
  | KPAdd      -- ^ Keypad @+@
  | KPEnter    -- ^ Keypad __Enter__ @⌤@ \/ @⎆@
  | KPEquals   -- ^ Keypad @=@

  -- ================== Section: alphanumeric =================================

  -- Zone: function -----------------------------------------------------------

  -- Edition
  | Tabulator -- ^ @⭾@
  | Return    -- ^ @↵@
  | Backspace -- ^ @⌫@
  | Delete    -- ^ @⌦@
  | Insert    -- ^ @⎀@
  | Undo | Redo | Cut | Copy | Paste

  -- Miscellaneous
  | Menu             -- ^ Menu \/ Application @▤@

  -- International keys: Japanese
  | Muhenkan         -- ^ IME Muhenkan (non-conversion) __無変換__ (Scan code: @0x7B@)
  | Henkan           -- ^ IME Henkan (conversion) __変換__ (Scan code: @0x79@)
  | Katakana         -- ^ IME Katakana __カタカナ__ (Scan code: @[TODO]@)
  | Hiragana         -- ^ IME Hiragana __ひらがな__ (Scan code: @[TODO]@)
  | HiraganaKatakana -- ^ IME Hiragana\/Katakana\/rōmaji toggle __カタカナ \/ ひらがな \/ ローマ字__ (Scan code: @0x70@)

  -- International keys: Korean
  | Hangeul          -- ^ IME Hangeul\/Latin toggle __한 \/ 영__ (scan code: @[TODO]@)
  | Hanja            -- ^ IME Hangeul to Hanja conversion __한자__ (scan code: @[TODO]@)

  -- [TODO] ZenhakuHandaku. Not used in XKB. Mapped to 'Grave'

  -- Modifiers
  | LShift     -- ^ Left __Shift__ @⇧@
  | RShift     -- ^ Right __Shift__ @⇧@
  | CapsLock   -- ^ __CapsLock__ @⇬@
  | LControl   -- ^ Left __Control__ @⎈@
  | RControl   -- ^ Right __Control__ @⎈@
  | LAlternate -- ^ Left __Alt__ @⎇@ \/ __Option__ @⌥@
  | RAlternate -- ^ Right __Alt__ @⎇@ \/ __AltGr__ @⇮@ \/ __Option__ @⌥@
  | LSuper     -- ^ Left __Super__ \/ __Windows__ @❖@ \/ __Command__ @⌘@
  | RSuper     -- ^ Right __Super__ \/ __Windows__ @❖@ \/ __Command__ @⌘@
  | NumLock    -- ^ __NumLock__ @⇭@ on Windows and Linux, __Clear__ @⌧@ on Mac.

  -- ⁑⁑⁑⁑⁑⁑⁑⁑⁑ The following keys have no corresponding special actions ⁑⁑⁑⁑⁑⁑⁑

  -- Zone: alphanumeric -------------------------------------------------------

  -- Alphanumeric
  | Grave       -- ^ US label: @` ~@.
  | N0          -- ^ US label: @0 )@
  | N1          -- ^ US label: @1 !@
  | N2          -- ^ US label: @2 \@@
  | N3          -- ^ US label: @3 \#@
  | N4          -- ^ US label: @4 \$@
  | N5          -- ^ US label: @5 %@
  | N6          -- ^ US label: @6 ^@
  | N7          -- ^ US label: @7 &@
  | N8          -- ^ US label: @8 *@
  | N9          -- ^ US label: @9 (@
  | Minus       -- ^ US label: @- _@
  | Equals      -- ^ US label: @= +@
  | A | B | C | D | E | F | G | H | I | J | K | L | M | N | O | P | Q | R | S | T | U | V | W | X | Y | Z
  | LBracket    -- ^ US label: @[ {@
  | RBracket    -- ^ US label: @] }@
  | Backslash   -- ^ US label: @\\ |@
  | Semicolon   -- ^ US label: @; :@
  | Quote       -- ^ US label: @\' \"@
  | Comma       -- ^ US label: @, \<@
  | Period      -- ^ US label: @. \>@
  | Slash       -- ^ US label: @\/ ?@
  | Iso102      -- ^ European label: @\< \>@ (Scan code: @0x56@)
  | Yen         -- ^ Japanese label: @¥ |@ (Scan code: @0x7D@)
  | Ro          -- ^ Japanese label: @\\ _@ and Katakana RO (__ロ\/ろ__), Brazilian label: @/ ?@ (Scan code: @0x73@)
  | Space       -- ^ Space bar
  -- [TODO] Generic is too slow
  -- deriving (Generic, NFData, Eq, Ord, Bounded, Enum, Ix, Show)
  -- deriving TextShow via (FromGeneric Key)
  deriving (Eq, Ord, Bounded, Enum, Ix, Show, Read)
  deriving TextShow via (FromStringShow Key)

pattern FirstAlphanumericKey :: Key
pattern FirstAlphanumericKey = Grave

-- | Alias for 'PrintScreen'
pattern SysReq :: Key
pattern SysReq = PrintScreen

-- | Alias for 'Grave'
pattern Tilde :: Key
pattern Tilde = Grave

-- | Japanese halfwidth\/fullwidth\/kanji (hankaku\/zenkaku\/kanji __半角 \/ 全角 \/ 漢字__). Alias for 'Grave'.
pattern HankakuZenkaku :: Key
pattern HankakuZenkaku = Grave

-- | Alias for 'Hangeul'
pattern Hangul :: Key
pattern Hangul = Hangeul

-- | Alias for 'Equals'
pattern Plus :: Key
pattern Plus = Equals

-- | Japanese alphanumeric key __英数__. Alias for 'CapsLock'
pattern Eisū :: Key
pattern Eisū = CapsLock

-- | Japanese @\\ _@ key. Alias for 'Ro'
pattern JPBackSlash :: Key
pattern JPBackSlash = Ro

-- | Brazilian @\/ ?@ key. Alias for 'Ro'
pattern BrSlash :: Key
pattern BrSlash = Ro

-- | Brazilian @.@ keypad key. Alias for 'KPComma'.
pattern KPBrDot :: Key
pattern KPBrDot = KPComma

pattern KPSeparator :: Key
pattern KPSeparator = KPComma

-- | Mac __Clear__ @⌧@ key. Alias for 'NumLock' in Mac keyboards
pattern Clear :: Key
pattern Clear = NumLock

-- | Windows __Logo__ key. Alias for 'LSuper'
pattern LWindows :: Key
pattern LWindows = LSuper

-- | Windows __Logo__ key. Alias for 'RSuper'
pattern RWindows :: Key
pattern RWindows = RSuper

-- | Mac left __Command__ @⌘@ key. Alias for 'LSuper'
pattern LCommand :: Key
pattern LCommand = LSuper

-- | Mac right __Command__ @⌘@ key. Alias for 'RSuper'
pattern RCommand :: Key
pattern RCommand = RSuper

-- | Alias for 'RAlternate'
pattern AltGr :: Key
pattern AltGr = RAlternate

-- | Mac left __Option__ @⌥@ key. Alias for 'LAlternate'
pattern LOption :: Key
pattern LOption = LAlternate

-- | Mac right __Option__ @⌥@ key. Alias for 'RAlternate'
pattern ROption :: Key
pattern ROption = RAlternate

instance NFData Key where
  rnf = (`seq` ())

instance ToJSON Key where
  -- toJSON     = Aeson.genericToJSON (jsonOptions id)
  -- toEncoding = Aeson.genericToEncoding (jsonOptions id)
  toJSON     = Aeson.String . showt
  toEncoding = Aeson.toEncoding . showt

instance FromJSON Key where
  -- parseJSON = Aeson.genericParseJSON (jsonOptions id)
  parseJSON = Aeson.withText "key" $
    either fail pure . parseKey

instance ToJSONKey Key where
  -- toJSONKey = Aeson.genericToJSONKey (jsonKeyOptions id)
  toJSONKey = Aeson.toJSONKeyText showt

instance FromJSONKey Key where
  -- fromJSONKey = Aeson.genericFromJSONKey (jsonKeyOptions id)
  fromJSONKey = Aeson.FromJSONKeyTextParser $
    either fail pure . parseKey

parseKey :: T.Text -> Either String Key
parseKey t = let s = T.unpack t in case reads s of
    [(k, "")] -> Right k
    _         -> Left $ "Invalid virtual key: " <> s

instance Arbitrary Key where
  arbitrary = chooseEnum (minBound, maxBound)
  -- shrink = genericShrink

-- | A sequence of key combinations
type KeySequence = NonEmpty KeyModCombo

-- | A key + modifiers combination on a specific level
data KeyModCombo = KeyModCombo
  { _kModifiers :: !ModifiersCombo
    -- ^ Active modifiers
  , _kKey :: !Key
    -- ^ Key
  } deriving stock (Show, Eq, Ord)

-- | The names of the keys.
type KeyNames = Map.Map Key RawLText
