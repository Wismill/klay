{-|
Description: 'Key's aliases for /ANSI&#xA0;104/ keyboards using ISO key position

This modules provides 'Key's aliases for /ANSI&#xA0;104/ keyboards
using key position numbering system from ISO/IEC&#xA0;9995-1, section&#xA0;7.
-}

module Klay.Keyboard.Hardware.Key.ANSI
  ( Key
    ( -- Section: Editing-and-function
      K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K10, K11, K12
    , K30, K31, K32
    , E30, E31, E32
    , D30, D31, D32
    , B31
    , A30, A31, A32
      -- Section: alphanumeric
    , E00, E01, E02, E03, E04, E05, E06, E07, E08, E09, E10, E11, E12, E13
    , D00, D01, D02, D03, D04, D05, D06, D07, D08, D09, D10, D11, D12, D13
    , C00, C01, C02, C03, C04, C05, C06, C07, C08, C09, C10, C11, C12
    , B99, B01, B02, B03, B04, B05, B06, B07, B08, B09, B10, B11
    , A99, A00, A01, A02, A08, A09, A10, A11
    -- Section: numeric
    , E51, E52, E53, E54
    , D51, D52, D53, D54
    , C51, C52, C53
    , B51, B52, B53, B54
    , A51, A53
    )
  ) where

import Klay.Keyboard.Hardware.Key.Types

-- Section: Editing-and-function -----------------------------------------------

-- Function keys

-- | Alias for 'Escape'
pattern K00 :: Key
pattern K00 = Escape

-- | Alias for 'F1'
pattern K01 :: Key
pattern K01 = F1

-- | Alias for 'F2'
pattern K02 :: Key
pattern K02 = F2

-- | Alias for 'F3'
pattern K03 :: Key
pattern K03 = F3

-- | Alias for 'F4'
pattern K04 :: Key
pattern K04 = F4

-- | Alias for 'F5'
pattern K05 :: Key
pattern K05 = F5

-- | Alias for 'F6'
pattern K06 :: Key
pattern K06 = F6

-- | Alias for 'F7'
pattern K07 :: Key
pattern K07 = F7

-- | Alias for 'F8'
pattern K08 :: Key
pattern K08 = F8

-- | Alias for 'F9'
pattern K09 :: Key
pattern K09 = F9

-- | Alias for 'F10'
pattern K10 :: Key
pattern K10 = F10

-- | Alias for 'F11'
pattern K11 :: Key
pattern K11 = F11

-- | Alias for 'F12'
pattern K12 :: Key
pattern K12 = F12

-- Movements keys

-- | Alias for 'CursorLeft'
pattern A30 :: Key
pattern A30 = CursorLeft

-- | Alias for 'CursorRight'
pattern A32 :: Key
pattern A32 = CursorRight

-- | Alias for 'CursorDown'
pattern A31 :: Key
pattern A31 = CursorDown

-- | Alias for 'CursorUp'
pattern B31 :: Key
pattern B31 = CursorUp

-- Other functions

-- | Alias for 'Insert'
pattern E30 :: Key
pattern E30 = Insert

-- | Alias for 'Home'
pattern E31 :: Key
pattern E31 = Home

-- | Alias for 'PageUp'
pattern E32 :: Key
pattern E32 = PageUp

-- | Alias for 'Delete'
pattern D30 :: Key
pattern D30 = Delete

-- | Alias for 'End'
pattern D31 :: Key
pattern D31 = End

-- | Alias for 'PageDown'
pattern D32 :: Key
pattern D32 = PageDown

-- | Alias for 'PrintScreen'
pattern K30 :: Key
pattern K30 = PrintScreen

-- | Alias for 'ScrollLock'
pattern K31 :: Key
pattern K31 = ScrollLock

-- | Alias for 'Pause'
pattern K32 :: Key
pattern K32 = Pause


-- Section: alphanumeric -------------------------------------------------------

-- 5th row: numbers

-- | Alias for 'Tilde'
pattern E00 :: Key
pattern E00 = Tilde

-- | Alias for 'N1'
pattern E01 :: Key
pattern E01 = N1

-- | Alias for 'N2'
pattern E02 :: Key
pattern E02 = N2

-- | Alias for 'N3'
pattern E03 :: Key
pattern E03 = N3

-- | Alias for 'N4'
pattern E04 :: Key
pattern E04 = N4

-- | Alias for 'N5'
pattern E05 :: Key
pattern E05 = N5

-- | Alias for 'N6'
pattern E06 :: Key
pattern E06 = N6

-- | Alias for 'N7'
pattern E07 :: Key
pattern E07 = N7

-- | Alias for 'N8'
pattern E08 :: Key
pattern E08 = N8

-- | Alias for 'N9'
pattern E09 :: Key
pattern E09 = N9

-- | Alias for 'N0'
pattern E10 :: Key
pattern E10 = N0

-- | Alias for 'Minus'
pattern E11 :: Key
pattern E11 = Minus

-- | Alias for 'Equals'
pattern E12 :: Key
pattern E12 = Equals

-- | Alias for 'Backspace'
pattern E13 :: Key
pattern E13 = Backspace

-- 4th row: letters

-- | Alias for 'Tabulator'
pattern D00 :: Key
pattern D00 = Tabulator

-- | Alias for 'Q'
pattern D01 :: Key
pattern D01 = Q

-- | Alias for 'W'
pattern D02 :: Key
pattern D02 = W

-- | Alias for 'E'
pattern D03 :: Key
pattern D03 = E

-- | Alias for 'R'
pattern D04 :: Key
pattern D04 = R

-- | Alias for 'T'
pattern D05 :: Key
pattern D05 = T

-- | Alias for 'Y'
pattern D06 :: Key
pattern D06 = Y

-- | Alias for 'U'
pattern D07 :: Key
pattern D07 = U

-- | Alias for 'I'
pattern D08 :: Key
pattern D08 = I

-- | Alias for 'O'
pattern D09 :: Key
pattern D09 = O

-- | Alias for 'P'
pattern D10 :: Key
pattern D10 = P

-- | Alias for 'LBracket'
pattern D11 :: Key
pattern D11 = LBracket

-- | Alias for 'RBracket'
pattern D12 :: Key
pattern D12 = RBracket

-- | Alias for 'Backslash'
pattern D13 :: Key
pattern D13 = Backslash

-- 3rd row: letters

-- | Alias for 'CapsLock'
pattern C00 :: Key
pattern C00 = CapsLock

-- | Alias for 'A'
pattern C01 :: Key
pattern C01 = A

-- | Alias for 'S'
pattern C02 :: Key
pattern C02 = S

-- | Alias for 'D'
pattern C03 :: Key
pattern C03 = D

-- | Alias for 'F'
pattern C04 :: Key
pattern C04 = F

-- | Alias for 'G'
pattern C05 :: Key
pattern C05 = G

-- | Alias for 'H'
pattern C06 :: Key
pattern C06 = H

-- | Alias for 'J'
pattern C07 :: Key
pattern C07 = J

-- | Alias for 'K'
pattern C08 :: Key
pattern C08 = K

-- | Alias for 'L'
pattern C09 :: Key
pattern C09 = L

-- | Alias for 'Semicolon'
pattern C10 :: Key
pattern C10 = Semicolon

-- | Alias for 'Quote'
pattern C11 :: Key
pattern C11 = Quote

-- | Alias for 'Return'
pattern C12 :: Key
pattern C12 = Return

-- 2nd row: letters

-- | Alias for 'LShift'
pattern B99 :: Key
pattern B99 = LShift

-- | Alias for 'Z'
pattern B01 :: Key
pattern B01 = Z

-- | Alias for 'X'
pattern B02 :: Key
pattern B02 = X

-- | Alias for 'C'
pattern B03 :: Key
pattern B03 = C

-- | Alias for 'V'
pattern B04 :: Key
pattern B04 = V

-- | Alias for 'B'
pattern B05 :: Key
pattern B05 = B

-- | Alias for 'N'
pattern B06 :: Key
pattern B06 = N

-- | Alias for 'M'
pattern B07 :: Key
pattern B07 = M

-- | Alias for 'Comma'
pattern B08 :: Key
pattern B08 = Comma

-- | Alias for 'Period'
pattern B09 :: Key
pattern B09 = Period

-- | Alias for 'Slash'
pattern B10 :: Key
pattern B10 = Slash

-- | Alias for 'RShift'
pattern B11 :: Key
pattern B11 = RShift

-- 1st row: system

-- | Alias for 'LControl'
pattern A99 :: Key
pattern A99 = LControl

-- | Alias for 'LSuper'
pattern A00 :: Key
pattern A00 = LSuper

-- | Alias for 'LAlternate'
pattern A01 :: Key
pattern A01 = LAlternate

-- | Alias for 'Space'
pattern A02 :: Key
pattern A02 = Space

-- | Alias for 'RAlternate'
pattern A08 :: Key
pattern A08 = RAlternate

-- | Alias for 'RSuper'
pattern A09 :: Key
pattern A09 = RSuper

-- | Alias for 'Menu'
pattern A10 :: Key
pattern A10 = Menu

-- | Alias for 'RShift'
pattern A11 :: Key
pattern A11 = RShift


-- Section: numeric ------------------------------------------------------------

-- | Alias for 'NumLock'
pattern E51 :: Key
pattern E51 = NumLock

-- | Alias for 'KPDivide'
pattern E52 :: Key
pattern E52 = KPDivide

-- | Alias for 'KPMultiply'
pattern E53 :: Key
pattern E53 = KPMultiply

-- | Alias for 'KPSubtract'
pattern E54 :: Key
pattern E54 = KPSubtract

-- | Alias for 'KP7'
pattern D51 :: Key
pattern D51 = KP7

-- | Alias for 'KP8'
pattern D52 :: Key
pattern D52 = KP8

-- | Alias for 'KP9'
pattern D53 :: Key
pattern D53 = KP9

-- | Alias for 'KPAdd'
pattern D54 :: Key
pattern D54 = KPAdd

-- | Alias for 'KP4'
pattern C51 :: Key
pattern C51 = KP4

-- | Alias for 'KP5'
pattern C52 :: Key
pattern C52 = KP5

-- | Alias for 'KP6'
pattern C53 :: Key
pattern C53 = KP6

-- | Alias for 'KP1'
pattern B51 :: Key
pattern B51 = KP1

-- | Alias for 'KP2'
pattern B52 :: Key
pattern B52 = KP2

-- | Alias for 'KP3'
pattern B53 :: Key
pattern B53 = KP3

-- | Alias for 'KPEnter'
pattern B54 :: Key
pattern B54 = KPEnter

-- | Alias for 'KP0'
pattern A51 :: Key
pattern A51 = KP0

-- | Alias for 'KPDecimal'
pattern A53 :: Key
pattern A53 = KPDecimal
