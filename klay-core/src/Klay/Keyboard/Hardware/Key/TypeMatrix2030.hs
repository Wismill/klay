{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Hardware.Key.TypeMatrix2030
  ( BaseHardwareAction
    ( ..
    , FnKey
    , Num
    , Shuffle
    , Desktop
    )
  , tm2030Key
  ) where

import Klay.Keyboard.Hardware.Geometry (TypeMatrixMode(..))
import Klay.Keyboard.Hardware.Key.Types

pattern FnKey, Num, Shuffle, Desktop :: BaseHardwareAction
pattern FnKey   = NonMappable "Fn"
pattern Num     = NonMappable "Num"
pattern Shuffle = NonMappable "\xE23C" --    \xF5FD 
pattern Desktop = NonMappable "\xF044" -- 

pattern SimpleKey :: Key -> PhysicalKey
pattern SimpleKey k = SingleKey (OneKey (Mappable k))

pattern SimpleDualKey :: Key -> Key -> PhysicalKey
pattern SimpleDualKey k1 k2 = FnDualKey (OneKey (Mappable k1)) (OneKey (Mappable k2))

pattern ComboDualKey :: Key -> Key -> Key -> PhysicalKey
pattern ComboDualKey k1 k2 k3 = FnDualKey (OneKey (Mappable k1)) (KeyCombo (Mappable k2) (Mappable k3))

tm2030Key :: BaseHardwareAction -> TypeMatrixMode -> PhysicalKey
tm2030Key (Mappable Minus) = \case
  TMDvorak  -> SimpleKey LBracket
  _         -> SimpleKey Minus
tm2030Key (Mappable Equals) = \case
  TMDvorak  -> SimpleKey RBracket
  _         -> SimpleKey Equals
tm2030Key (Mappable Q) = \case
  TMDvorak  -> SimpleKey Quote
  _         -> SimpleKey Q
tm2030Key (Mappable W) = \case
  TMDvorak  -> SimpleKey Comma
  _         -> SimpleKey W
tm2030Key (Mappable E) = \case
  TMDvorak  -> SimpleKey Period
  TMColemak -> SimpleKey F
  _         -> SimpleKey E
tm2030Key (Mappable R) = \case
  TMDvorak  -> SimpleKey P
  TMColemak -> SimpleKey P
  _         -> SimpleKey R
tm2030Key (Mappable T) = \case
  TMDvorak  -> SimpleKey Y
  TMColemak -> SimpleKey G
  _         -> SimpleKey T
tm2030Key (Mappable Y) = \case
  TMDvorak  -> SimpleKey F
  TMColemak -> SimpleKey J
  _         -> SimpleKey Y
tm2030Key (Mappable U) = \case
  TMDvorak  -> SimpleKey G
  TMColemak -> SimpleKey L
  _         -> SimpleKey U
tm2030Key (Mappable I) = \case
  TMDvorak  -> SimpleKey C
  TMColemak -> SimpleKey U
  _         -> SimpleKey I
tm2030Key (Mappable O) = \case
  TMDvorak  -> SimpleDualKey R Tabulator
  TMColemak -> SimpleDualKey Y Tabulator
  _         -> SimpleDualKey O Tabulator
tm2030Key (Mappable P) = \case
  TMDvorak  -> SimpleDualKey L         KPDivide
  TMColemak -> SimpleDualKey Semicolon KPDivide
  _         -> SimpleDualKey P         KPDivide
tm2030Key (Mappable LBracket) = \case
  TMDvorak  -> SimpleDualKey Slash    KPMultiply
  _         -> SimpleDualKey LBracket KPMultiply
tm2030Key (Mappable RBracket) = \case
  TMDvorak  -> SimpleDualKey Equals   KPSubtract
  _         -> SimpleDualKey RBracket KPSubtract
tm2030Key (Mappable S) = \case
  TMDvorak  -> SimpleKey O
  TMColemak -> SimpleKey R
  _         -> SimpleKey S
tm2030Key (Mappable D) = \case
  TMDvorak  -> SimpleKey E
  TMColemak -> SimpleKey S
  _         -> SimpleKey D
tm2030Key (Mappable F) = \case
  TMDvorak  -> SimpleKey U
  TMColemak -> SimpleKey T
  _         -> SimpleKey F
tm2030Key (Mappable G) = \case
  TMDvorak  -> SimpleKey I
  TMColemak -> SimpleKey D
  _         -> SimpleKey G
tm2030Key (Mappable H) = \case
  TMDvorak  -> SimpleDualKey D Home
  _         -> SimpleDualKey H Home
tm2030Key (Mappable J) = \case
  TMDvorak  -> SimpleDualKey H CursorUp
  TMColemak -> SimpleDualKey N CursorUp
  _         -> SimpleDualKey J CursorUp
tm2030Key (Mappable K) = \case
  TMDvorak  -> SimpleDualKey T End
  TMColemak -> SimpleDualKey E End
  _         -> SimpleDualKey K End
tm2030Key (Mappable L) = \case
  TMDvorak  -> SimpleDualKey N KP7
  TMColemak -> SimpleDualKey I KP7
  _         -> SimpleDualKey L KP7
tm2030Key (Mappable Semicolon) = \case
  TMDvorak  -> SimpleDualKey S         KP8
  TMColemak -> SimpleDualKey O         KP8
  _         -> SimpleDualKey Semicolon KP8
tm2030Key (Mappable Quote) = \case
  TMDvorak  -> SimpleDualKey Minus KP9
  _         -> SimpleDualKey Quote KP9
tm2030Key (Mappable RShift) = const $ SimpleDualKey RShift KPAdd
tm2030Key (Mappable Z) = \case
  TMDvorak  -> SimpleKey Semicolon
  _         -> SimpleKey Z
tm2030Key (Mappable X) = \case
  TMDvorak  -> SimpleKey Q
  _         -> SimpleKey X
tm2030Key (Mappable C) = \case
  TMDvorak  -> SimpleKey J
  _         -> SimpleKey C
tm2030Key (Mappable V) = \case
  TMDvorak  -> SimpleKey K
  _         -> SimpleKey V
tm2030Key (Mappable B) = \case
  TMDvorak  -> SimpleKey X
  _         -> SimpleKey B
tm2030Key (Mappable N) = \case
  TMDvorak  -> SimpleDualKey B CursorLeft
  TMColemak -> SimpleDualKey K CursorLeft
  _         -> SimpleDualKey N CursorLeft
tm2030Key (Mappable M) = const $ SimpleDualKey M CursorDown
tm2030Key (Mappable Comma) = \case
  TMDvorak  -> SimpleDualKey W     CursorRight
  _         -> SimpleDualKey Comma CursorRight
tm2030Key (Mappable Period) = \case
  TMDvorak  -> SimpleDualKey V      KP4
  _         -> SimpleDualKey Period KP4
tm2030Key (Mappable Slash) = \case
  TMDvorak  -> SimpleDualKey Z     KP5
  _         -> SimpleDualKey Slash KP5
tm2030Key (Mappable Backslash) = \case
  TM106 -> SimpleDualKey JPBackSlash KP6
  _     -> SimpleDualKey Backslash   KP6
tm2030Key (Mappable Delete) = const $ SimpleDualKey Delete Insert
tm2030Key (Mappable Backspace) = const $ SimpleDualKey Backspace VolumeUp
tm2030Key (Mappable Return) = const $ SimpleDualKey Return VolumeDown
tm2030Key (Mappable MediaPlayPause) = \case
  TM102 -> ComboDualKey Iso102         LShift Delete
  _     -> ComboDualKey MediaPlayPause LShift Delete
tm2030Key (Mappable Menu) = const $ ComboDualKey Menu LControl Insert
tm2030Key Shuffle = \case
  TM106  -> FnDualKey (OneKey (Mappable Muhenkan)) (KeyCombo (Mappable LShift) (Mappable Insert))
  _      -> FnDualKey (OneKey Shuffle            ) (KeyCombo (Mappable LShift) (Mappable Insert))
tm2030Key Desktop = \case
  TM106  -> FnDualKey (OneKey (Mappable Henkan)) (OneKey (Mappable MediaPrevious))
  _      -> FnDualKey (OneKey Desktop)           (OneKey (Mappable MediaPrevious))
tm2030Key (Mappable LSuper) = \case
  TMMac  -> SimpleKey LAlternate
  _      -> SimpleKey LSuper
tm2030Key (Mappable LAlternate) = \case
  TMMac  -> SimpleKey LSuper
  _      -> SimpleKey LAlternate
tm2030Key (Mappable Calculator) = \case
  TM106  -> SimpleKey Yen
  _      -> SimpleKey Calculator
tm2030Key (Mappable Email1) = \case
  TM106  -> ComboDualKey Backslash RShift Tabulator
  _      -> ComboDualKey Email1    RShift Tabulator
tm2030Key (Mappable CapsLock) = const $ SimpleDualKey CapsLock Escape
tm2030Key (Mappable Browser) = \case
  TM106  -> SimpleDualKey HiraganaKatakana Backspace
  _      -> SimpleDualKey Browser          Backspace
tm2030Key (Mappable Home) = const $ SimpleDualKey Home KP1
tm2030Key (Mappable CursorUp) = const $ SimpleDualKey CursorUp KP2
tm2030Key (Mappable End) = const $ SimpleDualKey End KP3
tm2030Key (Mappable RControl) = const $ SimpleDualKey RControl KPEnter
tm2030Key (Mappable RAlternate) = const $ SimpleDualKey RAlternate MediaNext
tm2030Key (Mappable CursorLeft) = const $ SimpleDualKey CursorLeft KP0
tm2030Key (Mappable CursorDown) = const $ FnDualKey (OneKey (Mappable CursorDown)) (KeySequence [Mappable KP0, Mappable KP0])
tm2030Key (Mappable CursorRight) = const $ SimpleDualKey CursorRight KPDecimal
tm2030Key (Mappable PageUp) = const $ SimpleDualKey PageUp BrowserBack
tm2030Key (Mappable PageDown) = const $ SimpleDualKey PageDown BrowserForward
tm2030Key (Mappable Space) = const $ SimpleDualKey Space VolumeMute
tm2030Key key = const (SingleKey . OneKey $ key)
