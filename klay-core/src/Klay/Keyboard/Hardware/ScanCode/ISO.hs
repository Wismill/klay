module Klay.Keyboard.Hardware.ScanCode.ISO
  ( escape, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12
  , insert, delete, home, end, pageUp, pageDown
  , left, right, up, down
  , printScreen, scrollLock, pause
  , tilde, n1, n2, n3, n4, n5, n6, n7, n8, n9, n0, minus, equal, backspace
  , tabulator, q, w, e, r, t, y , u, i, o, p, openingSquareBracket, closingSquareBracket, enter
  , caps, a, s, d, f, g, h, j, k, l, semicolon, apostrophe, backslash
  , leftShift, lowerThan, z, x, c, v, b, n, m, comma, period, slash, rightShift
  , controlLeft, superLeft, altLeft, space, altRight, superRight, menu, controlRight
  , kp0, kp1, kp2, kp3, kp4, kp5, kp6, kp7, kp8, kp9, kpDecimalSep
  , kpAdd, kpSubstract, kpDivide, kpMultiply, kpEnter, kpNumLock
  -- Re-export
  , ScanCode(..)
  ) where


import Klay.Keyboard.Hardware.ScanCode

-- [NOTE] Scan codes are from set 1.

-- Section: Editing-and-function -----------------------------------------------
-- Function keys
escape, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12 :: ScanCode
escape = ScanCode 0x01
f1  = ScanCode 0x3b
f2  = ScanCode 0x3c
f3  = ScanCode 0x3d
f4  = ScanCode 0x3e
f5  = ScanCode 0x3f
f6  = ScanCode 0x40
f7  = ScanCode 0x41
f8  = ScanCode 0x42
f9  = ScanCode 0x43
f10 = ScanCode 0x44
f11 = ScanCode 0x57
f12 = ScanCode 0x58

-- Movements keys
left, right, up, down :: ScanCode
left = ScanCode 0xe04b
right = ScanCode 0xe04d
up = ScanCode 0xe048
down = ScanCode 0xe050

-- Other function
insert, delete, home, end, pageUp, pageDown :: ScanCode
insert = ScanCode 0xe052
home = ScanCode 0xe047
pageUp = ScanCode 0xe049

delete = ScanCode 0xe053
end = ScanCode 0xe04f
pageDown = ScanCode 0xe051

printScreen, scrollLock, pause :: ScanCode
printScreen = ScanCode 0xE02AE037
scrollLock = ScanCode 0x46
pause = ScanCode 0xE11D45E19DC5

-- Section: alphanumeric -------------------------------------------------------

-- 5th row: numbers
tilde, n1, n2, n3, n4, n5, n6, n7, n8, n9, n0, minus, equal, backspace :: ScanCode
tilde = ScanCode 0x29
n1 = ScanCode 0x02
n2 = ScanCode 0x03
n3 = ScanCode 0x04
n4 = ScanCode 0x05
n5 = ScanCode 0x06
n6 = ScanCode 0x07
n7 = ScanCode 0x08
n8 = ScanCode 0x09
n9 = ScanCode 0x0a
n0 = ScanCode 0x0b
minus = ScanCode 0x0c
equal = ScanCode 0x0d
backspace = ScanCode 0x0e

-- 4th row: letters
tabulator, q, w, e, r, t, y , u, i, o, p :: ScanCode
tabulator = ScanCode 0x0f
q = ScanCode 0x10
w = ScanCode 0x11
e = ScanCode 0x12
r = ScanCode 0x13
t = ScanCode 0x14
y = ScanCode 0x15
u = ScanCode 0x16
i = ScanCode 0x17
o = ScanCode 0x18
p = ScanCode 0x19
openingSquareBracket, closingSquareBracket, enter :: ScanCode
openingSquareBracket = ScanCode 0x1a -- OEM_4
closingSquareBracket = ScanCode 0x1b -- OEM_6
backslash = ScanCode 0x2b -- OEM_5

-- 3rd row: letters
caps, a, s, d, f, g, h, j, k, l, semicolon, apostrophe, backslash :: ScanCode
caps = ScanCode 0x3a
a = ScanCode 0x1e
s = ScanCode 0x1f
d = ScanCode 0x20
f = ScanCode 0x21
g = ScanCode 0x22
h = ScanCode 0x23
j = ScanCode 0x24
k = ScanCode 0x25
l = ScanCode 0x26
semicolon = ScanCode 0x27 -- OEM_1
apostrophe = ScanCode 0x28 -- OEM_7
enter = ScanCode 0x1c

-- 2nd row: letters
leftShift, lowerThan, z, x, c, v, b, n, m, comma, period, slash, rightShift :: ScanCode
leftShift = ScanCode 0x2a
lowerThan = ScanCode 0x56
z = ScanCode 0x2c
x = ScanCode 0x2d
c = ScanCode 0x2e
v = ScanCode 0x2f
b = ScanCode 0x30
n = ScanCode 0x31
m = ScanCode 0x32
comma = ScanCode 0x33 -- OEM_COMMA
period = ScanCode 0x34 -- OEM_PERIOD
slash = ScanCode 0x35 -- OEM_2
rightShift = ScanCode 0x36

-- 1st row: system
controlLeft, superLeft, altLeft, space, altRight, superRight, menu, controlRight :: ScanCode
controlLeft = ScanCode 0x1d
superLeft = ScanCode 0xe05b
altLeft = ScanCode 0x38
space = ScanCode 0x39
altRight = ScanCode 0xe038
superRight = ScanCode 0xe05c
menu = ScanCode 0xe05d
controlRight = ScanCode 0xe01d

-- Section: numeric ------------------------------------------------------------
kp0, kpDecimalSep, kpEnter :: ScanCode
kp0 = ScanCode 0x52
kpDecimalSep = ScanCode 0x53
kpEnter = ScanCode 0xe01c

kp1, kp2, kp3 :: ScanCode
kp1 = ScanCode 0x4f
kp2 = ScanCode 0x50
kp3 = ScanCode 0x51

kp4, kp5, kp6, kpAdd :: ScanCode
kp4 = ScanCode 0x4b
kp5 = ScanCode 0x4c
kp6 = ScanCode 0x4d
kpAdd = ScanCode 0x4e

kp7, kp8, kp9 :: ScanCode
kp7 = ScanCode 0x47
kp8 = ScanCode 0x48
kp9 = ScanCode 0x49

kpNumLock, kpDivide, kpMultiply, kpSubstract :: ScanCode
kpNumLock = ScanCode 0x45
kpDivide = ScanCode 0xe036
kpMultiply = ScanCode 0x37
kpSubstract = ScanCode 0x4a
