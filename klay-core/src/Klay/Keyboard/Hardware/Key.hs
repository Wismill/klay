{-# LANGUAGE OverloadedLists #-}

{-|
Description: Modelling of physical keyboard keys
-}

module Klay.Keyboard.Hardware.Key
  ( -- * Physical key action
    PhysicalKey, APhysicalKey(..)
  , HardwareAction, AHardwareAction(..)
  , BaseHardwareAction(..)
  , IsPhysicalKey(..)
  , IsHardwareAction(..)
  , toBaseFunction
    -- * Physical key
  , Key(..)
  , KeyModCombo(..)
  , KeySequence
    -- ** Categories
    -- *** Alphanumeric
  , keyGroupAlphanumericIso105
  , keyGroupLetters
  , keyGroupNumerals
  , isAlphanumeric
  , isAlphanumericIso105
    -- *** Modifiers
  , keyGroupModifiers
    -- *** System
  , keyGroupNonAlphanumeric
    -- *** Numpad
  , keyGroupNumpad
  , keyGroupNumpadDigits
  , isKeypadDigit
    -- ** Conversion
    -- *** Conversion /to/ 'Char'
  , keyToChar
  , keyLetterToChar
  , keyPunctuationToChar
  , keyDecimalToChar
  , keyHexadecimalToChar
    -- *** Conversion /from/ 'Char'
  , charToKey
  , charLetterToKey
  , nonPunctuationCharToKey
  , charPunctuationToKey1
  , charPunctuationToKey2
  , charDecimalToKey
  , charSystemToKey
    -- *** Conversion to 'Int'
  , keyDecimalToInt
  , keyHexadecimalToInt
    -- *** Conversion to 'Keycode'
  , keyToKeycode
  , keyToKeycodeMap
  , keyToKeycodeAssoc
    -- * Miscellaneous
  , sequenceDifficulty
  ) where


import Data.Char (intToDigit, chr, ord)
import Data.Ix (Ix(..))
import Data.Set qualified as Set
import Data.Foldable (foldl')
import Data.Map.Strict qualified as Map
import Control.Applicative ((<|>))

import Klay.Keyboard.Hardware.Key.Types
import Klay.Keyboard.Hardware.Keycode
import Klay.Keyboard.Layout.Modifier (ModifiersCombo)


--- Categories ----------------------------------------------------------------

-- | List of the keys corresponding to a letter in the US layout.
keyGroupLetters :: [Key]
keyGroupLetters = range (A, Z)

-- | List of the keys corresponding to a digit in the alphanumeric section of the US layout.
keyGroupNumerals :: [Key]
keyGroupNumerals = range (N0, N9)

-- | List of the keys corresponding to the the alphanumeric section of the US layout.
keyGroupAlphanumericIso105 :: [Key]
keyGroupAlphanumericIso105 = Space : range (FirstAlphanumericKey, Iso102)

-- | Check if the given key is alphanumeric (numeric keypad not included).
isAlphanumeric :: Key -> Bool
isAlphanumeric = (>= FirstAlphanumericKey)

-- | Check if the given key is alphanumeric (numeric keypad not included) in the US layout.
isAlphanumericIso105 :: Key -> Bool
isAlphanumericIso105 Space = True
isAlphanumericIso105 key = inRange (FirstAlphanumericKey, Iso102) key

-- | List of the keys corresponding to a digit in the numpad section of the US layout.
keyGroupNumpad :: [Key]
keyGroupNumpad = range (KP0, KPEquals)

-- | List of the keys corresponding to a digit in the numpad section of the US layout.
keyGroupNumpadDigits :: [Key]
keyGroupNumpadDigits = range (KP0, KP9)

-- | Check if the given key is a digit of the numeric keypad
isKeypadDigit :: Key -> Bool
isKeypadDigit = inRange (KP0, KPEquals)

-- | List of the keys not in the alphanumeric section of the US layout.
keyGroupNonAlphanumeric :: [Key]
keyGroupNonAlphanumeric = range (minBound, pred FirstAlphanumericKey)

-- | List of the keys that are modifiers
keyGroupModifiers :: [Key]
keyGroupModifiers = range (LShift, NumLock)


--- Conversions: key to character ---------------------------------------------

-- | Get the character corresponding to a key in the US layout, if applicable.
keyToChar :: Key -> Maybe Char
keyToChar key
  =   keyLetterToChar key
  <|> keyPunctuationToChar key
  <|> keyDecimalToChar key

-- | Get the letter corresponding to a key in the US layout, if applicable.
keyLetterToChar :: Key -> Maybe Char
keyLetterToChar key
  | inRange (A, Z) key = Just . chr $ fromEnum key - fromEnum A + ord 'A'
  | otherwise          = Nothing

-- | Get the punctuation symbol corresponding to a key in the US layout, if applicable.
keyPunctuationToChar :: Key -> Maybe Char
keyPunctuationToChar Space     = Just '\x0020'
keyPunctuationToChar Minus     = Just '-'
keyPunctuationToChar Comma     = Just ','
keyPunctuationToChar Semicolon = Just ';'
keyPunctuationToChar Period    = Just '.'
keyPunctuationToChar Quote     = Just '\''
keyPunctuationToChar LBracket  = Just '['
keyPunctuationToChar RBracket  = Just ']'
keyPunctuationToChar Slash     = Just '/'
keyPunctuationToChar Backslash = Just '\\'
keyPunctuationToChar _         = Nothing

-- | Get the decimal digit (as a 'Char') corresponding to a key in the US layout, if applicable.
keyDecimalToChar :: Key -> Maybe Char
keyDecimalToChar = fmap intToDigit . keyDecimalToInt

-- | Get the decimal digit (as an 'Int') corresponding to a key in the US layout, if applicable.
keyDecimalToInt :: Key -> Maybe Int
keyDecimalToInt key
  | inRange (N0,  N9)  key = Just $ fromEnum key - fromEnum N0
  | inRange (KP0, KP9) key = Just $ fromEnum key - fromEnum KP0
  | otherwise              = Nothing

-- | Get the hexadecimal digit (as an 'Int') corresponding to a key in the US layout, if applicable.
keyHexadecimalToInt :: Key -> Maybe Int
keyHexadecimalToInt key
  | inRange (N0,  N9)  key = Just $ fromEnum key - fromEnum N0
  | inRange (KP0, KP9) key = Just $ fromEnum key - fromEnum KP0
  | inRange (A,   F)   key = Just $ fromEnum key - fromEnum A + 0xA
  | otherwise              = Nothing

-- | Get the hexadecimal digit (as a 'Char') corresponding to a key in the US layout, if applicable.
keyHexadecimalToChar :: Key -> Maybe Char
keyHexadecimalToChar = fmap intToDigit . keyHexadecimalToInt


--- Conversions: character to key ---------------------------------------------

-- | Get the key corresponding to a character in the US layout, if applicable.
charToKey :: Char -> Maybe Key
charToKey c
  =   charLetterToKey c
  <|> charDecimalToKey c
  <|> charPunctuationToKey1 c
  <|> charPunctuationToKey2 c
  <|> charSystemToKey c

-- | Get the key corresponding to a non-punctuation character (but including space) in the US layout, if applicable.
nonPunctuationCharToKey :: Char -> Maybe Key
nonPunctuationCharToKey '\x20' = Just Space
nonPunctuationCharToKey c
  =   charLetterToKey c
  <|> charDecimalToKey c
  <|> charSystemToKey c

-- [TODO] another function for other alphabets\/variants
-- | Get the key corresponding to a letter in the US layout, if applicable.
charLetterToKey :: Char -> Maybe Key
charLetterToKey c
  | inRange ('A', 'Z') c = Just . toEnum . (+ fromEnum A) $ fromEnum c - fromEnum 'A'
  | inRange ('a', 'z') c = Just . toEnum . (+ fromEnum A) $ fromEnum c - fromEnum 'a'
  | otherwise            = Nothing

-- | Get the key corresponding to a punctuation symbol
-- in the /base level/ of the US layout, if applicable.
charPunctuationToKey1 :: Char -> Maybe Key
charPunctuationToKey1 '`'    = Just Grave
charPunctuationToKey1 '='    = Just Equals
charPunctuationToKey1 '-'    = Just Minus
charPunctuationToKey1 '['    = Just LBracket
charPunctuationToKey1 ']'    = Just RBracket
charPunctuationToKey1 ';'    = Just Semicolon
charPunctuationToKey1 '\''   = Just Quote
charPunctuationToKey1 '\\'   = Just Backslash
charPunctuationToKey1 ','    = Just Comma
charPunctuationToKey1 '.'    = Just Period
charPunctuationToKey1 '/'    = Just Slash
charPunctuationToKey1 '\x20' = Just Space
charPunctuationToKey1 _      = Nothing

-- | Get the key corresponding to a punctuation symbol
-- in the /second level/ of the US layout, if applicable.
charPunctuationToKey2 :: Char -> Maybe Key
charPunctuationToKey2 '~'    = Just Grave
charPunctuationToKey2 '+'    = Just Equals
charPunctuationToKey2 '_'    = Just Minus
charPunctuationToKey2 '{'    = Just LBracket
charPunctuationToKey2 '}'    = Just RBracket
charPunctuationToKey2 ':'    = Just Semicolon
charPunctuationToKey2 '"'    = Just Quote
charPunctuationToKey2 '|'    = Just Backslash
charPunctuationToKey2 '<'    = Just Comma
charPunctuationToKey2 '>'    = Just Period
charPunctuationToKey2 '?'    = Just Slash
charPunctuationToKey2 '\x20' = Just Space
charPunctuationToKey2 _      = Nothing

-- [TODO] another function for other numeric systems/encoding (full width, etc.)
-- | Get the key corresponding to a decimal digit (as a 'Char') in the US layout, if applicable.
charDecimalToKey :: Char -> Maybe Key
charDecimalToKey '0' = Just N0
charDecimalToKey '1' = Just N1
charDecimalToKey '2' = Just N2
charDecimalToKey '3' = Just N3
charDecimalToKey '4' = Just N4
charDecimalToKey '5' = Just N5
charDecimalToKey '6' = Just N6
charDecimalToKey '7' = Just N7
charDecimalToKey '8' = Just N8
charDecimalToKey '9' = Just N9
charDecimalToKey _   = Nothing

charSystemToKey :: Char -> Maybe Key
charSystemToKey '\n' = Just Return
charSystemToKey '\r' = Just Return
charSystemToKey '\b' = Just Backspace
charSystemToKey '\t' = Just Tabulator
charSystemToKey _    = Nothing

-- | Compute the difficulty of a key sequence.
sequenceDifficulty :: KeySequence -> Int
sequenceDifficulty combos = fst $ foldl' go (0, mempty) combos
  where
    go :: (Int, ModifiersCombo) -> KeyModCombo -> (Int, ModifiersCombo)
    go (score, ms) KeyModCombo{_kModifiers=ms'} = (score + penalty + modifiers_score + 1, ms')
      where
        modifiers_score
          | null ms' = 0
          | otherwise = (2 *) . length $ ms'
        -- Penalty in case previous modifiers were different
        penalty
          | null ms || null ms' || ms == ms' = 0
          | otherwise                        = length $ Set.difference ms ms' <> Set.difference ms' ms

-- | Convert a 'Key' to its corresponding Linux 'Keycode'
keyToKeycode :: Key -> Keycode
keyToKeycode = (keyToKeycodeMap Map.!) -- we know it is total

keyToKeycodeMap :: Map.Map Key Keycode
keyToKeycodeMap
  = Map.fromList
  . fmap (\(k, kc, _) -> (k, kc))
  $ keyToKeycodeAssoc

{-|
Map of the keys to their [Linux keycodes](https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h)
equivalent (text and number).

These codes are converted from the [USB HID](https://www.usb.org/sites/default/files/hut1_12.pdf)
usage codes using the conversion table `usb_kbd_keycode` in
in the [USB HIDBP Keyboard support](https://github.com/torvalds/linux/blob/master/drivers/hid/usbhid/usbkbd.c).
-}
keyToKeycodeAssoc :: [(Key, Keycode, InputEventCode)]
keyToKeycodeAssoc =
  [ (PrintScreen, KEY_SYSRQ, 99)
  , (ScrollLock, KEY_SCROLLLOCK, 70)
  , (Pause, KEY_PAUSE, 119)

  , (F1, KEY_F1, 59)
  , (F2, KEY_F2, 60)
  , (F3, KEY_F3, 61)
  , (F4, KEY_F4, 62)
  , (F5, KEY_F5, 63)
  , (F6, KEY_F6, 64)
  , (F7, KEY_F7, 65)
  , (F8, KEY_F8, 66)
  , (F9, KEY_F9, 67)
  , (F10, KEY_F10, 68)
  , (F13, KEY_F13, 183)
  , (F14, KEY_F14, 184)
  , (F15, KEY_F15, 185)
  , (F16, KEY_F16, 186)
  , (F17, KEY_F17, 187)
  , (F18, KEY_F18, 188)
  , (F19, KEY_F19, 189)
  , (F20, KEY_F20, 190)
  , (F21, KEY_F21, 191)
  , (F22, KEY_F22, 192)
  , (F23, KEY_F23, 193)
  , (F24, KEY_F24, 194)
  , (F11, KEY_F11, 87)
  , (F12, KEY_F12, 88)

  , (Home, KEY_HOME, 102)
  , (End, KEY_END, 107)
  , (PageUp, KEY_PAGEUP, 104)
  , (PageDown, KEY_PAGEDOWN, 109)
  , (Escape, KEY_ESC, 1)

  , (CursorUp, KEY_UP, 103)
  , (CursorLeft, KEY_LEFT, 105)
  , (CursorRight, KEY_RIGHT, 106)
  , (CursorDown, KEY_DOWN, 108)

  , (ScreenLock, KEY_SCREENLOCK, 152)

  , (Stop, KEY_STOP, 128)
  , (Cancel, KEY_CANCEL, 223)
  , (Save, KEY_SAVE, 234)

  , (MediaPlayCD, KEY_PLAYCD, 200)
  , (MediaPause, KEY_PAUSECD, 201)
  , (MediaPlay, KEY_PLAY, 207)
  , (MediaPlayPause, KEY_PLAYPAUSE, 164)
  , (MediaStop, KEY_STOPCD, 166)
  , (MediaEject, KEY_EJECTCLOSECD, 162)
  , (MediaPrevious, KEY_PREVIOUSSONG, 165)
  , (MediaNext, KEY_NEXTSONG, 163)
  , (MediaRewind, KEY_REWIND, 168)
  , (MediaForward, KEY_FASTFORWARD, 208)
  --, (, "KEY_CLOSECD", 160) -- Not mapped in XKB evdev
  --, (, "KEY_EJECTCD", 161) -- XF86Eject
  --, (, "KEY_RECORD", 167) -- XF86AudioRecord
  --, (, "KEY_PHONE", 169) -- XF86Phone
  --, (, "KEY_MICMUTE", 248) -- Not mapped in XKB evdev

  , (VolumeMute, KEY_MUTE, 113)
  , (VolumeDown, KEY_VOLUMEDOWN, 114)
  , (VolumeUp, KEY_VOLUMEUP, 115)

  , (BrowserBack, KEY_BACK, 158)
  , (BrowserForward, KEY_FORWARD, 159)
  , (BrowserRefresh, KEY_REFRESH, 173)
  , (BrowserSearch, KEY_SEARCH, 217)
  , (BrowserFavorites, KEY_BOOKMARKS, 156)
  , (BrowserHomePage, KEY_HOMEPAGE, 172)

  , (Calculator, KEY_CALC, 140)
  , (MediaPlayer, KEY_MEDIA, 226)
  , (Browser, KEY_WWW, 150)
  , (Email1, KEY_MAIL, 155)
  , (Email2, KEY_EMAIL, 215)
  , (Find, KEY_FIND, 136)
  , (Explorer, KEY_FILE, 144)
  , (MyComputer, KEY_COMPUTER, 157)
  , (Documents, KEY_DOCUMENTS, 235)
  , (Help, KEY_HELP, 138)
  , (Launch1, KEY_PROG1, 148)
  , (Launch2, KEY_PROG2, 149)
  , (Launch3, KEY_PROG3, 202)
  , (Launch4, KEY_PROG4, 203)

  , (Power, KEY_POWER, 116)
  , (Suspend, KEY_SUSPEND, 205)
  , (Sleep, KEY_SLEEP, 142)
  , (WakeUp, KEY_WAKEUP, 143)
  , (BrightnessDown, KEY_BRIGHTNESSDOWN, 224)
  , (BrightnessUp, KEY_BRIGHTNESSUP, 225)
  --, (, "KEY_BRIGHTNESS_CYCLE", 243) -- XF86MonBrightnessCycle
  --, (, "KEY_BRIGHTNESS_AUTO", 244) -- Not mapped in XKB
  --, (, "KEY_DISPLAY_OFF", 245) -- Not mapped in XKB
  --, (, "KEY_KBDILLUMTOGGLE", 228) -- XF86KbdLightOnOff
  --, (, "KEY_KBDILLUMDOWN", 229) -- XF86KbdBrightnessDown
  --, (, "KEY_KBDILLUMUP", 230) -- XF86KbdBrightnessUp

  , (KP1, KEY_KP1, 79)
  , (KP2, KEY_KP2, 80)
  , (KP3, KEY_KP3, 81)
  , (KP0, KEY_KP0, 82)
  , (KP4, KEY_KP4, 75)
  , (KP5, KEY_KP5, 76)
  , (KP6, KEY_KP6, 77)
  , (KP7, KEY_KP7, 71)
  , (KP8, KEY_KP8, 72)
  , (KP9, KEY_KP9, 73)
  , (KPDecimal, KEY_KPDOT, 83)
  , (KPComma, KEY_KPCOMMA, 121)
  , (KPJPComma, KEY_KPJPCOMMA, 95)
  , (KPPlusMinus, KEY_KPPLUSMINUS, 118)
  , (KPDivide, KEY_KPSLASH, 98)
  , (KPMultiply, KEY_KPASTERISK, 55)
  , (KPSubtract, KEY_KPMINUS, 74)
  , (KPAdd, KEY_KPPLUS, 78)
  , (KPEnter, KEY_KPENTER, 96)
  , (KPEquals, KEY_KPEQUAL, 117)

  , (Tabulator, KEY_TAB, 15)
  , (Return, KEY_ENTER, 28)
  , (Backspace, KEY_BACKSPACE, 14)
  , (Delete, KEY_DELETE, 111)
  , (Insert, KEY_INSERT, 110)

  , (Undo, KEY_UNDO, 131)
  , (Redo, KEY_REDO, 182)
  , (Cut, KEY_CUT, 137)
  , (Copy, KEY_COPY, 133)
  , (Paste, KEY_PASTE, 135)

  , (Menu, KEY_COMPOSE, 127) -- [NOTE] Not to be confused with KEY_MENU
  -- [NOTE] KEY_MENU should not be confused with KEY_COMPOSE
  -- , (_, KEY_MENU, 139) -- I147 XF86MenuKB

  -- , (ZenhakuHandaku, "KEY_ZENKAKUHANKAKU", 85) -- Not mapped in XKB evdev
  , (Muhenkan, KEY_MUHENKAN, 94)
  , (Henkan, KEY_HENKAN, 92)
  , (Katakana, KEY_KATAKANA, 90)
  , (Hiragana, KEY_HIRAGANA, 91)
  , (HiraganaKatakana, KEY_KATAKANAHIRAGANA, 93)
  , (Hangeul, KEY_HANGEUL, 122)
  , (Hanja, KEY_HANJA, 123)

  , (LShift, KEY_LEFTSHIFT, 42)
  , (RShift, KEY_RIGHTSHIFT, 54)
  , (CapsLock, KEY_CAPSLOCK, 58)
  , (LControl, KEY_LEFTCTRL, 29)
  , (RControl, KEY_RIGHTCTRL, 97)
  , (LAlternate, KEY_LEFTALT, 56)
  , (RAlternate, KEY_RIGHTALT, 100)
  , (LSuper, KEY_LEFTMETA, 125)
  , (RSuper, KEY_RIGHTMETA, 126)
  , (NumLock, KEY_NUMLOCK, 69)

  --, (, "KEY_LINEFEED", 101) -- LNFD, Linefeed
  --, (, "KEY_MACRO", 112) -- I120
  --, (, "KEY_SCALE", 120)
  --, (, "KEY_AGAIN", 129)
  --, (, "KEY_PROPS", 130)
  --, (, "KEY_FRONT", 132)
  --, (, "KEY_OPEN", 134)
  --, (, "KEY_SETUP", 141)
  --, (, "KEY_SENDFILE", 145)
  --, (, "KEY_DELETEFILE", 146)
  --, (, "KEY_XFER", 147)-- /* AL Terminal Lock/Screensaver */
  --, (, "KEY_ROTATE_DISPLAY", 153)
  --, (, "KEY_CYCLEWINDOWS", 154)
  --, (, "KEY_ISO", 170)
  --, (, "KEY_CONFIG", 171)
  --, (, "KEY_EXIT", 174)
  --, (, "KEY_MOVE", 175)
  --, (, "KEY_EDIT", 176)
  --, (, "KEY_SCROLLUP", 177)
  --, (, "KEY_SCROLLDOWN", 178)
  --, (, "KEY_KPLEFTPAREN", 179)
  --, (, "KEY_KPRIGHTPAREN", 180)
  --, (, "KEY_NEW", 181)
  --, (, "KEY_DASHBOARD", 204)
  --, (, "KEY_CLOSE", 206)
  --, (, "KEY_BASSBOOST", 209)
  --, (, "KEY_PRINT", 210)
  --, (, "KEY_HP", 211)
  --, (, "KEY_CAMERA", 212)
  --, (, "KEY_SOUND", 213)
  --, (, "KEY_QUESTION", 214)
  --, (, "KEY_CHAT", 216)
  --, (, "KEY_CONNECT", 218)
  --, (, "KEY_FINANCE", 219)
  --, (, "KEY_SPORT", 220)
  --, (, "KEY_SHOP", 221)
  --, (, "KEY_ALTERASE", 222)
  --, (, "KEY_SWITCHVIDEOMODE", 227)
  --, (, "KEY_SEND", 231)
  --, (, "KEY_REPLY", 232)
  --, (, "KEY_FORWARDMAIL", 233)
  --, (, "KEY_BATTERY", 236)

  --, (, "KEY_BLUETOOTH", 237)
  --, (, "KEY_WLAN", 238)
  --, (, "KEY_UWB", 239)

  --, (, "KEY_UNKNOWN", 240)

  --, (, "KEY_VIDEO_NEXT", 241)
  --, (, "KEY_VIDEO_PREV", 242)

  --, (, "KEY_WWAN", 246)
  --, (, "KEY_WIMAX")
  --, (, "KEY_RFKILL", 247)

  , (Grave, KEY_GRAVE, 41)
  , (N1, KEY_1, 2)
  , (N2, KEY_2, 3)
  , (N3, KEY_3, 4)
  , (N4, KEY_4, 5)
  , (N5, KEY_5, 6)
  , (N6, KEY_6, 7)
  , (N7, KEY_7, 8)
  , (N8, KEY_8, 9)
  , (N9, KEY_9, 10)
  , (N0, KEY_0, 11)
  , (Minus, KEY_MINUS, 12)
  , (Equals, KEY_EQUAL, 13)
  , (Q, KEY_Q, 16)
  , (W, KEY_W, 17)
  , (E, KEY_E, 18)
  , (R, KEY_R, 19)
  , (T, KEY_T, 20)
  , (Y, KEY_Y, 21)
  , (U, KEY_U, 22)
  , (I, KEY_I, 23)
  , (O, KEY_O, 24)
  , (P, KEY_P, 25)
  , (LBracket, KEY_LEFTBRACE, 26)
  , (RBracket, KEY_RIGHTBRACE, 27)
  , (A, KEY_A, 30)
  , (S, KEY_S, 31)
  , (D, KEY_D, 32)
  , (F, KEY_F, 33)
  , (G, KEY_G, 34)
  , (H, KEY_H, 35)
  , (J, KEY_J, 36)
  , (K, KEY_K, 37)
  , (L, KEY_L, 38)
  , (Semicolon, KEY_SEMICOLON, 39)
  , (Quote, KEY_APOSTROPHE, 40)
  , (Backslash, KEY_BACKSLASH, 43)
  , (Z, KEY_Z, 44)
  , (X, KEY_X, 45)
  , (C, KEY_C, 46)
  , (V, KEY_V, 47)
  , (B, KEY_B, 48)
  , (N, KEY_N, 49)
  , (M, KEY_M, 50)
  , (Comma, KEY_COMMA, 51)
  , (Period, KEY_DOT, 52)
  , (Slash, KEY_SLASH, 53)
  , (Iso102, KEY_102ND, 86)
  , (Yen, KEY_YEN, 124)
  , (Ro, KEY_RO, 89)
  , (Space, KEY_SPACE, 57)
  ]
