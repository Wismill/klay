{-|
Description: ISO key positions

This modules provides 'Key's positions using the key numbering system
from ISO/IEC&#xA0;9995-1, section&#xA0;7.
-}

module Klay.Keyboard.Hardware.KeyPosition
  ( KeyPosition(..)
  , Row(..)
  , Column(..)
  , rowDistance
  , columnDistance
  , manhattanDistance
  , sameRow, sameRow3
  , bringKeyCloserTo
  , showKey )
  where

import GHC.Generics
import Control.DeepSeq

-- | An identifier for a row on a keyboard, according to ISO/IEC&#xA0;9995.
data Row
  = A -- ^ ISO: Row of the space bar
  | B -- ^ ISO: Row of the @z@ key on the US layout
  | C -- ^ ISO: Row of the @a@ key on the US layout
  | D -- ^ ISO: Row of the @q@ key on the US layout
  | E -- ^ ISO: Row of the numbers on the US layout
  | K -- ^ ISO: First row of function keys
  deriving (Generic, NFData, Eq, Ord, Enum, Bounded, Show)

-- | An identifier for a column on a keyboard, according to ISO/IEC&#xA0;9995.
newtype Column = Column Int
  deriving stock (Generic, Eq, Ord, Show)
  deriving newtype Num
  deriving anyclass NFData

instance Bounded Column where
  minBound = Column minColumnIndex
  maxBound = Column maxColumnIndex

instance Enum Column where
  toEnum n
    | n < minColumnIndex = error $ "Minimum column: " <> show minColumnIndex
    | n > maxColumnIndex = error $ "Maximum column: " <> show maxColumnIndex
    | otherwise            = Column n
  fromEnum (Column n) = n

maxColumnIndex :: Int
maxColumnIndex = 90

minColumnIndex :: Int
minColumnIndex = -9

rowDistance :: Integral n => Row -> Row -> n
rowDistance r1 r2 = fromIntegral . abs $ fromEnum r1 - fromEnum r2

columnDistance :: Integral n => Column -> Column -> n
columnDistance (Column c1) (Column c2) = fromIntegral $ abs (c1 - c2)

-- | Position of a key on a keyboard as defined in ISO/IEC&#xA0;9995
data KeyPosition = KeyPosition !Row !Column
  deriving (Generic, NFData, Eq, Ord, Show)

instance Enum KeyPosition where
  toEnum n = let (r, c) = quotRem n maxColumnCount in KeyPosition (toEnum r) (toEnum c)
  fromEnum (KeyPosition r c) = maxColumnCount * fromEnum r + fromEnum c

maxColumnCount :: Int
maxColumnCount = 100

showKey :: KeyPosition -> String
showKey (KeyPosition r (Column c)) = show r <> show c

sameRow :: KeyPosition -> KeyPosition -> Bool
sameRow (KeyPosition r1 _) (KeyPosition r2 _) = r1 == r2

sameRow3 :: KeyPosition -> KeyPosition -> KeyPosition -> Bool
sameRow3 (KeyPosition r1 _) (KeyPosition r2 _) (KeyPosition r3 _)
  = r1 == r2 && r2 == r3

-- | Manhattan distance between two keys
manhattanDistance :: Num a => KeyPosition -> KeyPosition -> a
manhattanDistance (KeyPosition r1 (Column c1)) (KeyPosition r2 (Column c2)) =
  fromIntegral $ abs (fromEnum r1 - fromEnum r2) + abs (fromIntegral c1 - fromIntegral c2)
{-# SPECIALISE manhattanDistance :: KeyPosition -> KeyPosition -> Double #-}
{-# SPECIALISE manhattanDistance :: KeyPosition -> KeyPosition -> Int    #-}

-- | Given a key, compute the key that is immediately closer to a reference key
bringKeyCloserTo
  :: KeyPosition -- ^ Reference key
  -> KeyPosition -- ^ Key to bring closer to the reference
  -> KeyPosition
bringKeyCloserTo (KeyPosition r0 c0) (KeyPosition r c) = KeyPosition r' c'
  where
    r' | r > r0    = pred r
       | r < r0    = succ r
       | otherwise = r
    c' | c > c0    = pred c
       | c < c0    = succ c
       | otherwise = c
