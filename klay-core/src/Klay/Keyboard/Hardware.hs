{-|
Description: Modelling of /physical/ keyboard

Today, most keyboards use one of three different physical layouts:

* __ISO:__ [ISO/IEC&#xA0;9995-2](https://en.wikipedia.org/wiki/ISO/IEC_9995)
* __ANSI:__ ANSI-INCITS&#xA0;154-1988
* __JIS:__ JIS&#xA0;X&#xA0;6002-1980
-}

module Klay.Keyboard.Hardware
  ( module Klay.Keyboard.Hardware.Divisions
  , module Klay.Keyboard.Hardware.Geometry
  , module Klay.Keyboard.Hardware.Key
  ) where

import Klay.Keyboard.Hardware.Divisions
import Klay.Keyboard.Hardware.Geometry
import Klay.Keyboard.Hardware.Key
