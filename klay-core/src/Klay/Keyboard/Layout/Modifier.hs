{-# LANGUAGE OverloadedLists           #-}

module Klay.Keyboard.Layout.Modifier
  ( -- * Modifier description
    Modifier(..)
  , ModifierBit(..)
  , ModifierVariant(..)
  , ModifierEffect(..)
  , isSystemModifier
  , serializeModifier
  , serializeModifierBit
  , modifierP
  , modifierBitP
  , modifierVariantP
  , modifierEffectP

    -- * Combination of modifiers
  , ModifiersCombo
  , ModifiersCombos
  , ModifiersMask
  , ModifiersField
  , ModifiersFields
  , noModifier, shift, capitals, caps, control, ctrl, alternate, alt, super, numeric
  , altGr, altGrShift, altGrCaps
  , isoLevel1
  , isoLevel2, isoLevel2Caps, isoLevel2ShiftCaps
  , isoLevel3
  , isoLevel4, isoLevel4Caps, isoLevel4ShiftCaps
  , isoLevel5
  , isoLevel6, isoLevel6Caps, isoLevel6ShiftCaps
  , isoLevel7
  , isoLevel8, isoLevel8Caps, isoLevel8ShiftCaps
  , allModifiers
  , modifierToMask
  , modifiersToMask
  , maskToInt
  , maskToModifiers
  , testMask
  , matchMask
  , splitMask
  , isActiveModifierBit
  , isActiveModifier

    -- * Modifiers options
  , ModifiersOption(..)
  , ModifiersOptions(NoModifiersOptions)
  , ModifiersOptionsDelta
  , ModifiersOptionsAlteration
  , ModifiersOptionsAlterations
  , AutoModifiersOptions(..)
  , AutoModifiersOptionsMode(..)
  , CapsLockBehaviour(..)
  , CapsLockBehaviourAlteration
  , mapMaybeModifiersOptions
  , traverseMaybeModifiersOptions
    -- ** Predefined options
    -- *** Two levels
  , twoLevel
  , makeAlphabetic
  , alphabeticNoCancel, alphabetic
    -- *** Four levels
  , fourLevel
  , makeFourLevelSemiAlphabetic
  , fourLevelSemiAlphabeticNoCancel, fourLevelSemiAlphabetic
  , fourLevelReverseSemiAlphabetic
  , makeFourLevelAlphabetic
  , fourLevelAlphabeticNoCancel, fourLevelAlphabetic
  , fourLevelWithLock
  , separateCapsAndShiftAlphabetic
    -- *** Eight levels
  , eightLevel
  , makeEightLevelSemiAlphabetic
  , eightLevelSemiAlphabeticNoCancel, eightLevelSemiAlphabetic
  , makeEightLevelAlphabetic
  , eightLevelAlphabeticNoCancel, eightLevelAlphabetic
  , eightLevelLevelFiveLock, eightLevelAlphabeticLevelFiveLock
    -- ** Miscellaneous
  , modifiersOptions
  , modifiersOptionsToList
  , capsBehaviourToOptions
  , processCapsBehaviour
  , processModifiersOptions
  , processModifiersOptionsDetails
  , toCanonicalMask
  , toCanonicalMask'

    -- * Labels
  , modifierLabel
  , modifiersFieldLabel
  , modifierBitLabel
  ) where

import Data.Bits (Bits(complement, (.&.)))
import Data.Maybe (fromMaybe, mapMaybe)
import Data.List (intersperse)
import Data.Set qualified as Set
import Data.IntMap.Strict qualified as IMap
import Data.Functor ((<&>))
import Control.Arrow ((&&&))

import Data.Witherable(Witherable(..))

import Klay.Keyboard.Layout.Modifier.Types

noModifier, shift, capitals, caps, control, ctrl, alternate, alt, super, numeric :: ModifiersMask
-- | No modifier
noModifier = mempty
-- | 'Shift'
shift = modifierToMask Shift
-- | 'Capitals'
capitals = modifierToMask Capitals
-- | 'Capitals'
caps = capitals
-- | 'Control'
control = modifierToMask Control
-- | 'Control'
ctrl = control
-- | 'Alternate'
alternate = modifierToMask Alternate
-- | 'Alternate'
alt = alternate
-- | 'Super'
super = modifierToMask Super
-- | 'Numeric'
numeric = modifierToMask Numeric

altGr, altGrShift, altGrCaps :: ModifiersField
-- | 'Control' + 'Alternate'
altGr = control <> alt
-- | 'Control' + 'Alternate' + 'Shift'
altGrShift = control <> alt <> shift
-- | 'Control' + 'Alternate' + 'Capitals'
altGrCaps = control <> alt <> capitals

isoLevel1, isoLevel2, isoLevel2Caps, isoLevel2ShiftCaps :: ModifiersMask
-- | No modifier
isoLevel1 = noModifier
-- | 'Shift'
isoLevel2 = shift
-- | 'Capitals'
isoLevel2Caps = capitals
-- | 'Shift' + 'Capitals'
isoLevel2ShiftCaps = isoLevel2 <> capitals
isoLevel3, isoLevel4, isoLevel4Caps, isoLevel4ShiftCaps :: ModifiersMask
-- | 'IsoLevel3'
isoLevel3 = modifierToMask IsoLevel3
-- | 'IsoLevel3' + 'Shift'
isoLevel4 = isoLevel3 <> shift
-- | 'IsoLevel3' + 'Capitals'
isoLevel4Caps = isoLevel3 <> capitals
-- | 'IsoLevel3' + 'Shift' + 'Capitals'
isoLevel4ShiftCaps = isoLevel4 <> capitals
isoLevel5, isoLevel6, isoLevel6Caps, isoLevel6ShiftCaps :: ModifiersMask
-- | 'IsoLevel5'
isoLevel5 = modifierToMask IsoLevel5
-- | 'IsoLevel5' + 'Shift'
isoLevel6 = isoLevel5 <> shift
-- | 'IsoLevel5' + 'Capitals'
isoLevel6Caps = isoLevel5 <> capitals
-- | 'IsoLevel5' + 'Shift' + 'Capitals'
isoLevel6ShiftCaps = isoLevel6 <> capitals
isoLevel7, isoLevel8, isoLevel8Caps, isoLevel8ShiftCaps :: ModifiersMask
-- | 'IsoLevel3' + 'IsoLevel5'
isoLevel7 = isoLevel3 <> isoLevel5
-- | 'IsoLevel3' + 'IsoLevel5' + 'Shift'
isoLevel8 = isoLevel7 <> shift
-- | 'IsoLevel3' + 'IsoLevel5' + 'Capitals'
isoLevel8Caps = isoLevel7 <> capitals
-- | 'IsoLevel3' + 'IsoLevel5' + 'Shift' + 'Capitals'
isoLevel8ShiftCaps = isoLevel8 <> capitals

-- | Check if a modifier is a /system/ modifier, i.e. @Control@, @Alt@ or @Super@.
isSystemModifier :: ModifiersCombo -> Bool
isSystemModifier [Control]   = True
isSystemModifier [Alternate] = True
isSystemModifier [Super]     = True
isSystemModifier _           = False


--- Modifiers Combinations ----------------------------------------------------

maskToInt :: ModifiersField -> Int
maskToInt (ModifiersField m) = m

-- | Test a 'ModifiersMask' on a 'ModifiersField'
testMask :: ModifiersMask -> ModifiersField -> Bool
testMask m f
  | f == mempty && m /= f = False
  | m == mempty           = True
  | otherwise             = f .&. m /= mempty

-- | Exact match
matchMask :: ModifiersMask -> ModifiersField -> Bool
matchMask m f
  | f == mempty && m /= f = False
  | m == mempty           = True
  | otherwise             = f .&. m == m

-- | Get 'ModifiersField' without the 'ModifiersMask' if it matches.
splitMask
  :: ModifiersMask
  -- ^ Mask to apply
  -> ModifiersField
  -- ^ Field to test
  -> Either ModifiersField ModifiersField
  -- ^ 'Left': field unchanged, 'Right': field without the mask
splitMask m f
  | testMask m f = Right $ f .&. complement m
  | otherwise     = Left f


--- Modifiers Options ---------------------------------------------------------

mapMaybeModifiersOptions :: (ModifiersField -> Maybe ModifiersField) -> ModifiersOptions -> ModifiersOptions
mapMaybeModifiersOptions f (ModifiersOptions mo) =
  ModifiersOptions (IMap.fromList . mapMaybe go . IMap.toList $ mo)
  where
    go (b, ModifiersOption t p) = f (ModifiersField b) <&> \l ->
      let t' = fromMaybe l (f t)
          p' = l .&. fromMaybe mempty (f p)
      in (_mField l, ModifiersOption t' p')

traverseMaybeModifiersOptions :: (Monad m) => (ModifiersField -> m (Maybe ModifiersField)) -> ModifiersOptions -> m ModifiersOptions
traverseMaybeModifiersOptions f (ModifiersOptions mo) =
  ModifiersOptions . IMap.fromList <$> wither go (IMap.toList mo)
  where
    go (l, ModifiersOption t p) = do
      ml' <- f (ModifiersField l)
      case ml' of
        Nothing -> pure Nothing
        Just l' -> do
          t' <- fromMaybe l' <$> f t
          p' <- fromMaybe mempty <$> f p
          pure . Just $ (_mField l', ModifiersOption t' (l' .&. p'))

{-
Default XKB types
Source: FindAutomaticType https://github.com/xkbcommon/libxkbcommon/blob/master/src/xkbcomp/symbols.c#L1291

- 1 level: "ONE_LEVEL"
- 2 levels:
  - if the two keysyms are letter and the first is lower case and the other upper case,
    then "ALPHABETIC"
  - if one of the keysyms is numpad, then "KEYPAD"
  - else "TWO_LEVEL"
- 3 or 4 levels (missing 4th keysym is set to NoSymbol):
  - if the first two keysyms are letters and the first is lower case and the other upper case
    - if the last two keysyms are letters and the first is lower case and the other upper case
      then "FOUR_LEVEL_ALPHABETIC"
    - else "FOUR_LEVEL_SEMIALPHABETIC"
  - if one of the first two keysyms is numpad, then "FOUR_LEVEL_KEYPAD"
  - else "FOUR_LEVEL"

[TODO] move this note to a proper location
-}


-- | Default options
twoLevel, fourLevel, eightLevel :: ModifiersOptions
twoLevel   = mempty
fourLevel  = mempty
eightLevel = mempty

alphabetic, alphabeticNoCancel :: ModifiersOptions
alphabetic           = capsBehaviourToOptions [(isoLevel1, CapsIsShiftCancel)]
alphabeticNoCancel = capsBehaviourToOptions [(isoLevel1, CapsIsShiftNoCancel)]

makeAlphabetic :: CapsLockBehaviour -> ModifiersOptions
makeAlphabetic CapsIsNotShift      = twoLevel
makeAlphabetic CapsIsShiftCancel   = alphabetic
makeAlphabetic CapsIsShiftNoCancel = alphabeticNoCancel

fourLevelSemiAlphabetic, fourLevelSemiAlphabeticNoCancel :: ModifiersOptions
fourLevelSemiAlphabetic =
  [ (isoLevel1 <> capitals, ModifiersOption isoLevel2 mempty)
  , (isoLevel2 <> capitals, ModifiersOption isoLevel1 mempty)
  , (isoLevel3 <> capitals, ModifiersOption isoLevel3 capitals)
  , (isoLevel4 <> capitals, ModifiersOption isoLevel4 capitals)
  ]
fourLevelSemiAlphabeticNoCancel =
  [ (isoLevel1 <> capitals, ModifiersOption isoLevel2 mempty)
  , (isoLevel2 <> capitals, ModifiersOption isoLevel2 mempty)
  , (isoLevel3 <> capitals, ModifiersOption isoLevel3 capitals)
  , (isoLevel4 <> capitals, ModifiersOption isoLevel4 capitals)
  ]
makeFourLevelSemiAlphabetic :: CapsLockBehaviour -> ModifiersOptions
makeFourLevelSemiAlphabetic CapsIsNotShift      = fourLevel
makeFourLevelSemiAlphabetic CapsIsShiftCancel   = fourLevelSemiAlphabetic
makeFourLevelSemiAlphabetic CapsIsShiftNoCancel = fourLevelSemiAlphabeticNoCancel

-- | 'Capitals' affects only levels 3 & 4
fourLevelReverseSemiAlphabetic :: ModifiersOptions
fourLevelReverseSemiAlphabetic =
  [ (isoLevel1 <> capitals, ModifiersOption isoLevel1 mempty)
  , (isoLevel2 <> capitals, ModifiersOption isoLevel2 mempty)
  , (isoLevel3 <> capitals, ModifiersOption isoLevel4 capitals)
  , (isoLevel4 <> capitals, ModifiersOption isoLevel3 capitals)
  ]

fourLevelWithLock :: ModifiersOptions
fourLevelWithLock =
  [ (isoLevel2 <> capitals, MTarget isoLevel2)
  , (isoLevel3 <> capitals, MTarget isoLevel3)
  , (isoLevel4 <> capitals, MTarget isoLevel4)
  ]

separateCapsAndShiftAlphabetic :: ModifiersOptions
separateCapsAndShiftAlphabetic =
  [ (isoLevel1 <> capitals, ModifiersOption isoLevel4 capitals)
  , (isoLevel3 <> capitals, ModifiersOption isoLevel3 capitals)
  , (isoLevel4 <> capitals, ModifiersOption isoLevel3 mempty)
  ]

fourLevelAlphabetic, fourLevelAlphabeticNoCancel :: ModifiersOptions
fourLevelAlphabetic           = capsBehaviourToOptions [(isoLevel1, CapsIsShiftCancel)  , (isoLevel3, CapsIsShiftCancel)]
fourLevelAlphabeticNoCancel = capsBehaviourToOptions [(isoLevel1, CapsIsShiftNoCancel), (isoLevel3, CapsIsShiftNoCancel)]

makeFourLevelAlphabetic :: CapsLockBehaviour -> ModifiersOptions
makeFourLevelAlphabetic CapsIsNotShift      = fourLevel
makeFourLevelAlphabetic CapsIsShiftCancel   = fourLevelAlphabetic
makeFourLevelAlphabetic CapsIsShiftNoCancel = fourLevelAlphabeticNoCancel

eightLevelSemiAlphabetic, eightLevelSemiAlphabeticNoCancel :: ModifiersOptions
eightLevelSemiAlphabetic =
  [ (capitals <> isoLevel1, MTarget         isoLevel2         )
  , (capitals <> isoLevel2, MTarget         isoLevel1         )
  , (capitals <> isoLevel3, ModifiersOption isoLevel3 capitals)
  , (capitals <> isoLevel4, ModifiersOption isoLevel4 capitals)
  , (capitals <> isoLevel5, ModifiersOption isoLevel6 capitals)
  , (capitals <> isoLevel6, ModifiersOption isoLevel6 capitals)
  , (capitals <> isoLevel7, ModifiersOption isoLevel7 capitals)
  , (capitals <> isoLevel8, ModifiersOption isoLevel8 capitals)
  ]
eightLevelSemiAlphabeticNoCancel =
  [ (capitals <> isoLevel1, MTarget         isoLevel2         )
  , (capitals <> isoLevel2, MTarget         isoLevel2         )
  , (capitals <> isoLevel3, ModifiersOption isoLevel3 capitals)
  , (capitals <> isoLevel4, ModifiersOption isoLevel4 capitals)
  , (capitals <> isoLevel5, ModifiersOption isoLevel6 capitals)
  , (capitals <> isoLevel6, ModifiersOption isoLevel6 capitals)
  , (capitals <> isoLevel7, ModifiersOption isoLevel7 capitals)
  , (capitals <> isoLevel8, ModifiersOption isoLevel8 capitals)
  ]

makeEightLevelSemiAlphabetic :: CapsLockBehaviour -> ModifiersOptions
makeEightLevelSemiAlphabetic CapsIsNotShift      = eightLevel
makeEightLevelSemiAlphabetic CapsIsShiftCancel   = eightLevelSemiAlphabetic
makeEightLevelSemiAlphabetic CapsIsShiftNoCancel = eightLevelSemiAlphabeticNoCancel

eightLevelAlphabeticNoCancel, eightLevelAlphabetic :: ModifiersOptions
eightLevelAlphabeticNoCancel = capsBehaviourToOptions
  [ (isoLevel1, CapsIsShiftNoCancel)
  , (isoLevel3, CapsIsShiftNoCancel)
  , (isoLevel5, CapsIsShiftNoCancel)
  , (isoLevel7, CapsIsShiftNoCancel) ]
-- [NOTE] In XKB, LevelFive+Shift+Lock = Level1 but here we have the more logical LevelFive+Shift+Lock = LevelFive
eightLevelAlphabetic = capsBehaviourToOptions
  [ (isoLevel1, CapsIsShiftCancel)
  , (isoLevel3, CapsIsShiftCancel)
  , (isoLevel5, CapsIsShiftCancel)
  , (isoLevel7, CapsIsShiftCancel) ]
eightLevelLevelFiveLock, eightLevelAlphabeticLevelFiveLock :: ModifiersOptions
eightLevelLevelFiveLock =
  [ (isoLevel6                        , ModifiersOption isoLevel6 shift)
  , (numeric  <> isoLevel1            , MTarget         isoLevel5      )
  , (numeric  <> isoLevel2            , ModifiersOption isoLevel6 shift)
  , (numeric  <> isoLevel3            , MTarget         isoLevel7      )
  , (numeric  <> isoLevel4            , MTarget         isoLevel8      )
  , (numeric  <> isoLevel5            , MTarget         isoLevel1      )
  , (numeric  <> isoLevel6            , MTarget         isoLevel2      )
  , (numeric  <> isoLevel7            , MTarget         isoLevel3      )
  , (numeric  <> isoLevel8            , MTarget         isoLevel4      )
  , (capitals <> isoLevel1            , MTarget         isoLevel1      )
  , (capitals <> isoLevel2            , MTarget         isoLevel2      )
  , (capitals <> isoLevel3            , MTarget         isoLevel3      )
  , (capitals <> isoLevel4            , MTarget         isoLevel4      )
  , (capitals <> isoLevel5            , MTarget         isoLevel5      )
  , (capitals <> isoLevel6            , ModifiersOption isoLevel6 shift)
  , (capitals <> isoLevel7            , MTarget         isoLevel7      )
  , (capitals <> isoLevel8            , MTarget         isoLevel8      )
  , (numeric  <> capitals <> isoLevel1, MTarget         isoLevel5      )
  , (numeric  <> capitals <> isoLevel2, ModifiersOption isoLevel6 shift)
  , (numeric  <> capitals <> isoLevel3, MTarget         isoLevel7      )
  , (numeric  <> capitals <> isoLevel4, MTarget         isoLevel8      )
  , (numeric  <> capitals <> isoLevel5, MTarget         isoLevel1      )
  , (numeric  <> capitals <> isoLevel6, MTarget         isoLevel2      )
  , (numeric  <> capitals <> isoLevel7, MTarget         isoLevel3      )
  , (numeric  <> capitals <> isoLevel8, MTarget         isoLevel4      )
  ]
eightLevelAlphabeticLevelFiveLock =
  [ (isoLevel6                        , ModifiersOption isoLevel6 shift)
  , (numeric  <> isoLevel1            , MTarget         isoLevel5      )
  , (numeric  <> isoLevel2            , ModifiersOption isoLevel6 shift)
  , (numeric  <> isoLevel3            , MTarget         isoLevel7      )
  , (numeric  <> isoLevel4            , MTarget         isoLevel8      )
  , (numeric  <> isoLevel5            , MTarget         isoLevel1      )
  , (numeric  <> isoLevel6            , MTarget         isoLevel2      )
  , (numeric  <> isoLevel7            , MTarget         isoLevel3      )
  , (numeric  <> isoLevel8            , MTarget         isoLevel4      )
  , (capitals <> isoLevel1            , MTarget         isoLevel2      )
  , (capitals <> isoLevel2            , MTarget         isoLevel1      )
  , (capitals <> isoLevel3            , MTarget         isoLevel3      )
  , (capitals <> isoLevel4            , MTarget         isoLevel4      )
  , (capitals <> isoLevel5            , MTarget         isoLevel5      )
  , (capitals <> isoLevel6            , MTarget         isoLevel6      )
  , (capitals <> isoLevel7            , MTarget         isoLevel7      )
  , (capitals <> isoLevel8            , MTarget         isoLevel8      )
  , (numeric  <> capitals <> isoLevel1, MTarget         isoLevel5      )
  , (numeric  <> capitals <> isoLevel2, MTarget         isoLevel6      )
  , (numeric  <> capitals <> isoLevel3, MTarget         isoLevel7      )
  , (numeric  <> capitals <> isoLevel4, MTarget         isoLevel8      )
  , (numeric  <> capitals <> isoLevel5, MTarget         isoLevel2      )
  , (numeric  <> capitals <> isoLevel6, MTarget         isoLevel1      )
  , (numeric  <> capitals <> isoLevel7, MTarget         isoLevel3      )
  , (numeric  <> capitals <> isoLevel8, MTarget         isoLevel4      )
  ]

makeEightLevelAlphabetic :: CapsLockBehaviour -> ModifiersOptions
makeEightLevelAlphabetic CapsIsNotShift      = eightLevel
makeEightLevelAlphabetic CapsIsShiftCancel   = eightLevelAlphabetic
makeEightLevelAlphabetic CapsIsShiftNoCancel = eightLevelAlphabeticNoCancel

capsBehaviourToOptions :: [(ModifiersMask, CapsLockBehaviour)] -> ModifiersOptions
capsBehaviourToOptions = fromMaybe mempty . modifiersOptions . mconcat . fmap (uncurry go)
  where
    go :: ModifiersMask -> CapsLockBehaviour -> [(ModifiersMask, ModifiersOption)]
    go m0 = \case
      CapsIsNotShift      -> mempty
      CapsIsShiftCancel   -> [(mCaps, MTarget mShift), (mCaps <> shift, MTarget m)]
      CapsIsShiftNoCancel -> [(mCaps, MTarget mShift), (mCaps <> shift, MTarget mShift)]
      where m = m0 .&. complement (shift <> capitals) -- mask without shift nor capitals
            mShift = m <> shift
            mCaps  = m <> capitals

-- | Process a 'CapsLockBehaviour' on a set of modifiers to get all the corresponding possible combinations.
processCapsBehaviour :: ModifiersMask -> CapsLockBehaviour -> [ModifiersMask]
processCapsBehaviour m CapsIsNotShift = [m] -- Capitals != Shift
processCapsBehaviour m capsBehaviour
  | has_none = [m]
  | otherwise = case capsBehaviour of
    CapsIsShiftNoCancel -> [m' <> shift, m' <> capitals, m' <> shift <> capitals]
    _ -> if has_both then [m', m] else [m' <> shift, m' <> capitals]
  where hasShift    = isActiveModifierBit m Shift
        has_capitals = isActiveModifierBit m Capitals
        has_none     = not (hasShift || has_capitals)
        has_both     = hasShift && has_capitals
        m' = m .&. complement (shift <> capitals)

-- | Process a 'ModifiersOptions' on a set of modifiers to get all the corresponding possible combinations.
processModifiersOptions :: ModifiersOptions -> ModifiersMask -> Set.Set ModifiersMask
processModifiersOptions (ModifiersOptions opts) m0@(ModifiersField b0) =
  IMap.foldlWithKey go [m0, m] opts
  where
    m = maybe m0 _mTarget $ IMap.lookup b0 opts
    go acc b ModifiersOption{_mTarget=m'}
      | m' == m   = Set.insert (ModifiersField b) acc
      | otherwise = acc

-- | Process a 'ModifiersOptions' on a set of modifiers to get all the corresponding possible combinations
-- and their "preserve" atttribute.
processModifiersOptionsDetails :: ModifiersOptions -> ModifiersMask -> [(ModifiersMask, ModifiersMask)]
processModifiersOptionsDetails opts@(ModifiersOptions opts') m0 =
  (id &&& go) <$> ms
  where
    ms = Set.toList $ processModifiersOptions opts m0
    go (ModifiersField b) = maybe mempty _mPreserve $ IMap.lookup b opts'

-- | Convert a 'ModifiersMask' to its canonical equivalent, as defined in the given 'ModifiersOptions'.
toCanonicalMask :: ModifiersOptions -> ModifiersMask -> ModifiersMask
toCanonicalMask opts (ModifiersField b) = ModifiersField $ toCanonicalMask' opts b

-- | Convert a 'ModifiersMask' to its canonical equivalent, as defined in the given 'ModifiersOptions'.
toCanonicalMask' :: ModifiersOptions -> Int -> Int
toCanonicalMask' (ModifiersOptions opts) b = maybe b (_mField . _mTarget) $ IMap.lookup b opts


--- Modifiers Labels ----------------------------------------------------------

-- | Label of a given modifier on a physical keyboard.
-- Symbols are inspired from ISO\/IEC&#xA0;9995-7:2009
modifierLabel :: Modifier -> String
modifierLabel (Modifier [Shift]     _ Lock) = "⇫"
modifierLabel (Modifier [Shift]     _ _   ) = "⇧"
modifierLabel (Modifier [Capitals]  _ Lock) = "⮸"
modifierLabel (Modifier [Capitals]  _ _   ) = "⇪"
modifierLabel (Modifier [Control]   _ _   ) = "⎈"
modifierLabel (Modifier [Alternate] _ _   ) = "⎇"
modifierLabel (Modifier [Control, Alternate] _ _) = "AltGr"
modifierLabel (Modifier [Numeric]   _ Lock) = "⇭"
modifierLabel (Modifier [Numeric]   _ _   ) = "1²₃"
modifierLabel (Modifier [Super]     _ _   ) = "◆" -- [NOTE] This symbol is originally for Meta. Other possibility: ❖
modifierLabel (Modifier [IsoLevel3] _ Lock) = "⇨͚"
modifierLabel (Modifier [IsoLevel3] _ _   ) = "⇨" -- Also: ⇮⇯
modifierLabel (Modifier [IsoLevel5] _ Lock) = "⇦͚"
modifierLabel (Modifier [IsoLevel5] _ _   ) = "⇦"
modifierLabel m                             = show m

modifiersFieldLabel :: ModifiersField -> String
modifiersFieldLabel = mconcat . intersperse " + " . foldr (\m acc -> show m : acc) mempty . maskToModifiers

-- | Label of a given modifier on a physical keyboard, in its neutral variant 'N' and with the 'Set' effect.
modifierBitLabel :: ModifierBit -> String
modifierBitLabel m = modifierLabel (Modifier [m] N Set)
