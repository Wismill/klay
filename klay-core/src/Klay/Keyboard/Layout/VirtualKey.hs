{-|
Description: Windows virtual keys definition
-}

module Klay.Keyboard.Layout.VirtualKey
  ( VirtualKey(..)
  , VirtualKeysDefinition
  , defaultVirtualKeysDefinition
    -- * Encoding
  , keyNativeVirtualKey4
  ) where

import Data.Maybe (mapMaybe)
-- import Data.Set qualified as Set
import Data.Map qualified as Map
-- import Data.Foldable (foldl')

import Klay.Keyboard.Hardware.Key qualified as K
-- import Klay.Keyboard.Layout.Modifier (ModifiersCombo)
import Klay.Keyboard.Layout.VirtualKey.Types

-- | Default 'VirtualKeysDefinition' with all the 'Key's mapped to their corresponding 'VirtualKey's.
defaultVirtualKeysDefinition :: VirtualKeysDefinition
defaultVirtualKeysDefinition = Map.fromList . mapMaybe (\k -> (k,) <$> keyNativeVirtualKey4 k) $ enumFromTo minBound maxBound

-- | Native VK for /type 4/ keyboard
keyNativeVirtualKey4 :: K.Key -> Maybe VirtualKey
keyNativeVirtualKey4 K.Backspace = Just VK_BACK
keyNativeVirtualKey4 K.Tabulator = Just VK_TAB
keyNativeVirtualKey4 K.KPEquals  = Just VK_CLEAR
keyNativeVirtualKey4 K.Return    = Just VK_RETURN
keyNativeVirtualKey4 K.Pause     = Just VK_PAUSE
keyNativeVirtualKey4 K.CapsLock  = Just VK_CAPITAL
-- keyNativeVirtualKey4 K.HiraganaKatakana = Just VK_KANA
-- keyNativeVirtualKey4 K. = Just VK_IME_ON
-- keyNativeVirtualKey4 K. = Just VK_JUNJA
-- keyNativeVirtualKey4 K. = Just VK_FINAL
-- keyNativeVirtualKey4 K.Hanja = Just VK_HANJA
-- keyNativeVirtualKey4 K. = Just VK_IME_OFF
keyNativeVirtualKey4 K.Escape      = Just VK_ESCAPE
-- keyNativeVirtualKey4 K.Henkan   = Just VK_CONVERT
-- keyNativeVirtualKey4 K.Muhenkan = Just VK_NONCONVERT
-- keyNativeVirtualKey4 K. = Just VK_ACCEPT
-- keyNativeVirtualKey4 K. = Just VK_MODECHANGE
keyNativeVirtualKey4 K.Space       = Just VK_SPACE
keyNativeVirtualKey4 K.PageUp      = Just VK_PRIOR
keyNativeVirtualKey4 K.PageDown    = Just VK_NEXT
keyNativeVirtualKey4 K.End         = Just VK_END
keyNativeVirtualKey4 K.Home        = Just VK_HOME
keyNativeVirtualKey4 K.CursorLeft  = Just VK_LEFT
keyNativeVirtualKey4 K.CursorUp    = Just VK_UP
keyNativeVirtualKey4 K.CursorRight = Just VK_RIGHT
keyNativeVirtualKey4 K.CursorDown  = Just VK_DOWN
-- keyNativeVirtualKey4 K. = Just VK_SELECT
-- keyNativeVirtualKey4 K. = Just VK_PRINT
-- keyNativeVirtualKey4 K. = Just VK_EXECUTE
keyNativeVirtualKey4 K.PrintScreen = Just VK_SNAPSHOT
keyNativeVirtualKey4 K.Insert = Just VK_INSERT
keyNativeVirtualKey4 K.Delete = Just VK_DELETE
keyNativeVirtualKey4 K.Help   = Just VK_HELP
keyNativeVirtualKey4 K.N0     = Just VK_0
keyNativeVirtualKey4 K.N1     = Just VK_1
keyNativeVirtualKey4 K.N2     = Just VK_2
keyNativeVirtualKey4 K.N3     = Just VK_3
keyNativeVirtualKey4 K.N4     = Just VK_4
keyNativeVirtualKey4 K.N5     = Just VK_5
keyNativeVirtualKey4 K.N6     = Just VK_6
keyNativeVirtualKey4 K.N7     = Just VK_7
keyNativeVirtualKey4 K.N8     = Just VK_8
keyNativeVirtualKey4 K.N9     = Just VK_9
keyNativeVirtualKey4 K.A      = Just VK_A
keyNativeVirtualKey4 K.B      = Just VK_B
keyNativeVirtualKey4 K.C      = Just VK_C
keyNativeVirtualKey4 K.D      = Just VK_D
keyNativeVirtualKey4 K.E      = Just VK_E
keyNativeVirtualKey4 K.F      = Just VK_F
keyNativeVirtualKey4 K.G      = Just VK_G
keyNativeVirtualKey4 K.H      = Just VK_H
keyNativeVirtualKey4 K.I      = Just VK_I
keyNativeVirtualKey4 K.J      = Just VK_J
keyNativeVirtualKey4 K.K      = Just VK_K
keyNativeVirtualKey4 K.L      = Just VK_L
keyNativeVirtualKey4 K.M      = Just VK_M
keyNativeVirtualKey4 K.N      = Just VK_N
keyNativeVirtualKey4 K.O      = Just VK_O
keyNativeVirtualKey4 K.P      = Just VK_P
keyNativeVirtualKey4 K.Q      = Just VK_Q
keyNativeVirtualKey4 K.R      = Just VK_R
keyNativeVirtualKey4 K.S      = Just VK_S
keyNativeVirtualKey4 K.T      = Just VK_T
keyNativeVirtualKey4 K.U      = Just VK_U
keyNativeVirtualKey4 K.V      = Just VK_V
keyNativeVirtualKey4 K.W      = Just VK_W
keyNativeVirtualKey4 K.X      = Just VK_X
keyNativeVirtualKey4 K.Y      = Just VK_Y
keyNativeVirtualKey4 K.Z      = Just VK_Z
keyNativeVirtualKey4 K.LSuper = Just VK_LWIN
keyNativeVirtualKey4 K.RSuper = Just VK_RWIN
keyNativeVirtualKey4 K.Menu   = Just VK_APPS
keyNativeVirtualKey4 K.Sleep  = Just VK_SLEEP
keyNativeVirtualKey4 K.KP0        = Just VK_NUMPAD0
keyNativeVirtualKey4 K.KP1        = Just VK_NUMPAD1
keyNativeVirtualKey4 K.KP2        = Just VK_NUMPAD2
keyNativeVirtualKey4 K.KP3        = Just VK_NUMPAD3
keyNativeVirtualKey4 K.KP4        = Just VK_NUMPAD4
keyNativeVirtualKey4 K.KP5        = Just VK_NUMPAD5
keyNativeVirtualKey4 K.KP6        = Just VK_NUMPAD6
keyNativeVirtualKey4 K.KP7        = Just VK_NUMPAD7
keyNativeVirtualKey4 K.KP8        = Just VK_NUMPAD8
keyNativeVirtualKey4 K.KP9        = Just VK_NUMPAD9
keyNativeVirtualKey4 K.KPMultiply = Just VK_MULTIPLY
keyNativeVirtualKey4 K.KPAdd      = Just VK_ADD
-- keyNativeVirtualKey4 K.KPJPComma  = Just VK_SEPARATOR
keyNativeVirtualKey4 K.KPSubtract = Just VK_SUBTRACT
keyNativeVirtualKey4 K.KPDecimal  = Just VK_DECIMAL
keyNativeVirtualKey4 K.KPDivide   = Just VK_DIVIDE
keyNativeVirtualKey4 K.F1  = Just VK_F1
keyNativeVirtualKey4 K.F2  = Just VK_F2
keyNativeVirtualKey4 K.F3  = Just VK_F3
keyNativeVirtualKey4 K.F4  = Just VK_F4
keyNativeVirtualKey4 K.F5  = Just VK_F5
keyNativeVirtualKey4 K.F6  = Just VK_F6
keyNativeVirtualKey4 K.F7  = Just VK_F7
keyNativeVirtualKey4 K.F8  = Just VK_F8
keyNativeVirtualKey4 K.F9  = Just VK_F9
keyNativeVirtualKey4 K.F10 = Just VK_F10
keyNativeVirtualKey4 K.F11 = Just VK_F11
keyNativeVirtualKey4 K.F12 = Just VK_F12
keyNativeVirtualKey4 K.F13 = Just VK_F13
keyNativeVirtualKey4 K.F14 = Just VK_F14
keyNativeVirtualKey4 K.F15 = Just VK_F15
keyNativeVirtualKey4 K.F16 = Just VK_F16
keyNativeVirtualKey4 K.F17 = Just VK_F17
keyNativeVirtualKey4 K.F18 = Just VK_F18
keyNativeVirtualKey4 K.F19 = Just VK_F19
keyNativeVirtualKey4 K.F20 = Just VK_F20
keyNativeVirtualKey4 K.F21 = Just VK_F21
keyNativeVirtualKey4 K.F22 = Just VK_F22
keyNativeVirtualKey4 K.F23 = Just VK_F23
keyNativeVirtualKey4 K.F24 = Just VK_F24
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_VIEW
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_MENU
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_UP
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_DOWN
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_LEFT
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_RIGHT
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_ACCEPT
-- keyNativeVirtualKey4 K. = Just VK_NAVIGATION_CANCEL
keyNativeVirtualKey4 K.NumLock = Just VK_NUMLOCK
keyNativeVirtualKey4 K.ScrollLock = Just VK_SCROLL
-- keyNativeVirtualKey4 K. = Just VK_OEM_FJ_JISHO
-- keyNativeVirtualKey4 K. = Just VK_OEM_FJ_MASSHOU
-- keyNativeVirtualKey4 K. = Just VK_OEM_FJ_TOUROKU
-- keyNativeVirtualKey4 K. = Just VK_OEM_FJ_LOYA
-- keyNativeVirtualKey4 K. = Just VK_OEM_FJ_ROYA
keyNativeVirtualKey4 K.LShift = Just VK_LSHIFT
keyNativeVirtualKey4 K.RShift = Just VK_RSHIFT
keyNativeVirtualKey4 K.LControl = Just VK_LCONTROL
keyNativeVirtualKey4 K.RControl = Just VK_RCONTROL
keyNativeVirtualKey4 K.LAlternate = Just VK_LMENU
keyNativeVirtualKey4 K.RAlternate = Just VK_RMENU
keyNativeVirtualKey4 K.BrowserBack = Just VK_BROWSER_BACK
keyNativeVirtualKey4 K.BrowserForward = Just VK_BROWSER_FORWARD
keyNativeVirtualKey4 K.BrowserRefresh = Just VK_BROWSER_REFRESH
-- keyNativeVirtualKey4 K. = Just VK_BROWSER_STOP
keyNativeVirtualKey4 K.BrowserSearch    = Just VK_BROWSER_SEARCH
keyNativeVirtualKey4 K.BrowserFavorites = Just VK_BROWSER_FAVORITES
keyNativeVirtualKey4 K.BrowserHomePage  = Just VK_BROWSER_HOME
keyNativeVirtualKey4 K.VolumeMute       = Just VK_VOLUME_MUTE
keyNativeVirtualKey4 K.VolumeDown       = Just VK_VOLUME_DOWN
keyNativeVirtualKey4 K.VolumeUp         = Just VK_VOLUME_UP
keyNativeVirtualKey4 K.MediaNext        = Just VK_MEDIA_NEXT_TRACK
keyNativeVirtualKey4 K.MediaPrevious    = Just VK_MEDIA_PREV_TRACK
keyNativeVirtualKey4 K.MediaStop        = Just VK_MEDIA_STOP
keyNativeVirtualKey4 K.MediaPlayCD      = Just VK_MEDIA_PLAY_PAUSE
keyNativeVirtualKey4 K.MediaPlay        = Just VK_MEDIA_PLAY_PAUSE
keyNativeVirtualKey4 K.MediaPlayPause   = Just VK_MEDIA_PLAY_PAUSE
keyNativeVirtualKey4 K.Email1           = Just VK_LAUNCH_MAIL
keyNativeVirtualKey4 K.Email2           = Just VK_LAUNCH_MAIL
keyNativeVirtualKey4 K.MediaPlayer      = Just VK_LAUNCH_MEDIA_SELECT
keyNativeVirtualKey4 K.Launch1          = Just VK_LAUNCH_APP1
keyNativeVirtualKey4 K.Launch2          = Just VK_LAUNCH_APP2
keyNativeVirtualKey4 K.Semicolon        = Just VK_OEM_1
keyNativeVirtualKey4 K.Equals           = Just VK_OEM_PLUS
keyNativeVirtualKey4 K.Comma            = Just VK_OEM_COMMA
keyNativeVirtualKey4 K.Minus            = Just VK_OEM_MINUS
keyNativeVirtualKey4 K.Period           = Just VK_OEM_PERIOD
keyNativeVirtualKey4 K.Slash            = Just VK_OEM_2
keyNativeVirtualKey4 K.Grave            = Just VK_OEM_3
keyNativeVirtualKey4 K.BrSlash          = Just VK_ABNT_C1
keyNativeVirtualKey4 K.KPBrDot          = Just VK_ABNT_C2
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_A
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_B
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_X
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_Y
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_SHOULDER
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_SHOULDER
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_TRIGGER
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_TRIGGER
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_DPAD_UP
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_DPAD_DOWN
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_DPAD_LEFT
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_DPAD_RIGHT
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_MENU
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_VIEW
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_THUMBSTICK_BUTTON
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_THUMBSTICK_BUTTON
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_THUMBSTICK_UP
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_THUMBSTICK_DOWN
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_THUMBSTICK_RIGHT
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_LEFT_THUMBSTICK_LEFT
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_THUMBSTICK_UP
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_THUMBSTICK_DOWN
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_THUMBSTICK_RIGHT
-- keyNativeVirtualKey4 K. = Just VK_GAMEPAD_RIGHT_THUMBSTICK_LEFT
keyNativeVirtualKey4 K.LBracket  = Just VK_OEM_4
keyNativeVirtualKey4 K.Backslash = Just VK_OEM_5
keyNativeVirtualKey4 K.RBracket  = Just VK_OEM_6
keyNativeVirtualKey4 K.Quote     = Just VK_OEM_7
-- keyNativeVirtualKey4 K.       = Just VK_OEM_8
-- keyNativeVirtualKey4 K. = Just VK_OEM_AX
keyNativeVirtualKey4 K.Iso102    = Just VK_OEM_102
-- keyNativeVirtualKey4 K. = Just VK_ICO_HELP
-- keyNativeVirtualKey4 K. = Just VK_ICO_00
-- keyNativeVirtualKey4 K. = Just VK_PROCESSKEY
-- keyNativeVirtualKey4 K. = Just VK_ICO_CLEAR
-- keyNativeVirtualKey4 K. = Just VK_PACKET
-- keyNativeVirtualKey4 K. = Just VK_OEM_RESET
-- keyNativeVirtualKey4 K. = Just VK_OEM_JUMP
-- keyNativeVirtualKey4 K. = Just VK_OEM_PA1 -- Reserved for Klay modifiers
-- keyNativeVirtualKey4 K. = Just VK_OEM_PA2 -- Reserved for Klay modifiers
-- keyNativeVirtualKey4 K. = Just VK_OEM_PA3 -- Reserved for Klay modifiers
-- keyNativeVirtualKey4 K. = Just VK_OEM_WSCTRL
-- keyNativeVirtualKey4 K. = Just VK_OEM_CUSEL
-- keyNativeVirtualKey4 K. = Just VK_OEM_ATTN
-- keyNativeVirtualKey4 K. = Just VK_OEM_FINISH
-- keyNativeVirtualKey4 K. = Just VK_OEM_COPY
-- keyNativeVirtualKey4 K. = Just VK_OEM_AUTO
-- keyNativeVirtualKey4 K. = Just VK_OEM_ENLW
-- keyNativeVirtualKey4 K. = Just VK_OEM_BACKTAB
-- keyNativeVirtualKey4 K. = Just VK_ATTN
-- keyNativeVirtualKey4 K. = Just VK_CRSEL
-- keyNativeVirtualKey4 K. = Just VK_EXSEL
-- keyNativeVirtualKey4 K. = Just VK_EREOF
-- keyNativeVirtualKey4 K. = Just VK_PLAY
-- keyNativeVirtualKey4 K. = Just VK_ZOOM
-- keyNativeVirtualKey4 K. = Just VK_NONAME
-- keyNativeVirtualKey4 K. = Just VK_PA1
-- keyNativeVirtualKey4 K. = Just VK_OEM_CLEAR
keyNativeVirtualKey4 _ = Nothing
