{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE ExistentialQuantification #-}

{-|
Description: Modelling of ISO groups

This module provides groups as defined in ISO/IEC&#xA0;9995-1, section&#xA0;5.1.
-}

module Klay.Keyboard.Layout.Group
  ( -- * Collection of groups
    Groups(..)
  , GroupIndex(..)
  , GroupedGroups
  , GroupsMappings

  , MultiOsGroupsMappings
  , SingleOsGroupsMappings

  , MultiOsRawGroups
  , RawGroups
  , ResolvedGroups
  , FinalGroups

  , groupsToList
  , groupsFromList
  , indexedGroups
  , onGroup
  , imapGroups
  , itraverseGroups
  , zipGroupsWith
  , replaceGroup
  , laxReplaceGroup
  , addGroup
  , removeGroup
  , firstGroup
  , groupIndexToInt

    -- * Group
  , Group(..)
  , SingleOsGroup
  , MultiOsGroup
  , GroupLevels
  , MultiOsGroupLevels

  , GroupMapping
  , MultiOsGroupMapping
  , MultiOsRawGroup
  , KeyMultiOsRawGroup
  , SingleOsGroupMapping
  , RawGroup
  , KeyRawGroup
  , ResolvedGroup
  , KeyResolvedGroup
  , FinalGroup
  , KeyFinalGroup
  , LayerGroup

  , mkGroup
  , mkRawGroup
  , groupLevels
  , groupModifiers
  , groupLevelsNames
  , groupIndexedLevelsNames
  , groupModifiersByLevel
  -- ** Action map
  , groupActionsMap
  , groupLayers
  , groupImplicitLayers
  , groupLayer
  , groupImplicitLayer
  , groupBaseLayer
    -- ** Miscellaneous
  , resolveOsGroups
  , resolveGroups
  , resolveFinalGroups
  , resolveOsGroup
  , resolveGroup
  , resolveGroupLayout
  , foldlGroupsMappingsWithKey
  , foldlGroupsMappingActionsWithKey
  , foldlGroupMappingWithKey
  , foldlGroupMappingActionsWithKey
  , foldlGroupMappingImplicitActionsWithKey
  , foldrGroupMappingM
  , foldrGroupMappingWithKeyM
  , foldlGroupsLayoutsWithIndexM
  , traverseGroupsMappings
  , itraverseGroupsLayouts_
  , firstGroupActionsMap
  ) where

import Data.List.NonEmpty qualified as NE
import Data.Map.Strict qualified as Map
import Data.Foldable (foldl', foldlM, traverse_)
import Control.Arrow ((&&&))
import GHC.Exts (IsList(..))

import Klay.Keyboard.Layout.Group.Types
import Klay.Keyboard.Layout.Action
  ( Action(..), Actions, Level, Layer
  , ActionsMap, MultiOsRawActionsKeyMap
  , getLayer , getImplicitLayer, untagCustomisableActions
  , multiOsRawActionsToResolvedActions, multiOsRawActionsToRawActions
  , foldlActions, foldlImplicitActions )
import Klay.Keyboard.Layout.Modifier (ModifiersCombo, ModifiersOptions, maskToModifiers)
import Klay.OS (OsSpecific(..), getOs)
import Klay.Utils (foldrMWithKey')
import Klay.Utils.UserInput (RawLText)


--- Groups --------------------------------------------------------------------

zipGroupsWith :: (g1 -> g2 -> g3) -> Groups g1 -> Groups g2 -> Groups g3
zipGroupsWith f (OneGroup    g1)       (OneGroup    h1)       = OneGroup (f g1 h1)
zipGroupsWith f (OneGroup    g1)       (TwoGroups   h1 _)     = OneGroup (f g1 h1)
zipGroupsWith f (OneGroup    g1)       (ThreeGroups h1 _ _)   = OneGroup (f g1 h1)
zipGroupsWith f (OneGroup    g1)       (FourGroups  h1 _ _ _) = OneGroup (f g1 h1)
zipGroupsWith f (TwoGroups   g1 _)     (OneGroup    h1)       = OneGroup (f g1 h1)
zipGroupsWith f (ThreeGroups g1 _ _)   (OneGroup    h1)       = OneGroup (f g1 h1)
zipGroupsWith f (FourGroups  g1 _ _ _) (OneGroup    h1)       = OneGroup (f g1 h1)
--
zipGroupsWith f (TwoGroups   g1 g2)     (TwoGroups   h1 h2)     = TwoGroups (f g1 h1) (f g2 h2)
zipGroupsWith f (TwoGroups   g1 g2)     (ThreeGroups h1 h2 _)   = TwoGroups (f g1 h1) (f g2 h2)
zipGroupsWith f (TwoGroups   g1 g2)     (FourGroups  h1 h2 _ _) = TwoGroups (f g1 h1) (f g2 h2)
zipGroupsWith f (ThreeGroups g1 g2 _)   (TwoGroups   h1 h2)     = TwoGroups (f g1 h1) (f g2 h2)
zipGroupsWith f (FourGroups  g1 g2 _ _) (TwoGroups   h1 h2)     = TwoGroups (f g1 h1) (f g2 h2)
--
zipGroupsWith f (ThreeGroups g1 g2 g3)   (ThreeGroups h1 h2 h3)   = ThreeGroups (f g1 h1) (f g2 h2) (f g3 h3)
zipGroupsWith f (ThreeGroups g1 g2 g3)   (FourGroups  h1 h2 h3 _) = ThreeGroups (f g1 h1) (f g2 h2) (f g3 h3)
zipGroupsWith f (FourGroups  g1 g2 g3 _) (ThreeGroups h1 h2 h3)   = ThreeGroups (f g1 h1) (f g2 h2) (f g3 h3)
--
zipGroupsWith f (FourGroups  g1 g2 g3 g4) (FourGroups h1 h2 h3 h4) = FourGroups (f g1 h1) (f g2 h2) (f g3 h3) (f g4 h4)

imapGroups :: (GroupIndex -> g1 -> g2) -> Groups g1 -> Groups g2
imapGroups f = zipGroupsWith f (fromList $ enumFromTo minBound maxBound)

itraverseGroups :: (Applicative f) => (GroupIndex -> g1 -> f g2) -> Groups g1 -> f (Groups g2)
itraverseGroups f = traverse (uncurry f) . zipGroupsWith (,) (fromList $ enumFromTo minBound maxBound)

onGroup :: GroupIndex -> (g -> g) -> Groups g -> Groups g
onGroup FirstGroup f (OneGroup g1) = OneGroup (f g1)
onGroup FirstGroup f (TwoGroups g1 g2) = TwoGroups (f g1) g2
onGroup FirstGroup f (ThreeGroups g1 g2 g3) = ThreeGroups (f g1) g2 g3
onGroup FirstGroup f (FourGroups g1 g2 g3 g4) = FourGroups (f g1) g2 g3 g4
onGroup SecondGroup f (TwoGroups g1 g2) = TwoGroups g1 (f g2)
onGroup SecondGroup f (ThreeGroups g1 g2 g3) = ThreeGroups g1 (f g2) g3
onGroup SecondGroup f (FourGroups g1 g2 g3 g4) = FourGroups g1 (f g2) g3 g4
onGroup SecondGroup _ gs = gs
onGroup ThirdGroup f (ThreeGroups g1 g2 g3) = ThreeGroups g1 g2 (f g3)
onGroup ThirdGroup f (FourGroups g1 g2 g3 g4) = FourGroups g1 g2 (f g3) g4
onGroup ThirdGroup _ gs = gs
onGroup FourthGroup f (FourGroups g1 g2 g3 g4) = FourGroups g1 g2 g3 (f g4)
onGroup FourthGroup _ gs = gs

replaceGroup :: GroupIndex -> g -> Groups g -> Groups g
replaceGroup k g = onGroup k (const g)

laxReplaceGroup :: GroupIndex -> g -> Groups g -> Groups g
laxReplaceGroup SecondGroup g (OneGroup g1) = TwoGroups g1 g
laxReplaceGroup ThirdGroup  g (TwoGroups g1 g2) = ThreeGroups g1 g2 g
laxReplaceGroup FourthGroup g (ThreeGroups g1 g2 g3) = FourGroups g1 g2 g3 g
laxReplaceGroup k           g gs = replaceGroup k g gs

addGroup :: g -> Groups g -> Maybe (Groups g)
addGroup g (OneGroup g1)          = Just $ TwoGroups g1 g
addGroup g (TwoGroups g1 g2)      = Just $ ThreeGroups g1 g2 g
addGroup g (ThreeGroups g1 g2 g3) = Just $ FourGroups g1 g2 g3 g
addGroup _ FourGroups{}           = Nothing

removeGroup :: GroupIndex -> Groups g -> Maybe (Groups g)
removeGroup FirstGroup (TwoGroups _ g2) = Just $ OneGroup g2
removeGroup FirstGroup (ThreeGroups _ g2 g3) = Just $ TwoGroups g2 g3
removeGroup FirstGroup (FourGroups _ g2 g3 g4) = Just $ ThreeGroups g2 g3 g4
removeGroup SecondGroup (TwoGroups g1 _) = Just $ OneGroup g1
removeGroup SecondGroup (ThreeGroups g1 _ g3) = Just $ TwoGroups g1 g3
removeGroup SecondGroup (FourGroups g1 _ g3 g4) = Just $ ThreeGroups g1 g3 g4
removeGroup ThirdGroup (ThreeGroups g1 g2 _) = Just $ TwoGroups g1 g2
removeGroup ThirdGroup (FourGroups g1 g2 _ g4) = Just $ ThreeGroups g1 g2 g4
removeGroup FourthGroup (FourGroups g1 g2 g3 _) = Just $ ThreeGroups g1 g2 g3
removeGroup _ _ = Nothing

firstGroup :: Groups g -> g
firstGroup (OneGroup g1) = g1
firstGroup (TwoGroups g1 _) = g1
firstGroup (ThreeGroups g1 _ _) = g1
firstGroup (FourGroups g1 _ _ _) = g1

groupsToList :: Groups g -> NE.NonEmpty g
groupsToList (OneGroup g1) = [g1]
groupsToList (TwoGroups g1 g2) = [g1, g2]
groupsToList (ThreeGroups g1 g2 g3) = [g1, g2, g3]
groupsToList (FourGroups g1 g2 g3 g4) = [g1, g2, g3, g4]

groupsFromList :: NE.NonEmpty g -> Maybe (Groups g)
groupsFromList [g1]             = Just $ OneGroup    g1
groupsFromList [g1, g2]         = Just $ TwoGroups   g1 g2
groupsFromList [g1, g2, g3]     = Just $ ThreeGroups g1 g2 g3
groupsFromList [g1, g2, g3, g4] = Just $ FourGroups  g1 g2 g3 g4
groupsFromList _                = Nothing

indexedGroups :: Groups g -> NE.NonEmpty (GroupIndex, g)
indexedGroups = NE.zip (NE.fromList $ enumFromTo minBound maxBound) . groupsToList

groupIndexToInt :: GroupIndex -> Int
groupIndexToInt = (+1) . fromEnum


--- Group ---------------------------------------------------------------------

-- | [TODO] Smart constructor
mkGroup :: RawLText -> GroupLevels -> a -> Maybe (SingleOsGroup a)
mkGroup n ls a = Just $ Group n ls a

-- | [TODO] Smart constructor
mkRawGroup :: RawLText -> MultiOsGroupLevels -> MultiOsRawActionsKeyMap -> KeyMultiOsRawGroup
mkRawGroup n ls am = Group
  { _groupName = n
  , _groupLevels = ls
  , _groupMapping = am
  }

-- | Levels /defined/ by a group.
groupLevels :: SingleOsGroup a -> [Level]
groupLevels = Map.keys . _groupLevels

-- | Get the indexes and the names of /defined/ levels in a group
groupLevelsNames :: SingleOsGroup a -> [(Level, RawLText)]
groupLevelsNames = Map.toList . _groupLevels

-- | Get the indexes and the names of /defined/ levels in a group
groupIndexedLevelsNames :: SingleOsGroup a -> [(Int, RawLText)]
groupIndexedLevelsNames = zip [1..] . Map.elems . _groupLevels

-- | Get all the modifiers defined by a group
groupModifiersByLevel :: SingleOsGroup a -> [(Level, ModifiersCombo)]
groupModifiersByLevel = fmap (id &&& maskToModifiers) . groupLevels

-- | Get all the modifiers defined by a group
groupModifiers :: SingleOsGroup a -> ModifiersCombo
groupModifiers = maskToModifiers . mconcat . groupLevels


--- Mappings ------------------------------------------------------------------

resolveOsGroups :: OsSpecific -> MultiOsRawGroups -> RawGroups
resolveOsGroups os = fmap (resolveOsGroup os)

resolveGroups :: OsSpecific -> MultiOsRawGroups -> ResolvedGroups
resolveGroups os = fmap (resolveGroup os)

resolveFinalGroups :: OsSpecific -> MultiOsRawGroups -> FinalGroups
resolveFinalGroups os = fmap (fmap (Map.map untagCustomisableActions)) . resolveGroups os

resolveOsGroup :: OsSpecific -> MultiOsRawGroup k -> RawGroup k
resolveOsGroup os g@(Group _ lss ams) = g{_groupLevels=ls, _groupMapping=am}
  where
    ls = getOs os lss
    am = Map.map (multiOsRawActionsToRawActions os) ams

resolveGroup :: OsSpecific -> MultiOsRawGroup k -> ResolvedGroup k
resolveGroup os g@(Group _ lss ams) = g{_groupLevels=ls, _groupMapping=am}
  where
    ls = getOs os lss
    am = Map.map (multiOsRawActionsToResolvedActions os) ams

resolveGroupLayout :: OsSpecific -> MultiOsRawGroup k -> FinalGroup k
resolveGroupLayout os g@(Group _ lss ams) = g{_groupLevels=ls, _groupMapping=am}
  where
    ls = getOs os lss
    am = Map.map (untagCustomisableActions . multiOsRawActionsToResolvedActions os) ams

traverseGroupsMappings :: (Applicative f) => (Map.Map k a -> f (Map.Map k b)) -> GroupsMappings ls k a -> f (GroupsMappings ls k b)
traverseGroupsMappings f = traverse (traverse f)

itraverseGroupsLayouts_ :: (Applicative f) => (GroupIndex -> GroupMapping ls k a -> f b) -> GroupsMappings ls k a -> f ()
itraverseGroupsLayouts_ f = traverse_ (uncurry f) . indexedGroups

foldlGroupsMappingsWithKey :: (b -> k -> a -> b) -> b -> GroupsMappings ls k a -> b
foldlGroupsMappingsWithKey f = foldl' (foldlGroupMappingWithKey f)

foldlGroupsMappingActionsWithKey :: (b -> k -> ModifiersOptions -> Level -> Action -> b) -> b -> GroupsMappings ls k Actions -> b
foldlGroupsMappingActionsWithKey f = foldl' (foldlGroupMappingActionsWithKey f)

foldlGroupMappingWithKey :: (b -> k -> a -> b) -> b -> GroupMapping ls k a -> b
foldlGroupMappingWithKey f acc = Map.foldlWithKey' f acc . _groupMapping

foldlGroupMappingActionsWithKey :: (b -> k -> ModifiersOptions -> Level -> Action -> b) -> b -> GroupMapping ls k Actions -> b
foldlGroupMappingActionsWithKey f = foldlGroupMappingWithKey f'
  where f' acc k = foldlActions (`f` k) acc

foldlGroupMappingImplicitActionsWithKey :: (b -> k -> ModifiersOptions -> Level -> Action -> b) -> b -> FinalGroup k -> b
foldlGroupMappingImplicitActionsWithKey f acc0 g = foldlGroupMappingWithKey f' acc0 g
  where f' acc k = foldlImplicitActions ls (`f` k) acc
        ls = Map.keys $ _groupLevels g

foldrGroupMappingM :: (Monad m) => (a -> b -> m b) -> b -> GroupMapping ls k a -> m b
foldrGroupMappingM f acc = foldrMWithKey' (const f) acc . _groupMapping

foldrGroupMappingWithKeyM :: (Monad m) => (k -> a -> b -> m b) -> b -> GroupMapping ls k a -> m b
foldrGroupMappingWithKeyM f acc = foldrMWithKey' f acc . _groupMapping

foldlGroupsLayoutsWithIndexM :: (Monad m) => (b -> GroupIndex -> GroupMapping ls k a -> m b) -> b -> GroupsMappings ls k a -> m b
foldlGroupsLayoutsWithIndexM f b = foldlM (\b' (k, g) -> f b' k g) b . indexedGroups

firstGroupActionsMap :: GroupsMappings ls k a -> Map.Map k a
firstGroupActionsMap = _groupMapping . firstGroup

groupLayers :: FinalGroup k -> [(Level, Layer k)]
groupLayers g = (id &&& getLayer (_groupMapping g)) <$> groupLevels g

groupImplicitLayers :: FinalGroup k -> [(Level, Layer k)]
groupImplicitLayers g = (id &&& getImplicitLayer (_groupMapping g)) <$> groupLevels g

groupLayer :: Level -> FinalGroup k -> Layer k
groupLayer l = flip getLayer l . _groupMapping

groupImplicitLayer :: Level -> FinalGroup k -> Layer k
groupImplicitLayer l = flip getImplicitLayer l . _groupMapping

groupBaseLayer :: FinalGroup k -> Layer k
groupBaseLayer = groupLayer minBound

groupActionsMap :: ResolvedGroup k -> ActionsMap k
groupActionsMap = fmap untagCustomisableActions . _groupMapping
