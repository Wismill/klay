{-|
Description: Modelling of ISO levels

This module provides levels as defined in ISO/IEC&#xA0;9995-1, section&#xA0;5.1.
-}

module Klay.Keyboard.Layout.Level
  ( -- * Levels
    type Level
  , pattern Level
  , IsLevel(..)
  , IsLevels(..)
    -- * System Levels
  , M.control, M.alternate, M.altGr, M.super, M.numeric
  , defaultControlLevel
  , defaultControlShiftLevel
  , defaultAlternateLevel
  , defaultAltGrLevel
  , defaultAltGrShiftLevel
  , defaultNumericLevel
  , defaultNumericShiftLevel
    -- * ISO Levels
  , M.isoLevel1, M.isoLevel2, M.isoLevel2ShiftCaps
  , M.isoLevel3, M.isoLevel4, M.isoLevel4ShiftCaps
  , M.isoLevel5, M.isoLevel6, M.isoLevel6ShiftCaps
  , M.isoLevel7, M.isoLevel8, M.isoLevel8ShiftCaps
  , defaultAlphanumericLower
  , defaultAlphanumericUpper
  , defaultAlphanumericCaps
  , defaultIsoLevel1
  , defaultIsoLevel2
  , defaultIsoLevel2Caps
  , defaultIsoLevel3
  , defaultIsoLevel4
  , defaultIsoLevel4Caps
  , defaultIsoLevel5
  , defaultIsoLevel6
  , defaultIsoLevel6Caps
  , defaultIsoLevel7
  , defaultIsoLevel8
  , defaultIsoLevel8Caps
  ) where

import Klay.Keyboard.Layout.Modifier.Types (ModifiersField(..))
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Utils.UserInput (RawLText)

-- | Alias to 'ModifiersField' in the context of action maps.
type Level = ModifiersField

{-# COMPLETE Level #-}
pattern Level :: Int -> ModifiersField
pattern Level l <- ModifiersField l

-- | A class to simplify action maps construction.
class (Bounded l, Enum l) => IsLevel l where
  toLevel :: l -> Level

instance IsLevel Level where
  toLevel = id

-- | A class to simplify action maps construction.
class IsLevels ls where
  toLevels :: ls -> [Level]

instance IsLevels Level where
  toLevels l = [l]

instance (IsLevel l) => IsLevels (l, l) where
  toLevels (l1, l2) = toLevel <$> [l1, l2]

instance (IsLevel l) => IsLevels (l, l, l) where
  toLevels (l1, l2, l3) = toLevel <$> [l1, l2, l3]

instance (IsLevel l) => IsLevels (l, l, l, l) where
  toLevels (l1, l2, l3, l4) = toLevel <$> [l1, l2, l3, l4]

defaultControlLevel, defaultControlShiftLevel :: RawLText
defaultControlLevel      = "Control"
defaultControlShiftLevel = "Control + Shift"
defaultAlternateLevel :: RawLText
defaultAlternateLevel    = "Alt"
defaultAltGrLevel, defaultAltGrShiftLevel :: RawLText
defaultAltGrLevel        = "Control + Alt"
defaultAltGrShiftLevel   = "Control + Alt + Shift"
defaultNumericLevel, defaultNumericShiftLevel :: RawLText
defaultNumericLevel      = "NumLock"
defaultNumericShiftLevel = "NumLock + Shift"
defaultAlphanumericLower, defaultAlphanumericUpper, defaultAlphanumericCaps :: RawLText
defaultAlphanumericLower = "Lower Alphanumeric"
defaultAlphanumericUpper = "Upper Alphanumeric"
defaultAlphanumericCaps  = "Capitals Alphanumeric"
defaultIsoLevel1, defaultIsoLevel2, defaultIsoLevel2Caps :: RawLText
defaultIsoLevel3, defaultIsoLevel4, defaultIsoLevel4Caps :: RawLText
defaultIsoLevel5, defaultIsoLevel6, defaultIsoLevel6Caps :: RawLText
defaultIsoLevel7, defaultIsoLevel8, defaultIsoLevel8Caps :: RawLText
defaultIsoLevel1      = "ISO Level 1"
defaultIsoLevel2      = "ISO Level 2"
defaultIsoLevel2Caps  = "ISO Level 2 (Capitals)"
defaultIsoLevel3      = "ISO Level 3"
defaultIsoLevel4      = "ISO Level 4"
defaultIsoLevel4Caps  = "ISO Level 4 (Capitals)"
defaultIsoLevel5      = "ISO Level 5"
defaultIsoLevel6      = "ISO Level 6"
defaultIsoLevel6Caps  = "ISO Level 6 (Capitals)"
defaultIsoLevel7      = "ISO Level 7"
defaultIsoLevel8      = "ISO Level 8"
defaultIsoLevel8Caps  = "ISO Level 8 (Capitals)"

