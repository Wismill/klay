-- [NOTE] Auto-generated module. Do not edit manually

{- HLINT ignore "Use camelCase" -}

module Klay.Keyboard.Layout.Action.Special
  ( SpecialAction
    ( ..
    , Henkan_Mode
    , Kanji_Bangou
    , Zen_Koho
    , Mae_Koho
    , Page_Up
    , Page_Down
    , Mode_Switch
    , Script_Switch
    , KP_Page_Up
    , KP_Page_Down
    , L1
    , L2
    , L3
    , L4
    , L5
    , L6
    , L7
    , L8
    , L9
    , L10
    , R1
    , R2
    , R3
    , R4
    , R5
    , R6
    , R7
    , R8
    , R9
    , R10
    , R11
    , R12
    , R13
    , R14
    , R15
    , Kana_Switch
    , Arabic_Switch
    , Greek_Switch
    , Hebrew_Switch
    , Hangul_Codeinput
    , Hangul_Singlecandidate
    , Hangul_Multiplecandidate
    , Hangul_Previouscandidate
    , Hangul_Switch
    , Sunprint_Screen
    , SunAltGraph
    , SunPageUp
    , SunPageDown
    , SunUndo
    , SunAgain
    , SunFind
    , SunStop
    , CursorLeft
    , CursorRight
    , CursorUp
    , CursorDown )
  , specialActionToMonogram
  , specialPrefix
  ) where

-- [TODO] Re-enable Generic deriving when the following issues are solved:
-- • https://gitlab.haskell.org/ghc/ghc/-/issues/5642
-- • https://gitlab.haskell.org/ghc/ghc/-/issues/16577

import Prelude hiding (Left, Right)
import Data.Either qualified as E
import Data.Ix (Ix)
import Data.Hashable (Hashable(..))
import Data.Text qualified as T
-- import GHC.Generics (Generic)
import Control.DeepSeq (NFData(..))

import TextShow (TextShow, FromStringShow(..))
-- import TextShow.Generic (FromGeneric(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..))
import Test.QuickCheck.Gen (chooseEnum)
import Data.Aeson qualified as Aeson
import Data.Aeson.Types (ToJSON(..), FromJSON(..))

-- import Klay.Utils (jsonOptions)

{-|
An action that need special handling by the operating system.

Sources:

- @\/usr\/include\/X11\/keysymdef.h@
- @\/usr\/include\/X11\/XF86keysym.h@
-}
data SpecialAction
  = -- TTY function keys

    BackSpace -- ^ @0xff08@ U+0008
  | Tab -- ^ @0xff09@ U+0009
  | Return -- ^ @0xff0d@ U+000D
  | Pause -- ^ @0xff13@ Pause, hold
  | Scroll_Lock -- ^ @0xff14@
  | Sys_Req -- ^ @0xff15@
  | Escape -- ^ @0xff1b@ U+001B
  | Delete -- ^ @0xffff@ Delete, rubout

  -- International & multi-key character composition

  | Codeinput -- ^ @0xff37@
  | SingleCandidate -- ^ @0xff3c@
  | MultipleCandidate -- ^ @0xff3d@
  | PreviousCandidate -- ^ @0xff3e@

  -- Japanese keyboard support

  | Kanji -- ^ @0xff21@ Kanji, Kanji convert
  | Muhenkan -- ^ @0xff22@ Cancel Conversion
  | Henkan -- ^ @0xff23@ Alias for Henkan_Mode
  | Romaji -- ^ @0xff24@ to Romaji
  | Hiragana -- ^ @0xff25@ to Hiragana
  | Katakana -- ^ @0xff26@ to Katakana
  | Hiragana_Katakana -- ^ @0xff27@ Hiragana\/Katakana toggle
  | Zenkaku -- ^ @0xff28@ to Zenkaku
  | Hankaku -- ^ @0xff29@ to Hankaku
  | Zenkaku_Hankaku -- ^ @0xff2a@ Zenkaku\/Hankaku toggle
  | Touroku -- ^ @0xff2b@ Add to Dictionary
  | Massyo -- ^ @0xff2c@ Delete from Dictionary
  | Kana_Lock -- ^ @0xff2d@ Kana Lock
  | Kana_Shift -- ^ @0xff2e@ Kana Shift
  | Eisu_Shift -- ^ @0xff2f@ Alphanumeric Shift
  | Eisu_Toggle -- ^ @0xff30@ Alphanumeric toggle

  -- Cursor control & motion

  | Home -- ^ @0xff50@
  | Left -- ^ @0xff51@ Move left, left arrow
  | Up -- ^ @0xff52@ Move up, up arrow
  | Right -- ^ @0xff53@ Move right, right arrow
  | Down -- ^ @0xff54@ Move down, down arrow
  | Prior -- ^ @0xff55@ Prior, previous
  | Next -- ^ @0xff56@ Next
  | End -- ^ @0xff57@ EOL
  | Begin -- ^ @0xff58@ BOL

  -- Misc functions

  | Select -- ^ @0xff60@ Select, mark
  | Print -- ^ @0xff61@
  | Execute -- ^ @0xff62@ Execute, run, do
  | Insert -- ^ @0xff63@ Insert, insert here
  | Undo -- ^ @0xff65@
  | Redo -- ^ @0xff66@ Redo, again
  | Menu -- ^ @0xff67@
  | Find -- ^ @0xff68@ Find, search
  | Cancel -- ^ @0xff69@ Cancel, stop, abort, exit
  | Help -- ^ @0xff6a@ Help
  | Break -- ^ @0xff6b@

  -- Keypad functions, keypad numbers

  | KP_Space -- ^ @0xff80@ U+0020 SPACE
  | KP_Tab -- ^ @0xff89@ U+0009
  | KP_Enter -- ^ @0xff8d@ U+000D
  | KP_F1 -- ^ @0xff91@ PF1, KP_A, ...
  | KP_F2 -- ^ @0xff92@
  | KP_F3 -- ^ @0xff93@
  | KP_F4 -- ^ @0xff94@
  | KP_Home -- ^ @0xff95@
  | KP_Left -- ^ @0xff96@
  | KP_Up -- ^ @0xff97@
  | KP_Right -- ^ @0xff98@
  | KP_Down -- ^ @0xff99@
  | KP_Prior -- ^ @0xff9a@
  | KP_Next -- ^ @0xff9b@
  | KP_End -- ^ @0xff9c@
  | KP_Begin -- ^ @0xff9d@
  | KP_Insert -- ^ @0xff9e@
  | KP_Delete -- ^ @0xff9f@
  | KP_Equal -- ^ @0xffbd@ U+003D EQUALS SIGN
  | KP_Multiply -- ^ @0xffaa@ U+002A ASTERISK
  | KP_Add -- ^ @0xffab@ U+002B PLUS SIGN
  | KP_Separator -- ^ @0xffac@ U+002C COMMA
  | KP_Subtract -- ^ @0xffad@ U+002D HYPHEN-MINUS
  | KP_Decimal -- ^ @0xffae@ U+002E FULL STOP
  | KP_Divide -- ^ @0xffaf@ U+002F SOLIDUS
  | KP_0 -- ^ @0xffb0@ U+0030 DIGIT ZERO
  | KP_1 -- ^ @0xffb1@ U+0031 DIGIT ONE
  | KP_2 -- ^ @0xffb2@ U+0032 DIGIT TWO
  | KP_3 -- ^ @0xffb3@ U+0033 DIGIT THREE
  | KP_4 -- ^ @0xffb4@ U+0034 DIGIT FOUR
  | KP_5 -- ^ @0xffb5@ U+0035 DIGIT FIVE
  | KP_6 -- ^ @0xffb6@ U+0036 DIGIT SIX
  | KP_7 -- ^ @0xffb7@ U+0037 DIGIT SEVEN
  | KP_8 -- ^ @0xffb8@ U+0038 DIGIT EIGHT
  | KP_9 -- ^ @0xffb9@ U+0039 DIGIT NINE

  -- Auxiliary functions

  | F1 -- ^ @0xffbe@
  | F2 -- ^ @0xffbf@
  | F3 -- ^ @0xffc0@
  | F4 -- ^ @0xffc1@
  | F5 -- ^ @0xffc2@
  | F6 -- ^ @0xffc3@
  | F7 -- ^ @0xffc4@
  | F8 -- ^ @0xffc5@
  | F9 -- ^ @0xffc6@
  | F10 -- ^ @0xffc7@
  | F11 -- ^ @0xffc8@
  | F12 -- ^ @0xffc9@
  | F13 -- ^ @0xffca@
  | F14 -- ^ @0xffcb@
  | F15 -- ^ @0xffcc@
  | F16 -- ^ @0xffcd@
  | F17 -- ^ @0xffce@
  | F18 -- ^ @0xffcf@
  | F19 -- ^ @0xffd0@
  | F20 -- ^ @0xffd1@
  | F21 -- ^ @0xffd2@
  | F22 -- ^ @0xffd3@
  | F23 -- ^ @0xffd4@
  | F24 -- ^ @0xffd5@
  | F25 -- ^ @0xffd6@
  | F26 -- ^ @0xffd7@
  | F27 -- ^ @0xffd8@
  | F28 -- ^ @0xffd9@
  | F29 -- ^ @0xffda@
  | F30 -- ^ @0xffdb@
  | F31 -- ^ @0xffdc@
  | F32 -- ^ @0xffdd@
  | F33 -- ^ @0xffde@
  | F34 -- ^ @0xffdf@
  | F35 -- ^ @0xffe0@

  -- Modifiers

  | Meta_L -- ^ @0xffe7@ Left meta
  | Meta_R -- ^ @0xffe8@ Right meta
  | Hyper_L -- ^ @0xffed@ Left hyper
  | Hyper_R -- ^ @0xffee@ Right hyper

  -- Keyboard (XKB) Extension function and modifier keys

  | Iso_Group_Shift -- ^ @0xff7e@ Alias for mode_switch
  | Iso_Group_Latch -- ^ @0xfe06@
  | Iso_Group_Lock -- ^ @0xfe07@
  | Iso_Next_Group -- ^ @0xfe08@
  | Iso_Next_Group_Lock -- ^ @0xfe09@
  | Iso_Prev_Group -- ^ @0xfe0a@
  | Iso_Prev_Group_Lock -- ^ @0xfe0b@
  | Iso_First_Group -- ^ @0xfe0c@
  | Iso_First_Group_Lock -- ^ @0xfe0d@
  | Iso_Last_Group -- ^ @0xfe0e@
  | Iso_Last_Group_Lock -- ^ @0xfe0f@
  | Iso_Left_Tab -- ^ @0xfe20@
  | Iso_Move_Line_Up -- ^ @0xfe21@
  | Iso_Move_Line_Down -- ^ @0xfe22@
  | Iso_Partial_Line_Up -- ^ @0xfe23@
  | Iso_Partial_Line_Down -- ^ @0xfe24@
  | Iso_Partial_Space_Left -- ^ @0xfe25@
  | Iso_Partial_Space_Right -- ^ @0xfe26@
  | Iso_Set_Margin_Left -- ^ @0xfe27@
  | Iso_Set_Margin_Right -- ^ @0xfe28@
  | Iso_Release_Margin_Left -- ^ @0xfe29@
  | Iso_Release_Margin_Right -- ^ @0xfe2a@
  | Iso_Release_Both_Margins -- ^ @0xfe2b@
  | Iso_Fast_Cursor_Left -- ^ @0xfe2c@
  | Iso_Fast_Cursor_Right -- ^ @0xfe2d@
  | Iso_Fast_Cursor_Up -- ^ @0xfe2e@
  | Iso_Fast_Cursor_Down -- ^ @0xfe2f@
  | Iso_Continuous_Underline -- ^ @0xfe30@
  | Iso_Discontinuous_Underline -- ^ @0xfe31@
  | Iso_Emphasize -- ^ @0xfe32@
  | Iso_Center_Object -- ^ @0xfe33@
  | Iso_Enter -- ^ @0xfe34@

  -- Miscelleanous X11

  | First_Virtual_Screen -- ^ @0xfed0@
  | Prev_Virtual_Screen -- ^ @0xfed1@
  | Next_Virtual_Screen -- ^ @0xfed2@
  | Last_Virtual_Screen -- ^ @0xfed4@
  | Terminate_Server -- ^ @0xfed5@
  | Accessx_Enable -- ^ @0xfe70@
  | Accessx_Feedback_Enable -- ^ @0xfe71@
  | Repeatkeys_Enable -- ^ @0xfe72@
  | Slowkeys_Enable -- ^ @0xfe73@
  | Bouncekeys_Enable -- ^ @0xfe74@
  | Stickykeys_Enable -- ^ @0xfe75@
  | Mousekeys_Enable -- ^ @0xfe76@
  | Mousekeys_Accel_Enable -- ^ @0xfe77@
  | Overlay1_Enable -- ^ @0xfe78@
  | Overlay2_Enable -- ^ @0xfe79@
  | Audiblebell_Enable -- ^ @0xfe7a@
  | Pointer_Left -- ^ @0xfee0@
  | Pointer_Right -- ^ @0xfee1@
  | Pointer_Up -- ^ @0xfee2@
  | Pointer_Down -- ^ @0xfee3@
  | Pointer_Upleft -- ^ @0xfee4@
  | Pointer_Upright -- ^ @0xfee5@
  | Pointer_Downleft -- ^ @0xfee6@
  | Pointer_Downright -- ^ @0xfee7@
  | Pointer_Button_Dflt -- ^ @0xfee8@
  | Pointer_Button1 -- ^ @0xfee9@
  | Pointer_Button2 -- ^ @0xfeea@
  | Pointer_Button3 -- ^ @0xfeeb@
  | Pointer_Button4 -- ^ @0xfeec@
  | Pointer_Button5 -- ^ @0xfeed@
  | Pointer_Dblclick_Dflt -- ^ @0xfeee@
  | Pointer_Dblclick1 -- ^ @0xfeef@
  | Pointer_Dblclick2 -- ^ @0xfef0@
  | Pointer_Dblclick3 -- ^ @0xfef1@
  | Pointer_Dblclick4 -- ^ @0xfef2@
  | Pointer_Dblclick5 -- ^ @0xfef3@
  | Pointer_Drag_Dflt -- ^ @0xfef4@
  | Pointer_Drag1 -- ^ @0xfef5@
  | Pointer_Drag2 -- ^ @0xfef6@
  | Pointer_Drag3 -- ^ @0xfef7@
  | Pointer_Drag4 -- ^ @0xfef8@
  | Pointer_Drag5 -- ^ @0xfefd@
  | Pointer_Enablekeys -- ^ @0xfef9@
  | Pointer_Accelerate -- ^ @0xfefa@
  | Pointer_Dfltbtnnext -- ^ @0xfefb@
  | Pointer_Dfltbtnprev -- ^ @0xfefc@

  -- Single-Stroke Multiple-Character N-Graph Keysyms For The X Input Method

  | XXXch -- ^ @0xfea0@
  | Ch -- ^ @0xfea1@
  | CH -- ^ @0xfea2@
  | XXXc_h -- ^ @0xfea3@
  | C_h -- ^ @0xfea4@
  | C_H -- ^ @0xfea5@

  -- 3270 Terminal Keys

  | XXX3270_Duplicate -- ^ @0xfd01@
  | XXX3270_FieldMark -- ^ @0xfd02@
  | XXX3270_Right2 -- ^ @0xfd03@
  | XXX3270_Left2 -- ^ @0xfd04@
  | XXX3270_BackTab -- ^ @0xfd05@
  | XXX3270_EraseEOF -- ^ @0xfd06@
  | XXX3270_EraseInput -- ^ @0xfd07@
  | XXX3270_Reset -- ^ @0xfd08@
  | XXX3270_Quit -- ^ @0xfd09@
  | XXX3270_PA1 -- ^ @0xfd0a@
  | XXX3270_PA2 -- ^ @0xfd0b@
  | XXX3270_PA3 -- ^ @0xfd0c@
  | XXX3270_Test -- ^ @0xfd0d@
  | XXX3270_Attn -- ^ @0xfd0e@
  | XXX3270_CursorBlink -- ^ @0xfd0f@
  | XXX3270_AltCursor -- ^ @0xfd10@
  | XXX3270_KeyClick -- ^ @0xfd11@
  | XXX3270_Jump -- ^ @0xfd12@
  | XXX3270_Ident -- ^ @0xfd13@
  | XXX3270_Rule -- ^ @0xfd14@
  | XXX3270_Copy -- ^ @0xfd15@
  | XXX3270_Play -- ^ @0xfd16@
  | XXX3270_Setup -- ^ @0xfd17@
  | XXX3270_Record -- ^ @0xfd18@
  | XXX3270_ChangeScreen -- ^ @0xfd19@
  | XXX3270_DeleteWord -- ^ @0xfd1a@
  | XXX3270_ExSelect -- ^ @0xfd1b@
  | XXX3270_CursorSelect -- ^ @0xfd1c@
  | XXX3270_PrintScreen -- ^ @0xfd1d@
  | XXX3270_Enter -- ^ @0xfd1e@

  -- Technical (from the DEC VT330/VT420 Technical Character Set, http://vt100.net/charsets/technical.html)

  | Topleftsummation -- ^ @0x8b1@
  | Botleftsummation -- ^ @0x8b2@
  | Topvertsummationconnector -- ^ @0x8b3@
  | Botvertsummationconnector -- ^ @0x8b4@
  | Toprightsummation -- ^ @0x8b5@
  | Botrightsummation -- ^ @0x8b6@
  | Rightmiddlesummation -- ^ @0x8b7@

  -- Special (from the DEC VT100 Special Graphics Character Set)

  | Blank -- ^ @0x9df@

  -- Publishing (these are probably from a long forgotten DEC Publishing font that once shipped with DECwrite)

  | Marker -- ^ @0xabf@
  | Trademarkincircle -- ^ @0xacb@
  | Hexagram -- ^ @0xada@
  | Cursor -- ^ @0xaff@

  -- Korean

  | Hangul -- ^ @0xff31@ Hangul start\/stop(toggle)
  | Hangul_Start -- ^ @0xff32@ Hangul start
  | Hangul_End -- ^ @0xff33@ Hangul end, English start
  | Hangul_Hanja -- ^ @0xff34@ Start Hangul->Hanja Conversion
  | Hangul_Jamo -- ^ @0xff35@ Hangul Jamo mode
  | Hangul_Romaja -- ^ @0xff36@ Hangul Romaja mode
  | Hangul_Jeonja -- ^ @0xff38@ Jeonja mode
  | Hangul_Banja -- ^ @0xff39@ Banja mode
  | Hangul_Prehanja -- ^ @0xff3a@ Pre Hanja conversion
  | Hangul_Posthanja -- ^ @0xff3b@ Post Hanja conversion
  | Hangul_Special -- ^ @0xff3f@ Special symbols

  -- Ancient Hangul Consonant Characters

  | Hangul_Kkogjidalrinieung -- ^ @0xef3@

  -- Braille

  | Braille_Dot_1 -- ^ @0xfff1@
  | Braille_Dot_2 -- ^ @0xfff2@
  | Braille_Dot_3 -- ^ @0xfff3@
  | Braille_Dot_4 -- ^ @0xfff4@
  | Braille_Dot_5 -- ^ @0xfff5@
  | Braille_Dot_6 -- ^ @0xfff6@
  | Braille_Dot_7 -- ^ @0xfff7@
  | Braille_Dot_8 -- ^ @0xfff8@
  | Braille_Dot_9 -- ^ @0xfff9@
  | Braille_Dot_10 -- ^ @0xfffa@

  -- XF86

  | XF86ModeLock -- ^ @0x1008ff01@ Mode Switch Lock

  -- XF86: Backlight controls

  | XF86MonBrightnessUp -- ^ @0x1008ff02@ Monitor\/panel brightness
  | XF86MonBrightnessDown -- ^ @0x1008ff03@ Monitor\/panel brightness
  | XF86KbdLightOnOff -- ^ @0x1008ff04@ Keyboards may be lit
  | XF86KbdBrightnessUp -- ^ @0x1008ff05@ Keyboards may be lit
  | XF86KbdBrightnessDown -- ^ @0x1008ff06@ Keyboards may be lit
  | XF86MonBrightnessCycle -- ^ @0x1008ff07@ Monitor\/panel brightness

  -- XF86: Keys found on some "Internet" keyboards.

  | XF86Standby -- ^ @0x1008ff10@ System into standby mode
  | XF86AudioLowerVolume -- ^ @0x1008ff11@ Volume control down
  | XF86AudioMute -- ^ @0x1008ff12@ Mute sound from the system
  | XF86AudioRaiseVolume -- ^ @0x1008ff13@ Volume control up
  | XF86AudioPlay -- ^ @0x1008ff14@ Start playing of audio >
  | XF86AudioStop -- ^ @0x1008ff15@ Stop playing audio
  | XF86AudioPrev -- ^ @0x1008ff16@ Previous track
  | XF86AudioNext -- ^ @0x1008ff17@ Next track
  | XF86HomePage -- ^ @0x1008ff18@ Display user's home page
  | XF86Mail -- ^ @0x1008ff19@ Invoke user's mail program
  | XF86Start -- ^ @0x1008ff1a@ Start application
  | XF86Search -- ^ @0x1008ff1b@ Search
  | XF86AudioRecord -- ^ @0x1008ff1c@ Record audio application

  -- XF86: These are sometimes found on PDA's (e.g. Palm, PocketPC or elsewhere)

  | XF86Calculator -- ^ @0x1008ff1d@ Invoke calculator program
  | XF86Memo -- ^ @0x1008ff1e@ Invoke Memo taking program
  | XF86ToDoList -- ^ @0x1008ff1f@ Invoke To Do List program
  | XF86Calendar -- ^ @0x1008ff20@ Invoke Calendar program
  | XF86PowerDown -- ^ @0x1008ff21@ Deep sleep the system
  | XF86ContrastAdjust -- ^ @0x1008ff22@ Adjust screen contrast
  | XF86RockerUp -- ^ @0x1008ff23@ Rocker switches exist up
  | XF86RockerDown -- ^ @0x1008ff24@ and down
  | XF86RockerEnter -- ^ @0x1008ff25@ and let you press them

  -- XF86: Some more "Internet" keyboard symbols

  | XF86Back -- ^ @0x1008ff26@ Like back on a browser
  | XF86Forward -- ^ @0x1008ff27@ Like forward on a browser
  | XF86Stop -- ^ @0x1008ff28@ Stop current operation
  | XF86Refresh -- ^ @0x1008ff29@ Refresh the page
  | XF86PowerOff -- ^ @0x1008ff2a@ Power off system entirely
  | XF86WakeUp -- ^ @0x1008ff2b@ Wake up system from sleep
  | XF86Eject -- ^ @0x1008ff2c@ Eject device (e.g. DVD)
  | XF86ScreenSaver -- ^ @0x1008ff2d@ Invoke screensaver
  | XF86WWW -- ^ @0x1008ff2e@ Invoke web browser
  | XF86Sleep -- ^ @0x1008ff2f@ Put system to sleep
  | XF86Favorites -- ^ @0x1008ff30@ Show favorite locations
  | XF86AudioPause -- ^ @0x1008ff31@ Pause audio playing
  | XF86AudioMedia -- ^ @0x1008ff32@ Launch media collection app
  | XF86MyComputer -- ^ @0x1008ff33@ Display "My Computer" window
  | XF86VendorHome -- ^ @0x1008ff34@ Display vendor home web site
  | XF86LightBulb -- ^ @0x1008ff35@ Light bulb keys exist
  | XF86Shop -- ^ @0x1008ff36@ Display shopping web site
  | XF86History -- ^ @0x1008ff37@ Show history of web surfing
  | XF86OpenURL -- ^ @0x1008ff38@ Open selected URL
  | XF86AddFavorite -- ^ @0x1008ff39@ Add URL to favorites list
  | XF86HotLinks -- ^ @0x1008ff3a@ Show "hot" links
  | XF86BrightnessAdjust -- ^ @0x1008ff3b@ Invoke brightness adj. UI
  | XF86Finance -- ^ @0x1008ff3c@ Display financial site
  | XF86Community -- ^ @0x1008ff3d@ Display user's community
  | XF86AudioRewind -- ^ @0x1008ff3e@ "rewind" audio track
  | XF86BackForward -- ^ @0x1008ff3f@ ???
  | XF86Launch0 -- ^ @0x1008ff40@ Launch Application
  | XF86Launch1 -- ^ @0x1008ff41@ Launch Application
  | XF86Launch2 -- ^ @0x1008ff42@ Launch Application
  | XF86Launch3 -- ^ @0x1008ff43@ Launch Application
  | XF86Launch4 -- ^ @0x1008ff44@ Launch Application
  | XF86Launch5 -- ^ @0x1008ff45@ Launch Application
  | XF86Launch6 -- ^ @0x1008ff46@ Launch Application
  | XF86Launch7 -- ^ @0x1008ff47@ Launch Application
  | XF86Launch8 -- ^ @0x1008ff48@ Launch Application
  | XF86Launch9 -- ^ @0x1008ff49@ Launch Application
  | XF86LaunchA -- ^ @0x1008ff4a@ Launch Application
  | XF86LaunchB -- ^ @0x1008ff4b@ Launch Application
  | XF86LaunchC -- ^ @0x1008ff4c@ Launch Application
  | XF86LaunchD -- ^ @0x1008ff4d@ Launch Application
  | XF86LaunchE -- ^ @0x1008ff4e@ Launch Application
  | XF86LaunchF -- ^ @0x1008ff4f@ Launch Application
  | XF86ApplicationLeft -- ^ @0x1008ff50@ switch to application, left
  | XF86ApplicationRight -- ^ @0x1008ff51@ switch to application, right
  | XF86Book -- ^ @0x1008ff52@ Launch bookreader
  | XF86CD -- ^ @0x1008ff53@ Launch CD\/DVD player
  | XF86Calculater -- ^ @0x1008ff54@ Launch Calculater
  | XF86Clear -- ^ @0x1008ff55@ Clear window, screen
  | XF86Close -- ^ @0x1008ff56@ Close window
  | XF86Copy -- ^ @0x1008ff57@ Copy selection
  | XF86Cut -- ^ @0x1008ff58@ Cut selection
  | XF86Display -- ^ @0x1008ff59@ Output switch key
  | XF86DOS -- ^ @0x1008ff5a@ Launch DOS (emulation)
  | XF86Documents -- ^ @0x1008ff5b@ Open documents window
  | XF86Excel -- ^ @0x1008ff5c@ Launch spread sheet
  | XF86Explorer -- ^ @0x1008ff5d@ Launch file explorer
  | XF86Game -- ^ @0x1008ff5e@ Launch game
  | XF86Go -- ^ @0x1008ff5f@ Go to URL
  | XF86iTouch -- ^ @0x1008ff60@ Logitech iTouch- don't use
  | XF86LogOff -- ^ @0x1008ff61@ Log off system
  | XF86Market -- ^ @0x1008ff62@ ??
  | XF86Meeting -- ^ @0x1008ff63@ enter meeting in calendar
  | XF86MenuKB -- ^ @0x1008ff65@ distinguish keyboard from PB
  | XF86MenuPB -- ^ @0x1008ff66@ distinguish PB from keyboard
  | XF86MySites -- ^ @0x1008ff67@ Favourites
  | XF86New -- ^ @0x1008ff68@ New (folder, document...
  | XF86News -- ^ @0x1008ff69@ News
  | XF86OfficeHome -- ^ @0x1008ff6a@ Office home (old Staroffice)
  | XF86Open -- ^ @0x1008ff6b@ Open
  | XF86Option -- ^ @0x1008ff6c@ ??
  | XF86Paste -- ^ @0x1008ff6d@ Paste
  | XF86Phone -- ^ @0x1008ff6e@ Launch phone; dial number
  | XF86Q -- ^ @0x1008ff70@ Compaq's Q - don't use
  | XF86Reply -- ^ @0x1008ff72@ Reply e.g., mail
  | XF86Reload -- ^ @0x1008ff73@ Reload web page, file, etc.
  | XF86RotateWindows -- ^ @0x1008ff74@ Rotate windows e.g. xrandr
  | XF86RotationPB -- ^ @0x1008ff75@ don't use
  | XF86RotationKB -- ^ @0x1008ff76@ don't use
  | XF86Save -- ^ @0x1008ff77@ Save (file, document, state
  | XF86ScrollUp -- ^ @0x1008ff78@ Scroll window\/contents up
  | XF86ScrollDown -- ^ @0x1008ff79@ Scrool window\/contentd down
  | XF86ScrollClick -- ^ @0x1008ff7a@ Use XKB mousekeys instead
  | XF86Send -- ^ @0x1008ff7b@ Send mail, file, object
  | XF86Spell -- ^ @0x1008ff7c@ Spell checker
  | XF86SplitScreen -- ^ @0x1008ff7d@ Split window or screen
  | XF86Support -- ^ @0x1008ff7e@ Get support (??)
  | XF86TaskPane -- ^ @0x1008ff7f@ Show tasks
  | XF86Terminal -- ^ @0x1008ff80@ Launch terminal emulator
  | XF86Tools -- ^ @0x1008ff81@ toolbox of desktop\/app.
  | XF86Travel -- ^ @0x1008ff82@ ??
  | XF86UserPB -- ^ @0x1008ff84@ ??
  | XF86User1KB -- ^ @0x1008ff85@ ??
  | XF86User2KB -- ^ @0x1008ff86@ ??
  | XF86Video -- ^ @0x1008ff87@ Launch video player
  | XF86WheelButton -- ^ @0x1008ff88@ button from a mouse wheel
  | XF86Word -- ^ @0x1008ff89@ Launch word processor
  | XF86Xfer -- ^ @0x1008ff8a@
  | XF86ZoomIn -- ^ @0x1008ff8b@ zoom in view, map, etc.
  | XF86ZoomOut -- ^ @0x1008ff8c@ zoom out view, map, etc.
  | XF86Away -- ^ @0x1008ff8d@ mark yourself as away
  | XF86Messenger -- ^ @0x1008ff8e@ as in instant messaging
  | XF86WebCam -- ^ @0x1008ff8f@ Launch web camera app.
  | XF86MailForward -- ^ @0x1008ff90@ Forward in mail
  | XF86Pictures -- ^ @0x1008ff91@ Show pictures
  | XF86Music -- ^ @0x1008ff92@ Launch music application
  | XF86Battery -- ^ @0x1008ff93@ Display battery information
  | XF86Bluetooth -- ^ @0x1008ff94@ Enable\/disable Bluetooth
  | XF86WLAN -- ^ @0x1008ff95@ Enable\/disable WLAN
  | XF86UWB -- ^ @0x1008ff96@ Enable\/disable UWB
  | XF86AudioForward -- ^ @0x1008ff97@ fast-forward audio track
  | XF86AudioRepeat -- ^ @0x1008ff98@ toggle repeat mode
  | XF86AudioRandomPlay -- ^ @0x1008ff99@ toggle shuffle mode
  | XF86Subtitle -- ^ @0x1008ff9a@ cycle through subtitle
  | XF86AudioCycleTrack -- ^ @0x1008ff9b@ cycle through audio tracks
  | XF86CycleAngle -- ^ @0x1008ff9c@ cycle through angles
  | XF86FrameBack -- ^ @0x1008ff9d@ video: go one frame back
  | XF86FrameForward -- ^ @0x1008ff9e@ video: go one frame forward
  | XF86Time -- ^ @0x1008ff9f@ display, or shows an entry for time seeking
  | XF86Select -- ^ @0x1008ffa0@ Select button on joypads and remotes
  | XF86View -- ^ @0x1008ffa1@ Show a view options\/properties
  | XF86TopMenu -- ^ @0x1008ffa2@ Go to a top-level menu in a video
  | XF86Red -- ^ @0x1008ffa3@ Red button
  | XF86Green -- ^ @0x1008ffa4@ Green button
  | XF86Yellow -- ^ @0x1008ffa5@ Yellow button
  | XF86Blue -- ^ @0x1008ffa6@ Blue button
  | XF86Suspend -- ^ @0x1008ffa7@ Sleep to RAM
  | XF86Hibernate -- ^ @0x1008ffa8@ Sleep to disk
  | XF86TouchpadToggle -- ^ @0x1008ffa9@ Toggle between touchpad\/trackstick
  | XF86TouchpadOn -- ^ @0x1008ffb0@ The touchpad got switched on
  | XF86TouchpadOff -- ^ @0x1008ffb1@ The touchpad got switched off
  | XF86AudioMicMute -- ^ @0x1008ffb2@ Mute the Mic from the system
  | XF86Keyboard -- ^ @0x1008ffb3@ User defined keyboard related action
  | XF86WWAN -- ^ @0x1008ffb4@ Toggle WWAN (LTE, UMTS, etc.) radio
  | XF86RFKill -- ^ @0x1008ffb5@ Toggle radios on\/off
  | XF86AudioPreset -- ^ @0x1008ffb6@ Select equalizer preset, e.g. theatre-mode
  | XF86RotationLockToggle -- ^ @0x1008ffb7@ Toggle screen rotation lock on\/off
  | XF86FullScreen -- ^ @0x1008ffb8@ Toggle fullscreen

  -- XF86: Virtual terminals on some operating systems

  | XF86Switch_Vt_1 -- ^ @0x1008fe01@
  | XF86Switch_Vt_2 -- ^ @0x1008fe02@
  | XF86Switch_Vt_3 -- ^ @0x1008fe03@
  | XF86Switch_Vt_4 -- ^ @0x1008fe04@
  | XF86Switch_Vt_5 -- ^ @0x1008fe05@
  | XF86Switch_Vt_6 -- ^ @0x1008fe06@
  | XF86Switch_Vt_7 -- ^ @0x1008fe07@
  | XF86Switch_Vt_8 -- ^ @0x1008fe08@
  | XF86Switch_Vt_9 -- ^ @0x1008fe09@
  | XF86Switch_Vt_10 -- ^ @0x1008fe0a@
  | XF86Switch_Vt_11 -- ^ @0x1008fe0b@
  | XF86Switch_Vt_12 -- ^ @0x1008fe0c@
  | XF86Ungrab -- ^ @0x1008fe20@ force ungrab
  | XF86ClearGrab -- ^ @0x1008fe21@ kill application with grab
  | XF86Next_Vmode -- ^ @0x1008fe22@ next video mode available
  | XF86Prev_Vmode -- ^ @0x1008fe23@ prev. video mode available
  | XF86LogWindowTree -- ^ @0x1008fe24@ print window tree to log
  | XF86LogGrabInfo -- ^ @0x1008fe25@ print all active grabs to log

  -- Sun: Floating Accent

  | Sunfa_Grave -- ^ @0x1005ff00@
  | Sunfa_Circum -- ^ @0x1005ff01@
  | Sunfa_Tilde -- ^ @0x1005ff02@
  | Sunfa_Acute -- ^ @0x1005ff03@
  | Sunfa_Diaeresis -- ^ @0x1005ff04@
  | Sunfa_Cedilla -- ^ @0x1005ff05@

  -- Sun: Miscellaneous Functions

  | SunF36 -- ^ @0x1005ff10@ Labeled F11
  | SunF37 -- ^ @0x1005ff11@ Labeled F12
  | Sunsys_Req -- ^ @0x1005ff60@

  -- Sun: Open Look Functions

  | SunProps -- ^ @0x1005ff70@
  | SunFront -- ^ @0x1005ff71@
  | SunCopy -- ^ @0x1005ff72@
  | SunOpen -- ^ @0x1005ff73@
  | SunPaste -- ^ @0x1005ff74@
  | SunCut -- ^ @0x1005ff75@
  | SunPowerSwitch -- ^ @0x1005ff76@
  | SunAudioLowerVolume -- ^ @0x1005ff77@
  | SunAudioMute -- ^ @0x1005ff78@
  | SunAudioRaiseVolume -- ^ @0x1005ff79@
  | SunVideoDegauss -- ^ @0x1005ff7a@
  | SunVideoLowerBrightness -- ^ @0x1005ff7b@
  | SunVideoRaiseBrightness -- ^ @0x1005ff7c@
  | SunPowerSwitchShift -- ^ @0x1005ff7d@

  -- DEC private keysyms

  | Dring_Accent -- ^ @0x1000feb0@
  | Dcircumflex_Accent -- ^ @0x1000fe5e@
  | Dcedilla_Accent -- ^ @0x1000fe2c@
  | Dacute_Accent -- ^ @0x1000fe27@
  | Dgrave_Accent -- ^ @0x1000fe60@
  | Dtilde -- ^ @0x1000fe7e@
  | Ddiaeresis -- ^ @0x1000fe22@
  | DRemove -- ^ @0x1000ff00@ Remove

  -- HP keysyms

  | Hpclearline -- ^ @0x1000ff6f@
  | Hpinsertline -- ^ @0x1000ff70@
  | Hpdeleteline -- ^ @0x1000ff71@
  | Hpinsertchar -- ^ @0x1000ff72@
  | Hpdeletechar -- ^ @0x1000ff73@
  | Hpbacktab -- ^ @0x1000ff74@
  | Hpkp_Backtab -- ^ @0x1000ff75@
  | Hpmodelock1 -- ^ @0x1000ff48@
  | Hpmodelock2 -- ^ @0x1000ff49@
  | Hpreset -- ^ @0x1000ff6c@
  | Hpsystem -- ^ @0x1000ff6d@
  | Hpuser -- ^ @0x1000ff6e@
  | Hpblock -- ^ @0x100000fc@

  -- OSF keysyms

  | Osfcopy -- ^ @0x1004ff02@
  | Osfcut -- ^ @0x1004ff03@
  | Osfpaste -- ^ @0x1004ff04@
  | Osfbacktab -- ^ @0x1004ff07@
  | Osfbackspace -- ^ @0x1004ff08@
  | Osfclear -- ^ @0x1004ff0b@
  | Osfescape -- ^ @0x1004ff1b@
  | Osfaddmode -- ^ @0x1004ff31@
  | Osfprimarypaste -- ^ @0x1004ff32@
  | Osfquickpaste -- ^ @0x1004ff33@
  | Osfpageleft -- ^ @0x1004ff40@
  | Osfpageup -- ^ @0x1004ff41@
  | Osfpagedown -- ^ @0x1004ff42@
  | Osfpageright -- ^ @0x1004ff43@
  | Osfactivate -- ^ @0x1004ff44@
  | Osfmenubar -- ^ @0x1004ff45@
  | Osfleft -- ^ @0x1004ff51@
  | Osfup -- ^ @0x1004ff52@
  | Osfright -- ^ @0x1004ff53@
  | Osfdown -- ^ @0x1004ff54@
  | Osfendline -- ^ @0x1004ff57@
  | Osfbeginline -- ^ @0x1004ff58@
  | Osfenddata -- ^ @0x1004ff59@
  | Osfbegindata -- ^ @0x1004ff5a@
  | Osfprevmenu -- ^ @0x1004ff5b@
  | Osfnextmenu -- ^ @0x1004ff5c@
  | Osfprevfield -- ^ @0x1004ff5d@
  | Osfnextfield -- ^ @0x1004ff5e@
  | Osfselect -- ^ @0x1004ff60@
  | Osfinsert -- ^ @0x1004ff63@
  | Osfundo -- ^ @0x1004ff65@
  | Osfmenu -- ^ @0x1004ff67@
  | Osfcancel -- ^ @0x1004ff69@
  | Osfhelp -- ^ @0x1004ff6a@
  | Osfselectall -- ^ @0x1004ff71@
  | Osfdeselectall -- ^ @0x1004ff72@
  | Osfreselect -- ^ @0x1004ff73@
  | Osfextend -- ^ @0x1004ff74@
  | Osfrestore -- ^ @0x1004ff78@
  | Osfdelete -- ^ @0x1004ffff@
  -- Disabled because Generic generation is too slow
  -- deriving (Generic, NFData, Hashable, Eq, Ord, Ix, Enum, Bounded, Show)
  -- deriving TextShow via (FromGeneric SpecialAction)
  deriving (Eq, Ord, Ix, Enum, Bounded, Show, Read)
  deriving TextShow via (FromStringShow SpecialAction)

-- | Alias for 'Henkan'
pattern Henkan_Mode :: SpecialAction
pattern Henkan_Mode = Henkan

-- | Alias for 'Codeinput'
pattern Kanji_Bangou :: SpecialAction
pattern Kanji_Bangou = Codeinput

-- | Alias for 'MultipleCandidate'
pattern Zen_Koho :: SpecialAction
pattern Zen_Koho = MultipleCandidate

-- | Alias for 'PreviousCandidate'
pattern Mae_Koho :: SpecialAction
pattern Mae_Koho = PreviousCandidate

-- | Alias for 'Prior'
pattern Page_Up :: SpecialAction
pattern Page_Up = Prior

-- | Alias for 'Next'
pattern Page_Down :: SpecialAction
pattern Page_Down = Next

-- | Alias for 'Iso_Group_Shift'
pattern Mode_Switch :: SpecialAction
pattern Mode_Switch = Iso_Group_Shift

-- | Alias for 'Iso_Group_Shift'
pattern Script_Switch :: SpecialAction
pattern Script_Switch = Iso_Group_Shift

-- | Alias for 'KP_Prior'
pattern KP_Page_Up :: SpecialAction
pattern KP_Page_Up = KP_Prior

-- | Alias for 'KP_Next'
pattern KP_Page_Down :: SpecialAction
pattern KP_Page_Down = KP_Next

-- | Alias for 'F11'
pattern L1 :: SpecialAction
pattern L1 = F11

-- | Alias for 'F12'
pattern L2 :: SpecialAction
pattern L2 = F12

-- | Alias for 'F13'
pattern L3 :: SpecialAction
pattern L3 = F13

-- | Alias for 'F14'
pattern L4 :: SpecialAction
pattern L4 = F14

-- | Alias for 'F15'
pattern L5 :: SpecialAction
pattern L5 = F15

-- | Alias for 'F16'
pattern L6 :: SpecialAction
pattern L6 = F16

-- | Alias for 'F17'
pattern L7 :: SpecialAction
pattern L7 = F17

-- | Alias for 'F18'
pattern L8 :: SpecialAction
pattern L8 = F18

-- | Alias for 'F19'
pattern L9 :: SpecialAction
pattern L9 = F19

-- | Alias for 'F20'
pattern L10 :: SpecialAction
pattern L10 = F20

-- | Alias for 'F21'
pattern R1 :: SpecialAction
pattern R1 = F21

-- | Alias for 'F22'
pattern R2 :: SpecialAction
pattern R2 = F22

-- | Alias for 'F23'
pattern R3 :: SpecialAction
pattern R3 = F23

-- | Alias for 'F24'
pattern R4 :: SpecialAction
pattern R4 = F24

-- | Alias for 'F25'
pattern R5 :: SpecialAction
pattern R5 = F25

-- | Alias for 'F26'
pattern R6 :: SpecialAction
pattern R6 = F26

-- | Alias for 'F27'
pattern R7 :: SpecialAction
pattern R7 = F27

-- | Alias for 'F28'
pattern R8 :: SpecialAction
pattern R8 = F28

-- | Alias for 'F29'
pattern R9 :: SpecialAction
pattern R9 = F29

-- | Alias for 'F30'
pattern R10 :: SpecialAction
pattern R10 = F30

-- | Alias for 'F31'
pattern R11 :: SpecialAction
pattern R11 = F31

-- | Alias for 'F32'
pattern R12 :: SpecialAction
pattern R12 = F32

-- | Alias for 'F33'
pattern R13 :: SpecialAction
pattern R13 = F33

-- | Alias for 'F34'
pattern R14 :: SpecialAction
pattern R14 = F34

-- | Alias for 'F35'
pattern R15 :: SpecialAction
pattern R15 = F35

-- | Alias for 'Iso_Group_Shift'
pattern Kana_Switch :: SpecialAction
pattern Kana_Switch = Iso_Group_Shift

-- | Alias for 'Iso_Group_Shift'
pattern Arabic_Switch :: SpecialAction
pattern Arabic_Switch = Iso_Group_Shift

-- | Alias for 'Iso_Group_Shift'
pattern Greek_Switch :: SpecialAction
pattern Greek_Switch = Iso_Group_Shift

-- | Alias for 'Iso_Group_Shift'
pattern Hebrew_Switch :: SpecialAction
pattern Hebrew_Switch = Iso_Group_Shift

-- | Alias for 'Codeinput'
pattern Hangul_Codeinput :: SpecialAction
pattern Hangul_Codeinput = Codeinput

-- | Alias for 'SingleCandidate'
pattern Hangul_Singlecandidate :: SpecialAction
pattern Hangul_Singlecandidate = SingleCandidate

-- | Alias for 'MultipleCandidate'
pattern Hangul_Multiplecandidate :: SpecialAction
pattern Hangul_Multiplecandidate = MultipleCandidate

-- | Alias for 'PreviousCandidate'
pattern Hangul_Previouscandidate :: SpecialAction
pattern Hangul_Previouscandidate = PreviousCandidate

-- | Alias for 'Iso_Group_Shift'
pattern Hangul_Switch :: SpecialAction
pattern Hangul_Switch = Iso_Group_Shift

-- | Alias for 'Print'
pattern Sunprint_Screen :: SpecialAction
pattern Sunprint_Screen = Print

-- | Alias for 'Iso_Group_Shift'
pattern SunAltGraph :: SpecialAction
pattern SunAltGraph = Iso_Group_Shift

-- | Alias for 'Prior'
pattern SunPageUp :: SpecialAction
pattern SunPageUp = Prior

-- | Alias for 'Next'
pattern SunPageDown :: SpecialAction
pattern SunPageDown = Next

-- | Alias for 'Undo'
pattern SunUndo :: SpecialAction
pattern SunUndo = Undo

-- | Alias for 'Redo'
pattern SunAgain :: SpecialAction
pattern SunAgain = Redo

-- | Alias for 'Find'
pattern SunFind :: SpecialAction
pattern SunFind = Find

-- | Alias for 'Cancel'
pattern SunStop :: SpecialAction
pattern SunStop = Cancel

-- | Alias for 'Left'
pattern CursorLeft :: SpecialAction
pattern CursorLeft = Left

-- | Alias for 'Right'
pattern CursorRight :: SpecialAction
pattern CursorRight = Right

-- | Alias for 'Up'
pattern CursorUp :: SpecialAction
pattern CursorUp = Up

-- | Alias for 'Down'
pattern CursorDown :: SpecialAction
pattern CursorDown = Down

instance Hashable SpecialAction where
  hashWithSalt s = (+s) . fromEnum

instance NFData SpecialAction where
  rnf = (`seq` ())

instance ToJSON SpecialAction where
  -- toJSON     = Aeson.genericToJSON jsonOptions'
  -- toEncoding = Aeson.genericToEncoding jsonOptions'
  toJSON     = Aeson.String . serializeSpecialAction
  toEncoding = Aeson.toEncoding . serializeSpecialAction

instance FromJSON SpecialAction where
--   parseJSON = Aeson.genericParseJSON jsonOptions'
  parseJSON = Aeson.withText "special action" $
    either fail pure . parseSpecialAction

-- jsonOptions' :: Aeson.Options
-- jsonOptions' = (jsonOptions id)
--   { Aeson.constructorTagModifier = \case
--     'X':'X':'X':xs -> xs
--     xs             -> xs
--   }

serializeSpecialAction :: SpecialAction -> T.Text
serializeSpecialAction a = case show a of
  'X':'X':'X':xs -> T.pack xs
  xs             -> T.pack xs

parseSpecialAction :: T.Text -> Either String SpecialAction
parseSpecialAction t = let s = T.unpack t in case reads s of
  [(a, "")] -> E.Right a
  _         -> E.Left $ "Invalid special action: " <> s

instance Arbitrary SpecialAction where
  arbitrary = chooseEnum (minBound, maxBound)

specialPrefix :: T.Text
specialPrefix = "XXX"

-- | Source: [@libxkbcommon@](https://github.com/xkbcommon/libxkbcommon/blob/master/src/keysym-utf.c#L867)
specialActionToMonogram :: SpecialAction -> Maybe Char
specialActionToMonogram BackSpace    = Just '\x08'
specialActionToMonogram Tab          = Just '\x09'
specialActionToMonogram Return       = Just '\x0d'
specialActionToMonogram Escape       = Just '\x1b'
specialActionToMonogram KP_Space     = Just '\x20'
specialActionToMonogram KP_Tab       = Just '\x09'
specialActionToMonogram KP_Enter     = Just '\x0d'
specialActionToMonogram KP_Equal     = Just '\x3d'
specialActionToMonogram KP_Multiply  = Just '\x2a'
specialActionToMonogram KP_Add       = Just '\x2b'
specialActionToMonogram KP_Separator = Just '\x2c'
specialActionToMonogram KP_Subtract  = Just '\x2d'
specialActionToMonogram KP_Decimal   = Just '\x2e'
specialActionToMonogram KP_Divide    = Just '\x2f'
specialActionToMonogram KP_0         = Just '0'
specialActionToMonogram KP_1         = Just '1'
specialActionToMonogram KP_2         = Just '2'
specialActionToMonogram KP_3         = Just '3'
specialActionToMonogram KP_4         = Just '4'
specialActionToMonogram KP_5         = Just '5'
specialActionToMonogram KP_6         = Just '6'
specialActionToMonogram KP_7         = Just '7'
specialActionToMonogram KP_8         = Just '8'
specialActionToMonogram KP_9         = Just '9'
specialActionToMonogram _            = Nothing
