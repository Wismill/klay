module Klay.Keyboard.Layout.Action.DeadKey.Predefined
  ( predefinedDeadKeys
  , predefinedDeadKeysMap
  , dkFromBaseChar
  , deadKeysNames
  , namedDeadKeys
  ) where

import Data.Tuple (swap)
import Data.Text qualified as T
import Data.Map.Strict qualified as Map
import Control.Arrow ((&&&))

import Klay.Keyboard.Layout.Action.DeadKey (DeadKey(..))
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.Diacritics qualified as DK'
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.Currency qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.Latin qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.Science qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Circled qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Cyrillic qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Compose qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Games qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Greek qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Math qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Mirror qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.RomanNumerals qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Rotate qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.SuperSubScript qualified as DK

predefinedDeadKeys :: [DeadKey]
predefinedDeadKeys = mconcat
  [ fst <$> DK.allDiacritics
  , DK.allMathDeadKeys
  , DK.allRomanDeadKeys
  , [ DK.bépoCurrency
    , DK.bépoLatin
    , DK.bépoScience
    , DK'.crochetEnChef
    , DK.circled, DK.negativeCircled
    , DK.compose
    , DK.cyrillic
    , DK.games
    , DK.greek
    , DK.math
    , DK.mirror
    , DK.rotate
    , DK.superscript
    , DK.subscript
    ]
  ]

dkFromBaseChar :: Char -> Maybe DeadKey
dkFromBaseChar = (predefinedDeadKeysMap Map.!?)

predefinedDeadKeysMap :: Map.Map Char DeadKey
predefinedDeadKeysMap = Map.fromList $ fmap (_dkBaseChar &&& id) predefinedDeadKeys

-- | Map from dead keys to their corresponding XKB keysym
deadKeysNames :: Map.Map DeadKey T.Text
deadKeysNames = Map.fromList . fmap swap $ namedDeadKeysAssocs

-- | Dead key with corresponding XKB keysym
namedDeadKeys :: Map.Map T.Text DeadKey
namedDeadKeys = Map.fromList namedDeadKeysAssocs

namedDeadKeysAssocs ::[(T.Text, DeadKey)]
namedDeadKeysAssocs =
  [ ("compose", DK.compose)
  , ("grave", DK.graveAbove)
  , ("acute", DK.acuteAbove)
  , ("circumflex", DK.circumflexAbove)
  , ("tilde", DK.tildeAbove)
  -- , ("perispomeni", DK.tildeAbove)
  , ("macron", DK.macronAbove)
  , ("breve", DK.breveAbove)
  , ("abovedot", DK.dotAbove)
  , ("diaeresis", DK.diaeresisAbove)
  , ("abovering", DK.ringAbove)
  , ("doubleacute", DK.doubledAcute)
  , ("caron", DK.caronAbove)
  , ("cedilla", DK.cedillaBelow)
  , ("ogonek", DK.ogonek)
  -- , [TODO] ("dead_iota", todo)
  -- , [TODO] ("dead_voiced_sound", todo)
  -- , [TODO] ("dead_semivoiced_sound", todo)
  , ("belowdot", DK.dotBelow)
  -- , [TODO] ("dead_hook", todo)
  , ("horn", DK.horn)
  , ("stroke", DK.longStroke)
  , ("abovecomma", DK.commaAbove)
  -- , ("psili", DK.commaAbove)
  , ("abovereversedcomma", DK.reversedCommaAbove)
  -- , ("dasia", DK.reversedCommaAbove)
  , ("doublegrave", DK.doubledGrave)
  , ("belowring", DK.ringBelow)
  , ("belowmacron", DK.macronBelow)
  , ("belowcircumflex", DK.circumflexBelow)
  , ("belowtilde", DK.tildeBelow)
  , ("belowbreve", DK.breveBelow)
  , ("belowdiaeresis", DK.diaeresisBelow)
  , ("invertedbreve", DK.invertedBreve)
  , ("belowcomma", DK.commaBelow)
  , ("currency", DK.bépoCurrency)
  , ("lowline", DK.lowLine)
  , ("aboveverticalline", DK.verticalLineAbove)
  , ("belowverticalline", DK.verticalLineBelow)
  , ("longsolidusoverlay", DK.longSolidusOverlay)
  -- , [TODO] ("dead_a", todo)
  -- , [TODO] ("dead_A", todo)
  -- , [TODO] ("dead_e", todo)
  -- , [TODO] ("dead_E", todo)
  -- , [TODO] ("dead_i", todo)
  -- , [TODO] ("dead_I", todo)
  -- , [TODO] ("dead_o", todo)
  -- , [TODO] ("dead_O", todo)
  -- , [TODO] ("dead_u", todo)
  -- , [TODO] ("dead_U", todo)
  -- , [TODO] ("dead_small_schwa", todo)
  -- , [TODO] ("dead_capital_schwa", todo)
  , ("greek", DK.greek)
  ]
