{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.SuperSubScript
  ( superscript, superscriptDef
  , subscript, subscriptDef
  , superSubscriptDef
  ) where


import Data.Map.Strict qualified as Map

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.Unicode.SuperSubScript (superscriptMap, subscriptMap)


superscript :: DeadKey
superscript = DeadKey
  { _dkName = "Superscript"
  , _dkLabel = "ᵉ"
  , _dkBaseChar = 'ᵉ'
  }

superscriptDef :: RawDeadKeyDefinition
superscriptDef = (superscript,
  let f combos c1 c2 = char c1 c2 : combos
  in Map.foldlWithKey f mempty superscriptMap
  <> [ chainedDeadKey superscript subscript ] )


subscript :: DeadKey
subscript = DeadKey
  { _dkName = "Subscript"
  , _dkLabel = "ᵢ"
  , _dkBaseChar = 'ᵢ'
  }

subscriptDef :: RawDeadKeyDefinition
subscriptDef = (subscript,
  chainedDeadKey subscript superscript :
  let f combos c1 c2 = char c1 c2 : combos
  in Map.foldlWithKey f mempty subscriptMap)

superSubscriptDef :: RawDeadKeyDefinitions
superSubscriptDef = [superscriptDef, subscriptDef]
