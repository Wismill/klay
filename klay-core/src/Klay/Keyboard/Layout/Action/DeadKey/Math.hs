{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Math
  ( MathMnemonic(..)
  , MathAlphanumericMnemonic(..)
  , allMathDeadKeys
  , math, mathDef
  , includeMathSymbols
  , defaultAlphanumericMnemonic
  , mathematicalAlphanumericDef
  , mathematicalAlphanumericSerifItalic, mathematicalAlphanumericSerifItalicDef
  , mathematicalAlphanumericSerifBold, mathematicalAlphanumericSerifBoldDef
  , mathematicalAlphanumericSerifBoldItalic, mathematicalAlphanumericSerifBoldItalicDef
  , mathematicalAlphanumericSansSerifItalic, mathematicalAlphanumericSansSerifItalicDef
  , mathematicalAlphanumericSansSerifBold, mathematicalAlphanumericSansSerifBoldDef
  , mathematicalAlphanumericSansSerifBoldItalic, mathematicalAlphanumericSansSerifBoldItalicDef
  , mathematicalAlphanumericScript, mathematicalAlphanumericScriptDef
  , mathematicalAlphanumericScriptBold, mathematicalAlphanumericScriptBoldDef
  , mathematicalAlphanumericFraktur, mathematicalAlphanumericFrakturDef
  , mathematicalAlphanumericFrakturBold, mathematicalAlphanumericFrakturBoldDef
  , mathematicalAlphanumericDoubleStruck, mathematicalAlphanumericDoubleStruckDef
  ) where


import Data.List.NonEmpty qualified as NE

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Math.Alphanumeric

allMathDeadKeys :: [DeadKey]
allMathDeadKeys =
  [ math
  , mathematicalAlphanumericSerifItalic
  , mathematicalAlphanumericSerifBold
  , mathematicalAlphanumericSerifBoldItalic
  , mathematicalAlphanumericSansSerifItalic
  , mathematicalAlphanumericSansSerifBold
  , mathematicalAlphanumericSansSerifBoldItalic
  , mathematicalAlphanumericScript
  , mathematicalAlphanumericScriptBold
  , mathematicalAlphanumericFraktur
  , mathematicalAlphanumericFrakturBold
  , mathematicalAlphanumericDoubleStruck
  ]

math :: DeadKey
math = DeadKey
  { _dkName = "Math"
  , _dkLabel = "π"
  , _dkBaseChar = 'π'
  }

data MathMnemonic = MathMnemonic
  { _mNegation :: Char
  , _mSet :: Char
  , _mAlphanumeric :: MathAlphanumericMnemonic
  }

mathDef :: MathMnemonic -> RawDeadKeyDefinitions
mathDef mnemo = [(math, includeMathSymbols (mempty :: [Symbol]) mnemo)]

operators :: MathMnemonic -> [DeadKeyCombo]
operators MathMnemonic{_mNegation=𝐧} =
  [ char '-' '−'
  , char '*' '×'
  , char '/' '∕'
  , char 'o' '∘'
  , char '.' '∙'
  -- [TODO] Probably better let those directly in compose
  , chars [𝐧, '='] '≠'
  , chars ['=', '='] '≡'
  , chars ['=', 𝐧, '='] '≢' -- [FIXME]
  ]

sets :: MathMnemonic -> [DeadKeyCombo]
sets MathMnemonic{_mNegation=𝐧, _mSet=s} =
  [ chars [s, 'e'] '∈', char 'e' '∈'
  , chars [s, 𝐧, 'e'] '∉', chars [𝐧, 'e'] '∉'
  , chars ['c'] '⊂'
  , chars ['C'] '⊃' -- ⊂⊃⊆⊇⊊⊋⊈⊉⊄⊅⋂⋃∩∪∈∉∊∋∌
  , char 'N' 'ℕ'
  , char 'Z' 'ℤ'
  , char 'Q' 'ℚ'
  , char 'R' 'ℝ'
  -- , char 'C' 'ℂ'
  ]

logic :: MathMnemonic -> [DeadKeyCombo]
logic MathMnemonic{_mNegation=𝐧} =
  [ chars [𝐧, 𝐧] '¬'
  , char 'A' '∀'
  , char 'E' '∃'
  , chars [𝐧, 'E'] '∄'
  , char '&' '∧'
  , char '^' '∧'
  --, char '|' '∨'
  , char 'v' '∨'
  -- ⋀⋁∏∐∑∀∃∧∨
  ]

miscellaneous :: MathMnemonic -> [DeadKeyCombo]
miscellaneous MathMnemonic{} =
  [ char '8' '∞'
  ]

includeMathSymbols :: IsSymbol s => [s] -> MathMnemonic -> [DeadKeyCombo]
includeMathSymbols prefix mnemo = mconcat
  [ prepend $ operators mnemo
  , prepend $ logic mnemo
  , prepend $ miscellaneous mnemo
  , prepend $ sets mnemo
  , includeMathAlphanumeric prefix (_mAlphanumeric mnemo)
  ]
  where
    prepend
      | null prefix = id
      | otherwise   = fmap prepend'
    prefix' = toSymbol <$> prefix
    prepend' (DeadKeyCombo symbols result) = DeadKeyCombo (NE.fromList (prefix' <> NE.toList symbols)) result
