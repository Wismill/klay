{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Rotate
  ( rotate
  , rotateDef
  ) where


import Klay.Keyboard.Layout.Action.DeadKey hiding (mkCombo)


rotate :: DeadKey
rotate = DeadKey
  { _dkName = "Rotate"
  , _dkLabel = "⟳" -- Also: ↺↻⟲⟳
  , _dkBaseChar = '⟳'
  }


rotateDef :: RawDeadKeyDefinitions
rotateDef = [(rotate, mkCombo <$> definitions)]
  where mkCombo [c1, c2] = char c1 c2
        mkCombo c        = error $ "[ERROR] Invalid definition in Klay.Keyboard.Layout.Action.DeadKey.Rotate.rotateDef: " <> c


definitions :: [String]
definitions =
  [ "ɑɒ" -- [NOTE] Latin version
  , "ẟƍ" -- [NOTE] Latin version
  , "aɐ"
  , "AⱯ"
  , "bq" -- [TODO] capital
  , "cɔ"
  , "CƆ"
  --, "eǝ" -- [FIXME] Conflict
  , "eə" -- Schwa
  , "ƎE"
  , "fⅎ"
  , "FℲ"
  , "gᵷ"
  , "G⅁"
  , "hɥ"
  , "HꞍ"
  , "iᴉ"
  , "kʞ"
  , "KꞰ"
  , "lꞁ"
  , "LꞀ"
  , "mɯ"
  , "Mꟺ"
  -- [TODO] ᴟ
  , "pd"
  , "rɹ"
  , "Rɺ"
  , "tʇ"
  , "TꞱ"
  , "vʌ"
  , "VɅ"
  , "wʍ"
  , "yʎ"
  , "Y⅄"
  , "æᴂ"
  , "œᴔ"
  , "2↊"
  , "3↋"
  , "8∞"
  , "&⅋"
  , ",⸲"
  , ";⸵"
  --, "A∀" [FIXME] Conflict
  --, "E∃" [FIXME] Conflict
  , "…⋮"
  , "¬⌙"
  , "*⁎"
  , "⊲⊳"
  , "⋬⋭"
  , "⊢⊣"
  , "∈∋"
  , "∉∌"
  ]
