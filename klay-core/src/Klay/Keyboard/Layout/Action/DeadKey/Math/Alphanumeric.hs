{-# LANGUAGE OverloadedLists   #-}

{-|
Description: <https://en.wikipedia.org/wiki/Mathematical_Alphanumeric_Symbols Unicode math alphanumeric symbols>
-}

module Klay.Keyboard.Layout.Action.DeadKey.Math.Alphanumeric
  ( MathAlphanumericMnemonic(..)
  , defaultAlphanumericMnemonic
  , includeMathAlphanumeric
  , mathematicalAlphanumericDef
  , mathematicalAlphanumericSerifItalic, mathematicalAlphanumericSerifItalicDef
  , mathematicalAlphanumericSerifBold, mathematicalAlphanumericSerifBoldDef
  , mathematicalAlphanumericSerifBoldItalic, mathematicalAlphanumericSerifBoldItalicDef
  , mathematicalAlphanumericSansSerifItalic, mathematicalAlphanumericSansSerifItalicDef
  , mathematicalAlphanumericSansSerifBold, mathematicalAlphanumericSansSerifBoldDef
  , mathematicalAlphanumericSansSerifBoldItalic, mathematicalAlphanumericSansSerifBoldItalicDef
  , mathematicalAlphanumericScript, mathematicalAlphanumericScriptDef
  , mathematicalAlphanumericScriptBold, mathematicalAlphanumericScriptBoldDef
  , mathematicalAlphanumericFraktur, mathematicalAlphanumericFrakturDef
  , mathematicalAlphanumericFrakturBold, mathematicalAlphanumericFrakturBoldDef
  , mathematicalAlphanumericDoubleStruck, mathematicalAlphanumericDoubleStruckDef
  ) where

import Data.Maybe (mapMaybe)
import Data.List.NonEmpty qualified as NE
import Data.Functor ((<&>))
import Control.Applicative ((<|>))

import Data.Char.Core qualified as U
import Data.Char.Math qualified as U

import Klay.Keyboard.Layout.Action.DeadKey

-- | Mnemonics for mathematical alphanumeric characters
data MathAlphanumericMnemonic = MathAlphanumericMnemonic
  { _mathSerifItalic :: Maybe Char         -- ^ Serif italic
  , _mathSerifBold :: Maybe Char           -- ^ Serif bold
  , _mathSerifBoldItalic :: Maybe Char     -- ^ Serif bold and italic
  , _mathSansSerifItalic :: Maybe Char     -- ^ Sans-serif italic
  , _mathSansSerifBold :: Maybe Char       -- ^ Sans-serif bold
  , _mathSansSerifBoldItalic :: Maybe Char -- ^ Sans-serif bold and italic
  , _mathFraktur :: Maybe Char             -- ^ Fraktur
  , _mathFrakturBold :: Maybe Char         -- ^ Fraktur bold
  , _mathScript :: Maybe Char              -- ^ Script
  , _mathScriptBold :: Maybe Char          -- ^ Script bold
  , _mathDoubleStruck :: Maybe Char        -- ^ Double struck
  }

instance Semigroup MathAlphanumericMnemonic where
  (MathAlphanumericMnemonic a1 b1 c1 d1 e1 f1 g1 h1 i1 j1 k1) <> (MathAlphanumericMnemonic a2 b2 c2 d2 e2 f2 g2 h2 i2 j2 k2) =
    MathAlphanumericMnemonic
      (a2 <|> a1)
      (b2 <|> b1)
      (c2 <|> c1)
      (d2 <|> d1)
      (e2 <|> e1)
      (f2 <|> f1)
      (g2 <|> g1)
      (h2 <|> h1)
      (i2 <|> i1)
      (j2 <|> j1)
      (k2 <|> k1)

instance Monoid MathAlphanumericMnemonic where
  mempty = MathAlphanumericMnemonic Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

includeMathAlphanumeric :: (IsSymbol s) => [s] -> MathAlphanumericMnemonic -> [DeadKeyCombo]
includeMathAlphanumeric prefix mnemonics = mconcat . mapMaybe mkInclude $
  [ (_mathSerifItalic, mathematicalAlphanumericSerifItalicDef)
  , (_mathSerifBoldItalic, mathematicalAlphanumericSerifBoldItalicDef)
  , (_mathSerifBold, mathematicalAlphanumericSerifBoldDef)
  , (_mathSansSerifItalic, mathematicalAlphanumericSansSerifItalicDef)
  , (_mathSansSerifBoldItalic, mathematicalAlphanumericSansSerifBoldItalicDef)
  , (_mathSansSerifBold, mathematicalAlphanumericSansSerifBoldDef)
  , (_mathFraktur, mathematicalAlphanumericFrakturDef)
  , (_mathFrakturBold, mathematicalAlphanumericFrakturBoldDef)
  , (_mathScript, mathematicalAlphanumericScriptDef)
  , (_mathScriptBold, mathematicalAlphanumericScriptBoldDef)
  , (_mathDoubleStruck, mathematicalAlphanumericDoubleStruckDef)
  ]
  where
    mkInclude (get_mnemonic, defs) = get_mnemonic mnemonics
                                 <&> \m -> includeDefsWithPrefix (NE.fromList $ (toSymbol <$> prefix) <> [SChar m]) defs

-- | Default mnemonics
defaultAlphanumericMnemonic :: MathAlphanumericMnemonic
defaultAlphanumericMnemonic = MathAlphanumericMnemonic
  { _mathSerifItalic = Just 'i'
  , _mathSerifBold = Just 'b'
  , _mathSerifBoldItalic = Just 'I'
  , _mathSansSerifItalic = Nothing
  , _mathSansSerifBold = Nothing
  , _mathSansSerifBoldItalic = Nothing
  , _mathFraktur = Just 'f'
  , _mathFrakturBold = Just 'F'
  , _mathScript = Just 's'
  , _mathScriptBold = Just 'S'
  , _mathDoubleStruck = Just 'd'
  }

alphanumeric, latin, greek, digits :: String
alphanumeric = mconcat [latin, greek, digits]
latin = ['A'..'Z'] <> ['a'..'z']
greek = "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ∇ϴαβγδεζηθικλμνξοπρςστυφχψω∂ϵϑϰϕϱϖ"
digits = ['0'..'9']

mathematicalAlphanumericDef :: RawDeadKeyDefinitions
mathematicalAlphanumericDef = mconcat
  [ mathematicalAlphanumericSerifItalicDef
  , mathematicalAlphanumericSerifBoldDef
  , mathematicalAlphanumericSerifBoldItalicDef
  , mathematicalAlphanumericSansSerifItalicDef
  , mathematicalAlphanumericSansSerifBoldDef
  , mathematicalAlphanumericSansSerifBoldItalicDef
  , mathematicalAlphanumericScriptDef
  , mathematicalAlphanumericScriptBoldDef
  , mathematicalAlphanumericFrakturDef
  , mathematicalAlphanumericFrakturBoldDef
  , mathematicalAlphanumericDoubleStruckDef
  ]

-- | Dead key for Math Alphanumeric Serif Italic
mathematicalAlphanumericSerifItalic :: DeadKey
mathematicalAlphanumericSerifItalic = DeadKey
  { _dkName = "Math Alphanumeric Serif Italic"
  , _dkLabel = "𝑀"
  , _dkBaseChar = '𝑀'
  }

mathematicalAlphanumericSerifItalicDef :: RawDeadKeyDefinitions
mathematicalAlphanumericSerifItalicDef = [
  ( mathematicalAlphanumericSerifItalic
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.math U.Serif U.Italic U.NoBold c) alphanumeric
  )]


mathematicalAlphanumericSerifBold :: DeadKey
mathematicalAlphanumericSerifBold = DeadKey
  { _dkName = "Math Alphanumeric Serif Bold"
  , _dkLabel = "𝐌"
  , _dkBaseChar = '𝐌'
  }

mathematicalAlphanumericSerifBoldDef :: RawDeadKeyDefinitions
mathematicalAlphanumericSerifBoldDef = [
  ( mathematicalAlphanumericSerifBold
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.math U.Serif U.NoItalic U.Bold c) alphanumeric
  )]

mathematicalAlphanumericSerifBoldItalic :: DeadKey
mathematicalAlphanumericSerifBoldItalic = DeadKey
  { _dkName = "Math Alphanumeric Serif Bold Italic"
  , _dkLabel = "𝑴"
  , _dkBaseChar = '𝑴'
  }

mathematicalAlphanumericSerifBoldItalicDef :: RawDeadKeyDefinitions
mathematicalAlphanumericSerifBoldItalicDef = [
  ( mathematicalAlphanumericSerifBoldItalic
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.math U.Serif U.Italic U.Bold c) alphanumeric
  )]

mathematicalAlphanumericSansSerifItalic :: DeadKey
mathematicalAlphanumericSansSerifItalic = DeadKey
  { _dkName = "Math Alphanumeric Sans Serif Italic"
  , _dkLabel = "𝘔"
  , _dkBaseChar = '𝘔'
  }

mathematicalAlphanumericSansSerifItalicDef :: RawDeadKeyDefinitions
mathematicalAlphanumericSansSerifItalicDef = [
  ( mathematicalAlphanumericSansSerifItalic
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.math U.SansSerif U.Italic U.NoBold c) alphanumeric
  )]


mathematicalAlphanumericSansSerifBold :: DeadKey
mathematicalAlphanumericSansSerifBold = DeadKey
  { _dkName = "Math Alphanumeric Sans Serif Bold"
  , _dkLabel = "𝗠"
  , _dkBaseChar = '𝗠'
  }

mathematicalAlphanumericSansSerifBoldDef :: RawDeadKeyDefinitions
mathematicalAlphanumericSansSerifBoldDef = [
  ( mathematicalAlphanumericSansSerifBold
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.math U.SansSerif U.NoItalic U.Bold c) alphanumeric
  )]


mathematicalAlphanumericSansSerifBoldItalic :: DeadKey
mathematicalAlphanumericSansSerifBoldItalic = DeadKey
  { _dkName = "Math Alphanumeric Sans Serif Bold Italic"
  , _dkLabel = "𝙈"
  , _dkBaseChar = '𝙈'
  }

mathematicalAlphanumericSansSerifBoldItalicDef :: RawDeadKeyDefinitions
mathematicalAlphanumericSansSerifBoldItalicDef = [
  ( mathematicalAlphanumericSansSerifBoldItalic
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.math U.SansSerif U.Italic U.Bold c) alphanumeric
  )]


mathematicalAlphanumericScript :: DeadKey
mathematicalAlphanumericScript = DeadKey
  { _dkName = "Math Alphanumeric Script"
  , _dkLabel = "ℳ"
  , _dkBaseChar = 'ℳ'
  }

mathematicalAlphanumericScriptDef :: RawDeadKeyDefinitions
mathematicalAlphanumericScriptDef = [
  ( mathematicalAlphanumericScript
  , uncurry char <$> mapMaybe (\c -> (c,) <$> U.script U.NoBold c) latin
  )]



mathematicalAlphanumericScriptBold :: DeadKey
mathematicalAlphanumericScriptBold = DeadKey
  { _dkName = "Math Alphanumeric Script Bold"
  , _dkLabel = "𝓜"
  , _dkBaseChar = '𝓜'
  }

mathematicalAlphanumericScriptBoldDef :: RawDeadKeyDefinitions
mathematicalAlphanumericScriptBoldDef =
  [ ( mathematicalAlphanumericScriptBold
    , uncurry char <$> mapMaybe (\c -> (c,) <$> U.script U.Bold c) latin)
  ]


mathematicalAlphanumericFraktur :: DeadKey
mathematicalAlphanumericFraktur = DeadKey
  { _dkName = "Math Alphanumeric Fraktur"
  , _dkLabel = "𝔐"
  , _dkBaseChar = '𝔐'
  }

mathematicalAlphanumericFrakturDef :: RawDeadKeyDefinitions
mathematicalAlphanumericFrakturDef =
  [ ( mathematicalAlphanumericFraktur
    , uncurry char <$> mapMaybe (\c -> (c,) <$> U.fraktur U.NoBold c) latin)
  ]


mathematicalAlphanumericFrakturBold :: DeadKey
mathematicalAlphanumericFrakturBold = DeadKey
  { _dkName = "Math Alphanumeric Fraktur Bold"
  , _dkLabel = "𝕸"
  , _dkBaseChar = '𝕸'
  }

mathematicalAlphanumericFrakturBoldDef :: RawDeadKeyDefinitions
mathematicalAlphanumericFrakturBoldDef =
  [ ( mathematicalAlphanumericFrakturBold
    , uncurry char <$> mapMaybe (\c -> (c,) <$> U.fraktur U.Bold c) latin)
  ]


mathematicalAlphanumericDoubleStruck :: DeadKey
mathematicalAlphanumericDoubleStruck = DeadKey
  { _dkName = "Math Alphanumeric Double Struck"
  , _dkLabel = "𝕄"
  , _dkBaseChar = '𝕄'
  }

mathematicalAlphanumericDoubleStruckDef :: RawDeadKeyDefinitions
mathematicalAlphanumericDoubleStruckDef =
  [ ( mathematicalAlphanumericDoubleStruck
    , uncurry char <$> mapMaybe (\c -> (c,) <$> U.doubleStruck c) alphanumeric)
  ]
