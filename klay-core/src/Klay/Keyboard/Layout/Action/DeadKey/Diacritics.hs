-- [NOTE] Auto-generated module. Do not edit manually

{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Diacritics
  ( allDiacritics, diacriticsDef
  -- * Diacritics: Accents
  , graveAbove, graveAboveDef
  , graveBelow, graveBelowDef
  , doubleGrave, doubleGraveDef
  , doubledGrave
  , acuteAbove, acuteAboveDef
  , acuteBelow, acuteBelowDef
  , doubleAcute, doubleAcuteDef
  , doubledAcute
  , circumflexAbove, circumflexAboveDef
  , circumflexBelow, circumflexBelowDef
  , doubledCircumflexAbove, doubledCircumflexAboveDef
  , hatchekAbove, hatchekAboveDef
  , caronAbove
  , hatchekBelow, hatchekBelowDef
  , caronBelow
  -- * Diacritics: Curves
  , tildeAbove, tildeAboveDef
  , tildeBelow, tildeBelowDef
  , doubleTildeAbove, doubleTildeAboveDef
  , breveAbove, breveAboveDef
  , breveBelow, breveBelowDef
  , doubleBreveAbove, doubleBreveAboveDef
  , doubleBreveBelow, doubleBreveBelowDef
  , invertedBreveAbove, invertedBreveAboveDef
  , invertedBreveBelow, invertedBreveBelowDef
  , doubleInvertedBreveAbove, doubleInvertedBreveAboveDef
  , doubleInvertedBreveBelow, doubleInvertedBreveBelowDef
  , horn, hornDef
  , hookAbove, hookAboveDef
  , palatalizedHookBelow, palatalizedHookBelowDef
  , retroflexHookBelow, retroflexHookBelowDef
  -- * Diacritics: Macrons
  , macronAbove, macronAboveDef
  , macronBelow, macronBelowDef
  , doubleMacronAbove, doubleMacronAboveDef
  , doubleMacronBelow, doubleMacronBelowDef
  , overline, overlineDef
  , lowLine, lowLineDef
  , doubleOverline, doubleOverlineDef
  , doubleLowLine, doubleLowLineDef
  -- * Diacritics: Dots
  , dotAbove, dotAboveDef
  , dotBelow, dotBelowDef
  , diaeresisAbove, diaeresisAboveDef
  , diaeresisBelow, diaeresisBelowDef
  , threeDotsAbove, threeDotsAboveDef
  , threeDotsBelow, threeDotsBelowDef
  , fourDotsAbove, fourDotsAboveDef
  -- * Diacritics: Overlays
  , shortStroke, shortStrokeDef
  , longStroke, longStrokeDef
  , doubledLongStroke, doubledLongStrokeDef
  , shortVerticalLine, shortVerticalLineDef
  , longVerticalLine, longVerticalLineDef
  , doubledLongVerticalLine, doubledLongVerticalLineDef
  , verticalLineAbove, verticalLineAboveDef
  , doubledVerticalLineAbove, doubledVerticalLineAboveDef
  , verticalLineBelow, verticalLineBelowDef
  , doubledVerticalLineBelow, doubledVerticalLineBelowDef
  , shortSolidus, shortSolidusDef
  , longSolidus, longSolidusDef
  , reverseLongSolidus, reverseLongSolidusDef
  , doubledLongSolidus, doubledLongSolidusDef
  , tildeOverlay, tildeOverlayDef
  -- * Diacritics: Rings
  , ringAbove, ringAboveDef
  , ringBelow, ringBelowDef
  , doubledRingBelow, doubledRingBelowDef
  -- * Diacritics: Curls
  , cedillaBelow, cedillaBelowDef
  , cedillaAbove
  , commaAbove, commaAboveDef
  , commaAboveRight, commaAboveRightDef
  , reversedCommaAbove, reversedCommaAboveDef
  , commaBelow, commaBelowDef
  , turnedCommaAbove, turnedCommaAboveDef
  , ogonek, ogonekDef
  -- * Diacritics: Miscellaneous
  , plusSignBelow, plusSignBelowDef
  , minusSignBelow, minusSignBelowDef
  -- * Symbols: Overlay
  , ringOverlay, ringOverlayDef
  , clockwiseRingOverlay, clockwiseRingOverlayDef
  , anticlockwiseRingOverlay, anticlockwiseRingOverlayDef
  -- * Symbols: Enclosing
  , enclosingCircle, enclosingCircleDef
  , enclosingSquare, enclosingSquareDef
  -- * Symbols: Arrow
  , rightwardsArrowAbove, rightwardsArrowAboveDef
  , rightwardsArrowBelow, rightwardsArrowBelowDef
  , doubleRightwardsArrowBelow, doubleRightwardsArrowBelowDef
  -- * Symbols: Miscellaneous
  , wigglyLineBelow, wigglyLineBelowDef
  -- * Block: Combining Diacritical Marks
  , graveAccent
  , acuteAccent
  , circumflexAccent
  , tilde
  , macron
  , breve
  , diaeresis
  , doubleAcuteAccent
  , caron
  , doubleVerticalLineAbove
  , doubleGraveAccent
  , invertedBreve
  , graveAccentBelow
  , acuteAccentBelow
  , cedilla
  , circumflexAccentBelow
  , shortStrokeOverlay
  , longStrokeOverlay
  , shortSolidusOverlay
  , longSolidusOverlay
  , graveToneMark, graveToneMarkDef
  , acuteToneMark, acuteToneMarkDef
  , greekPerispomeni, greekPerispomeniDef
  , greekKoronis, greekKoronisDef
  , greekDialytikaTonos, greekDialytikaTonosDef
  , greekYpogegrammeni, greekYpogegrammeniDef
  , doubleVerticalLineBelow
  , doubleRingBelow
  , doubleBreve
  , doubleMacron
  , doubleTilde
  , doubleInvertedBreve
  -- * Block: Arabic
  , arabicMaddahAbove, arabicMaddahAboveDef
  , arabicHamzaAbove, arabicHamzaAboveDef
  , arabicHamzaBelow, arabicHamzaBelowDef
  -- * Block: Devanagari
  , devanagariSignNukta, devanagariSignNuktaDef
  -- * Block: Telugu
  , teluguAiLengthMark, teluguAiLengthMarkDef
  -- * Block: Sinhala
  , sinhalaSignAlLakuna, sinhalaSignAlLakunaDef
  -- * Block: Combining Diacritical Marks Extended
  , doubledCircumflexAccent
  -- * Block: Combining Diacritical Marks for Symbols
  , longVerticalLineOverlay
  , shortVerticalLineOverlay
  , rightArrowAbove
  , reverseSolidusOverlay
  , doubleVerticalStrokeOverlay
  , tripleUnderdot
  , longDoubleSolidusOverlay
  , rightArrowBelow
  -- * Block: Hiragana
  , katakanaHiraganaVoicedSoundMark, katakanaHiraganaVoicedSoundMarkDef
  , katakanaHiraganaSemiVoicedSoundMark, katakanaHiraganaSemiVoicedSoundMarkDef
  -- * Block: Kaithi
  , kaithiSignNukta, kaithiSignNuktaDef
  , diacriticsExtraDef
  ) where

import Klay.Keyboard.Layout.Action.DeadKey

allDiacritics :: [(DeadKey, Char)]
allDiacritics =
  [ (graveAbove, '\x0300')
  , (graveBelow, '\x0316')
  , (doubleGrave, '\x030F')
  , (doubledGrave, '\x030F')
  , (acuteAbove, '\x0301')
  , (acuteBelow, '\x0317')
  , (doubleAcute, '\x030B')
  , (doubledAcute, '\x030B')
  , (circumflexAbove, '\x0302')
  , (circumflexBelow, '\x032D')
  , (doubledCircumflexAbove, '\x1AB0')
  , (hatchekAbove, '\x030C')
  , (caronAbove, '\x030C')
  , (hatchekBelow, '\x032C')
  , (caronBelow, '\x032C')
  , (tildeAbove, '\x0303')
  , (tildeBelow, '\x0330')
  , (doubleTildeAbove, '\x0360')
  , (breveAbove, '\x0306')
  , (breveBelow, '\x032E')
  , (doubleBreveAbove, '\x035D')
  , (doubleBreveBelow, '\x035C')
  , (invertedBreveAbove, '\x0311')
  , (invertedBreveBelow, '\x032F')
  , (doubleInvertedBreveAbove, '\x0361')
  , (doubleInvertedBreveBelow, '\x1DFC')
  , (horn, '\x031B')
  , (hookAbove, '\x0309')
  , (palatalizedHookBelow, '\x0321')
  , (retroflexHookBelow, '\x0322')
  , (macronAbove, '\x0304')
  , (macronBelow, '\x0331')
  , (doubleMacronAbove, '\x035E')
  , (doubleMacronBelow, '\x035F')
  , (overline, '\x0305')
  , (lowLine, '\x0332')
  , (doubleOverline, '\x033F')
  , (doubleLowLine, '\x0333')
  , (dotAbove, '\x0307')
  , (dotBelow, '\x0323')
  , (diaeresisAbove, '\x0308')
  , (diaeresisBelow, '\x0324')
  , (threeDotsAbove, '\x20DB')
  , (threeDotsBelow, '\x20E8')
  , (fourDotsAbove, '\x20DC')
  , (shortStroke, '\x0335')
  , (longStroke, '\x0336')
  , (shortVerticalLine, '\x20D3')
  , (longVerticalLine, '\x20D2')
  , (doubledLongVerticalLine, '\x20E6')
  , (verticalLineAbove, '\x030D')
  , (doubledVerticalLineAbove, '\x030E')
  , (verticalLineBelow, '\x0329')
  , (doubledVerticalLineBelow, '\x0348')
  , (shortSolidus, '\x0337')
  , (longSolidus, '\x0338')
  , (reverseLongSolidus, '\x20E5')
  , (doubledLongSolidus, '\x20EB')
  , (tildeOverlay, '\x0334')
  , (ringAbove, '\x030A')
  , (ringBelow, '\x0325')
  , (doubledRingBelow, '\x035A')
  , (cedillaBelow, '\x0327')
  , (cedillaAbove, '\x0312')
  , (commaAbove, '\x0313')
  , (commaAboveRight, '\x0315')
  , (reversedCommaAbove, '\x0314')
  , (commaBelow, '\x0326')
  , (turnedCommaAbove, '\x0312')
  , (ogonek, '\x0328')
  , (plusSignBelow, '\x031F')
  , (minusSignBelow, '\x0320')
  , (ringOverlay, '\x20D8')
  , (clockwiseRingOverlay, '\x20D9')
  , (anticlockwiseRingOverlay, '\x20DA')
  , (enclosingCircle, '\x20DD')
  , (enclosingSquare, '\x20DE')
  , (rightwardsArrowAbove, '\x20D7')
  , (rightwardsArrowBelow, '\x20EF')
  , (doubleRightwardsArrowBelow, '\x0362')
  , (wigglyLineBelow, '\x1AB6')
  , (graveAccent, '\x0300')
  , (acuteAccent, '\x0301')
  , (circumflexAccent, '\x0302')
  , (tilde, '\x0303')
  , (macron, '\x0304')
  , (breve, '\x0306')
  , (diaeresis, '\x0308')
  , (doubleAcuteAccent, '\x030B')
  , (caron, '\x030C')
  , (doubleVerticalLineAbove, '\x030E')
  , (doubleGraveAccent, '\x030F')
  , (invertedBreve, '\x0311')
  , (graveAccentBelow, '\x0316')
  , (acuteAccentBelow, '\x0317')
  , (cedilla, '\x0327')
  , (circumflexAccentBelow, '\x032D')
  , (shortStrokeOverlay, '\x0335')
  , (longStrokeOverlay, '\x0336')
  , (shortSolidusOverlay, '\x0337')
  , (longSolidusOverlay, '\x0338')
  , (graveToneMark, '\x0340')
  , (acuteToneMark, '\x0341')
  , (greekPerispomeni, '\x0342')
  , (greekKoronis, '\x0343')
  , (greekDialytikaTonos, '\x0344')
  , (greekYpogegrammeni, '\x0345')
  , (doubleVerticalLineBelow, '\x0348')
  , (doubleRingBelow, '\x035A')
  , (doubleBreve, '\x035D')
  , (doubleMacron, '\x035E')
  , (doubleTilde, '\x0360')
  , (doubleInvertedBreve, '\x0361')
  , (arabicMaddahAbove, '\x0653')
  , (arabicHamzaAbove, '\x0654')
  , (arabicHamzaBelow, '\x0655')
  , (devanagariSignNukta, '\x093C')
  , (teluguAiLengthMark, '\x0C56')
  , (sinhalaSignAlLakuna, '\x0DCA')
  , (doubledCircumflexAccent, '\x1AB0')
  , (longVerticalLineOverlay, '\x20D2')
  , (shortVerticalLineOverlay, '\x20D3')
  , (rightArrowAbove, '\x20D7')
  , (reverseSolidusOverlay, '\x20E5')
  , (doubleVerticalStrokeOverlay, '\x20E6')
  , (tripleUnderdot, '\x20E8')
  , (longDoubleSolidusOverlay, '\x20EB')
  , (rightArrowBelow, '\x20EF')
  , (katakanaHiraganaVoicedSoundMark, '\x3099')
  , (katakanaHiraganaSemiVoicedSoundMark, '\x309A')
  , (kaithiSignNukta, '\x110BA') ]


diacriticsDef :: RawDeadKeyDefinitions
diacriticsDef =
  [ graveAboveDef, graveBelowDef, doubleGraveDef
  , acuteAboveDef, acuteBelowDef, doubleAcuteDef
  , circumflexAboveDef, circumflexBelowDef, doubledCircumflexAboveDef
  , hatchekAboveDef, hatchekBelowDef
  , tildeAboveDef, tildeBelowDef, doubleTildeAboveDef
  , breveAboveDef, breveBelowDef, doubleBreveAboveDef, doubleBreveBelowDef
  , invertedBreveAboveDef, invertedBreveBelowDef, doubleInvertedBreveAboveDef, doubleInvertedBreveBelowDef
  , hornDef, hookAboveDef, palatalizedHookBelowDef, retroflexHookBelowDef
  , macronAboveDef, macronBelowDef, doubleMacronAboveDef, doubleMacronBelowDef
  , overlineDef, lowLineDef, doubleOverlineDef, doubleLowLineDef
  , dotAboveDef, dotBelowDef
  , diaeresisAboveDef, diaeresisBelowDef
  , threeDotsAboveDef, threeDotsBelowDef
  , fourDotsAboveDef
  , shortStrokeDef, longStrokeDef, doubledLongStrokeDef
  , shortVerticalLineDef, longVerticalLineDef, doubledLongVerticalLineDef
  , verticalLineAboveDef, doubledVerticalLineAboveDef, verticalLineBelowDef, doubledVerticalLineBelowDef
  , shortSolidusDef, longSolidusDef, reverseLongSolidusDef, doubledLongSolidusDef
  , tildeOverlayDef
  , ringAboveDef, ringBelowDef, doubledRingBelowDef
  , cedillaBelowDef
  , commaAboveDef, commaAboveRightDef, reversedCommaAboveDef, commaBelowDef, turnedCommaAboveDef
  , ogonekDef
  , plusSignBelowDef, minusSignBelowDef
  , ringOverlayDef, clockwiseRingOverlayDef, anticlockwiseRingOverlayDef
  , enclosingCircleDef, enclosingSquareDef
  , rightwardsArrowAboveDef, rightwardsArrowBelowDef, doubleRightwardsArrowBelowDef
  , wigglyLineBelowDef
  , graveToneMarkDef, acuteToneMarkDef, greekPerispomeniDef, greekKoronisDef, greekDialytikaTonosDef, greekYpogegrammeniDef
  , arabicMaddahAboveDef, arabicHamzaAboveDef, arabicHamzaBelowDef
  , devanagariSignNuktaDef
  , teluguAiLengthMarkDef
  , sinhalaSignAlLakunaDef
  , katakanaHiraganaVoicedSoundMarkDef, katakanaHiraganaSemiVoicedSoundMarkDef
  , kaithiSignNuktaDef ]


-- Diacritics: Accents----------------------------------------------------------
-- | @U+0300@ COMBINING GRAVE ACCENT: ◌̀
graveAbove :: DeadKey
graveAbove = DeadKey
  { _dkName = "Grave Accent"
  , _dkLabel = "◌̀"
  , _dkBaseChar = '\x0060'
  }

graveAboveDef :: RawDeadKeyDefinition
graveAboveDef = (graveAbove,
    [ char '\x002E' '\x0300' {- Combining character -}
    , char '\x00A0' '\x0060' {- Spacing diacritic -}
    , char '\x0020' '\x02CB' {- Modifier letter -}
    , char 'A' 'À'
    , char 'E' 'È'
    , char 'I' 'Ì'
    , char 'O' 'Ò'
    , char 'U' 'Ù'
    , char 'a' 'à'
    , char 'e' 'è'
    , char 'i' 'ì'
    , char 'o' 'ò'
    , char 'u' 'ù'
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'U'] (RChar 'Ǜ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'u'] (RChar 'ǜ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'ι'] (RChar 'ῒ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'υ'] (RChar 'ῢ')
    , char 'Ü' 'Ǜ'
    , char 'ü' 'ǜ'
    , char 'N' 'Ǹ'
    , char 'n' 'ǹ'
    , char 'Е' 'Ѐ'
    , char 'И' 'Ѝ'
    , char 'е' 'ѐ'
    , char 'и' 'ѝ'
    , DeadKeyCombo [SDeadKey macron, SChar 'E'] (RChar 'Ḕ')
    , DeadKeyCombo [SDeadKey macron, SChar 'e'] (RChar 'ḕ')
    , DeadKeyCombo [SDeadKey macron, SChar 'O'] (RChar 'Ṑ')
    , DeadKeyCombo [SDeadKey macron, SChar 'o'] (RChar 'ṑ')
    , char 'Ē' 'Ḕ'
    , char 'ē' 'ḕ'
    , char 'Ō' 'Ṑ'
    , char 'ō' 'ṑ'
    , char 'W' 'Ẁ'
    , char 'w' 'ẁ'
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'A'] (RChar 'Ầ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'a'] (RChar 'ầ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'E'] (RChar 'Ề')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'e'] (RChar 'ề')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'O'] (RChar 'Ồ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'o'] (RChar 'ồ')
    , char 'Â' 'Ầ'
    , char 'â' 'ầ'
    , DeadKeyCombo [SDeadKey breve, SChar 'A'] (RChar 'Ằ')
    , DeadKeyCombo [SDeadKey breve, SChar 'a'] (RChar 'ằ')
    , char 'Ă' 'Ằ'
    , char 'ă' 'ằ'
    , char 'Ê' 'Ề'
    , char 'ê' 'ề'
    , char 'Ô' 'Ồ'
    , char 'ô' 'ồ'
    , DeadKeyCombo [SDeadKey horn, SChar 'O'] (RChar 'Ờ')
    , DeadKeyCombo [SDeadKey horn, SChar 'o'] (RChar 'ờ')
    , DeadKeyCombo [SDeadKey horn, SChar 'U'] (RChar 'Ừ')
    , DeadKeyCombo [SDeadKey horn, SChar 'u'] (RChar 'ừ')
    , char 'Ơ' 'Ờ'
    , char 'ơ' 'ờ'
    , char 'Ư' 'Ừ'
    , char 'ư' 'ừ'
    , char 'Y' 'Ỳ'
    , char 'y' 'ỳ'
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'α'] (RChar 'ἂ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Α'] (RChar 'Ἂ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ε'] (RChar 'ἒ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ε'] (RChar 'Ἒ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'η'] (RChar 'ἢ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Η'] (RChar 'Ἢ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ι'] (RChar 'ἲ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ι'] (RChar 'Ἲ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ο'] (RChar 'ὂ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ο'] (RChar 'Ὂ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'υ'] (RChar 'ὒ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ω'] (RChar 'ὢ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ω'] (RChar 'Ὢ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾂ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾊ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾒ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾚ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾢ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾪ')
    , char 'ἀ' 'ἂ'
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'α'] (RChar 'ἃ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Α'] (RChar 'Ἃ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ε'] (RChar 'ἓ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ε'] (RChar 'Ἓ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'η'] (RChar 'ἣ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Η'] (RChar 'Ἣ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ι'] (RChar 'ἳ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ι'] (RChar 'Ἳ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ο'] (RChar 'ὃ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ο'] (RChar 'Ὃ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'υ'] (RChar 'ὓ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Υ'] (RChar 'Ὓ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ω'] (RChar 'ὣ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ω'] (RChar 'Ὣ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾃ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾋ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾓ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾛ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾣ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾫ')
    , char 'ἁ' 'ἃ'
    , char 'Ἀ' 'Ἂ'
    , char 'Ἁ' 'Ἃ'
    , char 'ἐ' 'ἒ'
    , char 'ἑ' 'ἓ'
    , char 'Ἐ' 'Ἒ'
    , char 'Ἑ' 'Ἓ'
    , char 'ἠ' 'ἢ'
    , char 'ἡ' 'ἣ'
    , char 'Ἠ' 'Ἢ'
    , char 'Ἡ' 'Ἣ'
    , char 'ἰ' 'ἲ'
    , char 'ἱ' 'ἳ'
    , char 'Ἰ' 'Ἲ'
    , char 'Ἱ' 'Ἳ'
    , char 'ὀ' 'ὂ'
    , char 'ὁ' 'ὃ'
    , char 'Ὀ' 'Ὂ'
    , char 'Ὁ' 'Ὃ'
    , char 'ὐ' 'ὒ'
    , char 'ὑ' 'ὓ'
    , char 'Ὑ' 'Ὓ'
    , char 'ὠ' 'ὢ'
    , char 'ὡ' 'ὣ'
    , char 'Ὠ' 'Ὢ'
    , char 'Ὡ' 'Ὣ'
    , char 'α' 'ὰ'
    , char 'ε' 'ὲ'
    , char 'η' 'ὴ'
    , char 'ι' 'ὶ'
    , char 'ο' 'ὸ'
    , char 'υ' 'ὺ'
    , char 'ω' 'ὼ'
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἀ'] (RChar 'ᾂ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἁ'] (RChar 'ᾃ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἀ'] (RChar 'ᾊ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἁ'] (RChar 'ᾋ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἠ'] (RChar 'ᾒ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἡ'] (RChar 'ᾓ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἠ'] (RChar 'ᾚ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἡ'] (RChar 'ᾛ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὠ'] (RChar 'ᾢ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὡ'] (RChar 'ᾣ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὠ'] (RChar 'ᾪ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὡ'] (RChar 'ᾫ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾲ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ῂ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ῲ')
    , char 'ᾳ' 'ᾲ'
    , char 'Α' 'Ὰ'
    , char 'ῃ' 'ῂ'
    , char 'Ε' 'Ὲ'
    , char 'Η' 'Ὴ'
    , char '᾿' '῍'
    , char 'ϊ' 'ῒ'
    , char 'Ι' 'Ὶ'
    , char '῾' '῝'
    , char 'ϋ' 'ῢ'
    , char 'Υ' 'Ὺ'
    , char '¨' '῭'
    , char 'ῳ' 'ῲ'
    , char 'Ο' 'Ὸ'
    , char 'Ω' 'Ὼ'
    , char 'v' 'ǜ'
    , char 'V' 'Ǜ' ] )

-- | @U+0316@ COMBINING GRAVE ACCENT BELOW: ◌̖
graveBelow :: DeadKey
graveBelow = DeadKey
  { _dkName = "Grave Accent Below"
  , _dkLabel = "◌̖"
  , _dkBaseChar = '\x02ce'
  }

graveBelowDef :: RawDeadKeyDefinition
graveBelowDef = (graveBelow,
    [ char '\x002E' '\x0316' {- Combining character -}
    , char '\x0020' '\x02CE' {- Modifier letter -} ] )

-- | @U+030F@ COMBINING DOUBLE GRAVE ACCENT: ◌̏
doubleGrave :: DeadKey
doubleGrave = DeadKey
  { _dkName = "Double Grave Accent"
  , _dkLabel = "◌̏"
  , _dkBaseChar = '\x030f'
  }

doubleGraveDef :: RawDeadKeyDefinition
doubleGraveDef = (doubleGrave,
    [ char '\x002E' '\x030F' {- Combining character -}
    , char 'A' 'Ȁ'
    , char 'a' 'ȁ'
    , char 'E' 'Ȅ'
    , char 'e' 'ȅ'
    , char 'I' 'Ȉ'
    , char 'i' 'ȉ'
    , char 'O' 'Ȍ'
    , char 'o' 'ȍ'
    , char 'R' 'Ȑ'
    , char 'r' 'ȑ'
    , char 'U' 'Ȕ'
    , char 'u' 'ȕ'
    , char 'Ѵ' 'Ѷ'
    , char 'ѵ' 'ѷ' ] )

-- | @U+030F@ COMBINING DOUBLE GRAVE ACCENT: ◌̏. Alias of 'doubleGrave'
doubledGrave :: DeadKey
doubledGrave = doubleGrave

-- | @U+0301@ COMBINING ACUTE ACCENT: ◌́
acuteAbove :: DeadKey
acuteAbove = DeadKey
  { _dkName = "Acute Accent"
  , _dkLabel = "◌́"
  , _dkBaseChar = '\x00b4'
  }

acuteAboveDef :: RawDeadKeyDefinition
acuteAboveDef = (acuteAbove,
    [ char '\x002E' '\x0301' {- Combining character -}
    , char '\x00A0' '\x00B4' {- Spacing diacritic -}
    , char '\x0020' '\x02CA' {- Modifier letter -}
    , char 'A' 'Á'
    , char 'E' 'É'
    , char 'I' 'Í'
    , char 'O' 'Ó'
    , char 'U' 'Ú'
    , char 'Y' 'Ý'
    , char 'a' 'á'
    , char 'e' 'é'
    , char 'i' 'í'
    , char 'o' 'ó'
    , char 'u' 'ú'
    , char 'y' 'ý'
    , char 'C' 'Ć'
    , char 'c' 'ć'
    , char 'L' 'Ĺ'
    , char 'l' 'ĺ'
    , char 'N' 'Ń'
    , char 'n' 'ń'
    , char 'R' 'Ŕ'
    , char 'r' 'ŕ'
    , char 'S' 'Ś'
    , char 's' 'ś'
    , char 'Z' 'Ź'
    , char 'z' 'ź'
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'U'] (RChar 'Ǘ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'u'] (RChar 'ǘ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'ι'] (RChar 'ΐ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'υ'] (RChar 'ΰ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'I'] (RChar 'Ḯ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'i'] (RChar 'ḯ')
    , char 'Ü' 'Ǘ'
    , char 'ü' 'ǘ'
    , char 'G' 'Ǵ'
    , char 'g' 'ǵ'
    , DeadKeyCombo [SDeadKey ringAbove, SChar 'A'] (RChar 'Ǻ')
    , DeadKeyCombo [SDeadKey ringAbove, SChar 'a'] (RChar 'ǻ')
    , char 'Å' 'Ǻ'
    , char 'å' 'ǻ'
    , char 'Æ' 'Ǽ'
    , char 'æ' 'ǽ'
    , char 'Ø' 'Ǿ'
    , char 'ø' 'ǿ'
    , char '¨' '΅'
    , char 'Α' 'Ά'
    , char 'Ε' 'Έ'
    , char 'Η' 'Ή'
    , char 'Ι' 'Ί'
    , char 'Ο' 'Ό'
    , char 'Υ' 'Ύ'
    , char 'Ω' 'Ώ'
    , char 'ϊ' 'ΐ'
    , char 'α' 'ά'
    , char 'ε' 'έ'
    , char 'η' 'ή'
    , char 'ι' 'ί'
    , char 'ϋ' 'ΰ'
    , char 'ο' 'ό'
    , char 'υ' 'ύ'
    , char 'ω' 'ώ'
    , char 'ϒ' 'ϓ'
    , char 'Г' 'Ѓ'
    , char 'К' 'Ќ'
    , char 'г' 'ѓ'
    , char 'к' 'ќ'
    , DeadKeyCombo [SDeadKey cedilla, SChar 'C'] (RChar 'Ḉ')
    , DeadKeyCombo [SDeadKey cedilla, SChar 'c'] (RChar 'ḉ')
    , char 'Ç' 'Ḉ'
    , char 'ç' 'ḉ'
    , DeadKeyCombo [SDeadKey macron, SChar 'E'] (RChar 'Ḗ')
    , DeadKeyCombo [SDeadKey macron, SChar 'e'] (RChar 'ḗ')
    , DeadKeyCombo [SDeadKey macron, SChar 'O'] (RChar 'Ṓ')
    , DeadKeyCombo [SDeadKey macron, SChar 'o'] (RChar 'ṓ')
    , char 'Ē' 'Ḗ'
    , char 'ē' 'ḗ'
    , char 'Ï' 'Ḯ'
    , char 'ï' 'ḯ'
    , char 'K' 'Ḱ'
    , char 'k' 'ḱ'
    , char 'M' 'Ḿ'
    , char 'm' 'ḿ'
    , DeadKeyCombo [SDeadKey tilde, SChar 'O'] (RChar 'Ṍ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'o'] (RChar 'ṍ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'U'] (RChar 'Ṹ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'u'] (RChar 'ṹ')
    , char 'Õ' 'Ṍ'
    , char 'õ' 'ṍ'
    , char 'Ō' 'Ṓ'
    , char 'ō' 'ṓ'
    , char 'P' 'Ṕ'
    , char 'p' 'ṕ'
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'S'] (RChar 'Ṥ')
    , DeadKeyCombo [SDeadKey dotAbove, SChar 's'] (RChar 'ṥ')
    , char 'Ṡ' 'Ṥ'
    , char 'ṡ' 'ṥ'
    , char 'Ũ' 'Ṹ'
    , char 'ũ' 'ṹ'
    , char 'W' 'Ẃ'
    , char 'w' 'ẃ'
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'A'] (RChar 'Ấ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'a'] (RChar 'ấ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'E'] (RChar 'Ế')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'e'] (RChar 'ế')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'O'] (RChar 'Ố')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'o'] (RChar 'ố')
    , char 'Â' 'Ấ'
    , char 'â' 'ấ'
    , DeadKeyCombo [SDeadKey breve, SChar 'A'] (RChar 'Ắ')
    , DeadKeyCombo [SDeadKey breve, SChar 'a'] (RChar 'ắ')
    , char 'Ă' 'Ắ'
    , char 'ă' 'ắ'
    , char 'Ê' 'Ế'
    , char 'ê' 'ế'
    , char 'Ô' 'Ố'
    , char 'ô' 'ố'
    , DeadKeyCombo [SDeadKey horn, SChar 'O'] (RChar 'Ớ')
    , DeadKeyCombo [SDeadKey horn, SChar 'o'] (RChar 'ớ')
    , DeadKeyCombo [SDeadKey horn, SChar 'U'] (RChar 'Ứ')
    , DeadKeyCombo [SDeadKey horn, SChar 'u'] (RChar 'ứ')
    , char 'Ơ' 'Ớ'
    , char 'ơ' 'ớ'
    , char 'Ư' 'Ứ'
    , char 'ư' 'ứ'
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'α'] (RChar 'ἄ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Α'] (RChar 'Ἄ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ε'] (RChar 'ἔ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ε'] (RChar 'Ἔ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'η'] (RChar 'ἤ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Η'] (RChar 'Ἤ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ι'] (RChar 'ἴ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ι'] (RChar 'Ἴ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ο'] (RChar 'ὄ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ο'] (RChar 'Ὄ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'υ'] (RChar 'ὔ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ω'] (RChar 'ὤ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ω'] (RChar 'Ὤ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾄ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾌ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾔ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾜ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾤ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾬ')
    , char 'ἀ' 'ἄ'
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'α'] (RChar 'ἅ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Α'] (RChar 'Ἅ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ε'] (RChar 'ἕ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ε'] (RChar 'Ἕ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'η'] (RChar 'ἥ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Η'] (RChar 'Ἥ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ι'] (RChar 'ἵ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ι'] (RChar 'Ἵ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ο'] (RChar 'ὅ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ο'] (RChar 'Ὅ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'υ'] (RChar 'ὕ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Υ'] (RChar 'Ὕ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ω'] (RChar 'ὥ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ω'] (RChar 'Ὥ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾅ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾍ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾕ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾝ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾥ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾭ')
    , char 'ἁ' 'ἅ'
    , char 'Ἀ' 'Ἄ'
    , char 'Ἁ' 'Ἅ'
    , char 'ἐ' 'ἔ'
    , char 'ἑ' 'ἕ'
    , char 'Ἐ' 'Ἔ'
    , char 'Ἑ' 'Ἕ'
    , char 'ἠ' 'ἤ'
    , char 'ἡ' 'ἥ'
    , char 'Ἠ' 'Ἤ'
    , char 'Ἡ' 'Ἥ'
    , char 'ἰ' 'ἴ'
    , char 'ἱ' 'ἵ'
    , char 'Ἰ' 'Ἴ'
    , char 'Ἱ' 'Ἵ'
    , char 'ὀ' 'ὄ'
    , char 'ὁ' 'ὅ'
    , char 'Ὀ' 'Ὄ'
    , char 'Ὁ' 'Ὅ'
    , char 'ὐ' 'ὔ'
    , char 'ὑ' 'ὕ'
    , char 'Ὑ' 'Ὕ'
    , char 'ὠ' 'ὤ'
    , char 'ὡ' 'ὥ'
    , char 'Ὠ' 'Ὤ'
    , char 'Ὡ' 'Ὥ'
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἀ'] (RChar 'ᾄ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἁ'] (RChar 'ᾅ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἀ'] (RChar 'ᾌ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἁ'] (RChar 'ᾍ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἠ'] (RChar 'ᾔ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἡ'] (RChar 'ᾕ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἠ'] (RChar 'ᾜ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἡ'] (RChar 'ᾝ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὠ'] (RChar 'ᾤ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὡ'] (RChar 'ᾥ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὠ'] (RChar 'ᾬ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὡ'] (RChar 'ᾭ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾴ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ῄ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ῴ')
    , char 'ᾳ' 'ᾴ'
    , char 'ῃ' 'ῄ'
    , char '᾿' '῎'
    , char '῾' '῞'
    , char 'ῳ' 'ῴ'
    , mkCombo [SDeadKey longSolidus, SChar 'o'] 'ǿ'
    , mkCombo [SDeadKey longSolidus, SChar 'O'] 'Ǿ'
    , char 'v' 'ǘ'
    , char 'V' 'Ǘ' ] )

-- | @U+0317@ COMBINING ACUTE ACCENT BELOW: ◌̗
acuteBelow :: DeadKey
acuteBelow = DeadKey
  { _dkName = "Acute Accent Below"
  , _dkLabel = "◌̗"
  , _dkBaseChar = '\x02cf'
  }

acuteBelowDef :: RawDeadKeyDefinition
acuteBelowDef = (acuteBelow,
    [ char '\x002E' '\x0317' {- Combining character -}
    , char '\x0020' '\x02CF' {- Modifier letter -} ] )

-- | @U+030B@ COMBINING DOUBLE ACUTE ACCENT: ◌̋
doubleAcute :: DeadKey
doubleAcute = DeadKey
  { _dkName = "Double Acute Accent"
  , _dkLabel = "◌̋"
  , _dkBaseChar = '\x02dd'
  }

doubleAcuteDef :: RawDeadKeyDefinition
doubleAcuteDef = (doubleAcute,
    [ char '\x002E' '\x030B' {- Combining character -}
    , char '\x0020' '\x02DD' {- Modifier letter -}
    , char 'O' 'Ő'
    , char 'o' 'ő'
    , char 'U' 'Ű'
    , char 'u' 'ű'
    , char 'У' 'Ӳ'
    , char 'у' 'ӳ' ] )

-- | @U+030B@ COMBINING DOUBLE ACUTE ACCENT: ◌̋. Alias of 'doubleAcute'
doubledAcute :: DeadKey
doubledAcute = doubleAcute

-- | @U+0302@ COMBINING CIRCUMFLEX ACCENT: ◌̂
circumflexAbove :: DeadKey
circumflexAbove = DeadKey
  { _dkName = "Circumflex Accent"
  , _dkLabel = "◌̂"
  , _dkBaseChar = '\x02c6'
  }

circumflexAboveDef :: RawDeadKeyDefinition
circumflexAboveDef = (circumflexAbove,
    [ char '\x002E' '\x0302' {- Combining character -}
    , char '\x00A0' '\x005E' {- Spacing diacritic -}
    , char '\x0020' '\x02C6' {- Modifier letter -}
    , char 'A' 'Â'
    , char 'E' 'Ê'
    , char 'I' 'Î'
    , char 'O' 'Ô'
    , char 'U' 'Û'
    , char 'a' 'â'
    , char 'e' 'ê'
    , char 'i' 'î'
    , char 'o' 'ô'
    , char 'u' 'û'
    , char 'C' 'Ĉ'
    , char 'c' 'ĉ'
    , char 'G' 'Ĝ'
    , char 'g' 'ĝ'
    , char 'H' 'Ĥ'
    , char 'h' 'ĥ'
    , char 'J' 'Ĵ'
    , char 'j' 'ĵ'
    , char 'S' 'Ŝ'
    , char 's' 'ŝ'
    , char 'W' 'Ŵ'
    , char 'w' 'ŵ'
    , char 'Y' 'Ŷ'
    , char 'y' 'ŷ'
    , char 'Z' 'Ẑ'
    , char 'z' 'ẑ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'A'] (RChar 'Ấ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'a'] (RChar 'ấ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'E'] (RChar 'Ế')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'e'] (RChar 'ế')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'O'] (RChar 'Ố')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'o'] (RChar 'ố')
    , char 'Á' 'Ấ'
    , char 'á' 'ấ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'A'] (RChar 'Ầ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'a'] (RChar 'ầ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'E'] (RChar 'Ề')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'e'] (RChar 'ề')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'O'] (RChar 'Ồ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'o'] (RChar 'ồ')
    , char 'À' 'Ầ'
    , char 'à' 'ầ'
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'A'] (RChar 'Ẩ')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'a'] (RChar 'ẩ')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'E'] (RChar 'Ể')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'e'] (RChar 'ể')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'O'] (RChar 'Ổ')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'o'] (RChar 'ổ')
    , char 'Ả' 'Ẩ'
    , char 'ả' 'ẩ'
    , DeadKeyCombo [SDeadKey tilde, SChar 'A'] (RChar 'Ẫ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'a'] (RChar 'ẫ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'E'] (RChar 'Ễ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'e'] (RChar 'ễ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'O'] (RChar 'Ỗ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'o'] (RChar 'ỗ')
    , char 'Ã' 'Ẫ'
    , char 'ã' 'ẫ'
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'A'] (RChar 'Ậ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'a'] (RChar 'ậ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'E'] (RChar 'Ệ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'e'] (RChar 'ệ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'O'] (RChar 'Ộ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'o'] (RChar 'ộ')
    , char 'Ạ' 'Ậ'
    , char 'ạ' 'ậ'
    , char 'É' 'Ế'
    , char 'é' 'ế'
    , char 'È' 'Ề'
    , char 'è' 'ề'
    , char 'Ẻ' 'Ể'
    , char 'ẻ' 'ể'
    , char 'Ẽ' 'Ễ'
    , char 'ẽ' 'ễ'
    , char 'Ẹ' 'Ệ'
    , char 'ẹ' 'ệ'
    , char 'Ó' 'Ố'
    , char 'ó' 'ố'
    , char 'Ò' 'Ồ'
    , char 'ò' 'ồ'
    , char 'Ỏ' 'Ổ'
    , char 'ỏ' 'ổ'
    , char 'Õ' 'Ỗ'
    , char 'õ' 'ỗ'
    , char 'Ọ' 'Ộ'
    , char 'ọ' 'ộ'
    , char 'n' 'ñ'
    , char 'N' 'Ñ'
    , char '0' '⁰'
    , char '1' '¹'
    , char '2' '²'
    , char '3' '³'
    , char '4' '⁴'
    , char '5' '⁵'
    , char '6' '⁶'
    , char '7' '⁷'
    , char '8' '⁸'
    , char '9' '⁹'
    , char '+' '⁺'
    , char '(' '⁽'
    , char ')' '⁾' ] )

-- | @U+032D@ COMBINING CIRCUMFLEX ACCENT BELOW: ◌̭
circumflexBelow :: DeadKey
circumflexBelow = DeadKey
  { _dkName = "Circumflex Accent Below"
  , _dkLabel = "◌̭"
  , _dkBaseChar = '\x2038'
  }

circumflexBelowDef :: RawDeadKeyDefinition
circumflexBelowDef = (circumflexBelow,
    [ char '\x002E' '\x032D' {- Combining character -}
    , char '\x00A0' '\x2038' {- Spacing diacritic -}
    , char '\x0020' '\xA788' {- Modifier letter -}
    , char 'D' 'Ḓ'
    , char 'd' 'ḓ'
    , char 'E' 'Ḙ'
    , char 'e' 'ḙ'
    , char 'L' 'Ḽ'
    , char 'l' 'ḽ'
    , char 'N' 'Ṋ'
    , char 'n' 'ṋ'
    , char 'T' 'Ṱ'
    , char 't' 'ṱ'
    , char 'U' 'Ṷ'
    , char 'u' 'ṷ' ] )

-- | @U+1AB0@ COMBINING DOUBLED CIRCUMFLEX ACCENT: ◌᪰
doubledCircumflexAbove :: DeadKey
doubledCircumflexAbove = DeadKey
  { _dkName = "Doubled Circumflex Accent"
  , _dkLabel = "◌᪰"
  , _dkBaseChar = '\x1ab0'
  }

doubledCircumflexAboveDef :: RawDeadKeyDefinition
doubledCircumflexAboveDef = (doubledCircumflexAbove,
    [ char '\x002E' '\x1AB0' {- Combining character -} ] )

-- | @U+030C@ COMBINING CARON: ◌̌
hatchekAbove :: DeadKey
hatchekAbove = DeadKey
  { _dkName = "Caron"
  , _dkLabel = "◌̌"
  , _dkBaseChar = '\x02c7'
  }

hatchekAboveDef :: RawDeadKeyDefinition
hatchekAboveDef = (hatchekAbove,
    [ char '\x002E' '\x030C' {- Combining character -}
    , char '\x0020' '\x02C7' {- Modifier letter -}
    , char 'C' 'Č'
    , char 'c' 'č'
    , char 'D' 'Ď'
    , char 'd' 'ď'
    , char 'E' 'Ě'
    , char 'e' 'ě'
    , char 'L' 'Ľ'
    , char 'l' 'ľ'
    , char 'N' 'Ň'
    , char 'n' 'ň'
    , char 'R' 'Ř'
    , char 'r' 'ř'
    , char 'S' 'Š'
    , char 's' 'š'
    , char 'T' 'Ť'
    , char 't' 'ť'
    , char 'Z' 'Ž'
    , char 'z' 'ž'
    , char 'A' 'Ǎ'
    , char 'a' 'ǎ'
    , char 'I' 'Ǐ'
    , char 'i' 'ǐ'
    , char 'O' 'Ǒ'
    , char 'o' 'ǒ'
    , char 'U' 'Ǔ'
    , char 'u' 'ǔ'
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'U'] (RChar 'Ǚ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'u'] (RChar 'ǚ')
    , char 'Ü' 'Ǚ'
    , char 'ü' 'ǚ'
    , char 'G' 'Ǧ'
    , char 'g' 'ǧ'
    , char 'K' 'Ǩ'
    , char 'k' 'ǩ'
    , char 'Ʒ' 'Ǯ'
    , char 'ʒ' 'ǯ'
    , char 'j' 'ǰ'
    , char 'H' 'Ȟ'
    , char 'h' 'ȟ'
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'S'] (RChar 'Ṧ')
    , DeadKeyCombo [SDeadKey dotAbove, SChar 's'] (RChar 'ṧ')
    , char 'Ṡ' 'Ṧ'
    , char 'ṡ' 'ṧ'
    , char '0' '₀'
    , char '1' '₁'
    , char '2' '₂'
    , char '3' '₃'
    , char '4' '₄'
    , char '5' '₅'
    , char '6' '₆'
    , char '7' '₇'
    , char '8' '₈'
    , char '9' '₉'
    , char '+' '₊'
    , char '(' '₍'
    , char ')' '₎' ] )

-- | @U+030C@ COMBINING CARON: ◌̌. Alias of 'hatchekAbove'
caronAbove :: DeadKey
caronAbove = hatchekAbove

-- | @U+032C@ COMBINING CARON BELOW: ◌̬
hatchekBelow :: DeadKey
hatchekBelow = DeadKey
  { _dkName = "Caron Below"
  , _dkLabel = "◌̬"
  , _dkBaseChar = '\x02ec'
  }

hatchekBelowDef :: RawDeadKeyDefinition
hatchekBelowDef = (hatchekBelow,
    [ char '\x002E' '\x032C' {- Combining character -}
    , char '\x0020' '\x02EC' {- Modifier letter -} ] )

-- | @U+032C@ COMBINING CARON BELOW: ◌̬. Alias of 'hatchekBelow'
caronBelow :: DeadKey
caronBelow = hatchekBelow

-- Diacritics: Curves-----------------------------------------------------------
-- | @U+0303@ COMBINING TILDE: ◌̃
tildeAbove :: DeadKey
tildeAbove = DeadKey
  { _dkName = "Tilde"
  , _dkLabel = "◌̃"
  , _dkBaseChar = '\x007e'
  }

tildeAboveDef :: RawDeadKeyDefinition
tildeAboveDef = (tildeAbove,
    [ char '\x002E' '\x0303' {- Combining character -}
    , char '\x00A0' '\x007E' {- Spacing diacritic -}
    , char '\x0020' '\x02DC' {- Modifier letter -}
    , char 'A' 'Ã'
    , char 'N' 'Ñ'
    , char 'O' 'Õ'
    , char 'a' 'ã'
    , char 'n' 'ñ'
    , char 'o' 'õ'
    , char 'I' 'Ĩ'
    , char 'i' 'ĩ'
    , char 'U' 'Ũ'
    , char 'u' 'ũ'
    , DeadKeyCombo [SDeadKey macron, SChar 'O'] (RChar 'Ȭ')
    , DeadKeyCombo [SDeadKey macron, SChar 'o'] (RChar 'ȭ')
    , char 'Ō' 'Ȭ'
    , char 'ō' 'ȭ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'O'] (RChar 'Ṍ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'o'] (RChar 'ṍ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'U'] (RChar 'Ṹ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'u'] (RChar 'ṹ')
    , char 'Ó' 'Ṍ'
    , char 'ó' 'ṍ'
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'O'] (RChar 'Ṏ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'o'] (RChar 'ṏ')
    , char 'Ö' 'Ṏ'
    , char 'ö' 'ṏ'
    , char 'Ú' 'Ṹ'
    , char 'ú' 'ṹ'
    , char 'V' 'Ṽ'
    , char 'v' 'ṽ'
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'A'] (RChar 'Ẫ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'a'] (RChar 'ẫ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'E'] (RChar 'Ễ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'e'] (RChar 'ễ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'O'] (RChar 'Ỗ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'o'] (RChar 'ỗ')
    , char 'Â' 'Ẫ'
    , char 'â' 'ẫ'
    , DeadKeyCombo [SDeadKey breve, SChar 'A'] (RChar 'Ẵ')
    , DeadKeyCombo [SDeadKey breve, SChar 'a'] (RChar 'ẵ')
    , char 'Ă' 'Ẵ'
    , char 'ă' 'ẵ'
    , char 'E' 'Ẽ'
    , char 'e' 'ẽ'
    , char 'Ê' 'Ễ'
    , char 'ê' 'ễ'
    , char 'Ô' 'Ỗ'
    , char 'ô' 'ỗ'
    , DeadKeyCombo [SDeadKey horn, SChar 'O'] (RChar 'Ỡ')
    , DeadKeyCombo [SDeadKey horn, SChar 'o'] (RChar 'ỡ')
    , DeadKeyCombo [SDeadKey horn, SChar 'U'] (RChar 'Ữ')
    , DeadKeyCombo [SDeadKey horn, SChar 'u'] (RChar 'ữ')
    , char 'Ơ' 'Ỡ'
    , char 'ơ' 'ỡ'
    , char 'Ư' 'Ữ'
    , char 'ư' 'ữ'
    , char 'Y' 'Ỹ'
    , char 'y' 'ỹ'
    , char '-' '≃' ] )

-- | @U+0330@ COMBINING TILDE BELOW: ◌̰
tildeBelow :: DeadKey
tildeBelow = DeadKey
  { _dkName = "Tilde Below"
  , _dkLabel = "◌̰"
  , _dkBaseChar = '\x02f7'
  }

tildeBelowDef :: RawDeadKeyDefinition
tildeBelowDef = (tildeBelow,
    [ char '\x002E' '\x0330' {- Combining character -}
    , char '\x0020' '\x02F7' {- Modifier letter -}
    , char 'E' 'Ḛ'
    , char 'e' 'ḛ'
    , char 'I' 'Ḭ'
    , char 'i' 'ḭ'
    , char 'U' 'Ṵ'
    , char 'u' 'ṵ' ] )

-- | @U+0360@ COMBINING DOUBLE TILDE: ◌͠◌
doubleTildeAbove :: DeadKey
doubleTildeAbove = DeadKey
  { _dkName = "Double Tilde"
  , _dkLabel = "◌͠◌"
  , _dkBaseChar = '\x0360'
  }

doubleTildeAboveDef :: RawDeadKeyDefinition
doubleTildeAboveDef = (doubleTildeAbove,
    [ char '\x002E' '\x0360' {- Combining character -} ] )

-- | @U+0306@ COMBINING BREVE: ◌̆
breveAbove :: DeadKey
breveAbove = DeadKey
  { _dkName = "Breve"
  , _dkLabel = "◌̆"
  , _dkBaseChar = '\x02d8'
  }

breveAboveDef :: RawDeadKeyDefinition
breveAboveDef = (breveAbove,
    [ char '\x002E' '\x0306' {- Combining character -}
    , char '\x0020' '\x02D8' {- Modifier letter -}
    , char 'A' 'Ă'
    , char 'a' 'ă'
    , char 'E' 'Ĕ'
    , char 'e' 'ĕ'
    , char 'G' 'Ğ'
    , char 'g' 'ğ'
    , char 'I' 'Ĭ'
    , char 'i' 'ĭ'
    , char 'O' 'Ŏ'
    , char 'o' 'ŏ'
    , char 'U' 'Ŭ'
    , char 'u' 'ŭ'
    , char 'У' 'Ў'
    , char 'И' 'Й'
    , char 'и' 'й'
    , char 'у' 'ў'
    , char 'Ж' 'Ӂ'
    , char 'ж' 'ӂ'
    , char 'А' 'Ӑ'
    , char 'а' 'ӑ'
    , char 'Е' 'Ӗ'
    , char 'е' 'ӗ'
    , DeadKeyCombo [SDeadKey cedilla, SChar 'E'] (RChar 'Ḝ')
    , DeadKeyCombo [SDeadKey cedilla, SChar 'e'] (RChar 'ḝ')
    , char 'Ȩ' 'Ḝ'
    , char 'ȩ' 'ḝ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'A'] (RChar 'Ắ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'a'] (RChar 'ắ')
    , char 'Á' 'Ắ'
    , char 'á' 'ắ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'A'] (RChar 'Ằ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'a'] (RChar 'ằ')
    , char 'À' 'Ằ'
    , char 'à' 'ằ'
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'A'] (RChar 'Ẳ')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'a'] (RChar 'ẳ')
    , char 'Ả' 'Ẳ'
    , char 'ả' 'ẳ'
    , DeadKeyCombo [SDeadKey tilde, SChar 'A'] (RChar 'Ẵ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'a'] (RChar 'ẵ')
    , char 'Ã' 'Ẵ'
    , char 'ã' 'ẵ'
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'A'] (RChar 'Ặ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'a'] (RChar 'ặ')
    , char 'Ạ' 'Ặ'
    , char 'ạ' 'ặ'
    , char 'α' 'ᾰ'
    , char 'Α' 'Ᾰ'
    , char 'ι' 'ῐ'
    , char 'Ι' 'Ῐ'
    , char 'υ' 'ῠ'
    , char 'Υ' 'Ῠ' ] )

-- | @U+032E@ COMBINING BREVE BELOW: ◌̮
breveBelow :: DeadKey
breveBelow = DeadKey
  { _dkName = "Breve Below"
  , _dkLabel = "◌̮"
  , _dkBaseChar = '\x032e'
  }

breveBelowDef :: RawDeadKeyDefinition
breveBelowDef = (breveBelow,
    [ char '\x002E' '\x032E' {- Combining character -}
    , char 'H' 'Ḫ'
    , char 'h' 'ḫ' ] )

-- | @U+035D@ COMBINING DOUBLE BREVE: ◌͝◌
doubleBreveAbove :: DeadKey
doubleBreveAbove = DeadKey
  { _dkName = "Double Breve"
  , _dkLabel = "◌͝◌"
  , _dkBaseChar = '\x035d'
  }

doubleBreveAboveDef :: RawDeadKeyDefinition
doubleBreveAboveDef = (doubleBreveAbove,
    [ char '\x002E' '\x035D' {- Combining character -} ] )

-- | @U+035C@ COMBINING DOUBLE BREVE BELOW: ◌͜◌
doubleBreveBelow :: DeadKey
doubleBreveBelow = DeadKey
  { _dkName = "Double Breve Below"
  , _dkLabel = "◌͜◌"
  , _dkBaseChar = '\x035c'
  }

doubleBreveBelowDef :: RawDeadKeyDefinition
doubleBreveBelowDef = (doubleBreveBelow,
    [ char '\x002E' '\x035C' {- Combining character -} ] )

-- | @U+0311@ COMBINING INVERTED BREVE: ◌̑
invertedBreveAbove :: DeadKey
invertedBreveAbove = DeadKey
  { _dkName = "Inverted Breve"
  , _dkLabel = "◌̑"
  , _dkBaseChar = '\x0311'
  }

invertedBreveAboveDef :: RawDeadKeyDefinition
invertedBreveAboveDef = (invertedBreveAbove,
    [ char '\x002E' '\x0311' {- Combining character -}
    , char 'A' 'Ȃ'
    , char 'a' 'ȃ'
    , char 'E' 'Ȇ'
    , char 'e' 'ȇ'
    , char 'I' 'Ȋ'
    , char 'i' 'ȋ'
    , char 'O' 'Ȏ'
    , char 'o' 'ȏ'
    , char 'R' 'Ȓ'
    , char 'r' 'ȓ'
    , char 'U' 'Ȗ'
    , char 'u' 'ȗ' ] )

-- | @U+032F@ COMBINING INVERTED BREVE BELOW: ◌̯
invertedBreveBelow :: DeadKey
invertedBreveBelow = DeadKey
  { _dkName = "Inverted Breve Below"
  , _dkLabel = "◌̯"
  , _dkBaseChar = '\x032f'
  }

invertedBreveBelowDef :: RawDeadKeyDefinition
invertedBreveBelowDef = (invertedBreveBelow,
    [ char '\x002E' '\x032F' {- Combining character -} ] )

-- | @U+0361@ COMBINING DOUBLE INVERTED BREVE: ◌͡◌
doubleInvertedBreveAbove :: DeadKey
doubleInvertedBreveAbove = DeadKey
  { _dkName = "Double Inverted Breve"
  , _dkLabel = "◌͡◌"
  , _dkBaseChar = '\x0361'
  }

doubleInvertedBreveAboveDef :: RawDeadKeyDefinition
doubleInvertedBreveAboveDef = (doubleInvertedBreveAbove,
    [ char '\x002E' '\x0361' {- Combining character -} ] )

-- | @U+1DFC@ COMBINING DOUBLE INVERTED BREVE BELOW: ◌᷼◌
doubleInvertedBreveBelow :: DeadKey
doubleInvertedBreveBelow = DeadKey
  { _dkName = "Double Inverted Breve Below"
  , _dkLabel = "◌᷼◌"
  , _dkBaseChar = '\x1dfc'
  }

doubleInvertedBreveBelowDef :: RawDeadKeyDefinition
doubleInvertedBreveBelowDef = (doubleInvertedBreveBelow,
    [ char '\x002E' '\x1DFC' {- Combining character -} ] )

-- | @U+031B@ COMBINING HORN: ◌̛
horn :: DeadKey
horn = DeadKey
  { _dkName = "Horn"
  , _dkLabel = "◌̛"
  , _dkBaseChar = '\x031b'
  }

hornDef :: RawDeadKeyDefinition
hornDef = (horn,
    [ char '\x002E' '\x031B' {- Combining character -}
    , char 'O' 'Ơ'
    , char 'o' 'ơ'
    , char 'U' 'Ư'
    , char 'u' 'ư'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'O'] (RChar 'Ớ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'o'] (RChar 'ớ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'U'] (RChar 'Ứ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'u'] (RChar 'ứ')
    , char 'Ó' 'Ớ'
    , char 'ó' 'ớ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'O'] (RChar 'Ờ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'o'] (RChar 'ờ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'U'] (RChar 'Ừ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'u'] (RChar 'ừ')
    , char 'Ò' 'Ờ'
    , char 'ò' 'ờ'
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'O'] (RChar 'Ở')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'o'] (RChar 'ở')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'U'] (RChar 'Ử')
    , DeadKeyCombo [SDeadKey hookAbove, SChar 'u'] (RChar 'ử')
    , char 'Ỏ' 'Ở'
    , char 'ỏ' 'ở'
    , DeadKeyCombo [SDeadKey tilde, SChar 'O'] (RChar 'Ỡ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'o'] (RChar 'ỡ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'U'] (RChar 'Ữ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'u'] (RChar 'ữ')
    , char 'Õ' 'Ỡ'
    , char 'õ' 'ỡ'
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'O'] (RChar 'Ợ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'o'] (RChar 'ợ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'U'] (RChar 'Ự')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'u'] (RChar 'ự')
    , char 'Ọ' 'Ợ'
    , char 'ọ' 'ợ'
    , char 'Ú' 'Ứ'
    , char 'ú' 'ứ'
    , char 'Ù' 'Ừ'
    , char 'ù' 'ừ'
    , char 'Ủ' 'Ử'
    , char 'ủ' 'ử'
    , char 'Ũ' 'Ữ'
    , char 'ũ' 'ữ'
    , char 'Ụ' 'Ự'
    , char 'ụ' 'ự' ] )

-- | @U+0309@ COMBINING HOOK ABOVE: ◌̉
hookAbove :: DeadKey
hookAbove = DeadKey
  { _dkName = "Hook Above"
  , _dkLabel = "◌̉"
  , _dkBaseChar = '\x0294'
  }

hookAboveDef :: RawDeadKeyDefinition
hookAboveDef = (hookAbove,
    [ char '\x002E' '\x0309' {- Combining character -}
    , char '\x00A0' '\x0294' {- Spacing diacritic -}
    , char '\x0020' '\x02C0' {- Modifier letter -}
    , char 'A' 'Ả'
    , char 'a' 'ả'
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'A'] (RChar 'Ẩ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'a'] (RChar 'ẩ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'E'] (RChar 'Ể')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'e'] (RChar 'ể')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'O'] (RChar 'Ổ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'o'] (RChar 'ổ')
    , char 'Â' 'Ẩ'
    , char 'â' 'ẩ'
    , DeadKeyCombo [SDeadKey breve, SChar 'A'] (RChar 'Ẳ')
    , DeadKeyCombo [SDeadKey breve, SChar 'a'] (RChar 'ẳ')
    , char 'Ă' 'Ẳ'
    , char 'ă' 'ẳ'
    , char 'E' 'Ẻ'
    , char 'e' 'ẻ'
    , char 'Ê' 'Ể'
    , char 'ê' 'ể'
    , char 'I' 'Ỉ'
    , char 'i' 'ỉ'
    , char 'O' 'Ỏ'
    , char 'o' 'ỏ'
    , char 'Ô' 'Ổ'
    , char 'ô' 'ổ'
    , DeadKeyCombo [SDeadKey horn, SChar 'O'] (RChar 'Ở')
    , DeadKeyCombo [SDeadKey horn, SChar 'o'] (RChar 'ở')
    , DeadKeyCombo [SDeadKey horn, SChar 'U'] (RChar 'Ử')
    , DeadKeyCombo [SDeadKey horn, SChar 'u'] (RChar 'ử')
    , char 'Ơ' 'Ở'
    , char 'ơ' 'ở'
    , char 'U' 'Ủ'
    , char 'u' 'ủ'
    , char 'Ư' 'Ử'
    , char 'ư' 'ử'
    , char 'Y' 'Ỷ'
    , char 'y' 'ỷ' ] )

-- | @U+0321@ COMBINING PALATALIZED HOOK BELOW: ◌̡
palatalizedHookBelow :: DeadKey
palatalizedHookBelow = DeadKey
  { _dkName = "Palatalized Hook Below"
  , _dkLabel = "◌̡"
  , _dkBaseChar = '\x0321'
  }

palatalizedHookBelowDef :: RawDeadKeyDefinition
palatalizedHookBelowDef = (palatalizedHookBelow,
    [ char '\x002E' '\x0321' {- Combining character -}
    , char 'f' 'ƒ'
    , char 'F' 'Ƒ'
    , char 'H' 'Ꜧ'
    , char 'h' 'ꜧ'
    , char 'm' 'ɱ'
    , char 'M' 'Ɱ'
    , char 'n' 'ɲ'
    , char 'N' 'Ɲ'
    , char 'z' 'ȥ' ] )

-- | @U+0322@ COMBINING RETROFLEX HOOK BELOW: ◌̢
retroflexHookBelow :: DeadKey
retroflexHookBelow = DeadKey
  { _dkName = "Retroflex Hook Below"
  , _dkLabel = "◌̢"
  , _dkBaseChar = '\x0322'
  }

retroflexHookBelowDef :: RawDeadKeyDefinition
retroflexHookBelowDef = (retroflexHookBelow,
    [ char '\x002E' '\x0322' {- Combining character -}
    , char 'd' 'ɖ'
    , char 'D' 'Ɖ'
    , char 'q' 'ɋ'
    , char 'Q' 'Ɋ'
    , char 'r' 'ɽ'
    , char 'R' 'Ɽ'
    , char 's' 'ʂ'
    , char 'S' 'Ʂ'
    , char 't' 'ʈ'
    , char 'T' 'Ʈ' ] )

-- Diacritics: Macrons----------------------------------------------------------
-- | @U+0304@ COMBINING MACRON: ◌̄
macronAbove :: DeadKey
macronAbove = DeadKey
  { _dkName = "Macron"
  , _dkLabel = "◌̄"
  , _dkBaseChar = '\x00af'
  }

macronAboveDef :: RawDeadKeyDefinition
macronAboveDef = (macronAbove,
    [ char '\x002E' '\x0304' {- Combining character -}
    , char '\x00A0' '\x00AF' {- Spacing diacritic -}
    , char '\x0020' '\x02C9' {- Modifier letter -}
    , char 'A' 'Ā'
    , char 'a' 'ā'
    , char 'E' 'Ē'
    , char 'e' 'ē'
    , char 'I' 'Ī'
    , char 'i' 'ī'
    , char 'O' 'Ō'
    , char 'o' 'ō'
    , char 'U' 'Ū'
    , char 'u' 'ū'
    , char 'Ü' 'Ǖ'
    , char 'ü' 'ǖ'
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'A'] (RChar 'Ǟ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'a'] (RChar 'ǟ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'O'] (RChar 'Ȫ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'o'] (RChar 'ȫ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'U'] (RChar 'Ṻ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'u'] (RChar 'ṻ')
    , char 'Ä' 'Ǟ'
    , char 'ä' 'ǟ'
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'A'] (RChar 'Ǡ')
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'a'] (RChar 'ǡ')
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'O'] (RChar 'Ȱ')
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'o'] (RChar 'ȱ')
    , char 'Ȧ' 'Ǡ'
    , char 'ȧ' 'ǡ'
    , char 'Æ' 'Ǣ'
    , char 'æ' 'ǣ'
    , DeadKeyCombo [SDeadKey ogonek, SChar 'O'] (RChar 'Ǭ')
    , DeadKeyCombo [SDeadKey ogonek, SChar 'o'] (RChar 'ǭ')
    , char 'Ǫ' 'Ǭ'
    , char 'ǫ' 'ǭ'
    , char 'Ö' 'Ȫ'
    , char 'ö' 'ȫ'
    , DeadKeyCombo [SDeadKey tilde, SChar 'O'] (RChar 'Ȭ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'o'] (RChar 'ȭ')
    , char 'Õ' 'Ȭ'
    , char 'õ' 'ȭ'
    , char 'Ȯ' 'Ȱ'
    , char 'ȯ' 'ȱ'
    , char 'Y' 'Ȳ'
    , char 'y' 'ȳ'
    , char 'И' 'Ӣ'
    , char 'и' 'ӣ'
    , char 'У' 'Ӯ'
    , char 'у' 'ӯ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'E'] (RChar 'Ḕ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'e'] (RChar 'ḕ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'O'] (RChar 'Ṑ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'o'] (RChar 'ṑ')
    , char 'È' 'Ḕ'
    , char 'è' 'ḕ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'E'] (RChar 'Ḗ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'e'] (RChar 'ḗ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'O'] (RChar 'Ṓ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'o'] (RChar 'ṓ')
    , char 'É' 'Ḗ'
    , char 'é' 'ḗ'
    , char 'G' 'Ḡ'
    , char 'g' 'ḡ'
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'L'] (RChar 'Ḹ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'l'] (RChar 'ḹ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'R'] (RChar 'Ṝ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'r'] (RChar 'ṝ')
    , char 'Ḷ' 'Ḹ'
    , char 'ḷ' 'ḹ'
    , char 'Ò' 'Ṑ'
    , char 'ò' 'ṑ'
    , char 'Ó' 'Ṓ'
    , char 'ó' 'ṓ'
    , char 'Ṛ' 'Ṝ'
    , char 'ṛ' 'ṝ'
    , char 'α' 'ᾱ'
    , char 'Α' 'Ᾱ'
    , char 'ι' 'ῑ'
    , char 'Ι' 'Ῑ'
    , char 'υ' 'ῡ'
    , char 'Υ' 'Ῡ'
    , char 'v' 'ǖ'
    , char 'V' 'Ǖ' ] )

-- | @U+0331@ COMBINING MACRON BELOW: ◌̱
macronBelow :: DeadKey
macronBelow = DeadKey
  { _dkName = "Macron Below"
  , _dkLabel = "◌̱"
  , _dkBaseChar = '\x02cd'
  }

macronBelowDef :: RawDeadKeyDefinition
macronBelowDef = (macronBelow,
    [ char '\x002E' '\x0331' {- Combining character -}
    , char '\x0020' '\x02CD' {- Modifier letter -}
    , char 'B' 'Ḇ'
    , char 'b' 'ḇ'
    , char 'D' 'Ḏ'
    , char 'd' 'ḏ'
    , char 'K' 'Ḵ'
    , char 'k' 'ḵ'
    , char 'L' 'Ḻ'
    , char 'l' 'ḻ'
    , char 'N' 'Ṉ'
    , char 'n' 'ṉ'
    , char 'R' 'Ṟ'
    , char 'r' 'ṟ'
    , char 'T' 'Ṯ'
    , char 't' 'ṯ'
    , char 'Z' 'Ẕ'
    , char 'z' 'ẕ'
    , char 'h' 'ẖ' ] )

-- | @U+035E@ COMBINING DOUBLE MACRON: ◌͞◌
doubleMacronAbove :: DeadKey
doubleMacronAbove = DeadKey
  { _dkName = "Double Macron"
  , _dkLabel = "◌͞◌"
  , _dkBaseChar = '\x035e'
  }

doubleMacronAboveDef :: RawDeadKeyDefinition
doubleMacronAboveDef = (doubleMacronAbove,
    [ char '\x002E' '\x035E' {- Combining character -} ] )

-- | @U+035F@ COMBINING DOUBLE MACRON BELOW: ◌͟◌
doubleMacronBelow :: DeadKey
doubleMacronBelow = DeadKey
  { _dkName = "Double Macron Below"
  , _dkLabel = "◌͟◌"
  , _dkBaseChar = '\x035f'
  }

doubleMacronBelowDef :: RawDeadKeyDefinition
doubleMacronBelowDef = (doubleMacronBelow,
    [ char '\x002E' '\x035F' {- Combining character -} ] )

-- | @U+0305@ COMBINING OVERLINE: ◌̅
overline :: DeadKey
overline = DeadKey
  { _dkName = "Overline"
  , _dkLabel = "◌̅"
  , _dkBaseChar = '\x203e'
  }

overlineDef :: RawDeadKeyDefinition
overlineDef = (overline,
    [ char '\x002E' '\x0305' {- Combining character -}
    , char '\x0020' '\x203E' {- Modifier letter -} ] )

-- | @U+0332@ COMBINING LOW LINE: ◌̲
lowLine :: DeadKey
lowLine = DeadKey
  { _dkName = "Low Line"
  , _dkLabel = "◌̲"
  , _dkBaseChar = '\x005f'
  }

lowLineDef :: RawDeadKeyDefinition
lowLineDef = (lowLine,
    [ char '\x002E' '\x0332' {- Combining character -}
    , char '\x00A0' '\x005F' {- Spacing diacritic -} ] )

-- | @U+033F@ COMBINING DOUBLE OVERLINE: ◌̿◌
doubleOverline :: DeadKey
doubleOverline = DeadKey
  { _dkName = "Double Overline"
  , _dkLabel = "◌̿◌"
  , _dkBaseChar = '\x033f'
  }

doubleOverlineDef :: RawDeadKeyDefinition
doubleOverlineDef = (doubleOverline,
    [ char '\x002E' '\x033F' {- Combining character -} ] )

-- | @U+0333@ COMBINING DOUBLE LOW LINE: ◌̳◌
doubleLowLine :: DeadKey
doubleLowLine = DeadKey
  { _dkName = "Double Low Line"
  , _dkLabel = "◌̳◌"
  , _dkBaseChar = '\x2017'
  }

doubleLowLineDef :: RawDeadKeyDefinition
doubleLowLineDef = (doubleLowLine,
    [ char '\x002E' '\x0333' {- Combining character -}
    , char '\x00A0' '\x2017' {- Spacing diacritic -} ] )

-- Diacritics: Dots-------------------------------------------------------------
-- | @U+0307@ COMBINING DOT ABOVE: ◌̇
dotAbove :: DeadKey
dotAbove = DeadKey
  { _dkName = "Dot Above"
  , _dkLabel = "◌̇"
  , _dkBaseChar = '\x02d9'
  }

dotAboveDef :: RawDeadKeyDefinition
dotAboveDef = (dotAbove,
    [ char '\x002E' '\x0307' {- Combining character -}
    , char '\x0020' '\x02D9' {- Modifier letter -}
    , char 'C' 'Ċ'
    , char 'c' 'ċ'
    , char 'E' 'Ė'
    , char 'e' 'ė'
    , char 'G' 'Ġ'
    , char 'g' 'ġ'
    , char 'I' 'İ'
    , char 'Z' 'Ż'
    , char 'z' 'ż'
    , DeadKeyCombo [SDeadKey macron, SChar 'A'] (RChar 'Ǡ')
    , DeadKeyCombo [SDeadKey macron, SChar 'a'] (RChar 'ǡ')
    , DeadKeyCombo [SDeadKey macron, SChar 'O'] (RChar 'Ȱ')
    , DeadKeyCombo [SDeadKey macron, SChar 'o'] (RChar 'ȱ')
    , char 'Ā' 'Ǡ'
    , char 'ā' 'ǡ'
    , char 'A' 'Ȧ'
    , char 'a' 'ȧ'
    , char 'O' 'Ȯ'
    , char 'o' 'ȯ'
    , char 'Ō' 'Ȱ'
    , char 'ō' 'ȱ'
    , char 'B' 'Ḃ'
    , char 'b' 'ḃ'
    , char 'D' 'Ḋ'
    , char 'd' 'ḋ'
    , char 'F' 'Ḟ'
    , char 'f' 'ḟ'
    , char 'H' 'Ḣ'
    , char 'h' 'ḣ'
    , char 'M' 'Ṁ'
    , char 'm' 'ṁ'
    , char 'N' 'Ṅ'
    , char 'n' 'ṅ'
    , char 'P' 'Ṗ'
    , char 'p' 'ṗ'
    , char 'R' 'Ṙ'
    , char 'r' 'ṙ'
    , char 'S' 'Ṡ'
    , char 's' 'ṡ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'S'] (RChar 'Ṥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 's'] (RChar 'ṥ')
    , char 'Ś' 'Ṥ'
    , char 'ś' 'ṥ'
    , DeadKeyCombo [SDeadKey caron, SChar 'S'] (RChar 'Ṧ')
    , DeadKeyCombo [SDeadKey caron, SChar 's'] (RChar 'ṧ')
    , char 'Š' 'Ṧ'
    , char 'š' 'ṧ'
    , DeadKeyCombo [SDeadKey dotBelow, SChar 'S'] (RChar 'Ṩ')
    , DeadKeyCombo [SDeadKey dotBelow, SChar 's'] (RChar 'ṩ')
    , char 'Ṣ' 'Ṩ'
    , char 'ṣ' 'ṩ'
    , char 'T' 'Ṫ'
    , char 't' 'ṫ'
    , char 'W' 'Ẇ'
    , char 'w' 'ẇ'
    , char 'X' 'Ẋ'
    , char 'x' 'ẋ'
    , char 'Y' 'Ẏ'
    , char 'y' 'ẏ'
    , char 'ſ' 'ẛ'
    , char 'ı' 'i'
    , char 'i' 'ı'
    , char 'ȷ' 'j'
    , char 'j' 'ȷ'
    , char 'ɉ' 'ɟ'
    , char 'ɟ' 'ɉ'
    , char 'l' 'ŀ'
    , char 'L' 'Ŀ' ] )

-- | @U+0323@ COMBINING DOT BELOW: ◌̣
dotBelow :: DeadKey
dotBelow = DeadKey
  { _dkName = "Dot Below"
  , _dkLabel = "◌̣"
  , _dkBaseChar = '\x002e'
  }

dotBelowDef :: RawDeadKeyDefinition
dotBelowDef = (dotBelow,
    [ char '\x002E' '\x0323' {- Combining character -}
    , char '\x00A0' '\x002E' {- Spacing diacritic -}
    , char '\x0020' '\xFBB3' {- Modifier letter -}
    , char 'B' 'Ḅ'
    , char 'b' 'ḅ'
    , char 'D' 'Ḍ'
    , char 'd' 'ḍ'
    , char 'H' 'Ḥ'
    , char 'h' 'ḥ'
    , char 'K' 'Ḳ'
    , char 'k' 'ḳ'
    , char 'L' 'Ḷ'
    , char 'l' 'ḷ'
    , DeadKeyCombo [SDeadKey macron, SChar 'L'] (RChar 'Ḹ')
    , DeadKeyCombo [SDeadKey macron, SChar 'l'] (RChar 'ḹ')
    , DeadKeyCombo [SDeadKey macron, SChar 'R'] (RChar 'Ṝ')
    , DeadKeyCombo [SDeadKey macron, SChar 'r'] (RChar 'ṝ')
    , char 'M' 'Ṃ'
    , char 'm' 'ṃ'
    , char 'N' 'Ṇ'
    , char 'n' 'ṇ'
    , char 'R' 'Ṛ'
    , char 'r' 'ṛ'
    , char 'S' 'Ṣ'
    , char 's' 'ṣ'
    , DeadKeyCombo [SDeadKey dotAbove, SChar 'S'] (RChar 'Ṩ')
    , DeadKeyCombo [SDeadKey dotAbove, SChar 's'] (RChar 'ṩ')
    , char 'Ṡ' 'Ṩ'
    , char 'ṡ' 'ṩ'
    , char 'T' 'Ṭ'
    , char 't' 'ṭ'
    , char 'V' 'Ṿ'
    , char 'v' 'ṿ'
    , char 'W' 'Ẉ'
    , char 'w' 'ẉ'
    , char 'Z' 'Ẓ'
    , char 'z' 'ẓ'
    , char 'A' 'Ạ'
    , char 'a' 'ạ'
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'A'] (RChar 'Ậ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'a'] (RChar 'ậ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'E'] (RChar 'Ệ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'e'] (RChar 'ệ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'O'] (RChar 'Ộ')
    , DeadKeyCombo [SDeadKey circumflexAccent, SChar 'o'] (RChar 'ộ')
    , char 'Â' 'Ậ'
    , char 'â' 'ậ'
    , DeadKeyCombo [SDeadKey breve, SChar 'A'] (RChar 'Ặ')
    , DeadKeyCombo [SDeadKey breve, SChar 'a'] (RChar 'ặ')
    , char 'Ă' 'Ặ'
    , char 'ă' 'ặ'
    , char 'E' 'Ẹ'
    , char 'e' 'ẹ'
    , char 'Ê' 'Ệ'
    , char 'ê' 'ệ'
    , char 'I' 'Ị'
    , char 'i' 'ị'
    , char 'O' 'Ọ'
    , char 'o' 'ọ'
    , char 'Ô' 'Ộ'
    , char 'ô' 'ộ'
    , DeadKeyCombo [SDeadKey horn, SChar 'O'] (RChar 'Ợ')
    , DeadKeyCombo [SDeadKey horn, SChar 'o'] (RChar 'ợ')
    , DeadKeyCombo [SDeadKey horn, SChar 'U'] (RChar 'Ự')
    , DeadKeyCombo [SDeadKey horn, SChar 'u'] (RChar 'ự')
    , char 'Ơ' 'Ợ'
    , char 'ơ' 'ợ'
    , char 'U' 'Ụ'
    , char 'u' 'ụ'
    , char 'Ư' 'Ự'
    , char 'ư' 'ự'
    , char 'Y' 'Ỵ'
    , char 'y' 'ỵ' ] )

-- | @U+0308@ COMBINING DIAERESIS: ◌̈
diaeresisAbove :: DeadKey
diaeresisAbove = DeadKey
  { _dkName = "Diaeresis"
  , _dkLabel = "◌̈"
  , _dkBaseChar = '\x003a'
  }

diaeresisAboveDef :: RawDeadKeyDefinition
diaeresisAboveDef = (diaeresisAbove,
    [ char '\x002E' '\x0308' {- Combining character -}
    , char '\x00A0' '\x00A8' {- Spacing diacritic -}
    , char 'A' 'Ä'
    , char 'E' 'Ë'
    , char 'I' 'Ï'
    , char 'O' 'Ö'
    , char 'U' 'Ü'
    , char 'a' 'ä'
    , char 'e' 'ë'
    , char 'i' 'ï'
    , char 'o' 'ö'
    , char 'u' 'ü'
    , char 'y' 'ÿ'
    , char 'Y' 'Ÿ'
    , DeadKeyCombo [SDeadKey macron, SChar 'U'] (RChar 'Ǖ')
    , DeadKeyCombo [SDeadKey macron, SChar 'u'] (RChar 'ǖ')
    , DeadKeyCombo [SDeadKey macron, SChar 'A'] (RChar 'Ǟ')
    , DeadKeyCombo [SDeadKey macron, SChar 'a'] (RChar 'ǟ')
    , DeadKeyCombo [SDeadKey macron, SChar 'O'] (RChar 'Ȫ')
    , DeadKeyCombo [SDeadKey macron, SChar 'o'] (RChar 'ȫ')
    , char 'Ū' 'Ṻ'
    , char 'ū' 'ṻ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'U'] (RChar 'Ǘ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'u'] (RChar 'ǘ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ι'] (RChar 'ΐ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'υ'] (RChar 'ΰ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'I'] (RChar 'Ḯ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'i'] (RChar 'ḯ')
    , char 'Ú' 'Ǘ'
    , char 'ú' 'ǘ'
    , DeadKeyCombo [SDeadKey caron, SChar 'U'] (RChar 'Ǚ')
    , DeadKeyCombo [SDeadKey caron, SChar 'u'] (RChar 'ǚ')
    , char 'Ǔ' 'Ǚ'
    , char 'ǔ' 'ǚ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'U'] (RChar 'Ǜ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'u'] (RChar 'ǜ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ι'] (RChar 'ῒ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'υ'] (RChar 'ῢ')
    , char 'Ù' 'Ǜ'
    , char 'ù' 'ǜ'
    , char 'Ā' 'Ǟ'
    , char 'ā' 'ǟ'
    , char 'Ō' 'Ȫ'
    , char 'ō' 'ȫ'
    , char 'ί' 'ΐ'
    , char 'Ι' 'Ϊ'
    , char 'Υ' 'Ϋ'
    , char 'ύ' 'ΰ'
    , char 'ι' 'ϊ'
    , char 'υ' 'ϋ'
    , char 'ϒ' 'ϔ'
    , char 'Е' 'Ё'
    , char 'І' 'Ї'
    , char 'е' 'ё'
    , char 'і' 'ї'
    , char 'А' 'Ӓ'
    , char 'а' 'ӓ'
    , char 'Ә' 'Ӛ'
    , char 'ә' 'ӛ'
    , char 'Ж' 'Ӝ'
    , char 'ж' 'ӝ'
    , char 'З' 'Ӟ'
    , char 'з' 'ӟ'
    , char 'И' 'Ӥ'
    , char 'и' 'ӥ'
    , char 'О' 'Ӧ'
    , char 'о' 'ӧ'
    , char 'Ө' 'Ӫ'
    , char 'ө' 'ӫ'
    , char 'Э' 'Ӭ'
    , char 'э' 'ӭ'
    , char 'У' 'Ӱ'
    , char 'у' 'ӱ'
    , char 'Ч' 'Ӵ'
    , char 'ч' 'ӵ'
    , char 'Ы' 'Ӹ'
    , char 'ы' 'ӹ'
    , char 'H' 'Ḧ'
    , char 'h' 'ḧ'
    , char 'Í' 'Ḯ'
    , char 'í' 'ḯ'
    , DeadKeyCombo [SDeadKey tilde, SChar 'O'] (RChar 'Ṏ')
    , DeadKeyCombo [SDeadKey tilde, SChar 'o'] (RChar 'ṏ')
    , char 'Õ' 'Ṏ'
    , char 'õ' 'ṏ'
    , char 'W' 'Ẅ'
    , char 'w' 'ẅ'
    , char 'X' 'Ẍ'
    , char 'x' 'ẍ'
    , char 't' 'ẗ'
    , char 'ὶ' 'ῒ'
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ι'] (RChar 'ῗ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'υ'] (RChar 'ῧ')
    , char 'ῖ' 'ῗ'
    , char 'ὺ' 'ῢ'
    , char 'ῦ' 'ῧ' ] )

-- | @U+0324@ COMBINING DIAERESIS BELOW: ◌̤
diaeresisBelow :: DeadKey
diaeresisBelow = DeadKey
  { _dkName = "Diaeresis Below"
  , _dkLabel = "◌̤"
  , _dkBaseChar = '\x2025'
  }

diaeresisBelowDef :: RawDeadKeyDefinition
diaeresisBelowDef = (diaeresisBelow,
    [ char '\x002E' '\x0324' {- Combining character -}
    , char '\x00A0' '\x2025' {- Spacing diacritic -}
    , char 'U' 'Ṳ'
    , char 'u' 'ṳ' ] )

-- | @U+20DB@ COMBINING THREE DOTS ABOVE: ◌⃛
threeDotsAbove :: DeadKey
threeDotsAbove = DeadKey
  { _dkName = "Three Dots Above"
  , _dkLabel = "◌⃛"
  , _dkBaseChar = '\x20db'
  }

threeDotsAboveDef :: RawDeadKeyDefinition
threeDotsAboveDef = (threeDotsAbove,
    [ char '\x002E' '\x20DB' {- Combining character -} ] )

-- | @U+20E8@ COMBINING TRIPLE UNDERDOT: ◌⃨
threeDotsBelow :: DeadKey
threeDotsBelow = DeadKey
  { _dkName = "Triple Underdot"
  , _dkLabel = "◌⃨"
  , _dkBaseChar = '\x20e8'
  }

threeDotsBelowDef :: RawDeadKeyDefinition
threeDotsBelowDef = (threeDotsBelow,
    [ char '\x002E' '\x20E8' {- Combining character -} ] )

-- | @U+20DC@ COMBINING FOUR DOTS ABOVE: ◌⃜
fourDotsAbove :: DeadKey
fourDotsAbove = DeadKey
  { _dkName = "Four Dots Above"
  , _dkLabel = "◌⃜"
  , _dkBaseChar = '\x20dc'
  }

fourDotsAboveDef :: RawDeadKeyDefinition
fourDotsAboveDef = (fourDotsAbove,
    [ char '\x002E' '\x20DC' {- Combining character -} ] )

-- Diacritics: Overlays---------------------------------------------------------
-- | @U+0335@ COMBINING SHORT STROKE OVERLAY: ◌̵
shortStroke :: DeadKey
shortStroke = DeadKey
  { _dkName = "Short Stroke Overlay"
  , _dkLabel = "◌̵"
  , _dkBaseChar = '\x2010'
  }

shortStrokeDef :: RawDeadKeyDefinition
shortStrokeDef = (shortStroke,
    [ char '\x002E' '\x0335' {- Combining character -}
    , char '\x00A0' '\x2010' {- Spacing diacritic -}
    , char 'p' 'ꝑ'
    , char 'P' 'Ꝑ' ] )

-- | @U+0336@ COMBINING LONG STROKE OVERLAY: ◌̶
longStroke :: DeadKey
longStroke = DeadKey
  { _dkName = "Long Stroke Overlay"
  , _dkLabel = "◌̶"
  , _dkBaseChar = '\x0336'
  }

longStrokeDef :: RawDeadKeyDefinition
longStrokeDef = (longStroke,
    [ char '\x002E' '\x0336' {- Combining character -}
    , char 'b' 'ƀ'
    , char 'B' 'Ƀ'
    , char 'c' 'ꞓ'
    , char 'C' 'Ꞓ'
    , char 'd' 'đ'
    , char 'D' 'Đ'
    , char 'f' 'ꞙ'
    , char 'F' 'Ꞙ'
    , char 'g' 'ǥ'
    , char 'G' 'Ǥ'
    , char 'h' 'ħ'
    , char 'H' 'Ħ'
    , char 'i' 'ɨ'
    , char 'I' 'Ɨ'
    , char 'j' 'ɉ'
    , mkCombo [SDeadKey dotAbove, SChar 'j'] 'ɟ'
    , char 'J' 'Ɉ'
    , char 'k' 'ꝁ'
    , char 'K' 'Ꝁ'
    , mkCombo [SDeadKey shortSolidus, SChar 'k'] 'ꝅ'
    , mkCombo [SDeadKey shortSolidus, SChar 'K'] 'Ꝅ'
    , char 'l' 'ƚ'
    , char 'L' 'Ƚ'
    , char 'o' 'ꝋ'
    , char 'O' 'Ꝋ'
    , char 'p' 'ᵽ'
    , char 'P' 'Ᵽ'
    , char 'q' 'ꝗ'
    , char 'Q' 'Ꝗ'
    , char 'r' 'ɍ'
    , char 'R' 'Ɍ'
    , char 's' 'ẝ'
    , char 't' 'ŧ'
    , char 'T' 'Ŧ'
    , char 'u' 'ʉ'
    , char 'U' 'Ʉ'
    , char 'y' 'ɏ'
    , char 'Y' 'Ɏ'
    , char 'z' 'ƶ'
    , char 'Z' 'Ƶ' ] )

-- | Doubled Long Stroke Overlay (Custom)
doubledLongStroke :: DeadKey
doubledLongStroke = DeadKey
  { _dkName = "Doubled Long Stroke Overlay"
  , _dkLabel = "[FIXME]"
  , _dkBaseChar = '\x2550'
  }

doubledLongStrokeDef :: RawDeadKeyDefinition
doubledLongStrokeDef = (doubledLongStroke,
    [ char '\x00A0' '\x2550' {- Spacing diacritic -}
    , char 'l' 'ⱡ'
    , char 'L' 'Ⱡ' ] )

-- | @U+20D3@ COMBINING SHORT VERTICAL LINE OVERLAY: ◌⃓
shortVerticalLine :: DeadKey
shortVerticalLine = DeadKey
  { _dkName = "Short Vertical Line Overlay"
  , _dkLabel = "◌⃓"
  , _dkBaseChar = '\x20d3'
  }

shortVerticalLineDef :: RawDeadKeyDefinition
shortVerticalLineDef = (shortVerticalLine,
    [ char '\x002E' '\x20D3' {- Combining character -} ] )

-- | @U+20D2@ COMBINING LONG VERTICAL LINE OVERLAY: ◌⃒
longVerticalLine :: DeadKey
longVerticalLine = DeadKey
  { _dkName = "Long Vertical Line Overlay"
  , _dkLabel = "◌⃒"
  , _dkBaseChar = '\x007c'
  }

longVerticalLineDef :: RawDeadKeyDefinition
longVerticalLineDef = (longVerticalLine,
    [ char '\x002E' '\x20D2' {- Combining character -}
    , char '\x00A0' '\x007C' {- Spacing diacritic -} ] )

-- | @U+20E6@ COMBINING DOUBLE VERTICAL STROKE OVERLAY: ◌⃦
doubledLongVerticalLine :: DeadKey
doubledLongVerticalLine = DeadKey
  { _dkName = "Double Vertical Stroke Overlay"
  , _dkLabel = "◌⃦"
  , _dkBaseChar = '\x2016'
  }

doubledLongVerticalLineDef :: RawDeadKeyDefinition
doubledLongVerticalLineDef = (doubledLongVerticalLine,
    [ char '\x002E' '\x20E6' {- Combining character -}
    , char '\x00A0' '\x2016' {- Spacing diacritic -} ] )

-- | @U+030D@ COMBINING VERTICAL LINE ABOVE: ◌̍
verticalLineAbove :: DeadKey
verticalLineAbove = DeadKey
  { _dkName = "Vertical Line Above"
  , _dkLabel = "◌̍"
  , _dkBaseChar = '\x0027'
  }

verticalLineAboveDef :: RawDeadKeyDefinition
verticalLineAboveDef = (verticalLineAbove,
    [ char '\x002E' '\x030D' {- Combining character -}
    , char '\x00A0' '\x0027' {- Spacing diacritic -}
    , char '\x0020' '\x02C8' {- Modifier letter -} ] )

-- | @U+030E@ COMBINING DOUBLE VERTICAL LINE ABOVE: ◌̎
doubledVerticalLineAbove :: DeadKey
doubledVerticalLineAbove = DeadKey
  { _dkName = "Double Vertical Line Above"
  , _dkLabel = "◌̎"
  , _dkBaseChar = '\x0022'
  }

doubledVerticalLineAboveDef :: RawDeadKeyDefinition
doubledVerticalLineAboveDef = (doubledVerticalLineAbove,
    [ char '\x002E' '\x030E' {- Combining character -}
    , char '\x00A0' '\x0022' {- Spacing diacritic -} ] )

-- | @U+0329@ COMBINING VERTICAL LINE BELOW: ◌̩
verticalLineBelow :: DeadKey
verticalLineBelow = DeadKey
  { _dkName = "Vertical Line Below"
  , _dkLabel = "◌̩"
  , _dkBaseChar = '\x02cc'
  }

verticalLineBelowDef :: RawDeadKeyDefinition
verticalLineBelowDef = (verticalLineBelow,
    [ char '\x002E' '\x0329' {- Combining character -}
    , char '\x0020' '\x02CC' {- Modifier letter -} ] )

-- | @U+0348@ COMBINING DOUBLE VERTICAL LINE BELOW: ◌͈
doubledVerticalLineBelow :: DeadKey
doubledVerticalLineBelow = DeadKey
  { _dkName = "Double Vertical Line Below"
  , _dkLabel = "◌͈"
  , _dkBaseChar = '\x0348'
  }

doubledVerticalLineBelowDef :: RawDeadKeyDefinition
doubledVerticalLineBelowDef = (doubledVerticalLineBelow,
    [ char '\x002E' '\x0348' {- Combining character -} ] )

-- | @U+0337@ COMBINING SHORT SOLIDUS OVERLAY: ◌̷
shortSolidus :: DeadKey
shortSolidus = DeadKey
  { _dkName = "Short Solidus Overlay"
  , _dkLabel = "◌̷"
  , _dkBaseChar = '\x0337'
  }

shortSolidusDef :: RawDeadKeyDefinition
shortSolidusDef = (shortSolidus,
    [ char '\x002E' '\x0337' {- Combining character -}
    , char 'k' 'ꝃ'
    , char 'K' 'Ꝃ'
    , mkCombo [SDeadKey longStroke, SChar 'k'] 'ꝅ'
    , mkCombo [SDeadKey longStroke, SChar 'K'] 'Ꝅ' ] )

-- | @U+0338@ COMBINING LONG SOLIDUS OVERLAY: ◌̸
longSolidus :: DeadKey
longSolidus = DeadKey
  { _dkName = "Long Solidus Overlay"
  , _dkLabel = "◌̸"
  , _dkBaseChar = '\x002f'
  }

longSolidusDef :: RawDeadKeyDefinition
longSolidusDef = (longSolidus,
    [ char '\x002E' '\x0338' {- Combining character -}
    , char '\x00A0' '\x002F' {- Spacing diacritic -}
    , char '←' '↚'
    , char '→' '↛'
    , char '↔' '↮'
    , char '⇐' '⇍'
    , char '⇔' '⇎'
    , char '⇒' '⇏'
    , char '∃' '∄'
    , char '∈' '∉'
    , char '∋' '∌'
    , char '∣' '∤'
    , char '∥' '∦'
    , char '∼' '≁'
    , char '≃' '≄'
    , char '≅' '≇'
    , char '≈' '≉'
    , char '=' '≠'
    , char '≡' '≢'
    , char '≍' '≭'
    , char '<' '≮'
    , char '>' '≯'
    , char '≤' '≰'
    , char '≥' '≱'
    , char '≲' '≴'
    , char '≳' '≵'
    , char '≶' '≸'
    , char '≷' '≹'
    , char '≺' '⊀'
    , char '≻' '⊁'
    , char '⊂' '⊄'
    , char '⊃' '⊅'
    , char '⊆' '⊈'
    , char '⊇' '⊉'
    , char '⊢' '⊬'
    , char '⊨' '⊭'
    , char '⊩' '⊮'
    , char '⊫' '⊯'
    , char '≼' '⋠'
    , char '≽' '⋡'
    , char '⊑' '⋢'
    , char '⊒' '⋣'
    , char '⊲' '⋪'
    , char '⊳' '⋫'
    , char '⊴' '⋬'
    , char '⊵' '⋭'
    , char 'a' 'ⱥ'
    , char 'A' 'Ⱥ'
    , char 'b' '␢'
    , char 'c' 'ȼ'
    , char 'C' 'Ȼ'
    , char 'e' 'ɇ'
    , char 'E' 'Ɇ'
    , char 'f' 'ẜ'
    , char 'g' 'ꞡ'
    , char 'G' 'Ꞡ'
    , char 'k' 'ꞣ'
    , char 'K' 'Ꞣ'
    , char 'l' 'ł'
    , char 'L' 'Ł'
    , char 'n' 'ꞥ'
    , char 'N' 'Ꞥ'
    , char 'o' 'ø'
    , char 'O' 'Ø'
    , char 'ó' 'ǿ'
    , char 'Ó' 'Ǿ'
    , mkCombo [SDeadKey acuteAbove, SChar 'o'] 'ǿ'
    , mkCombo [SDeadKey acuteAbove, SChar 'O'] 'Ǿ'
    , char 'p' 'ᵽ'
    , char 'q' 'ꝙ'
    , char 'Q' 'Ꝙ'
    , char 'r' 'ꞧ'
    , char 'R' 'Ꞧ'
    , char 's' 'ꞩ'
    , char 'S' 'Ꞩ'
    , char 'ſ' 'ẜ'
    , char 't' 'ⱦ'
    , char 'T' 'Ⱦ'
    , char 'u' 'Ꞹ'
    , char 'v' 'ꝟ'
    , char 'V' 'Ꝟ'
    , char 'ʊ' 'ᵿ'
    , char '0' '∅' ] )

-- | @U+20E5@ COMBINING REVERSE SOLIDUS OVERLAY: ◌⃥
reverseLongSolidus :: DeadKey
reverseLongSolidus = DeadKey
  { _dkName = "Reverse Solidus Overlay"
  , _dkLabel = "◌⃥"
  , _dkBaseChar = '\x005c'
  }

reverseLongSolidusDef :: RawDeadKeyDefinition
reverseLongSolidusDef = (reverseLongSolidus,
    [ char '\x002E' '\x20E5' {- Combining character -}
    , char '\x00A0' '\x005C' {- Spacing diacritic -} ] )

-- | @U+20EB@ COMBINING LONG DOUBLE SOLIDUS OVERLAY: ◌⃫
doubledLongSolidus :: DeadKey
doubledLongSolidus = DeadKey
  { _dkName = "Long Double Solidus Overlay"
  , _dkLabel = "◌⃫"
  , _dkBaseChar = '\x2afd'
  }

doubledLongSolidusDef :: RawDeadKeyDefinition
doubledLongSolidusDef = (doubledLongSolidus,
    [ char '\x002E' '\x20EB' {- Combining character -}
    , char '\x00A0' '\x2AFD' {- Spacing diacritic -} ] )

-- | @U+0334@ COMBINING TILDE OVERLAY: ◌̴
tildeOverlay :: DeadKey
tildeOverlay = DeadKey
  { _dkName = "Tilde Overlay"
  , _dkLabel = "◌̴"
  , _dkBaseChar = '\x301c'
  }

tildeOverlayDef :: RawDeadKeyDefinition
tildeOverlayDef = (tildeOverlay,
    [ char '\x002E' '\x0334' {- Combining character -}
    , char '\x00A0' '\x301C' {- Spacing diacritic -}
    , char 'b' 'ᵬ'
    , char 'd' 'ᵭ'
    , char 'e' 'ḛ'
    , char 'E' 'Ḛ'
    , char 'f' 'ᵮ'
    , char 'i' 'ḭ'
    , char 'I' 'Ḭ'
    , char 'l' 'ɫ'
    , char 'L' 'Ɫ'
    , char 'm' 'ᵯ'
    , char 'n' 'ᵰ'
    , char 'o' 'ɵ'
    , char 'O' 'Ɵ'
    , char 'p' 'ᵱ'
    , char 'r' 'ᵲ'
    , char 's' 'ᵴ'
    , char 't' 'ᵵ'
    , char 'u' 'ṵ'
    , char 'U' 'Ṵ'
    , char 'z' 'ᵶ'
    , mkCombo [SDeadKey tildeOverlay, SChar 'l'] 'ꬸ'
    , mkCombo [SDeadKey tildeAbove, SChar 'l'] 'ꬸ' ] )

-- Diacritics: Rings------------------------------------------------------------
-- | @U+030A@ COMBINING RING ABOVE: ◌̊
ringAbove :: DeadKey
ringAbove = DeadKey
  { _dkName = "Ring Above"
  , _dkLabel = "◌̊"
  , _dkBaseChar = '\x02da'
  }

ringAboveDef :: RawDeadKeyDefinition
ringAboveDef = (ringAbove,
    [ char '\x002E' '\x030A' {- Combining character -}
    , char '\x00A0' '\x00B0' {- Spacing diacritic -}
    , char '\x0020' '\x02DA' {- Modifier letter -}
    , char 'A' 'Å'
    , char 'a' 'å'
    , char 'U' 'Ů'
    , char 'u' 'ů'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'A'] (RChar 'Ǻ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'a'] (RChar 'ǻ')
    , char 'Á' 'Ǻ'
    , char 'á' 'ǻ'
    , char 'w' 'ẘ'
    , char 'y' 'ẙ' ] )

-- | @U+0325@ COMBINING RING BELOW: ◌̥
ringBelow :: DeadKey
ringBelow = DeadKey
  { _dkName = "Ring Below"
  , _dkLabel = "◌̥"
  , _dkBaseChar = '\x02f3'
  }

ringBelowDef :: RawDeadKeyDefinition
ringBelowDef = (ringBelow,
    [ char '\x002E' '\x0325' {- Combining character -}
    , char '\x0020' '\x02F3' {- Modifier letter -}
    , char 'A' 'Ḁ'
    , char 'a' 'ḁ'
    , char 'l' 'ꬹ'
    , char 'o' 'ⱺ'
    , char 'x' 'ꭖ'
    , char 'χ' 'ꭔ' ] )

-- | @U+035A@ COMBINING DOUBLE RING BELOW: ◌͚
doubledRingBelow :: DeadKey
doubledRingBelow = DeadKey
  { _dkName = "Double Ring Below"
  , _dkLabel = "◌͚"
  , _dkBaseChar = '\x035a'
  }

doubledRingBelowDef :: RawDeadKeyDefinition
doubledRingBelowDef = (doubledRingBelow,
    [ char '\x002E' '\x035A' {- Combining character -} ] )

-- Diacritics: Curls------------------------------------------------------------
-- | @U+0327@ COMBINING CEDILLA: ◌̧
cedillaBelow :: DeadKey
cedillaBelow = DeadKey
  { _dkName = "Cedilla"
  , _dkLabel = "◌̧"
  , _dkBaseChar = '\x002c'
  }

cedillaBelowDef :: RawDeadKeyDefinition
cedillaBelowDef = (cedillaBelow,
    [ char '\x002E' '\x0327' {- Combining character -}
    , char '\x00A0' '\x00B8' {- Spacing diacritic -}
    , char 'C' 'Ç'
    , char 'c' 'ç'
    , char 'G' 'Ģ'
    , char 'g' 'ģ'
    , char 'K' 'Ķ'
    , char 'k' 'ķ'
    , char 'L' 'Ļ'
    , char 'l' 'ļ'
    , char 'N' 'Ņ'
    , char 'n' 'ņ'
    , char 'R' 'Ŗ'
    , char 'r' 'ŗ'
    , char 'S' 'Ş'
    , char 's' 'ş'
    , char 'T' 'Ţ'
    , char 't' 'ţ'
    , char 'E' 'Ȩ'
    , char 'e' 'ȩ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'C'] (RChar 'Ḉ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'c'] (RChar 'ḉ')
    , char 'Ć' 'Ḉ'
    , char 'ć' 'ḉ'
    , char 'D' 'Ḑ'
    , char 'd' 'ḑ'
    , DeadKeyCombo [SDeadKey breve, SChar 'E'] (RChar 'Ḝ')
    , DeadKeyCombo [SDeadKey breve, SChar 'e'] (RChar 'ḝ')
    , char 'Ĕ' 'Ḝ'
    , char 'ĕ' 'ḝ'
    , char 'H' 'Ḩ'
    , char 'h' 'ḩ' ] )

-- | @U+0312@ COMBINING TURNED COMMA ABOVE: ◌̒. Alias of 'turnedCommaAbove'
cedillaAbove :: DeadKey
cedillaAbove = turnedCommaAbove

-- | @U+0313@ COMBINING COMMA ABOVE: ◌̓
commaAbove :: DeadKey
commaAbove = DeadKey
  { _dkName = "Comma Above"
  , _dkLabel = "◌̓"
  , _dkBaseChar = '\x2019'
  }

commaAboveDef :: RawDeadKeyDefinition
commaAboveDef = (commaAbove,
    [ char '\x002E' '\x0313' {- Combining character -}
    , char '\x00A0' '\x2019' {- Spacing diacritic -}
    , char 'α' 'ἀ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'α'] (RChar 'ἂ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Α'] (RChar 'Ἂ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ε'] (RChar 'ἒ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ε'] (RChar 'Ἒ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'η'] (RChar 'ἢ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Η'] (RChar 'Ἢ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ι'] (RChar 'ἲ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ι'] (RChar 'Ἲ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ο'] (RChar 'ὂ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ο'] (RChar 'Ὂ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'υ'] (RChar 'ὒ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ω'] (RChar 'ὢ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ω'] (RChar 'Ὢ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾂ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾊ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾒ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾚ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾢ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾪ')
    , char 'ὰ' 'ἂ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'α'] (RChar 'ἄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Α'] (RChar 'Ἄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ε'] (RChar 'ἔ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ε'] (RChar 'Ἔ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'η'] (RChar 'ἤ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Η'] (RChar 'Ἤ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ι'] (RChar 'ἴ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ι'] (RChar 'Ἴ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ο'] (RChar 'ὄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ο'] (RChar 'Ὄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'υ'] (RChar 'ὔ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ω'] (RChar 'ὤ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ω'] (RChar 'Ὤ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾌ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾔ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾜ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾤ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾬ')
    , char 'ά' 'ἄ'
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'α'] (RChar 'ἆ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Α'] (RChar 'Ἆ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'η'] (RChar 'ἦ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Η'] (RChar 'Ἦ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ι'] (RChar 'ἶ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ι'] (RChar 'Ἶ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'υ'] (RChar 'ὖ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ω'] (RChar 'ὦ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ω'] (RChar 'Ὦ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾆ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾎ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾖ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾞ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾦ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾮ')
    , char 'ᾶ' 'ἆ'
    , char 'Α' 'Ἀ'
    , char 'Ὰ' 'Ἂ'
    , char 'Ά' 'Ἄ'
    , char 'ε' 'ἐ'
    , char 'ὲ' 'ἒ'
    , char 'έ' 'ἔ'
    , char 'Ε' 'Ἐ'
    , char 'Ὲ' 'Ἒ'
    , char 'Έ' 'Ἔ'
    , char 'η' 'ἠ'
    , char 'ὴ' 'ἢ'
    , char 'ή' 'ἤ'
    , char 'ῆ' 'ἦ'
    , char 'Η' 'Ἠ'
    , char 'Ὴ' 'Ἢ'
    , char 'Ή' 'Ἤ'
    , char 'ι' 'ἰ'
    , char 'ὶ' 'ἲ'
    , char 'ί' 'ἴ'
    , char 'ῖ' 'ἶ'
    , char 'Ι' 'Ἰ'
    , char 'Ὶ' 'Ἲ'
    , char 'Ί' 'Ἴ'
    , char 'ο' 'ὀ'
    , char 'ὸ' 'ὂ'
    , char 'ό' 'ὄ'
    , char 'Ο' 'Ὀ'
    , char 'Ὸ' 'Ὂ'
    , char 'Ό' 'Ὄ'
    , char 'υ' 'ὐ'
    , char 'ὺ' 'ὒ'
    , char 'ύ' 'ὔ'
    , char 'ῦ' 'ὖ'
    , char 'ω' 'ὠ'
    , char 'ὼ' 'ὢ'
    , char 'ώ' 'ὤ'
    , char 'ῶ' 'ὦ'
    , char 'Ω' 'Ὠ'
    , char 'Ὼ' 'Ὢ'
    , char 'Ώ' 'Ὤ'
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾀ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὰ'] (RChar 'ᾂ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ά'] (RChar 'ᾄ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ᾶ'] (RChar 'ᾆ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾈ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὰ'] (RChar 'ᾊ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ά'] (RChar 'ᾌ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾐ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὴ'] (RChar 'ᾒ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ή'] (RChar 'ᾔ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ῆ'] (RChar 'ᾖ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾘ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὴ'] (RChar 'ᾚ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ή'] (RChar 'ᾜ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾠ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὼ'] (RChar 'ᾢ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ώ'] (RChar 'ᾤ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ῶ'] (RChar 'ᾦ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾨ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὼ'] (RChar 'ᾪ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ώ'] (RChar 'ᾬ')
    , char 'ᾳ' 'ᾀ'
    , char 'ᾼ' 'ᾈ'
    , char 'ῃ' 'ᾐ'
    , char 'ῌ' 'ᾘ'
    , char 'ῳ' 'ᾠ'
    , char 'ῼ' 'ᾨ'
    , char 'ρ' 'ῤ' ] )

-- | @U+0315@ COMBINING COMMA ABOVE RIGHT: ◌̕
commaAboveRight :: DeadKey
commaAboveRight = DeadKey
  { _dkName = "Comma Above Right"
  , _dkLabel = "◌̕"
  , _dkBaseChar = '\x02bc'
  }

commaAboveRightDef :: RawDeadKeyDefinition
commaAboveRightDef = (commaAboveRight,
    [ char '\x002E' '\x0315' {- Combining character -}
    , char '\x00A0' '\x2019' {- Spacing diacritic -}
    , char '\x0020' '\x02BC' {- Modifier letter -} ] )

-- | @U+0314@ COMBINING REVERSED COMMA ABOVE: ◌̔
reversedCommaAbove :: DeadKey
reversedCommaAbove = DeadKey
  { _dkName = "Reversed Comma Above"
  , _dkLabel = "◌̔"
  , _dkBaseChar = '\x201b'
  }

reversedCommaAboveDef :: RawDeadKeyDefinition
reversedCommaAboveDef = (reversedCommaAbove,
    [ char '\x002E' '\x0314' {- Combining character -}
    , char '\x00A0' '\x201B' {- Spacing diacritic -}
    , char '\x0020' '\x02BD' {- Modifier letter -}
    , char 'α' 'ἁ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'α'] (RChar 'ἃ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Α'] (RChar 'Ἃ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ε'] (RChar 'ἓ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ε'] (RChar 'Ἓ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'η'] (RChar 'ἣ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Η'] (RChar 'Ἣ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ι'] (RChar 'ἳ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ι'] (RChar 'Ἳ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ο'] (RChar 'ὃ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ο'] (RChar 'Ὃ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'υ'] (RChar 'ὓ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Υ'] (RChar 'Ὓ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ω'] (RChar 'ὣ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ω'] (RChar 'Ὣ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾃ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾋ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾓ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾛ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾣ')
    , DeadKeyCombo [SDeadKey graveAccent, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾫ')
    , char 'ὰ' 'ἃ'
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'α'] (RChar 'ἅ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Α'] (RChar 'Ἅ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ε'] (RChar 'ἕ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ε'] (RChar 'Ἕ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'η'] (RChar 'ἥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Η'] (RChar 'Ἥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ι'] (RChar 'ἵ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ι'] (RChar 'Ἵ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ο'] (RChar 'ὅ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ο'] (RChar 'Ὅ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'υ'] (RChar 'ὕ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Υ'] (RChar 'Ὕ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ω'] (RChar 'ὥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ω'] (RChar 'Ὥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾅ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾍ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾕ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾝ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾭ')
    , char 'ά' 'ἅ'
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'α'] (RChar 'ἇ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Α'] (RChar 'Ἇ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'η'] (RChar 'ἧ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Η'] (RChar 'Ἧ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ι'] (RChar 'ἷ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ι'] (RChar 'Ἷ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'υ'] (RChar 'ὗ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Υ'] (RChar 'Ὗ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ω'] (RChar 'ὧ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ω'] (RChar 'Ὧ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾇ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾏ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾗ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾟ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾧ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾯ')
    , char 'ᾶ' 'ἇ'
    , char 'Α' 'Ἁ'
    , char 'Ὰ' 'Ἃ'
    , char 'Ά' 'Ἅ'
    , char 'ε' 'ἑ'
    , char 'ὲ' 'ἓ'
    , char 'έ' 'ἕ'
    , char 'Ε' 'Ἑ'
    , char 'Ὲ' 'Ἓ'
    , char 'Έ' 'Ἕ'
    , char 'η' 'ἡ'
    , char 'ὴ' 'ἣ'
    , char 'ή' 'ἥ'
    , char 'ῆ' 'ἧ'
    , char 'Η' 'Ἡ'
    , char 'Ὴ' 'Ἣ'
    , char 'Ή' 'Ἥ'
    , char 'ι' 'ἱ'
    , char 'ὶ' 'ἳ'
    , char 'ί' 'ἵ'
    , char 'ῖ' 'ἷ'
    , char 'Ι' 'Ἱ'
    , char 'Ὶ' 'Ἳ'
    , char 'Ί' 'Ἵ'
    , char 'ο' 'ὁ'
    , char 'ὸ' 'ὃ'
    , char 'ό' 'ὅ'
    , char 'Ο' 'Ὁ'
    , char 'Ὸ' 'Ὃ'
    , char 'Ό' 'Ὅ'
    , char 'υ' 'ὑ'
    , char 'ὺ' 'ὓ'
    , char 'ύ' 'ὕ'
    , char 'ῦ' 'ὗ'
    , char 'Υ' 'Ὑ'
    , char 'Ὺ' 'Ὓ'
    , char 'Ύ' 'Ὕ'
    , char 'ω' 'ὡ'
    , char 'ὼ' 'ὣ'
    , char 'ώ' 'ὥ'
    , char 'ῶ' 'ὧ'
    , char 'Ω' 'Ὡ'
    , char 'Ὼ' 'Ὣ'
    , char 'Ώ' 'Ὥ'
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾁ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὰ'] (RChar 'ᾃ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ά'] (RChar 'ᾅ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ᾶ'] (RChar 'ᾇ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾉ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὰ'] (RChar 'ᾋ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ά'] (RChar 'ᾍ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾑ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὴ'] (RChar 'ᾓ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ή'] (RChar 'ᾕ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ῆ'] (RChar 'ᾗ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾙ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὴ'] (RChar 'ᾛ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ή'] (RChar 'ᾝ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾡ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὼ'] (RChar 'ᾣ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ώ'] (RChar 'ᾥ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ῶ'] (RChar 'ᾧ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾩ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὼ'] (RChar 'ᾫ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ώ'] (RChar 'ᾭ')
    , char 'ᾳ' 'ᾁ'
    , char 'ᾼ' 'ᾉ'
    , char 'ῃ' 'ᾑ'
    , char 'ῌ' 'ᾙ'
    , char 'ῳ' 'ᾡ'
    , char 'ῼ' 'ᾩ'
    , char 'ρ' 'ῥ'
    , char 'Ρ' 'Ῥ' ] )

-- | @U+0326@ COMBINING COMMA BELOW: ◌̦
commaBelow :: DeadKey
commaBelow = DeadKey
  { _dkName = "Comma Below"
  , _dkLabel = "◌̦"
  , _dkBaseChar = '\x003b'
  }

commaBelowDef :: RawDeadKeyDefinition
commaBelowDef = (commaBelow,
    [ char '\x002E' '\x0326' {- Combining character -}
    , char '\x00A0' '\x002C' {- Spacing diacritic -}
    , char 'S' 'Ș'
    , char 's' 'ș'
    , char 'T' 'Ț'
    , char 't' 'ț' ] )

-- | @U+0312@ COMBINING TURNED COMMA ABOVE: ◌̒
turnedCommaAbove :: DeadKey
turnedCommaAbove = DeadKey
  { _dkName = "Turned Comma Above"
  , _dkLabel = "◌̒"
  , _dkBaseChar = '\x2018'
  }

turnedCommaAboveDef :: RawDeadKeyDefinition
turnedCommaAboveDef = (turnedCommaAbove,
    [ char '\x002E' '\x0312' {- Combining character -}
    , char '\x00A0' '\x2018' {- Spacing diacritic -}
    , char '\x0020' '\x02BB' {- Modifier letter -} ] )

-- | @U+0328@ COMBINING OGONEK: ◌̨
ogonek :: DeadKey
ogonek = DeadKey
  { _dkName = "Ogonek"
  , _dkLabel = "◌̨"
  , _dkBaseChar = '\x02db'
  }

ogonekDef :: RawDeadKeyDefinition
ogonekDef = (ogonek,
    [ char '\x002E' '\x0328' {- Combining character -}
    , char '\x0020' '\x02DB' {- Modifier letter -}
    , char 'A' 'Ą'
    , char 'a' 'ą'
    , char 'E' 'Ę'
    , char 'e' 'ę'
    , char 'I' 'Į'
    , char 'i' 'į'
    , char 'U' 'Ų'
    , char 'u' 'ų'
    , char 'O' 'Ǫ'
    , char 'o' 'ǫ'
    , DeadKeyCombo [SDeadKey macron, SChar 'O'] (RChar 'Ǭ')
    , DeadKeyCombo [SDeadKey macron, SChar 'o'] (RChar 'ǭ')
    , char 'Ō' 'Ǭ'
    , char 'ō' 'ǭ' ] )

-- Diacritics: Miscellaneous----------------------------------------------------
-- | @U+031F@ COMBINING PLUS SIGN BELOW: ◌̟
plusSignBelow :: DeadKey
plusSignBelow = DeadKey
  { _dkName = "Plus Sign Below"
  , _dkLabel = "◌̟"
  , _dkBaseChar = '\x207a'
  }

plusSignBelowDef :: RawDeadKeyDefinition
plusSignBelowDef = (plusSignBelow,
    [ char '\x002E' '\x031F' {- Combining character -}
    , char '\x0020' '\x02D6' {- Modifier letter -} ] )

-- | @U+0320@ COMBINING MINUS SIGN BELOW: ◌̠
minusSignBelow :: DeadKey
minusSignBelow = DeadKey
  { _dkName = "Minus Sign Below"
  , _dkLabel = "◌̠"
  , _dkBaseChar = '\x208a'
  }

minusSignBelowDef :: RawDeadKeyDefinition
minusSignBelowDef = (minusSignBelow,
    [ char '\x002E' '\x0320' {- Combining character -}
    , char '\x0020' '\x02D7' {- Modifier letter -} ] )

-- Symbols: Overlay-------------------------------------------------------------
-- | @U+20D8@ COMBINING RING OVERLAY: ◌⃘
ringOverlay :: DeadKey
ringOverlay = DeadKey
  { _dkName = "Ring Overlay"
  , _dkLabel = "◌⃘"
  , _dkBaseChar = '\x20d8'
  }

ringOverlayDef :: RawDeadKeyDefinition
ringOverlayDef = (ringOverlay,
    [ char '\x002E' '\x20D8' {- Combining character -}
    , char 'l' 'ꬹ'
    , char 'o' 'ⱺ' ] )

-- | @U+20D9@ COMBINING CLOCKWISE RING OVERLAY: ◌⃙
clockwiseRingOverlay :: DeadKey
clockwiseRingOverlay = DeadKey
  { _dkName = "Clockwise Ring Overlay"
  , _dkLabel = "◌⃙"
  , _dkBaseChar = '\x20d9'
  }

clockwiseRingOverlayDef :: RawDeadKeyDefinition
clockwiseRingOverlayDef = (clockwiseRingOverlay,
    [ char '\x002E' '\x20D9' {- Combining character -} ] )

-- | @U+20DA@ COMBINING ANTICLOCKWISE RING OVERLAY: ◌⃚
anticlockwiseRingOverlay :: DeadKey
anticlockwiseRingOverlay = DeadKey
  { _dkName = "Anticlockwise Ring Overlay"
  , _dkLabel = "◌⃚"
  , _dkBaseChar = '\x20da'
  }

anticlockwiseRingOverlayDef :: RawDeadKeyDefinition
anticlockwiseRingOverlayDef = (anticlockwiseRingOverlay,
    [ char '\x002E' '\x20DA' {- Combining character -} ] )

-- Symbols: Enclosing-----------------------------------------------------------
-- | @U+20DD@ COMBINING ENCLOSING CIRCLE: ◌⃝
enclosingCircle :: DeadKey
enclosingCircle = DeadKey
  { _dkName = "Enclosing Circle"
  , _dkLabel = "◌⃝"
  , _dkBaseChar = '\x20dd'
  }

enclosingCircleDef :: RawDeadKeyDefinition
enclosingCircleDef = (enclosingCircle,
    [ char '\x002E' '\x20DD' {- Combining character -} ] )

-- | @U+20DE@ COMBINING ENCLOSING SQUARE: ◌⃞
enclosingSquare :: DeadKey
enclosingSquare = DeadKey
  { _dkName = "Enclosing Square"
  , _dkLabel = "◌⃞"
  , _dkBaseChar = '\x20de'
  }

enclosingSquareDef :: RawDeadKeyDefinition
enclosingSquareDef = (enclosingSquare,
    [ char '\x002E' '\x20DE' {- Combining character -} ] )

-- Symbols: Arrow---------------------------------------------------------------
-- | @U+20D7@ COMBINING RIGHT ARROW ABOVE: ◌⃗
rightwardsArrowAbove :: DeadKey
rightwardsArrowAbove = DeadKey
  { _dkName = "Right Arrow Above"
  , _dkLabel = "◌⃗"
  , _dkBaseChar = '\x20d7'
  }

rightwardsArrowAboveDef :: RawDeadKeyDefinition
rightwardsArrowAboveDef = (rightwardsArrowAbove,
    [ char '\x002E' '\x20D7' {- Combining character -} ] )

-- | @U+20EF@ COMBINING RIGHT ARROW BELOW: ◌⃯
rightwardsArrowBelow :: DeadKey
rightwardsArrowBelow = DeadKey
  { _dkName = "Right Arrow Below"
  , _dkLabel = "◌⃯"
  , _dkBaseChar = '\x20ef'
  }

rightwardsArrowBelowDef :: RawDeadKeyDefinition
rightwardsArrowBelowDef = (rightwardsArrowBelow,
    [ char '\x002E' '\x20EF' {- Combining character -} ] )

-- | @U+0362@ COMBINING DOUBLE RIGHTWARDS ARROW BELOW: ◌͢◌
doubleRightwardsArrowBelow :: DeadKey
doubleRightwardsArrowBelow = DeadKey
  { _dkName = "Double Rightwards Arrow Below"
  , _dkLabel = "◌͢◌"
  , _dkBaseChar = '\x0362'
  }

doubleRightwardsArrowBelowDef :: RawDeadKeyDefinition
doubleRightwardsArrowBelowDef = (doubleRightwardsArrowBelow,
    [ char '\x002E' '\x0362' {- Combining character -} ] )

-- Symbols: Miscellaneous-------------------------------------------------------
-- | @U+1AB6@ COMBINING WIGGLY LINE BELOW: ◌᪶
wigglyLineBelow :: DeadKey
wigglyLineBelow = DeadKey
  { _dkName = "Wiggly Line Below"
  , _dkLabel = "◌᪶"
  , _dkBaseChar = '\xfe4f'
  }

wigglyLineBelowDef :: RawDeadKeyDefinition
wigglyLineBelowDef = (wigglyLineBelow,
    [ char '\x002E' '\x1AB6' {- Combining character -}
    , char '\x00A0' '\xFE4F' {- Spacing diacritic -} ] )

-- Block: Combining Diacritical Marks-------------------------------------------
-- | @U+0300@ COMBINING GRAVE ACCENT: ◌̀. Alias of 'graveAbove'
graveAccent :: DeadKey
graveAccent = graveAbove

-- | @U+0301@ COMBINING ACUTE ACCENT: ◌́. Alias of 'acuteAbove'
acuteAccent :: DeadKey
acuteAccent = acuteAbove

-- | @U+0302@ COMBINING CIRCUMFLEX ACCENT: ◌̂. Alias of 'circumflexAbove'
circumflexAccent :: DeadKey
circumflexAccent = circumflexAbove

-- | @U+0303@ COMBINING TILDE: ◌̃. Alias of 'tildeAbove'
tilde :: DeadKey
tilde = tildeAbove

-- | @U+0304@ COMBINING MACRON: ◌̄. Alias of 'macronAbove'
macron :: DeadKey
macron = macronAbove

-- | @U+0306@ COMBINING BREVE: ◌̆. Alias of 'breveAbove'
breve :: DeadKey
breve = breveAbove

-- | @U+0308@ COMBINING DIAERESIS: ◌̈. Alias of 'diaeresisAbove'
diaeresis :: DeadKey
diaeresis = diaeresisAbove

-- | @U+030B@ COMBINING DOUBLE ACUTE ACCENT: ◌̋. Alias of 'doubledAcute'
doubleAcuteAccent :: DeadKey
doubleAcuteAccent = doubledAcute

-- | @U+030C@ COMBINING CARON: ◌̌. Alias of 'caronAbove'
caron :: DeadKey
caron = caronAbove

-- | @U+030E@ COMBINING DOUBLE VERTICAL LINE ABOVE: ◌̎. Alias of 'doubledVerticalLineAbove'
doubleVerticalLineAbove :: DeadKey
doubleVerticalLineAbove = doubledVerticalLineAbove

-- | @U+030F@ COMBINING DOUBLE GRAVE ACCENT: ◌̏. Alias of 'doubledGrave'
doubleGraveAccent :: DeadKey
doubleGraveAccent = doubledGrave

-- | @U+0311@ COMBINING INVERTED BREVE: ◌̑. Alias of 'invertedBreveAbove'
invertedBreve :: DeadKey
invertedBreve = invertedBreveAbove

-- | @U+0316@ COMBINING GRAVE ACCENT BELOW: ◌̖. Alias of 'graveBelow'
graveAccentBelow :: DeadKey
graveAccentBelow = graveBelow

-- | @U+0317@ COMBINING ACUTE ACCENT BELOW: ◌̗. Alias of 'acuteBelow'
acuteAccentBelow :: DeadKey
acuteAccentBelow = acuteBelow

-- | @U+0327@ COMBINING CEDILLA: ◌̧. Alias of 'cedillaBelow'
cedilla :: DeadKey
cedilla = cedillaBelow

-- | @U+032D@ COMBINING CIRCUMFLEX ACCENT BELOW: ◌̭. Alias of 'circumflexBelow'
circumflexAccentBelow :: DeadKey
circumflexAccentBelow = circumflexBelow

-- | @U+0335@ COMBINING SHORT STROKE OVERLAY: ◌̵. Alias of 'shortStroke'
shortStrokeOverlay :: DeadKey
shortStrokeOverlay = shortStroke

-- | @U+0336@ COMBINING LONG STROKE OVERLAY: ◌̶. Alias of 'longStroke'
longStrokeOverlay :: DeadKey
longStrokeOverlay = longStroke

-- | @U+0337@ COMBINING SHORT SOLIDUS OVERLAY: ◌̷. Alias of 'shortSolidus'
shortSolidusOverlay :: DeadKey
shortSolidusOverlay = shortSolidus

-- | @U+0338@ COMBINING LONG SOLIDUS OVERLAY: ◌̸. Alias of 'longSolidus'
longSolidusOverlay :: DeadKey
longSolidusOverlay = longSolidus

-- | @U+0340@ COMBINING GRAVE TONE MARK: ◌̀, equivalent to: @U+0300@
graveToneMark :: DeadKey
graveToneMark = DeadKey
  { _dkName = "Grave Tone Mark"
  , _dkLabel = "◌̀"
  , _dkBaseChar = '\x0340'
  }

graveToneMarkDef :: RawDeadKeyDefinition
graveToneMarkDef = (graveToneMark, snd graveAboveDef)

-- | @U+0341@ COMBINING ACUTE TONE MARK: ◌́, equivalent to: @U+0301@
acuteToneMark :: DeadKey
acuteToneMark = DeadKey
  { _dkName = "Acute Tone Mark"
  , _dkLabel = "◌́"
  , _dkBaseChar = '\x1ffd'
  }

acuteToneMarkDef :: RawDeadKeyDefinition
acuteToneMarkDef = (acuteToneMark, snd acuteAboveDef)

-- | @U+0342@ COMBINING GREEK PERISPOMENI: ◌͂
greekPerispomeni :: DeadKey
greekPerispomeni = DeadKey
  { _dkName = "Greek Perispomeni"
  , _dkLabel = "◌͂"
  , _dkBaseChar = '\x1fc0'
  }

greekPerispomeniDef :: RawDeadKeyDefinition
greekPerispomeniDef = (greekPerispomeni,
    [ char '\x002E' '\x0342' {- Combining character -}
    , char '\x00A0' '\x1FC0' {- Spacing diacritic -}
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'α'] (RChar 'ἆ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Α'] (RChar 'Ἆ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'η'] (RChar 'ἦ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Η'] (RChar 'Ἦ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ι'] (RChar 'ἶ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ι'] (RChar 'Ἶ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'υ'] (RChar 'ὖ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ω'] (RChar 'ὦ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ω'] (RChar 'Ὦ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾆ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾎ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾖ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾞ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾦ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾮ')
    , char 'ἀ' 'ἆ'
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'α'] (RChar 'ἇ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Α'] (RChar 'Ἇ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'η'] (RChar 'ἧ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Η'] (RChar 'Ἧ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ι'] (RChar 'ἷ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ι'] (RChar 'Ἷ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'υ'] (RChar 'ὗ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Υ'] (RChar 'Ὗ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ω'] (RChar 'ὧ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ω'] (RChar 'Ὧ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾇ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Α'] (RChar 'ᾏ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ᾗ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Η'] (RChar 'ᾟ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ᾧ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekYpogegrammeni, SChar 'Ω'] (RChar 'ᾯ')
    , char 'ἁ' 'ἇ'
    , char 'Ἀ' 'Ἆ'
    , char 'Ἁ' 'Ἇ'
    , char 'ἠ' 'ἦ'
    , char 'ἡ' 'ἧ'
    , char 'Ἠ' 'Ἦ'
    , char 'Ἡ' 'Ἧ'
    , char 'ἰ' 'ἶ'
    , char 'ἱ' 'ἷ'
    , char 'Ἰ' 'Ἶ'
    , char 'Ἱ' 'Ἷ'
    , char 'ὐ' 'ὖ'
    , char 'ὑ' 'ὗ'
    , char 'Ὑ' 'Ὗ'
    , char 'ὠ' 'ὦ'
    , char 'ὡ' 'ὧ'
    , char 'Ὠ' 'Ὦ'
    , char 'Ὡ' 'Ὧ'
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἀ'] (RChar 'ᾆ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἁ'] (RChar 'ᾇ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἀ'] (RChar 'ᾎ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἁ'] (RChar 'ᾏ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἠ'] (RChar 'ᾖ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ἡ'] (RChar 'ᾗ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἠ'] (RChar 'ᾞ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ἡ'] (RChar 'ᾟ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὠ'] (RChar 'ᾦ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ὡ'] (RChar 'ᾧ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὠ'] (RChar 'ᾮ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'Ὡ'] (RChar 'ᾯ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'α'] (RChar 'ᾷ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'η'] (RChar 'ῇ')
    , DeadKeyCombo [SDeadKey greekYpogegrammeni, SChar 'ω'] (RChar 'ῷ')
    , char 'α' 'ᾶ'
    , char 'ᾳ' 'ᾷ'
    , char '¨' '῁'
    , char 'η' 'ῆ'
    , char 'ῃ' 'ῇ'
    , char '᾿' '῏'
    , char 'ι' 'ῖ'
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'ι'] (RChar 'ῗ')
    , DeadKeyCombo [SDeadKey diaeresis, SChar 'υ'] (RChar 'ῧ')
    , char 'ϊ' 'ῗ'
    , char '῾' '῟'
    , char 'υ' 'ῦ'
    , char 'ϋ' 'ῧ'
    , char 'ω' 'ῶ'
    , char 'ῳ' 'ῷ' ] )

-- | @U+0343@ COMBINING GREEK KORONIS: ◌̓, equivalent to: @U+0313@
greekKoronis :: DeadKey
greekKoronis = DeadKey
  { _dkName = "Greek Koronis"
  , _dkLabel = "◌̓"
  , _dkBaseChar = '\x1fbf'
  }

greekKoronisDef :: RawDeadKeyDefinition
greekKoronisDef = (greekKoronis, snd commaAboveDef)

-- | @U+0344@ COMBINING GREEK DIALYTIKA TONOS: ◌̈́, equivalent to: @U+0308 + U+0301@
greekDialytikaTonos :: DeadKey
greekDialytikaTonos = DeadKey
  { _dkName = "Greek Dialytika Tonos"
  , _dkLabel = "◌̈́"
  , _dkBaseChar = '\x1fee'
  }

greekDialytikaTonosDef :: RawDeadKeyDefinition
greekDialytikaTonosDef = (greekDialytikaTonos,
    [ char '\x002E' '\x0344' {- Combining character -}
    , char '\x00A0' '\x1FEE' {- Spacing diacritic -}
    , char 'U' 'Ǘ'
    , char 'u' 'ǘ'
    , char 'ι' 'ΐ'
    , char 'υ' 'ΰ'
    , char 'I' 'Ḯ'
    , char 'i' 'ḯ' ] )

-- | @U+0345@ COMBINING GREEK YPOGEGRAMMENI: ◌ͅ
greekYpogegrammeni :: DeadKey
greekYpogegrammeni = DeadKey
  { _dkName = "Greek Ypogegrammeni"
  , _dkLabel = "◌ͅ"
  , _dkBaseChar = '\x037a'
  }

greekYpogegrammeniDef :: RawDeadKeyDefinition
greekYpogegrammeniDef = (greekYpogegrammeni,
    [ char '\x002E' '\x0345' {- Combining character -}
    , char '\x00A0' '\x037A' {- Spacing diacritic -}
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'α'] (RChar 'ᾀ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey graveAccent, SChar 'α'] (RChar 'ᾂ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey graveAccent, SChar 'Α'] (RChar 'ᾊ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey graveAccent, SChar 'η'] (RChar 'ᾒ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey graveAccent, SChar 'Η'] (RChar 'ᾚ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey graveAccent, SChar 'ω'] (RChar 'ᾢ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey graveAccent, SChar 'Ω'] (RChar 'ᾪ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey acuteAccent, SChar 'α'] (RChar 'ᾄ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey acuteAccent, SChar 'Α'] (RChar 'ᾌ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey acuteAccent, SChar 'η'] (RChar 'ᾔ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey acuteAccent, SChar 'Η'] (RChar 'ᾜ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey acuteAccent, SChar 'ω'] (RChar 'ᾤ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey acuteAccent, SChar 'Ω'] (RChar 'ᾬ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekPerispomeni, SChar 'α'] (RChar 'ᾆ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekPerispomeni, SChar 'Α'] (RChar 'ᾎ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekPerispomeni, SChar 'η'] (RChar 'ᾖ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekPerispomeni, SChar 'Η'] (RChar 'ᾞ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekPerispomeni, SChar 'ω'] (RChar 'ᾦ')
    , DeadKeyCombo [SDeadKey commaAbove, SDeadKey greekPerispomeni, SChar 'Ω'] (RChar 'ᾮ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Α'] (RChar 'ᾈ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'η'] (RChar 'ᾐ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Η'] (RChar 'ᾘ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'ω'] (RChar 'ᾠ')
    , DeadKeyCombo [SDeadKey commaAbove, SChar 'Ω'] (RChar 'ᾨ')
    , char 'ἀ' 'ᾀ'
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'α'] (RChar 'ᾁ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey graveAccent, SChar 'α'] (RChar 'ᾃ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey graveAccent, SChar 'Α'] (RChar 'ᾋ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey graveAccent, SChar 'η'] (RChar 'ᾓ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey graveAccent, SChar 'Η'] (RChar 'ᾛ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey graveAccent, SChar 'ω'] (RChar 'ᾣ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey graveAccent, SChar 'Ω'] (RChar 'ᾫ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey acuteAccent, SChar 'α'] (RChar 'ᾅ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey acuteAccent, SChar 'Α'] (RChar 'ᾍ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey acuteAccent, SChar 'η'] (RChar 'ᾕ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey acuteAccent, SChar 'Η'] (RChar 'ᾝ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey acuteAccent, SChar 'ω'] (RChar 'ᾥ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey acuteAccent, SChar 'Ω'] (RChar 'ᾭ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekPerispomeni, SChar 'α'] (RChar 'ᾇ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekPerispomeni, SChar 'Α'] (RChar 'ᾏ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekPerispomeni, SChar 'η'] (RChar 'ᾗ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekPerispomeni, SChar 'Η'] (RChar 'ᾟ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekPerispomeni, SChar 'ω'] (RChar 'ᾧ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SDeadKey greekPerispomeni, SChar 'Ω'] (RChar 'ᾯ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Α'] (RChar 'ᾉ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'η'] (RChar 'ᾑ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Η'] (RChar 'ᾙ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'ω'] (RChar 'ᾡ')
    , DeadKeyCombo [SDeadKey reversedCommaAbove, SChar 'Ω'] (RChar 'ᾩ')
    , char 'ἁ' 'ᾁ'
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ἀ'] (RChar 'ᾂ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ἁ'] (RChar 'ᾃ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ἀ'] (RChar 'ᾊ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ἁ'] (RChar 'ᾋ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ἠ'] (RChar 'ᾒ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ἡ'] (RChar 'ᾓ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ἠ'] (RChar 'ᾚ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ἡ'] (RChar 'ᾛ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ὠ'] (RChar 'ᾢ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ὡ'] (RChar 'ᾣ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ὠ'] (RChar 'ᾪ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'Ὡ'] (RChar 'ᾫ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'α'] (RChar 'ᾲ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'η'] (RChar 'ῂ')
    , DeadKeyCombo [SDeadKey graveAccent, SChar 'ω'] (RChar 'ῲ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ἀ'] (RChar 'ᾄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ἁ'] (RChar 'ᾅ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ἀ'] (RChar 'ᾌ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ἁ'] (RChar 'ᾍ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ἠ'] (RChar 'ᾔ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ἡ'] (RChar 'ᾕ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ἠ'] (RChar 'ᾜ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ἡ'] (RChar 'ᾝ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ὠ'] (RChar 'ᾤ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ὡ'] (RChar 'ᾥ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ὠ'] (RChar 'ᾬ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'Ὡ'] (RChar 'ᾭ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'α'] (RChar 'ᾴ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'η'] (RChar 'ῄ')
    , DeadKeyCombo [SDeadKey acuteAccent, SChar 'ω'] (RChar 'ῴ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ἀ'] (RChar 'ᾆ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ἁ'] (RChar 'ᾇ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ἀ'] (RChar 'ᾎ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ἁ'] (RChar 'ᾏ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ἠ'] (RChar 'ᾖ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ἡ'] (RChar 'ᾗ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ἠ'] (RChar 'ᾞ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ἡ'] (RChar 'ᾟ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ὠ'] (RChar 'ᾦ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ὡ'] (RChar 'ᾧ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ὠ'] (RChar 'ᾮ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'Ὡ'] (RChar 'ᾯ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'α'] (RChar 'ᾷ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'η'] (RChar 'ῇ')
    , DeadKeyCombo [SDeadKey greekPerispomeni, SChar 'ω'] (RChar 'ῷ')
    , char 'Ἀ' 'ᾈ'
    , char 'Ἁ' 'ᾉ'
    , char 'ἠ' 'ᾐ'
    , char 'ἡ' 'ᾑ'
    , char 'Ἠ' 'ᾘ'
    , char 'Ἡ' 'ᾙ'
    , char 'ὠ' 'ᾠ'
    , char 'ὡ' 'ᾡ'
    , char 'Ὠ' 'ᾨ'
    , char 'Ὡ' 'ᾩ'
    , char 'ὰ' 'ᾲ'
    , char 'α' 'ᾳ'
    , char 'ά' 'ᾴ'
    , char 'ᾶ' 'ᾷ'
    , char 'Α' 'ᾼ'
    , char 'ὴ' 'ῂ'
    , char 'η' 'ῃ'
    , char 'ή' 'ῄ'
    , char 'ῆ' 'ῇ'
    , char 'Η' 'ῌ'
    , char 'ὼ' 'ῲ'
    , char 'ω' 'ῳ'
    , char 'ώ' 'ῴ'
    , char 'ῶ' 'ῷ'
    , char 'Ω' 'ῼ' ] )

-- | @U+0348@ COMBINING DOUBLE VERTICAL LINE BELOW: ◌͈. Alias of 'doubledVerticalLineBelow'
doubleVerticalLineBelow :: DeadKey
doubleVerticalLineBelow = doubledVerticalLineBelow

-- | @U+035A@ COMBINING DOUBLE RING BELOW: ◌͚. Alias of 'doubledRingBelow'
doubleRingBelow :: DeadKey
doubleRingBelow = doubledRingBelow

-- | @U+035D@ COMBINING DOUBLE BREVE: ◌͝. Alias of 'doubleBreveAbove'
doubleBreve :: DeadKey
doubleBreve = doubleBreveAbove

-- | @U+035E@ COMBINING DOUBLE MACRON: ◌͞. Alias of 'doubleMacronAbove'
doubleMacron :: DeadKey
doubleMacron = doubleMacronAbove

-- | @U+0360@ COMBINING DOUBLE TILDE: ◌͠. Alias of 'doubleTildeAbove'
doubleTilde :: DeadKey
doubleTilde = doubleTildeAbove

-- | @U+0361@ COMBINING DOUBLE INVERTED BREVE: ◌͡. Alias of 'doubleInvertedBreveAbove'
doubleInvertedBreve :: DeadKey
doubleInvertedBreve = doubleInvertedBreveAbove

-- Block: Arabic----------------------------------------------------------------
-- | @U+0653@ ARABIC MADDAH ABOVE: ◌ٓ
arabicMaddahAbove :: DeadKey
arabicMaddahAbove = DeadKey
  { _dkName = "Arabic Maddah Above"
  , _dkLabel = "◌ٓ"
  , _dkBaseChar = '\x0653'
  }

arabicMaddahAboveDef :: RawDeadKeyDefinition
arabicMaddahAboveDef = (arabicMaddahAbove,
    [ char '\x002E' '\x0653' {- Combining character -}
    , char 'ا' 'آ' ] )

-- | @U+0654@ ARABIC HAMZA ABOVE: ◌ٔ
arabicHamzaAbove :: DeadKey
arabicHamzaAbove = DeadKey
  { _dkName = "Arabic Hamza Above"
  , _dkLabel = "◌ٔ"
  , _dkBaseChar = '\x0654'
  }

arabicHamzaAboveDef :: RawDeadKeyDefinition
arabicHamzaAboveDef = (arabicHamzaAbove,
    [ char '\x002E' '\x0654' {- Combining character -}
    , char 'ا' 'أ'
    , char 'و' 'ؤ'
    , char 'ي' 'ئ'
    , char 'ە' 'ۀ'
    , char 'ہ' 'ۂ'
    , char 'ے' 'ۓ' ] )

-- | @U+0655@ ARABIC HAMZA BELOW: ◌ٕ
arabicHamzaBelow :: DeadKey
arabicHamzaBelow = DeadKey
  { _dkName = "Arabic Hamza Below"
  , _dkLabel = "◌ٕ"
  , _dkBaseChar = '\x0655'
  }

arabicHamzaBelowDef :: RawDeadKeyDefinition
arabicHamzaBelowDef = (arabicHamzaBelow,
    [ char '\x002E' '\x0655' {- Combining character -}
    , char 'ا' 'إ' ] )

-- Block: Devanagari------------------------------------------------------------
-- | @U+093C@ DEVANAGARI SIGN NUKTA: ◌़
devanagariSignNukta :: DeadKey
devanagariSignNukta = DeadKey
  { _dkName = "Devanagari Sign Nukta"
  , _dkLabel = "◌़"
  , _dkBaseChar = '\x093c'
  }

devanagariSignNuktaDef :: RawDeadKeyDefinition
devanagariSignNuktaDef = (devanagariSignNukta,
    [ char '\x002E' '\x093C' {- Combining character -}
    , char 'न' 'ऩ'
    , char 'र' 'ऱ'
    , char 'ळ' 'ऴ' ] )

-- Block: Telugu----------------------------------------------------------------
-- | @U+0C56@ TELUGU AI LENGTH MARK: ◌ౖ
teluguAiLengthMark :: DeadKey
teluguAiLengthMark = DeadKey
  { _dkName = "Telugu Ai Length Mark"
  , _dkLabel = "◌ౖ"
  , _dkBaseChar = '\x0c56'
  }

teluguAiLengthMarkDef :: RawDeadKeyDefinition
teluguAiLengthMarkDef = (teluguAiLengthMark,
    [ char '\x002E' '\x0C56' {- Combining character -}
    , char 'ె' 'ై' ] )

-- Block: Sinhala---------------------------------------------------------------
-- | @U+0DCA@ SINHALA SIGN AL-LAKUNA: ◌්
sinhalaSignAlLakuna :: DeadKey
sinhalaSignAlLakuna = DeadKey
  { _dkName = "Sinhala Sign Al-Lakuna"
  , _dkLabel = "◌්"
  , _dkBaseChar = '\x0dca'
  }

sinhalaSignAlLakunaDef :: RawDeadKeyDefinition
sinhalaSignAlLakunaDef = (sinhalaSignAlLakuna,
    [ char '\x002E' '\x0DCA' {- Combining character -}
    , char 'ෙ' 'ේ' ] )

-- Block: Combining Diacritical Marks Extended----------------------------------
-- | @U+1AB0@ COMBINING DOUBLED CIRCUMFLEX ACCENT: ◌᪰. Alias of 'doubledCircumflexAbove'
doubledCircumflexAccent :: DeadKey
doubledCircumflexAccent = doubledCircumflexAbove

-- Block: Combining Diacritical Marks for Symbols-------------------------------
-- | @U+20D2@ COMBINING LONG VERTICAL LINE OVERLAY: ◌⃒. Alias of 'longVerticalLine'
longVerticalLineOverlay :: DeadKey
longVerticalLineOverlay = longVerticalLine

-- | @U+20D3@ COMBINING SHORT VERTICAL LINE OVERLAY: ◌⃓. Alias of 'shortVerticalLine'
shortVerticalLineOverlay :: DeadKey
shortVerticalLineOverlay = shortVerticalLine

-- | @U+20D7@ COMBINING RIGHT ARROW ABOVE: ◌⃗. Alias of 'rightwardsArrowAbove'
rightArrowAbove :: DeadKey
rightArrowAbove = rightwardsArrowAbove

-- | @U+20E5@ COMBINING REVERSE SOLIDUS OVERLAY: ◌⃥. Alias of 'reverseLongSolidus'
reverseSolidusOverlay :: DeadKey
reverseSolidusOverlay = reverseLongSolidus

-- | @U+20E6@ COMBINING DOUBLE VERTICAL STROKE OVERLAY: ◌⃦. Alias of 'doubledLongVerticalLine'
doubleVerticalStrokeOverlay :: DeadKey
doubleVerticalStrokeOverlay = doubledLongVerticalLine

-- | @U+20E8@ COMBINING TRIPLE UNDERDOT: ◌⃨. Alias of 'threeDotsBelow'
tripleUnderdot :: DeadKey
tripleUnderdot = threeDotsBelow

-- | @U+20EB@ COMBINING LONG DOUBLE SOLIDUS OVERLAY: ◌⃫. Alias of 'doubledLongSolidus'
longDoubleSolidusOverlay :: DeadKey
longDoubleSolidusOverlay = doubledLongSolidus

-- | @U+20EF@ COMBINING RIGHT ARROW BELOW: ◌⃯. Alias of 'rightwardsArrowBelow'
rightArrowBelow :: DeadKey
rightArrowBelow = rightwardsArrowBelow

-- Block: Hiragana--------------------------------------------------------------
-- | @U+3099@ COMBINING KATAKANA-HIRAGANA VOICED SOUND MARK: ◌゙
katakanaHiraganaVoicedSoundMark :: DeadKey
katakanaHiraganaVoicedSoundMark = DeadKey
  { _dkName = "Katakana-Hiragana Voiced Sound Mark"
  , _dkLabel = "◌゙"
  , _dkBaseChar = '\x309b'
  }

katakanaHiraganaVoicedSoundMarkDef :: RawDeadKeyDefinition
katakanaHiraganaVoicedSoundMarkDef = (katakanaHiraganaVoicedSoundMark,
    [ char '\x002E' '\x3099' {- Combining character -}
    , char '\x00A0' '\x309B' {- Spacing diacritic -}
    , char 'か' 'が'
    , char 'き' 'ぎ'
    , char 'く' 'ぐ'
    , char 'け' 'げ'
    , char 'こ' 'ご'
    , char 'さ' 'ざ'
    , char 'し' 'じ'
    , char 'す' 'ず'
    , char 'せ' 'ぜ'
    , char 'そ' 'ぞ'
    , char 'た' 'だ'
    , char 'ち' 'ぢ'
    , char 'つ' 'づ'
    , char 'て' 'で'
    , char 'と' 'ど'
    , char 'は' 'ば'
    , char 'ひ' 'び'
    , char 'ふ' 'ぶ'
    , char 'へ' 'べ'
    , char 'ほ' 'ぼ'
    , char 'う' 'ゔ'
    , char 'ゝ' 'ゞ'
    , char 'カ' 'ガ'
    , char 'キ' 'ギ'
    , char 'ク' 'グ'
    , char 'ケ' 'ゲ'
    , char 'コ' 'ゴ'
    , char 'サ' 'ザ'
    , char 'シ' 'ジ'
    , char 'ス' 'ズ'
    , char 'セ' 'ゼ'
    , char 'ソ' 'ゾ'
    , char 'タ' 'ダ'
    , char 'チ' 'ヂ'
    , char 'ツ' 'ヅ'
    , char 'テ' 'デ'
    , char 'ト' 'ド'
    , char 'ハ' 'バ'
    , char 'ヒ' 'ビ'
    , char 'フ' 'ブ'
    , char 'ヘ' 'ベ'
    , char 'ホ' 'ボ'
    , char 'ウ' 'ヴ'
    , char 'ワ' 'ヷ'
    , char 'ヰ' 'ヸ'
    , char 'ヱ' 'ヹ'
    , char 'ヲ' 'ヺ'
    , char 'ヽ' 'ヾ' ] )

-- | @U+309A@ COMBINING KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK: ◌゚
katakanaHiraganaSemiVoicedSoundMark :: DeadKey
katakanaHiraganaSemiVoicedSoundMark = DeadKey
  { _dkName = "Katakana-Hiragana Semi-Voiced Sound Mark"
  , _dkLabel = "◌゚"
  , _dkBaseChar = '\x309c'
  }

katakanaHiraganaSemiVoicedSoundMarkDef :: RawDeadKeyDefinition
katakanaHiraganaSemiVoicedSoundMarkDef = (katakanaHiraganaSemiVoicedSoundMark,
    [ char '\x002E' '\x309A' {- Combining character -}
    , char '\x00A0' '\x309C' {- Spacing diacritic -}
    , char 'は' 'ぱ'
    , char 'ひ' 'ぴ'
    , char 'ふ' 'ぷ'
    , char 'へ' 'ぺ'
    , char 'ほ' 'ぽ'
    , char 'ハ' 'パ'
    , char 'ヒ' 'ピ'
    , char 'フ' 'プ'
    , char 'ヘ' 'ペ'
    , char 'ホ' 'ポ' ] )

-- Block: Kaithi----------------------------------------------------------------
-- | @U+110BA@ KAITHI SIGN NUKTA: ◌𑂺
kaithiSignNukta :: DeadKey
kaithiSignNukta = DeadKey
  { _dkName = "Kaithi Sign Nukta"
  , _dkLabel = "◌𑂺"
  , _dkBaseChar = '\x110ba'
  }

kaithiSignNuktaDef :: RawDeadKeyDefinition
kaithiSignNuktaDef = (kaithiSignNukta,
    [ char '\x002E' '\x110BA' {- Combining character -}
    , char '𑂙' '𑂚'
    , char '𑂛' '𑂜'
    , char '𑂥' '𑂫' ] )


-- | Extra chained dead keys definitions
diacriticsExtraDef :: RawDeadKeyDefinitions
diacriticsExtraDef =
  [ (acuteAbove, [chainedDeadKey acuteAbove doubledAcute])
  , (graveAbove, [chainedDeadKey graveAbove doubledGrave])
  , (circumflexAbove, [chainedDeadKey circumflexAbove circumflexBelow])
  , (circumflexBelow, [chainedDeadKey circumflexBelow circumflexAbove])
  , (hatchekAbove, [chainedDeadKey hatchekAbove hatchekBelow])
  , (hatchekBelow, [chainedDeadKey hatchekBelow hatchekAbove])
  , (tildeAbove, [chainedDeadKey tildeAbove tildeBelow])
  , (tildeBelow, [chainedDeadKey tildeBelow tildeAbove])
  , (breveAbove, [chainedDeadKey breveAbove breveBelow])
  , (breveBelow, [chainedDeadKey breveBelow breveAbove])
  , (invertedBreveAbove, [chainedDeadKey invertedBreveAbove invertedBreveBelow])
  , (invertedBreveBelow, [chainedDeadKey invertedBreveBelow invertedBreveAbove])
  , (palatalizedHookBelow, [chainedDeadKey palatalizedHookBelow retroflexHookBelow])
  , (retroflexHookBelow, [chainedDeadKey retroflexHookBelow palatalizedHookBelow])
  , (macronAbove, [chainedDeadKey macronAbove macronBelow])
  , (macronBelow, [chainedDeadKey macronBelow macronAbove])
  , (overline, [chainedDeadKey overline lowLine])
  , (lowLine, [chainedDeadKey lowLine overline])
  , (dotAbove,
      [ chainedDeadKey dotAbove dotBelow
      , chainedDeadKey diaeresisAbove threeDotsAbove
      , chainedDeadKey threeDotsAbove fourDotsAbove ])
  , (dotBelow,
      [ chainedDeadKey dotBelow dotAbove
      , chainedDeadKey diaeresisBelow threeDotsBelow ])
  , (diaeresisAbove,
      [ chainedDeadKey dotAbove threeDotsAbove
      , chainedDeadKey diaeresisAbove diaeresisBelow ])
  , (diaeresisBelow,
      [ chainedDeadKey dotBelow threeDotsBelow
      , chainedDeadKey diaeresisBelow diaeresisAbove
      ])
  , (threeDotsAbove, [chainedDeadKey dotAbove fourDotsAbove])
  , (longStroke, [chainedDeadKey longStroke doubledLongStroke])
  , (longVerticalLine, [chainedDeadKey longVerticalLine doubledLongVerticalLine])
  , (ringAbove, [chainedDeadKey ringAbove ringBelow])
  , (ringBelow, [chainedDeadKey ringBelow ringAbove])
  , (rightwardsArrowBelow, [chainedDeadKey rightwardsArrowBelow doubleRightwardsArrowBelow])
  ]
