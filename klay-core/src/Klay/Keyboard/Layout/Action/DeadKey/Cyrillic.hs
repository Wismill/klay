{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Cyrillic
  ( cyrillic
  , cyrillicDef
  ) where


import Data.Map.Strict qualified as Map
import Data.Char (toUpper)

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.ISO_9995_9 (latinToCyrillicISO_9995_9)


cyrillic :: DeadKey
cyrillic = DeadKey
  { _dkName = "cyrillic"
  , _dkLabel = "г"
  , _dkBaseChar = 'г'
  }

cyrillicDef :: RawDeadKeyDefinitions
cyrillicDef = [(cyrillic, Map.foldlWithKey f mempty latinToCyrillicISO_9995_9 )]
  where
    f :: [DeadKeyCombo] -> Char -> Char -> [DeadKeyCombo]
    f combos char1 char2 = char char1 char2 : char (toUpper char1) (toUpper char2) : combos
