{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Games
  ( GamesMnemonic(..)
  , games
  , gamesDef
  ) where


import Data.List.NonEmpty qualified as NE

import Klay.Keyboard.Layout.Action.DeadKey


games :: DeadKey
games = DeadKey
  { _dkName = "Games"
  , _dkLabel = "♠"
  , _dkBaseChar = '♠'
  }

data GamesMnemonic = GamesMnemonic
  { _gChess :: (Char, String)
  , _gDraughts :: (Char, String)
  , _gCards :: (Char, Char, String, String) -- Prefix, colors, values
  , _gDominoes :: Char
  }

gamesDef :: GamesMnemonic -> RawDeadKeyDefinitions
gamesDef GamesMnemonic
  { _gChess = (chess_prefix, chess_chars)
  , _gDraughts = (draughts_prefix, draughts_chars)
  , _gCards = (cards_prefix1, cards_prefix2, color_chars, value_chars)
  , _gDominoes = dominoes_prefix
  } = [(games, mconcat
    [ mkGameCombos Nothing "123456" "⚀⚁⚂⚃⚄⚅"
    , mkGameCombos (Just chess_prefix) chess_chars "♔♕♖♗♘♙♚♛♜♝♞♟"
    , mkGameCombos (Just draughts_prefix) draughts_chars "⛀⛁⛂⛃"
    , mkGameCombos (Just cards_prefix1) color_chars "♤♡♢♧♠♥♦♣🂠🃟🃏🂿"
    , mkGameCombos2 (Just cards_prefix2) cards "🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂬🂭🂮🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂼🂽🂾🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊🃋🃌🃍🃎🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃜🃝🃞"
    , mkGameCombos2 (Just dominoes_prefix) dominoes "🀱🀲🀳🀴🀵🀶🀷🀸🀹🀺🀻🀼🀽🀾🀿🁀🁁🁂🁃🁄🁅🁆🁇🁈🁉🁊🁋🁌🁍🁎🁏🁐🁑🁒🁓🁔🁕🁖🁗🁘🁙🁚🁛🁜🁝🁞🁟🁠🁡"
    ])]
  where
    cards = [[color, value] | color <- take 4 color_chars, value <- value_chars]
    dominoes = [[v1, v2] | v1 <- ['0'..'6'], v2 <- ['0'..'6']]

mkGameCombos :: Maybe Char -> String -> String -> [DeadKeyCombo]
mkGameCombos Nothing       mnemo results = (\(c1, c2) -> chars [c1] c2) <$> zip mnemo results
mkGameCombos (Just prefix) mnemo results = (\(c1, c2) -> chars [prefix, c1] c2) <$> zip mnemo results

mkGameCombos2 :: Maybe Char -> [NE.NonEmpty Char] -> String -> [DeadKeyCombo]
mkGameCombos2 Nothing       mnemo results = uncurry chars <$> zip mnemo results
mkGameCombos2 (Just prefix) mnemo results = uncurry chars <$> zip (NE.cons prefix <$> mnemo) results

{-
🂠 🃏 🃟 🂿
Domino
🀰
🀱 🀲 🀳 🀴 🀵 🀶 🀷
🀸 🀹 🀺 🀻 🀼 🀽 🀾
🀿 🁀 🁁 🁂 🁃 🁄 🁅
🁆 🁇 🁈 🁉 🁊 🁋 🁌
🁍 🁎 🁏 🁐 🁑 🁒 🁓
🁔 🁕 🁖 🁗 🁘 🁙 🁚
🁛 🁜 🁝 🁞 🁟 🁠 🁡
Mahjong
🀇 🀈 🀉 🀊 🀋 🀌 🀍 🀎 🀏
🀐 🀑 🀒 🀓 🀔 🀕 🀖 🀗 🀘
🀙 🀚 🀛 🀜 🀝 🀞 🀟 🀠 🀡
🀀 🀁 🀂 🀃
🀢 🀣 🀤 🀥
🀦 🀧 🀨 🀩
🀄 🀅 🀆 🀪
🀫
-}
