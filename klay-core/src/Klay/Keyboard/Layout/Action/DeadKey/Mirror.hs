{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Mirror
  ( mirror
  , mirrorDef
  ) where


import Klay.Keyboard.Layout.Action.DeadKey hiding (mkCombo)


mirror :: DeadKey
mirror = DeadKey
  { _dkName = "Mirror"
  , _dkLabel = "╬" -- Also: ⧞⋮↔⫿∃╬⌯
  , _dkBaseChar = '╬'
  }


mirrorDef :: RawDeadKeyDefinitions
mirrorDef = [(mirror, mkCombo <$> definitions)]
  where mkCombo [c1, c2] = char c1 c2
        mkCombo c        = error $ "[ERROR] Invalid definition in Klay.Keyboard.Layout.Action.DeadKey.Mirror.mirrorDef: " <> c


definitions :: [String]
definitions =
  [ "AⱯ"
  , "cɔ"
  , "CƆ"
  , "EƎ"
  , "KꞰ"
  , "Pꟼ"
  , "Rᴙ"
  , "sƨ"
  , "SƧ"
  , "TꞱ"
  , "‸ˇ"
  , "VɅ"
  -- [TODO], "ɛ" -- U025B LATIN SMALL LETTER OPEN E
  , "3Ɛ" -- U0190 LATIN CAPITAL LETTER OPEN E
  , "?⸮"
  , ";⁏"
  , "’‛"
  , "”‟"
  , "′‵"
  , "″‶"
  , "‴‷"
  , "†⸸"
  --, "A∀" -- [FIXME] Conflict
  --, "E∃" -- [FIXME] Conflict
  , "⊂⊃"
  , "⊄⊅"
  , "⊆⊇"
  , "⊊⊋"
  , "⊑⊒"
  , "⊏⊐"
  , "⋤⋥"
  , "⋢⋣"
  , "*⁎"
  , "⊲⊳"
  , "⋪⋫"
  , "⊴⊵"
  , "⋬⋭"
  , "∼∽"
  , "⊢⊣"
  , "∈∋"
  , "∉∌"
  ]
