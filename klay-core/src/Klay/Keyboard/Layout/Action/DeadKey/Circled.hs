{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Circled
  ( -- * Positive circled characters
    circled
  , circledDef
  , mkCircledDef
  , mkCircledNumberDef
    -- * Negative circled characters
  , negativeCircled
  , negativeCircledDef
  , mkNegativeCircledNumberDef
  ) where

import Data.Maybe
import Data.List.NonEmpty qualified as NE

import Klay.Utils.Unicode.Circled qualified as C
import Klay.Keyboard.Layout.Action.DeadKey

-- | Dead key for circled characters, such as: ⑩.
circled :: DeadKey
circled = DeadKey
  { _dkName = "Circled"
  , _dkLabel = "⑩"
  , _dkBaseChar = '⑩'
  }

circledDef :: RawDeadKeyDefinitions
circledDef = [(circled, mkCircledDef mempty (SDeadKey circled))]

mkCircledDef :: [Symbol] -> Symbol -> [DeadKeyCombo]
mkCircledDef prefix s = mconcat
  [ mkComboC <$> cs
  , mkCircledNumberDef prefix s ]
  where
    mkComboC c = mkCombo (NE.fromList $ prefix <> [SChar c]) (fromJust . C.circled $ c)
    cs = mconcat [['A'..'Z'], ['a'..'z'], ['0'..'9']]

mkCircledNumberDef :: [Symbol] -> Symbol -> [DeadKeyCombo]
mkCircledNumberDef prefix s = mconcat
  [ mkComboN prefix             <$> zip ['0'..'9'] [0..9]
  , mkComboN (prefix <> [s])    <$> zip ['0'..'9'] [10..19]
  , mkComboN (prefix <> [s, s]) <$> zip ['0'..'9'] [20]
  ]
  where
    mkComboN ss (c, n) = mkCombo (NE.fromList $ ss <> [SChar c]) (fromJust . C.circledNumber $ n)

-- | Negative circled characters, such as: ❿.
negativeCircled :: DeadKey
negativeCircled = DeadKey
  { _dkName = "Negative Circled"
  , _dkLabel = "❿"
  , _dkBaseChar = '❿'
  }

negativeCircledDef :: RawDeadKeyDefinitions
negativeCircledDef = [(negativeCircled, mkNegativeCircledNumberDef mempty (SDeadKey negativeCircled))]

mkNegativeCircledNumberDef :: [Symbol] -> Symbol -> [DeadKeyCombo]
mkNegativeCircledNumberDef prefix s = mconcat
  [ mkComboN prefix             <$> zip ['0'..'9'] [0..9]
  , mkComboN (prefix <> [s])    <$> zip ['0'..'9'] [10..19]
  , mkComboN (prefix <> [s, s]) <$> zip ['0'..'9'] [20]
  ]
  where
    mkComboN ss (c, n) = mkCombo (NE.fromList $ ss <> [SChar c]) (fromJust . C.negativeCircledNumber $ n)
