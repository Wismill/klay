{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE OverloadedStrings #-}

module Klay.Keyboard.Layout.Action.DeadKey.Greek
  ( greek
  , greekDef
  ) where


import Data.Map.Strict qualified as Map
import Data.Char (toLower)

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.ISO_9995_9 (latinToGreekISO_9995_9)


greek :: DeadKey
greek = DeadKey
  { _dkName = "Greek"
  , _dkLabel = "µ"
  , _dkBaseChar = 'µ'
  }

greekDef :: RawDeadKeyDefinitions
greekDef = [(greek, Map.foldlWithKey f mempty latinToGreekISO_9995_9 <> extraDef)]
  where
    f :: [DeadKeyCombo] -> Char -> Char -> [DeadKeyCombo]
    f combos char1 char2 = char char1 char2 : char (toLower char1) (toLower char2) : combos
    extraDef :: [DeadKeyCombo]
    extraDef =
      [ char 'w' 'ς'
      , mkCombo [greek] 'µ'
      -- [TODO] ϴ
      ]


