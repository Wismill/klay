-- [NOTE] Auto-generated module. Do not edit manually

{-|
Description : “science” dead key from the Bépo layout
-}

module Klay.Keyboard.Layout.Action.DeadKey.Bépo.Science
  ( bépoScience, bépoScienceDef
  ) where

import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All
