-- [NOTE] Auto-generated module. Do not edit manually

{-|
Description : “latin” dead key from the Bépo layout
-}

module Klay.Keyboard.Layout.Action.DeadKey.Bépo.Latin
  ( bépoLatin, bépoLatinDef
  ) where

import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All

-- Source: https://bepo.fr/wiki/Latin_et_ponctuation

