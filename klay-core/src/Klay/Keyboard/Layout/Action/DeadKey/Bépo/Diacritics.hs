-- [NOTE] Auto-generated module. Do not edit manually

{-|
Description : Additional diacritics dead key from the Bépo layout
-}

module Klay.Keyboard.Layout.Action.DeadKey.Bépo.Diacritics
  ( bépoDef
  , ringBelow, ringBelowDef
  , dotBelow, dotBelowDef
  , ringAbove, ringAboveDef
  , tildeAbove, tildeAboveDef
  , hatchekBelow, hatchekBelowDef
  , circumflexBelow, circumflexBelowDef
  , bépoDotBelow2, bépoDotBelow2Def
  , commaBelow, commaBelowDef
  , diaeresisAbove, diaeresisAboveDef
  , tildeOverlay, tildeOverlayDef
  , macronBelow, macronBelowDef
  , crosse, crosseDef
  , hatchekAbove, hatchekAboveDef
  , crochetEnChef, crochetEnChefDef
  , doubleGrave, doubleGraveDef
  , horn, hornDef
  , circumflexAbove, circumflexAboveDef
  , longSolidus, longSolidusDef
  , longStroke, longStrokeDef
  , ogonek, ogonekDef
  , cedillaBelow, cedillaBelowDef
  , acuteAbove, acuteAboveDef
  , invertedBreveAbove, invertedBreveAboveDef
  , macronAbove, macronAboveDef
  , dotAbove, dotAboveDef
  , doubledLongStroke, doubledLongStrokeDef
  , diaeresisBelow, diaeresisBelowDef
  , breveAbove, breveAboveDef
  , graveAbove, graveAboveDef
  , doubleAcute, doubleAcuteDef
  ) where

import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All
