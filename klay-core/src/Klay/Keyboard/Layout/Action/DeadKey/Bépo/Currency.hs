-- [NOTE] Auto-generated module. Do not edit manually

{-|
Description : “currency” dead key from the Bépo layout
-}

module Klay.Keyboard.Layout.Action.DeadKey.Bépo.Currency
  ( bépoCurrency, bépoCurrencyDef
  ) where

import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All
