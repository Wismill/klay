module Klay.Keyboard.Layout.Action.DeadKey.SmallCapitals
  ( smallCapitals
  , smallCapitalsDef
  ) where

import Data.Maybe (mapMaybe)

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.Unicode.Latin qualified as L


smallCapitals :: DeadKey
smallCapitals = DeadKey
  { _dkName = "Small capitals"
  , _dkLabel = "ᴀ"
  , _dkBaseChar = 'ᴀ'
  }

smallCapitalsDef :: RawDeadKeyDefinition
smallCapitalsDef =
  ( smallCapitals
  , mapMaybe (\c -> char c <$> L.latinSmallCapital c) ('æ':'œ':['a'..'z'])
  )
