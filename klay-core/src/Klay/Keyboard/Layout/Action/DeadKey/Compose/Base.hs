module Klay.Keyboard.Layout.Action.DeadKey.Compose.Base
  ( compose
  ) where

import Klay.Keyboard.Layout.Action.DeadKey

-- | The /compose/ dead key
compose :: DeadKey
compose = DeadKey
  { _dkName = "Compose"
  , _dkLabel = "⎄"
  , _dkBaseChar = '♫'
  }
