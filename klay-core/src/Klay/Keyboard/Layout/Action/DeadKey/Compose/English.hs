{-# LANGUAGE OverloadedLists   #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}

module Klay.Keyboard.Layout.Action.DeadKey.Compose.English
  ( C.Mnemonic(..)
  , mnemonic
  , C.mkMathMnemonic
  , C.compose
  , composeDef
  , C.ligaturesDef
  , C.ordinalDef
  , unitsDef
  , C.numbersDef, C.fractionsDef, C.miscNumbersDef
  , punctuationDef
  , mathematicsDef
  , C.intellectualPropertyDef )
  where

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Compose (Mnemonic(..))
import Klay.Keyboard.Layout.Action.DeadKey.Compose qualified as C

-- | English Mnemonic for compose
mnemonic :: Mnemonic
mnemonic = Mnemonic
  { _mnNegation = '!'
  , _mnLineThrough = '/'
  , _mnWord = '&'

  , _mnLeft = '<'
  , _mnRight = '>'
  , _mnBottom = '_'
  , _mnTop = '^'
  , _mnCenter = 'c'
  , _mnVertical = 'v'
  , _mnMirror = 'm'
  , _mnHMirror = 'M'
  , _mnRotation = 'R'

  , _mnCircled = '@'
  , _mnComplementary = '%'
  , _mnSquared = '#'
  }

-- 𝐧, 𝐬, 𝐰, 𝒍, 𝒓, 𝒃, 𝒕, 𝒄, 𝒗, 𝒎, 𝕞, 𝕣, 𝒐, 𝕟, 𝕤 :: Char
-- Mnemonic 𝐧 𝐬 𝐰 𝒍 𝒓 𝒃 𝒕 𝒄 𝒗 𝒎 𝕞 𝕣 𝒐 𝕟 𝕤 = mnemonic
𝐧, 𝐰 :: Char
Mnemonic 𝐧 _ 𝐰 _ _ _ _ _ _ _ _ _ _ _ _ = mnemonic

-- | Default 'C.compose' definition with English mnemonic
composeDef :: RawDeadKeyDefinitions
composeDef = composeDef' <> extraDef
  where
    composeDef' = C.composeDef mnemonic
    extraDef = [ (C.compose, mconcat
      [ unitsDef
      , punctuationDef
      , mathematicsDef
      ])]

unitsDef :: [DeadKeyCombo]
unitsDef = C.unitsDef mnemonic <>
  [ chars [𝐰, 'e', 's', 't', 'i', 'm'] '℮'
  --, chars [𝐰, 'i', 'n', 'f', 'o'] 'ℹ' [TODO] conflict
  ]

punctuationDef :: [DeadKeyCombo]
punctuationDef = C.punctuationDef mnemonic <>
  [ ]

mathematicsDef :: [DeadKeyCombo]
mathematicsDef = C.mathematicsDef mnemonic <>
  [ chars [𝐰, 'e', 'x', 'i', 's', 't', 's'] '∃'
  , chars [𝐰, 𝐧, 'e', 'x', 'i', 's', 't', 's'] '∄'
  , chars [𝐰, 'f', 'o', 'r', 'a', 'l', 'l'] '∀'

  , chars [𝐰, 'e', 'l', 'e', 'm'] '∈'
  , chars [𝐰, 𝐧, 'e', 'l', 'e', 'm'] '∉'
  , chars [𝐰, 'i', 'n'] '∈'
  , chars [𝐰, 𝐧, 'i', 'n'] '∉'
  , chars [𝐰, 'c', 'o', 'n', 't', 'a', 'i', 'n', 's'] '∋'
  , chars [𝐰, 𝐧, 'c', 'o', 'n', 't', 'a', 'i', 'n', 's'] '∌'
  , chars [𝐰, 'n', 'i'] '∋'
  , chars [𝐰, 𝐧, 'n', 'i'] '∌'

  --, chars [𝐰, 'i', 'n', 'f'] '∞' [TODO] conflict with "in"

  , chars [𝐰, 'n', 'o', 't'] '¬'
  , chars [𝐰, 'a', 'n', 'd'] '∧'
  , chars [𝐰, 'o', 'r'] '∨'
  , chars [𝐰, 'n', 'a', 'n', 'd'] '⊼'
  , chars [𝐰, 'n', 'o', 'r'] '⊽'
  , chars [𝐰, 'x', 'o', 'r'] '⊻'
  --, chars [𝐰, 'x', 'n', 'o', 'r'] '⊽̲'
  ]
