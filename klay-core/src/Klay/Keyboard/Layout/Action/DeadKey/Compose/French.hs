{-# LANGUAGE OverloadedLists   #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}

module Klay.Keyboard.Layout.Action.DeadKey.Compose.French
  ( C.Mnemonic(..)
  , mnemonic
  , C.mkMathMnemonic
  , C.compose
  , composeDef
  , C.ligaturesDef
  , C.ordinalDef
  , unitsDef
  , C.numbersDef, C.fractionsDef, C.miscNumbersDef
  , punctuationDef
  , mathematicsDef
  , C.intellectualPropertyDef
  , gamesDef )
  where

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Compose (Mnemonic(..))
import Klay.Keyboard.Layout.Action.DeadKey.Compose qualified as C
import Klay.Keyboard.Layout.Action.DeadKey.Compose.English qualified as EN
import Klay.Keyboard.Layout.Action.DeadKey.Games qualified as G

-- | French Mnemonic for 'C.compose'
mnemonic :: Mnemonic
mnemonic = EN.mnemonic
--  where (C.Mnemonic n s w _ _ _ _ _ _ _ _ _ o n') = EN.mnemonic

-- 𝐧, 𝐬, 𝐰, 𝒍, 𝒓, 𝒃, 𝒕, 𝒄, 𝒗, 𝒎, 𝕞, 𝕣, 𝒐, 𝕟, 𝕤 :: Char
𝐧, 𝐰 :: Char
-- Mnemonic 𝐧 𝐬 𝐰 𝒍 𝒓 𝒃 𝒕 𝒄 𝒗 𝒎 𝕞 𝕣 𝒐 𝕟 𝕤 = mnemonic
Mnemonic 𝐧 _ 𝐰 _ _ _ _ _ _ _ _ _ _ _ _ = mnemonic

-- | Default 'C.compose' definition with French mnemonic
composeDef :: RawDeadKeyDefinitions
composeDef = composeDef' <> extraDef
  where
    composeDef' = C.composeDef mnemonic
    extraDef = [(C.compose, mconcat
      [ unitsDef
      , punctuationDef
      , mathematicsDef
      , gamesDef
      ])]

unitsDef :: [DeadKeyCombo]
unitsDef = C.unitsDef mnemonic <>
  [ chars [𝐰, 'e', 's', 't', 'i', 'm'] '℮'
  --, chars [𝐰, 'i', 'n', 'f', 'o'] 'ℹ' [TODO] conflict
  ]

punctuationDef :: [DeadKeyCombo]
punctuationDef = C.punctuationDef mnemonic <>
  [ chars [' ', 'i'] '\xA0' -- espace insécable [TODO] other spaces
  ]

mathematicsDef :: [DeadKeyCombo]
mathematicsDef = C.mathematicsDef mnemonic <>
  [ chars [𝐰, 'e', 'x', 'i', 's', 't', 'e'] '∃'
  , chars [𝐰, 𝐧, 'e', 'x', 'i', 's', 't', 'e'] '∄'
  , chars [𝐰, 't', 'o', 'u'] '∀' -- [NOTE] singulier et pluriel

  , chars [𝐰, 'e', 'l', 'e', 'm'] '∈'
  , chars [𝐰, '!', 'e', 'l', 'e', 'm'] '∉'
  , chars [𝐰, 'i', 'n'] '∈'
  , chars [𝐰, 𝐧, 'i', 'n'] '∉'
  , chars [𝐰, 'c', 'o', 'n', 't', 'i', 'e', 'n', 't'] '∋'
  , chars [𝐰, '!', 'c', 'o', 'n', 't', 'i', 'e', 'n', 't'] '∌'
  , chars [𝐰, 'n', 'i'] '∋'
  , chars [𝐰, 𝐧, 'n', 'i'] '∌'

  --, chars [𝐰, 'i', 'n', 'f'] '∞' [TODO] conflict with "in"

  , chars [𝐰, 'n', 'o', 't'] '¬', chars [𝐰, 'n', 'o', 'n'] '¬'
  , chars [𝐰, 'a', 'n', 'd'] '∧', chars [𝐰, 'e', 't'] '∧'
  , chars [𝐰, 'o', 'r'] '∨', chars [𝐰, 'o', 'u'] '∨'
  , chars [𝐰, 'n', 'a', 'n', 'd'] '⊼'
  , chars [𝐰, 'n', 'o', 'r'] '⊽'
  , chars [𝐰, 'x', 'o', 'r'] '⊻'
  ]

gamesMnemonic :: G.GamesMnemonic
gamesMnemonic = G.GamesMnemonic
  { G._gChess = ('é', "rdtfpRDTFP") -- Échecs: ♔♕♖♗♘♙♚♛♜♝♞♟
  , G._gDraughts = ('d', "pdPD") -- Dames: ⛀⛁⛂⛃
  , G._gCards = ('c', 'C', "pcktPCKTdjJr", "a234567890vcdr") -- Cartes: ♤♡♢♧♠♥♦♣🂠🃟🃏🂿, 🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂬🂭🂮🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂼🂽🂾🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊🃋🃌🃍🃎🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃜🃝🃞
  , G._gDominoes = 'd'
  }

gamesDef :: [DeadKeyCombo]
gamesDef = C.includeDefsWithPrefix ['j'] (G.gamesDef gamesMnemonic)
