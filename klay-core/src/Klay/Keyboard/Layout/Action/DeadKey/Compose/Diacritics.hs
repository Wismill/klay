{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.Action.DeadKey.Compose.Diacritics
  ( makeComposeDiacriticsDef
  , defaultDiacriticsDef
  , defaultDiacriticsMnemonics
  , diacriticsExtraComposeDef )
  where


import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.List.NonEmpty qualified as NE

import Klay.Keyboard.Layout.Action.CombiningCharacters qualified as CC
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Compose.Base (compose)


-- | Compose combinations obtained by replacing dead keys with a corresponding mnemonic character
defaultDiacriticsDef :: [DeadKeyCombo]
defaultDiacriticsDef = makeComposeDiacriticsDef defaultDiacriticsMnemonics

makeComposeDiacriticsDef :: [(DeadKey, String)] -> [DeadKeyCombo]
makeComposeDiacriticsDef diacritics_mnemonics = mconcat $ mkCombos <$> diacritics_mnemonics
  where
    mkCombos :: (DeadKey, String) -> [DeadKeyCombo]
    mkCombos (dk, mnemonic) = case Map.lookup dk diacriticsDef' of
      Nothing   -> error "Cannot found dead key"
      Just defs -> foldr (prepend mnemonic) mempty defs
    diacriticsDef' :: Map DeadKey [DeadKeyCombo]
    diacriticsDef' = Map.fromList DK.diacriticsDef
    prepend :: String -> DeadKeyCombo -> [DeadKeyCombo] -> [DeadKeyCombo]
    prepend _ (DeadKeyCombo _ (RDeadKey _)) combos = combos
    prepend mnemonic (DeadKeyCombo symbols result) combos
      | any isSDK symbols = combos
      | otherwise = let mnemonic' = NE.fromList $ SChar <$> mnemonic
                    in DeadKeyCombo (mnemonic' <> symbols) result : combos
    isSDK (SDeadKey _) = True
    isSDK _            = False


{-| Mnemotechnic characters: @[(dead key, mnemonic character)]@

[Source](http://bepo.fr/wiki/Version_1.1rc1/Touches_mortes/Composition)
-}
defaultDiacriticsMnemonics :: [(DeadKey, String)]
defaultDiacriticsMnemonics =
  [ (DK.acuteAbove, "'")
  --, (DK.acuteBelow, "_'") [FIXME]
  , (DK.doubledAcute, "\"")
  , (DK.graveAbove, "`")
  , (DK.graveBelow, "_`")
  , (DK.doubledGrave, "2`")
  , (DK.circumflexAbove, "<")
  , (DK.circumflexBelow, "_<")
  , (DK.hatchekAbove, ">")
  , (DK.hatchekBelow, "_>")
  , (DK.breveAbove, ")")
  --, (DK.breveBelow, "_)") [FIXME]
  , (DK.invertedBreveAbove, "(")
  --, (DK.invertedBreveBelow, "_(") [FIXME]
  , (DK.macronAbove, "|")
  , (DK.macronBelow, "_|")
  , (DK.tildeAbove, "~")
  , (DK.tildeBelow, "_~")
  --, (DK.dotAbove, ".")
  --, (DK.dotAelow, "_.")
  , (DK.diaeresisAbove, ":")
  , (DK.diaeresisBelow, "_:")
  , (DK.ringAbove, "0")
  , (DK.commaBelow, ";")   -- [NOTE] Separated from letter (. + ,)
  , (DK.cedillaBelow, ",") -- [NOTE] Linked to letter (,)
  , (DK.ogonek, "[")
  , (DK.longSolidus, "/")
  -- [TODO] crosse
  ]

-- | Extra diacritics definitions
diacriticsExtraComposeDef :: [DeadKeyCombo]
diacriticsExtraComposeDef = mconcat
  [ mkCombo' [SChar '2', SDeadKey DK.graveAbove] DK.doubleGrave
  , mkCombo' [SChar '2', SDeadKey DK.acuteAbove] DK.doubleAcute
  , mkCombo' [SChar '2', SDeadKey DK.circumflexAbove] CC.doubleCircumflexAbove
  , mkCombo' [SChar '2', SDeadKey DK.tildeAbove] CC.doubleTildeAbove
  , mkCombo' [SChar '2', SDeadKey DK.breveAbove] CC.doubleBreveAbove
  , mkCombo' [SChar '2', SDeadKey DK.breveBelow] CC.doubleBreveBelow
  , mkCombo' [SChar '2', SDeadKey DK.invertedBreveAbove] CC.doubleInvertedBreveAbove
  , mkCombo' [SChar '2', SDeadKey DK.invertedBreveBelow] CC.doubleInvertedBreveBelow
  , mkCombo' [SChar '2', SDeadKey DK.macronAbove] CC.doubleMacronAbove
  , mkCombo' [SChar '2', SDeadKey DK.macronBelow] CC.doubleMacronBelow
  , mkCombo' [SChar '2', SDeadKey DK.dotAbove] DK.diaeresisAbove
  , mkCombo' [SChar '3', SDeadKey DK.dotAbove] CC.threeDotsAbove
  , mkCombo' [SChar '4', SDeadKey DK.dotAbove] CC.fourDotsAbove
  , mkCombo' [SChar '2', SDeadKey DK.dotBelow] DK.diaeresisBelow
  , mkCombo' [SChar '3', SDeadKey DK.dotBelow] CC.threeDotsBelow
  , mkCombo' [SChar '2', SDeadKey DK.longStroke] DK.doubledLongStroke
  , mkCombo' [SChar '2', SDeadKey DK.longSolidus] CC.doubledLongSolidus
  , mkCombo' [SChar '2', SDeadKey DK.ringBelow] CC.doubledRingBelow
  ]
  where
    mkCombo' s0@(SChar '2' NE.:| s1) r =
      [ mkCombo s0 r
      , mkCombo (SDeadKey compose NE.:| s1) r]
    mkCombo' s0 r =
      [ mkCombo s0 r ]
