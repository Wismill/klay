{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.Action.DeadKey.RomanNumerals
  ( allRomanDeadKeys
  -- * Dead keys
  , romanNumeralLower
  , romanNumeralLowerSingleCharacter
  , romanNumeralUpper
  , romanNumeralUpperSingleCharacter
  -- * Definitions
  -- ** Single character conversion
  -- *** Lower case
  , romanNumeralLowerDef
  , romanNumeralLowerSingleCharacterDef
  , romanNumeralLowerSingleConversionDef
  -- *** Upper case
  , romanNumeralUpperDef
  , romanNumeralUpperSingleCharacterDef
  , romanNumeralUpperSingleConversionDef
  -- *** All
  , romanNumeralDef
  -- ** Multiple characters conversion
  , romanNumeralLowerConversionDef
  , romanNumeralUpperConversionDef
  , romanNumeralConversionDef
  ) where

import Data.Maybe (mapMaybe)
import Data.List.NonEmpty qualified as NE
import Data.String (fromString)

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.Unicode.Roman qualified as R

allRomanDeadKeys :: [DeadKey]
allRomanDeadKeys =
  [ romanNumeralLower
  , romanNumeralUpper
  , romanNumeralLowerSingleCharacter
  , romanNumeralUpperSingleCharacter
  ]

romanNumeralLower :: DeadKey
romanNumeralLower = DeadKey
  { _dkName = "Roman numerals (lower)"
  , _dkLabel = "ⅳ"
  , _dkBaseChar = 'ⅳ'
  }

commonLowerConversions :: [DeadKeyCombo]
commonLowerConversions =
  [ char 'i' 'ⅰ'
  , char 'I' 'Ⅰ'
  , char 'v' 'ⅴ'
  , char 'V' 'Ⅴ'
  , char 'x' 'ⅹ'
  , char 'X' 'Ⅹ'
  , char 'l' 'ⅼ'
  , char 'L' 'Ⅼ'
  , char 'c' 'ⅽ'
  , char 'C' 'Ⅽ'
  , char 'd' 'ⅾ'
  , char 'D' 'Ⅾ'
  , char 'm' 'ⅿ'
  , char 'M' 'Ⅿ' ]

commonUpperConversions :: [DeadKeyCombo]
commonUpperConversions =
  [ char 'i' 'Ⅰ'
  , char 'I' 'Ⅰ'
  , char 'v' 'Ⅴ'
  , char 'V' 'Ⅴ'
  , char 'x' 'Ⅹ'
  , char 'X' 'Ⅹ'
  , char 'l' 'Ⅼ'
  , char 'L' 'Ⅼ'
  , char 'c' 'Ⅽ'
  , char 'C' 'Ⅽ'
  , char 'd' 'Ⅾ'
  , char 'D' 'Ⅾ'
  , char 'm' 'Ⅿ'
  , char 'M' 'Ⅿ' ]

romanNumeralLowerDef :: RawDeadKeyDefinition
romanNumeralLowerDef =
  ( romanNumeralLower
  , [ char '1' 'ⅰ'
    , mkCombo ['2'] (RText "ⅰⅰ")
    , mkCombo ['3'] (RText "ⅰⅰⅰ")
    , mkCombo ['4'] (RText "ⅰⅴ")
    , char '5' 'ⅴ'
    , mkCombo ['6'] (RText "ⅴⅰ")
    , mkCombo ['7'] (RText "ⅴⅰⅰ")
    , mkCombo ['8'] (RText "ⅴⅰⅰⅰ")
    , mkCombo ['9'] (RText "ⅰⅹ")
    , char '0' 'ⅹ'
    ] <> commonLowerConversions
  )

romanNumeralLowerConversionDef :: RawDeadKeyDefinition
romanNumeralLowerConversionDef =
  ( romanNumeralLower
  , mkRomanConversion R.toLowerRoman 3999
    <> commonLowerConversions
  )

romanNumeralUpper :: DeadKey
romanNumeralUpper = DeadKey
  { _dkName = "Roman numerals (upper)"
  , _dkLabel = "Ⅳ"
  , _dkBaseChar = 'Ⅳ'
  }

romanNumeralUpperDef :: RawDeadKeyDefinition
romanNumeralUpperDef =
  ( romanNumeralUpper
  , [ char '1' 'Ⅰ'
    , mkCombo ['2'] (RText "ⅠⅠ")
    , mkCombo ['3'] (RText "ⅠⅠⅠ")
    , mkCombo ['4'] (RText "ⅠⅤ")
    , char '5' 'Ⅴ'
    , mkCombo ['6'] (RText "ⅤⅠ")
    , mkCombo ['7'] (RText "ⅤⅠⅠ")
    , mkCombo ['8'] (RText "ⅤⅠⅠⅠ")
    , mkCombo ['9'] (RText "ⅠⅩ")
    , char '0' 'Ⅹ'
    ] <> commonUpperConversions
  )

romanNumeralUpperConversionDef :: RawDeadKeyDefinition
romanNumeralUpperConversionDef =
  ( romanNumeralUpper
  , mkRomanConversion R.toUpperRoman 3999
    <> commonUpperConversions
  )

romanNumeralLowerSingleCharacter :: DeadKey
romanNumeralLowerSingleCharacter = DeadKey
  { _dkName = "Roman numerals (lower, single character)"
  , _dkLabel = "ⅸ"
  , _dkBaseChar = 'ⅸ'
  }

romanNumeralLowerSingleCharacterDef :: RawDeadKeyDefinition
romanNumeralLowerSingleCharacterDef =
  ( romanNumeralLowerSingleCharacter
  , [ char '1' 'ⅰ'
    , char '2' 'ⅱ'
    , char '3' 'ⅲ'
    , char '4' 'ⅳ'
    , char '5' 'ⅴ'
    , char '6' 'ⅵ'
    , char '7' 'ⅶ'
    , char '8' 'ⅷ'
    , char '9' 'ⅸ'
    , char '0' 'ⅹ'
    , mkCombo [SDeadKey romanNumeralLowerSingleCharacter, SChar '1'] 'ⅺ'
    , mkCombo [SDeadKey romanNumeralLowerSingleCharacter, SChar 'i'] 'ⅺ'
    , mkCombo [SDeadKey romanNumeralLowerSingleCharacter, SChar 'I'] 'Ⅺ'
    , mkCombo [SDeadKey romanNumeralLowerSingleCharacter, SChar 'x', SChar 'i', SChar ' '] 'ⅺ'
    , mkCombo [SDeadKey romanNumeralLowerSingleCharacter, SChar '2'] 'ⅻ'
    , mkCombo [SDeadKey romanNumeralLowerSingleCharacter, SChar 'X', SChar 'I', SChar 'I'] 'Ⅻ'
    ] <> commonLowerConversions
  )

romanNumeralLowerSingleConversionDef :: RawDeadKeyDefinition
romanNumeralLowerSingleConversionDef =
  ( romanNumeralLowerSingleCharacter
  , mkRomanConversion R.toLowerSingleRoman 12
    <> commonLowerConversions
  )

romanNumeralUpperSingleCharacter :: DeadKey
romanNumeralUpperSingleCharacter = DeadKey
  { _dkName = "Roman numerals (upper, single character)"
  , _dkLabel = "Ⅸ"
  , _dkBaseChar = 'Ⅸ'
  }

romanNumeralUpperSingleCharacterDef :: RawDeadKeyDefinition
romanNumeralUpperSingleCharacterDef =
  ( romanNumeralUpperSingleCharacter
  , [ char '1' 'Ⅰ'
    , char '2' 'Ⅱ'
    , char '3' 'Ⅲ'
    , char '4' 'Ⅳ'
    , char '5' 'Ⅴ'
    , char '6' 'Ⅵ'
    , char '7' 'Ⅶ'
    , char '8' 'Ⅷ'
    , char '9' 'Ⅸ'
    , char '0' 'Ⅹ'
    , mkCombo [SDeadKey romanNumeralUpperSingleCharacter, SChar '1'] 'Ⅺ'
    , mkCombo [SDeadKey romanNumeralUpperSingleCharacter, SChar 'i'] 'Ⅺ'
    , mkCombo [SDeadKey romanNumeralUpperSingleCharacter, SChar 'I'] 'Ⅺ'
    , mkCombo [SDeadKey romanNumeralUpperSingleCharacter, SChar 'X', SChar 'I', SChar ' '] 'Ⅺ'
    , mkCombo [SDeadKey romanNumeralUpperSingleCharacter, SChar '2'] 'Ⅻ'
    , mkCombo [SDeadKey romanNumeralUpperSingleCharacter, SChar 'X', SChar 'I', SChar 'I'] 'Ⅻ'
    ] <> commonUpperConversions
  )

romanNumeralUpperSingleConversionDef :: RawDeadKeyDefinition
romanNumeralUpperSingleConversionDef =
  ( romanNumeralUpperSingleCharacter
  , mkRomanConversion R.toUpperSingleRoman 12
    <> commonUpperConversions
  )

-- | Roman numerals combos with single character input.
romanNumeralDef :: RawDeadKeyDefinitions
romanNumeralDef =
  [ romanNumeralLowerDef
  , romanNumeralUpperDef
  , romanNumeralLowerSingleCharacterDef
  , romanNumeralUpperSingleCharacterDef ]

-- | Roman numerals conversion with multiple characters input
romanNumeralConversionDef :: RawDeadKeyDefinitions
romanNumeralConversionDef =
  [ romanNumeralLowerConversionDef
  , romanNumeralUpperConversionDef
  , romanNumeralLowerSingleConversionDef
  , romanNumeralUpperSingleConversionDef
  ]

mkRomanConversion
  :: (Int -> Maybe String)
  -- ^ Conversion function
  -> Int
  -- ^ Max number
  -> [DeadKeyCombo]
mkRomanConversion convert m = mapMaybe mkRoman [1..m]
  where
    m' | m < 10    = 0
       | m < 100   = 10
       | m < 1000  = 100
       | otherwise = 1000
    mkRoman n =
      let result = convert n >>= mkResult
      in mkCombo (NE.fromList $ mkSymbols n) <$> result
    mkSymbols n | n < m'    = show n <> " "
                | otherwise = show n
    mkResult []  = Nothing
    mkResult [c] = Just (RChar c)
    mkResult t   = Just (RText . fromString $ t)
