{-# LANGUAGE OverloadedLists   #-}

{-|
Description : The usual “compose” dead key on Linux
-}

module Klay.Keyboard.Layout.Action.DeadKey.Compose
  ( -- * Dead key
    compose
  , composeDef
    -- * Mnemonics
  , Mnemonic(..)
  , DK.MathAlphanumericMnemonic(..)
  , mkMathMnemonic
    -- * Components
  , ligaturesDef
  , ordinalDef
  , unitsDef
    -- ** Number
  , numbersDef
  , fractionsDef
  , circledNumbersDef
  , segmentedNumbers
  , miscNumbersDef
    -- ** Punctuation
  , punctuationDef
    -- ** Math
  , mathematicsDef
    -- ** Office
  , intellectualPropertyDef
  , officeDef
    -- ** Miscellaneous
  , todoDef
    -- * Utils
  , compose2
  , compose3
  -- * Includes
  , includeDefsWithPrefix
  , includeDefs )
  where

import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Action.DeadKey.Bépo qualified as DK (bépoCurrencyDef) --, bépoLatinDef, bépoScienceDef)
import Klay.Keyboard.Layout.Action.DeadKey.Circled qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Cyrillic qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Greek qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Math qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Mirror qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.RomanNumerals qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Rotate qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.SmallCapitals qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.SuperSubScript qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Compose.Base (compose)
import Klay.Keyboard.Layout.Action.DeadKey.Compose.Diacritics qualified as CD
import Klay.Keyboard.Layout.Action.DeadKey.Compose.CombiningDiacritics qualified as CCD

-- [NOTE] See "compose" manual for information on the format
--        https://linux.die.net/man/3/xcompose
-- US: /usr/share/X11/locale/en_US.UTF-8/Compose

-- [TODO] For example, U+278C ➌ DINGBAT NEGATIVE CIRCLED SANS-SERIF DIGIT THREE might be inserted by Compose@%$3,
-- where @ indicates circled characters,[10] % indicates inverse, $ indicates sans-serif,
-- and 3 indicates the final character.

-- See: RFC 1345

-- | Default combinations for 'compose'.
composeDef :: Mnemonic -> RawDeadKeyDefinitions
composeDef m = mconcat
  [ [(compose, mconcat
      [ ligaturesDef
      , ordinalDef m
      , unitsDef m
      , numbersDef m
      , punctuationDef m
      , mathematicsDef m
      , intellectualPropertyDef m
      , officeDef
      --, todoDef m [FIXME]
      , includes m DK.defaultAlphanumericMnemonic
      ])]
  , DK.bépoCurrencyDef
  , DK.superSubscriptDef
  ]

-- | Create a 'DeadKeyCombo' prefixed with /one/ 'compose'. The complete combo will start with /two/ 'compose'.
compose2
  :: Char         -- ^ Letter ending the combo
  -> Char         -- ^ Resulting character
  -> DeadKeyCombo -- ^ Resulting combo
compose2 c1 c2 = DeadKeyCombo [SDeadKey compose, SChar c1] (RChar c2)

-- | Create a 'DeadKeyCombo' prefixed with /two/ 'compose'. The complete combo will start with /three/ 'compose'.
compose3
  :: Char         -- ^ Letter ending the combo
  -> Char         -- ^ Resulting character
  -> DeadKeyCombo -- ^ Resulting combo
compose3 c1 c2 = DeadKeyCombo [SDeadKey compose, SDeadKey compose, SChar c1] (RChar c2)

-- Mnemonics ------------------------------------------------------------------

-- | Mnemonic for 'compose' combos.
data Mnemonic = Mnemonic
  { _mnNegation :: Char    -- ^ Negation
  , _mnLineThrough :: Char -- ^ Line-through, negation
  , _mnWord :: Char        -- ^ Word

  , _mnLeft :: Char     -- ^ Left variant
  , _mnRight :: Char    -- ^ Right variant
  , _mnBottom :: Char   -- ^ Bottom variant
  , _mnTop :: Char      -- ^ Top variant
  , _mnCenter :: Char   -- ^ Center
  , _mnVertical :: Char -- ^ Vertical
  , _mnMirror :: Char   -- ^ (vertical)   Mirror variant
  , _mnHMirror :: Char  -- ^ (horizontal) Mirror variant
  , _mnRotation :: Char -- ^ Rotation

  , _mnCircled :: Char       -- ^ Positive circle
  , _mnComplementary :: Char -- ^ Negative circle
  , _mnSquared :: Char       -- ^ Square box
  } deriving (Show, Eq)

-- | Create a consistent 'DK.MathMnemonic' from 'Mnemonic' and 'DK.MathAlphanumericMnemonic'.
mkMathMnemonic :: Mnemonic -> DK.MathAlphanumericMnemonic -> DK.MathMnemonic
mkMathMnemonic Mnemonic{_mnNegation=𝐧} m = DK.MathMnemonic{DK._mNegation=𝐧, DK._mSet='{', DK._mAlphanumeric=m}

-- Numbers --------------------------------------------------------------------

{-| Combinations for ligatures.

See:

* [Orthographic ligature](https://en.wikipedia.org/wiki/Orthographic_ligature)
* [Latin script letters](https://en.wikipedia.org/wiki/List_of_Latin-script_letters)
-}
ligaturesDef :: [DeadKeyCombo]
ligaturesDef =
  [ chars ['a', 'e'] 'æ'
  , chars ['A', 'e'] 'Æ'
  , chars ['A', 'E'] 'Æ'
  , chars ['a', 'o'] 'ꜵ'
  , chars ['A', 'o'] 'Ꜵ'
  , chars ['A', 'O'] 'Ꜵ'
  , chars ['a', 'u'] 'ꜷ'
  , chars ['A', 'U'] 'Ꜷ'
  , chars ['A', 'U'] 'Ꜷ'
  , chars ['a', 'v'] 'ꜹ'
  , chars ['A', 'v'] 'Ꜹ'
  , chars ['A', 'V'] 'Ꜹ'
  -- [TODO] Ꜽ, ꜽ 🙰 ℔ Ỻ, ỻꝎ, ꝏꭢ ȸʣȹʫʪʩ
  --   Ꝭ ꝭꝷꝸ ᴑᴒ
  , chars ['c', 'k'] '\xE03A' -- 
  , chars ['c', 'h'] '\xE03B' -- 
  , chars ['c', 't'] '\xE03D' -- 
  , chars ['D', 'Z'] 'Ǳ'
  , chars ['D', 'z'] 'ǲ'
  , chars ['d', 'z'] 'ǳ' -- [TODO] check Ǆǅǆ
  , chars ['e', 't'] 'ꝫ' -- U+A76B LATIN SMALL LETTER ET
  , chars ['E', 't'] 'Ꝫ' -- U+A76A LATIN CAPITAL LETTER ET
  , chars ['f', 'b'] '\xE030' -- 
  , chars ['f', 'f'] 'ﬀ'
  , chars ['f', 'h'] '\xE036' -- 
  , chars ['f', 'i'] 'ﬁ'
  , chars ['f', 'j'] '\xE037' -- 
  , chars ['f', 'k'] '\xE038' -- 
  , chars ['f', 'l'] 'ﬂ'
  , chars ['f', 's'] 'ſ' -- [TODO]
  , chars ['F', 'i'] 'ﬃ' -- May conflict with ﬀ and ﬄ
  , chars ['F', 'l'] 'ﬄ' -- May conflict with ﬀ and ﬃ
  --, chars ['F', 'h'] '' -- ffh
  --, chars ['F', 't'] '' -- fft
  --, chars ['F', 'j'] '' -- ffj
  --, chars ['F', 'k'] '' -- ffk
  , chars ['f', 't'] 'ﬅ'
  , chars ['h', 'v'] 'ƕ' -- U0195 LATIN SMALL LETTER HV
  , chars ['H', 'v'] 'Ƕ' -- U01F6 LATIN CAPITAL LETTER HWAIR
  , chars ['i', 'j'] 'ĳ'
  , chars ['I', 'J'] 'Ĳ'
  , chars ['L', 'J'] 'Ǉ'
  , chars ['L', 'j'] 'ǈ'
  , chars ['l', 'j'] 'ǉ'
  , chars ['N', 'J'] 'Ǌ'
  , chars ['N', 'j'] 'ǋ'
  , chars ['n', 'j'] 'ǌ'
  , chars ['o', 'e'] 'œ'
  , chars ['O', 'e'] 'Œ'
  , chars ['O', 'E'] 'Œ'
  , chars ['o', 'u'] 'ȣ' -- U0223 LATIN SMALL LETTER OU
  , chars ['O', 'u'] 'Ȣ' -- U0222 LATIN CAPITAL LETTER OU
  -- , chars ['Q', 'u'] '\xE048' -- 
  , chars ['s', 'a'] 'ꞌ' -- UA78C LATIN SMALL LETTER SALTILLO
  , chars ['S', 'a'] 'Ꞌ' -- UA78B LATIN CAPITAL LETTER SALTILLO
  , chars ['s', 'h'] 'ʃ' -- U0283 LATIN SMALL LETTER ESH
  , chars ['S', 'h'] 'Ʃ' -- U01A9 LATIN CAPITAL LETTER ESH
  , chars ['s', 'z'] 'ß'
  , chars ['S', 'z'] 'ẞ'
  , chars ['S', 'Z'] 'ẞ'
  , chars ['s', 's'] 'ß'
  , chars ['S', 's'] 'ẞ'
  , chars ['S', 'S'] 'ẞ'
  , chars ['s', 't'] 'ﬆ'
  -- , chars ['t', 't'] '\xE03C' -- 
  -- , chars ['T', 'h'] '\xE049' -- 
  , chars ['t', 'h'] 'þ' -- U00FE LATIN SMALL LETTER THORN
  , chars ['T', 'h'] 'Þ' -- U00DE LATIN CAPITAL LETTER THORN
  -- [TODO] Ꜩ, ꜩᵫꭣꝠ, ꝡ
  , chars ['x', '?'] 'ɂ' -- U0242 LATIN SMALL LETTER GLOTTAL STOP
  , chars ['X', '?'] 'Ɂ' -- U0241 LATIN CAPITAL LETTER GLOTTAL STOP
  , chars ['w', 'y'] 'ƿ' -- U01BF LATIN LETTER WYNN
  , chars ['W', 'y'] 'Ƿ' -- U01F7 LATIN CAPITAL LETTER WYNN
  , chars ['y', 'o'] 'ȝ' -- U021D LATIN SMALL LETTER YOGH
  , chars ['Y', 'o'] 'Ȝ' -- U021C LATIN CAPITAL LETTER YOGH
  , chars ['y', 'r'] 'ʀ' -- U0280 LATIN LETTER SMALL CAPITAL R
  , chars ['Y', 'r'] 'Ʀ' -- U01A6 LATIN LETTER YR
  , chars ['z', 'h'] 'ʒ' -- U0292 LATIN SMALL LETTER EZH
  , chars ['Z', 'h'] 'Ʒ' -- U01B7 LATIN CAPITAL LETTER EZH
  -- [TODO] Ɣɣ
  , chars ['2', '4'] 'ꝝ' -- UA75D LATIN SMALL LETTER RUM ROTUNDA
  , mkCombo [SDeadKey compose, SChar '2', SChar '4'] 'Ꝝ' -- UA75D LATIN SMALL LETTER RUM ROTUNDA
  , chars ['9', '9'] 'ꝯ' -- U+A76F LATIN SMALL LETTER CON
  , mkCombo [SDeadKey compose, SChar '9', SChar '9'] 'Ꝯ' -- U+A76E LATIN CAPITAL LETTER CON
  , chars ['i', '.'] 'ı'
  , chars ['j', '.'] 'ȷ'
  , chars ['ı', '.'] 'i'
  , chars ['ȷ', '.'] 'j'
  ]

-- | Combinations for ordinals
ordinalDef :: Mnemonic -> [DeadKeyCombo]
ordinalDef Mnemonic{_mnTop=𝒕} =
  [ chars [𝒕, '_', 'a'] 'ª' -- Note: without '_' conflicts with superscript
  , chars ['a', 'a'] 'ª'
  , chars [𝒕, '_', 'o'] 'º' -- Note: without '_' conflicts with superscript
  --, chars ['o', 'o'] 'º' -- conflict with °
  ]

-- | Combinations for units of measurement.
unitsDef :: Mnemonic -> [DeadKeyCombo]
unitsDef Mnemonic{ _mnWord=𝐰, _mnTop=_𝒕} =
  [ -- Temperature
    chars ['0', '0'] '°'
  --, chars [𝒕, 'o'] '°' -- conflict with superscript
  , chars ['0', 'c'] '℃'
  , chars ['o', 'c'] '℃'
  , chars ['°', 'c'] '℃'
  , chars ['°', 'C'] '℃'
  , chars ['0', 'f'] '℉'
  , chars ['o', 'f'] '℉'
  , chars ['°', 'f'] '℉'
  , chars ['°', 'F'] '℉'
  , chars ['e', 'e'] '℮'
  , chars [𝐰, 'd', 'e', 'g'] '°'
   --
  , chars [𝐰, 'o', 'h', 'm'] 'Ω'
  , chars [𝐰, 'O', 'h', 'm'] '℧'
  -- Mass
  , chars ['u', 'g'] '㎍'
  , chars ['m', 'g'] '㎎'
  , chars ['k', 'g'] '㎏'
  , chars ['l', 'b'] '℔'
  -- Length
  , chars ['f', 'm'] '㎙'
  , chars ['u', 'm'] '㎛'
  , chars ['n', 'm'] '㎚'
  , chars ['m', 'm'] '㎜'
  , chars ['c', 'm'] '㎝'
  , chars ['d', 'm'] '㍷'
  , chars ['k', 'm'] '㎞'
  -- Surface
  , chars ['2', 'm', 'm'] '㎟'
  , chars ['2', 'c', 'm'] '㎠'
  , chars ['2', 'm', ' '] '㎡'
  , chars ['m', '2'] '㎡'
  , chars ['2', 'd', 'm'] '㍸'
  , chars ['2', 'k', 'm'] '㎢'
  -- Volume
  , chars ['u', 'l'] '㎕'
  , chars ['m', 'l'] '㎖'
  , chars ['l', 'l'] 'ℓ'
  , chars ['d', 'l'] '㎗'
  , chars ['k', 'l'] '㎘'
  , chars ['3', 'm', 'm'] '㎣'
  , chars ['3', 'c', 'm'] '㎤'
  , chars ['3', 'm', ' '] '㎥'
  , chars ['3', 'd', 'm'] '㍹'
  , chars ['3', 'k', 'm'] '㎦'
  -- Frequence
  -- ㎐㎑㎒㎓㎔
  --
  , chars ['d', 'B'] '㏈'
  --
  , chars ['p', 'p', 'm'] '㏙'
  , chars ['k', 'B'] '㎅'
  , chars ['M', 'B'] '㎆'
  , chars ['G', 'B'] '㎇'
  --
  , chars ['m', 'u'] 'µ' -- U+00B5 MICRO SIGN
  ]

-- Numbers --------------------------------------------------------------------

{-| Combinations for numbers

It combines:

* 'fractionsDef'
* 'circledNumbersDef'
* 'segmentedNumbers'
* 'miscNumbersDef'
-}
numbersDef :: Mnemonic -> [DeadKeyCombo]
numbersDef m = mconcat
  [ fractionsDef
  , circledNumbersDef m
  , segmentedNumbers m
  , miscNumbersDef m
  ]

-- | Combinations for fractions, such as: @⅓@.
fractionsDef :: [DeadKeyCombo]
fractionsDef =
  [ chars ['0', '3'] '↉'
  , chars ['1', '2'] '½'
  , chars ['1', '3'] '⅓'
  , chars ['1', '4'] '¼'
  , chars ['1', '5'] '⅕'
  , chars ['1', '6'] '⅙'
  , chars ['1', '7'] '⅐'
  , chars ['1', '8'] '⅛'
  , chars ['1', '9'] '⅑'
  , chars ['1', '1', '0'] '⅒'
  , chars ['1', ' '] '⅟'
  , chars ['2', '3'] '⅔'
  , chars ['2', '5'] '⅖'
  , chars ['3', '4'] '¾'
  , chars ['3', '5'] '⅗'
  , chars ['3', '8'] '⅜'
  , chars ['4', '5'] '⅘'
  , chars ['5', '6'] '⅚'
  , chars ['5', '8'] '⅝'
  , chars ['7', '8'] '⅞'
  ]

-- | Combinations for circled numbers, such as: @⑩@.
circledNumbersDef :: Mnemonic -> [DeadKeyCombo]
circledNumbersDef Mnemonic{_mnCircled=𝒐, _mnComplementary=𝕟}
  =  DK.mkCircledNumberDef [SChar 𝒐] (SDeadKey compose)
  <> DK.mkNegativeCircledNumberDef [SChar 𝒐, SChar 𝕟] (SDeadKey compose)

-- | Combinations for segmented numbers, such as: @🯲@.
segmentedNumbers :: Mnemonic -> [DeadKeyCombo]
segmentedNumbers Mnemonic{_mnSquared=𝕤} =
  [ chars [𝕤, '0'] '\x1fbf0'
  , chars [𝕤, '1'] '\x1fbf1'
  , chars [𝕤, '2'] '\x1fbf2'
  , chars [𝕤, '3'] '\x1fbf3'
  , chars [𝕤, '4'] '\x1fbf4'
  , chars [𝕤, '5'] '\x1fbf5'
  , chars [𝕤, '6'] '\x1fbf6'
  , chars [𝕤, '7'] '\x1fbf7'
  , chars [𝕤, '8'] '\x1fbf8'
  , chars [𝕤, '9'] '\x1fbf9'
  ]

-- | Miscellaneous combinations about numbers.
miscNumbersDef :: Mnemonic -> [DeadKeyCombo]
miscNumbersDef Mnemonic{_mnCircled=_𝒐, _mnComplementary=_𝕟} =
  [ chars ['n', '°'] '№'
  , chars ['n', 'o'] '№'
  , chars ['N', 'o'] '№'
  , chars ['1', '0'] '⏨'
  , chars ['1', 'e'] '⏨'
  ]

-- Punctuation ----------------------------------------------------------------

-- | Combinations for punctuation
punctuationDef :: Mnemonic -> [DeadKeyCombo]
punctuationDef Mnemonic
  { _mnNegation = 𝐧
  --, _mnLineThrough = 𝐬
  --, _mnWord = 𝐰
  , _mnLeft = 𝒍
  , _mnRight = 𝒓
  , _mnBottom = 𝒃
  , _mnTop = 𝒕
  , _mnCenter = 𝒄
  , _mnVertical = 𝒗
  , _mnMirror = 𝒎
  , _mnHMirror = 𝕞
  --, _mnRotation = 𝕣
  , _mnCircled = 𝒐
  , _mnComplementary = 𝕟
  , _mnSquared = 𝕤
  } =
  [ -- Punctuation: dashes
    chars ['-', '-'] '\x2013'                            -- – EN dash
  , chars ['-', ' '] '\x00AD'                            -- ]­[ SOFT HYPHEN [NOTE] Latex: \-
  , chars ['-', '0'] '\x00AD', chars ['0', '-'] '\x00AD' -- ]­[ SOFT HYPHEN
  , chars ['-', '1'] '\x2010', chars ['1', '-'] '\x2010' -- ‐ True hyphen
  , chars ['-', '~'] '\x2011'                            -- ‑ NON-BREAKING HYPHEN
  , chars ['-', '#'] '\x2012'                            -- ‒ Figure dash
  , chars ['-', 'n'] '\x2013', compose2 '-' '\x2013'     -- – EN dash
  , chars ['-', 'm'] '\x2014', compose3 '-' '\x2014'     -- — EM dash
  , chars ['-', 'b'] '\x2015'                            -- ― Horizontal bar
  , chars ['-', '2'] '\x2E3A', chars ['2', '-'] '\x2E3A' -- ⸺ TWO-EM DASH
  , chars ['-', '3'] '\x2E3B', chars ['3', '-'] '\x2E3B' -- ⸻ THREE-EM DASH
  , chars ['-', 'l'] '\x2500'                            -- ─ BOX DRAWINGS LIGHT HORIZONTAL
  , chars ['-', '%'] '\x2212'                            -- − Minus
  , chars ['-', '$'] '\x2052'                            -- ⁒ U2052 COMMERCIAL MINUS SIGN

  , chars ['%', '%'] '‰'
  , chars ['%', '-'] '⁒' -- U2052 COMMERCIAL MINUS SIGN
  , chars ['0', '/', '0'] '⁒' -- U2052 COMMERCIAL MINUS SIGN
  , chars ['2', '%'] '‰', compose2 '%' '‰'
  , chars ['3', '%'] '‱', compose3 '%' '‱'

  , chars ['!', '!'] '‼'
  , chars ['!', '?'] '⁉'
  , chars ['𝕞', '!'] '¡'
  , chars ['2', '!'] '‼', compose2 '!' '‼'
  , chars ['3', '!'] '❢', compose3 '!' '❢'

  , chars ['?', '?'] '⁇'
  , chars ['?', '!'] '⁈'
  , chars [𝕞, '?'] '¿'
  , chars [𝒎, '?'] '⸮'
  , chars [𝒍, '?'] '⸮'
  , chars ['2', '?'] '⁇', compose2 '?' '⁇'

    -- Punctuation: quotes ‘’‚‛“”„‟‹›«»
    -- • ‘’, -> ‚‛ “”„‟
    -- • '   -> ‹› ′″
    -- • "   -> «» ‴⁗
  , chars ['\'', '\''] '’'
  , chars ['\'', '"'] '‘'
  , chars ['\'', ','] '‚'
  , chars [𝒓, '\''] '’'
  , chars [𝒍, '\''] '‘'
  , chars [𝒕, '\''] '‛'
  , chars [𝒃, '\''] '‚'
  , chars ['\'', '<'] '‹'
  , chars ['\'', '>'] '›'
  , chars ['0', '\''] '°'
  , chars ['1', '\''] '′'
  , chars ['2', '\''] '″'
  , chars ['3', '\''] '‴'
  , chars ['4', '\''] '⁗'
  , chars ['\'', '0'] '°'
  , chars ['\'', '1'] '‵'
  , chars ['\'', '2'] '‶'
  , chars ['\'', '3'] '‷'
  , chars ['\'', '['] '⸢'
  , chars ['\'', '_', '['] '⸤'
  , chars ['\'', ']'] '⸣'
  , chars ['\'', '_', ']'] '⸥'

  , chars ['"', '"'] '”'
  , chars ['"', '\''] '“'
  , chars ['"', ','] '„'
  , chars [𝒓, '"'] '”'
  , chars [𝒍, '"'] '“'
  , chars [𝒕, '"'] '‟'
  , chars [𝒃, '"'] '„'
  , chars ['"', '<'] '«'
  , chars ['"', '>'] '»'
  --, chars ['"', '['] '⸤'
  --, chars ['"', ']'] '⸥'

  , chars ['’', '’'] '”', compose2 '’' '”', chars ['2', '’'] '”'
  , chars ['’', '<'] '‹'
  , chars ['’', '>'] '›'
  , chars ['’', ','] '‚'
  , chars ['’', '\''] '‛', chars [𝒎, '’'] '‛'
  , chars ['’', '"'] '‟'
  --, chars ['0', '’'] '°'
  --, chars ['1', '’'] '′'
  --, chars ['2', '’'] '″'
  --, chars ['3', '’'] '‴'
  --, chars ['4', '’'] '⁗'
  , chars ['’', '['] '⸢'
  , chars ['’', ']'] '⸣'
  , chars ['’', '_', '['] '⸤'
  , chars ['’', '_', ']'] '⸥'

  , chars ['‘', '‘'] '“', compose2 '‘' '“', chars ['2', '‘'] '“'
  , chars ['‘', '\''] '‛', chars [𝕞, '‘'] '‛'
  , chars ['‘', '"'] '‟'
  , chars ['‘', '['] '⸤'
  , chars ['‘', ']'] '⸥'

  , chars [',', ','] '„', compose2 ',' '„'
  , chars [',', '['] '⸤'
  , chars [',', ']'] '⸥'
  , chars [𝒄, ','] '⸴' -- ⸴ U+2E34 RAISED COMMA

  , chars [';', ';'] '⁏'

    -- Punctuation: brackets
  , chars ['(', '('] '⦅'
  , chars [')', ')'] '⦆'
  , chars ['2', '('] '⸨', compose2 '(' '⸨'
  , chars ['2', ')'] '⸩', compose2 ')' '⸩'
  , chars [𝒗, '('] '︵' -- [TODO] ‿⁔⁀⁐
  , chars [𝒗, ')'] '︶'
  , chars ['(', 𝒕] '⏜'
  , chars [')', 𝒕] '⏝'
  , chars ['(', '<'] '〈'
  , chars ['(', '>'] '〉'
  , chars [')', '<'] '〉'
  , chars ['(', 𝒗, '<'] '︿'
  , chars ['(', 𝒗, '>'] '﹀'
  , chars [')', 𝒗, '<'] '﹀'
  , chars ['(', '2', '<'] '⟪'
  , chars ['(', '2', '>'] '⟫'
  , chars [')', '2', '<'] '⟫'
  , chars ['(', 𝒗, '2', '<'] '︽'
  , chars ['(', 𝒗, '2', '>'] '︾'
  , chars [')', 𝒗, '2', '<'] '︾'
  , chars ['(', '|'] '⦇'
  , chars [')', '|'] '⦈'
  , chars [')', 's'] '⟅'
  , chars ['(', 's'] '⟆'
  , chars ['(', 'w'] '⧘'
  , chars [')', 'w'] '⧙'
  , chars ['(', '2', 'w'] '⧚'
  , chars [')', '2', 'w'] '⧛'

  , chars ['[', '['] '⟦', chars ['[', '|'] '⟦'
  , chars [']', ']'] '⟧', chars [']', '|'] '⟧'
  , chars [𝒗, '['] '︹'
  , chars [𝒗, ']'] '︺'
  , chars ['[', 𝒗] '⎴'
  , chars [']', 𝒗] '⎵'
  , chars [𝒕, '['] '⸢'
  , chars [𝒃, '['] '⸤'
  , chars [𝒕, ']'] '⸣'
  , chars [𝒃, ']'] '⸥'

  , chars [𝒗, '{'] '︷'
  , chars [𝒗, '}'] '︸'
  , chars ['{', 𝒗] '⏞'
  , chars ['}', 𝒗] '⏟'
  , chars ['{', '{'] '⦃', chars ['{', '|'] '⦃'
  , chars ['}', '}'] '⦄', chars ['}', '|'] '⦄'

  , chars [𝒗, '⟨'] '︿' -- [TODO] ︽ ︾
  , chars [𝒗, '⟩'] '﹀'
  , chars ['⟨', '⟨'] '⟪', compose2 '⟨' '⟪', chars ['2', '⟨'] '⟪'
  , chars ['⟩', '⟩'] '⟫', compose2 '⟩' '⟫', chars ['2', '⟩'] '⟫'

    -- Punctuation: bullets
  , chars ['*', '.'] '•'
  , chars ['*', 𝕟, '.'] '◦'
  , chars ['*', '>'] '‣'
  , chars ['*', ']'] '▪'
  , chars ['*', 𝕤] '▪'
  , chars ['*', '-'] '⁃'
  , chars ['*', 𝒐] '◉'

    -- Punctuation: spaces
  , chars [' ', 't'] '\t'     -- CHARACTER TABULATION
  , chars [' ', 'r'] '\r'     -- CARRIAGE RETURN
  --, chars [' ', 'n'] '\n'   -- LINE FEED [TODO] conflict with EN SPACE
  , chars [' ', '~'] '\x00A0' -- NO-BREAK SPACE
  , chars [' ', '-'] '\x00AD' -- SOFT HYPHEN [NOTE] Latex: \-
  , chars [' ', '|'] '\x034F' -- COMBINING GRAPHEME JOINER [NOTE] Name is misleading
  , chars [' ', 'n'] '\x2002' -- EN SPACE
  , chars [' ', 'm'] '\x2003' -- EM SPACE
  , chars [' ', '3'] '\x2004' -- THREE-PER-EM SPACE
  , chars [' ', '4'] '\x2005' -- FOUR-PER-EM SPACE
  , chars [' ', '6'] '\x2006' -- SIX-PER-EM SPACE
  , chars [' ', '#'] '\x2007' -- FIGURE SPACE
  , chars [' ', ':'] '\x2008' -- PUNCTUATION SPACE
  , chars [' ', '‧'] '·'      -- U+2027 HYPHENATION POINT
  , chars [' ', '2'] '\x2009' -- THIN SPACE
  , chars [' ', '1'] '\x200A' -- HAIR SPACE
  , chars [' ', '0'] '\x200B' -- ZERO WIDTH SPACE
  , chars [' ', 'j'] '\x200C' -- ZERO WIDTH NON-JOINER [TODO] consider using hyphen or comma prefix
  , chars [' ', 'J'] '\x200D' -- ZERO WIDTH JOINER
  , chars [' ', 'L'] '\x2028' -- Line separator
  , chars [' ', 'P'] '\x2029' -- Paragraph separator
  , chars [' ', ','] '\x202F' -- NARROW NO-BREAK SPACE [NOTE] Latex uses \,
  , chars [' ', '%'] '\x205F' -- MEDIUM MATHEMATICAL SPACE
  , chars [' ', 'w'] '\x2060' -- Word joiner
  , chars [' ', 'f'] '\x2061' -- ⁡FUNCTION APPLICATION
  , chars [' ', '*'] '\x2062' -- INVISIBLE TIMES
  , chars [' ', ';'] '\x2063' -- INVISIBLE SEPARATOR (invisible comma) [NOTE] "," conflicts with NARROW NO-BREAK SPACE
  , chars [' ', '+'] '\x2064' -- INVISIBLE PLUS

  , chars [' ', ' '] '␣'      -- U+2423 Open box
  , chars [' ', '\x00A0'] '⍽' -- U+237D Shouldered open box
  , chars [' ', '\x202F'] '⍽' -- U+237D Shouldered open box -- [TODO]
  , chars [' ', 'h', 't'] '␉' -- U+2409 Symbol for horizontal tabulation
  , chars [' ', 'l', 'f'] '␊' -- U+24A0 Symbol for line feed
  , chars [' ', 's', 'p'] '␠' -- U+2420 Symbol for space
  , chars [' ', 'c', 'r'] '␍' -- U+240D Symbol for carriage return
  , chars [' ', 'b']      '␢' -- U+2422 BLANK SYMBOL

  , chars [' ', 'F']    '❦'  -- U+2766 FLORAL HEART
  , chars [' ', 𝒍, 'F'] '☙'  -- U+2767 ROTATED FLORAL HEART BULLET
  , chars [' ', 𝒓, 'F'] '❧'  -- U+2619 REVERSED ROTATED FLORAL HEART BULLET

  , chars [' ', '?'] '�'      -- U+FFFD REPLACEMENT CHARACTER
  , chars [' ', 'o'] '◌'      -- U+25CC DOTTED CIRCLE

  -- Dots
  --, chars ['.', '.'] '‥' [FIXME] Conflict with diacritic dot below
  , chars ['.', '-'] '\x2027' -- U+2027 HYPHENATION POINT ‧ (hyphen)
  , chars ['.', '*'] '\x2219' -- U+2219 BULLET OPERATOR ∙ (multiplication)
  , chars ['.', 'x'] '\x22C5' -- U+22C5 DOT OPERATOR ⋅ (multiplication)
  , chars ['.', ','] '\x00b7' -- U+00B7 MIDDLE DOT · (used as comma in some contexts)
  , chars ['.', ' '] '\x2E31' -- U+2E31 WORD SEPARATOR MIDDLE DOT ⸱ [FIXME] Conflict
  , chars ['.', 'o'] '⸰' -- ⸰ U+2E30 RING POINT
  , chars ['o', '.'] '◌' -- U+25CC DOTTED CIRCLE
  , chars [𝒄, '.'] '\x00b7'
  , chars ['1', '.'] '\x2024' -- U+2024 ONE DOT LEADER ․
  , chars ['2', '.'] '‥', compose2 '.' '‥' -- U+2025 TWO DOT LEADER
  , chars ['3', '.'] '…', compose3 '.' '…' -- U+2026 HORIZONTAL ELLIPSIS/THREE DOT LEADER, chars ['.', '3', 𝒃] '…'
  , chars ['.', '3', 𝒃] '…'
  , chars ['.', '3', 𝒄] '⋯'
  , chars ['.', '3', 𝒗] '⋮'
  , chars ['.', '3', 𝒍] '⋱'
  , chars ['.', '3', 𝒓] '⋰'
  , chars ['.', '|'] '⁞' -- ⁞ U+205E VERTICAL FOUR DOTS
  , chars ['.', '#'] '∎' -- U+220E END OF PROOF

  , chars ['·', '·'] '⋯'

  , chars [𝒄, '…'] '⋯'
  , chars [𝒗, '…'] '⋮'
  , chars [𝒍, '…'] '⋱'
  , chars [𝒓, '…'] '⋰'

    -- Arrows
  , chars ['-', '>'] '→'
  , chars ['-', '<'] '←', chars ['<', '-'] '←'
  , chars ['-', '^'] '↑'
  --, chars ['|', '^'] '↑'
  , chars ['-', '_'] '↓'
  --, chars ['|', '_'] '↓'
  , chars ['<', '\\'] '↖'
  , chars ['>', '\\'] '↘'
  , chars ['<', '/'] '↙'
  , chars ['>', '/'] '↗'
  --, chars ['<', '-', '>'] '↔' [FIXME]
  , chars ['-', '*', '<', '>'] '↔'
  , chars ['-', '*', '^', '_'] '↕'
  , chars ['^', '|', '_'] '↕'
  , chars [𝐧, '-', '<'] '↚'
  , chars [𝐧, '-', '>'] '↛'
  , chars [𝐧, '-', '*', '<', '>'] '↮'

  , chars ['=', '>'] '⇒'
  , chars ['=', '<'] '⇐'
  , chars ['=', '^'] '⇑'
  , chars ['=', '_'] '⇓'
  , chars ['=', '*', '<', '>'] '⇔'
  , chars ['=', '*', '^', '_'] '⇕'
  , chars ['=', 𝐧, '<'] '⇍'
  , chars ['=', 𝐧, '>'] '⇏'
  , chars ['=', 𝐧, '*', '<', '>'] '⇎'

  , chars ['w', '>'] '⇝'
  , chars ['w', '<'] '⇜'
  , chars ['w', '*', '<', '>'] '↭'

  , chars ['.', '>'] '⇢'
  , chars ['.', '<'] '⇠'
  , chars ['.', '^'] '⇡'
  , chars ['.', '_'] '⇣'

    -- Document division, links
  , chars ['#', 'p'] '¶'
  , chars ['#', 'P'] '⁋'
  , chars ['#', 's'] '§'
  , chars ['#', 'l'] '🔗'
  , chars ['#', 'a'] '⚓'
  , chars ['#', '#'] '♯'
  -- , chars ['#', '*'] '⁕' -- U+2055 FLOWER PUNCTUATION MARK [TODO] conflict with ⧆
  , chars ['#', '~'] '⌘'
  , chars ['8', '#'] '⌘' -- [NOTE] Conflict with segmented numbers # + 8

    -- Fractions
  , chars ['/', ':'] '⁄' -- U+2044 FRACTION SLASH
  , chars [':', '/'] '∶' -- U+2236 RATIO

    -- Control
  , chars ['\\', '0'] '\NUL'
  , chars ['\\', 'b'] '\b'
  , chars ['\\', 'n'] '\n'
  , chars ['\\', 'r'] '\r'
  , chars ['\\', 't'] '\t'
  , chars ['\\', 'v'] '\v'

    -- Miscellaneous
  , chars [':', ':'] '∷'
  , chars ['2', ':'] '∷', compose2 ':' '∷'
  , chars [𝒎, ';'] '⁏'

  , chars ['*', '*'] '⁑'
  , chars ['2', '*'] '⁑', compose2 '*' '⁑'
  , chars ['3', '*'] '⁂', compose3 '*' '⁂'
  , chars [𝒃, '*'] '⁎'

  , chars ['<', '>'] '◊'
  , chars [𝕟, '<', '>'] '⧫'
  , chars ['&', '&'] '⅋', chars ['𝕣', '&'] '⅋'

  , chars [𝒗, '~'] 'ⸯ'

  , chars [𝒃, '^'] '‸'

  , chars ['v', 'v'] '✓'
  , chars ['V', 'V'] '✔'
  , chars ['X', 'x'] '✗'
  , chars ['X', 'X'] '✘'
  , chars [𝕤, ' '] '☐'
  , chars [𝕤, 'v'] '☑'
  , chars [𝕤, 'X'] '☒'
  -- , chars ['[', ' ', ']'] '☐'
  -- , chars ['[', 'v', ']'] '☑'
  -- , chars ['[', 'x', ']'] '☒'

  , chars ['7', '7'] '⁊' -- U+204A TIRONIAN SIGN ET
  ]

-- | Combinations for math. See also: "Klay.Keyboard.Layout.Action.DeadKey.Math.Alphanumeric".
mathematicsDef :: Mnemonic -> [DeadKeyCombo]
mathematicsDef (Mnemonic 𝐧 𝐬 _𝐰 𝒍 𝒓 𝒃 𝒕 _𝒄 _𝒗 _𝒎 _𝕞 _𝕣 𝒐 _𝕟 𝕤) =
    -- [FIXME] '∝'
  [ chars ['8', '8'] '∞'
  , chars ['q', 'e', 'd'] '∎'

  -- Math: equalities
  , chars [':', '='] '≔'
  , chars ['=', '='] '≡'
  , chars ['=', '?'] '≟'
  --, chars ['=', '<'] '⪕' [FIXME] conflict with arrows
  --, chars ['=', '>'] '⪖'
  , chars ['=', '|', '|'] '⋕'
  , chars ['=', '/', '/'] '⧣'
  , chars ['=', 'd', 'e', 'f'] '≝'
  , chars [𝐬, '='] '≠'
  , chars [𝐧, '='] '≠'

  , chars ['~', '~'] '≈', chars ['2', '~'] '≈'
  , chars ['3', '~'] '≋'
  , chars ['~', '-'] '≃'
  , chars [𝐧, '~', '-'] '≄'
  --, chars ['-', '~'] '≂' [TODO] conflict with NON-BREAKING HYPHEN
  , chars ['~', '='] '≅' -- [TODO] ≌
  , chars ['=', '~'] '⩳'
  , chars ['~', '≠'] '≆'

  , chars ['≈', '='] '⩰'
  -- [TODO] ∽∼≁≀≇

  -- Math: orders
  , chars ['2', '>'] '≫', compose2 '>' '≫', chars ['>', '>'] '≫'
  , chars ['3', '>'] '⋙', compose3 '>' '⋙'
  , chars ['2', '<'] '≪', compose2 '<' '≪', chars ['<', '<'] '≪'
  , chars ['3', '<'] '⋘', compose3 '<' '⋘'
  -- , chars ['<', '>'] '≶' [FIXME] conflict
  , chars [𝐧, '<', '>'] '≸'
  , chars ['>', '<'] '≷'
  , chars [𝐧, '>', '<'] '≹'
  , chars ['<', '_'] '≤'
  , chars ['>', '_'] '≥'
  , chars ['<', '='] '⩽'
  , chars ['>', '='] '⩾'
  , chars ['<', '~'] '≲'
  , chars ['>', '~'] '≳'
  , chars ['~', '<'] '⪝'
  , chars ['~', '>'] '⪞'

  , chars ['{', 'c'] '⊂' -- [TODO] ⊄⊅⊆⊇⊈⊉⊊⊋
  , chars ['}', 'c'] '⊃'
  , chars ['{', 'i'] '∩'
  , chars ['{', 'u'] '∪'
  , chars ['{', 'n', 'i'] '⋂'
  , chars ['{', 'n', 'u'] '⋃'

  , chars ['[', 'c'] '⊏' -- [TODO] ⊑⊒⋢⋣⋤⋥
  , chars [']', 'c'] '⊐'
  -- , chars ['[', 'i'] '⊓' -- [FIXME] conflict with diacritics
  -- , chars ['[', 'u'] '⊔'
  , chars ['[', 'n', 'i'] '⨅'
  , chars ['[', 'n', 'u'] '⨆'

  , chars [':', '-'] '÷'
  , chars ['-', ':'] '÷'
  , chars ['+', '-'] '±'
  , chars ['-', '+'] '∓'

  -- Math: floor, ceiling
  , chars ['[', '0'] '⌊'
  , chars [']', '0'] '⌋'
  , chars ['0', '['] '⌈'
  , chars ['0', ']'] '⌉'

  , chars ['E', 'E'] '∃'
  , chars [𝐧, 'E', 'E'] '∄'
  , chars ['A', 'A'] '∀'

  , chars ['/', '\\'] '∧'
  , chars ['\\', '/'] '∨'
  , chars ['-', ','] '¬'

  , chars ['T', 𝒃] '⊥'
  , chars ['T', 𝒕] '⊤' -- [TODO] better sequence?
  , chars ['|', '-'] '⊢'
  , chars ['-', '|'] '⊣'

  , chars ['2', '/'] '⫽', compose2 '/' '⫽', chars ['/', '/'] '⫽'
  , chars ['3', '/'] '⫻', compose3 '/' '⫻'
  , chars ['1', '|'] '∣' -- U+2223 Divides
  , chars ['2', '|'] '‖', compose2 '|' '‖', chars ['|', '|'] '‖'
  , chars ['3', '|'] '⦀', compose3 '|' '⦀'
  , chars [𝐬, '|'] '∤'
  , chars [𝐬, '2', '|'] '∦'

  , chars ['1', 's'] '∫'
  , chars ['2', 's'] '∬'
  , chars ['3', 's'] '∭'
  , chars ['4', 's'] '⨌'
  , chars ['2', '∫'] '∬'
  , chars ['3', '∫'] '∭'
  , chars ['4', '∫'] '⨌'
  , chars ['1', 𝒐, 's'] '∮'
  , chars ['2', 𝒐, 's'] '∯'
  , chars ['3', 𝒐, 's'] '∰'
  , chars [𝒐, '∫'] '∮'
  , chars [𝒐, '∬'] '∯'
  , chars [𝒐, '∭'] '∰'
  , chars [𝒍, '∫'] '∱'
  , chars [𝒓, '∫'] '⨑'
  --, chars [𝒓, '∮'] '∲'
  --, chars [𝒍, '∮'] '∳'

  , chars ['o', '/'] '⌀' -- Diameter [NOTE] Conflict with ø
  , chars ['/', '0'] '∅' -- Empty set

  -- Math: operators
  , chars [𝒐, '+'] '⊕'
  , chars [𝕤, '+'] '⊞'
  , chars ['n', '+'] '∑'
  , chars [𝒐, 'n', '+'] '⨁'

  , chars [𝒐, '-'] '⊖'
  , chars [𝕤, '-'] '⊟'

  , chars ['1', '/'] '∕' -- U+2215 DIVISION SLASH
  , chars [𝒐, '/'] '⊘'
  , chars [𝕤, '/'] '⧄'
  , chars [𝒐, '\\'] '⦸'
  , chars [𝕤, '\\'] '⧅'

  , chars ['x', 'x'] '×'
  , chars [𝒐, 'x'] '⊗'
  , chars [𝒐, '×'] '⊗'
  , chars [𝕤, 'x'] '⊠'
  , chars ['n', 'x'] '⨉'
  , chars [𝒐, 'n', 'x'] '⨂'
  , chars [𝒐, 'n', '×'] '⨂'

  , chars ['1', 'x'] '⋆' -- U+22C6 STAR OPERATOR

  , chars ['1', '*'] '∗' -- U+2217 ASTERISK OPERATOR
  , chars [𝒐, '*'] '⊛'
  , chars [𝕤, '*'] '⧆'
  , chars ['n', '*'] '∏'
  , chars ['N', '*'] '∐'


  , chars ['o', 'o'] '∘'
  , chars [𝒐, 'o'] '⊚'
  , chars [𝒐, '∘'] '⊚'
  , chars [𝕤, 'o'] '⧇'
  , chars [𝕤, '∘'] '⧇'

  , chars [𝒐, '.'] '⊙'
  , chars [𝕤, '.'] '⊡'
  , chars [𝒐, 'n', '.'] '⨀'

  , chars [𝒐, '='] '⊜'

  , chars [𝒐, '∙'] '⦿'

  , chars [𝒐, '|'] '⦶'
  , chars [𝒐, '<'] '⧀'
  , chars [𝒐, '>'] '⧁'

  , chars ['2', 'v'] '√'
  , chars ['3', 'v'] '∛'
  , chars ['4', 'v'] '∜'


  , chars [')', '('] '≬'
  ]

-- | Combinations about intellectual property
intellectualPropertyDef :: Mnemonic -> [DeadKeyCombo]
intellectualPropertyDef Mnemonic {_mnCircled=𝒐} =
  [ chars [𝒐, 'C'] '🄯' -- U+1F12F COPYLEFT SYMBO
  , chars [𝒐, 'c'] '©'
  , chars [𝒐, 'p'] '℗'
  , chars [𝒐, 'r'] '®'
  , chars ['s', 'm'] '℠'
  , chars ['t', 'm'] '™'
  ]

-- | Office
officeDef :: [DeadKeyCombo]
officeDef =
  [ chars ['t', 'e', 'l'] '℡'
  , chars ['f', 'a', 'x'] '℻'
  , chars ['a', 'c'] '℀'
  , chars ['a', 's'] '℁'
  , chars ['a', 's'] '℁'
  , chars ['c', 'o'] '℅'
  , chars ['c', 'u'] '℆'
  , chars ['A', 'S'] '⅍'
  , chars ['p', 'e', 'r'] '⅌'
  ]

todoDef :: Mnemonic -> [DeadKeyCombo]
todoDef _ =
  [ text ['[', 't'] "[TODO]"
  , text ['[', 'n'] "[NOTE]"
  ]

-- | Combinations obtained by including other dead keys
includes :: Mnemonic -> DK.MathAlphanumericMnemonic -> [DeadKeyCombo]
includes m@Mnemonic
  { _mnNegation = _𝐧
  , _mnLineThrough = _𝐬
  , _mnWord = _𝐰
  , _mnLeft = _𝒍
  , _mnRight = _𝒓
  , _mnBottom = 𝒃
  , _mnTop = 𝒕
  , _mnCenter = _𝒄
  , _mnVertical = _𝒗
  , _mnMirror = 𝒎
  , _mnHMirror = _𝕞
  , _mnRotation = 𝕣
  , _mnCircled = 𝒐
  , _mnComplementary = 𝕟
  , _mnSquared = _𝕤
  } mMath = mconcat
  [ includeDefsWithPrefix ['$'] DK.bépoCurrencyDef
  , includeDefsWithPrefix ['€'] DK.bépoCurrencyDef
  , includeDefsWithPrefix [𝒕] [DK.superscriptDef]
  , includeDefsWithPrefix [𝒃] [DK.subscriptDef]
  , includeDefsWith (<> [toSymbol 𝒃]) [DK.smallCapitalsDef]
  , includeDefsWithPrefix ['C'] DK.cyrillicDef -- [TODO] lower case conflicts with ligatures
  , includeDefsWithPrefix ['g'] DK.greekDef
  -- , includeDefsWithPrefix ['l'] DK.bépoLatinDef [TODO] create conflicts with ligatures
  , includeDefsWithPrefix [𝒎] DK.mirrorDef
  , DK.includeMathSymbols ['M'] (mkMathMnemonic m mMath)
  --, includeDefsWithPrefix ['s'] DK.bépoScienceDef
  , includeDefsWithPrefix ['i'] [DK.romanNumeralLowerDef]
  , includeDefsWithPrefix ['I'] [DK.romanNumeralUpperDef]
  , includeDefsWithPrefix [𝕣] DK.rotateDef
  , DK.mkCircledDef [SDeadKey compose, SChar 𝒐] (SDeadKey compose)
  , DK.mkNegativeCircledNumberDef [SDeadKey compose, SChar 𝒐, SChar 𝕟] (SDeadKey compose)
  , CD.defaultDiacriticsDef
  , CCD.diacriticsDef
  , includeDefs DK.diacriticsExtraDef
  ]
