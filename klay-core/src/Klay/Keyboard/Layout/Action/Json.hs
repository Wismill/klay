module Klay.Keyboard.Layout.Action.Json
  ( charToKeysym
  , charFromKeysym
  , serializeDeadKey
  , escapeText
  , textP
  , quotedTextP
  , prefixedTextP
  , charP
  , prefixedDeadKeyP
  , encodedDeadKeyP
  , predefinedEncodedDeadKeyP
  , namedDeadKeyP
  , keyP
  ) where

import Data.Void (Void)
import Data.Maybe (fromMaybe)
import Data.List (sortBy)
import Data.Char (ord, isPrint, isSpace, isMark, isLetter)
import Data.Text qualified as T
import Data.Map.Strict qualified as Map
import Data.Function (on)
import Data.Functor ((<&>), ($>))
import Data.Bifunctor (Bifunctor(..))
import Control.Arrow ((&&&))

import TextShow (showt)
import Text.Megaparsec
import Text.Megaparsec.Char

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.DeadKey
  (DeadKey(..), Symbol(..), DeadKeysEncoding, mkCustomDeadKey)
import Klay.Keyboard.Layout.Action.DeadKey.Predefined
import Klay.Import.Linux.XCompose (symbolP)
import Klay.Import.Linux.Lookup.Keysyms (mSymbolToKeysym)
import Klay.Utils.Unicode (showHeX, showUnicode)

type Parser = Parsec Void T.Text

-- | Convert spaces, marks and non-printable characters to keysyms
charToKeysym :: Char -> T.Text
charToKeysym c
  | isSpecialChar = keysym
  | otherwise     = T.singleton c
  where isSpecialChar = isSpace c || not (isPrint c) || isMark c
        keysym = fromMaybe unicode (mSymbolToKeysym (SChar c))
        unicode = T.pack . ('U':) . showHeX 4 . ord $ c

charFromKeysym :: T.Text -> Either String Char
charFromKeysym
  = first errorBundlePretty
  . parse (charP <* eof) mempty

escapeText :: T.Text -> T.Text
escapeText = T.replace "\"" "\"\""

textP :: Parsec Void T.Text T.Text
textP = try quotedTextP <|> prefixedTextP

prefixedTextP :: Parsec Void T.Text T.Text
prefixedTextP = string "text:" *> takeWhile1P Nothing (not . isSpace) >>= \case
  "" -> fail "empty text"
  t  -> pure t

quotedTextP :: Parsec Void T.Text T.Text
quotedTextP = mconcat <$> between (char '"') (char '"') (some textP')
  where
    textP' = pAnyButQuote <|> pEscapedQuote
    pAnyButQuote = takeWhile1P Nothing (/= '"')
    pEscapedQuote = string "\"\"" $> T.singleton '"'

charP :: Parser Char
charP = try charP' <|> anySingle
  where charP' = symbolP >>= \case
          SDeadKey _ -> fail "expected a char keysym, but got a dead key"
          SChar    c -> pure c

serializeDeadKey :: DeadKey -> T.Text
serializeDeadKey dk = ("dk:" <>)
              . fromMaybe (charToKeysym . _dkBaseChar $ dk)
              $ Map.lookup dk deadKeysNames

prefixedDeadKeyP :: Parser DeadKey
prefixedDeadKeyP
  =  string "dk:" *> (try namedDeadKeyP <|> encodedDeadKeyP)
  <?> "dead key"

encodedDeadKeyP :: Parser DeadKey
encodedDeadKeyP = charP <&> mkCustomDeadKey

predefinedEncodedDeadKeyP :: DeadKeysEncoding -> Parser DeadKey
predefinedEncodedDeadKeyP dke = anySingle >>= \bc -> case Map.lookup bc dke of
  Nothing -> fail $ "unknown dead key with base char: " <> showUnicode bc
  Just dk -> pure dk

namedDeadKeyP :: Parser DeadKey
namedDeadKeyP
  =  takeWhile1P (Just "dead key name") (\c -> isLetter c || c == '_')
  <* notFollowedBy digitChar -- detect if unicode keysym UXXXX
  >>= (\t -> case Map.lookup t namedDeadKeys of
    Nothing -> fail $ "Unknown named dead key: " <> T.unpack t
    Just dk -> pure dk)
  <?> "Named dead key"

keyP :: Parser K.Key
keyP = choice $ sortBy comp keys <&> \(t, k) -> string t $> k
  where keys = (showt &&& id) <$> enumFromTo minBound maxBound
        comp = flip compare `on` fst
