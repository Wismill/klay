module Klay.Keyboard.Layout.Action.Label
  ( -- * Types
    ActionLabeler
  , CustomActionLabels
    -- * Functions
  , actionLabel
  , specialActionLabel
  , mkActionLabeler
  ) where

import Prelude hiding (Left, Right)
import Data.Map.Strict qualified as Map
import Control.Applicative ((<|>))

import Klay.Keyboard.Layout.Action.Action (Action(..))
import Klay.Keyboard.Layout.Action.Special (SpecialAction(..))
import Klay.Keyboard.Layout.Action.DeadKey (showDeadKeyLabel)
import Klay.Keyboard.Layout.Modifier (modifierLabel)
import Klay.Utils.Unicode (showInvisibleChar)
import Klay.Utils.UserInput (rawTextToString)

-- | An action labeler
type ActionLabeler = Action -> Maybe String
type CustomActionLabels = Map.Map Action String

-- | Convert an 'Action' into its label
actionLabel :: Action -> Maybe String
actionLabel (AChar c)       = Just . showInvisibleChar $ c
actionLabel (AText t)       = Just . mconcat . fmap showInvisibleChar . rawTextToString $ t
actionLabel (ADeadKey dk)   = Just . showDeadKeyLabel $ dk
actionLabel (AModifier m)   = Just . modifierLabel $ m
actionLabel (ASpecial a)    = Just . specialActionLabel $ a
actionLabel NoAction        = Nothing
actionLabel UndefinedAction = Nothing

-- | Create a custom action labeler
mkActionLabeler :: CustomActionLabels -> ActionLabeler
mkActionLabeler labels
  | null labels = actionLabel
  | otherwise   = \a -> Map.lookup a labels <|> actionLabel a

-- | Convert a 'SpecialAction' into its label
specialActionLabel :: SpecialAction -> String
-- System keys
specialActionLabel Print     = "⎙"
-- specialActionLabel SysReq -- [TODO] SysReq
specialActionLabel Scroll_Lock      = "⇳"
specialActionLabel Pause            = "⎉"
specialActionLabel Break            = "⎊"
-- Input method
specialActionLabel Iso_Group_Shift    = "⬌"
specialActionLabel Iso_Group_Latch    = "⬌᷈"
specialActionLabel Iso_Group_Lock     = "⬌͚"
specialActionLabel Iso_Prev_Group     = "⬅" -- 🢪
specialActionLabel Iso_Next_Group     = "➡" -- 🢩
specialActionLabel Iso_First_Group    = "🢘"
specialActionLabel Iso_Last_Group     = "🢚"
-- specialActionLabel Romani [TODO]
specialActionLabel Muhenkan    = "無変換"
specialActionLabel Henkan      = "変換"
specialActionLabel Katakana    = "カタカナ"
specialActionLabel Hiragana    = "ひらがな "
specialActionLabel Hiragana_Katakana = "カタカナ"
specialActionLabel Hangul      = "한/영"
-- specialActionLabel Hanja       = "한자"
-- Moves
-- ⬅➡⬆⬇ ⮈⮊⮉⮋ ◀▶▲▼ ⮜⮞⮝⮟
specialActionLabel Left      = "◀"
specialActionLabel Right     = "▶"
specialActionLabel Up        = "▲"
specialActionLabel Down      = "▼"
specialActionLabel Home      = "⇱"
specialActionLabel End       = "⇲"
specialActionLabel Page_Up   = "⎗" -- ⇞
specialActionLabel Page_Down = "⎘" -- ⇟
-- Edition
specialActionLabel Escape        = "⎋"
specialActionLabel Tab           = "↹" -- ↹ ⇥
specialActionLabel Return        = "⏎"
specialActionLabel Iso_Enter     = "⎆"
specialActionLabel BackSpace     = "⌫"
specialActionLabel Delete        = "⌦"
specialActionLabel Insert        = "⎀"
specialActionLabel Iso_Left_Tab  = "⇤"
specialActionLabel Undo          = "↶" -- ⎌
specialActionLabel Redo          = "↷"
specialActionLabel XF86Cut       = "\xF0C4" --     ✄
specialActionLabel XF86Copy      = "\xF0C5" -- 
specialActionLabel XF86Paste     = "\xF0EA" -- 
-- Media
specialActionLabel XF86AudioPlay        = "⏯" -- Also: ⏵
specialActionLabel XF86AudioPause       = "⏸"
specialActionLabel XF86AudioStop        = "⏹"
specialActionLabel XF86Eject            = "⏏"
specialActionLabel XF86AudioPrev        = "⏮"
specialActionLabel XF86AudioNext        = "⏭"
specialActionLabel XF86AudioRewind      = "⏪"
specialActionLabel XF86AudioForward     = "⏩"
specialActionLabel XF86AudioMute        = "\xF026" --  🔇
specialActionLabel XF86AudioLowerVolume = "\xF027" -- 🔉
specialActionLabel XF86AudioRaiseVolume = "\xF028" -- 🔊
-- Browser
specialActionLabel XF86Back    = "⮈"
specialActionLabel XF86Forward = "⮊"
specialActionLabel XF86Refresh = "⭮"
-- specialActionLabel XF86Stop [TODO]
specialActionLabel XF86Search    = "\xF002" -- 
specialActionLabel XF86HomePage  = "\xF015" -- 
specialActionLabel XF86Favorites = "\xF005" -- 
-- Applications
specialActionLabel Menu           = "▤"
specialActionLabel XF86Calculator = "\xF1EC" -- 
specialActionLabel XF86AudioMedia = "\xF144" -- 
specialActionLabel XF86WWW        = "\xF1FA" -- 
specialActionLabel XF86Mail       = "\xE215" -- 
specialActionLabel Find           = "\xF002" -- 
specialActionLabel XF86Explorer   = "\xF07C" -- 
specialActionLabel XF86MyComputer = "\xF108" -- 
specialActionLabel Help           = "\xF059" -- 
-- Numeric keypad
specialActionLabel KP_0        = "0"
specialActionLabel KP_1        = "1"
specialActionLabel KP_2        = "2"
specialActionLabel KP_3        = "3"
specialActionLabel KP_4        = "4"
specialActionLabel KP_5        = "5"
specialActionLabel KP_6        = "6"
specialActionLabel KP_7        = "7"
specialActionLabel KP_8        = "8"
specialActionLabel KP_9        = "9"
-- specialActionLabel KPClear    = "⌧"
specialActionLabel KP_Divide    = "∕"
specialActionLabel KP_Multiply  = "×"
specialActionLabel KP_Subtract  = "−"
specialActionLabel KP_Add       = "+"
specialActionLabel KP_Enter     = "⌤"
specialActionLabel KP_Equal     = "="
specialActionLabel KP_Decimal   = "." -- Also: ⎖
specialActionLabel KP_Separator = ","
specialActionLabel KP_Home      = specialActionLabel Home
specialActionLabel KP_Up        = specialActionLabel Up
specialActionLabel KP_Page_Up   = specialActionLabel Page_Up
specialActionLabel KP_Left      = specialActionLabel Left
-- specialActionLabel KP_Begin [TODO]
specialActionLabel KP_Right     = specialActionLabel Right
specialActionLabel KP_End       = specialActionLabel Return
specialActionLabel KP_Down      = specialActionLabel Down
specialActionLabel KP_Page_Down = specialActionLabel Page_Down
specialActionLabel KP_Insert    = specialActionLabel Insert
specialActionLabel KP_Delete    = specialActionLabel Delete
-- Power
specialActionLabel XF86PowerOff   = "⏻" -- 
specialActionLabel XF86Sleep      = "⏾"
-- specialActionLabel WakeUp = "\\x" --
specialActionLabel XF86MonBrightnessDown = "🔅"
specialActionLabel XF86MonBrightnessUp   = "🔆"
-- Default
specialActionLabel a = show a
