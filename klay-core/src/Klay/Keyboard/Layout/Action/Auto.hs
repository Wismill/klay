{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Klay.Keyboard.Layout.Action.Auto
  ( -- * Automation
    keysToSingletonMap
  , keyToAction
  , keysToActionMap1
  , keysToActionMap2
  , keysToActionMap
    -- * Options
  , mkModifierOptions
  , defaultModifiersOptions
  ) where

import Data.Map.Strict qualified as Map
import Data.Maybe (mapMaybe)

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Layout.Action.Action (Action)
import Klay.Keyboard.Layout.Action.Actions (Level, Actions, singleton, mkActions)
import Klay.Keyboard.Layout.Action.Raw (custom)
import Klay.Keyboard.Layout.Action.Map (RawActionsKeyMap, ActionsKeyMap)
import Klay.Keyboard.Layout.Modifier (ModifiersOptions, CapsLockBehaviour(..), capsBehaviourToOptions)

type MkModifierOptions = Key -> [Level] -> ModifiersOptions
type MkActions1 = Key -> Maybe Action
type MkActions2 = Key -> Maybe (Action, Action)
type MkActionsN = Key -> Maybe [Action]

mkModifierOptions :: Maybe CapsLockBehaviour -> MkModifierOptions
mkModifierOptions Nothing    = defaultModifiersOptions
mkModifierOptions (Just opt) = \_ -> capsBehaviourToOptions . fmap (,opt)

defaultModifiersOptions :: MkModifierOptions
defaultModifiersOptions _ _ = mempty

keysToSingletonMap :: (Key -> Maybe Action) -> [Key] -> ActionsKeyMap
keysToSingletonMap f = Map.fromList . mapMaybe f'
  where
    f' :: Key -> Maybe (Key, Actions)
    f' key = (key,) . singleton <$> f key

keyToAction :: Level -> MkModifierOptions -> MkActions1 -> Key -> Maybe (Key, Actions)
keyToAction l mkOpts mkAction key = (\a -> (key, mkActions (mkOpts key [l]) mempty [(l, a)])) <$> mkAction key

keysToActionMap1 :: Level -> MkModifierOptions -> MkActions1 -> [Key] -> RawActionsKeyMap
keysToActionMap1 l mkOpts mkAction = Map.map custom . Map.fromList . mapMaybe f'
  where f' = keyToAction l mkOpts mkAction

keyToActions2 :: Level -> Level -> MkModifierOptions -> MkActions2 -> Key -> Maybe (Key, Actions)
keyToActions2 l1 l2 mkOpts mkActions' key = mkMapping <$> mkActions' key
  where mkMapping (a1, a2) = (key, mkActions (mkOpts key [l1, l2]) mempty [(l1, a1), (l2, a2)])

keysToActionMap2 :: Level -> Level -> MkModifierOptions -> MkActions2 -> [Key] -> RawActionsKeyMap
keysToActionMap2 l1 l2 mkOpts mkActions' = Map.map custom . Map.fromList . mapMaybe (keyToActions2 l1 l2 mkOpts mkActions')

keyToActions :: [Level] -> MkModifierOptions -> MkActionsN -> Key -> Maybe (Key, Actions)
keyToActions ls mkOpts mkActions' key = mkMapping <$> mkActions' key
  where mkMapping as = (key, mkActions (mkOpts key ls) mempty $ zip ls as)

keysToActionMap :: [Level] -> MkModifierOptions -> MkActionsN -> [Key] -> RawActionsKeyMap
keysToActionMap ls mkOpts mkActions' = Map.map custom . Map.fromList . mapMaybe (keyToActions ls mkOpts mkActions')
