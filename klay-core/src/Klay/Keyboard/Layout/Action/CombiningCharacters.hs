-- [NOTE] Auto-generated module. Do not edit manually

{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.Action.CombiningCharacters
  ( allCombiningChars
  -- * Diacritics: Accents
  , graveAbove, graveBelow, doubleGrave, doubledGrave
  , acuteAbove, acuteBelow, doubleAcute, doubledAcute
  , circumflexAbove, circumflexBelow, doubledCircumflexAbove
  , hatchekAbove, caronAbove, hatchekBelow, caronBelow
  -- * Diacritics: Curves
  , tildeAbove, tildeBelow, doubleTildeAbove
  , breveAbove, breveBelow, doubleBreveAbove, doubleBreveBelow
  , invertedBreveAbove, invertedBreveBelow, doubleInvertedBreveAbove, doubleInvertedBreveBelow
  , horn, hookAbove, palatalizedHookBelow, retroflexHookBelow
  -- * Diacritics: Macrons
  , macronAbove, macronBelow, doubleMacronAbove, doubleMacronBelow
  , macronLeftHalfAbove, macronRightHalfAbove, conjoiningMacronAbove
  , macronLeftHalfBelow, macronRightHalfBelow, conjoiningMacronBelow
  , overline, lowLine, doubleOverline, doubleLowLine
  -- * Diacritics: Dots
  , dotAbove, dotBelow
  , diaeresisAbove, diaeresisBelow
  , threeDotsAbove, threeDotsBelow
  , fourDotsAbove
  -- * Diacritics: Overlays
  , shortStroke, longStroke
  , shortVerticalLine, longVerticalLine, doubledLongVerticalLine
  , verticalLineAbove, doubledVerticalLineAbove, verticalLineBelow, doubledVerticalLineBelow
  , shortSolidus, longSolidus, reverseLongSolidus, doubledLongSolidus
  , tildeOverlay
  -- * Diacritics: Rings
  , ringAbove, ringBelow, doubledRingBelow
  -- * Diacritics: Curls
  , cedillaBelow, cedillaAbove
  , commaAbove, commaAboveRight, reversedCommaAbove, commaBelow, turnedCommaAbove
  , ogonek
  -- * Diacritics: Miscellaneous
  , plusSignBelow, minusSignBelow
  -- * Symbols: Overlay
  , ringOverlay, clockwiseRingOverlay, anticlockwiseRingOverlay
  -- * Symbols: Enclosing
  , enclosingCircle, enclosingSquare, enclosingDiamond
  , enclosingCircleBackslash, enclosingKeycap, enclosingTriangle
  -- * Symbols: Arrow
  , clockwiseArrowAbove, anticlockwiseArrowAbove
  , leftwardsArrowAbove, leftwardsArrowBelow
  , rightwardsArrowAbove, rightwardsArrowBelow, doubleRightwardsArrowBelow
  -- * Symbols: Miscellaneous
  , asteriskAbove, asteriskBelow
  , infinityAbove, wigglyLineBelow
  -- * Block: Combining Diacritical Marks
  , graveAccent, acuteAccent, circumflexAccent, tilde, macron, breve, diaeresis, doubleAcuteAccent, caron, doubleVerticalLineAbove, doubleGraveAccent, candrabindu, invertedBreve, graveAccentBelow, acuteAccentBelow, leftTackBelow, rightTackBelow, leftAngleAbove, leftHalfRingBelow, upTackBelow, downTackBelow, cedilla, bridgeBelow, invertedDoubleArchBelow, circumflexAccentBelow, shortStrokeOverlay, longStrokeOverlay, shortSolidusOverlay, longSolidusOverlay, rightHalfRingBelow, invertedBridgeBelow, squareBelow, seagullBelow, xAbove, verticalTilde, graveToneMark, acuteToneMark, greekPerispomeni, greekKoronis, greekDialytikaTonos, greekYpogegrammeni, bridgeAbove, equalsSignBelow, doubleVerticalLineBelow, leftAngleBelow, notTildeAbove, homotheticAbove, almostEqualToAbove, leftRightArrowBelow, upwardsArrowBelow, rightArrowheadAbove, leftHalfRingAbove, fermata, xBelow, leftArrowheadBelow, rightArrowheadBelow, rightArrowheadAndUpArrowheadBelow, rightHalfRingAbove, dotAboveRight, doubleRingBelow, zigzagAbove, doubleBreve, doubleMacron, doubleTilde, doubleInvertedBreve, latinSmallLetterA, latinSmallLetterE, latinSmallLetterI, latinSmallLetterO, latinSmallLetterU, latinSmallLetterC, latinSmallLetterD, latinSmallLetterH, latinSmallLetterM, latinSmallLetterR, latinSmallLetterT, latinSmallLetterV, latinSmallLetterX
  -- * Block: Cyrillic
  , cyrillicTitlo, cyrillicPalatalization, cyrillicDasiaPneumata, cyrillicPsiliPneumata, cyrillicPokrytie
  -- * Block: Hebrew
  , hebrewAccentEtnahta, hebrewAccentSegol, hebrewAccentShalshelet, hebrewAccentZaqefQatan, hebrewAccentZaqefGadol, hebrewAccentTipeha, hebrewAccentRevia, hebrewAccentZarqa, hebrewAccentPashta, hebrewAccentYetiv, hebrewAccentTevir, hebrewAccentGeresh, hebrewAccentGereshMuqdam, hebrewAccentGershayim, hebrewAccentQarneyPara, hebrewAccentTelishaGedola, hebrewAccentPazer, hebrewAccentAtnahHafukh, hebrewAccentMunah, hebrewAccentMahapakh, hebrewAccentMerkha, hebrewAccentMerkhaKefula, hebrewAccentDarga, hebrewAccentQadma, hebrewAccentTelishaQetana, hebrewAccentYerahBenYomo, hebrewAccentOle, hebrewAccentIluy, hebrewAccentDehi, hebrewAccentZinor, hebrewMarkMasoraCircle, hebrewPointSheva, hebrewPointHatafSegol, hebrewPointHatafPatah, hebrewPointHatafQamats, hebrewPointHiriq, hebrewPointTsere, hebrewPointSegol, hebrewPointPatah, hebrewPointQamats, hebrewPointHolam, hebrewPointHolamHaserForVav, hebrewPointQubuts, hebrewPointDageshOrMapiq, hebrewPointMeteg, hebrewPointRafe, hebrewPointShinDot, hebrewPointSinDot, hebrewMarkUpperDot, hebrewMarkLowerDot, hebrewPointQamatsQatan
  -- * Block: Arabic
  , arabicSignSallallahouAlayheWassallam, arabicSignAlayheAssallam, arabicSignRahmatullahAlayhe, arabicSignRadiAllahouAnhu, arabicSignTakhallus, arabicSmallHighTah, arabicSmallHighLigatureAlefWithLamWithYeh, arabicSmallHighZain, arabicSmallFatha, arabicSmallDamma, arabicSmallKasra, arabicFathatan, arabicDammatan, arabicKasratan, arabicFatha, arabicDamma, arabicKasra, arabicShadda, arabicSukun, arabicMaddahAbove, arabicHamzaAbove, arabicHamzaBelow, arabicSubscriptAlef, arabicInvertedDamma, arabicMarkNoonGhunna, arabicZwarakay, arabicVowelSignSmallVAbove, arabicVowelSignInvertedSmallVAbove, arabicVowelSignDotBelow, arabicReversedDamma, arabicFathaWithTwoDots, arabicWavyHamzaBelow, arabicLetterSuperscriptAlef, arabicSmallHighLigatureSadWithLamWithAlefMaksura, arabicSmallHighLigatureQafWithLamWithAlefMaksura, arabicSmallHighMeemInitialForm, arabicSmallHighLamAlef, arabicSmallHighJeem, arabicSmallHighThreeDots, arabicSmallHighSeen, arabicSmallHighRoundedZero, arabicSmallHighUprightRectangularZero, arabicSmallHighDotlessHeadOfKhah, arabicSmallHighMeemIsolatedForm, arabicSmallLowSeen, arabicSmallHighMadda, arabicSmallHighYeh, arabicSmallHighNoon, arabicEmptyCentreLowStop, arabicEmptyCentreHighStop, arabicRoundedHighStopWithFilledCentre, arabicSmallLowMeem
  -- * Block: Syriac
  , syriacLetterSuperscriptAlaph, syriacPthahaAbove, syriacPthahaBelow, syriacPthahaDotted, syriacZqaphaAbove, syriacZqaphaBelow, syriacZqaphaDotted, syriacRbasaAbove, syriacRbasaBelow, syriacDottedZlamaHorizontal, syriacDottedZlamaAngular, syriacHbasaAbove, syriacHbasaBelow, syriacHbasaEsasaDotted, syriacEsasaAbove, syriacEsasaBelow, syriacRwaha, syriacFeminineDot, syriacQushshaya, syriacRukkakha, syriacTwoVerticalDotsAbove, syriacTwoVerticalDotsBelow, syriacThreeDotsAbove, syriacThreeDotsBelow, syriacObliqueLineAbove, syriacObliqueLineBelow, syriacMusic, syriacBarrekh
  -- * Block: NKo
  , nkoCombiningShortHighTone, nkoCombiningShortLowTone, nkoCombiningShortRisingTone, nkoCombiningLongDescendingTone, nkoCombiningLongHighTone, nkoCombiningLongLowTone, nkoCombiningLongRisingTone, nkoCombiningNasalizationMark, nkoCombiningDoubleDotAbove, nkoDantayalan
  -- * Block: Samaritan
  , samaritanMarkIn, samaritanMarkInAlaf, samaritanMarkOcclusion, samaritanMarkDagesh, samaritanMarkEpentheticYut, samaritanVowelSignLongE, samaritanVowelSignE, samaritanVowelSignOverlongAa, samaritanVowelSignLongAa, samaritanVowelSignAa, samaritanVowelSignOverlongA, samaritanVowelSignLongA, samaritanVowelSignA, samaritanVowelSignShortA, samaritanVowelSignLongU, samaritanVowelSignU, samaritanVowelSignLongI, samaritanVowelSignI, samaritanVowelSignO, samaritanVowelSignSukun, samaritanMarkNequdaa
  -- * Block: Mandaic
  , mandaicAffricationMark, mandaicVocalizationMark, mandaicGeminationMark
  -- * Block: Arabic Extended-A
  , arabicSmallLowWaw, arabicSmallHighWordArRub, arabicSmallHighSad, arabicSmallHighAin, arabicSmallHighQaf, arabicSmallHighNoonWithKasra, arabicSmallLowNoonWithKasra, arabicSmallHighWordAthThalatha, arabicSmallHighWordAsSajda, arabicSmallHighWordAnNisf, arabicSmallHighWordSakta, arabicSmallHighWordQif, arabicSmallHighWordWaqfa, arabicSmallHighFootnoteMarker, arabicSmallHighSignSafha, arabicTurnedDammaBelow, arabicCurlyFatha, arabicCurlyDamma, arabicCurlyKasra, arabicCurlyFathatan, arabicCurlyDammatan, arabicCurlyKasratan, arabicToneOneDotAbove, arabicToneTwoDotsAbove, arabicToneLoopAbove, arabicToneOneDotBelow, arabicToneTwoDotsBelow, arabicToneLoopBelow, arabicOpenFathatan, arabicOpenDammatan, arabicOpenKasratan, arabicSmallHighWaw, arabicFathaWithRing, arabicFathaWithDotAbove, arabicKasraWithDotBelow, arabicLeftArrowheadAbove, arabicRightArrowheadAbove, arabicLeftArrowheadBelow, arabicRightArrowheadBelow, arabicDoubleRightArrowheadAbove, arabicDoubleRightArrowheadAboveWithDot, arabicRightArrowheadAboveWithDot, arabicDammaWithDot, arabicMarkSidewaysNoonGhunna
  -- * Block: Devanagari
  , devanagariSignNukta, devanagariSignVirama, devanagariStressSignUdatta, devanagariStressSignAnudatta, devanagariGraveAccent, devanagariAcuteAccent
  -- * Block: Bengali
  , bengaliSignNukta, bengaliSignVirama, bengaliSandhiMark
  -- * Block: Gurmukhi
  , gurmukhiSignNukta, gurmukhiSignVirama
  -- * Block: Gujarati
  , gujaratiSignNukta, gujaratiSignVirama
  -- * Block: Oriya
  , oriyaSignNukta, oriyaSignVirama
  -- * Block: Tamil
  , tamilSignVirama
  -- * Block: Telugu
  , teluguSignVirama, teluguLengthMark, teluguAiLengthMark
  -- * Block: Kannada
  , kannadaSignNukta, kannadaSignVirama
  -- * Block: Malayalam
  , malayalamSignVerticalBarVirama, malayalamSignCircularVirama, malayalamSignVirama
  -- * Block: Sinhala
  , sinhalaSignAlLakuna
  -- * Block: Thai
  , thaiCharacterSaraU, thaiCharacterSaraUu, thaiCharacterPhinthu, thaiCharacterMaiEk, thaiCharacterMaiTho, thaiCharacterMaiTri, thaiCharacterMaiChattawa
  -- * Block: Lao
  , laoVowelSignU, laoVowelSignUu, laoSignPaliVirama, laoToneMaiEk, laoToneMaiTho, laoToneMaiTi, laoToneMaiCatawa
  -- * Block: Tibetan
  , tibetanAstrologicalSignKhyudPa, tibetanAstrologicalSignSdongTshugs, tibetanMarkNgasBzungNyiZla, tibetanMarkNgasBzungSgorRtags, tibetanMarkTsaPhru, tibetanVowelSignAa, tibetanVowelSignI, tibetanVowelSignU, tibetanVowelSignE, tibetanVowelSignEe, tibetanVowelSignO, tibetanVowelSignOo, tibetanVowelSignReversedI, tibetanSignNyiZlaNaaDa, tibetanSignSnaLdan, tibetanMarkHalanta, tibetanSignLciRtags, tibetanSignYangRtags, tibetanSymbolPadmaGdan
  -- * Block: Myanmar
  , myanmarSignDotBelow, myanmarSignVirama, myanmarSignAsat, myanmarSignShanCouncilEmphaticTone
  -- * Block: Ethiopic
  , ethiopicCombiningGeminationAndVowelLengthMark, ethiopicCombiningVowelLengthMark, ethiopicCombiningGeminationMark
  -- * Block: Tagalog
  , tagalogSignVirama
  -- * Block: Hanunoo
  , hanunooSignPamudpod
  -- * Block: Khmer
  , khmerSignCoeng, khmerSignAtthacan
  -- * Block: Mongolian
  , mongolianLetterAliGaliDagalga
  -- * Block: Limbu
  , limbuSignMukphreng, limbuSignKemphreng, limbuSignSaI
  -- * Block: Buginese
  , bugineseVowelSignI, bugineseVowelSignU
  -- * Block: Tai Tham
  , taiThamSignSakot, taiThamSignTone1, taiThamSignTone2, taiThamSignKhuenTone3, taiThamSignKhuenTone4, taiThamSignKhuenTone5, taiThamSignRaHaam, taiThamSignMaiSam, taiThamSignKhuenLueKaran, taiThamCombiningCryptogrammicDot
  -- * Block: Combining Diacritical Marks Extended
  , doubledCircumflexAccent, diaeresisRing, infinity, downwardsArrow, tripleDot, xXBelow, openMarkBelow, doubleOpenMarkBelow, lightCentralizationStrokeBelow, strongCentralizationStrokeBelow, parenthesesAbove, doubleParenthesesAbove, parenthesesBelow
  -- * Block: Balinese
  , balineseSignRerekan, balineseAdegAdeg, balineseMusicalSymbolCombiningTegeh, balineseMusicalSymbolCombiningEndep, balineseMusicalSymbolCombiningKempul, balineseMusicalSymbolCombiningKempli, balineseMusicalSymbolCombiningJegogan, balineseMusicalSymbolCombiningKempulWithJegogan, balineseMusicalSymbolCombiningKempliWithJegogan, balineseMusicalSymbolCombiningBende, balineseMusicalSymbolCombiningGong
  -- * Block: Sundanese
  , sundaneseSignPamaaeh, sundaneseSignVirama
  -- * Block: Batak
  , batakSignTompi, batakPangolat, batakPanongonan
  -- * Block: Lepcha
  , lepchaSignNukta
  -- * Block: Vedic Extensions
  , vedicToneKarshana, vedicToneShara, vedicTonePrenkha, vedicSignYajurvedicMidlineSvarita, vedicToneYajurvedicAggravatedIndependentSvarita, vedicToneYajurvedicIndependentSvarita, vedicToneYajurvedicKathakaIndependentSvarita, vedicToneCandraBelow, vedicToneYajurvedicKathakaIndependentSvaritaSchroeder, vedicToneDoubleSvarita, vedicToneTripleSvarita, vedicToneKathakaAnudatta, vedicToneDotBelow, vedicToneTwoDotsBelow, vedicToneThreeDotsBelow, vedicToneRigvedicKashmiriIndependentSvarita, vedicSignVisargaSvarita, vedicSignVisargaUdatta, vedicSignReversedVisargaUdatta, vedicSignVisargaAnudatta, vedicSignReversedVisargaAnudatta, vedicSignVisargaUdattaWithTail, vedicSignVisargaAnudattaWithTail, vedicSignTiryak, vedicToneCandraAbove, vedicToneRingAbove, vedicToneDoubleRingAbove
  -- * Block: Combining Diacritical Marks Supplement
  , dottedGraveAccent, dottedAcuteAccent, snakeBelow, suspensionMark, macronAcute, graveMacron, macronGrave, acuteMacron, graveAcuteGrave, acuteGraveAcute, latinSmallLetterRBelow, breveMacron, macronBreve, doubleCircumflexAbove, ogonekAbove, zigzagBelow, isBelow, urAbove, usAbove, latinSmallLetterFlattenedOpenAAbove, latinSmallLetterAe, latinSmallLetterAo, latinSmallLetterAv, latinSmallLetterCCedilla, latinSmallLetterInsularD, latinSmallLetterEth, latinSmallLetterG, latinLetterSmallCapitalG, latinSmallLetterK, latinSmallLetterL, latinLetterSmallCapitalL, latinLetterSmallCapitalM, latinSmallLetterN, latinLetterSmallCapitalN, latinLetterSmallCapitalR, latinSmallLetterRRotunda, latinSmallLetterS, latinSmallLetterLongS, latinSmallLetterZ, latinSmallLetterAlpha, latinSmallLetterB, latinSmallLetterBeta, latinSmallLetterSchwa, latinSmallLetterF, latinSmallLetterLWithDoubleMiddleTilde, latinSmallLetterOWithLightCentralizationStroke, latinSmallLetterP, latinSmallLetterEsh, latinSmallLetterUWithLightCentralizationStroke, latinSmallLetterW, latinSmallLetterAWithDiaeresis, latinSmallLetterOWithDiaeresis, latinSmallLetterUWithDiaeresis, upTackAbove, kavykaAboveRight, kavykaAboveLeft, dotAboveLeft, wideInvertedBridgeBelow, deletionMark, almostEqualToBelow, leftArrowheadAbove, rightArrowheadAndDownArrowheadBelow
  -- * Block: Combining Diacritical Marks for Symbols
  , leftHarpoonAbove, rightHarpoonAbove, longVerticalLineOverlay, shortVerticalLineOverlay, leftArrowAbove, rightArrowAbove, leftRightArrowAbove, reverseSolidusOverlay, doubleVerticalStrokeOverlay, annuitySymbol, tripleUnderdot, wideBridgeAbove, leftwardsArrowOverlay, longDoubleSolidusOverlay, rightwardsHarpoonWithBarbDownwards, leftwardsHarpoonWithBarbDownwards, leftArrowBelow, rightArrowBelow
  -- * Block: Coptic
  , copticCombiningNiAbove, copticCombiningSpiritusAsper, copticCombiningSpiritusLenis
  -- * Block: Tifinagh
  , tifinaghConsonantJoiner
  -- * Block: Cyrillic Extended-A
  , cyrillicLetterBe, cyrillicLetterVe, cyrillicLetterGhe, cyrillicLetterDe, cyrillicLetterZhe, cyrillicLetterZe, cyrillicLetterKa, cyrillicLetterEl, cyrillicLetterEm, cyrillicLetterEn, cyrillicLetterO, cyrillicLetterPe, cyrillicLetterEr, cyrillicLetterEs, cyrillicLetterTe, cyrillicLetterHa, cyrillicLetterTse, cyrillicLetterChe, cyrillicLetterSha, cyrillicLetterShcha, cyrillicLetterFita, cyrillicLetterEsTe, cyrillicLetterA, cyrillicLetterIe, cyrillicLetterDjerv, cyrillicLetterMonographUk, cyrillicLetterYat, cyrillicLetterYu, cyrillicLetterIotifiedA, cyrillicLetterLittleYus, cyrillicLetterBigYus, cyrillicLetterIotifiedBigYus
  -- * Block: CJK Symbols and Punctuation
  , ideographicLevelToneMark, ideographicRisingToneMark, ideographicDepartingToneMark, ideographicEnteringToneMark, hangulSingleDotToneMark, hangulDoubleDotToneMark
  -- * Block: Hiragana
  , katakanaHiraganaVoicedSoundMark, katakanaHiraganaSemiVoicedSoundMark
  -- * Block: Cyrillic Extended-B
  , cyrillicVzmet, cyrillicLetterUkrainianIe, cyrillicLetterI, cyrillicLetterYi, cyrillicLetterU, cyrillicLetterHardSign, cyrillicLetterYeru, cyrillicLetterSoftSign, cyrillicLetterOmega, cyrillicKavyka, cyrillicPayerok, cyrillicLetterEf, cyrillicLetterIotifiedE
  -- * Block: Bamum
  , bamumCombiningMarkKoqndon, bamumCombiningMarkTukwentis
  -- * Block: Syloti Nagri
  , sylotiNagriSignHasanta
  -- * Block: Saurashtra
  , saurashtraSignVirama
  -- * Block: Devanagari Extended
  , devanagariDigitZero, devanagariDigitOne, devanagariDigitTwo, devanagariDigitThree, devanagariDigitFour, devanagariDigitFive, devanagariDigitSix, devanagariDigitSeven, devanagariDigitEight, devanagariDigitNine, devanagariLetterA, devanagariLetterU, devanagariLetterKa, devanagariLetterNa, devanagariLetterPa, devanagariLetterRa, devanagariLetterVi, devanagariSignAvagraha
  -- * Block: Kayah Li
  , kayahLiTonePlophu, kayahLiToneCalya, kayahLiToneCalyaPlophu
  -- * Block: Rejang
  , rejangVirama
  -- * Block: Javanese
  , javaneseSignCecakTelu, javanesePangkon
  -- * Block: Tai Viet
  , taiVietMaiKang, taiVietVowelI, taiVietVowelUe, taiVietVowelU, taiVietMaiKhit, taiVietVowelIa, taiVietVowelAm, taiVietToneMaiEk, taiVietToneMaiTho
  -- * Block: Meetei Mayek Extensions
  , meeteiMayekVirama
  -- * Block: Meetei Mayek
  , meeteiMayekApunIyek
  -- * Block: Alphabetic Presentation Forms
  , hebrewPointJudeoSpanishVarika
  -- * Block: Combining Half Marks
  , ligatureLeftHalf, ligatureRightHalf, doubleTildeLeftHalf, doubleTildeRightHalf, macronLeftHalf, macronRightHalf, conjoiningMacron, ligatureLeftHalfBelow, ligatureRightHalfBelow, tildeLeftHalfBelow, tildeRightHalfBelow, cyrillicTitloLeftHalf, cyrillicTitloRightHalf
  -- * Block: Phaistos Disc
  , phaistosDiscSignCombiningObliqueStroke
  -- * Block: Coptic Epact Numbers
  , copticEpactThousandsMark
  -- * Block: Old Permic
  , oldPermicLetterAn, oldPermicLetterDoi, oldPermicLetterZata, oldPermicLetterNenoe, oldPermicLetterSii
  -- * Block: Kharoshthi
  , kharoshthiSignDoubleRingBelow, kharoshthiSignVisarga, kharoshthiSignBarAbove, kharoshthiSignCauda, kharoshthiSignDotBelow, kharoshthiVirama
  -- * Block: Manichaean
  , manichaeanAbbreviationMarkAbove, manichaeanAbbreviationMarkBelow
  -- * Block: Hanifi Rohingya
  , hanifiRohingyaSignHarbahay, hanifiRohingyaSignTahala, hanifiRohingyaSignTana, hanifiRohingyaSignTassi
  -- * Block: Sogdian
  , sogdianCombiningDotBelow, sogdianCombiningTwoDotsBelow, sogdianCombiningDotAbove, sogdianCombiningTwoDotsAbove, sogdianCombiningCurveAbove, sogdianCombiningCurveBelow, sogdianCombiningHookAbove, sogdianCombiningHookBelow, sogdianCombiningLongHookBelow, sogdianCombiningReshBelow, sogdianCombiningStrokeBelow
  -- * Block: Brahmi
  , brahmiVirama, brahmiNumberJoiner
  -- * Block: Kaithi
  , kaithiSignVirama, kaithiSignNukta
  -- * Block: Chakma
  , chakmaSignCandrabindu, chakmaSignAnusvara, chakmaSignVisarga, chakmaVirama, chakmaMaayyaa
  -- * Block: Mahajani
  , mahajaniSignNukta
  -- * Block: Sharada
  , sharadaSignVirama, sharadaSignNukta
  -- * Block: Khojki
  , khojkiSignVirama, khojkiSignNukta
  -- * Block: Khudawadi
  , khudawadiSignNukta, khudawadiSignVirama
  -- * Block: Grantha
  , binduBelow, granthaSignNukta, granthaSignVirama, granthaDigitZero, granthaDigitOne, granthaDigitTwo, granthaDigitThree, granthaDigitFour, granthaDigitFive, granthaDigitSix, granthaLetterA, granthaLetterKa, granthaLetterNa, granthaLetterVi, granthaLetterPa
  -- * Block: Newa
  , newaSignVirama, newaSignNukta, newaSandhiMark
  -- * Block: Tirhuta
  , tirhutaSignVirama, tirhutaSignNukta
  -- * Block: Siddham
  , siddhamSignVirama, siddhamSignNukta
  -- * Block: Modi
  , modiSignVirama
  -- * Block: Takri
  , takriSignVirama, takriSignNukta
  -- * Block: Ahom
  , ahomSignKiller
  -- * Block: Dogra
  , dograSignVirama, dograSignNukta
  -- * Block: Nandinagari
  , nandinagariSignVirama
  -- * Block: Zanabazar Square
  , zanabazarSquareSignVirama, zanabazarSquareSubjoiner
  -- * Block: Soyombo
  , soyomboSubjoiner
  -- * Block: Bhaiksuki
  , bhaiksukiSignVirama
  -- * Block: Masaram Gondi
  , masaramGondiSignNukta, masaramGondiSignHalanta, masaramGondiVirama
  -- * Block: Gunjala Gondi
  , gunjalaGondiVirama
  -- * Block: Bassa Vah
  , bassaVahCombiningHighTone, bassaVahCombiningLowTone, bassaVahCombiningMidTone, bassaVahCombiningLowMidTone, bassaVahCombiningHighLowTone
  -- * Block: Pahawh Hmong
  , pahawhHmongMarkCimTub, pahawhHmongMarkCimSo, pahawhHmongMarkCimKes, pahawhHmongMarkCimKhav, pahawhHmongMarkCimSuam, pahawhHmongMarkCimHom, pahawhHmongMarkCimTaum
  -- * Block: Duployan
  , duployanDoubleMark
  -- * Block: Musical Symbols
  , musicalSymbolCombiningStem, musicalSymbolCombiningSprechgesangStem, musicalSymbolCombiningTremolo1, musicalSymbolCombiningTremolo2, musicalSymbolCombiningTremolo3, musicalSymbolCombiningAugmentationDot, musicalSymbolCombiningFlag1, musicalSymbolCombiningFlag2, musicalSymbolCombiningFlag3, musicalSymbolCombiningFlag4, musicalSymbolCombiningFlag5, musicalSymbolCombiningAccent, musicalSymbolCombiningStaccato, musicalSymbolCombiningTenuto, musicalSymbolCombiningStaccatissimo, musicalSymbolCombiningMarcato, musicalSymbolCombiningMarcatoStaccato, musicalSymbolCombiningAccentStaccato, musicalSymbolCombiningLoure, musicalSymbolCombiningDoit, musicalSymbolCombiningRip, musicalSymbolCombiningFlip, musicalSymbolCombiningSmear, musicalSymbolCombiningBend, musicalSymbolCombiningDoubleTongue, musicalSymbolCombiningTripleTongue, musicalSymbolCombiningDownBow, musicalSymbolCombiningUpBow, musicalSymbolCombiningHarmonic, musicalSymbolCombiningSnapPizzicato
  -- * Block: Ancient Greek Musical Notation
  , greekMusicalTriseme, greekMusicalTetraseme, greekMusicalPentaseme
  -- * Block: Glagolitic Supplement
  , glagoliticLetterAzu, glagoliticLetterBuky, glagoliticLetterVede, glagoliticLetterGlagoli, glagoliticLetterDobro, glagoliticLetterYestu, glagoliticLetterZhivete, glagoliticLetterZemlja, glagoliticLetterIzhe, glagoliticLetterInitialIzhe, glagoliticLetterI, glagoliticLetterDjervi, glagoliticLetterKako, glagoliticLetterLjudije, glagoliticLetterMyslite, glagoliticLetterNashi, glagoliticLetterOnu, glagoliticLetterPokoji, glagoliticLetterRitsi, glagoliticLetterSlovo, glagoliticLetterTvrido, glagoliticLetterUku, glagoliticLetterFritu, glagoliticLetterHeru, glagoliticLetterShta, glagoliticLetterTsi, glagoliticLetterChrivi, glagoliticLetterSha, glagoliticLetterYeru, glagoliticLetterYeri, glagoliticLetterYati, glagoliticLetterYu, glagoliticLetterSmallYus, glagoliticLetterYo, glagoliticLetterIotatedSmallYus, glagoliticLetterBigYus, glagoliticLetterIotatedBigYus, glagoliticLetterFita
  -- * Block: Nyiakeng Puachue Hmong
  , nyiakengPuachueHmongToneB, nyiakengPuachueHmongToneM, nyiakengPuachueHmongToneJ, nyiakengPuachueHmongToneV, nyiakengPuachueHmongToneS, nyiakengPuachueHmongToneG, nyiakengPuachueHmongToneD
  -- * Block: Wancho
  , wanchoToneTup, wanchoToneTupni, wanchoToneKoi, wanchoToneKoini
  -- * Block: Mende Kikakui
  , mendeKikakuiCombiningNumberTeens, mendeKikakuiCombiningNumberTens, mendeKikakuiCombiningNumberHundreds, mendeKikakuiCombiningNumberThousands, mendeKikakuiCombiningNumberTenThousands, mendeKikakuiCombiningNumberHundredThousands, mendeKikakuiCombiningNumberMillions
  -- * Block: Adlam
  , adlamAlifLengthener, adlamVowelLengthener, adlamGeminationMark, adlamHamza, adlamConsonantModifier, adlamGeminateConsonantModifier, adlamNukta
  ) where


-- Diacritics: Accents----------------------------------------------------------
graveAbove, graveBelow, doubleGrave, doubledGrave :: Char
graveAbove = '\x0300' -- @U+0300@ COMBINING GRAVE ACCENT: ◌̀
graveBelow = '\x0316' -- @U+0316@ COMBINING GRAVE ACCENT BELOW: ◌̖
doubleGrave = '\x030f' -- @U+030F@ COMBINING DOUBLE GRAVE ACCENT: ◌̏
doubledGrave = '\x030f' -- @U+030F@ COMBINING DOUBLE GRAVE ACCENT: ◌̏. Alias of 'doubleGrave'
acuteAbove, acuteBelow, doubleAcute, doubledAcute :: Char
acuteAbove = '\x0301' -- @U+0301@ COMBINING ACUTE ACCENT: ◌́
acuteBelow = '\x0317' -- @U+0317@ COMBINING ACUTE ACCENT BELOW: ◌̗
doubleAcute = '\x030b' -- @U+030B@ COMBINING DOUBLE ACUTE ACCENT: ◌̋
doubledAcute = '\x030b' -- @U+030B@ COMBINING DOUBLE ACUTE ACCENT: ◌̋. Alias of 'doubleAcute'
circumflexAbove, circumflexBelow, doubledCircumflexAbove :: Char
circumflexAbove = '\x0302' -- @U+0302@ COMBINING CIRCUMFLEX ACCENT: ◌̂
circumflexBelow = '\x032d' -- @U+032D@ COMBINING CIRCUMFLEX ACCENT BELOW: ◌̭
doubledCircumflexAbove = '\x1ab0' -- @U+1AB0@ COMBINING DOUBLED CIRCUMFLEX ACCENT: ◌᪰
hatchekAbove, caronAbove, hatchekBelow, caronBelow :: Char
hatchekAbove = '\x030c' -- @U+030C@ COMBINING CARON: ◌̌
caronAbove = '\x030c' -- @U+030C@ COMBINING CARON: ◌̌. Alias of 'hatchekAbove'
hatchekBelow = '\x032c' -- @U+032C@ COMBINING CARON BELOW: ◌̬
caronBelow = '\x032c' -- @U+032C@ COMBINING CARON BELOW: ◌̬. Alias of 'hatchekBelow'

-- Diacritics: Curves-----------------------------------------------------------
tildeAbove, tildeBelow, doubleTildeAbove :: Char
tildeAbove = '\x0303' -- @U+0303@ COMBINING TILDE: ◌̃
tildeBelow = '\x0330' -- @U+0330@ COMBINING TILDE BELOW: ◌̰
doubleTildeAbove = '\x0360' -- @U+0360@ COMBINING DOUBLE TILDE: ◌͠◌
breveAbove, breveBelow, doubleBreveAbove, doubleBreveBelow :: Char
breveAbove = '\x0306' -- @U+0306@ COMBINING BREVE: ◌̆
breveBelow = '\x032e' -- @U+032E@ COMBINING BREVE BELOW: ◌̮
doubleBreveAbove = '\x035d' -- @U+035D@ COMBINING DOUBLE BREVE: ◌͝◌
doubleBreveBelow = '\x035c' -- @U+035C@ COMBINING DOUBLE BREVE BELOW: ◌͜◌
invertedBreveAbove, invertedBreveBelow, doubleInvertedBreveAbove, doubleInvertedBreveBelow :: Char
invertedBreveAbove = '\x0311' -- @U+0311@ COMBINING INVERTED BREVE: ◌̑
invertedBreveBelow = '\x032f' -- @U+032F@ COMBINING INVERTED BREVE BELOW: ◌̯
doubleInvertedBreveAbove = '\x0361' -- @U+0361@ COMBINING DOUBLE INVERTED BREVE: ◌͡◌
doubleInvertedBreveBelow = '\x1dfc' -- @U+1DFC@ COMBINING DOUBLE INVERTED BREVE BELOW: ◌᷼◌
horn, hookAbove, palatalizedHookBelow, retroflexHookBelow :: Char
horn = '\x031b' -- @U+031B@ COMBINING HORN: ◌̛
hookAbove = '\x0309' -- @U+0309@ COMBINING HOOK ABOVE: ◌̉
palatalizedHookBelow = '\x0321' -- @U+0321@ COMBINING PALATALIZED HOOK BELOW: ◌̡
retroflexHookBelow = '\x0322' -- @U+0322@ COMBINING RETROFLEX HOOK BELOW: ◌̢

-- Diacritics: Macrons----------------------------------------------------------
macronAbove, macronBelow, doubleMacronAbove, doubleMacronBelow :: Char
macronAbove = '\x0304' -- @U+0304@ COMBINING MACRON: ◌̄
macronBelow = '\x0331' -- @U+0331@ COMBINING MACRON BELOW: ◌̱
doubleMacronAbove = '\x035e' -- @U+035E@ COMBINING DOUBLE MACRON: ◌͞◌
doubleMacronBelow = '\x035f' -- @U+035F@ COMBINING DOUBLE MACRON BELOW: ◌͟◌
macronLeftHalfAbove, macronRightHalfAbove, conjoiningMacronAbove :: Char
macronLeftHalfAbove = '\xfe24' -- @U+FE24@ COMBINING MACRON LEFT HALF: ◌︤
macronRightHalfAbove = '\xfe25' -- @U+FE25@ COMBINING MACRON RIGHT HALF: ◌︥
conjoiningMacronAbove = '\xfe26' -- @U+FE26@ COMBINING CONJOINING MACRON: ◌︦
macronLeftHalfBelow, macronRightHalfBelow, conjoiningMacronBelow :: Char
macronLeftHalfBelow = '\xfe2b' -- @U+FE2B@ COMBINING MACRON LEFT HALF BELOW: ◌︫
macronRightHalfBelow = '\xfe2c' -- @U+FE2C@ COMBINING MACRON RIGHT HALF BELOW: ◌︬
conjoiningMacronBelow = '\xfe2d' -- @U+FE2D@ COMBINING CONJOINING MACRON BELOW: ◌︭
overline, lowLine, doubleOverline, doubleLowLine :: Char
overline = '\x0305' -- @U+0305@ COMBINING OVERLINE: ◌̅
lowLine = '\x0332' -- @U+0332@ COMBINING LOW LINE: ◌̲
doubleOverline = '\x033f' -- @U+033F@ COMBINING DOUBLE OVERLINE: ◌̿◌
doubleLowLine = '\x0333' -- @U+0333@ COMBINING DOUBLE LOW LINE: ◌̳◌

-- Diacritics: Dots-------------------------------------------------------------
dotAbove, dotBelow :: Char
dotAbove = '\x0307' -- @U+0307@ COMBINING DOT ABOVE: ◌̇
dotBelow = '\x0323' -- @U+0323@ COMBINING DOT BELOW: ◌̣
diaeresisAbove, diaeresisBelow :: Char
diaeresisAbove = '\x0308' -- @U+0308@ COMBINING DIAERESIS: ◌̈
diaeresisBelow = '\x0324' -- @U+0324@ COMBINING DIAERESIS BELOW: ◌̤
threeDotsAbove, threeDotsBelow :: Char
threeDotsAbove = '\x20db' -- @U+20DB@ COMBINING THREE DOTS ABOVE: ◌⃛
threeDotsBelow = '\x20e8' -- @U+20E8@ COMBINING TRIPLE UNDERDOT: ◌⃨
fourDotsAbove :: Char
fourDotsAbove = '\x20dc' -- @U+20DC@ COMBINING FOUR DOTS ABOVE: ◌⃜

-- Diacritics: Overlays---------------------------------------------------------
shortStroke, longStroke :: Char
shortStroke = '\x0335' -- @U+0335@ COMBINING SHORT STROKE OVERLAY: ◌̵
longStroke = '\x0336' -- @U+0336@ COMBINING LONG STROKE OVERLAY: ◌̶
shortVerticalLine, longVerticalLine, doubledLongVerticalLine :: Char
shortVerticalLine = '\x20d3' -- @U+20D3@ COMBINING SHORT VERTICAL LINE OVERLAY: ◌⃓
longVerticalLine = '\x20d2' -- @U+20D2@ COMBINING LONG VERTICAL LINE OVERLAY: ◌⃒
doubledLongVerticalLine = '\x20e6' -- @U+20E6@ COMBINING DOUBLE VERTICAL STROKE OVERLAY: ◌⃦
verticalLineAbove, doubledVerticalLineAbove, verticalLineBelow, doubledVerticalLineBelow :: Char
verticalLineAbove = '\x030d' -- @U+030D@ COMBINING VERTICAL LINE ABOVE: ◌̍
doubledVerticalLineAbove = '\x030e' -- @U+030E@ COMBINING DOUBLE VERTICAL LINE ABOVE: ◌̎
verticalLineBelow = '\x0329' -- @U+0329@ COMBINING VERTICAL LINE BELOW: ◌̩
doubledVerticalLineBelow = '\x0348' -- @U+0348@ COMBINING DOUBLE VERTICAL LINE BELOW: ◌͈
shortSolidus, longSolidus, reverseLongSolidus, doubledLongSolidus :: Char
shortSolidus = '\x0337' -- @U+0337@ COMBINING SHORT SOLIDUS OVERLAY: ◌̷
longSolidus = '\x0338' -- @U+0338@ COMBINING LONG SOLIDUS OVERLAY: ◌̸
reverseLongSolidus = '\x20e5' -- @U+20E5@ COMBINING REVERSE SOLIDUS OVERLAY: ◌⃥
doubledLongSolidus = '\x20eb' -- @U+20EB@ COMBINING LONG DOUBLE SOLIDUS OVERLAY: ◌⃫
tildeOverlay :: Char
tildeOverlay = '\x0334' -- @U+0334@ COMBINING TILDE OVERLAY: ◌̴

-- Diacritics: Rings------------------------------------------------------------
ringAbove, ringBelow, doubledRingBelow :: Char
ringAbove = '\x030a' -- @U+030A@ COMBINING RING ABOVE: ◌̊
ringBelow = '\x0325' -- @U+0325@ COMBINING RING BELOW: ◌̥
doubledRingBelow = '\x035a' -- @U+035A@ COMBINING DOUBLE RING BELOW: ◌͚

-- Diacritics: Curls------------------------------------------------------------
cedillaBelow, cedillaAbove :: Char
cedillaBelow = '\x0327' -- @U+0327@ COMBINING CEDILLA: ◌̧
cedillaAbove = '\x0312' -- @U+0312@ COMBINING TURNED COMMA ABOVE: ◌̒. Alias of 'turnedCommaAbove'
commaAbove, commaAboveRight, reversedCommaAbove, commaBelow, turnedCommaAbove :: Char
commaAbove = '\x0313' -- @U+0313@ COMBINING COMMA ABOVE: ◌̓
commaAboveRight = '\x0315' -- @U+0315@ COMBINING COMMA ABOVE RIGHT: ◌̕
reversedCommaAbove = '\x0314' -- @U+0314@ COMBINING REVERSED COMMA ABOVE: ◌̔
commaBelow = '\x0326' -- @U+0326@ COMBINING COMMA BELOW: ◌̦
turnedCommaAbove = '\x0312' -- @U+0312@ COMBINING TURNED COMMA ABOVE: ◌̒
ogonek :: Char
ogonek = '\x0328' -- @U+0328@ COMBINING OGONEK: ◌̨

-- Diacritics: Miscellaneous----------------------------------------------------
plusSignBelow, minusSignBelow :: Char
plusSignBelow = '\x031f' -- @U+031F@ COMBINING PLUS SIGN BELOW: ◌̟
minusSignBelow = '\x0320' -- @U+0320@ COMBINING MINUS SIGN BELOW: ◌̠

-- Symbols: Overlay-------------------------------------------------------------
ringOverlay, clockwiseRingOverlay, anticlockwiseRingOverlay :: Char
ringOverlay = '\x20d8' -- @U+20D8@ COMBINING RING OVERLAY: ◌⃘
clockwiseRingOverlay = '\x20d9' -- @U+20D9@ COMBINING CLOCKWISE RING OVERLAY: ◌⃙
anticlockwiseRingOverlay = '\x20da' -- @U+20DA@ COMBINING ANTICLOCKWISE RING OVERLAY: ◌⃚

-- Symbols: Enclosing-----------------------------------------------------------
enclosingCircle, enclosingSquare, enclosingDiamond :: Char
enclosingCircle = '\x20dd' -- @U+20DD@ COMBINING ENCLOSING CIRCLE: ◌⃝
enclosingSquare = '\x20de' -- @U+20DE@ COMBINING ENCLOSING SQUARE: ◌⃞
enclosingDiamond = '\x20df' -- @U+20DF@ COMBINING ENCLOSING DIAMOND: ◌⃟
enclosingCircleBackslash, enclosingKeycap, enclosingTriangle :: Char
enclosingCircleBackslash = '\x20e0' -- @U+20E0@ COMBINING ENCLOSING CIRCLE BACKSLASH: ◌⃠
enclosingKeycap = '\x20e3' -- @U+20E3@ COMBINING ENCLOSING KEYCAP: ◌⃣
enclosingTriangle = '\x20e4' -- @U+20E4@ COMBINING ENCLOSING UPWARD POINTING TRIANGLE: ◌⃤

-- Symbols: Arrow---------------------------------------------------------------
clockwiseArrowAbove, anticlockwiseArrowAbove :: Char
clockwiseArrowAbove = '\x20d5' -- @U+20D5@ COMBINING CLOCKWISE ARROW ABOVE: ◌⃕
anticlockwiseArrowAbove = '\x20d4' -- @U+20D4@ COMBINING ANTICLOCKWISE ARROW ABOVE: ◌⃔
leftwardsArrowAbove, leftwardsArrowBelow :: Char
leftwardsArrowAbove = '\x20d6' -- @U+20D6@ COMBINING LEFT ARROW ABOVE: ◌⃖
leftwardsArrowBelow = '\x20ee' -- @U+20EE@ COMBINING LEFT ARROW BELOW: ◌⃮
rightwardsArrowAbove, rightwardsArrowBelow, doubleRightwardsArrowBelow :: Char
rightwardsArrowAbove = '\x20d7' -- @U+20D7@ COMBINING RIGHT ARROW ABOVE: ◌⃗
rightwardsArrowBelow = '\x20ef' -- @U+20EF@ COMBINING RIGHT ARROW BELOW: ◌⃯
doubleRightwardsArrowBelow = '\x0362' -- @U+0362@ COMBINING DOUBLE RIGHTWARDS ARROW BELOW: ◌͢◌

-- Symbols: Miscellaneous-------------------------------------------------------
asteriskAbove, asteriskBelow :: Char
asteriskAbove = '\x20f0' -- @U+20F0@ COMBINING ASTERISK ABOVE: ◌⃰
asteriskBelow = '\x0359' -- @U+0359@ COMBINING ASTERISK BELOW: ◌͙
infinityAbove, wigglyLineBelow :: Char
infinityAbove = '\x1ab2' -- @U+1AB2@ COMBINING INFINITY: ◌᪲
wigglyLineBelow = '\x1ab6' -- @U+1AB6@ COMBINING WIGGLY LINE BELOW: ◌᪶

-- Block: Combining Diacritical Marks-------------------------------------------
graveAccent, acuteAccent, circumflexAccent, tilde, macron, breve, diaeresis, doubleAcuteAccent, caron, doubleVerticalLineAbove, doubleGraveAccent, candrabindu, invertedBreve, graveAccentBelow, acuteAccentBelow, leftTackBelow, rightTackBelow, leftAngleAbove, leftHalfRingBelow, upTackBelow, downTackBelow, cedilla, bridgeBelow, invertedDoubleArchBelow, circumflexAccentBelow, shortStrokeOverlay, longStrokeOverlay, shortSolidusOverlay, longSolidusOverlay, rightHalfRingBelow, invertedBridgeBelow, squareBelow, seagullBelow, xAbove, verticalTilde, graveToneMark, acuteToneMark, greekPerispomeni, greekKoronis, greekDialytikaTonos, greekYpogegrammeni, bridgeAbove, equalsSignBelow, doubleVerticalLineBelow, leftAngleBelow, notTildeAbove, homotheticAbove, almostEqualToAbove, leftRightArrowBelow, upwardsArrowBelow, rightArrowheadAbove, leftHalfRingAbove, fermata, xBelow, leftArrowheadBelow, rightArrowheadBelow, rightArrowheadAndUpArrowheadBelow, rightHalfRingAbove, dotAboveRight, doubleRingBelow, zigzagAbove, doubleBreve, doubleMacron, doubleTilde, doubleInvertedBreve, latinSmallLetterA, latinSmallLetterE, latinSmallLetterI, latinSmallLetterO, latinSmallLetterU, latinSmallLetterC, latinSmallLetterD, latinSmallLetterH, latinSmallLetterM, latinSmallLetterR, latinSmallLetterT, latinSmallLetterV, latinSmallLetterX :: Char
graveAccent = '\x0300' -- @U+0300@ COMBINING GRAVE ACCENT: ◌̀. Alias of 'graveAbove'
acuteAccent = '\x0301' -- @U+0301@ COMBINING ACUTE ACCENT: ◌́. Alias of 'acuteAbove'
circumflexAccent = '\x0302' -- @U+0302@ COMBINING CIRCUMFLEX ACCENT: ◌̂. Alias of 'circumflexAbove'
tilde = '\x0303' -- @U+0303@ COMBINING TILDE: ◌̃. Alias of 'tildeAbove'
macron = '\x0304' -- @U+0304@ COMBINING MACRON: ◌̄. Alias of 'macronAbove'
breve = '\x0306' -- @U+0306@ COMBINING BREVE: ◌̆. Alias of 'breveAbove'
diaeresis = '\x0308' -- @U+0308@ COMBINING DIAERESIS: ◌̈. Alias of 'diaeresisAbove'
doubleAcuteAccent = '\x030b' -- @U+030B@ COMBINING DOUBLE ACUTE ACCENT: ◌̋. Alias of 'doubledAcute'
caron = '\x030c' -- @U+030C@ COMBINING CARON: ◌̌. Alias of 'caronAbove'
doubleVerticalLineAbove = '\x030e' -- @U+030E@ COMBINING DOUBLE VERTICAL LINE ABOVE: ◌̎. Alias of 'doubledVerticalLineAbove'
doubleGraveAccent = '\x030f' -- @U+030F@ COMBINING DOUBLE GRAVE ACCENT: ◌̏. Alias of 'doubledGrave'
candrabindu = '\x0310' -- @U+0310@ COMBINING CANDRABINDU: ◌̐
invertedBreve = '\x0311' -- @U+0311@ COMBINING INVERTED BREVE: ◌̑. Alias of 'invertedBreveAbove'
graveAccentBelow = '\x0316' -- @U+0316@ COMBINING GRAVE ACCENT BELOW: ◌̖. Alias of 'graveBelow'
acuteAccentBelow = '\x0317' -- @U+0317@ COMBINING ACUTE ACCENT BELOW: ◌̗. Alias of 'acuteBelow'
leftTackBelow = '\x0318' -- @U+0318@ COMBINING LEFT TACK BELOW: ◌̘
rightTackBelow = '\x0319' -- @U+0319@ COMBINING RIGHT TACK BELOW: ◌̙
leftAngleAbove = '\x031a' -- @U+031A@ COMBINING LEFT ANGLE ABOVE: ◌̚
leftHalfRingBelow = '\x031c' -- @U+031C@ COMBINING LEFT HALF RING BELOW: ◌̜
upTackBelow = '\x031d' -- @U+031D@ COMBINING UP TACK BELOW: ◌̝
downTackBelow = '\x031e' -- @U+031E@ COMBINING DOWN TACK BELOW: ◌̞
cedilla = '\x0327' -- @U+0327@ COMBINING CEDILLA: ◌̧. Alias of 'cedillaBelow'
bridgeBelow = '\x032a' -- @U+032A@ COMBINING BRIDGE BELOW: ◌̪
invertedDoubleArchBelow = '\x032b' -- @U+032B@ COMBINING INVERTED DOUBLE ARCH BELOW: ◌̫
circumflexAccentBelow = '\x032d' -- @U+032D@ COMBINING CIRCUMFLEX ACCENT BELOW: ◌̭. Alias of 'circumflexBelow'
shortStrokeOverlay = '\x0335' -- @U+0335@ COMBINING SHORT STROKE OVERLAY: ◌̵. Alias of 'shortStroke'
longStrokeOverlay = '\x0336' -- @U+0336@ COMBINING LONG STROKE OVERLAY: ◌̶. Alias of 'longStroke'
shortSolidusOverlay = '\x0337' -- @U+0337@ COMBINING SHORT SOLIDUS OVERLAY: ◌̷. Alias of 'shortSolidus'
longSolidusOverlay = '\x0338' -- @U+0338@ COMBINING LONG SOLIDUS OVERLAY: ◌̸. Alias of 'longSolidus'
rightHalfRingBelow = '\x0339' -- @U+0339@ COMBINING RIGHT HALF RING BELOW: ◌̹
invertedBridgeBelow = '\x033a' -- @U+033A@ COMBINING INVERTED BRIDGE BELOW: ◌̺
squareBelow = '\x033b' -- @U+033B@ COMBINING SQUARE BELOW: ◌̻
seagullBelow = '\x033c' -- @U+033C@ COMBINING SEAGULL BELOW: ◌̼
xAbove = '\x033d' -- @U+033D@ COMBINING X ABOVE: ◌̽
verticalTilde = '\x033e' -- @U+033E@ COMBINING VERTICAL TILDE: ◌̾
graveToneMark = '\x0340' -- @U+0340@ COMBINING GRAVE TONE MARK: ◌̀, equivalent to: @U+0300@
acuteToneMark = '\x0341' -- @U+0341@ COMBINING ACUTE TONE MARK: ◌́, equivalent to: @U+0301@
greekPerispomeni = '\x0342' -- @U+0342@ COMBINING GREEK PERISPOMENI: ◌͂
greekKoronis = '\x0343' -- @U+0343@ COMBINING GREEK KORONIS: ◌̓, equivalent to: @U+0313@
greekDialytikaTonos = '\x0344' -- @U+0344@ COMBINING GREEK DIALYTIKA TONOS: ◌̈́, equivalent to: @U+0308 + U+0301@
greekYpogegrammeni = '\x0345' -- @U+0345@ COMBINING GREEK YPOGEGRAMMENI: ◌ͅ
bridgeAbove = '\x0346' -- @U+0346@ COMBINING BRIDGE ABOVE: ◌͆
equalsSignBelow = '\x0347' -- @U+0347@ COMBINING EQUALS SIGN BELOW: ◌͇
doubleVerticalLineBelow = '\x0348' -- @U+0348@ COMBINING DOUBLE VERTICAL LINE BELOW: ◌͈. Alias of 'doubledVerticalLineBelow'
leftAngleBelow = '\x0349' -- @U+0349@ COMBINING LEFT ANGLE BELOW: ◌͉
notTildeAbove = '\x034a' -- @U+034A@ COMBINING NOT TILDE ABOVE: ◌͊
homotheticAbove = '\x034b' -- @U+034B@ COMBINING HOMOTHETIC ABOVE: ◌͋
almostEqualToAbove = '\x034c' -- @U+034C@ COMBINING ALMOST EQUAL TO ABOVE: ◌͌
leftRightArrowBelow = '\x034d' -- @U+034D@ COMBINING LEFT RIGHT ARROW BELOW: ◌͍
upwardsArrowBelow = '\x034e' -- @U+034E@ COMBINING UPWARDS ARROW BELOW: ◌͎
rightArrowheadAbove = '\x0350' -- @U+0350@ COMBINING RIGHT ARROWHEAD ABOVE: ◌͐
leftHalfRingAbove = '\x0351' -- @U+0351@ COMBINING LEFT HALF RING ABOVE: ◌͑
fermata = '\x0352' -- @U+0352@ COMBINING FERMATA: ◌͒
xBelow = '\x0353' -- @U+0353@ COMBINING X BELOW: ◌͓
leftArrowheadBelow = '\x0354' -- @U+0354@ COMBINING LEFT ARROWHEAD BELOW: ◌͔
rightArrowheadBelow = '\x0355' -- @U+0355@ COMBINING RIGHT ARROWHEAD BELOW: ◌͕
rightArrowheadAndUpArrowheadBelow = '\x0356' -- @U+0356@ COMBINING RIGHT ARROWHEAD AND UP ARROWHEAD BELOW: ◌͖
rightHalfRingAbove = '\x0357' -- @U+0357@ COMBINING RIGHT HALF RING ABOVE: ◌͗
dotAboveRight = '\x0358' -- @U+0358@ COMBINING DOT ABOVE RIGHT: ◌͘
doubleRingBelow = '\x035a' -- @U+035A@ COMBINING DOUBLE RING BELOW: ◌͚. Alias of 'doubledRingBelow'
zigzagAbove = '\x035b' -- @U+035B@ COMBINING ZIGZAG ABOVE: ◌͛
doubleBreve = '\x035d' -- @U+035D@ COMBINING DOUBLE BREVE: ◌͝. Alias of 'doubleBreveAbove'
doubleMacron = '\x035e' -- @U+035E@ COMBINING DOUBLE MACRON: ◌͞. Alias of 'doubleMacronAbove'
doubleTilde = '\x0360' -- @U+0360@ COMBINING DOUBLE TILDE: ◌͠. Alias of 'doubleTildeAbove'
doubleInvertedBreve = '\x0361' -- @U+0361@ COMBINING DOUBLE INVERTED BREVE: ◌͡. Alias of 'doubleInvertedBreveAbove'
latinSmallLetterA = '\x0363' -- @U+0363@ COMBINING LATIN SMALL LETTER A: ◌ͣ
latinSmallLetterE = '\x0364' -- @U+0364@ COMBINING LATIN SMALL LETTER E: ◌ͤ
latinSmallLetterI = '\x0365' -- @U+0365@ COMBINING LATIN SMALL LETTER I: ◌ͥ
latinSmallLetterO = '\x0366' -- @U+0366@ COMBINING LATIN SMALL LETTER O: ◌ͦ
latinSmallLetterU = '\x0367' -- @U+0367@ COMBINING LATIN SMALL LETTER U: ◌ͧ
latinSmallLetterC = '\x0368' -- @U+0368@ COMBINING LATIN SMALL LETTER C: ◌ͨ
latinSmallLetterD = '\x0369' -- @U+0369@ COMBINING LATIN SMALL LETTER D: ◌ͩ
latinSmallLetterH = '\x036a' -- @U+036A@ COMBINING LATIN SMALL LETTER H: ◌ͪ
latinSmallLetterM = '\x036b' -- @U+036B@ COMBINING LATIN SMALL LETTER M: ◌ͫ
latinSmallLetterR = '\x036c' -- @U+036C@ COMBINING LATIN SMALL LETTER R: ◌ͬ
latinSmallLetterT = '\x036d' -- @U+036D@ COMBINING LATIN SMALL LETTER T: ◌ͭ
latinSmallLetterV = '\x036e' -- @U+036E@ COMBINING LATIN SMALL LETTER V: ◌ͮ
latinSmallLetterX = '\x036f' -- @U+036F@ COMBINING LATIN SMALL LETTER X: ◌ͯ

-- Block: Cyrillic--------------------------------------------------------------
cyrillicTitlo, cyrillicPalatalization, cyrillicDasiaPneumata, cyrillicPsiliPneumata, cyrillicPokrytie :: Char
cyrillicTitlo = '\x0483' -- @U+0483@ COMBINING CYRILLIC TITLO: ◌҃
cyrillicPalatalization = '\x0484' -- @U+0484@ COMBINING CYRILLIC PALATALIZATION: ◌҄
cyrillicDasiaPneumata = '\x0485' -- @U+0485@ COMBINING CYRILLIC DASIA PNEUMATA: ◌҅
cyrillicPsiliPneumata = '\x0486' -- @U+0486@ COMBINING CYRILLIC PSILI PNEUMATA: ◌҆
cyrillicPokrytie = '\x0487' -- @U+0487@ COMBINING CYRILLIC POKRYTIE: ◌҇

-- Block: Hebrew----------------------------------------------------------------
hebrewAccentEtnahta, hebrewAccentSegol, hebrewAccentShalshelet, hebrewAccentZaqefQatan, hebrewAccentZaqefGadol, hebrewAccentTipeha, hebrewAccentRevia, hebrewAccentZarqa, hebrewAccentPashta, hebrewAccentYetiv, hebrewAccentTevir, hebrewAccentGeresh, hebrewAccentGereshMuqdam, hebrewAccentGershayim, hebrewAccentQarneyPara, hebrewAccentTelishaGedola, hebrewAccentPazer, hebrewAccentAtnahHafukh, hebrewAccentMunah, hebrewAccentMahapakh, hebrewAccentMerkha, hebrewAccentMerkhaKefula, hebrewAccentDarga, hebrewAccentQadma, hebrewAccentTelishaQetana, hebrewAccentYerahBenYomo, hebrewAccentOle, hebrewAccentIluy, hebrewAccentDehi, hebrewAccentZinor, hebrewMarkMasoraCircle, hebrewPointSheva, hebrewPointHatafSegol, hebrewPointHatafPatah, hebrewPointHatafQamats, hebrewPointHiriq, hebrewPointTsere, hebrewPointSegol, hebrewPointPatah, hebrewPointQamats, hebrewPointHolam, hebrewPointHolamHaserForVav, hebrewPointQubuts, hebrewPointDageshOrMapiq, hebrewPointMeteg, hebrewPointRafe, hebrewPointShinDot, hebrewPointSinDot, hebrewMarkUpperDot, hebrewMarkLowerDot, hebrewPointQamatsQatan :: Char
hebrewAccentEtnahta = '\x0591' -- @U+0591@ HEBREW ACCENT ETNAHTA: ◌֑
hebrewAccentSegol = '\x0592' -- @U+0592@ HEBREW ACCENT SEGOL: ◌֒
hebrewAccentShalshelet = '\x0593' -- @U+0593@ HEBREW ACCENT SHALSHELET: ◌֓
hebrewAccentZaqefQatan = '\x0594' -- @U+0594@ HEBREW ACCENT ZAQEF QATAN: ◌֔
hebrewAccentZaqefGadol = '\x0595' -- @U+0595@ HEBREW ACCENT ZAQEF GADOL: ◌֕
hebrewAccentTipeha = '\x0596' -- @U+0596@ HEBREW ACCENT TIPEHA: ◌֖
hebrewAccentRevia = '\x0597' -- @U+0597@ HEBREW ACCENT REVIA: ◌֗
hebrewAccentZarqa = '\x0598' -- @U+0598@ HEBREW ACCENT ZARQA: ◌֘
hebrewAccentPashta = '\x0599' -- @U+0599@ HEBREW ACCENT PASHTA: ◌֙
hebrewAccentYetiv = '\x059a' -- @U+059A@ HEBREW ACCENT YETIV: ◌֚
hebrewAccentTevir = '\x059b' -- @U+059B@ HEBREW ACCENT TEVIR: ◌֛
hebrewAccentGeresh = '\x059c' -- @U+059C@ HEBREW ACCENT GERESH: ◌֜
hebrewAccentGereshMuqdam = '\x059d' -- @U+059D@ HEBREW ACCENT GERESH MUQDAM: ◌֝
hebrewAccentGershayim = '\x059e' -- @U+059E@ HEBREW ACCENT GERSHAYIM: ◌֞
hebrewAccentQarneyPara = '\x059f' -- @U+059F@ HEBREW ACCENT QARNEY PARA: ◌֟
hebrewAccentTelishaGedola = '\x05a0' -- @U+05A0@ HEBREW ACCENT TELISHA GEDOLA: ◌֠
hebrewAccentPazer = '\x05a1' -- @U+05A1@ HEBREW ACCENT PAZER: ◌֡
hebrewAccentAtnahHafukh = '\x05a2' -- @U+05A2@ HEBREW ACCENT ATNAH HAFUKH: ◌֢
hebrewAccentMunah = '\x05a3' -- @U+05A3@ HEBREW ACCENT MUNAH: ◌֣
hebrewAccentMahapakh = '\x05a4' -- @U+05A4@ HEBREW ACCENT MAHAPAKH: ◌֤
hebrewAccentMerkha = '\x05a5' -- @U+05A5@ HEBREW ACCENT MERKHA: ◌֥
hebrewAccentMerkhaKefula = '\x05a6' -- @U+05A6@ HEBREW ACCENT MERKHA KEFULA: ◌֦
hebrewAccentDarga = '\x05a7' -- @U+05A7@ HEBREW ACCENT DARGA: ◌֧
hebrewAccentQadma = '\x05a8' -- @U+05A8@ HEBREW ACCENT QADMA: ◌֨
hebrewAccentTelishaQetana = '\x05a9' -- @U+05A9@ HEBREW ACCENT TELISHA QETANA: ◌֩
hebrewAccentYerahBenYomo = '\x05aa' -- @U+05AA@ HEBREW ACCENT YERAH BEN YOMO: ◌֪
hebrewAccentOle = '\x05ab' -- @U+05AB@ HEBREW ACCENT OLE: ◌֫
hebrewAccentIluy = '\x05ac' -- @U+05AC@ HEBREW ACCENT ILUY: ◌֬
hebrewAccentDehi = '\x05ad' -- @U+05AD@ HEBREW ACCENT DEHI: ◌֭
hebrewAccentZinor = '\x05ae' -- @U+05AE@ HEBREW ACCENT ZINOR: ◌֮
hebrewMarkMasoraCircle = '\x05af' -- @U+05AF@ HEBREW MARK MASORA CIRCLE: ◌֯
hebrewPointSheva = '\x05b0' -- @U+05B0@ HEBREW POINT SHEVA: ◌ְ
hebrewPointHatafSegol = '\x05b1' -- @U+05B1@ HEBREW POINT HATAF SEGOL: ◌ֱ
hebrewPointHatafPatah = '\x05b2' -- @U+05B2@ HEBREW POINT HATAF PATAH: ◌ֲ
hebrewPointHatafQamats = '\x05b3' -- @U+05B3@ HEBREW POINT HATAF QAMATS: ◌ֳ
hebrewPointHiriq = '\x05b4' -- @U+05B4@ HEBREW POINT HIRIQ: ◌ִ
hebrewPointTsere = '\x05b5' -- @U+05B5@ HEBREW POINT TSERE: ◌ֵ
hebrewPointSegol = '\x05b6' -- @U+05B6@ HEBREW POINT SEGOL: ◌ֶ
hebrewPointPatah = '\x05b7' -- @U+05B7@ HEBREW POINT PATAH: ◌ַ
hebrewPointQamats = '\x05b8' -- @U+05B8@ HEBREW POINT QAMATS: ◌ָ
hebrewPointHolam = '\x05b9' -- @U+05B9@ HEBREW POINT HOLAM: ◌ֹ
hebrewPointHolamHaserForVav = '\x05ba' -- @U+05BA@ HEBREW POINT HOLAM HASER FOR VAV: ◌ֺ
hebrewPointQubuts = '\x05bb' -- @U+05BB@ HEBREW POINT QUBUTS: ◌ֻ
hebrewPointDageshOrMapiq = '\x05bc' -- @U+05BC@ HEBREW POINT DAGESH OR MAPIQ: ◌ּ
hebrewPointMeteg = '\x05bd' -- @U+05BD@ HEBREW POINT METEG: ◌ֽ
hebrewPointRafe = '\x05bf' -- @U+05BF@ HEBREW POINT RAFE: ◌ֿ
hebrewPointShinDot = '\x05c1' -- @U+05C1@ HEBREW POINT SHIN DOT: ◌ׁ
hebrewPointSinDot = '\x05c2' -- @U+05C2@ HEBREW POINT SIN DOT: ◌ׂ
hebrewMarkUpperDot = '\x05c4' -- @U+05C4@ HEBREW MARK UPPER DOT: ◌ׄ
hebrewMarkLowerDot = '\x05c5' -- @U+05C5@ HEBREW MARK LOWER DOT: ◌ׅ
hebrewPointQamatsQatan = '\x05c7' -- @U+05C7@ HEBREW POINT QAMATS QATAN: ◌ׇ

-- Block: Arabic----------------------------------------------------------------
arabicSignSallallahouAlayheWassallam, arabicSignAlayheAssallam, arabicSignRahmatullahAlayhe, arabicSignRadiAllahouAnhu, arabicSignTakhallus, arabicSmallHighTah, arabicSmallHighLigatureAlefWithLamWithYeh, arabicSmallHighZain, arabicSmallFatha, arabicSmallDamma, arabicSmallKasra, arabicFathatan, arabicDammatan, arabicKasratan, arabicFatha, arabicDamma, arabicKasra, arabicShadda, arabicSukun, arabicMaddahAbove, arabicHamzaAbove, arabicHamzaBelow, arabicSubscriptAlef, arabicInvertedDamma, arabicMarkNoonGhunna, arabicZwarakay, arabicVowelSignSmallVAbove, arabicVowelSignInvertedSmallVAbove, arabicVowelSignDotBelow, arabicReversedDamma, arabicFathaWithTwoDots, arabicWavyHamzaBelow, arabicLetterSuperscriptAlef, arabicSmallHighLigatureSadWithLamWithAlefMaksura, arabicSmallHighLigatureQafWithLamWithAlefMaksura, arabicSmallHighMeemInitialForm, arabicSmallHighLamAlef, arabicSmallHighJeem, arabicSmallHighThreeDots, arabicSmallHighSeen, arabicSmallHighRoundedZero, arabicSmallHighUprightRectangularZero, arabicSmallHighDotlessHeadOfKhah, arabicSmallHighMeemIsolatedForm, arabicSmallLowSeen, arabicSmallHighMadda, arabicSmallHighYeh, arabicSmallHighNoon, arabicEmptyCentreLowStop, arabicEmptyCentreHighStop, arabicRoundedHighStopWithFilledCentre, arabicSmallLowMeem :: Char
arabicSignSallallahouAlayheWassallam = '\x0610' -- @U+0610@ ARABIC SIGN SALLALLAHOU ALAYHE WASSALLAM: ◌ؐ
arabicSignAlayheAssallam = '\x0611' -- @U+0611@ ARABIC SIGN ALAYHE ASSALLAM: ◌ؑ
arabicSignRahmatullahAlayhe = '\x0612' -- @U+0612@ ARABIC SIGN RAHMATULLAH ALAYHE: ◌ؒ
arabicSignRadiAllahouAnhu = '\x0613' -- @U+0613@ ARABIC SIGN RADI ALLAHOU ANHU: ◌ؓ
arabicSignTakhallus = '\x0614' -- @U+0614@ ARABIC SIGN TAKHALLUS: ◌ؔ
arabicSmallHighTah = '\x0615' -- @U+0615@ ARABIC SMALL HIGH TAH: ◌ؕ
arabicSmallHighLigatureAlefWithLamWithYeh = '\x0616' -- @U+0616@ ARABIC SMALL HIGH LIGATURE ALEF WITH LAM WITH YEH: ◌ؖ
arabicSmallHighZain = '\x0617' -- @U+0617@ ARABIC SMALL HIGH ZAIN: ◌ؗ
arabicSmallFatha = '\x0618' -- @U+0618@ ARABIC SMALL FATHA: ◌ؘ
arabicSmallDamma = '\x0619' -- @U+0619@ ARABIC SMALL DAMMA: ◌ؙ
arabicSmallKasra = '\x061a' -- @U+061A@ ARABIC SMALL KASRA: ◌ؚ
arabicFathatan = '\x064b' -- @U+064B@ ARABIC FATHATAN: ◌ً
arabicDammatan = '\x064c' -- @U+064C@ ARABIC DAMMATAN: ◌ٌ
arabicKasratan = '\x064d' -- @U+064D@ ARABIC KASRATAN: ◌ٍ
arabicFatha = '\x064e' -- @U+064E@ ARABIC FATHA: ◌َ
arabicDamma = '\x064f' -- @U+064F@ ARABIC DAMMA: ◌ُ
arabicKasra = '\x0650' -- @U+0650@ ARABIC KASRA: ◌ِ
arabicShadda = '\x0651' -- @U+0651@ ARABIC SHADDA: ◌ّ
arabicSukun = '\x0652' -- @U+0652@ ARABIC SUKUN: ◌ْ
arabicMaddahAbove = '\x0653' -- @U+0653@ ARABIC MADDAH ABOVE: ◌ٓ
arabicHamzaAbove = '\x0654' -- @U+0654@ ARABIC HAMZA ABOVE: ◌ٔ
arabicHamzaBelow = '\x0655' -- @U+0655@ ARABIC HAMZA BELOW: ◌ٕ
arabicSubscriptAlef = '\x0656' -- @U+0656@ ARABIC SUBSCRIPT ALEF: ◌ٖ
arabicInvertedDamma = '\x0657' -- @U+0657@ ARABIC INVERTED DAMMA: ◌ٗ
arabicMarkNoonGhunna = '\x0658' -- @U+0658@ ARABIC MARK NOON GHUNNA: ◌٘
arabicZwarakay = '\x0659' -- @U+0659@ ARABIC ZWARAKAY: ◌ٙ
arabicVowelSignSmallVAbove = '\x065a' -- @U+065A@ ARABIC VOWEL SIGN SMALL V ABOVE: ◌ٚ
arabicVowelSignInvertedSmallVAbove = '\x065b' -- @U+065B@ ARABIC VOWEL SIGN INVERTED SMALL V ABOVE: ◌ٛ
arabicVowelSignDotBelow = '\x065c' -- @U+065C@ ARABIC VOWEL SIGN DOT BELOW: ◌ٜ
arabicReversedDamma = '\x065d' -- @U+065D@ ARABIC REVERSED DAMMA: ◌ٝ
arabicFathaWithTwoDots = '\x065e' -- @U+065E@ ARABIC FATHA WITH TWO DOTS: ◌ٞ
arabicWavyHamzaBelow = '\x065f' -- @U+065F@ ARABIC WAVY HAMZA BELOW: ◌ٟ
arabicLetterSuperscriptAlef = '\x0670' -- @U+0670@ ARABIC LETTER SUPERSCRIPT ALEF: ◌ٰ
arabicSmallHighLigatureSadWithLamWithAlefMaksura = '\x06d6' -- @U+06D6@ ARABIC SMALL HIGH LIGATURE SAD WITH LAM WITH ALEF MAKSURA: ◌ۖ
arabicSmallHighLigatureQafWithLamWithAlefMaksura = '\x06d7' -- @U+06D7@ ARABIC SMALL HIGH LIGATURE QAF WITH LAM WITH ALEF MAKSURA: ◌ۗ
arabicSmallHighMeemInitialForm = '\x06d8' -- @U+06D8@ ARABIC SMALL HIGH MEEM INITIAL FORM: ◌ۘ
arabicSmallHighLamAlef = '\x06d9' -- @U+06D9@ ARABIC SMALL HIGH LAM ALEF: ◌ۙ
arabicSmallHighJeem = '\x06da' -- @U+06DA@ ARABIC SMALL HIGH JEEM: ◌ۚ
arabicSmallHighThreeDots = '\x06db' -- @U+06DB@ ARABIC SMALL HIGH THREE DOTS: ◌ۛ
arabicSmallHighSeen = '\x06dc' -- @U+06DC@ ARABIC SMALL HIGH SEEN: ◌ۜ
arabicSmallHighRoundedZero = '\x06df' -- @U+06DF@ ARABIC SMALL HIGH ROUNDED ZERO: ◌۟
arabicSmallHighUprightRectangularZero = '\x06e0' -- @U+06E0@ ARABIC SMALL HIGH UPRIGHT RECTANGULAR ZERO: ◌۠
arabicSmallHighDotlessHeadOfKhah = '\x06e1' -- @U+06E1@ ARABIC SMALL HIGH DOTLESS HEAD OF KHAH: ◌ۡ
arabicSmallHighMeemIsolatedForm = '\x06e2' -- @U+06E2@ ARABIC SMALL HIGH MEEM ISOLATED FORM: ◌ۢ
arabicSmallLowSeen = '\x06e3' -- @U+06E3@ ARABIC SMALL LOW SEEN: ◌ۣ
arabicSmallHighMadda = '\x06e4' -- @U+06E4@ ARABIC SMALL HIGH MADDA: ◌ۤ
arabicSmallHighYeh = '\x06e7' -- @U+06E7@ ARABIC SMALL HIGH YEH: ◌ۧ
arabicSmallHighNoon = '\x06e8' -- @U+06E8@ ARABIC SMALL HIGH NOON: ◌ۨ
arabicEmptyCentreLowStop = '\x06ea' -- @U+06EA@ ARABIC EMPTY CENTRE LOW STOP: ◌۪
arabicEmptyCentreHighStop = '\x06eb' -- @U+06EB@ ARABIC EMPTY CENTRE HIGH STOP: ◌۫
arabicRoundedHighStopWithFilledCentre = '\x06ec' -- @U+06EC@ ARABIC ROUNDED HIGH STOP WITH FILLED CENTRE: ◌۬
arabicSmallLowMeem = '\x06ed' -- @U+06ED@ ARABIC SMALL LOW MEEM: ◌ۭ

-- Block: Syriac----------------------------------------------------------------
syriacLetterSuperscriptAlaph, syriacPthahaAbove, syriacPthahaBelow, syriacPthahaDotted, syriacZqaphaAbove, syriacZqaphaBelow, syriacZqaphaDotted, syriacRbasaAbove, syriacRbasaBelow, syriacDottedZlamaHorizontal, syriacDottedZlamaAngular, syriacHbasaAbove, syriacHbasaBelow, syriacHbasaEsasaDotted, syriacEsasaAbove, syriacEsasaBelow, syriacRwaha, syriacFeminineDot, syriacQushshaya, syriacRukkakha, syriacTwoVerticalDotsAbove, syriacTwoVerticalDotsBelow, syriacThreeDotsAbove, syriacThreeDotsBelow, syriacObliqueLineAbove, syriacObliqueLineBelow, syriacMusic, syriacBarrekh :: Char
syriacLetterSuperscriptAlaph = '\x0711' -- @U+0711@ SYRIAC LETTER SUPERSCRIPT ALAPH: ◌ܑ
syriacPthahaAbove = '\x0730' -- @U+0730@ SYRIAC PTHAHA ABOVE: ◌ܰ
syriacPthahaBelow = '\x0731' -- @U+0731@ SYRIAC PTHAHA BELOW: ◌ܱ
syriacPthahaDotted = '\x0732' -- @U+0732@ SYRIAC PTHAHA DOTTED: ◌ܲ
syriacZqaphaAbove = '\x0733' -- @U+0733@ SYRIAC ZQAPHA ABOVE: ◌ܳ
syriacZqaphaBelow = '\x0734' -- @U+0734@ SYRIAC ZQAPHA BELOW: ◌ܴ
syriacZqaphaDotted = '\x0735' -- @U+0735@ SYRIAC ZQAPHA DOTTED: ◌ܵ
syriacRbasaAbove = '\x0736' -- @U+0736@ SYRIAC RBASA ABOVE: ◌ܶ
syriacRbasaBelow = '\x0737' -- @U+0737@ SYRIAC RBASA BELOW: ◌ܷ
syriacDottedZlamaHorizontal = '\x0738' -- @U+0738@ SYRIAC DOTTED ZLAMA HORIZONTAL: ◌ܸ
syriacDottedZlamaAngular = '\x0739' -- @U+0739@ SYRIAC DOTTED ZLAMA ANGULAR: ◌ܹ
syriacHbasaAbove = '\x073a' -- @U+073A@ SYRIAC HBASA ABOVE: ◌ܺ
syriacHbasaBelow = '\x073b' -- @U+073B@ SYRIAC HBASA BELOW: ◌ܻ
syriacHbasaEsasaDotted = '\x073c' -- @U+073C@ SYRIAC HBASA-ESASA DOTTED: ◌ܼ
syriacEsasaAbove = '\x073d' -- @U+073D@ SYRIAC ESASA ABOVE: ◌ܽ
syriacEsasaBelow = '\x073e' -- @U+073E@ SYRIAC ESASA BELOW: ◌ܾ
syriacRwaha = '\x073f' -- @U+073F@ SYRIAC RWAHA: ◌ܿ
syriacFeminineDot = '\x0740' -- @U+0740@ SYRIAC FEMININE DOT: ◌݀
syriacQushshaya = '\x0741' -- @U+0741@ SYRIAC QUSHSHAYA: ◌݁
syriacRukkakha = '\x0742' -- @U+0742@ SYRIAC RUKKAKHA: ◌݂
syriacTwoVerticalDotsAbove = '\x0743' -- @U+0743@ SYRIAC TWO VERTICAL DOTS ABOVE: ◌݃
syriacTwoVerticalDotsBelow = '\x0744' -- @U+0744@ SYRIAC TWO VERTICAL DOTS BELOW: ◌݄
syriacThreeDotsAbove = '\x0745' -- @U+0745@ SYRIAC THREE DOTS ABOVE: ◌݅
syriacThreeDotsBelow = '\x0746' -- @U+0746@ SYRIAC THREE DOTS BELOW: ◌݆
syriacObliqueLineAbove = '\x0747' -- @U+0747@ SYRIAC OBLIQUE LINE ABOVE: ◌݇
syriacObliqueLineBelow = '\x0748' -- @U+0748@ SYRIAC OBLIQUE LINE BELOW: ◌݈
syriacMusic = '\x0749' -- @U+0749@ SYRIAC MUSIC: ◌݉
syriacBarrekh = '\x074a' -- @U+074A@ SYRIAC BARREKH: ◌݊

-- Block: NKo-------------------------------------------------------------------
nkoCombiningShortHighTone, nkoCombiningShortLowTone, nkoCombiningShortRisingTone, nkoCombiningLongDescendingTone, nkoCombiningLongHighTone, nkoCombiningLongLowTone, nkoCombiningLongRisingTone, nkoCombiningNasalizationMark, nkoCombiningDoubleDotAbove, nkoDantayalan :: Char
nkoCombiningShortHighTone = '\x07eb' -- @U+07EB@ NKO COMBINING SHORT HIGH TONE: ◌߫
nkoCombiningShortLowTone = '\x07ec' -- @U+07EC@ NKO COMBINING SHORT LOW TONE: ◌߬
nkoCombiningShortRisingTone = '\x07ed' -- @U+07ED@ NKO COMBINING SHORT RISING TONE: ◌߭
nkoCombiningLongDescendingTone = '\x07ee' -- @U+07EE@ NKO COMBINING LONG DESCENDING TONE: ◌߮
nkoCombiningLongHighTone = '\x07ef' -- @U+07EF@ NKO COMBINING LONG HIGH TONE: ◌߯
nkoCombiningLongLowTone = '\x07f0' -- @U+07F0@ NKO COMBINING LONG LOW TONE: ◌߰
nkoCombiningLongRisingTone = '\x07f1' -- @U+07F1@ NKO COMBINING LONG RISING TONE: ◌߱
nkoCombiningNasalizationMark = '\x07f2' -- @U+07F2@ NKO COMBINING NASALIZATION MARK: ◌߲
nkoCombiningDoubleDotAbove = '\x07f3' -- @U+07F3@ NKO COMBINING DOUBLE DOT ABOVE: ◌߳
nkoDantayalan = '\x07fd' -- @U+07FD@ NKO DANTAYALAN: ◌߽

-- Block: Samaritan-------------------------------------------------------------
samaritanMarkIn, samaritanMarkInAlaf, samaritanMarkOcclusion, samaritanMarkDagesh, samaritanMarkEpentheticYut, samaritanVowelSignLongE, samaritanVowelSignE, samaritanVowelSignOverlongAa, samaritanVowelSignLongAa, samaritanVowelSignAa, samaritanVowelSignOverlongA, samaritanVowelSignLongA, samaritanVowelSignA, samaritanVowelSignShortA, samaritanVowelSignLongU, samaritanVowelSignU, samaritanVowelSignLongI, samaritanVowelSignI, samaritanVowelSignO, samaritanVowelSignSukun, samaritanMarkNequdaa :: Char
samaritanMarkIn = '\x0816' -- @U+0816@ SAMARITAN MARK IN: ◌ࠖ
samaritanMarkInAlaf = '\x0817' -- @U+0817@ SAMARITAN MARK IN-ALAF: ◌ࠗ
samaritanMarkOcclusion = '\x0818' -- @U+0818@ SAMARITAN MARK OCCLUSION: ◌࠘
samaritanMarkDagesh = '\x0819' -- @U+0819@ SAMARITAN MARK DAGESH: ◌࠙
samaritanMarkEpentheticYut = '\x081b' -- @U+081B@ SAMARITAN MARK EPENTHETIC YUT: ◌ࠛ
samaritanVowelSignLongE = '\x081c' -- @U+081C@ SAMARITAN VOWEL SIGN LONG E: ◌ࠜ
samaritanVowelSignE = '\x081d' -- @U+081D@ SAMARITAN VOWEL SIGN E: ◌ࠝ
samaritanVowelSignOverlongAa = '\x081e' -- @U+081E@ SAMARITAN VOWEL SIGN OVERLONG AA: ◌ࠞ
samaritanVowelSignLongAa = '\x081f' -- @U+081F@ SAMARITAN VOWEL SIGN LONG AA: ◌ࠟ
samaritanVowelSignAa = '\x0820' -- @U+0820@ SAMARITAN VOWEL SIGN AA: ◌ࠠ
samaritanVowelSignOverlongA = '\x0821' -- @U+0821@ SAMARITAN VOWEL SIGN OVERLONG A: ◌ࠡ
samaritanVowelSignLongA = '\x0822' -- @U+0822@ SAMARITAN VOWEL SIGN LONG A: ◌ࠢ
samaritanVowelSignA = '\x0823' -- @U+0823@ SAMARITAN VOWEL SIGN A: ◌ࠣ
samaritanVowelSignShortA = '\x0825' -- @U+0825@ SAMARITAN VOWEL SIGN SHORT A: ◌ࠥ
samaritanVowelSignLongU = '\x0826' -- @U+0826@ SAMARITAN VOWEL SIGN LONG U: ◌ࠦ
samaritanVowelSignU = '\x0827' -- @U+0827@ SAMARITAN VOWEL SIGN U: ◌ࠧ
samaritanVowelSignLongI = '\x0829' -- @U+0829@ SAMARITAN VOWEL SIGN LONG I: ◌ࠩ
samaritanVowelSignI = '\x082a' -- @U+082A@ SAMARITAN VOWEL SIGN I: ◌ࠪ
samaritanVowelSignO = '\x082b' -- @U+082B@ SAMARITAN VOWEL SIGN O: ◌ࠫ
samaritanVowelSignSukun = '\x082c' -- @U+082C@ SAMARITAN VOWEL SIGN SUKUN: ◌ࠬ
samaritanMarkNequdaa = '\x082d' -- @U+082D@ SAMARITAN MARK NEQUDAA: ◌࠭

-- Block: Mandaic---------------------------------------------------------------
mandaicAffricationMark, mandaicVocalizationMark, mandaicGeminationMark :: Char
mandaicAffricationMark = '\x0859' -- @U+0859@ MANDAIC AFFRICATION MARK: ◌࡙
mandaicVocalizationMark = '\x085a' -- @U+085A@ MANDAIC VOCALIZATION MARK: ◌࡚
mandaicGeminationMark = '\x085b' -- @U+085B@ MANDAIC GEMINATION MARK: ◌࡛

-- Block: Arabic Extended-A-----------------------------------------------------
arabicSmallLowWaw, arabicSmallHighWordArRub, arabicSmallHighSad, arabicSmallHighAin, arabicSmallHighQaf, arabicSmallHighNoonWithKasra, arabicSmallLowNoonWithKasra, arabicSmallHighWordAthThalatha, arabicSmallHighWordAsSajda, arabicSmallHighWordAnNisf, arabicSmallHighWordSakta, arabicSmallHighWordQif, arabicSmallHighWordWaqfa, arabicSmallHighFootnoteMarker, arabicSmallHighSignSafha, arabicTurnedDammaBelow, arabicCurlyFatha, arabicCurlyDamma, arabicCurlyKasra, arabicCurlyFathatan, arabicCurlyDammatan, arabicCurlyKasratan, arabicToneOneDotAbove, arabicToneTwoDotsAbove, arabicToneLoopAbove, arabicToneOneDotBelow, arabicToneTwoDotsBelow, arabicToneLoopBelow, arabicOpenFathatan, arabicOpenDammatan, arabicOpenKasratan, arabicSmallHighWaw, arabicFathaWithRing, arabicFathaWithDotAbove, arabicKasraWithDotBelow, arabicLeftArrowheadAbove, arabicRightArrowheadAbove, arabicLeftArrowheadBelow, arabicRightArrowheadBelow, arabicDoubleRightArrowheadAbove, arabicDoubleRightArrowheadAboveWithDot, arabicRightArrowheadAboveWithDot, arabicDammaWithDot, arabicMarkSidewaysNoonGhunna :: Char
arabicSmallLowWaw = '\x08d3' -- @U+08D3@ ARABIC SMALL LOW WAW: ◌࣓
arabicSmallHighWordArRub = '\x08d4' -- @U+08D4@ ARABIC SMALL HIGH WORD AR-RUB: ◌ࣔ
arabicSmallHighSad = '\x08d5' -- @U+08D5@ ARABIC SMALL HIGH SAD: ◌ࣕ
arabicSmallHighAin = '\x08d6' -- @U+08D6@ ARABIC SMALL HIGH AIN: ◌ࣖ
arabicSmallHighQaf = '\x08d7' -- @U+08D7@ ARABIC SMALL HIGH QAF: ◌ࣗ
arabicSmallHighNoonWithKasra = '\x08d8' -- @U+08D8@ ARABIC SMALL HIGH NOON WITH KASRA: ◌ࣘ
arabicSmallLowNoonWithKasra = '\x08d9' -- @U+08D9@ ARABIC SMALL LOW NOON WITH KASRA: ◌ࣙ
arabicSmallHighWordAthThalatha = '\x08da' -- @U+08DA@ ARABIC SMALL HIGH WORD ATH-THALATHA: ◌ࣚ
arabicSmallHighWordAsSajda = '\x08db' -- @U+08DB@ ARABIC SMALL HIGH WORD AS-SAJDA: ◌ࣛ
arabicSmallHighWordAnNisf = '\x08dc' -- @U+08DC@ ARABIC SMALL HIGH WORD AN-NISF: ◌ࣜ
arabicSmallHighWordSakta = '\x08dd' -- @U+08DD@ ARABIC SMALL HIGH WORD SAKTA: ◌ࣝ
arabicSmallHighWordQif = '\x08de' -- @U+08DE@ ARABIC SMALL HIGH WORD QIF: ◌ࣞ
arabicSmallHighWordWaqfa = '\x08df' -- @U+08DF@ ARABIC SMALL HIGH WORD WAQFA: ◌ࣟ
arabicSmallHighFootnoteMarker = '\x08e0' -- @U+08E0@ ARABIC SMALL HIGH FOOTNOTE MARKER: ◌࣠
arabicSmallHighSignSafha = '\x08e1' -- @U+08E1@ ARABIC SMALL HIGH SIGN SAFHA: ◌࣡
arabicTurnedDammaBelow = '\x08e3' -- @U+08E3@ ARABIC TURNED DAMMA BELOW: ◌ࣣ
arabicCurlyFatha = '\x08e4' -- @U+08E4@ ARABIC CURLY FATHA: ◌ࣤ
arabicCurlyDamma = '\x08e5' -- @U+08E5@ ARABIC CURLY DAMMA: ◌ࣥ
arabicCurlyKasra = '\x08e6' -- @U+08E6@ ARABIC CURLY KASRA: ◌ࣦ
arabicCurlyFathatan = '\x08e7' -- @U+08E7@ ARABIC CURLY FATHATAN: ◌ࣧ
arabicCurlyDammatan = '\x08e8' -- @U+08E8@ ARABIC CURLY DAMMATAN: ◌ࣨ
arabicCurlyKasratan = '\x08e9' -- @U+08E9@ ARABIC CURLY KASRATAN: ◌ࣩ
arabicToneOneDotAbove = '\x08ea' -- @U+08EA@ ARABIC TONE ONE DOT ABOVE: ◌࣪
arabicToneTwoDotsAbove = '\x08eb' -- @U+08EB@ ARABIC TONE TWO DOTS ABOVE: ◌࣫
arabicToneLoopAbove = '\x08ec' -- @U+08EC@ ARABIC TONE LOOP ABOVE: ◌࣬
arabicToneOneDotBelow = '\x08ed' -- @U+08ED@ ARABIC TONE ONE DOT BELOW: ◌࣭
arabicToneTwoDotsBelow = '\x08ee' -- @U+08EE@ ARABIC TONE TWO DOTS BELOW: ◌࣮
arabicToneLoopBelow = '\x08ef' -- @U+08EF@ ARABIC TONE LOOP BELOW: ◌࣯
arabicOpenFathatan = '\x08f0' -- @U+08F0@ ARABIC OPEN FATHATAN: ◌ࣰ
arabicOpenDammatan = '\x08f1' -- @U+08F1@ ARABIC OPEN DAMMATAN: ◌ࣱ
arabicOpenKasratan = '\x08f2' -- @U+08F2@ ARABIC OPEN KASRATAN: ◌ࣲ
arabicSmallHighWaw = '\x08f3' -- @U+08F3@ ARABIC SMALL HIGH WAW: ◌ࣳ
arabicFathaWithRing = '\x08f4' -- @U+08F4@ ARABIC FATHA WITH RING: ◌ࣴ
arabicFathaWithDotAbove = '\x08f5' -- @U+08F5@ ARABIC FATHA WITH DOT ABOVE: ◌ࣵ
arabicKasraWithDotBelow = '\x08f6' -- @U+08F6@ ARABIC KASRA WITH DOT BELOW: ◌ࣶ
arabicLeftArrowheadAbove = '\x08f7' -- @U+08F7@ ARABIC LEFT ARROWHEAD ABOVE: ◌ࣷ
arabicRightArrowheadAbove = '\x08f8' -- @U+08F8@ ARABIC RIGHT ARROWHEAD ABOVE: ◌ࣸ
arabicLeftArrowheadBelow = '\x08f9' -- @U+08F9@ ARABIC LEFT ARROWHEAD BELOW: ◌ࣹ
arabicRightArrowheadBelow = '\x08fa' -- @U+08FA@ ARABIC RIGHT ARROWHEAD BELOW: ◌ࣺ
arabicDoubleRightArrowheadAbove = '\x08fb' -- @U+08FB@ ARABIC DOUBLE RIGHT ARROWHEAD ABOVE: ◌ࣻ
arabicDoubleRightArrowheadAboveWithDot = '\x08fc' -- @U+08FC@ ARABIC DOUBLE RIGHT ARROWHEAD ABOVE WITH DOT: ◌ࣼ
arabicRightArrowheadAboveWithDot = '\x08fd' -- @U+08FD@ ARABIC RIGHT ARROWHEAD ABOVE WITH DOT: ◌ࣽ
arabicDammaWithDot = '\x08fe' -- @U+08FE@ ARABIC DAMMA WITH DOT: ◌ࣾ
arabicMarkSidewaysNoonGhunna = '\x08ff' -- @U+08FF@ ARABIC MARK SIDEWAYS NOON GHUNNA: ◌ࣿ

-- Block: Devanagari------------------------------------------------------------
devanagariSignNukta, devanagariSignVirama, devanagariStressSignUdatta, devanagariStressSignAnudatta, devanagariGraveAccent, devanagariAcuteAccent :: Char
devanagariSignNukta = '\x093c' -- @U+093C@ DEVANAGARI SIGN NUKTA: ◌़
devanagariSignVirama = '\x094d' -- @U+094D@ DEVANAGARI SIGN VIRAMA: ◌्
devanagariStressSignUdatta = '\x0951' -- @U+0951@ DEVANAGARI STRESS SIGN UDATTA: ◌॑
devanagariStressSignAnudatta = '\x0952' -- @U+0952@ DEVANAGARI STRESS SIGN ANUDATTA: ◌॒
devanagariGraveAccent = '\x0953' -- @U+0953@ DEVANAGARI GRAVE ACCENT: ◌॓
devanagariAcuteAccent = '\x0954' -- @U+0954@ DEVANAGARI ACUTE ACCENT: ◌॔

-- Block: Bengali---------------------------------------------------------------
bengaliSignNukta, bengaliSignVirama, bengaliSandhiMark :: Char
bengaliSignNukta = '\x09bc' -- @U+09BC@ BENGALI SIGN NUKTA: ◌়
bengaliSignVirama = '\x09cd' -- @U+09CD@ BENGALI SIGN VIRAMA: ◌্
bengaliSandhiMark = '\x09fe' -- @U+09FE@ BENGALI SANDHI MARK: ◌৾

-- Block: Gurmukhi--------------------------------------------------------------
gurmukhiSignNukta, gurmukhiSignVirama :: Char
gurmukhiSignNukta = '\x0a3c' -- @U+0A3C@ GURMUKHI SIGN NUKTA: ◌਼
gurmukhiSignVirama = '\x0a4d' -- @U+0A4D@ GURMUKHI SIGN VIRAMA: ◌੍

-- Block: Gujarati--------------------------------------------------------------
gujaratiSignNukta, gujaratiSignVirama :: Char
gujaratiSignNukta = '\x0abc' -- @U+0ABC@ GUJARATI SIGN NUKTA: ◌઼
gujaratiSignVirama = '\x0acd' -- @U+0ACD@ GUJARATI SIGN VIRAMA: ◌્

-- Block: Oriya-----------------------------------------------------------------
oriyaSignNukta, oriyaSignVirama :: Char
oriyaSignNukta = '\x0b3c' -- @U+0B3C@ ORIYA SIGN NUKTA: ◌଼
oriyaSignVirama = '\x0b4d' -- @U+0B4D@ ORIYA SIGN VIRAMA: ◌୍

-- Block: Tamil-----------------------------------------------------------------
tamilSignVirama :: Char
tamilSignVirama = '\x0bcd' -- @U+0BCD@ TAMIL SIGN VIRAMA: ◌்

-- Block: Telugu----------------------------------------------------------------
teluguSignVirama, teluguLengthMark, teluguAiLengthMark :: Char
teluguSignVirama = '\x0c4d' -- @U+0C4D@ TELUGU SIGN VIRAMA: ◌్
teluguLengthMark = '\x0c55' -- @U+0C55@ TELUGU LENGTH MARK: ◌ౕ
teluguAiLengthMark = '\x0c56' -- @U+0C56@ TELUGU AI LENGTH MARK: ◌ౖ

-- Block: Kannada---------------------------------------------------------------
kannadaSignNukta, kannadaSignVirama :: Char
kannadaSignNukta = '\x0cbc' -- @U+0CBC@ KANNADA SIGN NUKTA: ◌಼
kannadaSignVirama = '\x0ccd' -- @U+0CCD@ KANNADA SIGN VIRAMA: ◌್

-- Block: Malayalam-------------------------------------------------------------
malayalamSignVerticalBarVirama, malayalamSignCircularVirama, malayalamSignVirama :: Char
malayalamSignVerticalBarVirama = '\x0d3b' -- @U+0D3B@ MALAYALAM SIGN VERTICAL BAR VIRAMA: ◌഻
malayalamSignCircularVirama = '\x0d3c' -- @U+0D3C@ MALAYALAM SIGN CIRCULAR VIRAMA: ◌഼
malayalamSignVirama = '\x0d4d' -- @U+0D4D@ MALAYALAM SIGN VIRAMA: ◌്

-- Block: Sinhala---------------------------------------------------------------
sinhalaSignAlLakuna :: Char
sinhalaSignAlLakuna = '\x0dca' -- @U+0DCA@ SINHALA SIGN AL-LAKUNA: ◌්

-- Block: Thai------------------------------------------------------------------
thaiCharacterSaraU, thaiCharacterSaraUu, thaiCharacterPhinthu, thaiCharacterMaiEk, thaiCharacterMaiTho, thaiCharacterMaiTri, thaiCharacterMaiChattawa :: Char
thaiCharacterSaraU = '\x0e38' -- @U+0E38@ THAI CHARACTER SARA U: ◌ุ
thaiCharacterSaraUu = '\x0e39' -- @U+0E39@ THAI CHARACTER SARA UU: ◌ู
thaiCharacterPhinthu = '\x0e3a' -- @U+0E3A@ THAI CHARACTER PHINTHU: ◌ฺ
thaiCharacterMaiEk = '\x0e48' -- @U+0E48@ THAI CHARACTER MAI EK: ◌่
thaiCharacterMaiTho = '\x0e49' -- @U+0E49@ THAI CHARACTER MAI THO: ◌้
thaiCharacterMaiTri = '\x0e4a' -- @U+0E4A@ THAI CHARACTER MAI TRI: ◌๊
thaiCharacterMaiChattawa = '\x0e4b' -- @U+0E4B@ THAI CHARACTER MAI CHATTAWA: ◌๋

-- Block: Lao-------------------------------------------------------------------
laoVowelSignU, laoVowelSignUu, laoSignPaliVirama, laoToneMaiEk, laoToneMaiTho, laoToneMaiTi, laoToneMaiCatawa :: Char
laoVowelSignU = '\x0eb8' -- @U+0EB8@ LAO VOWEL SIGN U: ◌ຸ
laoVowelSignUu = '\x0eb9' -- @U+0EB9@ LAO VOWEL SIGN UU: ◌ູ
laoSignPaliVirama = '\x0eba' -- @U+0EBA@ LAO SIGN PALI VIRAMA: ◌຺
laoToneMaiEk = '\x0ec8' -- @U+0EC8@ LAO TONE MAI EK: ◌່
laoToneMaiTho = '\x0ec9' -- @U+0EC9@ LAO TONE MAI THO: ◌້
laoToneMaiTi = '\x0eca' -- @U+0ECA@ LAO TONE MAI TI: ◌໊
laoToneMaiCatawa = '\x0ecb' -- @U+0ECB@ LAO TONE MAI CATAWA: ◌໋

-- Block: Tibetan---------------------------------------------------------------
tibetanAstrologicalSignKhyudPa, tibetanAstrologicalSignSdongTshugs, tibetanMarkNgasBzungNyiZla, tibetanMarkNgasBzungSgorRtags, tibetanMarkTsaPhru, tibetanVowelSignAa, tibetanVowelSignI, tibetanVowelSignU, tibetanVowelSignE, tibetanVowelSignEe, tibetanVowelSignO, tibetanVowelSignOo, tibetanVowelSignReversedI, tibetanSignNyiZlaNaaDa, tibetanSignSnaLdan, tibetanMarkHalanta, tibetanSignLciRtags, tibetanSignYangRtags, tibetanSymbolPadmaGdan :: Char
tibetanAstrologicalSignKhyudPa = '\x0f18' -- @U+0F18@ TIBETAN ASTROLOGICAL SIGN -KHYUD PA: ◌༘
tibetanAstrologicalSignSdongTshugs = '\x0f19' -- @U+0F19@ TIBETAN ASTROLOGICAL SIGN SDONG TSHUGS: ◌༙
tibetanMarkNgasBzungNyiZla = '\x0f35' -- @U+0F35@ TIBETAN MARK NGAS BZUNG NYI ZLA: ◌༵
tibetanMarkNgasBzungSgorRtags = '\x0f37' -- @U+0F37@ TIBETAN MARK NGAS BZUNG SGOR RTAGS: ◌༷
tibetanMarkTsaPhru = '\x0f39' -- @U+0F39@ TIBETAN MARK TSA -PHRU: ◌༹
tibetanVowelSignAa = '\x0f71' -- @U+0F71@ TIBETAN VOWEL SIGN AA: ◌ཱ
tibetanVowelSignI = '\x0f72' -- @U+0F72@ TIBETAN VOWEL SIGN I: ◌ི
tibetanVowelSignU = '\x0f74' -- @U+0F74@ TIBETAN VOWEL SIGN U: ◌ུ
tibetanVowelSignE = '\x0f7a' -- @U+0F7A@ TIBETAN VOWEL SIGN E: ◌ེ
tibetanVowelSignEe = '\x0f7b' -- @U+0F7B@ TIBETAN VOWEL SIGN EE: ◌ཻ
tibetanVowelSignO = '\x0f7c' -- @U+0F7C@ TIBETAN VOWEL SIGN O: ◌ོ
tibetanVowelSignOo = '\x0f7d' -- @U+0F7D@ TIBETAN VOWEL SIGN OO: ◌ཽ
tibetanVowelSignReversedI = '\x0f80' -- @U+0F80@ TIBETAN VOWEL SIGN REVERSED I: ◌ྀ
tibetanSignNyiZlaNaaDa = '\x0f82' -- @U+0F82@ TIBETAN SIGN NYI ZLA NAA DA: ◌ྂ
tibetanSignSnaLdan = '\x0f83' -- @U+0F83@ TIBETAN SIGN SNA LDAN: ◌ྃ
tibetanMarkHalanta = '\x0f84' -- @U+0F84@ TIBETAN MARK HALANTA: ◌྄
tibetanSignLciRtags = '\x0f86' -- @U+0F86@ TIBETAN SIGN LCI RTAGS: ◌྆
tibetanSignYangRtags = '\x0f87' -- @U+0F87@ TIBETAN SIGN YANG RTAGS: ◌྇
tibetanSymbolPadmaGdan = '\x0fc6' -- @U+0FC6@ TIBETAN SYMBOL PADMA GDAN: ◌࿆

-- Block: Myanmar---------------------------------------------------------------
myanmarSignDotBelow, myanmarSignVirama, myanmarSignAsat, myanmarSignShanCouncilEmphaticTone :: Char
myanmarSignDotBelow = '\x1037' -- @U+1037@ MYANMAR SIGN DOT BELOW: ◌့
myanmarSignVirama = '\x1039' -- @U+1039@ MYANMAR SIGN VIRAMA: ◌္
myanmarSignAsat = '\x103a' -- @U+103A@ MYANMAR SIGN ASAT: ◌်
myanmarSignShanCouncilEmphaticTone = '\x108d' -- @U+108D@ MYANMAR SIGN SHAN COUNCIL EMPHATIC TONE: ◌ႍ

-- Block: Ethiopic--------------------------------------------------------------
ethiopicCombiningGeminationAndVowelLengthMark, ethiopicCombiningVowelLengthMark, ethiopicCombiningGeminationMark :: Char
ethiopicCombiningGeminationAndVowelLengthMark = '\x135d' -- @U+135D@ ETHIOPIC COMBINING GEMINATION AND VOWEL LENGTH MARK: ◌፝
ethiopicCombiningVowelLengthMark = '\x135e' -- @U+135E@ ETHIOPIC COMBINING VOWEL LENGTH MARK: ◌፞
ethiopicCombiningGeminationMark = '\x135f' -- @U+135F@ ETHIOPIC COMBINING GEMINATION MARK: ◌፟

-- Block: Tagalog---------------------------------------------------------------
tagalogSignVirama :: Char
tagalogSignVirama = '\x1714' -- @U+1714@ TAGALOG SIGN VIRAMA: ◌᜔

-- Block: Hanunoo---------------------------------------------------------------
hanunooSignPamudpod :: Char
hanunooSignPamudpod = '\x1734' -- @U+1734@ HANUNOO SIGN PAMUDPOD: ◌᜴

-- Block: Khmer-----------------------------------------------------------------
khmerSignCoeng, khmerSignAtthacan :: Char
khmerSignCoeng = '\x17d2' -- @U+17D2@ KHMER SIGN COENG: ◌្
khmerSignAtthacan = '\x17dd' -- @U+17DD@ KHMER SIGN ATTHACAN: ◌៝

-- Block: Mongolian-------------------------------------------------------------
mongolianLetterAliGaliDagalga :: Char
mongolianLetterAliGaliDagalga = '\x18a9' -- @U+18A9@ MONGOLIAN LETTER ALI GALI DAGALGA: ◌ᢩ

-- Block: Limbu-----------------------------------------------------------------
limbuSignMukphreng, limbuSignKemphreng, limbuSignSaI :: Char
limbuSignMukphreng = '\x1939' -- @U+1939@ LIMBU SIGN MUKPHRENG: ◌᤹
limbuSignKemphreng = '\x193a' -- @U+193A@ LIMBU SIGN KEMPHRENG: ◌᤺
limbuSignSaI = '\x193b' -- @U+193B@ LIMBU SIGN SA-I: ◌᤻

-- Block: Buginese--------------------------------------------------------------
bugineseVowelSignI, bugineseVowelSignU :: Char
bugineseVowelSignI = '\x1a17' -- @U+1A17@ BUGINESE VOWEL SIGN I: ◌ᨗ
bugineseVowelSignU = '\x1a18' -- @U+1A18@ BUGINESE VOWEL SIGN U: ◌ᨘ

-- Block: Tai Tham--------------------------------------------------------------
taiThamSignSakot, taiThamSignTone1, taiThamSignTone2, taiThamSignKhuenTone3, taiThamSignKhuenTone4, taiThamSignKhuenTone5, taiThamSignRaHaam, taiThamSignMaiSam, taiThamSignKhuenLueKaran, taiThamCombiningCryptogrammicDot :: Char
taiThamSignSakot = '\x1a60' -- @U+1A60@ TAI THAM SIGN SAKOT: ◌᩠
taiThamSignTone1 = '\x1a75' -- @U+1A75@ TAI THAM SIGN TONE-1: ◌᩵
taiThamSignTone2 = '\x1a76' -- @U+1A76@ TAI THAM SIGN TONE-2: ◌᩶
taiThamSignKhuenTone3 = '\x1a77' -- @U+1A77@ TAI THAM SIGN KHUEN TONE-3: ◌᩷
taiThamSignKhuenTone4 = '\x1a78' -- @U+1A78@ TAI THAM SIGN KHUEN TONE-4: ◌᩸
taiThamSignKhuenTone5 = '\x1a79' -- @U+1A79@ TAI THAM SIGN KHUEN TONE-5: ◌᩹
taiThamSignRaHaam = '\x1a7a' -- @U+1A7A@ TAI THAM SIGN RA HAAM: ◌᩺
taiThamSignMaiSam = '\x1a7b' -- @U+1A7B@ TAI THAM SIGN MAI SAM: ◌᩻
taiThamSignKhuenLueKaran = '\x1a7c' -- @U+1A7C@ TAI THAM SIGN KHUEN-LUE KARAN: ◌᩼
taiThamCombiningCryptogrammicDot = '\x1a7f' -- @U+1A7F@ TAI THAM COMBINING CRYPTOGRAMMIC DOT: ◌᩿

-- Block: Combining Diacritical Marks Extended----------------------------------
doubledCircumflexAccent, diaeresisRing, infinity, downwardsArrow, tripleDot, xXBelow, openMarkBelow, doubleOpenMarkBelow, lightCentralizationStrokeBelow, strongCentralizationStrokeBelow, parenthesesAbove, doubleParenthesesAbove, parenthesesBelow :: Char
doubledCircumflexAccent = '\x1ab0' -- @U+1AB0@ COMBINING DOUBLED CIRCUMFLEX ACCENT: ◌᪰. Alias of 'doubledCircumflexAbove'
diaeresisRing = '\x1ab1' -- @U+1AB1@ COMBINING DIAERESIS-RING: ◌᪱
infinity = '\x1ab2' -- @U+1AB2@ COMBINING INFINITY: ◌᪲. Alias of 'infinityAbove'
downwardsArrow = '\x1ab3' -- @U+1AB3@ COMBINING DOWNWARDS ARROW: ◌᪳
tripleDot = '\x1ab4' -- @U+1AB4@ COMBINING TRIPLE DOT: ◌᪴
xXBelow = '\x1ab5' -- @U+1AB5@ COMBINING X-X BELOW: ◌᪵
openMarkBelow = '\x1ab7' -- @U+1AB7@ COMBINING OPEN MARK BELOW: ◌᪷
doubleOpenMarkBelow = '\x1ab8' -- @U+1AB8@ COMBINING DOUBLE OPEN MARK BELOW: ◌᪸
lightCentralizationStrokeBelow = '\x1ab9' -- @U+1AB9@ COMBINING LIGHT CENTRALIZATION STROKE BELOW: ◌᪹
strongCentralizationStrokeBelow = '\x1aba' -- @U+1ABA@ COMBINING STRONG CENTRALIZATION STROKE BELOW: ◌᪺
parenthesesAbove = '\x1abb' -- @U+1ABB@ COMBINING PARENTHESES ABOVE: ◌᪻
doubleParenthesesAbove = '\x1abc' -- @U+1ABC@ COMBINING DOUBLE PARENTHESES ABOVE: ◌᪼
parenthesesBelow = '\x1abd' -- @U+1ABD@ COMBINING PARENTHESES BELOW: ◌᪽

-- Block: Balinese--------------------------------------------------------------
balineseSignRerekan, balineseAdegAdeg, balineseMusicalSymbolCombiningTegeh, balineseMusicalSymbolCombiningEndep, balineseMusicalSymbolCombiningKempul, balineseMusicalSymbolCombiningKempli, balineseMusicalSymbolCombiningJegogan, balineseMusicalSymbolCombiningKempulWithJegogan, balineseMusicalSymbolCombiningKempliWithJegogan, balineseMusicalSymbolCombiningBende, balineseMusicalSymbolCombiningGong :: Char
balineseSignRerekan = '\x1b34' -- @U+1B34@ BALINESE SIGN REREKAN: ◌᬴
balineseAdegAdeg = '\x1b44' -- @U+1B44@ BALINESE ADEG ADEG: ◌᭄
balineseMusicalSymbolCombiningTegeh = '\x1b6b' -- @U+1B6B@ BALINESE MUSICAL SYMBOL COMBINING TEGEH: ◌᭫
balineseMusicalSymbolCombiningEndep = '\x1b6c' -- @U+1B6C@ BALINESE MUSICAL SYMBOL COMBINING ENDEP: ◌᭬
balineseMusicalSymbolCombiningKempul = '\x1b6d' -- @U+1B6D@ BALINESE MUSICAL SYMBOL COMBINING KEMPUL: ◌᭭
balineseMusicalSymbolCombiningKempli = '\x1b6e' -- @U+1B6E@ BALINESE MUSICAL SYMBOL COMBINING KEMPLI: ◌᭮
balineseMusicalSymbolCombiningJegogan = '\x1b6f' -- @U+1B6F@ BALINESE MUSICAL SYMBOL COMBINING JEGOGAN: ◌᭯
balineseMusicalSymbolCombiningKempulWithJegogan = '\x1b70' -- @U+1B70@ BALINESE MUSICAL SYMBOL COMBINING KEMPUL WITH JEGOGAN: ◌᭰
balineseMusicalSymbolCombiningKempliWithJegogan = '\x1b71' -- @U+1B71@ BALINESE MUSICAL SYMBOL COMBINING KEMPLI WITH JEGOGAN: ◌᭱
balineseMusicalSymbolCombiningBende = '\x1b72' -- @U+1B72@ BALINESE MUSICAL SYMBOL COMBINING BENDE: ◌᭲
balineseMusicalSymbolCombiningGong = '\x1b73' -- @U+1B73@ BALINESE MUSICAL SYMBOL COMBINING GONG: ◌᭳

-- Block: Sundanese-------------------------------------------------------------
sundaneseSignPamaaeh, sundaneseSignVirama :: Char
sundaneseSignPamaaeh = '\x1baa' -- @U+1BAA@ SUNDANESE SIGN PAMAAEH: ◌᮪
sundaneseSignVirama = '\x1bab' -- @U+1BAB@ SUNDANESE SIGN VIRAMA: ◌᮫

-- Block: Batak-----------------------------------------------------------------
batakSignTompi, batakPangolat, batakPanongonan :: Char
batakSignTompi = '\x1be6' -- @U+1BE6@ BATAK SIGN TOMPI: ◌᯦
batakPangolat = '\x1bf2' -- @U+1BF2@ BATAK PANGOLAT: ◌᯲
batakPanongonan = '\x1bf3' -- @U+1BF3@ BATAK PANONGONAN: ◌᯳

-- Block: Lepcha----------------------------------------------------------------
lepchaSignNukta :: Char
lepchaSignNukta = '\x1c37' -- @U+1C37@ LEPCHA SIGN NUKTA: ◌᰷

-- Block: Vedic Extensions------------------------------------------------------
vedicToneKarshana, vedicToneShara, vedicTonePrenkha, vedicSignYajurvedicMidlineSvarita, vedicToneYajurvedicAggravatedIndependentSvarita, vedicToneYajurvedicIndependentSvarita, vedicToneYajurvedicKathakaIndependentSvarita, vedicToneCandraBelow, vedicToneYajurvedicKathakaIndependentSvaritaSchroeder, vedicToneDoubleSvarita, vedicToneTripleSvarita, vedicToneKathakaAnudatta, vedicToneDotBelow, vedicToneTwoDotsBelow, vedicToneThreeDotsBelow, vedicToneRigvedicKashmiriIndependentSvarita, vedicSignVisargaSvarita, vedicSignVisargaUdatta, vedicSignReversedVisargaUdatta, vedicSignVisargaAnudatta, vedicSignReversedVisargaAnudatta, vedicSignVisargaUdattaWithTail, vedicSignVisargaAnudattaWithTail, vedicSignTiryak, vedicToneCandraAbove, vedicToneRingAbove, vedicToneDoubleRingAbove :: Char
vedicToneKarshana = '\x1cd0' -- @U+1CD0@ VEDIC TONE KARSHANA: ◌᳐
vedicToneShara = '\x1cd1' -- @U+1CD1@ VEDIC TONE SHARA: ◌᳑
vedicTonePrenkha = '\x1cd2' -- @U+1CD2@ VEDIC TONE PRENKHA: ◌᳒
vedicSignYajurvedicMidlineSvarita = '\x1cd4' -- @U+1CD4@ VEDIC SIGN YAJURVEDIC MIDLINE SVARITA: ◌᳔
vedicToneYajurvedicAggravatedIndependentSvarita = '\x1cd5' -- @U+1CD5@ VEDIC TONE YAJURVEDIC AGGRAVATED INDEPENDENT SVARITA: ◌᳕
vedicToneYajurvedicIndependentSvarita = '\x1cd6' -- @U+1CD6@ VEDIC TONE YAJURVEDIC INDEPENDENT SVARITA: ◌᳖
vedicToneYajurvedicKathakaIndependentSvarita = '\x1cd7' -- @U+1CD7@ VEDIC TONE YAJURVEDIC KATHAKA INDEPENDENT SVARITA: ◌᳗
vedicToneCandraBelow = '\x1cd8' -- @U+1CD8@ VEDIC TONE CANDRA BELOW: ◌᳘
vedicToneYajurvedicKathakaIndependentSvaritaSchroeder = '\x1cd9' -- @U+1CD9@ VEDIC TONE YAJURVEDIC KATHAKA INDEPENDENT SVARITA SCHROEDER: ◌᳙
vedicToneDoubleSvarita = '\x1cda' -- @U+1CDA@ VEDIC TONE DOUBLE SVARITA: ◌᳚
vedicToneTripleSvarita = '\x1cdb' -- @U+1CDB@ VEDIC TONE TRIPLE SVARITA: ◌᳛
vedicToneKathakaAnudatta = '\x1cdc' -- @U+1CDC@ VEDIC TONE KATHAKA ANUDATTA: ◌᳜
vedicToneDotBelow = '\x1cdd' -- @U+1CDD@ VEDIC TONE DOT BELOW: ◌᳝
vedicToneTwoDotsBelow = '\x1cde' -- @U+1CDE@ VEDIC TONE TWO DOTS BELOW: ◌᳞
vedicToneThreeDotsBelow = '\x1cdf' -- @U+1CDF@ VEDIC TONE THREE DOTS BELOW: ◌᳟
vedicToneRigvedicKashmiriIndependentSvarita = '\x1ce0' -- @U+1CE0@ VEDIC TONE RIGVEDIC KASHMIRI INDEPENDENT SVARITA: ◌᳠
vedicSignVisargaSvarita = '\x1ce2' -- @U+1CE2@ VEDIC SIGN VISARGA SVARITA: ◌᳢
vedicSignVisargaUdatta = '\x1ce3' -- @U+1CE3@ VEDIC SIGN VISARGA UDATTA: ◌᳣
vedicSignReversedVisargaUdatta = '\x1ce4' -- @U+1CE4@ VEDIC SIGN REVERSED VISARGA UDATTA: ◌᳤
vedicSignVisargaAnudatta = '\x1ce5' -- @U+1CE5@ VEDIC SIGN VISARGA ANUDATTA: ◌᳥
vedicSignReversedVisargaAnudatta = '\x1ce6' -- @U+1CE6@ VEDIC SIGN REVERSED VISARGA ANUDATTA: ◌᳦
vedicSignVisargaUdattaWithTail = '\x1ce7' -- @U+1CE7@ VEDIC SIGN VISARGA UDATTA WITH TAIL: ◌᳧
vedicSignVisargaAnudattaWithTail = '\x1ce8' -- @U+1CE8@ VEDIC SIGN VISARGA ANUDATTA WITH TAIL: ◌᳨
vedicSignTiryak = '\x1ced' -- @U+1CED@ VEDIC SIGN TIRYAK: ◌᳭
vedicToneCandraAbove = '\x1cf4' -- @U+1CF4@ VEDIC TONE CANDRA ABOVE: ◌᳴
vedicToneRingAbove = '\x1cf8' -- @U+1CF8@ VEDIC TONE RING ABOVE: ◌᳸
vedicToneDoubleRingAbove = '\x1cf9' -- @U+1CF9@ VEDIC TONE DOUBLE RING ABOVE: ◌᳹

-- Block: Combining Diacritical Marks Supplement--------------------------------
dottedGraveAccent, dottedAcuteAccent, snakeBelow, suspensionMark, macronAcute, graveMacron, macronGrave, acuteMacron, graveAcuteGrave, acuteGraveAcute, latinSmallLetterRBelow, breveMacron, macronBreve, doubleCircumflexAbove, ogonekAbove, zigzagBelow, isBelow, urAbove, usAbove, latinSmallLetterFlattenedOpenAAbove, latinSmallLetterAe, latinSmallLetterAo, latinSmallLetterAv, latinSmallLetterCCedilla, latinSmallLetterInsularD, latinSmallLetterEth, latinSmallLetterG, latinLetterSmallCapitalG, latinSmallLetterK, latinSmallLetterL, latinLetterSmallCapitalL, latinLetterSmallCapitalM, latinSmallLetterN, latinLetterSmallCapitalN, latinLetterSmallCapitalR, latinSmallLetterRRotunda, latinSmallLetterS, latinSmallLetterLongS, latinSmallLetterZ, latinSmallLetterAlpha, latinSmallLetterB, latinSmallLetterBeta, latinSmallLetterSchwa, latinSmallLetterF, latinSmallLetterLWithDoubleMiddleTilde, latinSmallLetterOWithLightCentralizationStroke, latinSmallLetterP, latinSmallLetterEsh, latinSmallLetterUWithLightCentralizationStroke, latinSmallLetterW, latinSmallLetterAWithDiaeresis, latinSmallLetterOWithDiaeresis, latinSmallLetterUWithDiaeresis, upTackAbove, kavykaAboveRight, kavykaAboveLeft, dotAboveLeft, wideInvertedBridgeBelow, deletionMark, almostEqualToBelow, leftArrowheadAbove, rightArrowheadAndDownArrowheadBelow :: Char
dottedGraveAccent = '\x1dc0' -- @U+1DC0@ COMBINING DOTTED GRAVE ACCENT: ◌᷀
dottedAcuteAccent = '\x1dc1' -- @U+1DC1@ COMBINING DOTTED ACUTE ACCENT: ◌᷁
snakeBelow = '\x1dc2' -- @U+1DC2@ COMBINING SNAKE BELOW: ◌᷂
suspensionMark = '\x1dc3' -- @U+1DC3@ COMBINING SUSPENSION MARK: ◌᷃
macronAcute = '\x1dc4' -- @U+1DC4@ COMBINING MACRON-ACUTE: ◌᷄
graveMacron = '\x1dc5' -- @U+1DC5@ COMBINING GRAVE-MACRON: ◌᷅
macronGrave = '\x1dc6' -- @U+1DC6@ COMBINING MACRON-GRAVE: ◌᷆
acuteMacron = '\x1dc7' -- @U+1DC7@ COMBINING ACUTE-MACRON: ◌᷇
graveAcuteGrave = '\x1dc8' -- @U+1DC8@ COMBINING GRAVE-ACUTE-GRAVE: ◌᷈
acuteGraveAcute = '\x1dc9' -- @U+1DC9@ COMBINING ACUTE-GRAVE-ACUTE: ◌᷉
latinSmallLetterRBelow = '\x1dca' -- @U+1DCA@ COMBINING LATIN SMALL LETTER R BELOW: ◌᷊
breveMacron = '\x1dcb' -- @U+1DCB@ COMBINING BREVE-MACRON: ◌᷋
macronBreve = '\x1dcc' -- @U+1DCC@ COMBINING MACRON-BREVE: ◌᷌
doubleCircumflexAbove = '\x1dcd' -- @U+1DCD@ COMBINING DOUBLE CIRCUMFLEX ABOVE: ◌᷍
ogonekAbove = '\x1dce' -- @U+1DCE@ COMBINING OGONEK ABOVE: ◌᷎
zigzagBelow = '\x1dcf' -- @U+1DCF@ COMBINING ZIGZAG BELOW: ◌᷏
isBelow = '\x1dd0' -- @U+1DD0@ COMBINING IS BELOW: ◌᷐
urAbove = '\x1dd1' -- @U+1DD1@ COMBINING UR ABOVE: ◌᷑
usAbove = '\x1dd2' -- @U+1DD2@ COMBINING US ABOVE: ◌᷒
latinSmallLetterFlattenedOpenAAbove = '\x1dd3' -- @U+1DD3@ COMBINING LATIN SMALL LETTER FLATTENED OPEN A ABOVE: ◌ᷓ
latinSmallLetterAe = '\x1dd4' -- @U+1DD4@ COMBINING LATIN SMALL LETTER AE: ◌ᷔ
latinSmallLetterAo = '\x1dd5' -- @U+1DD5@ COMBINING LATIN SMALL LETTER AO: ◌ᷕ
latinSmallLetterAv = '\x1dd6' -- @U+1DD6@ COMBINING LATIN SMALL LETTER AV: ◌ᷖ
latinSmallLetterCCedilla = '\x1dd7' -- @U+1DD7@ COMBINING LATIN SMALL LETTER C CEDILLA: ◌ᷗ
latinSmallLetterInsularD = '\x1dd8' -- @U+1DD8@ COMBINING LATIN SMALL LETTER INSULAR D: ◌ᷘ
latinSmallLetterEth = '\x1dd9' -- @U+1DD9@ COMBINING LATIN SMALL LETTER ETH: ◌ᷙ
latinSmallLetterG = '\x1dda' -- @U+1DDA@ COMBINING LATIN SMALL LETTER G: ◌ᷚ
latinLetterSmallCapitalG = '\x1ddb' -- @U+1DDB@ COMBINING LATIN LETTER SMALL CAPITAL G: ◌ᷛ
latinSmallLetterK = '\x1ddc' -- @U+1DDC@ COMBINING LATIN SMALL LETTER K: ◌ᷜ
latinSmallLetterL = '\x1ddd' -- @U+1DDD@ COMBINING LATIN SMALL LETTER L: ◌ᷝ
latinLetterSmallCapitalL = '\x1dde' -- @U+1DDE@ COMBINING LATIN LETTER SMALL CAPITAL L: ◌ᷞ
latinLetterSmallCapitalM = '\x1ddf' -- @U+1DDF@ COMBINING LATIN LETTER SMALL CAPITAL M: ◌ᷟ
latinSmallLetterN = '\x1de0' -- @U+1DE0@ COMBINING LATIN SMALL LETTER N: ◌ᷠ
latinLetterSmallCapitalN = '\x1de1' -- @U+1DE1@ COMBINING LATIN LETTER SMALL CAPITAL N: ◌ᷡ
latinLetterSmallCapitalR = '\x1de2' -- @U+1DE2@ COMBINING LATIN LETTER SMALL CAPITAL R: ◌ᷢ
latinSmallLetterRRotunda = '\x1de3' -- @U+1DE3@ COMBINING LATIN SMALL LETTER R ROTUNDA: ◌ᷣ
latinSmallLetterS = '\x1de4' -- @U+1DE4@ COMBINING LATIN SMALL LETTER S: ◌ᷤ
latinSmallLetterLongS = '\x1de5' -- @U+1DE5@ COMBINING LATIN SMALL LETTER LONG S: ◌ᷥ
latinSmallLetterZ = '\x1de6' -- @U+1DE6@ COMBINING LATIN SMALL LETTER Z: ◌ᷦ
latinSmallLetterAlpha = '\x1de7' -- @U+1DE7@ COMBINING LATIN SMALL LETTER ALPHA: ◌ᷧ
latinSmallLetterB = '\x1de8' -- @U+1DE8@ COMBINING LATIN SMALL LETTER B: ◌ᷨ
latinSmallLetterBeta = '\x1de9' -- @U+1DE9@ COMBINING LATIN SMALL LETTER BETA: ◌ᷩ
latinSmallLetterSchwa = '\x1dea' -- @U+1DEA@ COMBINING LATIN SMALL LETTER SCHWA: ◌ᷪ
latinSmallLetterF = '\x1deb' -- @U+1DEB@ COMBINING LATIN SMALL LETTER F: ◌ᷫ
latinSmallLetterLWithDoubleMiddleTilde = '\x1dec' -- @U+1DEC@ COMBINING LATIN SMALL LETTER L WITH DOUBLE MIDDLE TILDE: ◌ᷬ
latinSmallLetterOWithLightCentralizationStroke = '\x1ded' -- @U+1DED@ COMBINING LATIN SMALL LETTER O WITH LIGHT CENTRALIZATION STROKE: ◌ᷭ
latinSmallLetterP = '\x1dee' -- @U+1DEE@ COMBINING LATIN SMALL LETTER P: ◌ᷮ
latinSmallLetterEsh = '\x1def' -- @U+1DEF@ COMBINING LATIN SMALL LETTER ESH: ◌ᷯ
latinSmallLetterUWithLightCentralizationStroke = '\x1df0' -- @U+1DF0@ COMBINING LATIN SMALL LETTER U WITH LIGHT CENTRALIZATION STROKE: ◌ᷰ
latinSmallLetterW = '\x1df1' -- @U+1DF1@ COMBINING LATIN SMALL LETTER W: ◌ᷱ
latinSmallLetterAWithDiaeresis = '\x1df2' -- @U+1DF2@ COMBINING LATIN SMALL LETTER A WITH DIAERESIS: ◌ᷲ
latinSmallLetterOWithDiaeresis = '\x1df3' -- @U+1DF3@ COMBINING LATIN SMALL LETTER O WITH DIAERESIS: ◌ᷳ
latinSmallLetterUWithDiaeresis = '\x1df4' -- @U+1DF4@ COMBINING LATIN SMALL LETTER U WITH DIAERESIS: ◌ᷴ
upTackAbove = '\x1df5' -- @U+1DF5@ COMBINING UP TACK ABOVE: ◌᷵
kavykaAboveRight = '\x1df6' -- @U+1DF6@ COMBINING KAVYKA ABOVE RIGHT: ◌᷶
kavykaAboveLeft = '\x1df7' -- @U+1DF7@ COMBINING KAVYKA ABOVE LEFT: ◌᷷
dotAboveLeft = '\x1df8' -- @U+1DF8@ COMBINING DOT ABOVE LEFT: ◌᷸
wideInvertedBridgeBelow = '\x1df9' -- @U+1DF9@ COMBINING WIDE INVERTED BRIDGE BELOW: ◌᷹
deletionMark = '\x1dfb' -- @U+1DFB@ COMBINING DELETION MARK: ◌᷻
almostEqualToBelow = '\x1dfd' -- @U+1DFD@ COMBINING ALMOST EQUAL TO BELOW: ◌᷽
leftArrowheadAbove = '\x1dfe' -- @U+1DFE@ COMBINING LEFT ARROWHEAD ABOVE: ◌᷾
rightArrowheadAndDownArrowheadBelow = '\x1dff' -- @U+1DFF@ COMBINING RIGHT ARROWHEAD AND DOWN ARROWHEAD BELOW: ◌᷿

-- Block: Combining Diacritical Marks for Symbols-------------------------------
leftHarpoonAbove, rightHarpoonAbove, longVerticalLineOverlay, shortVerticalLineOverlay, leftArrowAbove, rightArrowAbove, leftRightArrowAbove, reverseSolidusOverlay, doubleVerticalStrokeOverlay, annuitySymbol, tripleUnderdot, wideBridgeAbove, leftwardsArrowOverlay, longDoubleSolidusOverlay, rightwardsHarpoonWithBarbDownwards, leftwardsHarpoonWithBarbDownwards, leftArrowBelow, rightArrowBelow :: Char
leftHarpoonAbove = '\x20d0' -- @U+20D0@ COMBINING LEFT HARPOON ABOVE: ◌⃐
rightHarpoonAbove = '\x20d1' -- @U+20D1@ COMBINING RIGHT HARPOON ABOVE: ◌⃑
longVerticalLineOverlay = '\x20d2' -- @U+20D2@ COMBINING LONG VERTICAL LINE OVERLAY: ◌⃒. Alias of 'longVerticalLine'
shortVerticalLineOverlay = '\x20d3' -- @U+20D3@ COMBINING SHORT VERTICAL LINE OVERLAY: ◌⃓. Alias of 'shortVerticalLine'
leftArrowAbove = '\x20d6' -- @U+20D6@ COMBINING LEFT ARROW ABOVE: ◌⃖. Alias of 'leftwardsArrowAbove'
rightArrowAbove = '\x20d7' -- @U+20D7@ COMBINING RIGHT ARROW ABOVE: ◌⃗. Alias of 'rightwardsArrowAbove'
leftRightArrowAbove = '\x20e1' -- @U+20E1@ COMBINING LEFT RIGHT ARROW ABOVE: ◌⃡
reverseSolidusOverlay = '\x20e5' -- @U+20E5@ COMBINING REVERSE SOLIDUS OVERLAY: ◌⃥. Alias of 'reverseLongSolidus'
doubleVerticalStrokeOverlay = '\x20e6' -- @U+20E6@ COMBINING DOUBLE VERTICAL STROKE OVERLAY: ◌⃦. Alias of 'doubledLongVerticalLine'
annuitySymbol = '\x20e7' -- @U+20E7@ COMBINING ANNUITY SYMBOL: ◌⃧
tripleUnderdot = '\x20e8' -- @U+20E8@ COMBINING TRIPLE UNDERDOT: ◌⃨. Alias of 'threeDotsBelow'
wideBridgeAbove = '\x20e9' -- @U+20E9@ COMBINING WIDE BRIDGE ABOVE: ◌⃩
leftwardsArrowOverlay = '\x20ea' -- @U+20EA@ COMBINING LEFTWARDS ARROW OVERLAY: ◌⃪
longDoubleSolidusOverlay = '\x20eb' -- @U+20EB@ COMBINING LONG DOUBLE SOLIDUS OVERLAY: ◌⃫. Alias of 'doubledLongSolidus'
rightwardsHarpoonWithBarbDownwards = '\x20ec' -- @U+20EC@ COMBINING RIGHTWARDS HARPOON WITH BARB DOWNWARDS: ◌⃬
leftwardsHarpoonWithBarbDownwards = '\x20ed' -- @U+20ED@ COMBINING LEFTWARDS HARPOON WITH BARB DOWNWARDS: ◌⃭
leftArrowBelow = '\x20ee' -- @U+20EE@ COMBINING LEFT ARROW BELOW: ◌⃮. Alias of 'leftwardsArrowBelow'
rightArrowBelow = '\x20ef' -- @U+20EF@ COMBINING RIGHT ARROW BELOW: ◌⃯. Alias of 'rightwardsArrowBelow'

-- Block: Coptic----------------------------------------------------------------
copticCombiningNiAbove, copticCombiningSpiritusAsper, copticCombiningSpiritusLenis :: Char
copticCombiningNiAbove = '\x2cef' -- @U+2CEF@ COPTIC COMBINING NI ABOVE: ◌⳯
copticCombiningSpiritusAsper = '\x2cf0' -- @U+2CF0@ COPTIC COMBINING SPIRITUS ASPER: ◌⳰
copticCombiningSpiritusLenis = '\x2cf1' -- @U+2CF1@ COPTIC COMBINING SPIRITUS LENIS: ◌⳱

-- Block: Tifinagh--------------------------------------------------------------
tifinaghConsonantJoiner :: Char
tifinaghConsonantJoiner = '\x2d7f' -- @U+2D7F@ TIFINAGH CONSONANT JOINER: ◌⵿

-- Block: Cyrillic Extended-A---------------------------------------------------
cyrillicLetterBe, cyrillicLetterVe, cyrillicLetterGhe, cyrillicLetterDe, cyrillicLetterZhe, cyrillicLetterZe, cyrillicLetterKa, cyrillicLetterEl, cyrillicLetterEm, cyrillicLetterEn, cyrillicLetterO, cyrillicLetterPe, cyrillicLetterEr, cyrillicLetterEs, cyrillicLetterTe, cyrillicLetterHa, cyrillicLetterTse, cyrillicLetterChe, cyrillicLetterSha, cyrillicLetterShcha, cyrillicLetterFita, cyrillicLetterEsTe, cyrillicLetterA, cyrillicLetterIe, cyrillicLetterDjerv, cyrillicLetterMonographUk, cyrillicLetterYat, cyrillicLetterYu, cyrillicLetterIotifiedA, cyrillicLetterLittleYus, cyrillicLetterBigYus, cyrillicLetterIotifiedBigYus :: Char
cyrillicLetterBe = '\x2de0' -- @U+2DE0@ COMBINING CYRILLIC LETTER BE: ◌ⷠ
cyrillicLetterVe = '\x2de1' -- @U+2DE1@ COMBINING CYRILLIC LETTER VE: ◌ⷡ
cyrillicLetterGhe = '\x2de2' -- @U+2DE2@ COMBINING CYRILLIC LETTER GHE: ◌ⷢ
cyrillicLetterDe = '\x2de3' -- @U+2DE3@ COMBINING CYRILLIC LETTER DE: ◌ⷣ
cyrillicLetterZhe = '\x2de4' -- @U+2DE4@ COMBINING CYRILLIC LETTER ZHE: ◌ⷤ
cyrillicLetterZe = '\x2de5' -- @U+2DE5@ COMBINING CYRILLIC LETTER ZE: ◌ⷥ
cyrillicLetterKa = '\x2de6' -- @U+2DE6@ COMBINING CYRILLIC LETTER KA: ◌ⷦ
cyrillicLetterEl = '\x2de7' -- @U+2DE7@ COMBINING CYRILLIC LETTER EL: ◌ⷧ
cyrillicLetterEm = '\x2de8' -- @U+2DE8@ COMBINING CYRILLIC LETTER EM: ◌ⷨ
cyrillicLetterEn = '\x2de9' -- @U+2DE9@ COMBINING CYRILLIC LETTER EN: ◌ⷩ
cyrillicLetterO = '\x2dea' -- @U+2DEA@ COMBINING CYRILLIC LETTER O: ◌ⷪ
cyrillicLetterPe = '\x2deb' -- @U+2DEB@ COMBINING CYRILLIC LETTER PE: ◌ⷫ
cyrillicLetterEr = '\x2dec' -- @U+2DEC@ COMBINING CYRILLIC LETTER ER: ◌ⷬ
cyrillicLetterEs = '\x2ded' -- @U+2DED@ COMBINING CYRILLIC LETTER ES: ◌ⷭ
cyrillicLetterTe = '\x2dee' -- @U+2DEE@ COMBINING CYRILLIC LETTER TE: ◌ⷮ
cyrillicLetterHa = '\x2def' -- @U+2DEF@ COMBINING CYRILLIC LETTER HA: ◌ⷯ
cyrillicLetterTse = '\x2df0' -- @U+2DF0@ COMBINING CYRILLIC LETTER TSE: ◌ⷰ
cyrillicLetterChe = '\x2df1' -- @U+2DF1@ COMBINING CYRILLIC LETTER CHE: ◌ⷱ
cyrillicLetterSha = '\x2df2' -- @U+2DF2@ COMBINING CYRILLIC LETTER SHA: ◌ⷲ
cyrillicLetterShcha = '\x2df3' -- @U+2DF3@ COMBINING CYRILLIC LETTER SHCHA: ◌ⷳ
cyrillicLetterFita = '\x2df4' -- @U+2DF4@ COMBINING CYRILLIC LETTER FITA: ◌ⷴ
cyrillicLetterEsTe = '\x2df5' -- @U+2DF5@ COMBINING CYRILLIC LETTER ES-TE: ◌ⷵ
cyrillicLetterA = '\x2df6' -- @U+2DF6@ COMBINING CYRILLIC LETTER A: ◌ⷶ
cyrillicLetterIe = '\x2df7' -- @U+2DF7@ COMBINING CYRILLIC LETTER IE: ◌ⷷ
cyrillicLetterDjerv = '\x2df8' -- @U+2DF8@ COMBINING CYRILLIC LETTER DJERV: ◌ⷸ
cyrillicLetterMonographUk = '\x2df9' -- @U+2DF9@ COMBINING CYRILLIC LETTER MONOGRAPH UK: ◌ⷹ
cyrillicLetterYat = '\x2dfa' -- @U+2DFA@ COMBINING CYRILLIC LETTER YAT: ◌ⷺ
cyrillicLetterYu = '\x2dfb' -- @U+2DFB@ COMBINING CYRILLIC LETTER YU: ◌ⷻ
cyrillicLetterIotifiedA = '\x2dfc' -- @U+2DFC@ COMBINING CYRILLIC LETTER IOTIFIED A: ◌ⷼ
cyrillicLetterLittleYus = '\x2dfd' -- @U+2DFD@ COMBINING CYRILLIC LETTER LITTLE YUS: ◌ⷽ
cyrillicLetterBigYus = '\x2dfe' -- @U+2DFE@ COMBINING CYRILLIC LETTER BIG YUS: ◌ⷾ
cyrillicLetterIotifiedBigYus = '\x2dff' -- @U+2DFF@ COMBINING CYRILLIC LETTER IOTIFIED BIG YUS: ◌ⷿ

-- Block: CJK Symbols and Punctuation-------------------------------------------
ideographicLevelToneMark, ideographicRisingToneMark, ideographicDepartingToneMark, ideographicEnteringToneMark, hangulSingleDotToneMark, hangulDoubleDotToneMark :: Char
ideographicLevelToneMark = '\x302a' -- @U+302A@ IDEOGRAPHIC LEVEL TONE MARK: ◌〪
ideographicRisingToneMark = '\x302b' -- @U+302B@ IDEOGRAPHIC RISING TONE MARK: ◌〫
ideographicDepartingToneMark = '\x302c' -- @U+302C@ IDEOGRAPHIC DEPARTING TONE MARK: ◌〬
ideographicEnteringToneMark = '\x302d' -- @U+302D@ IDEOGRAPHIC ENTERING TONE MARK: ◌〭
hangulSingleDotToneMark = '\x302e' -- @U+302E@ HANGUL SINGLE DOT TONE MARK: ◌〮
hangulDoubleDotToneMark = '\x302f' -- @U+302F@ HANGUL DOUBLE DOT TONE MARK: ◌〯

-- Block: Hiragana--------------------------------------------------------------
katakanaHiraganaVoicedSoundMark, katakanaHiraganaSemiVoicedSoundMark :: Char
katakanaHiraganaVoicedSoundMark = '\x3099' -- @U+3099@ COMBINING KATAKANA-HIRAGANA VOICED SOUND MARK: ◌゙
katakanaHiraganaSemiVoicedSoundMark = '\x309a' -- @U+309A@ COMBINING KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK: ◌゚

-- Block: Cyrillic Extended-B---------------------------------------------------
cyrillicVzmet, cyrillicLetterUkrainianIe, cyrillicLetterI, cyrillicLetterYi, cyrillicLetterU, cyrillicLetterHardSign, cyrillicLetterYeru, cyrillicLetterSoftSign, cyrillicLetterOmega, cyrillicKavyka, cyrillicPayerok, cyrillicLetterEf, cyrillicLetterIotifiedE :: Char
cyrillicVzmet = '\xa66f' -- @U+A66F@ COMBINING CYRILLIC VZMET: ◌꙯
cyrillicLetterUkrainianIe = '\xa674' -- @U+A674@ COMBINING CYRILLIC LETTER UKRAINIAN IE: ◌ꙴ
cyrillicLetterI = '\xa675' -- @U+A675@ COMBINING CYRILLIC LETTER I: ◌ꙵ
cyrillicLetterYi = '\xa676' -- @U+A676@ COMBINING CYRILLIC LETTER YI: ◌ꙶ
cyrillicLetterU = '\xa677' -- @U+A677@ COMBINING CYRILLIC LETTER U: ◌ꙷ
cyrillicLetterHardSign = '\xa678' -- @U+A678@ COMBINING CYRILLIC LETTER HARD SIGN: ◌ꙸ
cyrillicLetterYeru = '\xa679' -- @U+A679@ COMBINING CYRILLIC LETTER YERU: ◌ꙹ
cyrillicLetterSoftSign = '\xa67a' -- @U+A67A@ COMBINING CYRILLIC LETTER SOFT SIGN: ◌ꙺ
cyrillicLetterOmega = '\xa67b' -- @U+A67B@ COMBINING CYRILLIC LETTER OMEGA: ◌ꙻ
cyrillicKavyka = '\xa67c' -- @U+A67C@ COMBINING CYRILLIC KAVYKA: ◌꙼
cyrillicPayerok = '\xa67d' -- @U+A67D@ COMBINING CYRILLIC PAYEROK: ◌꙽
cyrillicLetterEf = '\xa69e' -- @U+A69E@ COMBINING CYRILLIC LETTER EF: ◌ꚞ
cyrillicLetterIotifiedE = '\xa69f' -- @U+A69F@ COMBINING CYRILLIC LETTER IOTIFIED E: ◌ꚟ

-- Block: Bamum-----------------------------------------------------------------
bamumCombiningMarkKoqndon, bamumCombiningMarkTukwentis :: Char
bamumCombiningMarkKoqndon = '\xa6f0' -- @U+A6F0@ BAMUM COMBINING MARK KOQNDON: ◌꛰
bamumCombiningMarkTukwentis = '\xa6f1' -- @U+A6F1@ BAMUM COMBINING MARK TUKWENTIS: ◌꛱

-- Block: Syloti Nagri----------------------------------------------------------
sylotiNagriSignHasanta :: Char
sylotiNagriSignHasanta = '\xa806' -- @U+A806@ SYLOTI NAGRI SIGN HASANTA: ◌꠆

-- Block: Saurashtra------------------------------------------------------------
saurashtraSignVirama :: Char
saurashtraSignVirama = '\xa8c4' -- @U+A8C4@ SAURASHTRA SIGN VIRAMA: ◌꣄

-- Block: Devanagari Extended---------------------------------------------------
devanagariDigitZero, devanagariDigitOne, devanagariDigitTwo, devanagariDigitThree, devanagariDigitFour, devanagariDigitFive, devanagariDigitSix, devanagariDigitSeven, devanagariDigitEight, devanagariDigitNine, devanagariLetterA, devanagariLetterU, devanagariLetterKa, devanagariLetterNa, devanagariLetterPa, devanagariLetterRa, devanagariLetterVi, devanagariSignAvagraha :: Char
devanagariDigitZero = '\xa8e0' -- @U+A8E0@ COMBINING DEVANAGARI DIGIT ZERO: ◌꣠
devanagariDigitOne = '\xa8e1' -- @U+A8E1@ COMBINING DEVANAGARI DIGIT ONE: ◌꣡
devanagariDigitTwo = '\xa8e2' -- @U+A8E2@ COMBINING DEVANAGARI DIGIT TWO: ◌꣢
devanagariDigitThree = '\xa8e3' -- @U+A8E3@ COMBINING DEVANAGARI DIGIT THREE: ◌꣣
devanagariDigitFour = '\xa8e4' -- @U+A8E4@ COMBINING DEVANAGARI DIGIT FOUR: ◌꣤
devanagariDigitFive = '\xa8e5' -- @U+A8E5@ COMBINING DEVANAGARI DIGIT FIVE: ◌꣥
devanagariDigitSix = '\xa8e6' -- @U+A8E6@ COMBINING DEVANAGARI DIGIT SIX: ◌꣦
devanagariDigitSeven = '\xa8e7' -- @U+A8E7@ COMBINING DEVANAGARI DIGIT SEVEN: ◌꣧
devanagariDigitEight = '\xa8e8' -- @U+A8E8@ COMBINING DEVANAGARI DIGIT EIGHT: ◌꣨
devanagariDigitNine = '\xa8e9' -- @U+A8E9@ COMBINING DEVANAGARI DIGIT NINE: ◌꣩
devanagariLetterA = '\xa8ea' -- @U+A8EA@ COMBINING DEVANAGARI LETTER A: ◌꣪
devanagariLetterU = '\xa8eb' -- @U+A8EB@ COMBINING DEVANAGARI LETTER U: ◌꣫
devanagariLetterKa = '\xa8ec' -- @U+A8EC@ COMBINING DEVANAGARI LETTER KA: ◌꣬
devanagariLetterNa = '\xa8ed' -- @U+A8ED@ COMBINING DEVANAGARI LETTER NA: ◌꣭
devanagariLetterPa = '\xa8ee' -- @U+A8EE@ COMBINING DEVANAGARI LETTER PA: ◌꣮
devanagariLetterRa = '\xa8ef' -- @U+A8EF@ COMBINING DEVANAGARI LETTER RA: ◌꣯
devanagariLetterVi = '\xa8f0' -- @U+A8F0@ COMBINING DEVANAGARI LETTER VI: ◌꣰
devanagariSignAvagraha = '\xa8f1' -- @U+A8F1@ COMBINING DEVANAGARI SIGN AVAGRAHA: ◌꣱

-- Block: Kayah Li--------------------------------------------------------------
kayahLiTonePlophu, kayahLiToneCalya, kayahLiToneCalyaPlophu :: Char
kayahLiTonePlophu = '\xa92b' -- @U+A92B@ KAYAH LI TONE PLOPHU: ◌꤫
kayahLiToneCalya = '\xa92c' -- @U+A92C@ KAYAH LI TONE CALYA: ◌꤬
kayahLiToneCalyaPlophu = '\xa92d' -- @U+A92D@ KAYAH LI TONE CALYA PLOPHU: ◌꤭

-- Block: Rejang----------------------------------------------------------------
rejangVirama :: Char
rejangVirama = '\xa953' -- @U+A953@ REJANG VIRAMA: ◌꥓

-- Block: Javanese--------------------------------------------------------------
javaneseSignCecakTelu, javanesePangkon :: Char
javaneseSignCecakTelu = '\xa9b3' -- @U+A9B3@ JAVANESE SIGN CECAK TELU: ◌꦳
javanesePangkon = '\xa9c0' -- @U+A9C0@ JAVANESE PANGKON: ◌꧀

-- Block: Tai Viet--------------------------------------------------------------
taiVietMaiKang, taiVietVowelI, taiVietVowelUe, taiVietVowelU, taiVietMaiKhit, taiVietVowelIa, taiVietVowelAm, taiVietToneMaiEk, taiVietToneMaiTho :: Char
taiVietMaiKang = '\xaab0' -- @U+AAB0@ TAI VIET MAI KANG: ◌ꪰ
taiVietVowelI = '\xaab2' -- @U+AAB2@ TAI VIET VOWEL I: ◌ꪲ
taiVietVowelUe = '\xaab3' -- @U+AAB3@ TAI VIET VOWEL UE: ◌ꪳ
taiVietVowelU = '\xaab4' -- @U+AAB4@ TAI VIET VOWEL U: ◌ꪴ
taiVietMaiKhit = '\xaab7' -- @U+AAB7@ TAI VIET MAI KHIT: ◌ꪷ
taiVietVowelIa = '\xaab8' -- @U+AAB8@ TAI VIET VOWEL IA: ◌ꪸ
taiVietVowelAm = '\xaabe' -- @U+AABE@ TAI VIET VOWEL AM: ◌ꪾ
taiVietToneMaiEk = '\xaabf' -- @U+AABF@ TAI VIET TONE MAI EK: ◌꪿
taiVietToneMaiTho = '\xaac1' -- @U+AAC1@ TAI VIET TONE MAI THO: ◌꫁

-- Block: Meetei Mayek Extensions-----------------------------------------------
meeteiMayekVirama :: Char
meeteiMayekVirama = '\xaaf6' -- @U+AAF6@ MEETEI MAYEK VIRAMA: ◌꫶

-- Block: Meetei Mayek----------------------------------------------------------
meeteiMayekApunIyek :: Char
meeteiMayekApunIyek = '\xabed' -- @U+ABED@ MEETEI MAYEK APUN IYEK: ◌꯭

-- Block: Alphabetic Presentation Forms-----------------------------------------
hebrewPointJudeoSpanishVarika :: Char
hebrewPointJudeoSpanishVarika = '\xfb1e' -- @U+FB1E@ HEBREW POINT JUDEO-SPANISH VARIKA: ◌ﬞ

-- Block: Combining Half Marks--------------------------------------------------
ligatureLeftHalf, ligatureRightHalf, doubleTildeLeftHalf, doubleTildeRightHalf, macronLeftHalf, macronRightHalf, conjoiningMacron, ligatureLeftHalfBelow, ligatureRightHalfBelow, tildeLeftHalfBelow, tildeRightHalfBelow, cyrillicTitloLeftHalf, cyrillicTitloRightHalf :: Char
ligatureLeftHalf = '\xfe20' -- @U+FE20@ COMBINING LIGATURE LEFT HALF: ◌︠
ligatureRightHalf = '\xfe21' -- @U+FE21@ COMBINING LIGATURE RIGHT HALF: ◌︡
doubleTildeLeftHalf = '\xfe22' -- @U+FE22@ COMBINING DOUBLE TILDE LEFT HALF: ◌︢
doubleTildeRightHalf = '\xfe23' -- @U+FE23@ COMBINING DOUBLE TILDE RIGHT HALF: ◌︣
macronLeftHalf = '\xfe24' -- @U+FE24@ COMBINING MACRON LEFT HALF: ◌︤. Alias of 'macronLeftHalfAbove'
macronRightHalf = '\xfe25' -- @U+FE25@ COMBINING MACRON RIGHT HALF: ◌︥. Alias of 'macronRightHalfAbove'
conjoiningMacron = '\xfe26' -- @U+FE26@ COMBINING CONJOINING MACRON: ◌︦. Alias of 'conjoiningMacronAbove'
ligatureLeftHalfBelow = '\xfe27' -- @U+FE27@ COMBINING LIGATURE LEFT HALF BELOW: ◌︧
ligatureRightHalfBelow = '\xfe28' -- @U+FE28@ COMBINING LIGATURE RIGHT HALF BELOW: ◌︨
tildeLeftHalfBelow = '\xfe29' -- @U+FE29@ COMBINING TILDE LEFT HALF BELOW: ◌︩
tildeRightHalfBelow = '\xfe2a' -- @U+FE2A@ COMBINING TILDE RIGHT HALF BELOW: ◌︪
cyrillicTitloLeftHalf = '\xfe2e' -- @U+FE2E@ COMBINING CYRILLIC TITLO LEFT HALF: ◌︮
cyrillicTitloRightHalf = '\xfe2f' -- @U+FE2F@ COMBINING CYRILLIC TITLO RIGHT HALF: ◌︯

-- Block: Phaistos Disc---------------------------------------------------------
phaistosDiscSignCombiningObliqueStroke :: Char
phaistosDiscSignCombiningObliqueStroke = '\x101fd' -- @U+101FD@ PHAISTOS DISC SIGN COMBINING OBLIQUE STROKE: ◌𐇽

-- Block: Coptic Epact Numbers--------------------------------------------------
copticEpactThousandsMark :: Char
copticEpactThousandsMark = '\x102e0' -- @U+102E0@ COPTIC EPACT THOUSANDS MARK: ◌𐋠

-- Block: Old Permic------------------------------------------------------------
oldPermicLetterAn, oldPermicLetterDoi, oldPermicLetterZata, oldPermicLetterNenoe, oldPermicLetterSii :: Char
oldPermicLetterAn = '\x10376' -- @U+10376@ COMBINING OLD PERMIC LETTER AN: ◌𐍶
oldPermicLetterDoi = '\x10377' -- @U+10377@ COMBINING OLD PERMIC LETTER DOI: ◌𐍷
oldPermicLetterZata = '\x10378' -- @U+10378@ COMBINING OLD PERMIC LETTER ZATA: ◌𐍸
oldPermicLetterNenoe = '\x10379' -- @U+10379@ COMBINING OLD PERMIC LETTER NENOE: ◌𐍹
oldPermicLetterSii = '\x1037a' -- @U+1037A@ COMBINING OLD PERMIC LETTER SII: ◌𐍺

-- Block: Kharoshthi------------------------------------------------------------
kharoshthiSignDoubleRingBelow, kharoshthiSignVisarga, kharoshthiSignBarAbove, kharoshthiSignCauda, kharoshthiSignDotBelow, kharoshthiVirama :: Char
kharoshthiSignDoubleRingBelow = '\x10a0d' -- @U+10A0D@ KHAROSHTHI SIGN DOUBLE RING BELOW: ◌𐨍
kharoshthiSignVisarga = '\x10a0f' -- @U+10A0F@ KHAROSHTHI SIGN VISARGA: ◌𐨏
kharoshthiSignBarAbove = '\x10a38' -- @U+10A38@ KHAROSHTHI SIGN BAR ABOVE: ◌𐨸
kharoshthiSignCauda = '\x10a39' -- @U+10A39@ KHAROSHTHI SIGN CAUDA: ◌𐨹
kharoshthiSignDotBelow = '\x10a3a' -- @U+10A3A@ KHAROSHTHI SIGN DOT BELOW: ◌𐨺
kharoshthiVirama = '\x10a3f' -- @U+10A3F@ KHAROSHTHI VIRAMA: ◌𐨿

-- Block: Manichaean------------------------------------------------------------
manichaeanAbbreviationMarkAbove, manichaeanAbbreviationMarkBelow :: Char
manichaeanAbbreviationMarkAbove = '\x10ae5' -- @U+10AE5@ MANICHAEAN ABBREVIATION MARK ABOVE: ◌𐫥
manichaeanAbbreviationMarkBelow = '\x10ae6' -- @U+10AE6@ MANICHAEAN ABBREVIATION MARK BELOW: ◌𐫦

-- Block: Hanifi Rohingya-------------------------------------------------------
hanifiRohingyaSignHarbahay, hanifiRohingyaSignTahala, hanifiRohingyaSignTana, hanifiRohingyaSignTassi :: Char
hanifiRohingyaSignHarbahay = '\x10d24' -- @U+10D24@ HANIFI ROHINGYA SIGN HARBAHAY: ◌𐴤
hanifiRohingyaSignTahala = '\x10d25' -- @U+10D25@ HANIFI ROHINGYA SIGN TAHALA: ◌𐴥
hanifiRohingyaSignTana = '\x10d26' -- @U+10D26@ HANIFI ROHINGYA SIGN TANA: ◌𐴦
hanifiRohingyaSignTassi = '\x10d27' -- @U+10D27@ HANIFI ROHINGYA SIGN TASSI: ◌𐴧

-- Block: Sogdian---------------------------------------------------------------
sogdianCombiningDotBelow, sogdianCombiningTwoDotsBelow, sogdianCombiningDotAbove, sogdianCombiningTwoDotsAbove, sogdianCombiningCurveAbove, sogdianCombiningCurveBelow, sogdianCombiningHookAbove, sogdianCombiningHookBelow, sogdianCombiningLongHookBelow, sogdianCombiningReshBelow, sogdianCombiningStrokeBelow :: Char
sogdianCombiningDotBelow = '\x10f46' -- @U+10F46@ SOGDIAN COMBINING DOT BELOW: ◌𐽆
sogdianCombiningTwoDotsBelow = '\x10f47' -- @U+10F47@ SOGDIAN COMBINING TWO DOTS BELOW: ◌𐽇
sogdianCombiningDotAbove = '\x10f48' -- @U+10F48@ SOGDIAN COMBINING DOT ABOVE: ◌𐽈
sogdianCombiningTwoDotsAbove = '\x10f49' -- @U+10F49@ SOGDIAN COMBINING TWO DOTS ABOVE: ◌𐽉
sogdianCombiningCurveAbove = '\x10f4a' -- @U+10F4A@ SOGDIAN COMBINING CURVE ABOVE: ◌𐽊
sogdianCombiningCurveBelow = '\x10f4b' -- @U+10F4B@ SOGDIAN COMBINING CURVE BELOW: ◌𐽋
sogdianCombiningHookAbove = '\x10f4c' -- @U+10F4C@ SOGDIAN COMBINING HOOK ABOVE: ◌𐽌
sogdianCombiningHookBelow = '\x10f4d' -- @U+10F4D@ SOGDIAN COMBINING HOOK BELOW: ◌𐽍
sogdianCombiningLongHookBelow = '\x10f4e' -- @U+10F4E@ SOGDIAN COMBINING LONG HOOK BELOW: ◌𐽎
sogdianCombiningReshBelow = '\x10f4f' -- @U+10F4F@ SOGDIAN COMBINING RESH BELOW: ◌𐽏
sogdianCombiningStrokeBelow = '\x10f50' -- @U+10F50@ SOGDIAN COMBINING STROKE BELOW: ◌𐽐

-- Block: Brahmi----------------------------------------------------------------
brahmiVirama, brahmiNumberJoiner :: Char
brahmiVirama = '\x11046' -- @U+11046@ BRAHMI VIRAMA: ◌𑁆
brahmiNumberJoiner = '\x1107f' -- @U+1107F@ BRAHMI NUMBER JOINER: ◌𑁿

-- Block: Kaithi----------------------------------------------------------------
kaithiSignVirama, kaithiSignNukta :: Char
kaithiSignVirama = '\x110b9' -- @U+110B9@ KAITHI SIGN VIRAMA: ◌𑂹
kaithiSignNukta = '\x110ba' -- @U+110BA@ KAITHI SIGN NUKTA: ◌𑂺

-- Block: Chakma----------------------------------------------------------------
chakmaSignCandrabindu, chakmaSignAnusvara, chakmaSignVisarga, chakmaVirama, chakmaMaayyaa :: Char
chakmaSignCandrabindu = '\x11100' -- @U+11100@ CHAKMA SIGN CANDRABINDU: ◌𑄀
chakmaSignAnusvara = '\x11101' -- @U+11101@ CHAKMA SIGN ANUSVARA: ◌𑄁
chakmaSignVisarga = '\x11102' -- @U+11102@ CHAKMA SIGN VISARGA: ◌𑄂
chakmaVirama = '\x11133' -- @U+11133@ CHAKMA VIRAMA: ◌𑄳
chakmaMaayyaa = '\x11134' -- @U+11134@ CHAKMA MAAYYAA: ◌𑄴

-- Block: Mahajani--------------------------------------------------------------
mahajaniSignNukta :: Char
mahajaniSignNukta = '\x11173' -- @U+11173@ MAHAJANI SIGN NUKTA: ◌𑅳

-- Block: Sharada---------------------------------------------------------------
sharadaSignVirama, sharadaSignNukta :: Char
sharadaSignVirama = '\x111c0' -- @U+111C0@ SHARADA SIGN VIRAMA: ◌𑇀
sharadaSignNukta = '\x111ca' -- @U+111CA@ SHARADA SIGN NUKTA: ◌𑇊

-- Block: Khojki----------------------------------------------------------------
khojkiSignVirama, khojkiSignNukta :: Char
khojkiSignVirama = '\x11235' -- @U+11235@ KHOJKI SIGN VIRAMA: ◌𑈵
khojkiSignNukta = '\x11236' -- @U+11236@ KHOJKI SIGN NUKTA: ◌𑈶

-- Block: Khudawadi-------------------------------------------------------------
khudawadiSignNukta, khudawadiSignVirama :: Char
khudawadiSignNukta = '\x112e9' -- @U+112E9@ KHUDAWADI SIGN NUKTA: ◌𑋩
khudawadiSignVirama = '\x112ea' -- @U+112EA@ KHUDAWADI SIGN VIRAMA: ◌𑋪

-- Block: Grantha---------------------------------------------------------------
binduBelow, granthaSignNukta, granthaSignVirama, granthaDigitZero, granthaDigitOne, granthaDigitTwo, granthaDigitThree, granthaDigitFour, granthaDigitFive, granthaDigitSix, granthaLetterA, granthaLetterKa, granthaLetterNa, granthaLetterVi, granthaLetterPa :: Char
binduBelow = '\x1133b' -- @U+1133B@ COMBINING BINDU BELOW: ◌𑌻
granthaSignNukta = '\x1133c' -- @U+1133C@ GRANTHA SIGN NUKTA: ◌𑌼
granthaSignVirama = '\x1134d' -- @U+1134D@ GRANTHA SIGN VIRAMA: ◌𑍍
granthaDigitZero = '\x11366' -- @U+11366@ COMBINING GRANTHA DIGIT ZERO: ◌𑍦
granthaDigitOne = '\x11367' -- @U+11367@ COMBINING GRANTHA DIGIT ONE: ◌𑍧
granthaDigitTwo = '\x11368' -- @U+11368@ COMBINING GRANTHA DIGIT TWO: ◌𑍨
granthaDigitThree = '\x11369' -- @U+11369@ COMBINING GRANTHA DIGIT THREE: ◌𑍩
granthaDigitFour = '\x1136a' -- @U+1136A@ COMBINING GRANTHA DIGIT FOUR: ◌𑍪
granthaDigitFive = '\x1136b' -- @U+1136B@ COMBINING GRANTHA DIGIT FIVE: ◌𑍫
granthaDigitSix = '\x1136c' -- @U+1136C@ COMBINING GRANTHA DIGIT SIX: ◌𑍬
granthaLetterA = '\x11370' -- @U+11370@ COMBINING GRANTHA LETTER A: ◌𑍰
granthaLetterKa = '\x11371' -- @U+11371@ COMBINING GRANTHA LETTER KA: ◌𑍱
granthaLetterNa = '\x11372' -- @U+11372@ COMBINING GRANTHA LETTER NA: ◌𑍲
granthaLetterVi = '\x11373' -- @U+11373@ COMBINING GRANTHA LETTER VI: ◌𑍳
granthaLetterPa = '\x11374' -- @U+11374@ COMBINING GRANTHA LETTER PA: ◌𑍴

-- Block: Newa------------------------------------------------------------------
newaSignVirama, newaSignNukta, newaSandhiMark :: Char
newaSignVirama = '\x11442' -- @U+11442@ NEWA SIGN VIRAMA: ◌𑑂
newaSignNukta = '\x11446' -- @U+11446@ NEWA SIGN NUKTA: ◌𑑆
newaSandhiMark = '\x1145e' -- @U+1145E@ NEWA SANDHI MARK: ◌𑑞

-- Block: Tirhuta---------------------------------------------------------------
tirhutaSignVirama, tirhutaSignNukta :: Char
tirhutaSignVirama = '\x114c2' -- @U+114C2@ TIRHUTA SIGN VIRAMA: ◌𑓂
tirhutaSignNukta = '\x114c3' -- @U+114C3@ TIRHUTA SIGN NUKTA: ◌𑓃

-- Block: Siddham---------------------------------------------------------------
siddhamSignVirama, siddhamSignNukta :: Char
siddhamSignVirama = '\x115bf' -- @U+115BF@ SIDDHAM SIGN VIRAMA: ◌𑖿
siddhamSignNukta = '\x115c0' -- @U+115C0@ SIDDHAM SIGN NUKTA: ◌𑗀

-- Block: Modi------------------------------------------------------------------
modiSignVirama :: Char
modiSignVirama = '\x1163f' -- @U+1163F@ MODI SIGN VIRAMA: ◌𑘿

-- Block: Takri-----------------------------------------------------------------
takriSignVirama, takriSignNukta :: Char
takriSignVirama = '\x116b6' -- @U+116B6@ TAKRI SIGN VIRAMA: ◌𑚶
takriSignNukta = '\x116b7' -- @U+116B7@ TAKRI SIGN NUKTA: ◌𑚷

-- Block: Ahom------------------------------------------------------------------
ahomSignKiller :: Char
ahomSignKiller = '\x1172b' -- @U+1172B@ AHOM SIGN KILLER: ◌𑜫

-- Block: Dogra-----------------------------------------------------------------
dograSignVirama, dograSignNukta :: Char
dograSignVirama = '\x11839' -- @U+11839@ DOGRA SIGN VIRAMA: ◌𑠹
dograSignNukta = '\x1183a' -- @U+1183A@ DOGRA SIGN NUKTA: ◌𑠺

-- Block: Nandinagari-----------------------------------------------------------
nandinagariSignVirama :: Char
nandinagariSignVirama = '\x119e0' -- @U+119E0@ NANDINAGARI SIGN VIRAMA: ◌𑧠

-- Block: Zanabazar Square------------------------------------------------------
zanabazarSquareSignVirama, zanabazarSquareSubjoiner :: Char
zanabazarSquareSignVirama = '\x11a34' -- @U+11A34@ ZANABAZAR SQUARE SIGN VIRAMA: ◌𑨴
zanabazarSquareSubjoiner = '\x11a47' -- @U+11A47@ ZANABAZAR SQUARE SUBJOINER: ◌𑩇

-- Block: Soyombo---------------------------------------------------------------
soyomboSubjoiner :: Char
soyomboSubjoiner = '\x11a99' -- @U+11A99@ SOYOMBO SUBJOINER: ◌𑪙

-- Block: Bhaiksuki-------------------------------------------------------------
bhaiksukiSignVirama :: Char
bhaiksukiSignVirama = '\x11c3f' -- @U+11C3F@ BHAIKSUKI SIGN VIRAMA: ◌𑰿

-- Block: Masaram Gondi---------------------------------------------------------
masaramGondiSignNukta, masaramGondiSignHalanta, masaramGondiVirama :: Char
masaramGondiSignNukta = '\x11d42' -- @U+11D42@ MASARAM GONDI SIGN NUKTA: ◌𑵂
masaramGondiSignHalanta = '\x11d44' -- @U+11D44@ MASARAM GONDI SIGN HALANTA: ◌𑵄
masaramGondiVirama = '\x11d45' -- @U+11D45@ MASARAM GONDI VIRAMA: ◌𑵅

-- Block: Gunjala Gondi---------------------------------------------------------
gunjalaGondiVirama :: Char
gunjalaGondiVirama = '\x11d97' -- @U+11D97@ GUNJALA GONDI VIRAMA: ◌𑶗

-- Block: Bassa Vah-------------------------------------------------------------
bassaVahCombiningHighTone, bassaVahCombiningLowTone, bassaVahCombiningMidTone, bassaVahCombiningLowMidTone, bassaVahCombiningHighLowTone :: Char
bassaVahCombiningHighTone = '\x16af0' -- @U+16AF0@ BASSA VAH COMBINING HIGH TONE: ◌𖫰
bassaVahCombiningLowTone = '\x16af1' -- @U+16AF1@ BASSA VAH COMBINING LOW TONE: ◌𖫱
bassaVahCombiningMidTone = '\x16af2' -- @U+16AF2@ BASSA VAH COMBINING MID TONE: ◌𖫲
bassaVahCombiningLowMidTone = '\x16af3' -- @U+16AF3@ BASSA VAH COMBINING LOW-MID TONE: ◌𖫳
bassaVahCombiningHighLowTone = '\x16af4' -- @U+16AF4@ BASSA VAH COMBINING HIGH-LOW TONE: ◌𖫴

-- Block: Pahawh Hmong----------------------------------------------------------
pahawhHmongMarkCimTub, pahawhHmongMarkCimSo, pahawhHmongMarkCimKes, pahawhHmongMarkCimKhav, pahawhHmongMarkCimSuam, pahawhHmongMarkCimHom, pahawhHmongMarkCimTaum :: Char
pahawhHmongMarkCimTub = '\x16b30' -- @U+16B30@ PAHAWH HMONG MARK CIM TUB: ◌𖬰
pahawhHmongMarkCimSo = '\x16b31' -- @U+16B31@ PAHAWH HMONG MARK CIM SO: ◌𖬱
pahawhHmongMarkCimKes = '\x16b32' -- @U+16B32@ PAHAWH HMONG MARK CIM KES: ◌𖬲
pahawhHmongMarkCimKhav = '\x16b33' -- @U+16B33@ PAHAWH HMONG MARK CIM KHAV: ◌𖬳
pahawhHmongMarkCimSuam = '\x16b34' -- @U+16B34@ PAHAWH HMONG MARK CIM SUAM: ◌𖬴
pahawhHmongMarkCimHom = '\x16b35' -- @U+16B35@ PAHAWH HMONG MARK CIM HOM: ◌𖬵
pahawhHmongMarkCimTaum = '\x16b36' -- @U+16B36@ PAHAWH HMONG MARK CIM TAUM: ◌𖬶

-- Block: Duployan--------------------------------------------------------------
duployanDoubleMark :: Char
duployanDoubleMark = '\x1bc9e' -- @U+1BC9E@ DUPLOYAN DOUBLE MARK: ◌𛲞

-- Block: Musical Symbols-------------------------------------------------------
musicalSymbolCombiningStem, musicalSymbolCombiningSprechgesangStem, musicalSymbolCombiningTremolo1, musicalSymbolCombiningTremolo2, musicalSymbolCombiningTremolo3, musicalSymbolCombiningAugmentationDot, musicalSymbolCombiningFlag1, musicalSymbolCombiningFlag2, musicalSymbolCombiningFlag3, musicalSymbolCombiningFlag4, musicalSymbolCombiningFlag5, musicalSymbolCombiningAccent, musicalSymbolCombiningStaccato, musicalSymbolCombiningTenuto, musicalSymbolCombiningStaccatissimo, musicalSymbolCombiningMarcato, musicalSymbolCombiningMarcatoStaccato, musicalSymbolCombiningAccentStaccato, musicalSymbolCombiningLoure, musicalSymbolCombiningDoit, musicalSymbolCombiningRip, musicalSymbolCombiningFlip, musicalSymbolCombiningSmear, musicalSymbolCombiningBend, musicalSymbolCombiningDoubleTongue, musicalSymbolCombiningTripleTongue, musicalSymbolCombiningDownBow, musicalSymbolCombiningUpBow, musicalSymbolCombiningHarmonic, musicalSymbolCombiningSnapPizzicato :: Char
musicalSymbolCombiningStem = '\x1d165' -- @U+1D165@ MUSICAL SYMBOL COMBINING STEM: ◌𝅥
musicalSymbolCombiningSprechgesangStem = '\x1d166' -- @U+1D166@ MUSICAL SYMBOL COMBINING SPRECHGESANG STEM: ◌𝅦
musicalSymbolCombiningTremolo1 = '\x1d167' -- @U+1D167@ MUSICAL SYMBOL COMBINING TREMOLO-1: ◌𝅧
musicalSymbolCombiningTremolo2 = '\x1d168' -- @U+1D168@ MUSICAL SYMBOL COMBINING TREMOLO-2: ◌𝅨
musicalSymbolCombiningTremolo3 = '\x1d169' -- @U+1D169@ MUSICAL SYMBOL COMBINING TREMOLO-3: ◌𝅩
musicalSymbolCombiningAugmentationDot = '\x1d16d' -- @U+1D16D@ MUSICAL SYMBOL COMBINING AUGMENTATION DOT: ◌𝅭
musicalSymbolCombiningFlag1 = '\x1d16e' -- @U+1D16E@ MUSICAL SYMBOL COMBINING FLAG-1: ◌𝅮
musicalSymbolCombiningFlag2 = '\x1d16f' -- @U+1D16F@ MUSICAL SYMBOL COMBINING FLAG-2: ◌𝅯
musicalSymbolCombiningFlag3 = '\x1d170' -- @U+1D170@ MUSICAL SYMBOL COMBINING FLAG-3: ◌𝅰
musicalSymbolCombiningFlag4 = '\x1d171' -- @U+1D171@ MUSICAL SYMBOL COMBINING FLAG-4: ◌𝅱
musicalSymbolCombiningFlag5 = '\x1d172' -- @U+1D172@ MUSICAL SYMBOL COMBINING FLAG-5: ◌𝅲
musicalSymbolCombiningAccent = '\x1d17b' -- @U+1D17B@ MUSICAL SYMBOL COMBINING ACCENT: ◌𝅻
musicalSymbolCombiningStaccato = '\x1d17c' -- @U+1D17C@ MUSICAL SYMBOL COMBINING STACCATO: ◌𝅼
musicalSymbolCombiningTenuto = '\x1d17d' -- @U+1D17D@ MUSICAL SYMBOL COMBINING TENUTO: ◌𝅽
musicalSymbolCombiningStaccatissimo = '\x1d17e' -- @U+1D17E@ MUSICAL SYMBOL COMBINING STACCATISSIMO: ◌𝅾
musicalSymbolCombiningMarcato = '\x1d17f' -- @U+1D17F@ MUSICAL SYMBOL COMBINING MARCATO: ◌𝅿
musicalSymbolCombiningMarcatoStaccato = '\x1d180' -- @U+1D180@ MUSICAL SYMBOL COMBINING MARCATO-STACCATO: ◌𝆀
musicalSymbolCombiningAccentStaccato = '\x1d181' -- @U+1D181@ MUSICAL SYMBOL COMBINING ACCENT-STACCATO: ◌𝆁
musicalSymbolCombiningLoure = '\x1d182' -- @U+1D182@ MUSICAL SYMBOL COMBINING LOURE: ◌𝆂
musicalSymbolCombiningDoit = '\x1d185' -- @U+1D185@ MUSICAL SYMBOL COMBINING DOIT: ◌𝆅
musicalSymbolCombiningRip = '\x1d186' -- @U+1D186@ MUSICAL SYMBOL COMBINING RIP: ◌𝆆
musicalSymbolCombiningFlip = '\x1d187' -- @U+1D187@ MUSICAL SYMBOL COMBINING FLIP: ◌𝆇
musicalSymbolCombiningSmear = '\x1d188' -- @U+1D188@ MUSICAL SYMBOL COMBINING SMEAR: ◌𝆈
musicalSymbolCombiningBend = '\x1d189' -- @U+1D189@ MUSICAL SYMBOL COMBINING BEND: ◌𝆉
musicalSymbolCombiningDoubleTongue = '\x1d18a' -- @U+1D18A@ MUSICAL SYMBOL COMBINING DOUBLE TONGUE: ◌𝆊
musicalSymbolCombiningTripleTongue = '\x1d18b' -- @U+1D18B@ MUSICAL SYMBOL COMBINING TRIPLE TONGUE: ◌𝆋
musicalSymbolCombiningDownBow = '\x1d1aa' -- @U+1D1AA@ MUSICAL SYMBOL COMBINING DOWN BOW: ◌𝆪
musicalSymbolCombiningUpBow = '\x1d1ab' -- @U+1D1AB@ MUSICAL SYMBOL COMBINING UP BOW: ◌𝆫
musicalSymbolCombiningHarmonic = '\x1d1ac' -- @U+1D1AC@ MUSICAL SYMBOL COMBINING HARMONIC: ◌𝆬
musicalSymbolCombiningSnapPizzicato = '\x1d1ad' -- @U+1D1AD@ MUSICAL SYMBOL COMBINING SNAP PIZZICATO: ◌𝆭

-- Block: Ancient Greek Musical Notation----------------------------------------
greekMusicalTriseme, greekMusicalTetraseme, greekMusicalPentaseme :: Char
greekMusicalTriseme = '\x1d242' -- @U+1D242@ COMBINING GREEK MUSICAL TRISEME: ◌𝉂
greekMusicalTetraseme = '\x1d243' -- @U+1D243@ COMBINING GREEK MUSICAL TETRASEME: ◌𝉃
greekMusicalPentaseme = '\x1d244' -- @U+1D244@ COMBINING GREEK MUSICAL PENTASEME: ◌𝉄

-- Block: Glagolitic Supplement-------------------------------------------------
glagoliticLetterAzu, glagoliticLetterBuky, glagoliticLetterVede, glagoliticLetterGlagoli, glagoliticLetterDobro, glagoliticLetterYestu, glagoliticLetterZhivete, glagoliticLetterZemlja, glagoliticLetterIzhe, glagoliticLetterInitialIzhe, glagoliticLetterI, glagoliticLetterDjervi, glagoliticLetterKako, glagoliticLetterLjudije, glagoliticLetterMyslite, glagoliticLetterNashi, glagoliticLetterOnu, glagoliticLetterPokoji, glagoliticLetterRitsi, glagoliticLetterSlovo, glagoliticLetterTvrido, glagoliticLetterUku, glagoliticLetterFritu, glagoliticLetterHeru, glagoliticLetterShta, glagoliticLetterTsi, glagoliticLetterChrivi, glagoliticLetterSha, glagoliticLetterYeru, glagoliticLetterYeri, glagoliticLetterYati, glagoliticLetterYu, glagoliticLetterSmallYus, glagoliticLetterYo, glagoliticLetterIotatedSmallYus, glagoliticLetterBigYus, glagoliticLetterIotatedBigYus, glagoliticLetterFita :: Char
glagoliticLetterAzu = '\x1e000' -- @U+1E000@ COMBINING GLAGOLITIC LETTER AZU: ◌𞀀
glagoliticLetterBuky = '\x1e001' -- @U+1E001@ COMBINING GLAGOLITIC LETTER BUKY: ◌𞀁
glagoliticLetterVede = '\x1e002' -- @U+1E002@ COMBINING GLAGOLITIC LETTER VEDE: ◌𞀂
glagoliticLetterGlagoli = '\x1e003' -- @U+1E003@ COMBINING GLAGOLITIC LETTER GLAGOLI: ◌𞀃
glagoliticLetterDobro = '\x1e004' -- @U+1E004@ COMBINING GLAGOLITIC LETTER DOBRO: ◌𞀄
glagoliticLetterYestu = '\x1e005' -- @U+1E005@ COMBINING GLAGOLITIC LETTER YESTU: ◌𞀅
glagoliticLetterZhivete = '\x1e006' -- @U+1E006@ COMBINING GLAGOLITIC LETTER ZHIVETE: ◌𞀆
glagoliticLetterZemlja = '\x1e008' -- @U+1E008@ COMBINING GLAGOLITIC LETTER ZEMLJA: ◌𞀈
glagoliticLetterIzhe = '\x1e009' -- @U+1E009@ COMBINING GLAGOLITIC LETTER IZHE: ◌𞀉
glagoliticLetterInitialIzhe = '\x1e00a' -- @U+1E00A@ COMBINING GLAGOLITIC LETTER INITIAL IZHE: ◌𞀊
glagoliticLetterI = '\x1e00b' -- @U+1E00B@ COMBINING GLAGOLITIC LETTER I: ◌𞀋
glagoliticLetterDjervi = '\x1e00c' -- @U+1E00C@ COMBINING GLAGOLITIC LETTER DJERVI: ◌𞀌
glagoliticLetterKako = '\x1e00d' -- @U+1E00D@ COMBINING GLAGOLITIC LETTER KAKO: ◌𞀍
glagoliticLetterLjudije = '\x1e00e' -- @U+1E00E@ COMBINING GLAGOLITIC LETTER LJUDIJE: ◌𞀎
glagoliticLetterMyslite = '\x1e00f' -- @U+1E00F@ COMBINING GLAGOLITIC LETTER MYSLITE: ◌𞀏
glagoliticLetterNashi = '\x1e010' -- @U+1E010@ COMBINING GLAGOLITIC LETTER NASHI: ◌𞀐
glagoliticLetterOnu = '\x1e011' -- @U+1E011@ COMBINING GLAGOLITIC LETTER ONU: ◌𞀑
glagoliticLetterPokoji = '\x1e012' -- @U+1E012@ COMBINING GLAGOLITIC LETTER POKOJI: ◌𞀒
glagoliticLetterRitsi = '\x1e013' -- @U+1E013@ COMBINING GLAGOLITIC LETTER RITSI: ◌𞀓
glagoliticLetterSlovo = '\x1e014' -- @U+1E014@ COMBINING GLAGOLITIC LETTER SLOVO: ◌𞀔
glagoliticLetterTvrido = '\x1e015' -- @U+1E015@ COMBINING GLAGOLITIC LETTER TVRIDO: ◌𞀕
glagoliticLetterUku = '\x1e016' -- @U+1E016@ COMBINING GLAGOLITIC LETTER UKU: ◌𞀖
glagoliticLetterFritu = '\x1e017' -- @U+1E017@ COMBINING GLAGOLITIC LETTER FRITU: ◌𞀗
glagoliticLetterHeru = '\x1e018' -- @U+1E018@ COMBINING GLAGOLITIC LETTER HERU: ◌𞀘
glagoliticLetterShta = '\x1e01b' -- @U+1E01B@ COMBINING GLAGOLITIC LETTER SHTA: ◌𞀛
glagoliticLetterTsi = '\x1e01c' -- @U+1E01C@ COMBINING GLAGOLITIC LETTER TSI: ◌𞀜
glagoliticLetterChrivi = '\x1e01d' -- @U+1E01D@ COMBINING GLAGOLITIC LETTER CHRIVI: ◌𞀝
glagoliticLetterSha = '\x1e01e' -- @U+1E01E@ COMBINING GLAGOLITIC LETTER SHA: ◌𞀞
glagoliticLetterYeru = '\x1e01f' -- @U+1E01F@ COMBINING GLAGOLITIC LETTER YERU: ◌𞀟
glagoliticLetterYeri = '\x1e020' -- @U+1E020@ COMBINING GLAGOLITIC LETTER YERI: ◌𞀠
glagoliticLetterYati = '\x1e021' -- @U+1E021@ COMBINING GLAGOLITIC LETTER YATI: ◌𞀡
glagoliticLetterYu = '\x1e023' -- @U+1E023@ COMBINING GLAGOLITIC LETTER YU: ◌𞀣
glagoliticLetterSmallYus = '\x1e024' -- @U+1E024@ COMBINING GLAGOLITIC LETTER SMALL YUS: ◌𞀤
glagoliticLetterYo = '\x1e026' -- @U+1E026@ COMBINING GLAGOLITIC LETTER YO: ◌𞀦
glagoliticLetterIotatedSmallYus = '\x1e027' -- @U+1E027@ COMBINING GLAGOLITIC LETTER IOTATED SMALL YUS: ◌𞀧
glagoliticLetterBigYus = '\x1e028' -- @U+1E028@ COMBINING GLAGOLITIC LETTER BIG YUS: ◌𞀨
glagoliticLetterIotatedBigYus = '\x1e029' -- @U+1E029@ COMBINING GLAGOLITIC LETTER IOTATED BIG YUS: ◌𞀩
glagoliticLetterFita = '\x1e02a' -- @U+1E02A@ COMBINING GLAGOLITIC LETTER FITA: ◌𞀪

-- Block: Nyiakeng Puachue Hmong------------------------------------------------
nyiakengPuachueHmongToneB, nyiakengPuachueHmongToneM, nyiakengPuachueHmongToneJ, nyiakengPuachueHmongToneV, nyiakengPuachueHmongToneS, nyiakengPuachueHmongToneG, nyiakengPuachueHmongToneD :: Char
nyiakengPuachueHmongToneB = '\x1e130' -- @U+1E130@ NYIAKENG PUACHUE HMONG TONE-B: ◌𞄰
nyiakengPuachueHmongToneM = '\x1e131' -- @U+1E131@ NYIAKENG PUACHUE HMONG TONE-M: ◌𞄱
nyiakengPuachueHmongToneJ = '\x1e132' -- @U+1E132@ NYIAKENG PUACHUE HMONG TONE-J: ◌𞄲
nyiakengPuachueHmongToneV = '\x1e133' -- @U+1E133@ NYIAKENG PUACHUE HMONG TONE-V: ◌𞄳
nyiakengPuachueHmongToneS = '\x1e134' -- @U+1E134@ NYIAKENG PUACHUE HMONG TONE-S: ◌𞄴
nyiakengPuachueHmongToneG = '\x1e135' -- @U+1E135@ NYIAKENG PUACHUE HMONG TONE-G: ◌𞄵
nyiakengPuachueHmongToneD = '\x1e136' -- @U+1E136@ NYIAKENG PUACHUE HMONG TONE-D: ◌𞄶

-- Block: Wancho----------------------------------------------------------------
wanchoToneTup, wanchoToneTupni, wanchoToneKoi, wanchoToneKoini :: Char
wanchoToneTup = '\x1e2ec' -- @U+1E2EC@ WANCHO TONE TUP: ◌𞋬
wanchoToneTupni = '\x1e2ed' -- @U+1E2ED@ WANCHO TONE TUPNI: ◌𞋭
wanchoToneKoi = '\x1e2ee' -- @U+1E2EE@ WANCHO TONE KOI: ◌𞋮
wanchoToneKoini = '\x1e2ef' -- @U+1E2EF@ WANCHO TONE KOINI: ◌𞋯

-- Block: Mende Kikakui---------------------------------------------------------
mendeKikakuiCombiningNumberTeens, mendeKikakuiCombiningNumberTens, mendeKikakuiCombiningNumberHundreds, mendeKikakuiCombiningNumberThousands, mendeKikakuiCombiningNumberTenThousands, mendeKikakuiCombiningNumberHundredThousands, mendeKikakuiCombiningNumberMillions :: Char
mendeKikakuiCombiningNumberTeens = '\x1e8d0' -- @U+1E8D0@ MENDE KIKAKUI COMBINING NUMBER TEENS: ◌𞣐
mendeKikakuiCombiningNumberTens = '\x1e8d1' -- @U+1E8D1@ MENDE KIKAKUI COMBINING NUMBER TENS: ◌𞣑
mendeKikakuiCombiningNumberHundreds = '\x1e8d2' -- @U+1E8D2@ MENDE KIKAKUI COMBINING NUMBER HUNDREDS: ◌𞣒
mendeKikakuiCombiningNumberThousands = '\x1e8d3' -- @U+1E8D3@ MENDE KIKAKUI COMBINING NUMBER THOUSANDS: ◌𞣓
mendeKikakuiCombiningNumberTenThousands = '\x1e8d4' -- @U+1E8D4@ MENDE KIKAKUI COMBINING NUMBER TEN THOUSANDS: ◌𞣔
mendeKikakuiCombiningNumberHundredThousands = '\x1e8d5' -- @U+1E8D5@ MENDE KIKAKUI COMBINING NUMBER HUNDRED THOUSANDS: ◌𞣕
mendeKikakuiCombiningNumberMillions = '\x1e8d6' -- @U+1E8D6@ MENDE KIKAKUI COMBINING NUMBER MILLIONS: ◌𞣖

-- Block: Adlam-----------------------------------------------------------------
adlamAlifLengthener, adlamVowelLengthener, adlamGeminationMark, adlamHamza, adlamConsonantModifier, adlamGeminateConsonantModifier, adlamNukta :: Char
adlamAlifLengthener = '\x1e944' -- @U+1E944@ ADLAM ALIF LENGTHENER: ◌𞥄
adlamVowelLengthener = '\x1e945' -- @U+1E945@ ADLAM VOWEL LENGTHENER: ◌𞥅
adlamGeminationMark = '\x1e946' -- @U+1E946@ ADLAM GEMINATION MARK: ◌𞥆
adlamHamza = '\x1e947' -- @U+1E947@ ADLAM HAMZA: ◌𞥇
adlamConsonantModifier = '\x1e948' -- @U+1E948@ ADLAM CONSONANT MODIFIER: ◌𞥈
adlamGeminateConsonantModifier = '\x1e949' -- @U+1E949@ ADLAM GEMINATE CONSONANT MODIFIER: ◌𞥉
adlamNukta = '\x1e94a' -- @U+1E94A@ ADLAM NUKTA: ◌𞥊

allCombiningChars :: String
allCombiningChars =
  [ graveAbove
  , graveBelow
  , doubleGrave
  , doubledGrave
  , acuteAbove
  , acuteBelow
  , doubleAcute
  , doubledAcute
  , circumflexAbove
  , circumflexBelow
  , doubledCircumflexAbove
  , hatchekAbove
  , caronAbove
  , hatchekBelow
  , caronBelow
  , tildeAbove
  , tildeBelow
  , doubleTildeAbove
  , breveAbove
  , breveBelow
  , doubleBreveAbove
  , doubleBreveBelow
  , invertedBreveAbove
  , invertedBreveBelow
  , doubleInvertedBreveAbove
  , doubleInvertedBreveBelow
  , horn
  , hookAbove
  , palatalizedHookBelow
  , retroflexHookBelow
  , macronAbove
  , macronBelow
  , doubleMacronAbove
  , doubleMacronBelow
  , macronLeftHalfAbove
  , macronRightHalfAbove
  , conjoiningMacronAbove
  , macronLeftHalfBelow
  , macronRightHalfBelow
  , conjoiningMacronBelow
  , overline
  , lowLine
  , doubleOverline
  , doubleLowLine
  , dotAbove
  , dotBelow
  , diaeresisAbove
  , diaeresisBelow
  , threeDotsAbove
  , threeDotsBelow
  , fourDotsAbove
  , shortStroke
  , longStroke
  , shortVerticalLine
  , longVerticalLine
  , doubledLongVerticalLine
  , verticalLineAbove
  , doubledVerticalLineAbove
  , verticalLineBelow
  , doubledVerticalLineBelow
  , shortSolidus
  , longSolidus
  , reverseLongSolidus
  , doubledLongSolidus
  , tildeOverlay
  , ringAbove
  , ringBelow
  , doubledRingBelow
  , cedillaBelow
  , cedillaAbove
  , commaAbove
  , commaAboveRight
  , reversedCommaAbove
  , commaBelow
  , turnedCommaAbove
  , ogonek
  , plusSignBelow
  , minusSignBelow
  , ringOverlay
  , clockwiseRingOverlay
  , anticlockwiseRingOverlay
  , enclosingCircle
  , enclosingSquare
  , enclosingDiamond
  , enclosingCircleBackslash
  , enclosingKeycap
  , enclosingTriangle
  , clockwiseArrowAbove
  , anticlockwiseArrowAbove
  , leftwardsArrowAbove
  , leftwardsArrowBelow
  , rightwardsArrowAbove
  , rightwardsArrowBelow
  , doubleRightwardsArrowBelow
  , asteriskAbove
  , asteriskBelow
  , infinityAbove
  , wigglyLineBelow
  , graveAccent
  , acuteAccent
  , circumflexAccent
  , tilde
  , macron
  , breve
  , diaeresis
  , doubleAcuteAccent
  , caron
  , doubleVerticalLineAbove
  , doubleGraveAccent
  , candrabindu
  , invertedBreve
  , graveAccentBelow
  , acuteAccentBelow
  , leftTackBelow
  , rightTackBelow
  , leftAngleAbove
  , leftHalfRingBelow
  , upTackBelow
  , downTackBelow
  , cedilla
  , bridgeBelow
  , invertedDoubleArchBelow
  , circumflexAccentBelow
  , shortStrokeOverlay
  , longStrokeOverlay
  , shortSolidusOverlay
  , longSolidusOverlay
  , rightHalfRingBelow
  , invertedBridgeBelow
  , squareBelow
  , seagullBelow
  , xAbove
  , verticalTilde
  , graveToneMark
  , acuteToneMark
  , greekPerispomeni
  , greekKoronis
  , greekDialytikaTonos
  , greekYpogegrammeni
  , bridgeAbove
  , equalsSignBelow
  , doubleVerticalLineBelow
  , leftAngleBelow
  , notTildeAbove
  , homotheticAbove
  , almostEqualToAbove
  , leftRightArrowBelow
  , upwardsArrowBelow
  , rightArrowheadAbove
  , leftHalfRingAbove
  , fermata
  , xBelow
  , leftArrowheadBelow
  , rightArrowheadBelow
  , rightArrowheadAndUpArrowheadBelow
  , rightHalfRingAbove
  , dotAboveRight
  , doubleRingBelow
  , zigzagAbove
  , doubleBreve
  , doubleMacron
  , doubleTilde
  , doubleInvertedBreve
  , latinSmallLetterA
  , latinSmallLetterE
  , latinSmallLetterI
  , latinSmallLetterO
  , latinSmallLetterU
  , latinSmallLetterC
  , latinSmallLetterD
  , latinSmallLetterH
  , latinSmallLetterM
  , latinSmallLetterR
  , latinSmallLetterT
  , latinSmallLetterV
  , latinSmallLetterX
  , cyrillicTitlo
  , cyrillicPalatalization
  , cyrillicDasiaPneumata
  , cyrillicPsiliPneumata
  , cyrillicPokrytie
  , hebrewAccentEtnahta
  , hebrewAccentSegol
  , hebrewAccentShalshelet
  , hebrewAccentZaqefQatan
  , hebrewAccentZaqefGadol
  , hebrewAccentTipeha
  , hebrewAccentRevia
  , hebrewAccentZarqa
  , hebrewAccentPashta
  , hebrewAccentYetiv
  , hebrewAccentTevir
  , hebrewAccentGeresh
  , hebrewAccentGereshMuqdam
  , hebrewAccentGershayim
  , hebrewAccentQarneyPara
  , hebrewAccentTelishaGedola
  , hebrewAccentPazer
  , hebrewAccentAtnahHafukh
  , hebrewAccentMunah
  , hebrewAccentMahapakh
  , hebrewAccentMerkha
  , hebrewAccentMerkhaKefula
  , hebrewAccentDarga
  , hebrewAccentQadma
  , hebrewAccentTelishaQetana
  , hebrewAccentYerahBenYomo
  , hebrewAccentOle
  , hebrewAccentIluy
  , hebrewAccentDehi
  , hebrewAccentZinor
  , hebrewMarkMasoraCircle
  , hebrewPointSheva
  , hebrewPointHatafSegol
  , hebrewPointHatafPatah
  , hebrewPointHatafQamats
  , hebrewPointHiriq
  , hebrewPointTsere
  , hebrewPointSegol
  , hebrewPointPatah
  , hebrewPointQamats
  , hebrewPointHolam
  , hebrewPointHolamHaserForVav
  , hebrewPointQubuts
  , hebrewPointDageshOrMapiq
  , hebrewPointMeteg
  , hebrewPointRafe
  , hebrewPointShinDot
  , hebrewPointSinDot
  , hebrewMarkUpperDot
  , hebrewMarkLowerDot
  , hebrewPointQamatsQatan
  , arabicSignSallallahouAlayheWassallam
  , arabicSignAlayheAssallam
  , arabicSignRahmatullahAlayhe
  , arabicSignRadiAllahouAnhu
  , arabicSignTakhallus
  , arabicSmallHighTah
  , arabicSmallHighLigatureAlefWithLamWithYeh
  , arabicSmallHighZain
  , arabicSmallFatha
  , arabicSmallDamma
  , arabicSmallKasra
  , arabicFathatan
  , arabicDammatan
  , arabicKasratan
  , arabicFatha
  , arabicDamma
  , arabicKasra
  , arabicShadda
  , arabicSukun
  , arabicMaddahAbove
  , arabicHamzaAbove
  , arabicHamzaBelow
  , arabicSubscriptAlef
  , arabicInvertedDamma
  , arabicMarkNoonGhunna
  , arabicZwarakay
  , arabicVowelSignSmallVAbove
  , arabicVowelSignInvertedSmallVAbove
  , arabicVowelSignDotBelow
  , arabicReversedDamma
  , arabicFathaWithTwoDots
  , arabicWavyHamzaBelow
  , arabicLetterSuperscriptAlef
  , arabicSmallHighLigatureSadWithLamWithAlefMaksura
  , arabicSmallHighLigatureQafWithLamWithAlefMaksura
  , arabicSmallHighMeemInitialForm
  , arabicSmallHighLamAlef
  , arabicSmallHighJeem
  , arabicSmallHighThreeDots
  , arabicSmallHighSeen
  , arabicSmallHighRoundedZero
  , arabicSmallHighUprightRectangularZero
  , arabicSmallHighDotlessHeadOfKhah
  , arabicSmallHighMeemIsolatedForm
  , arabicSmallLowSeen
  , arabicSmallHighMadda
  , arabicSmallHighYeh
  , arabicSmallHighNoon
  , arabicEmptyCentreLowStop
  , arabicEmptyCentreHighStop
  , arabicRoundedHighStopWithFilledCentre
  , arabicSmallLowMeem
  , syriacLetterSuperscriptAlaph
  , syriacPthahaAbove
  , syriacPthahaBelow
  , syriacPthahaDotted
  , syriacZqaphaAbove
  , syriacZqaphaBelow
  , syriacZqaphaDotted
  , syriacRbasaAbove
  , syriacRbasaBelow
  , syriacDottedZlamaHorizontal
  , syriacDottedZlamaAngular
  , syriacHbasaAbove
  , syriacHbasaBelow
  , syriacHbasaEsasaDotted
  , syriacEsasaAbove
  , syriacEsasaBelow
  , syriacRwaha
  , syriacFeminineDot
  , syriacQushshaya
  , syriacRukkakha
  , syriacTwoVerticalDotsAbove
  , syriacTwoVerticalDotsBelow
  , syriacThreeDotsAbove
  , syriacThreeDotsBelow
  , syriacObliqueLineAbove
  , syriacObliqueLineBelow
  , syriacMusic
  , syriacBarrekh
  , nkoCombiningShortHighTone
  , nkoCombiningShortLowTone
  , nkoCombiningShortRisingTone
  , nkoCombiningLongDescendingTone
  , nkoCombiningLongHighTone
  , nkoCombiningLongLowTone
  , nkoCombiningLongRisingTone
  , nkoCombiningNasalizationMark
  , nkoCombiningDoubleDotAbove
  , nkoDantayalan
  , samaritanMarkIn
  , samaritanMarkInAlaf
  , samaritanMarkOcclusion
  , samaritanMarkDagesh
  , samaritanMarkEpentheticYut
  , samaritanVowelSignLongE
  , samaritanVowelSignE
  , samaritanVowelSignOverlongAa
  , samaritanVowelSignLongAa
  , samaritanVowelSignAa
  , samaritanVowelSignOverlongA
  , samaritanVowelSignLongA
  , samaritanVowelSignA
  , samaritanVowelSignShortA
  , samaritanVowelSignLongU
  , samaritanVowelSignU
  , samaritanVowelSignLongI
  , samaritanVowelSignI
  , samaritanVowelSignO
  , samaritanVowelSignSukun
  , samaritanMarkNequdaa
  , mandaicAffricationMark
  , mandaicVocalizationMark
  , mandaicGeminationMark
  , arabicSmallLowWaw
  , arabicSmallHighWordArRub
  , arabicSmallHighSad
  , arabicSmallHighAin
  , arabicSmallHighQaf
  , arabicSmallHighNoonWithKasra
  , arabicSmallLowNoonWithKasra
  , arabicSmallHighWordAthThalatha
  , arabicSmallHighWordAsSajda
  , arabicSmallHighWordAnNisf
  , arabicSmallHighWordSakta
  , arabicSmallHighWordQif
  , arabicSmallHighWordWaqfa
  , arabicSmallHighFootnoteMarker
  , arabicSmallHighSignSafha
  , arabicTurnedDammaBelow
  , arabicCurlyFatha
  , arabicCurlyDamma
  , arabicCurlyKasra
  , arabicCurlyFathatan
  , arabicCurlyDammatan
  , arabicCurlyKasratan
  , arabicToneOneDotAbove
  , arabicToneTwoDotsAbove
  , arabicToneLoopAbove
  , arabicToneOneDotBelow
  , arabicToneTwoDotsBelow
  , arabicToneLoopBelow
  , arabicOpenFathatan
  , arabicOpenDammatan
  , arabicOpenKasratan
  , arabicSmallHighWaw
  , arabicFathaWithRing
  , arabicFathaWithDotAbove
  , arabicKasraWithDotBelow
  , arabicLeftArrowheadAbove
  , arabicRightArrowheadAbove
  , arabicLeftArrowheadBelow
  , arabicRightArrowheadBelow
  , arabicDoubleRightArrowheadAbove
  , arabicDoubleRightArrowheadAboveWithDot
  , arabicRightArrowheadAboveWithDot
  , arabicDammaWithDot
  , arabicMarkSidewaysNoonGhunna
  , devanagariSignNukta
  , devanagariSignVirama
  , devanagariStressSignUdatta
  , devanagariStressSignAnudatta
  , devanagariGraveAccent
  , devanagariAcuteAccent
  , bengaliSignNukta
  , bengaliSignVirama
  , bengaliSandhiMark
  , gurmukhiSignNukta
  , gurmukhiSignVirama
  , gujaratiSignNukta
  , gujaratiSignVirama
  , oriyaSignNukta
  , oriyaSignVirama
  , tamilSignVirama
  , teluguSignVirama
  , teluguLengthMark
  , teluguAiLengthMark
  , kannadaSignNukta
  , kannadaSignVirama
  , malayalamSignVerticalBarVirama
  , malayalamSignCircularVirama
  , malayalamSignVirama
  , sinhalaSignAlLakuna
  , thaiCharacterSaraU
  , thaiCharacterSaraUu
  , thaiCharacterPhinthu
  , thaiCharacterMaiEk
  , thaiCharacterMaiTho
  , thaiCharacterMaiTri
  , thaiCharacterMaiChattawa
  , laoVowelSignU
  , laoVowelSignUu
  , laoSignPaliVirama
  , laoToneMaiEk
  , laoToneMaiTho
  , laoToneMaiTi
  , laoToneMaiCatawa
  , tibetanAstrologicalSignKhyudPa
  , tibetanAstrologicalSignSdongTshugs
  , tibetanMarkNgasBzungNyiZla
  , tibetanMarkNgasBzungSgorRtags
  , tibetanMarkTsaPhru
  , tibetanVowelSignAa
  , tibetanVowelSignI
  , tibetanVowelSignU
  , tibetanVowelSignE
  , tibetanVowelSignEe
  , tibetanVowelSignO
  , tibetanVowelSignOo
  , tibetanVowelSignReversedI
  , tibetanSignNyiZlaNaaDa
  , tibetanSignSnaLdan
  , tibetanMarkHalanta
  , tibetanSignLciRtags
  , tibetanSignYangRtags
  , tibetanSymbolPadmaGdan
  , myanmarSignDotBelow
  , myanmarSignVirama
  , myanmarSignAsat
  , myanmarSignShanCouncilEmphaticTone
  , ethiopicCombiningGeminationAndVowelLengthMark
  , ethiopicCombiningVowelLengthMark
  , ethiopicCombiningGeminationMark
  , tagalogSignVirama
  , hanunooSignPamudpod
  , khmerSignCoeng
  , khmerSignAtthacan
  , mongolianLetterAliGaliDagalga
  , limbuSignMukphreng
  , limbuSignKemphreng
  , limbuSignSaI
  , bugineseVowelSignI
  , bugineseVowelSignU
  , taiThamSignSakot
  , taiThamSignTone1
  , taiThamSignTone2
  , taiThamSignKhuenTone3
  , taiThamSignKhuenTone4
  , taiThamSignKhuenTone5
  , taiThamSignRaHaam
  , taiThamSignMaiSam
  , taiThamSignKhuenLueKaran
  , taiThamCombiningCryptogrammicDot
  , doubledCircumflexAccent
  , diaeresisRing
  , infinity
  , downwardsArrow
  , tripleDot
  , xXBelow
  , openMarkBelow
  , doubleOpenMarkBelow
  , lightCentralizationStrokeBelow
  , strongCentralizationStrokeBelow
  , parenthesesAbove
  , doubleParenthesesAbove
  , parenthesesBelow
  , balineseSignRerekan
  , balineseAdegAdeg
  , balineseMusicalSymbolCombiningTegeh
  , balineseMusicalSymbolCombiningEndep
  , balineseMusicalSymbolCombiningKempul
  , balineseMusicalSymbolCombiningKempli
  , balineseMusicalSymbolCombiningJegogan
  , balineseMusicalSymbolCombiningKempulWithJegogan
  , balineseMusicalSymbolCombiningKempliWithJegogan
  , balineseMusicalSymbolCombiningBende
  , balineseMusicalSymbolCombiningGong
  , sundaneseSignPamaaeh
  , sundaneseSignVirama
  , batakSignTompi
  , batakPangolat
  , batakPanongonan
  , lepchaSignNukta
  , vedicToneKarshana
  , vedicToneShara
  , vedicTonePrenkha
  , vedicSignYajurvedicMidlineSvarita
  , vedicToneYajurvedicAggravatedIndependentSvarita
  , vedicToneYajurvedicIndependentSvarita
  , vedicToneYajurvedicKathakaIndependentSvarita
  , vedicToneCandraBelow
  , vedicToneYajurvedicKathakaIndependentSvaritaSchroeder
  , vedicToneDoubleSvarita
  , vedicToneTripleSvarita
  , vedicToneKathakaAnudatta
  , vedicToneDotBelow
  , vedicToneTwoDotsBelow
  , vedicToneThreeDotsBelow
  , vedicToneRigvedicKashmiriIndependentSvarita
  , vedicSignVisargaSvarita
  , vedicSignVisargaUdatta
  , vedicSignReversedVisargaUdatta
  , vedicSignVisargaAnudatta
  , vedicSignReversedVisargaAnudatta
  , vedicSignVisargaUdattaWithTail
  , vedicSignVisargaAnudattaWithTail
  , vedicSignTiryak
  , vedicToneCandraAbove
  , vedicToneRingAbove
  , vedicToneDoubleRingAbove
  , dottedGraveAccent
  , dottedAcuteAccent
  , snakeBelow
  , suspensionMark
  , macronAcute
  , graveMacron
  , macronGrave
  , acuteMacron
  , graveAcuteGrave
  , acuteGraveAcute
  , latinSmallLetterRBelow
  , breveMacron
  , macronBreve
  , doubleCircumflexAbove
  , ogonekAbove
  , zigzagBelow
  , isBelow
  , urAbove
  , usAbove
  , latinSmallLetterFlattenedOpenAAbove
  , latinSmallLetterAe
  , latinSmallLetterAo
  , latinSmallLetterAv
  , latinSmallLetterCCedilla
  , latinSmallLetterInsularD
  , latinSmallLetterEth
  , latinSmallLetterG
  , latinLetterSmallCapitalG
  , latinSmallLetterK
  , latinSmallLetterL
  , latinLetterSmallCapitalL
  , latinLetterSmallCapitalM
  , latinSmallLetterN
  , latinLetterSmallCapitalN
  , latinLetterSmallCapitalR
  , latinSmallLetterRRotunda
  , latinSmallLetterS
  , latinSmallLetterLongS
  , latinSmallLetterZ
  , latinSmallLetterAlpha
  , latinSmallLetterB
  , latinSmallLetterBeta
  , latinSmallLetterSchwa
  , latinSmallLetterF
  , latinSmallLetterLWithDoubleMiddleTilde
  , latinSmallLetterOWithLightCentralizationStroke
  , latinSmallLetterP
  , latinSmallLetterEsh
  , latinSmallLetterUWithLightCentralizationStroke
  , latinSmallLetterW
  , latinSmallLetterAWithDiaeresis
  , latinSmallLetterOWithDiaeresis
  , latinSmallLetterUWithDiaeresis
  , upTackAbove
  , kavykaAboveRight
  , kavykaAboveLeft
  , dotAboveLeft
  , wideInvertedBridgeBelow
  , deletionMark
  , almostEqualToBelow
  , leftArrowheadAbove
  , rightArrowheadAndDownArrowheadBelow
  , leftHarpoonAbove
  , rightHarpoonAbove
  , longVerticalLineOverlay
  , shortVerticalLineOverlay
  , leftArrowAbove
  , rightArrowAbove
  , leftRightArrowAbove
  , reverseSolidusOverlay
  , doubleVerticalStrokeOverlay
  , annuitySymbol
  , tripleUnderdot
  , wideBridgeAbove
  , leftwardsArrowOverlay
  , longDoubleSolidusOverlay
  , rightwardsHarpoonWithBarbDownwards
  , leftwardsHarpoonWithBarbDownwards
  , leftArrowBelow
  , rightArrowBelow
  , copticCombiningNiAbove
  , copticCombiningSpiritusAsper
  , copticCombiningSpiritusLenis
  , tifinaghConsonantJoiner
  , cyrillicLetterBe
  , cyrillicLetterVe
  , cyrillicLetterGhe
  , cyrillicLetterDe
  , cyrillicLetterZhe
  , cyrillicLetterZe
  , cyrillicLetterKa
  , cyrillicLetterEl
  , cyrillicLetterEm
  , cyrillicLetterEn
  , cyrillicLetterO
  , cyrillicLetterPe
  , cyrillicLetterEr
  , cyrillicLetterEs
  , cyrillicLetterTe
  , cyrillicLetterHa
  , cyrillicLetterTse
  , cyrillicLetterChe
  , cyrillicLetterSha
  , cyrillicLetterShcha
  , cyrillicLetterFita
  , cyrillicLetterEsTe
  , cyrillicLetterA
  , cyrillicLetterIe
  , cyrillicLetterDjerv
  , cyrillicLetterMonographUk
  , cyrillicLetterYat
  , cyrillicLetterYu
  , cyrillicLetterIotifiedA
  , cyrillicLetterLittleYus
  , cyrillicLetterBigYus
  , cyrillicLetterIotifiedBigYus
  , ideographicLevelToneMark
  , ideographicRisingToneMark
  , ideographicDepartingToneMark
  , ideographicEnteringToneMark
  , hangulSingleDotToneMark
  , hangulDoubleDotToneMark
  , katakanaHiraganaVoicedSoundMark
  , katakanaHiraganaSemiVoicedSoundMark
  , cyrillicVzmet
  , cyrillicLetterUkrainianIe
  , cyrillicLetterI
  , cyrillicLetterYi
  , cyrillicLetterU
  , cyrillicLetterHardSign
  , cyrillicLetterYeru
  , cyrillicLetterSoftSign
  , cyrillicLetterOmega
  , cyrillicKavyka
  , cyrillicPayerok
  , cyrillicLetterEf
  , cyrillicLetterIotifiedE
  , bamumCombiningMarkKoqndon
  , bamumCombiningMarkTukwentis
  , sylotiNagriSignHasanta
  , saurashtraSignVirama
  , devanagariDigitZero
  , devanagariDigitOne
  , devanagariDigitTwo
  , devanagariDigitThree
  , devanagariDigitFour
  , devanagariDigitFive
  , devanagariDigitSix
  , devanagariDigitSeven
  , devanagariDigitEight
  , devanagariDigitNine
  , devanagariLetterA
  , devanagariLetterU
  , devanagariLetterKa
  , devanagariLetterNa
  , devanagariLetterPa
  , devanagariLetterRa
  , devanagariLetterVi
  , devanagariSignAvagraha
  , kayahLiTonePlophu
  , kayahLiToneCalya
  , kayahLiToneCalyaPlophu
  , rejangVirama
  , javaneseSignCecakTelu
  , javanesePangkon
  , taiVietMaiKang
  , taiVietVowelI
  , taiVietVowelUe
  , taiVietVowelU
  , taiVietMaiKhit
  , taiVietVowelIa
  , taiVietVowelAm
  , taiVietToneMaiEk
  , taiVietToneMaiTho
  , meeteiMayekVirama
  , meeteiMayekApunIyek
  , hebrewPointJudeoSpanishVarika
  , ligatureLeftHalf
  , ligatureRightHalf
  , doubleTildeLeftHalf
  , doubleTildeRightHalf
  , macronLeftHalf
  , macronRightHalf
  , conjoiningMacron
  , ligatureLeftHalfBelow
  , ligatureRightHalfBelow
  , tildeLeftHalfBelow
  , tildeRightHalfBelow
  , cyrillicTitloLeftHalf
  , cyrillicTitloRightHalf
  , phaistosDiscSignCombiningObliqueStroke
  , copticEpactThousandsMark
  , oldPermicLetterAn
  , oldPermicLetterDoi
  , oldPermicLetterZata
  , oldPermicLetterNenoe
  , oldPermicLetterSii
  , kharoshthiSignDoubleRingBelow
  , kharoshthiSignVisarga
  , kharoshthiSignBarAbove
  , kharoshthiSignCauda
  , kharoshthiSignDotBelow
  , kharoshthiVirama
  , manichaeanAbbreviationMarkAbove
  , manichaeanAbbreviationMarkBelow
  , hanifiRohingyaSignHarbahay
  , hanifiRohingyaSignTahala
  , hanifiRohingyaSignTana
  , hanifiRohingyaSignTassi
  , sogdianCombiningDotBelow
  , sogdianCombiningTwoDotsBelow
  , sogdianCombiningDotAbove
  , sogdianCombiningTwoDotsAbove
  , sogdianCombiningCurveAbove
  , sogdianCombiningCurveBelow
  , sogdianCombiningHookAbove
  , sogdianCombiningHookBelow
  , sogdianCombiningLongHookBelow
  , sogdianCombiningReshBelow
  , sogdianCombiningStrokeBelow
  , brahmiVirama
  , brahmiNumberJoiner
  , kaithiSignVirama
  , kaithiSignNukta
  , chakmaSignCandrabindu
  , chakmaSignAnusvara
  , chakmaSignVisarga
  , chakmaVirama
  , chakmaMaayyaa
  , mahajaniSignNukta
  , sharadaSignVirama
  , sharadaSignNukta
  , khojkiSignVirama
  , khojkiSignNukta
  , khudawadiSignNukta
  , khudawadiSignVirama
  , binduBelow
  , granthaSignNukta
  , granthaSignVirama
  , granthaDigitZero
  , granthaDigitOne
  , granthaDigitTwo
  , granthaDigitThree
  , granthaDigitFour
  , granthaDigitFive
  , granthaDigitSix
  , granthaLetterA
  , granthaLetterKa
  , granthaLetterNa
  , granthaLetterVi
  , granthaLetterPa
  , newaSignVirama
  , newaSignNukta
  , newaSandhiMark
  , tirhutaSignVirama
  , tirhutaSignNukta
  , siddhamSignVirama
  , siddhamSignNukta
  , modiSignVirama
  , takriSignVirama
  , takriSignNukta
  , ahomSignKiller
  , dograSignVirama
  , dograSignNukta
  , nandinagariSignVirama
  , zanabazarSquareSignVirama
  , zanabazarSquareSubjoiner
  , soyomboSubjoiner
  , bhaiksukiSignVirama
  , masaramGondiSignNukta
  , masaramGondiSignHalanta
  , masaramGondiVirama
  , gunjalaGondiVirama
  , bassaVahCombiningHighTone
  , bassaVahCombiningLowTone
  , bassaVahCombiningMidTone
  , bassaVahCombiningLowMidTone
  , bassaVahCombiningHighLowTone
  , pahawhHmongMarkCimTub
  , pahawhHmongMarkCimSo
  , pahawhHmongMarkCimKes
  , pahawhHmongMarkCimKhav
  , pahawhHmongMarkCimSuam
  , pahawhHmongMarkCimHom
  , pahawhHmongMarkCimTaum
  , duployanDoubleMark
  , musicalSymbolCombiningStem
  , musicalSymbolCombiningSprechgesangStem
  , musicalSymbolCombiningTremolo1
  , musicalSymbolCombiningTremolo2
  , musicalSymbolCombiningTremolo3
  , musicalSymbolCombiningAugmentationDot
  , musicalSymbolCombiningFlag1
  , musicalSymbolCombiningFlag2
  , musicalSymbolCombiningFlag3
  , musicalSymbolCombiningFlag4
  , musicalSymbolCombiningFlag5
  , musicalSymbolCombiningAccent
  , musicalSymbolCombiningStaccato
  , musicalSymbolCombiningTenuto
  , musicalSymbolCombiningStaccatissimo
  , musicalSymbolCombiningMarcato
  , musicalSymbolCombiningMarcatoStaccato
  , musicalSymbolCombiningAccentStaccato
  , musicalSymbolCombiningLoure
  , musicalSymbolCombiningDoit
  , musicalSymbolCombiningRip
  , musicalSymbolCombiningFlip
  , musicalSymbolCombiningSmear
  , musicalSymbolCombiningBend
  , musicalSymbolCombiningDoubleTongue
  , musicalSymbolCombiningTripleTongue
  , musicalSymbolCombiningDownBow
  , musicalSymbolCombiningUpBow
  , musicalSymbolCombiningHarmonic
  , musicalSymbolCombiningSnapPizzicato
  , greekMusicalTriseme
  , greekMusicalTetraseme
  , greekMusicalPentaseme
  , glagoliticLetterAzu
  , glagoliticLetterBuky
  , glagoliticLetterVede
  , glagoliticLetterGlagoli
  , glagoliticLetterDobro
  , glagoliticLetterYestu
  , glagoliticLetterZhivete
  , glagoliticLetterZemlja
  , glagoliticLetterIzhe
  , glagoliticLetterInitialIzhe
  , glagoliticLetterI
  , glagoliticLetterDjervi
  , glagoliticLetterKako
  , glagoliticLetterLjudije
  , glagoliticLetterMyslite
  , glagoliticLetterNashi
  , glagoliticLetterOnu
  , glagoliticLetterPokoji
  , glagoliticLetterRitsi
  , glagoliticLetterSlovo
  , glagoliticLetterTvrido
  , glagoliticLetterUku
  , glagoliticLetterFritu
  , glagoliticLetterHeru
  , glagoliticLetterShta
  , glagoliticLetterTsi
  , glagoliticLetterChrivi
  , glagoliticLetterSha
  , glagoliticLetterYeru
  , glagoliticLetterYeri
  , glagoliticLetterYati
  , glagoliticLetterYu
  , glagoliticLetterSmallYus
  , glagoliticLetterYo
  , glagoliticLetterIotatedSmallYus
  , glagoliticLetterBigYus
  , glagoliticLetterIotatedBigYus
  , glagoliticLetterFita
  , nyiakengPuachueHmongToneB
  , nyiakengPuachueHmongToneM
  , nyiakengPuachueHmongToneJ
  , nyiakengPuachueHmongToneV
  , nyiakengPuachueHmongToneS
  , nyiakengPuachueHmongToneG
  , nyiakengPuachueHmongToneD
  , wanchoToneTup
  , wanchoToneTupni
  , wanchoToneKoi
  , wanchoToneKoini
  , mendeKikakuiCombiningNumberTeens
  , mendeKikakuiCombiningNumberTens
  , mendeKikakuiCombiningNumberHundreds
  , mendeKikakuiCombiningNumberThousands
  , mendeKikakuiCombiningNumberTenThousands
  , mendeKikakuiCombiningNumberHundredThousands
  , mendeKikakuiCombiningNumberMillions
  , adlamAlifLengthener
  , adlamVowelLengthener
  , adlamGeminationMark
  , adlamHamza
  , adlamConsonantModifier
  , adlamGeminateConsonantModifier
  , adlamNukta ]
