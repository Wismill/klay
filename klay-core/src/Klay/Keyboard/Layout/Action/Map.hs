{-# LANGUAGE UndecidableInstances #-}

module Klay.Keyboard.Layout.Action.Map
  ( MultiOsRawActionsMap
  , MultiOsRawActionsKeyMap
  , MultiOsRawActionsKeyMapAlteration
  , MultiOsRawActionsKeyMapAlteration'
  , MultiOsRawActionsKeyMapAlterations
  , MultiOsRawActionsKeyMapDelta

  , MultiOsAutoActionsMap
  , MultiOsAutoActionsKeyMap

  , MultiOsActionsMap
  , MultiOsActionsKeyMap

  , RawActionsMap
  , RawActionsKeyMap
  , RawActionsKeyMapAlteration
  , RawActionsKeyMapDelta

  , AutoActionsMap
  , AutoActionsKeyMap
  , AutoActionsMapUpdate
  , AutoActionsKeyMapUpdate

  , ResolvedActionsMap
  , ResolvedActionsKeyMap
  , ResolvedActionsKeyMapAlteration
  , ResolvedActionsKeyMapDelta

  , ActionsMap
  , ActionsKeyMap
  , ActionsKeyMapAlteration
  , ActionsKeyMapAlterations
  , ActionsMapUpdate
  , ActionsKeyMapUpdate

  , Layers
  , KeyLayers

  , Layer
  , KeyLayer

  , getLayers
  , getLayer
  , getImplicitLayer
  , getBaseLayer
  , actionsAtKey
  , actionAtKeyLevel

  , (+>)
  , (⇒)
  , (⇨)
  ) where

import Data.Map.Strict qualified as Map
import Data.Bifunctor (Bifunctor(..))

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Layout.Action.Action (Action(..))
import Klay.Keyboard.Layout.Action.Actions
  ( Actions(..), ActionsAlterations, ActionsAlteration
  , actionAtLevel, implicitActionAtLevel, foldrActions )
import Klay.Keyboard.Layout.Action.Auto.ModifiersOptions
  (TAutoActions(..), AutoActions, AutoActionsAlteration)
import Klay.Keyboard.Layout.Action.MultiOs
  ( MultiOsRawActions, MultiOsRawActionsAlteration, MultiOsRawActionsAlteration'
  , MultiOsRawActionsAlterations, MultiOsRawActionsDelta
  , MultiOsAutoActions
  , MultiOsResolvedActions
  , MultiOsActions
  , multiOsActions )
import Klay.Keyboard.Layout.Action.Raw
  ( CustomActions(..)
  , RawActions, RawActionsAlteration, RawActionsDelta
  , ResolvedActions, ResolvedActionsAlteration, ResolvedActionsDelta )
import Klay.Keyboard.Layout.Action.Default (checkMultiOsDefaultActions)
import Klay.Keyboard.Layout.Modifier.Types (AutoModifiersOptions(Auto))
import Klay.Keyboard.Layout.Level (Level)
import Data.Alterable (Magma(..), insertOrUpdate0d)

type MultiOsRawActionsMap k     = Map.Map k MultiOsRawActions
type MultiOsRawActionsKeyMap    = MultiOsRawActionsMap Key

type MultiOsRawActionsKeyMapAlteration  = Map.Map Key MultiOsRawActionsAlteration
type MultiOsRawActionsKeyMapAlteration' = Map.Map Key MultiOsRawActionsAlteration'
type MultiOsRawActionsKeyMapAlterations = Map.Map Key MultiOsRawActionsAlterations
type MultiOsRawActionsKeyMapDelta       = Map.Map Key MultiOsRawActionsDelta

type MultiOsAutoActionsMap k     = Map.Map k MultiOsAutoActions
type MultiOsAutoActionsKeyMap    = MultiOsAutoActionsMap Key

type MultiOsActionsMap k     = Map.Map k MultiOsActions
type MultiOsActionsKeyMap    = MultiOsActionsMap Key

type RawActionsMap k            = Map.Map k RawActions
type RawActionsKeyMap           = RawActionsMap Key
type RawActionsKeyMapAlteration = Map.Map Key RawActionsAlteration
type RawActionsKeyMapDelta      = Map.Map Key RawActionsDelta

type AutoActionsMap k           = Map.Map k AutoActions
type AutoActionsKeyMap          = AutoActionsMap Key
type AutoActionsMapUpdate k     = Map.Map k AutoActionsAlteration
type AutoActionsKeyMapUpdate    = AutoActionsMapUpdate Key

type ResolvedActionsMap k       = Map.Map k ResolvedActions
type ResolvedActionsKeyMap      = ResolvedActionsMap Key
type ResolvedActionsKeyMapAlteration = Map.Map Key ResolvedActionsAlteration
type ResolvedActionsKeyMapDelta = Map.Map Key ResolvedActionsDelta


-- | An action map is a mapping between a key and its actions for the different levels.
type ActionsMap k               = Map.Map k Actions
-- | An 'ActionsMap' indexed by 'Key's.
type ActionsKeyMap              = ActionsMap Key

type ActionsKeyMapAlteration    = Map.Map Key ActionsAlteration
type ActionsKeyMapAlterations   = Map.Map Key ActionsAlterations
type ActionsMapUpdate k         = Map.Map k   ActionsAlteration
type ActionsKeyMapUpdate        = ActionsMapUpdate Key

-- | A group of 'Layer's indexed by their level
type Layers k  = Map.Map Level (Layer k)
type KeyLayers = Layers Key

-- | An 'ActionsMap' for one level only.
type Layer k  = Map.Map k Action
type KeyLayer = Layer Key


getLayers :: (Ord k) => ActionsMap k -> Map.Map Level (Layer k)
getLayers = Map.foldrWithKey (\k as acc -> foldrActions (go k) acc as) mempty
  where go k _ l a = Map.insertWith (<>) l (Map.singleton k a)

-- | Get a 'Layer' at a specific level.
getLayer :: ActionsMap k -> Level -> Layer k
getLayer am l = Map.mapMaybe go am
  where
    go UndefinedActions = Nothing
    go NoActions        = Nothing
    go (SingleAction a) = Just a
    go as               = Just $ actionAtLevel as l

-- | Get a 'Layer' at a specific level, with implicit actions.
getImplicitLayer :: ActionsMap k -> Level -> Layer k
getImplicitLayer am l = Map.mapMaybe go am
  where
    go UndefinedActions = Nothing
    go NoActions        = Nothing
    go (SingleAction a) = Just a
    go as               = Just $ implicitActionAtLevel as l

-- | Get the base 'Layer'.
getBaseLayer :: ActionsMap k -> Layer k
getBaseLayer am = getLayer am minBound

actionsAtKey :: (Ord k) => ActionsMap k -> k -> Actions
actionsAtKey am key = Map.findWithDefault UndefinedActions key am

-- | Get the action of a key at a specific 'Level' if defined, else 'UndefinedAction'.
actionAtKeyLevel :: (Ord k) => ActionsMap k -> k -> Level -> Action
actionAtKeyLevel am key l = maybe UndefinedAction (`actionAtLevel` l) (Map.lookup key am)

-- | Define an entry in a 'RawActionsMap'.
(⇒) :: Key -> MultiOsResolvedActions -> (Key, MultiOsRawActions)
k ⇒ as = (k, checkMultiOsDefaultActions k $ bimap (AutoActions Auto) (AutoActions Auto) <$> as)
infixr 1 ⇒

-- | Define a possible entry in a 'RawActionsMap'.
(⇨) :: k -> RawActionsAlteration -> (k, MultiOsRawActionsAlteration)
vk ⇨ as = (vk, insertOrUpdate0d (multiOsActions $ ACustom AutoUndefinedActions) . multiOsActions . Just $ as)
infixr 1 ⇨
