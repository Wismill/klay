module Klay.Keyboard.Layout.Action.Map
  ( MultiOsAutoActionsKeyMap
  , AutoActionsKeyMap
  , AutoActionsKeyMapUpdate
  )where

import Klay.Keyboard.Hardware.Key (Key)
import Data.Map.Strict qualified as Map
import Klay.Keyboard.Layout.Action.MultiOs
import Klay.Keyboard.Layout.Action.Auto.ModifiersOptions

type MultiOsAutoActionsMap k     = Map.Map k MultiOsAutoActions
type MultiOsAutoActionsKeyMap    = MultiOsAutoActionsMap Key

type AutoActionsMap k           = Map.Map k AutoActions
type AutoActionsKeyMap          = AutoActionsMap Key

type AutoActionsMapUpdate k     = Map.Map k AutoActionsAlteration
type AutoActionsKeyMapUpdate    = AutoActionsMapUpdate Key
