{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedLists       #-}

module Klay.Keyboard.Layout.Action.Actions
  ( -- * Actions
    Actions
      ( ..
      , UndefinedActions
      , NoActions )
  , IsActions(..)
  , actionsOptions

    -- * Actions alterations

  , ActionsAlteration
  , ActionsAlterations
    ( ..
    , EmptyActionsAlt
    , ResetActionsAlt
    , SingleActionAlt
    , NoActionsAlt )
  , ModifiersOptionsAlteration
  , DefaultActionAlteration
  , LevelsActionsAlteration
  , fromActionsAlterations
  , toActionsAlterations
  , mkActionsAlterations

    -- * Levels action map
  , Level
  , LevelsActions
  , NELevelsActions

  , normaliseActions
  , mapActions
  , mapActionsLevels
  , mbimapActions
  , foldlActions
  , foldlImplicitActions
  , foldrActions
  , foldlActionsM
  , foldrActionsM
  , traverseActions
  , traverseMaybeActions
  , bitraverseMaybeActions

    -- ** Creation of an action map
  , singleton
  , mkActions
  , (.=)
  , (⏵)

    -- ** Miscellaneous
  , actionsList
  , implicitActionsList
  , actionsByLevel
  , implicitActionsByLevel
  , actionsByMaybeLevel
  , actionAtLevel
  , implicitActionAtLevel
  , definedLevels

    -- * Arbitrary
  , arbitrarySingleAction
  , arbitraryDefinedSingleAction
  , arbitraryNormalSingleAction
  , arbitraryMultiActions
  ) where

import Data.Maybe (fromMaybe, mapMaybe)
import Data.List.NonEmpty qualified as NE
import Data.IntMap.Strict qualified as IMap
import Data.IntMap.NonEmpty qualified as NEIntMap
import Klay.Utils.Orphans.IntMap.NonEmpty ()
import Data.Function (on)
import Data.Functor (($>), (<&>))
import Data.Foldable (foldl')
import Data.Bifunctor (Bifunctor(..))
import Control.Arrow ((&&&))
import Control.DeepSeq ( NFData )
import GHC.Generics ( Generic )
import GHC.Exts (IsList(..))

import Data.Witherable (Witherable(..), WitherableWithIndex(..))
import TextShow (TextShow)
import TextShow.Generic (FromGeneric(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..), Arbitrary1(..), genericShrink)
import Test.QuickCheck.Gen (Gen, frequency)
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Keyboard.Layout.Action.Action
  ( Action(..), IsAction(..), ActionUpgrade, IsActionUpgrade(..)
  , isDefinedAction, arbitraryNormalAction, arbitraryDefinedAction)
import Klay.Keyboard.Layout.Level (Level, IsLevels(..))
import Klay.Keyboard.Layout.Modifier.Types
  ( ModifiersOptions(NoModifiersOptions)
  , ModifiersOptionsAlteration
  , ModifiersField(..) )
import Klay.Keyboard.Layout.Modifier
  ( toCanonicalMask, toCanonicalMask'
  , mapMaybeModifiersOptions, traverseMaybeModifiersOptions)
import Data.Alterable
  ( Differentiable(..), Magma(..), Alterable(..), Alteration(..), Upgrade(..)
  , insertOrUpdate1, insertOrUpdate0d, malter)
import Klay.Utils (jsonOptions)


--- Actions -------------------------------------------------------------------

-- | A mapping of actions by level for a specific key.
data Actions
  = SingleAction !Action
  -- ^ A single action, not affected by the modifiers.
  | Actions -- ^ Multiple actions
    !ModifiersOptions -- ^ Options
    !Action           -- ^ Default action
    !NELevelsActions  -- ^ Action by level map

  deriving (Generic, NFData, Show)
  deriving TextShow via (FromGeneric Actions)

-- | Internal mapping of modifiers to actions.
type LevelsActions = IMap.IntMap Action
-- | Non empty internal mapping of modifiers to actions.
type NELevelsActions = NEIntMap.NEIntMap Action

-- | /Implicitely/ no actions mapped.
pattern UndefinedActions :: Actions
pattern UndefinedActions = SingleAction UndefinedAction

-- | /Explicitely/ no actions mapped.
pattern NoActions :: Actions
pattern NoActions = SingleAction NoAction

instance {-# OVERLAPPING #-} Eq Actions where
  SingleAction a1    == SingleAction a2 = a1 == a2
  SingleAction _     == Actions{}       = False
  Actions{}          == SingleAction _  = False
  Actions mo1 a1 as1 == Actions mo2 a2 as2
    | a1  /= a2  = False
    | mo1 /= mo2 = False
    | otherwise  =
      let as1' = NEIntMap.toMap as1
          as2' = NEIntMap.toMap as2
      in normaliseMap mo1 as1' == normaliseMap mo2 as2'

instance {-# OVERLAPPING #-} Ord Actions where
  SingleAction a1    `compare` SingleAction a2 = a1 `compare` a2
  SingleAction _     `compare` Actions{}       = LT
  Actions{}          `compare` SingleAction _  = GT
  Actions mo1 a1 as1 `compare` Actions mo2 a2 as2 = case compare a1 a2 of
    EQ -> case compare mo1 mo2 of
      EQ -> let as1' = NEIntMap.toMap as1
                as2' = NEIntMap.toMap as2
            in normaliseMap mo1 as1' `compare` normaliseMap mo2 as2'
      c -> c
    c -> c

{-| Notes:

* Right-biased
* 'UndefinedActions' is the neutral element.
* A 'SingleAction' merged with a 'Actions' is interpreted as an 'Actions'.
  with the same action on /all possible/ levels.
-}
instance Magma Actions where
  UndefinedActions   +> as               = as
  as                 +> UndefinedActions = as
  _                  +> as@(SingleAction _) = as
  SingleAction a1    +> Actions mo2 a2 as2 = Actions mo2 (a1 +> a2) as2
  Actions mo1 a1 as1 +> Actions mo2 a2 as2 =
    let mo = mo1 +> mo2
        a = a1 +> a2
        as = as1' +> as2'
        as1' = normaliseMap mo $ normaliseMap mo1 (NEIntMap.toMap as1)
        as2' = normaliseMap mo $ normaliseMap mo2 (NEIntMap.toMap as2)
    in mkActionsFromMap mo a as

-- | No normalisation to preserve associativity
instance Semigroup Actions where
  UndefinedActions   <> as                 = as
  as                 <> UndefinedActions   = as
  SingleAction a1    <> SingleAction a2    = SingleAction (a1 <> a2)
  SingleAction a1    <> Actions mo2 a2 as2 = Actions mo2 (a1 <> a2) as2
  Actions mo1 a1 as1 <> SingleAction a2    = Actions mo1 (a1 <> a2) as1
  Actions mo1 a1 as1 <> Actions mo2 a2 as2 =
    Actions (mo1 <> mo2) (a1 <> a2) (NEIntMap.unionWith (<>) as1 as2)

instance Monoid Actions where
  mempty = UndefinedActions

-- [FIXME] SingleAction
instance Arbitrary Actions where
  arbitrary = frequency
    [ (1, arbitrarySingleAction)
    , (3, arbitraryMultiActions)
    ]
  shrink = genericShrink

arbitrarySingleAction :: Gen Actions
arbitrarySingleAction = SingleAction <$> arbitrary

arbitraryDefinedSingleAction :: Gen Actions
arbitraryDefinedSingleAction = SingleAction <$> arbitraryDefinedAction

arbitraryNormalSingleAction :: Gen Actions
arbitraryNormalSingleAction = SingleAction <$> arbitraryNormalAction

arbitraryMultiActions :: Gen Actions
arbitraryMultiActions = Actions <$> arbitrary <*> arbitrary <*> arbitraryLeveled

instance ToJSON Actions where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON Actions where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance IsList Actions where
  type Item Actions = [(Level, Action)]
  fromList [] = NoActions
  fromList as = mkActions NoModifiersOptions mempty . mconcat $ as
  toList _ = error "not implemented"


-- [TODO] check if all levels with same action
mkActionsFromMap :: ModifiersOptions -> Action -> LevelsActions -> Actions
mkActionsFromMap mo a as =
  case NEIntMap.nonEmptyMap (normaliseMap mo as) of
    Nothing  -> SingleAction a
    Just as' -> Actions mo a as'


{-| Create a 'Actions' or a 'NoActions', depending if some actions different from 'UndefinedAction' are given.

Note: to create a single action, use 'singleton' instead.
-}
mkActions :: ModifiersOptions -> Action -> [(Level, Action)] -> Actions
mkActions mo a as
  | a == UndefinedAction && all (== UndefinedAction) as' = UndefinedActions
  | otherwise = mkActionsFromMap mo a as'
  where as' = IMap.fromList . fmap (first _mField) $ as

-- | Normalise 'Actions'
normaliseActions :: Actions -> Actions
normaliseActions (Actions mo a as) = mkActionsFromMap mo a (NEIntMap.toMap as)
  -- [TODO] in current state we cannot know what levels we are dealing with, so the following is not right
  -- all (== UndefinedAction) as' && a == UndefinedAction = Actions mo UndefinedAction mempty
  -- all (== NoAction)        as' && a == NoAction        = Actions mo NoAction        mempty
  -- otherwise = Actions mo a as'
  -- where as' = normaliseMap mo as
normaliseActions as = as

-- | Convert to canonical levels
normaliseMap :: ModifiersOptions -> IMap.IntMap a -> IMap.IntMap a
normaliseMap mo as = IMap.fromList . mapMaybe go . IMap.toList $ as
  where
    go (l, a) =
      let l' = toCanonicalMask' mo l
      in if l' == l
        -- Canonical level: keep action
        then Just (l, a)
        -- Non-canonical level: check if canonical level is already mapped
        -- If so, discard the non-canonical level
        else case IMap.lookup l' as of
          Nothing -> Just (l', a)
          _       -> Nothing

actionsOptions :: Actions -> ModifiersOptions
actionsOptions (SingleAction _) = mempty
actionsOptions (Actions mo _ _) = mo

{- [NOTE] Actions difference
Computing actions differences requires a preliminary normalisation.
-}

instance Differentiable Actions ActionsAlteration where
  diff = diff' `on` normaliseActions
    where
      diff' UndefinedActions UndefinedActions = Nothing
      diff' UndefinedActions as               = Just (insertOrUpdate1 as)
      diff' _                UndefinedActions = Just Delete
      diff' as1 as2 = diff as1 as2 <&> \d -> InsertOrUpdate (alter d UndefinedActions) d

instance Differentiable Actions ActionsAlterations where
  diff = diff' `on` normaliseActions
    where
      diff' (SingleAction a1) (SingleAction a2) =
        let d = diff a1 a2 in d $> ActionsAlterations Nothing d Nothing
      diff' (SingleAction a1) (Actions mo2 a2 as2) =
        Just $ ActionsAlterations (diff mempty mo2) (diff a1 a2) (diff mempty (NEIntMap.toMap as2) >>= traverse NEIntMap.nonEmptyMap)
      diff' (Actions mo1 a1 as1) (SingleAction a2) =
        Just $ ActionsAlterations (diff mo1 mempty) (diff a1 a2) (diff (NEIntMap.toMap as1) mempty >>= traverse NEIntMap.nonEmptyMap)
      diff' (Actions mo1 a1 as1) (Actions mo2 a2 as2) =
        case ActionsAlterations (diff mo1 mo2) (diff a1 a2) (diff as1 as2) of
          EmptyActionsAlt -> Nothing
          d               -> Just d

{- [NOTE] Actions update

[TODO] check if this is relevant
Updating actions must follow the following steps:
1. Modify modifiers options
2. Modify default action
3. Modify levels, using the updated modifiers options
4. Normalisation
-}

instance Alterable Actions ModifiersOptions where
  alter mo (Actions _ a as) = normaliseActions (Actions mo a as)
  alter _  a                = a

instance Alterable Actions ActionsAlteration where
  alter (InsertOrIgnore as)   UndefinedActions = as
  alter (InsertOrIgnore _)    as               = as
  alter (InsertOrReplace as)  _                = as -- [TODO] check
  alter (InsertOrUpdate as _) UndefinedActions = as
  alter (InsertOrUpdate _ d)  as               = alter d as
  alter (Update d)            as               = alter d as
  alter Delete                _                = UndefinedActions

instance Alterable Actions ActionsAlterations where
  alter (ActionsAlterations dmo dma das) (SingleAction a) =
    let mo' = malter dmo mempty
        a'  = malter dma a
        as' = malter (fmap (normaliseMap mo' . NEIntMap.toMap) <$> das) mempty
    in mkActionsFromMap mo' a' as'
  alter (ActionsAlterations dmo dma das) (Actions mo a as) =
    let mo' = malter dmo mo
        a'  = malter dma a
        as' = malter
          (fmap (normaliseMap mo' . NEIntMap.toMap) <$> das)
          (normaliseMap mo' . normaliseMap mo . NEIntMap.toMap $ as)
    in mkActionsFromMap mo' a' as'

instance Alterable Actions (Alteration Actions Actions) where
  alter (InsertOrIgnore as)    UndefinedActions = as
  alter (InsertOrIgnore _)     as               = as
  alter (InsertOrReplace as)   _                = as -- [TODO] check
  alter (InsertOrUpdate as _)  UndefinedActions = as
  alter (InsertOrUpdate _ as') as               = alter as' as
  alter (Update as')           as               = alter as' as
  alter Delete                 _                = UndefinedActions


--- Actions Alterations -------------------------------------------------------

data ActionsAlterations = ActionsAlterations
  { _aOptions :: !(Maybe ModifiersOptionsAlteration)
  -- ^ Modifiers options alterations
  , _aDefault :: !(Maybe DefaultActionAlteration)
  -- ^ Default 'Action' alteration
  , _aLevels  :: !(Maybe LevelsActionsAlteration)
  -- ^ 'NELevelsActions' alterations
  } deriving (Generic, NFData, Eq, Show)
  -- deriving TextShow via (FromGeneric ActionsAlterations)

type ActionsAlteration = Alteration Actions ActionsAlterations
type DefaultActionAlteration = ActionUpgrade
type LevelsActionsAlteration = Upgrade (NEIntMap.NEIntMap ActionUpgrade)

pattern EmptyActionsAlt :: ActionsAlterations
pattern EmptyActionsAlt = ActionsAlterations Nothing Nothing Nothing

pattern NoActionsAlt :: ActionsAlterations
pattern NoActionsAlt = SingleActionAlt NoAction

pattern SingleActionAlt :: Action -> ActionsAlterations
pattern SingleActionAlt a = ActionsAlterations (Just Delete) (Just (Replace a)) (Just Reset)

pattern ResetActionsAlt :: ActionsAlterations
pattern ResetActionsAlt = ActionsAlterations (Just Delete) (Just Reset) (Just Reset)

instance Magma ActionsAlterations where
  ActionsAlterations dmo1 dma1 das1 +> ActionsAlterations dmo2 dma2 das2 =
    ActionsAlterations (dmo1 +> dmo2) (dma1 +> dma2) (das1 +> das2)

instance IsList ActionsAlteration where
  type Item ActionsAlteration = [(Level, ActionUpgrade)]
  fromList [] = Delete
  fromList ds = insertOrUpdate0d UndefinedActions . fromList $ ds
  toList _ = error "not implemented"

instance IsList ActionsAlterations where
  type Item ActionsAlterations = [(Level, ActionUpgrade)]
  fromList [] = ResetActionsAlt
  fromList ds = mkActionsAlterations Nothing Nothing (mconcat ds)
  toList _ = error "not implemented"

instance Arbitrary ActionsAlterations where
  arbitrary = ActionsAlterations
    <$> arbitrary
    <*> arbitrary
    <*> liftArbitrary (liftArbitrary arbitraryLeveled)
  shrink = genericShrink

fromActionsAlterations :: ActionsAlterations -> Actions
fromActionsAlterations a = alter a mempty

toActionsAlterations :: Actions -> ActionsAlterations
toActionsAlterations = fromMaybe EmptyActionsAlt . diff mempty

mkActionsAlterations
  :: Maybe ModifiersOptionsAlteration
  -> Maybe DefaultActionAlteration
  -> [(Level, ActionUpgrade)]
  -> ActionsAlterations
mkActionsAlterations mo ma as = case NEIntMap.nonEmptyMap as' of
  Nothing   -> ActionsAlterations mo ma Nothing
  Just as'' -> ActionsAlterations mo ma (Just $ Merge as'')
  where as' = IMap.fromList . fmap (first _mField) $ as

instance Differentiable ActionsAlterations ActionsAlterations where
  diff (ActionsAlterations dmo1 dma1 das1) (ActionsAlterations dmo2 dma2 das2) =
    -- let das = diff das1 das2 >>= traverse NEIntMap.nonEmptyMap :: Maybe LevelsActionsAlteration
    case ActionsAlterations (diff dmo1 dmo2) (diff dma1 dma2) (diff das1 das2) of
      EmptyActionsAlt -> Nothing
      d               -> Just d

instance Alterable ActionsAlterations ModifiersOptions where
  alter d as = as +> ActionsAlterations (Just (insertOrUpdate1 d)) Nothing Nothing

-- | A class to simplify actions mapping in layouts.
class IsActions a where
  toActions :: a -> Actions

instance IsActions Actions where
  toActions = id

instance IsActions Action where
  toActions = SingleAction

instance IsActions LevelsActions where
  toActions = mkActionsFromMap NoModifiersOptions mempty


--- Mapping creation ----------------------------------------------------------

-- | Map a single action at all the levels.
singleton :: (IsAction a) => a -> Actions
singleton = SingleAction . toAction

(.=) :: (IsLevels ls, IsAction a) => ls -> a -> [(Level, Action)]
ls .= a = (,toAction a) <$> toLevels ls

(⏵) :: (IsLevels ls, IsActionUpgrade a) => ls -> a -> [(Level, ActionUpgrade)]
ls ⏵ a = (,toActionUpgrade a) <$> toLevels ls


--- Mapping processing --------------------------------------------------------

mapActions :: (Level -> Action -> Action) -> Actions -> Actions
mapActions _ UndefinedActions = UndefinedActions
mapActions _ NoActions        = NoActions
mapActions f (SingleAction a) = SingleAction (f minBound a) -- [FIXME] minBound?
mapActions f (Actions mo a0 as) = -- [FIXME] minBound?
  normaliseActions $ Actions mo (f minBound a0) (NEIntMap.mapWithKey (f . ModifiersField) as)

mapActionsLevels :: (Level -> Maybe Level) -> Actions -> Actions
mapActionsLevels f (Actions mo a0 as) = mkActionsFromMap mo a0 as'
  where
    as' = IMap.foldlWithKey' go mempty (NEIntMap.toMap as)
    go acc l a = case f (ModifiersField l) of
      Nothing -> acc
      Just l' -> IMap.insert (_mField l') a acc
mapActionsLevels _ as = as

mbimapActions :: (Level -> Maybe Level) -> (Level -> Action -> Maybe Action) -> Actions -> Actions
mbimapActions _ g (SingleAction a) = maybe UndefinedActions SingleAction (g minBound a) -- [FIXME] minBound?
mbimapActions f g (Actions mo a0 as) = mkActionsFromMap mo' a' as'
  where
    mo' = mapMaybeModifiersOptions f mo
    a' = fromMaybe mempty $ g minBound a0 -- [FIXME] minBound?
    as' = IMap.fromList . mapMaybe go . NE.toList . NEIntMap.toList $ as
    go (b, a) = let l = ModifiersField b
                in (,) <$> fmap _mField (f l) <*> g l a

foldlActions :: (a -> ModifiersOptions -> Level -> Action -> a) -> a -> Actions -> a
foldlActions f acc0 (SingleAction a) = f acc0 NoModifiersOptions minBound a -- [FIXME] minBound?
foldlActions f acc0 (Actions mo a0 as) =
  let acc = f acc0 NoModifiersOptions minBound a0 -- [FIXME] minBound?
  in NEIntMap.foldlWithKey' (\acc' l a -> f acc' mo (ModifiersField l) a) acc as

foldlImplicitActions :: [Level] -> (a -> ModifiersOptions -> Level -> Action -> a) -> a -> Actions -> a
foldlImplicitActions ls f acc0 as0 = foldl' go acc0 as
  where
    (mo, as) = implicitActionsByLevel ls as0
    go acc (l, a) = f acc mo l a

foldrActions :: (ModifiersOptions -> Level -> Action -> a -> a) -> a -> Actions -> a
foldrActions f acc (SingleAction a) = f NoModifiersOptions minBound a acc -- [FIXME] minBound?
foldrActions f acc (Actions mo a0 as) =
  let acc' = f NoModifiersOptions minBound a0 acc -- [FIXME] minBound?
  in NEIntMap.foldrWithKey' (f mo . ModifiersField) acc' as

foldlActionsM :: (Monad m) => (a -> ModifiersOptions -> Level -> Action -> m a) -> a -> Actions -> m a
foldlActionsM f acc = foldlActions f' (pure acc)
  where f' acc' ms l a = acc' >>= \acc'' -> f acc'' ms l a

foldrActionsM :: (Monad m) => (ModifiersOptions -> Level -> Action -> a -> m a) -> a -> Actions -> m a
foldrActionsM f acc = foldrActions f' (pure acc)
  where f' ms l a acc' = acc' >>= \acc'' -> f ms l a acc''

traverseActions :: (Applicative f) => (Level -> Action -> f Action) -> Actions -> f Actions
traverseActions f (SingleAction a) = SingleAction <$> f minBound a -- [FIXME] minBound?
traverseActions f (Actions mo a0 as) = -- [FIXME] minBound?
  fmap normaliseActions $ Actions mo <$> f minBound a0 <*> NEIntMap.traverseWithKey (f . ModifiersField) as

traverseMaybeActions :: (Applicative f) => (Level -> Action -> f (Maybe Action)) -> Actions -> f Actions
traverseMaybeActions f (SingleAction a) = maybe UndefinedActions SingleAction <$> f minBound a -- [FIXME] minBound?
traverseMaybeActions f (Actions mo a0 as0) = mkActionsFromMap mo
  <$> fmap (fromMaybe mempty) (f minBound a0) -- [FIXME] minBound?
  <*> iwither (f . ModifiersField) (NEIntMap.toMap as0)

bitraverseMaybeActions :: (Monad m) => (Level -> m (Maybe Level)) -> (Level -> Action -> m (Maybe Action)) -> Actions -> m Actions
bitraverseMaybeActions _ g (SingleAction a) =
  maybe UndefinedActions SingleAction <$> g minBound a -- [FIXME] minBound?
bitraverseMaybeActions f g (Actions mo0 a0 as0) = do
  mo <- traverseMaybeModifiersOptions f mo0
  a <- fromMaybe UndefinedAction <$> g minBound a0-- [FIXME] minBound?
  as <- wither go . NE.toList . NEIntMap.toList $ as0
  pure case NE.nonEmpty as of
    Nothing  -> SingleAction a
    Just as' -> Actions mo a (NEIntMap.fromList as')
  where
    go (l, a) = f (ModifiersField l) >>= \case
      Nothing -> pure Nothing
      Just l' -> fmap (_mField l',) <$> g l' a

actionAtLevel :: Actions -> Level -> Action
actionAtLevel (SingleAction a) _ = a
actionAtLevel (Actions _ a as) l = _actionAtLevel a as l

_actionAtLevel :: Action -> NEIntMap.NEIntMap Action -> Level -> Action
_actionAtLevel a as l = fromMaybe a $ NEIntMap.lookup (_mField l) as

-- | Get the implicit action at the given level, that is,
--   the action corresponding to the canonical level if the action is defined,
--   else the action at the base level, else 'UndefinedAction'.
implicitActionAtLevel :: Actions -> Level -> Action
implicitActionAtLevel (SingleAction a)  _ = a
implicitActionAtLevel (Actions mo a as) l
  = _implicitActionAtLevel mo a as l

_implicitActionAtLevel :: ModifiersOptions -> Action -> NEIntMap.NEIntMap Action -> Level -> Action
_implicitActionAtLevel mo a as l =
  fromMaybe a' $ NEIntMap.lookup (_mField $ toCanonicalMask mo l) as
  where a' | a == UndefinedAction = NEIntMap.findWithDefault UndefinedAction (_mField minBound) as
           | otherwise            = a

actionsList :: [Level] -> Actions -> [Action]
actionsList levels = \case
  SingleAction a -> levels $> a
  Actions _ a as -> _actionAtLevel a as <$> levels

implicitActionsList :: [Level] -> Actions -> [Action]
implicitActionsList levels = \case
  SingleAction a  -> levels $> a
  Actions mo a as -> _implicitActionAtLevel mo a as <$> levels

actionsByLevel :: [Level] -> Actions -> (ModifiersOptions, [(Level, Action)])
actionsByLevel levels = \case
  SingleAction a  -> (mempty, (,a) <$> levels)
  Actions mo a as -> (mo,)
                    . fmap (\l -> (l, _actionAtLevel a as l))
                    $ levels

implicitActionsByLevel :: [Level] -> Actions -> (ModifiersOptions, [(Level, Action)])
implicitActionsByLevel levels = \case
  SingleAction a  -> (mempty, (,a) <$> levels)
  Actions mo a as -> (mo,)
                    . fmap (\l -> (l, _implicitActionAtLevel mo a as l))
                    $ levels

definedLevels :: Actions -> [Level]
definedLevels = foldrActions go mempty
  where go _ l a acc = if isDefinedAction a then l:acc else acc

actionsByMaybeLevel :: [Maybe Level] -> Actions -> (ModifiersOptions, [(Maybe Level, Action)])
actionsByMaybeLevel mlevels = \case
  SingleAction a   -> (mempty, (id &&& maybe UndefinedAction (const a)) <$> mlevels)
  Actions mo a as  ->
    let getActions = maybe UndefinedAction (_actionAtLevel a as)
    in (mo, (id &&& getActions) <$> mlevels)


--- Utils ---------------------------------------------------------------------

-- | Generator of 'NEIntMap.NEIntMap' which indexes are valid modifiers bits
arbitraryLeveled :: (Arbitrary a) => Gen (NEIntMap.NEIntMap a)
arbitraryLeveled = NEIntMap.fromList . fmap (first _mField) <$>
  ((NE.:|) <$> arbitrary <*> arbitrary)

