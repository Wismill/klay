{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE OverloadedLists       #-}

module Klay.Keyboard.Layout.Action.Action
  ( -- * Action
    Action(..)
  , IsAction(..)
  , ActionDelta
  , ActionUpgrade
  , IsActionUpgrade(..)
  , ActionAlteration
  , IsActionAlteration(..)
  , MultiOsAction
  , LongPressActions(..)
  , toMonogram
  , prettyActionB
  , isDefinedAction
  , isActiveModifier
  , serializeAction
  , serializeAction'
  , actionP
  , actionP'

    -- * Generators
  , arbitraryAChar
  , arbitraryAText
  , arbitraryADeadKey
  , arbitraryAModifier
  , arbitraryASpecial
  , arbitraryNormalAction
  , arbitraryDefinedAction
  ) where

import Data.Void (Void)
import Data.List.NonEmpty qualified as NE
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Char (isSpace)
import Data.String(IsString(..))
import Data.Functor ((<&>), ($>))
import Data.Bifunctor (Bifunctor(..))
import Data.Hashable (Hashable)
import Control.DeepSeq (NFData)
import GHC.Generics (Generic)

import TextShow (TextShow(..), FromStringShow(..), showb)
import Test.QuickCheck.Arbitrary (Arbitrary(..), arbitraryUnicodeChar, genericShrink)
import Test.QuickCheck.Gen (Gen, frequency)
import Data.Aeson qualified as Aeson
import Data.Aeson.Types (ToJSON(..), ToJSONKey(..), FromJSON(..), FromJSONKey(..))
import Data.Aeson.Types qualified as Aeson
import Data.Aeson.Encoding qualified as Aeson
import Text.Megaparsec
import Text.Megaparsec.Char

import Klay.Import.Linux.XCompose (rawKeysymP, unicodeKeysymP)
import Klay.Import.Linux.Lookup.Keysyms (keysymToAction, mActionToKeysym)
import Klay.Keyboard.Layout.Action.DeadKey (DeadKey(..), prettyDeadKey)
import Klay.Keyboard.Layout.Action.Json
import Klay.Keyboard.Layout.Action.Special (SpecialAction, specialActionToMonogram)
import Klay.Keyboard.Layout.Level (Level)
import Klay.Keyboard.Layout.Modifier.Types (Modifier(..), modifierP, serializeModifier)
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.OS (MultiOs)
import Data.Alterable
  ( Differentiable(..)
  , Magma(..), Alterable(..), Alteration(..), Delta(..), Upgrade(..) )
import Klay.Utils.Unicode (showUnicodEB, showCombiningChar, showInvisibleChar, padL)
import Klay.Utils.UserInput (RawText(..), RawLText, escapeControlNewlines)

type MultiOsAction = MultiOs Action Action Action

-- | Action to perform when a key is pressed
data LongPressActions
  = BaseAction !Action
  -- ^ A base action
  | LongPress !Action !(NE.NonEmpty Action)
  -- ^ * A base action to perform with a normal key press
  --   * A list of base actions to choose after a long key press.
  --     Useful for virtual keyboards.

-- | Base action to perform when a key is pressed.
data Action
  = AChar !Char
  -- ^ A single character
  | AText !RawLText -- [TODO] should be non empty
  -- ^ A sequence of characters
  | ADeadKey !DeadKey
  -- ^ A dead key
  | AModifier !Modifier
  -- ^ A /modifier/, its /variant/ (left, right, neutral) and its /effect/ (set, latch, lock)
  | ASpecial !SpecialAction
  -- ^ A special action of the operating system
  | NoAction
  -- ^ /Explicitly/ no action.
  | UndefinedAction
  -- ^ /Implicitly/ no action defined. The exact behaviour will depend of the target OS.
  --   It may inherit from some default configuration.
  deriving (Generic, NFData, Hashable, Eq, Ord)
  deriving TextShow via (FromStringShow Action)

-- | Right biased
instance Magma Action where
  UndefinedAction +> a               = a
  a               +> UndefinedAction = a
  _               +> a               = a

-- | Right biased
instance Semigroup Action where
  UndefinedAction <> a               = a
  a               <> UndefinedAction = a
  _               <> a               = a

-- | 'UndefinedAction' is the neutral element.
instance Monoid Action where
  mempty = UndefinedAction

instance Differentiable Action Action

instance Differentiable Action ActionDelta where
  diff UndefinedAction UndefinedAction = Nothing
  diff UndefinedAction a               = Just $ Inserted a
  diff a               UndefinedAction = Just $ Deleted a
  diff a1              a2              = Updated <$> diff a1 a2

instance Differentiable Action ActionUpgrade where
  diff UndefinedAction UndefinedAction = Nothing
  diff UndefinedAction a               = Just $ Replace a
  diff _               UndefinedAction = Just Reset
  diff a1              a2              = Merge <$> diff a1 a2

instance Differentiable Action ActionAlteration where
  diff UndefinedAction UndefinedAction = Nothing
  diff UndefinedAction a               = Just $ InsertOrReplace a
  diff _               UndefinedAction = Just Delete
  diff a1              a2              = Update <$> diff a1 a2

instance Alterable Action ActionDelta where
  alter (Inserted a) _  = a
  alter (Updated a1) a2 = a1 +> a2
  alter (Deleted _)  _  = UndefinedAction

instance Alterable Action ActionUpgrade where
  alter (Merge a)   _ = a
  alter Reset       _ = UndefinedAction
  alter (Replace a) _ = a

instance Alterable Action ActionAlteration where
  alter (InsertOrIgnore a)   UndefinedAction = a
  alter (InsertOrIgnore _)   a               = a
  alter (InsertOrUpdate a _) UndefinedAction = a
  alter (InsertOrUpdate _ a) _               = a
  alter (InsertOrReplace a)  _               = a
  alter (Update a1)          a2              = a1 +> a2
  alter Delete               _               = UndefinedAction

instance Show Action where
  show (AChar c) = showCombiningChar c
  show (AText t) = show t
  show (ADeadKey dk) = mconcat ["‹DK: ", prettyDeadKey dk, "›"]
  show (AModifier (Modifier m v e)) = mconcat ["‹", show v, " ", show m, " ", show e, "›"]
  show (ASpecial a) = mconcat ["‹", show a, "›"]
  show NoAction = "‹NoAction›"
  show UndefinedAction = "‹UndefinedAction›"

instance IsString Action where
  fromString []  = NoAction
  fromString [c] = AChar c
  fromString s   = AText . fromString $ s

instance ToJSON Action where
  toJSON     = Aeson.String . serializeAction
  toEncoding = Aeson.text   . serializeAction

instance FromJSON Action where
  parseJSON = Aeson.withText "action"
            $ either fail pure . parseAction

instance ToJSONKey Action where
  toJSONKey = Aeson.toJSONKeyText serializeAction

instance FromJSONKey Action where
  fromJSONKey = Aeson.FromJSONKeyTextParser
              $ either fail pure . parseAction

serializeAction :: Action -> T.Text
serializeAction (AChar c)       = charToKeysym c
serializeAction (AText t)       = mconcat ["\"", escapeText . TL.toStrict . getRawText $ t, "\""]
serializeAction (ADeadKey dk)   = serializeDeadKey dk
serializeAction (AModifier m)   = "mod:" <> serializeModifier m
serializeAction a@(ASpecial s)  = case mActionToKeysym a of
  Nothing -> "special:" <> showt s
  Just t  -> t
serializeAction NoAction        = "none"
serializeAction UndefinedAction = "undefined"

parseAction :: T.Text -> Either String Action
parseAction
  = first errorBundlePretty
  . parse (actionP <* eof) mempty

type Parser = Parsec Void T.Text

actionP :: Parser Action
actionP = choice (
  [ string "none" $> NoAction                    <?> "No action"
  , string "undefined" $> UndefinedAction        <?> "Undefined action"
  , AText . RawText . TL.fromStrict <$> textP    <?> "Text action"
  , ADeadKey <$> prefixedDeadKeyP                <?> "Dead key action"
  , string "mod:" *> (AModifier <$> modifierP)   <?> "Modifier action"
  , string "special:" *> (ASpecial <$> pSpecial) <?> "Special action"
  , try actionKeysymP                            <?> "Keysym action"
  , anySingle <&> AChar                          <?> "Single char action"
  ] :: [Parser Action])
  where
    pSpecial
      = try $ takeWhile1P Nothing (not . isSpace)
      >>= \t -> case Aeson.fromJSON (Aeson.String t) of
        Aeson.Error e   -> fail e
        Aeson.Success a -> pure a

actionKeysymP :: Parser Action
actionKeysymP = namedKeysymP <|> fmap AChar unicodeKeysymP

namedKeysymP :: Parser Action
namedKeysymP = try $ rawKeysymP >>= maybe (fail "Invalid keysym") pure
                   . keysymToAction

serializeAction' :: Maybe Action -> T.Text
serializeAction' Nothing  = "__"
serializeAction' (Just a) = case a of
  NoAction        -> "XX"
  UndefinedAction -> "��"
  _               -> serializeAction a

actionP' :: Parsec Void T.Text (Maybe Action)
actionP' = choice (
  [ string "��" $> Just UndefinedAction
  , string "XX" $> Just NoAction
  , string "__" $> Nothing
  , Just <$> actionP
  ] :: [Parsec Void T.Text (Maybe Action)])

instance Arbitrary Action where
  arbitrary = frequency
    [ (2, arbitraryAChar)
    , (1, arbitraryAText)
    , (2, arbitraryADeadKey)
    , (2, arbitraryAModifier)
    , (1, arbitraryASpecial)
    , (1, pure NoAction)
    , (1, pure UndefinedAction)
    ]
  shrink = genericShrink

arbitraryAChar :: Gen Action
arbitraryAChar = AChar <$> arbitraryUnicodeChar

arbitraryAText :: Gen Action
arbitraryAText = AText <$> arbitrary

arbitraryADeadKey :: Gen Action
arbitraryADeadKey = ADeadKey <$> arbitrary

arbitraryAModifier :: Gen Action
arbitraryAModifier = AModifier <$> arbitrary

arbitraryASpecial :: Gen Action
arbitraryASpecial = ASpecial <$> arbitrary

-- | Non-empty action
arbitraryNormalAction :: Gen Action
arbitraryNormalAction = frequency
    [ (5, arbitraryAChar)
    , (1, arbitraryAText)
    , (5, arbitraryADeadKey)
    , (2, arbitraryAModifier)
    , (2, arbitraryASpecial)
    ]

-- | Defined action
arbitraryDefinedAction :: Gen Action
arbitraryDefinedAction = frequency
    [ (5, arbitraryAChar)
    , (1, arbitraryAText)
    , (5, arbitraryADeadKey)
    , (2, arbitraryAModifier)
    , (2, arbitraryASpecial)
    , (2, pure NoAction)
    ]

type ActionDelta = Delta Action Action

type ActionAlteration = Alteration Action Action

class IsActionAlteration a where
  to_action_alteration :: a -> ActionAlteration

instance IsActionAlteration ActionAlteration where
  to_action_alteration = id

instance IsActionAlteration Action where
  to_action_alteration = InsertOrReplace

instance IsActionAlteration Char where
  to_action_alteration = InsertOrReplace . toAction

instance IsActionAlteration TL.Text where
  to_action_alteration = InsertOrReplace . toAction

instance IsActionAlteration DeadKey where
  to_action_alteration = InsertOrReplace . toAction

instance IsActionAlteration SpecialAction where
  to_action_alteration = InsertOrReplace . toAction

instance IsActionAlteration Modifier where
  to_action_alteration = InsertOrReplace . toAction

instance IsActionAlteration () where
  to_action_alteration = InsertOrReplace . toAction

instance (IsAction a) => IsActionAlteration (Maybe a) where
  to_action_alteration = maybe Delete (InsertOrReplace . toAction)


type ActionUpgrade = Upgrade Action

class IsActionUpgrade a where
  toActionUpgrade :: a -> ActionUpgrade

instance IsActionUpgrade ActionUpgrade where
  toActionUpgrade = id

instance IsActionUpgrade Action where
  toActionUpgrade UndefinedAction = Reset
  toActionUpgrade a               = Replace a

instance IsActionUpgrade Char where
  toActionUpgrade = Replace . toAction

instance IsActionUpgrade TL.Text where
  toActionUpgrade = Replace . toAction

instance IsActionUpgrade DeadKey where
  toActionUpgrade = Replace . toAction

instance IsActionUpgrade SpecialAction where
  toActionUpgrade = Replace . toAction

instance IsActionUpgrade Modifier where
  toActionUpgrade = Replace . toAction

instance IsActionUpgrade () where
  toActionUpgrade = Replace . toAction

instance (IsAction a) => IsActionUpgrade (Maybe a) where
  toActionUpgrade = maybe Reset (Replace . toAction)

-- | A class to simplify actions mapping in layout.
class IsAction a where
  toAction :: a -> Action

instance IsAction Action where
  toAction = id

instance IsAction Char where
  toAction = AChar

instance IsAction TL.Text where
  toAction = AText . RawText

instance IsAction DeadKey where
  toAction = ADeadKey

instance IsAction SpecialAction where
  toAction = ASpecial

instance IsAction Modifier where
  toAction = AModifier

instance IsAction () where
  toAction () = NoAction

instance (IsAction a) => IsAction (Maybe a) where
  toAction Nothing  = UndefinedAction
  toAction (Just a) = toAction a

prettyActionB :: Action -> TB.Builder
prettyActionB (AChar c) = mconcat ["U+", padL 4 '0' $ showUnicodEB c, " ", showInvisibleChar c]
prettyActionB (AText t) = mconcat ["\"", TB.fromLazyText . escapeControlNewlines $ t, "\""]
prettyActionB (ADeadKey dk) = mconcat ["‹", TB.fromString $ prettyDeadKey dk, "›"]
prettyActionB (AModifier m) = mconcat ["‹", showb m, "›"] -- [FIXME]
prettyActionB (ASpecial a) = mconcat ["‹", showb a, "›"]
prettyActionB UndefinedAction = "‹UndefinedAction›"
prettyActionB NoAction = "‹NoAction›"

isDefinedAction :: Action -> Bool
isDefinedAction UndefinedAction = False
isDefinedAction _               = True

-- | Test if an action corresponds to an active modifier of a given 'Level'.
isActiveModifier
  :: Level  -- ^ Level to test
  -> Action -- ^ An action that may correspondond to a modifier
  -> Bool
isActiveModifier l (AModifier m) = M.isActiveModifier l . _mBit $ m
isActiveModifier _ _             = False

-- | Convert an action that produce a normal character into this character
toMonogram :: Action -> Maybe Char
toMonogram (AChar c)    = Just c
toMonogram (ASpecial a) = specialActionToMonogram a
toMonogram _            = Nothing
