module Klay.Keyboard.Layout.Action.MultiOs
  ( -- * Multi OS Actions
    IsMultiOsActions(..)
  , TMultiOsActions(.., AOsIndependent, ALinux, AWindows)
  , MultiOsRawActions
  , MultiOsRawActionsAlteration
  , MultiOsRawActionsAlteration'
  , MultiOsRawActionsAlterations
  , MultiOsRawActionsDelta
  , MultiOsRawActionsDeltas
  , MultiOsAutoActions
  , MultiOsResolvedActions
  , MultiOsActions
  , MultiOsActionsAlteration
  , MultiOsActionsAlteration'
  , MultiOsActionsAlterations
  -- , IsMultiOsActions(..)
  , forceMultiosCustomActions
  , multiOsActions
  , multiOsActionsWithDefault
  , toMultiOsRawActionsMap
  , toMultiOsRawActionsAlteration
  , toMultiOsRawActionsAlterationMap
  , toOsRawActionsAlteration
  , toOsRawActionsAlterationMap
  , mapMultiOsActions
  , mapMultiOsActionsWithOs
  , insertOrUpdate1d

  , multiOsRawActionsToResolvedActions
  , multiOsRawActionsToRawActions

  , multiOsRawSingleton
  ) where

import Data.Map.Strict qualified as Map
import Data.Bifunctor (Bifunctor(..))
import Control.DeepSeq (NFData(..))
import GHC.Generics (Generic)
import GHC.Exts (IsList(..))

import Test.QuickCheck.Arbitrary (Arbitrary(..))
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Keyboard.Layout.Action.Action
  (Action, ActionUpgrade)
import Klay.Keyboard.Layout.Action.Actions
  ( Actions, ActionsAlteration, Level )
import Klay.Keyboard.Layout.Action.Auto.ModifiersOptions
  (TAutoActions(..), AutoActions, resolveModifiersOptionsLax)
import Klay.Keyboard.Layout.Action.Raw
  ( RawActions, RawActionsAlteration, RawActionsAlterations, RawActionsDelta
  , ResolvedActions, CustomActions(..)
  , rawUndefinedActions, custom, forceCustomActions, rawSingleton)
import Klay.OS
  ( OsSpecific(..), MultiOs(..), MultiOsId
  , fromListWithDefault, getOs, mapWithOs, onOss, toMultiOs, toMultiOsWithDefault)
import Data.Alterable
  ( Differentiable(..), Magma(..), Alterable(..), Alteration(..), Delta, insertOrUpdate0d )
import Klay.Utils (toJsonOptionsNewtype)

type MultiOsRawActions            = TMultiOsActions RawActions
type MultiOsAutoActions           = TMultiOsActions AutoActions
type MultiOsResolvedActions       = TMultiOsActions ResolvedActions
type MultiOsActions               = TMultiOsActions Actions

type MultiOsRawActionsAlterations = TMultiOsActions (Maybe RawActionsAlteration)
type MultiOsRawActionsDeltas      = TMultiOsActions (Maybe RawActionsDelta)
type MultiOsActionsAlterations    = TMultiOsActions (Maybe ActionsAlteration)
type MultiOsRawActionsAlteration  = Alteration MultiOsRawActions MultiOsRawActionsAlterations
type MultiOsRawActionsAlteration' = Alteration MultiOsRawActions RawActionsAlteration
type MultiOsActionsAlteration     = Alteration MultiOsActions    MultiOsActionsAlterations
type MultiOsActionsAlteration'    = Alteration MultiOsActions    ActionsAlteration
type MultiOsRawActionsDelta       = Delta      MultiOsRawActions MultiOsRawActionsDeltas


--- MultiOs Actions -----------------------------------------------------------

newtype TMultiOsActions a = MultiOsActions { unMultiOsActions :: MultiOsId a }
  deriving stock (Generic, Show)
  deriving newtype (Magma, Semigroup, Monoid, Eq, Ord, Arbitrary)
  deriving anyclass (NFData)

-- deriving newtype instance {-# OVERLAPPABLE #-} (Magma a) => Magma (TMultiOsActions a)

pattern AOsIndependent :: a -> TMultiOsActions a
pattern AOsIndependent a <- MultiOsActions MultiOs{_osIndependent=a}

pattern ALinux :: a -> TMultiOsActions a
pattern ALinux a <- MultiOsActions MultiOs{_linux=a}

pattern AWindows :: a -> TMultiOsActions a
pattern AWindows a <- MultiOsActions MultiOs{_windows=a}

instance Functor TMultiOsActions where
  fmap f (MultiOsActions (MultiOs i l w)) = MultiOsActions (MultiOs (f i) (f l) (f w))

instance Foldable TMultiOsActions where
  foldr f acc (MultiOsActions (MultiOs i l w)) = f i (f l (f w acc))

instance Traversable TMultiOsActions where
  traverse f (MultiOsActions (MultiOs i l w)) = MultiOsActions <$> (MultiOs <$> f i <*> f l <*> f w)

instance IsList MultiOsRawActions where
  type Item MultiOsRawActions = (OsSpecific, RawActions)
  fromList = MultiOsActions . fromListWithDefault (ACustom AutoUndefinedActions)
  toList _ = error "not implemented"

instance IsList MultiOsRawActionsAlteration where
  type Item MultiOsRawActionsAlteration = (OsSpecific, RawActionsAlteration)
  fromList = insertOrUpdate0d (multiOsActions $ ACustom AutoUndefinedActions) . fromList
  toList _ = error "not implemented"

instance IsList MultiOsRawActionsAlteration' where
  type Item MultiOsRawActionsAlteration' = [(Level, ActionUpgrade)]
  fromList = insertOrUpdate0d (multiOsActions $ ACustom AutoUndefinedActions) . fromList
  toList _ = error "not implemented"

instance IsList MultiOsRawActionsAlterations where
  type Item MultiOsRawActionsAlterations = (OsSpecific, RawActionsAlteration)
  fromList = MultiOsActions . fromListWithDefault Nothing . fmap (second Just)
  toList _ = error "not implemented"

instance IsList MultiOsResolvedActions where
  type Item MultiOsResolvedActions = (OsSpecific, ResolvedActions)
  fromList = MultiOsActions . fromListWithDefault (ACustom mempty)
  toList _ = error "not implemented"

instance ToJSON MultiOsRawActions where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON MultiOsRawActions where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

mapMultiOsActions :: MultiOsId (a -> a') -> TMultiOsActions a -> TMultiOsActions a'
mapMultiOsActions fs (MultiOsActions as) = MultiOsActions (onOss fs as)

mapMultiOsActionsWithOs :: (OsSpecific -> a -> a') -> TMultiOsActions a -> TMultiOsActions a'
mapMultiOsActionsWithOs f (MultiOsActions as) = MultiOsActions (mapWithOs f as)

multiOsActions :: a -> TMultiOsActions a
multiOsActions = MultiOsActions . toMultiOs

multiOsActionsWithDefault :: OsSpecific -> a -> a -> TMultiOsActions a
multiOsActionsWithDefault os a0 = MultiOsActions . toMultiOsWithDefault a0 os

toMultiOsRawActionsMap :: Map.Map k RawActions -> Map.Map k MultiOsRawActions
toMultiOsRawActionsMap = Map.map multiOsActions

toMultiOsRawActionsAlteration :: RawActionsAlteration -> MultiOsRawActionsAlteration
toMultiOsRawActionsAlteration a = InsertOrUpdate
  (multiOsActions (alter a rawUndefinedActions))
  (multiOsActions (Just a))

toOsRawActionsAlteration :: OsSpecific -> RawActionsAlteration -> MultiOsRawActionsAlteration
toOsRawActionsAlteration os a = InsertOrUpdate
  (multiOsActionsWithDefault os rawUndefinedActions (alter a rawUndefinedActions))
  (multiOsActionsWithDefault os Nothing (Just a))

toMultiOsRawActionsAlterationMap :: Map.Map k RawActionsAlteration -> Map.Map k MultiOsRawActionsAlteration
toMultiOsRawActionsAlterationMap = Map.map toMultiOsRawActionsAlteration

toOsRawActionsAlterationMap :: OsSpecific -> Map.Map k RawActionsAlteration -> Map.Map k MultiOsRawActionsAlteration
toOsRawActionsAlterationMap os = Map.map (toOsRawActionsAlteration os)

class IsMultiOsActions a b where
  to_multiOsActions :: a -> TMultiOsActions b

instance IsMultiOsActions (TMultiOsActions a) a where
  to_multiOsActions = id

instance IsMultiOsActions (MultiOsId a) a where
  to_multiOsActions = MultiOsActions

instance Differentiable MultiOsRawActions MultiOsRawActionsDeltas where
  diff (MultiOsActions as1) (MultiOsActions as2) = MultiOsActions <$> diff as1 as2

{- Modification of multi-os raw actions.
[TODO] revise these rules
Modifications on 'OsIndependent' may impact 'OS' entries.

They are subject to the following rules:

- when _adding an action_ to a previously unmapped VK, then the VK
  is discarded for all the 'OS's.
- when _discarding a VK_, it is also discarded for all 'OS's.
- when updating a VK, discard it for all 'OS's.

**[TODO]** in the future, we would like to replace the last rule the following:

- when _updating_ a VK to a 'SingleAction', 'NoActions' or 'UndefinedActions',
  then discard the VK for all the 'OS's.
- when _updating_ some levels that are also defined for some 'OS's, then
  discard them ('UndefinedAction').
-}

instance {-# OVERLAPPING #-} Alterable MultiOsRawActions RawActions where
  alter as = alter (multiOsActions as)

instance {-# OVERLAPPING #-} Alterable MultiOsRawActions Actions where
  alter as = alter (multiOsActions . custom $ as)

instance Alterable MultiOsRawActions MultiOsRawActionsAlterations where
  alter (MultiOsActions (MultiOs di dl dw)) (MultiOsActions (MultiOs i l w)) = MultiOsActions (MultiOs i' l' w')
    where
      i' = malter di i
      diff_ = diff i i'
      l' = malter (diff_ +> dl) l -- [TODO] check semigroup
      w' = malter (diff_ +> dw) w -- [TODO] check semigroup

instance Alterable MultiOsRawActions RawActionsAlteration where
  alter u = mapMultiOsActionsWithOs (\_ -> alter u)

instance Alterable MultiOsRawActions RawActionsAlterations where
  alter as =
    let u = insertOrUpdate0d rawUndefinedActions as
    in alter (multiOsActions . Just $ u)

-- instance Alterable MultiOsRawActions ActionsAlterations where
--   alter as =
--     let u = insertOrUpdate0d rawUndefinedActions as
--     in alter (multiOsActions . Just $ u)

multiOsRawActionsToResolvedActions :: OsSpecific -> MultiOsRawActions -> ResolvedActions
multiOsRawActionsToResolvedActions os = bimap go go . getOs os . unMultiOsActions
  where go = resolveModifiersOptionsLax

multiOsRawActionsToRawActions :: OsSpecific -> MultiOsRawActions -> RawActions
multiOsRawActionsToRawActions os (MultiOsActions as) = getOs os as


--- Utils ---------------------------------------------------------------------

forceMultiosCustomActions :: MultiOsRawActions -> MultiOsRawActions
forceMultiosCustomActions = fmap forceCustomActions

multiOsRawSingleton :: Action -> MultiOsRawActions
multiOsRawSingleton = multiOsActions . rawSingleton

insertOrUpdate1d :: RawActionsAlteration -> MultiOsRawActionsAlteration
insertOrUpdate1d as =
  let u = multiOsActions (Just as) :: MultiOsRawActionsAlterations
  in InsertOrUpdate (alter u . multiOsActions $ rawUndefinedActions) u
