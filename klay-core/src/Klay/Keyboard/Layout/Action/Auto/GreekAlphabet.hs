module Klay.Keyboard.Layout.Action.Auto.GreekAlphabet
  ( CapsLockBehaviour(..)
  , greekLower
  , greekUpper
  , greek
  , greekMathematicalBold
  , greekMathematicalItalic
  , greekMathematicalBoldItalic
  , greekMathematicalDoubleStruck
  , greekActionMap
  , greekMathematicalBoldMap
  , greekMathematicalItalicMap
  , greekMathematicalBoldItalicMap
  , greekMathematicalDoubleStruckMap )
  where

import Data.Map.Strict qualified as Map
import Control.Monad ((>=>))
import Data.Char (toLower)
import Control.Arrow ((&&&), (>>>))

import Klay.Keyboard.Hardware.Key (Key, keyToChar, keyGroupLetters)
import Klay.Keyboard.Layout.Action.Action (Action(NoAction), toAction)
import Klay.Keyboard.Layout.Action.Auto (keysToActionMap2, mkModifierOptions)
import Klay.Keyboard.Layout.Action.Map (RawActionsKeyMap)
import Klay.Keyboard.Layout.Level (Level)
import Klay.Keyboard.Layout.Modifier (CapsLockBehaviour(..))
import Klay.Utils.ISO_9995_9 (latinToGreekISO_9995_9)
import Klay.Utils.Unicode (unsafeUnicodeOffset)

-- Utility functions
mkPair :: (Char -> Char) -> (Char -> Char) -> Maybe Char -> (Maybe Char, Maybe Char)
mkPair f1 f2 = fmap f1 &&& fmap f2

-- [NOTE] The offset is for the upper case
mkGreekPairWithOffsets :: Int -> Int -> Key -> (Maybe Char, Maybe Char)
mkGreekPairWithOffsets o1 o2 = greekUpper >>> mkPair (unsafeUnicodeOffset o1) (unsafeUnicodeOffset o2)

-- Conversion
greekLower :: Key -> Maybe Char
greekLower = greekUpper >>> fmap toLower

greekUpper ::  Key -> Maybe Char
greekUpper = keyToChar >=> (`Map.lookup` latinToGreekISO_9995_9)

greek :: Key -> (Maybe Char, Maybe Char)
greek = greekLower &&& greekUpper

greekMathematicalBold :: Key -> (Maybe Char, Maybe Char)
greekMathematicalBold = mkGreekPairWithOffsets 0x1d331 0x1d317

greekMathematicalItalic :: Key -> (Maybe Char, Maybe Char)
greekMathematicalItalic = mkGreekPairWithOffsets 0x1d36b 0x1d351

greekMathematicalBoldItalic :: Key -> (Maybe Char, Maybe Char)
greekMathematicalBoldItalic = mkGreekPairWithOffsets 0x1d3a5 0x1d38b

greekMathematicalDoubleStruck :: Key -> (Maybe Char, Maybe Char)
greekMathematicalDoubleStruck = greekUpper >>> maybe (Nothing, Nothing) mkLowerUpper
  where mkLower 'Γ' = Just 'ℽ'
        mkLower 'Π' = Just 'ℼ'
        mkLower _   = Nothing
        mkUpper 'Γ' = Just 'ℾ'
        mkUpper 'Π' = Just 'ℿ'
        mkUpper 'Σ' = Just '⅀'
        mkUpper  _  = Nothing
        mkLowerUpper = mkLower &&& mkUpper

-- Action maps

-- Utility functions
actionMap2 :: Maybe CapsLockBehaviour -> (Key -> Maybe (Action, Action)) -> Level -> Level -> RawActionsKeyMap
actionMap2 capsBehaviour f l1 l2 = keysToActionMap2 l1 l2 (mkModifierOptions capsBehaviour) f keyGroupLetters

toActions :: (Key -> (Maybe Char, Maybe Char)) -> (Key -> Maybe (Action, Action))
toActions = fmap \case
  (Nothing, Nothing) -> Nothing
  (c1, c2)           -> Just (maybe NoAction toAction c1, maybe NoAction toAction c2)

greekActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
greekActionMap opt = actionMap2 opt $ toActions greek

greekMathematicalBoldMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
greekMathematicalBoldMap opt = actionMap2 opt $ toActions greekMathematicalBold

greekMathematicalItalicMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
greekMathematicalItalicMap opt = actionMap2 opt $ toActions greekMathematicalItalic

greekMathematicalBoldItalicMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
greekMathematicalBoldItalicMap opt = actionMap2 opt $ toActions greekMathematicalBoldItalic

greekMathematicalDoubleStruckMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
greekMathematicalDoubleStruckMap opt = actionMap2 opt $ toActions greekMathematicalDoubleStruck
