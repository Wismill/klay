{-# LANGUAGE OverloadedLists #-}

-- | Automated Modifiers Options Resolution

module Klay.Keyboard.Layout.Action.Auto.ModifiersOptions
  ( TAutoActions(.., AutoUndefinedActions, AutoNoActions)
  , AutoActions
  , AutoActionsAlteration
  , AutoActionsAlterations
  , pattern M.DefaultCapsLockBehaviour
  , resolveModifiersOptions
  , resolveModifiersOptionsLax
  , autoDiscardModifiersOptions
  , noResolveModifiersOptions
  ) where

import Data.Char (toUpper, isLower, isUpper)
import Data.Maybe (fromMaybe)
import Data.Function ((&))
import Data.Bifunctor (Bifunctor(..))
-- import Data.Bifoldable (Bifoldable(..))
-- import Data.Bitraversable (Bitraversable(..))
import Control.Monad (guard)
import Control.DeepSeq (NFData(..))
import GHC.Generics (Generic(..))
import GHC.Exts (IsList(..))

import Test.QuickCheck.Arbitrary (Arbitrary(..), genericShrink)
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Keyboard.Layout.Action.Action (Action, toMonogram)
import Klay.Keyboard.Layout.Action.Actions
  ( Actions(..), ActionsAlterations(..), Level
  , actionAtLevel, definedLevels, actionsOptions)
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.Modifier
  ( ModifiersOptions(NoModifiersOptions)
  , AutoModifiersOptions(..), AutoModifiersOptionsMode(..) )
import Data.Alterable (Differentiable(..), Magma(..), Alterable(..), Alteration)
import Klay.Utils (jsonOptions)

-- | A wrapper around 'Actions' to describe how to decuce 'ModifiersOptions' automatically
type AutoActions            = TAutoActions AutoModifiersOptions         Actions
type AutoActionsAlterations = TAutoActions (Maybe AutoModifiersOptions) ActionsAlterations
type AutoActionsAlteration  = Alteration AutoActions AutoActionsAlterations

-- | A container for both 'AutoActions' and 'AutoActionsAlterations'
data TAutoActions a b = AutoActions
  { _aAuto    :: !a -- ^ Automatic modifiers options configuration
  , _aActions :: !b -- ^ Actions
  } deriving (Generic, NFData, Functor, Foldable, Traversable, Eq, Ord, Show)

pattern AutoUndefinedActions :: AutoActions
pattern AutoUndefinedActions <- AutoActions _ UndefinedActions where
  AutoUndefinedActions = AutoActions Auto UndefinedActions

pattern AutoNoActions :: AutoActions
pattern AutoNoActions <- AutoActions _ NoActions where
  AutoNoActions = AutoActions Auto NoActions

instance IsList AutoActions where
  type Item AutoActions = [(Level, Action)]
  fromList = AutoActions Auto . fromList
  toList _ = error "not implemented"

instance Bifunctor TAutoActions where
  bimap f g (AutoActions a as) = AutoActions (f a) (g as)

instance Magma AutoActions where
  -- AutoUndefinedActions +> as                   = as
  -- as                   +> AutoUndefinedActions = as
  AutoActions x xs +> AutoActions y ys = AutoActions z zs
    where zs = xs +> ys
          z = let z' = x +> y
              in if z' /= Manual && actionsOptions zs /= actionsOptions xs && actionsOptions zs /= actionsOptions ys
                then Manual
                else z'

instance Semigroup AutoActions where
  AutoActions a1 as1 <> AutoActions a2 as2 = AutoActions (a1 <> a2) (as1 <> as2)

instance Monoid AutoActions where
  mempty = AutoActions mempty mempty

instance (Arbitrary a, Arbitrary b) => Arbitrary (TAutoActions a b) where
  arbitrary = AutoActions <$> arbitrary <*> arbitrary
  shrink = genericShrink

instance Magma AutoActionsAlterations where
  AutoActions a xs +> AutoActions b ys = AutoActions (a +> b) (xs +> ys)

instance Differentiable AutoActions AutoActionsAlterations where
  diff (AutoActions a xs) (AutoActions b ys) = case (diff a b, diff xs ys) of
    (Nothing, Nothing) -> Nothing
    (d1, d2)           -> Just (AutoActions d1 $ fromMaybe EmptyActionsAlt d2)

instance Alterable AutoActions AutoModifiersOptions where
  alter m = first (alter m)

instance (Alterable Actions m) => Alterable AutoActions (TAutoActions (Maybe AutoModifiersOptions) m) where
  alter (AutoActions m ms) (AutoActions a as) = AutoActions (fromMaybe a m) (alter ms as)

instance {-# OVERLAPPABLE #-} (Alterable Actions m) => Alterable AutoActions m where
  alter m (AutoActions a as)
    | actionsOptions as' == actionsOptions as = AutoActions a      as'
    | otherwise                                 = AutoActions Manual as'
    where as' = alter m as

instance (ToJSON a, ToJSON b) => ToJSON (TAutoActions a b) where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 2))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 2))

instance (FromJSON a, FromJSON b) => FromJSON (TAutoActions a b) where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 2))

resolveModifiersOptionsLax :: AutoActions -> Actions
resolveModifiersOptionsLax = resolveModifiersOptions Lax

{-| Automated Modifiers Options Resolution

Based on:
  - [XKB FindAutomaticType](https://github.com/xkbcommon/libxkbcommon/blob/master/src/xkbcomp/symbols.c#L1291)
  - [XKB xkb_keysym_is_lower](https://github.com/xkbcommon/libxkbcommon/blob/master/src/keysym.c#L244)
-}
resolveModifiersOptions
  :: AutoModifiersOptionsMode
  -> AutoActions
  -> Actions
resolveModifiersOptions _ (AutoActions Manual as) = as
resolveModifiersOptions m (AutoActions Auto as) =
  resolveModifiersOptions m (AutoActions (AutoWith M.DefaultCapsLockBehaviour) as)
resolveModifiersOptions mode (AutoActions (AutoWith capsBehaviour) as0)
  -- length_ <= 1             = const -- unchanged
  | not hasSupportedLevels = alter NoModifiersOptions as0 -- clear options
  | otherwise = resolve as0
  where
    ls = definedLevels as0
    length_ = length ls
    hasSupportedLevels = hasLevels12 -- [TODO] numpad
    hasLevels12 = hasLevel1 && hasLevel2
    hasLevels34 = hasLevel3 && hasLevel4
    hasLevels56 = hasLevel5 && hasLevel6
    hasLevels78 = hasLevel7 && hasLevel8
    hasLevel1     = M.isoLevel1     `elem` ls
    hasLevel2     = M.isoLevel2     `elem` ls
    hasLevel2Caps = M.isoLevel2Caps `elem` ls
    hasLevel3     = M.isoLevel3     `elem` ls
    hasLevel4     = M.isoLevel4     `elem` ls
    hasLevel5     = M.isoLevel5     `elem` ls
    hasLevel6     = M.isoLevel6     `elem` ls
    hasLevel7     = M.isoLevel7     `elem` ls
    hasLevel8     = M.isoLevel8     `elem` ls
    -- hasControl      = M.control          `elem` ls
    -- has_noLevel5_or_higher = all (M.testMask M.isoLevel5) ls
    -- has_no_alt_super = not $ any (M.testMask (M.alternate <> M.super)) ls
    has_only_alphanumeric_level_four = not $ any (M.testMask $ mconcat [M.alt, M.super, M.numeric, M.isoLevel5]) ls
    resolve :: Actions -> Actions
    resolve UndefinedActions = UndefinedActions
    resolve NoActions        = NoActions
    resolve as               = as & alter if
      | length_ == 2 -> if hasLevels12 && isAlphabetic12
        then M.makeAlphabetic capsBehaviour
        else mempty
      | length_ <= 6 && hasLevels12 && has_only_alphanumeric_level_four -> if
        | hasLevel3 -> if
          | hasLevel2Caps && isUpper2Caps -> M.fourLevelWithLock
          | isAlphabetic12 && isAlphabetic34 -> M.makeFourLevelAlphabetic capsBehaviour
          | isAlphabetic12Caps               -> M.separateCapsAndShiftAlphabetic
          | isAlphabetic12                    -> M.makeFourLevelSemiAlphabetic capsBehaviour
          | otherwise                          -> mempty
        | isAlphabetic12                      -> M.makeAlphabetic capsBehaviour
        | otherwise -> mempty
      | length_ <= 8 -> if
        | hasLevels12 && hasLevels34 && hasLevels56 && hasLevels78 -> if
          | isAlphabetic12 && isAlphabetic34 && isAlphabetic56 && isAlphabetic78 ->
            M.makeEightLevelAlphabetic capsBehaviour
          | isAlphabetic12 -> M.makeEightLevelSemiAlphabetic capsBehaviour
          | otherwise -> mempty
        | isAlphabetic12 -> M.makeAlphabetic capsBehaviour
        | otherwise -> mempty
      | isAlphabetic12 -> M.makeAlphabetic capsBehaviour
      | isAlphabetic12 && isAlphabetic34 -> M.makeFourLevelAlphabetic capsBehaviour
      | otherwise -> mempty
      where
        sym1     = toMonogram $ actionAtLevel as M.isoLevel1
        sym2     = toMonogram $ actionAtLevel as M.isoLevel2
        sym2Caps = toMonogram $ actionAtLevel as M.isoLevel2Caps
        sym3     = toMonogram $ actionAtLevel as M.isoLevel3
        sym4     = toMonogram $ actionAtLevel as M.isoLevel4
        sym5     = toMonogram $ actionAtLevel as M.isoLevel5
        sym6     = toMonogram $ actionAtLevel as M.isoLevel6
        sym7     = toMonogram $ actionAtLevel as M.isoLevel7
        sym8     = toMonogram $ actionAtLevel as M.isoLevel8

        isUpper2Caps = maybe False isUpper sym2Caps

        isAlphabetic12     = isAlphabetic sym1 sym2
        isAlphabetic12Caps = isAlphabetic sym1 sym2Caps
        isAlphabetic34     = isAlphabetic sym3 sym4
        isAlphabetic56     = isAlphabetic sym5 sym6
        isAlphabetic78     = isAlphabetic sym7 sym8

    isAlphabetic :: Maybe Char -> Maybe Char -> Bool
    isAlphabetic ms1 ms2 = Just True == do
      s1 <- ms1
      s2 <- ms2
      guard (isLower s1 && isUpper s2)
      pure case mode of
        Strict -> toUpper s1 == s2
        Lax    -> True

autoDiscardModifiersOptions :: Actions -> AutoActions
autoDiscardModifiersOptions = AutoActions Auto . alter NoModifiersOptions

noResolveModifiersOptions :: AutoActions -> Actions
noResolveModifiersOptions = _aActions . alter NoModifiersOptions
