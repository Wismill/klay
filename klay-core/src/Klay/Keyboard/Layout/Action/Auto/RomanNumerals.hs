module Klay.Keyboard.Layout.Action.Auto.RomanNumerals
  ( romanNumerals
  , romanNumeralLowerCase
  , romanNumeralUpperCase
  , romanNumeralsMap
  , keyToRomanNumeralLowerCase
  , keyToRomanNumeralUpperCase )
  where

import Data.Char (toLower, chr)
import Control.Arrow ((>>>))

import Klay.Keyboard.Hardware.Key
import Klay.Keyboard.Layout.Action.Action (Action, toAction)
import Klay.Keyboard.Layout.Action.Auto (keysToActionMap2, defaultModifiersOptions)
import Klay.Keyboard.Layout.Action.Map (RawActionsKeyMap)
import Klay.Keyboard.Layout.Level (Level)

keyToRomanNumeralUpperCase :: Key -> Maybe Char
keyToRomanNumeralUpperCase I   = Just '\x2160'
keyToRomanNumeralUpperCase V   = Just '\x2164'
keyToRomanNumeralUpperCase X   = Just '\x2169'
keyToRomanNumeralUpperCase L   = Just '\x216C'
keyToRomanNumeralUpperCase C   = Just '\x216D'
keyToRomanNumeralUpperCase D   = Just '\x216E'
keyToRomanNumeralUpperCase M   = Just '\x216F'
keyToRomanNumeralUpperCase N0  = Just '\x2169'
keyToRomanNumeralUpperCase KP0 = Just '\x2169'
keyToRomanNumeralUpperCase vk  = keyDecimalToInt >>> fmap ((+0x215F) >>> chr) $ vk

keyToRomanNumeralLowerCase :: Key -> Maybe Char
keyToRomanNumeralLowerCase = fmap toLower . keyToRomanNumeralUpperCase

romanNumeralUpperCase :: Key -> Maybe Action
romanNumeralUpperCase = fmap toAction . keyToRomanNumeralUpperCase

romanNumeralLowerCase :: Key -> Maybe Action
romanNumeralLowerCase = fmap toAction . keyToRomanNumeralLowerCase

romanNumerals :: Key -> Maybe (Action, Action)
romanNumerals vk = (,) <$> romanNumeralLowerCase vk <*> romanNumeralUpperCase vk

romanNumeralsMap :: Level -> Level -> RawActionsKeyMap
romanNumeralsMap l1 l2 = keysToActionMap2 l1 l2 defaultModifiersOptions romanNumerals keys
  where keys = mconcat [keyGroupLetters, keyGroupNumerals, keyGroupNumpadDigits]
