{-|
Description : Latin alphabet and mathematical variants

See Unicode block [Mathematical Alphanumeric Symbols](https://en.wikipedia.org/wiki/Mathematical_Alphanumeric_Symbols).
-}

module Klay.Keyboard.Layout.Action.Auto.LatinAlphabet
  ( CapsLockBehaviour(..)
  , latinActionMap
  , latinSmallCapitalsActionMap
  , latinMathematicalBoldActionMap
  , latinMathematicalItalicActionMap
  , latinMathematicalBoldItalicActionMap
  , latinMathematicalScriptActionMap
  , latinMathematicalScriptBoldActionMap
  , latinMathematicalFrakturActionMap
  , latinMathematicalFrakturBoldActionMap
  , latinMathematicalDoubleStruckActionMap
  , latinLower
  , latinUpper
  , latin
  , latinSmallCapital
  , latinMathematicalBold
  , latinMathematicalItalic
  , latinMathematicalBoldItalic
  , latinMathematicalScript
  , latinMathematicalScriptBold
  , latinMathematicalFraktur
  , latinMathematicalFrakturBold
  , latinMathematicalDoubleStruck
  , toActions )
  where

import Data.Char (toLower, ord)
import Control.Arrow ((&&&), (***), (>>>))
import Control.Monad ((>=>))

import Klay.Keyboard.Hardware.Key (Key(Space), keyLetterToChar, keyGroupLetters)
import Klay.Keyboard.Layout.Action.Action (Action, toAction)
import Klay.Keyboard.Layout.Action.Auto (keysToActionMap1, keysToActionMap2, mkModifierOptions)
import Klay.Keyboard.Layout.Action.Map (RawActionsKeyMap)
import Klay.Keyboard.Layout.Level (Level)
import Klay.Keyboard.Layout.Modifier (CapsLockBehaviour(..))
import Klay.Utils.Unicode.Latin qualified as L
import Klay.Utils.Unicode (unsafeUnicodeOffset)


latinLower :: Key -> Maybe Char
latinLower = fmap toLower . keyLetterToChar

latinUpper :: Key -> Maybe Char
latinUpper = keyLetterToChar

latin :: Key -> Maybe (Char, Char)
latin vk = (,) <$> latinLower vk <*> latinUpper vk

latinSmallCapital :: Key -> Maybe Char
latinSmallCapital = latinLower >=> L.latinSmallCapital

-- [NOTE] Unicode math alphanumeric symbols: https://en.wikipedia.org/wiki/Mathematical_Alphanumeric_Symbols

latinMathematicalBold :: Key -> Maybe (Char, Char)
latinMathematicalBold = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower = unsafeUnicodeOffset (0x1D41A - ord 'a') . toLower
        mkUpper = unsafeUnicodeOffset (0x1D400 - ord 'A')

latinMathematicalItalic :: Key -> Maybe (Char, Char)
latinMathematicalItalic = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower 'H' = '\x210e'
        mkLower  c  = unsafeUnicodeOffset 0x1d3ed . toLower $ c

        mkUpper     = unsafeUnicodeOffset 0x1d3f3

latinMathematicalBoldItalic :: Key -> Maybe (Char, Char)
latinMathematicalBoldItalic = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower = unsafeUnicodeOffset 0x1d421 . toLower
        mkUpper = unsafeUnicodeOffset 0x1d427

latinMathematicalScript :: Key -> Maybe (Char, Char)
latinMathematicalScript = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower 'E' = '\x212f'
        mkLower 'G' = '\x210a'
        mkLower 'O' = '\x2134'
        mkLower  c  = unsafeUnicodeOffset 0x1d455 . toLower $ c
        mkUpper 'B' = '\x212c'
        mkUpper 'E' = '\x2130'
        mkUpper 'F' = '\x2131'
        mkUpper 'H' = '\x210b'
        mkUpper 'I' = '\x2110'
        mkUpper 'L' = '\x2112'
        mkUpper 'M' = '\x2133'
        mkUpper 'R' = '\x211b'
        mkUpper  c  = unsafeUnicodeOffset 0x1d45b c

latinMathematicalScriptBold :: Key -> Maybe (Char, Char)
latinMathematicalScriptBold = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower = unsafeUnicodeOffset (0x1D4EA - ord 'a') . toLower
        mkUpper = unsafeUnicodeOffset (0x1D4D0 - ord 'A')

latinMathematicalFraktur :: Key -> Maybe (Char, Char)
latinMathematicalFraktur = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower = unsafeUnicodeOffset 0x1d4bd . toLower
        mkUpper 'C' = 'ℭ'
        mkUpper 'H' = 'ℌ'
        mkUpper 'I' = 'ℑ'
        mkUpper 'R' = 'ℜ'
        mkUpper 'Z' = 'ℨ'
        mkUpper  c  = unsafeUnicodeOffset 0x1d4c3 c

latinMathematicalFrakturBold :: Key -> Maybe (Char, Char)
latinMathematicalFrakturBold = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower = unsafeUnicodeOffset 0x1d525 . toLower
        mkUpper = unsafeUnicodeOffset 0x1d52b

latinMathematicalDoubleStruck :: Key -> Maybe (Char, Char)
latinMathematicalDoubleStruck = keyLetterToChar >>> fmap (mkLower &&& mkUpper)
  where mkLower = unsafeUnicodeOffset (0x1D552 - ord 'a') . toLower
        mkUpper 'C' = 'ℂ'
        mkUpper 'H' = 'ℍ'
        mkUpper 'N' = 'ℕ'
        mkUpper 'P' = 'ℙ'
        mkUpper 'Q' = 'ℚ'
        mkUpper 'R' = 'ℝ'
        mkUpper 'Z' = 'ℤ'
        mkUpper  c  = unsafeUnicodeOffset 0x1d4f7 c

keys :: [Key]
keys = Space : keyGroupLetters

actionMap1 :: (Key -> Maybe Action) -> Level -> RawActionsKeyMap
actionMap1 f l1 =
  let mkOpts = mkModifierOptions Nothing
  in keysToActionMap1 l1 mkOpts f keys

actionMap2 :: Maybe CapsLockBehaviour -> (Key -> Maybe (Action, Action)) -> Level -> Level -> RawActionsKeyMap
actionMap2 capsBehaviour f l1 l2 =
  let mkOpts = mkModifierOptions capsBehaviour
  in keysToActionMap2 l1 l2 mkOpts f keys

toActions :: (Key -> Maybe (Char, Char)) -> (Key -> Maybe (Action, Action))
toActions f = f >>> fmap (toAction *** toAction)

latinActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinActionMap opt = actionMap2 opt $ toActions latin

latinSmallCapitalsActionMap :: Level -> RawActionsKeyMap
latinSmallCapitalsActionMap = actionMap1 $ fmap toAction . latinSmallCapital

latinMathematicalBoldActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalBoldActionMap opt = actionMap2 opt $ toActions latinMathematicalBold

latinMathematicalItalicActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalItalicActionMap opt = actionMap2 opt $ toActions latinMathematicalItalic

latinMathematicalBoldItalicActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalBoldItalicActionMap opt = actionMap2 opt $ toActions latinMathematicalBoldItalic

latinMathematicalScriptActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalScriptActionMap opt = actionMap2 opt $ toActions latinMathematicalScript

latinMathematicalScriptBoldActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalScriptBoldActionMap opt = actionMap2 opt $ toActions latinMathematicalScriptBold

latinMathematicalFrakturActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalFrakturActionMap opt = actionMap2 opt $ toActions latinMathematicalFraktur

latinMathematicalFrakturBoldActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalFrakturBoldActionMap opt = actionMap2 opt $ toActions latinMathematicalFrakturBold

latinMathematicalDoubleStruckActionMap :: Maybe CapsLockBehaviour -> Level -> Level -> RawActionsKeyMap
latinMathematicalDoubleStruckActionMap opt = actionMap2 opt $ toActions latinMathematicalDoubleStruck
