module Klay.Keyboard.Layout.Action.Auto.ArabicNumerals
  ( toWesternArabicNumeral
  , toWesternArabicNumeralSubscript
  , toWesternArabicNumeralSuperscript
  , toWesternArabicNumeralDoubleStruck
  , toWesternArabicNumeralBold
  , westernArabicNumeralsMap )
  where

import Data.Char (chr)
import Control.Monad ((>=>))

import Klay.Keyboard.Hardware.Key (Key, keyDecimalToChar, keyDecimalToInt, keyToChar, keyGroupNumerals)
import Klay.Keyboard.Layout.Action.Action (Action, toAction)
import Klay.Keyboard.Layout.Action.Auto (keysToActionMap1, defaultModifiersOptions)
import Klay.Keyboard.Layout.Action.Map (RawActionsKeyMap)
import Klay.Keyboard.Layout.Level (Level)

{-
See: https://en.wikipedia.org/wiki/Mathematical_Alphanumeric_Symbols
-}

unicodeOffset :: Int -> Int -> Maybe Action
unicodeOffset o n
  | n >= 0 && n <= 9 = Just . toAction . chr $ o + n
  | otherwise        = Nothing

toWesternArabicNumeral :: Key -> Maybe Action
toWesternArabicNumeral = fmap toAction . keyDecimalToChar

toWesternArabicNumeralSubscript :: Key -> Maybe Action
toWesternArabicNumeralSubscript = keyDecimalToInt >=> unicodeOffset 0x2080

toWesternArabicNumeralSuperscript :: Key -> Maybe Action
toWesternArabicNumeralSuperscript = keyDecimalToInt >=> toSuperscript
  where toSuperscript n
          | n == 1    = Just . toAction $ '\xb9'
          | n == 2    = Just . toAction $ '\xb2'
          | n == 3    = Just . toAction $ '\xb3'
          | otherwise = unicodeOffset 0x2070 n

toWesternArabicNumeralDoubleStruck :: Key -> Maybe Action
toWesternArabicNumeralDoubleStruck = keyDecimalToInt >=> unicodeOffset 0x1d7d8

toWesternArabicNumeralBold :: Key -> Maybe Action
toWesternArabicNumeralBold = keyDecimalToInt >=> unicodeOffset 0x1d7ce

westernArabicNumeralsMap :: Level -> RawActionsKeyMap
westernArabicNumeralsMap l = keysToActionMap1 l defaultModifiersOptions (fmap toAction . keyToChar) keyGroupNumerals
