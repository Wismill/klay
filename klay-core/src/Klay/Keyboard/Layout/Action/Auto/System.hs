module Klay.Keyboard.Layout.Action.Auto.System
  ( systemActionMap
  , defaultSystemAction )
  where

import Data.Map.Strict qualified as Map
import Control.Arrow ((&&&))

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.Default (defaultSystemAction, osDefault)
import Klay.Keyboard.Layout.Action.Map (RawActionsKeyMap)
import Klay.OS (OsSpecific)

-- | Default action map for system keys
systemActionMap :: OsSpecific -> RawActionsKeyMap
systemActionMap os = Map.fromList . fmap (id &&& osDefault os) $ K.keyGroupNonAlphanumeric
