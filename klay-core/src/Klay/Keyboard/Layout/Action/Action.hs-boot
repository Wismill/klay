module Klay.Keyboard.Layout.Action.Action
  ( Action(..)
  ) where

import Data.Hashable
-- import Control.DeepSeq (NFData)
-- import GHC.Generics (Generic)
-- import TextShow (TextShow(..), FromStringShow(..))

import Klay.Keyboard.Layout.Action.DeadKey (DeadKey)
import Klay.Keyboard.Layout.Action.Special (SpecialAction)
import Klay.Keyboard.Layout.Modifier.Types (Modifier(..))
import Klay.Utils.UserInput (RawLText)


data Action
  = AChar !Char
  | AText !RawLText
  | ADeadKey !DeadKey
  | AModifier !Modifier
  | ASpecial !SpecialAction
  | NoAction
  | UndefinedAction

instance Eq Action
instance Ord Action
instance Hashable Action
