{-# LANGUAGE FlexibleInstances     #-}

module Klay.Keyboard.Layout.Action.Raw
  ( -- * Customisable Actions
    CustomActions
    (..
    , ADefault, ACustom
    , RawUndefinedActions, RawNoActions)
  , untagCustomisableActions

    -- * Raw Actions
  -- , IsRawActions(..)
  , RawActions
  , RawActionsAlteration
  , RawActionsAlterations
  , RawActionsDelta
  , RawActionsChanges

    -- * Resolved Actions
  , ResolvedAction
  , ResolvedActions
  , ResolvedActionsAlteration
  , ResolvedActionsAlterations
  , ResolvedActionsDelta
  , ResolvedActionsChanges

    -- * Utils
  , rawActionsToActions
  , forceCustomActions
  , custom
  , customAuto
  , customManual
  , rawSingleton
  , rawUndefinedActions
  , rawNoActions

  , arbitraryADefault
  , arbitraryACustom
  ) where

import Data.Bifunctor (Bifunctor(..))
import Data.Bifoldable (Bifoldable(..))
import Data.Bitraversable (Bitraversable(..))
import Control.DeepSeq (NFData(..))
import GHC.Generics (Generic, Generic1)
import GHC.Exts (IsList(..))

import Test.QuickCheck.Arbitrary (Arbitrary(..), genericShrink)
import Test.QuickCheck.Gen (Gen)
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Keyboard.Layout.Action.Action
  (Action(..), IsAction(..), ActionUpgrade)
import Klay.Keyboard.Layout.Action.Actions
  (Actions(..), ActionsAlterations(..), IsActions(..), Level)
import Klay.Keyboard.Layout.Action.Auto.ModifiersOptions
  ( TAutoActions(..), AutoActions, AutoActionsAlterations
  , resolveModifiersOptionsLax, autoDiscardModifiersOptions)
import Klay.Keyboard.Layout.Modifier
  (ModifiersOptions, AutoModifiersOptions(..))
import Data.Alterable
  ( Differentiable(..), Magma(..), Alterable(..), Alteration(..), Delta(..)
  , defaultDiff, insertOrUpdate0d )
import Data.Customisable
  (Customisable(..), CustomisableChange(..), CustomisableTypeChange(..), untag)
import Klay.Utils (toJsonOptionsNewtype)

-- | Actions mapped to a key, that may have a default OS value and which auto modifiers options are not resolved.
type RawActions                = CustomActions AutoActions AutoActions
-- | Actions mapped to a key, that may have a default OS value.
type ResolvedActions           = CustomActions Actions     Actions
type ResolvedAction            = Customisable  Action      Action

type RawActionsAlterations      = AutoActionsAlterations
type ResolvedActionsAlterations = ActionsAlterations
type RawActionsAlteration       = Alteration RawActions      RawActionsAlterations
type ResolvedActionsAlteration  = Alteration ResolvedActions ResolvedActionsAlterations
type RawActionsChanges          = CustomisableChange RawActionsAlterations
type ResolvedActionsChanges     = CustomisableChange ResolvedActionsAlterations
type RawActionsDelta            = Delta RawActions RawActionsChanges
type ResolvedActionsDelta       = Delta ResolvedActions ResolvedActionsChanges


--- Customisable Actions ------------------------------------------------------

-- | A container for both 'RawActions' and 'ResolvedActions'.
newtype CustomActions a b = CustomActions { unCustomActions :: Customisable a b }
  deriving stock (Generic, Generic1, Show)
  deriving newtype (NFData, Functor, Bifunctor, Foldable, Bifoldable, Eq, Ord)

{-# COMPLETE ADefault, ACustom #-}
pattern ADefault :: a -> CustomActions a b
pattern ADefault a = CustomActions (Default a)

pattern ACustom :: b -> CustomActions a b
pattern ACustom b = CustomActions (Custom b)

instance Traversable (CustomActions a) where
  traverse f (CustomActions as) = CustomActions <$> traverse f as

instance Bitraversable CustomActions where
  bitraverse f g (CustomActions as) = CustomActions <$> bitraverse f g as

instance (Arbitrary a, Arbitrary b) => Arbitrary (CustomActions a b) where
  arbitrary = CustomActions <$> arbitrary
  shrink = genericShrink

instance ToJSON RawActions where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON RawActions where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

arbitraryADefault :: (Arbitrary a) => Gen (CustomActions a b)
arbitraryADefault = ADefault <$> arbitrary

arbitraryACustom :: (Arbitrary b) => Gen (CustomActions a b)
arbitraryACustom = ACustom <$> arbitrary


--- Raw actions ---------------------------------------------------------------

pattern RawUndefinedActions :: RawActions
pattern RawUndefinedActions = ACustom AutoUndefinedActions

pattern RawNoActions :: RawActions
pattern RawNoActions = ACustom AutoNoActions

instance IsList RawActions where
  type Item RawActions = [(Level, Action)]
  fromList = ACustom . AutoActions Auto . fromList
  toList _ = error "not implemented"

instance IsList RawActionsAlteration where
  type Item RawActionsAlteration = [(Level, ActionUpgrade)]
  fromList [] = Delete
  fromList as = let a = fromList as :: ActionsAlterations
                in insertOrUpdate0d rawUndefinedActions (AutoActions Nothing a)
  toList _ = error "not implemented"

instance Magma RawActions where
  ADefault _                  +> ADefault y        = ADefault y -- Right-biased
  ADefault (AutoActions x xs) +> ACustom (AutoActions y ys) =
    let f = AutoActions (x +> y)
    in bimap f f (ADefault xs +> ACustom ys)
  ACustom (AutoActions x xs) +> ADefault (AutoActions y ys) =
    let f = AutoActions (x +> y)
    in bimap f f (ACustom xs +> ADefault ys)
  ACustom xs +> ACustom ys = ACustom (xs +> ys)

instance Semigroup RawActions where
  -- CustomActions a <> CustomActions b = CustomActions (a <> b)
  ADefault a          <> ADefault b          = ADefault (a <> b)
  ADefault a          <> RawUndefinedActions = ADefault a
  ADefault a          <> ACustom b           = ACustom (a <> b)
  RawUndefinedActions <> ADefault b          = ADefault b
  ACustom a           <> ADefault b          = ADefault (a <> b)
  ACustom a           <> ACustom b           = ACustom (a <> b)

instance Monoid RawActions where
  mempty = CustomActions mempty

instance Differentiable RawActions RawActionsAlteration where
  diff (ADefault as1) (ADefault as2) = InsertOrReplace (ADefault as2) <$ defaultDiff as1 as2
  diff (ADefault as1) (ACustom as2)  = InsertOrUpdate (ACustom as2) <$> diff as1 as2
  diff _             as@(ADefault _) = Just (InsertOrReplace as)
  diff (ACustom as1) (ACustom as2)   = Update <$> diff as1 as2

instance Differentiable RawActions RawActionsAlterations where
  diff as1 as2 = diff (untagCustomisableActions as1) (untagCustomisableActions as2)

instance Differentiable RawActions RawActionsChanges where
  diff (CustomActions as1) (CustomActions as2) = diff as1 as2

instance Differentiable RawActions RawActionsDelta where
  diff (ADefault AutoUndefinedActions) (ACustom AutoUndefinedActions) =
    Just $ Updated (CustomisableChange (Just ToCustom) Nothing)
  diff (ACustom AutoUndefinedActions) (ACustom AutoUndefinedActions) =
    Nothing
  diff as1 (ACustom AutoUndefinedActions) =
    Just $ Deleted as1
  diff (ADefault AutoUndefinedActions) as2 =
    Just $ Inserted as2
  diff (ACustom AutoUndefinedActions) as2 =
    Just $ Inserted as2
  diff as1 as2 = Updated <$> diff as1 as2

instance Alterable RawActions ModifiersOptions where
  alter mo = alter (Update (AutoActions (Just Manual) (alter mo EmptyActionsAlt)) :: RawActionsAlteration)

instance Alterable RawActions RawActionsAlteration where
  alter (InsertOrIgnore as)   (ADefault AutoUndefinedActions) = as
  alter (InsertOrIgnore _)    bs@(ADefault _)                 = bs
  alter (InsertOrIgnore as)   (ACustom AutoUndefinedActions)  = as
  alter (InsertOrIgnore _)    bs@(ACustom _)                  = bs
  alter (InsertOrUpdate as _) (ADefault AutoUndefinedActions) = as
  alter (InsertOrUpdate _  u) bs@(ADefault _)                 =
    alter (Update u :: RawActionsAlteration) bs
  alter (InsertOrUpdate as _) (ACustom AutoUndefinedActions)  = as
  alter (InsertOrUpdate _  u) (ACustom bs)                    = ACustom (alter u bs)
  alter (InsertOrReplace as)  _                               = as
  alter (Update u)            bs@(ADefault as)
    | _aActions as' == _aActions as = bs
    | otherwise                     = ACustom as'
    where as' = alter u as
  alter (Update u)            (ACustom bs)                       = ACustom (alter u bs)
  alter Delete                bs@(ADefault AutoUndefinedActions) = bs
  alter Delete                _                                  = ACustom AutoUndefinedActions

instance Alterable RawActions RawActionsAlterations where
  alter m (ADefault as)
    | as' == as = ADefault as
    | otherwise = ACustom as'
    where as' = alter m as
  alter m (ACustom as) = ACustom (alter m as)

instance Alterable RawActions RawActionsChanges where
  alter m (CustomActions as) = CustomActions $ alter m as

instance Alterable RawActionsAlteration ModifiersOptions where
  alter d = (+> Update (AutoActions (Just Manual) (alter d EmptyActionsAlt)))


--- Resolved Actions ----------------------------------------------------------

instance IsList ResolvedActions where
  type Item ResolvedActions = [(Level, Action)]
  fromList = ACustom . fromList
  toList _ = error "not implemented"

instance Magma ResolvedActions where
  ADefault _                +> ADefault b                = ADefault b -- Right-biased

  ADefault UndefinedActions +> ACustom NoActions         = ADefault NoActions
  ADefault a                +> ACustom UndefinedActions  = ADefault a
  ADefault a                +> ACustom b
    | a == b    = ADefault a       -- If identical then keep default
    | otherwise = ACustom (a +> b) -- Otherwise combine

  ACustom UndefinedActions  +> ADefault UndefinedActions = ADefault UndefinedActions
  ACustom a                 +> ADefault UndefinedActions = ACustom a
  ACustom _                 +> ADefault b                = ADefault b -- Right-biased

  ACustom a                 +> ACustom b                 = ACustom (a +> b)

instance Differentiable ResolvedActions ResolvedActionsChanges where
  diff (CustomActions as1) (CustomActions as2) = diff as1 as2

instance Alterable ResolvedActions ActionsAlterations where
  alter d = bimap (alter d) (alter d)

instance Alterable ResolvedActions (Alteration ResolvedActions ResolvedActions) where
  alter (InsertOrIgnore as')   (ADefault UndefinedActions) = as'
  alter (InsertOrIgnore as')   (ACustom UndefinedActions)  = as'
  alter (InsertOrIgnore _)     as                          = as
  alter (InsertOrUpdate as' _) (ADefault UndefinedActions) = as'
  alter (InsertOrUpdate as' _) (ACustom UndefinedActions)  = as'
  alter (InsertOrUpdate _ as') as                          = as +> as'
  alter (InsertOrReplace as')  _                           = as'
  alter (Update as')           as                          = as +> as'
  alter Delete                 _                           = ACustom UndefinedActions

instance Alterable ResolvedActions ResolvedActionsAlteration where
  alter (InsertOrIgnore as')   (ADefault UndefinedActions) = as'
  alter (InsertOrIgnore as')   (ACustom UndefinedActions)  = as'
  alter (InsertOrIgnore _)     as                         = as
  alter (InsertOrUpdate as' _) (ADefault UndefinedActions) = as'
  alter (InsertOrUpdate as' _) (ACustom UndefinedActions)  = as'
  alter (InsertOrUpdate _ a)   as                         = alter a as
  alter (InsertOrReplace as)   _                          = as
  alter (Update a)             as                         = alter a as
  alter Delete                 _                          = ACustom mempty


--- Utils ---------------------------------------------------------------------

rawActionsToActions :: RawActions -> Actions
rawActionsToActions = untagCustomisableActions . bimap go go
  where go = resolveModifiersOptionsLax

untagCustomisableActions :: CustomActions a a -> a
untagCustomisableActions (CustomActions as) = untag as

forceCustomActions :: RawActions -> RawActions
forceCustomActions (ADefault as) = ACustom as
forceCustomActions as            = as

custom :: (IsActions a) => a -> RawActions
custom = ACustom . checkOptions . toActions
  where checkOptions as@(Actions mo _ _)
          | mo == mempty = AutoActions Auto   as
          | otherwise    = AutoActions Manual as
        checkOptions as = AutoActions Auto as

customAuto :: (IsActions a) => a -> RawActions
customAuto = ACustom . autoDiscardModifiersOptions . toActions

customManual :: (IsActions a) => a -> RawActions
customManual = ACustom . AutoActions Manual . toActions

-- | Map a single action at all the levels.
rawSingleton :: (IsAction a) => a -> RawActions
rawSingleton = custom . toAction

-- | Undefined actions
rawUndefinedActions :: RawActions
rawUndefinedActions = ACustom AutoUndefinedActions

-- | No actions
rawNoActions :: RawActions
rawNoActions = ACustom AutoNoActions
