{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Action.Default
  ( -- * Default Actions
    defaultActions
  , mDefaultActions
  , defaultSystemAction
  , pattern DefaultCapsLockBehaviour
  , pattern DefaultAutoModifiersOptions
  , resolveModifiersOptions

    -- * Blank Layout
  , multiOsBlankActionMap
  , mkMultiOsBlankActionMap
  , osIndependentBlankActionMap
  , mkOsIndependentBlankActionMap
  , linuxBlankActionMap
  , mkLinuxSpecificBlankActionMap_update
  , windowsBlankActionMap
  , mkWindowsSpecificBlankActionMapUpdate

    -- * US Layout
  , multiOsUsActionMap
  , make_multiOsUsActionMap
  , osIndependentUsActionMap
  , linuxUsActionMap
  , windowsUsActionMap
  , makeOsIndependentUsActionMap
  , makeLinuxSpecificUsActionMapUpdate
  , mkWindowsSpecificUsActionMapUpdate

  , osDefault
  , osDefaults
  , checkDefaultActions
  , checkMultiOsDefaultActions
  ) where

import Data.Char (toLower)
import Data.Maybe (mapMaybe, fromMaybe)
import Data.List ((\\))
import Data.Ix (Ix(..))
import Data.Map.Strict qualified as Map
import Control.Arrow ((&&&))

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.Action
  ( Action(..), IsAction(..) )
import Klay.Keyboard.Layout.Action.Actions
  ( Actions(..), Level
  , singleton, mkActions )
import Klay.Keyboard.Layout.Action.Auto.ModifiersOptions
  ( TAutoActions(..), AutoActions, AutoActionsAlteration, resolveModifiersOptions )
import {-# SOURCE #-} Klay.Keyboard.Layout.Action.Map (MultiOsAutoActionsKeyMap, AutoActionsKeyMap, AutoActionsKeyMapUpdate)
import Klay.Keyboard.Layout.Action.MultiOs (TMultiOsActions(..), MultiOsRawActions, mapMultiOsActionsWithOs)
import Klay.Keyboard.Layout.Action.Raw (RawActions, CustomActions(..), untagCustomisableActions)
import Klay.Keyboard.Layout.Action.Special qualified as A
import Klay.Keyboard.Layout.Modifier
  ( Modifier(..), CapsLockBehaviour(..), AutoModifiersOptions(..)
  , control, alternate, isoLevel1, isoLevel2, isoLevel3, isoLevel4 )
import Klay.OS (OS(..), MultiOs(..), OsSpecific(..), mapWithOs, toMultiOs)
import Data.Alterable (Magma(..), Alterable(..), Alteration(..), (!>))


-- | Resolution of the OS-specific actions with default value.
defaultActions :: K.Key -> OsSpecific -> AutoActions
defaultActions key = fromMaybe AutoUndefinedActions . mDefaultActions key

-- | Resolution of the OS-specific actions.
mDefaultActions :: K.Key -> OsSpecific -> Maybe AutoActions
mDefaultActions key OsIndependent        = Map.lookup key osIndependentUsActionMap
mDefaultActions key (OsSpecific Linux)   = Map.lookup key linuxUsActionMap
mDefaultActions key (OsSpecific Windows) = Map.lookup key windowsUsActionMap

-- | Get the default actions of a VK for all OS
osDefaults :: K.Key -> MultiOsRawActions
osDefaults = MultiOsActions . mapWithOs osDefault . toMultiOs

-- | Get the default actions of a VK for a given OS
osDefault :: OsSpecific -> K.Key -> RawActions
osDefault os key = ADefault (defaultActions key os)

defaultSystemAction :: K.Key -> Maybe Action
-- Special actions(ASpecial )
defaultSystemAction K.PrintScreen      = Just (ASpecial A.Print)
defaultSystemAction K.ScrollLock       = Just (ASpecial A.Scroll_Lock)
defaultSystemAction K.Pause            = Just (ASpecial A.Pause)
defaultSystemAction K.F1               = Just (ASpecial A.F1)
defaultSystemAction K.F2               = Just (ASpecial A.F2)
defaultSystemAction K.F3               = Just (ASpecial A.F3)
defaultSystemAction K.F4               = Just (ASpecial A.F4)
defaultSystemAction K.F5               = Just (ASpecial A.F5)
defaultSystemAction K.F6               = Just (ASpecial A.F6)
defaultSystemAction K.F7               = Just (ASpecial A.F7)
defaultSystemAction K.F8               = Just (ASpecial A.F8)
defaultSystemAction K.F9               = Just (ASpecial A.F9)
defaultSystemAction K.F10              = Just (ASpecial A.F10)
defaultSystemAction K.F11              = Just (ASpecial A.F11)
defaultSystemAction K.F12              = Just (ASpecial A.F12)
defaultSystemAction K.F13              = Just (ASpecial A.F13)
defaultSystemAction K.F14              = Just (ASpecial A.F14)
defaultSystemAction K.F15              = Just (ASpecial A.F15)
defaultSystemAction K.F16              = Just (ASpecial A.F16)
defaultSystemAction K.F17              = Just (ASpecial A.F17)
defaultSystemAction K.F18              = Just (ASpecial A.F18)
defaultSystemAction K.F19              = Just (ASpecial A.F19)
defaultSystemAction K.F20              = Just (ASpecial A.F20)
defaultSystemAction K.F21              = Just (ASpecial A.F21)
defaultSystemAction K.F22              = Just (ASpecial A.F22)
defaultSystemAction K.F23              = Just (ASpecial A.F23)
defaultSystemAction K.F24              = Just (ASpecial A.F24)
defaultSystemAction K.Home             = Just (ASpecial A.Home)
defaultSystemAction K.End              = Just (ASpecial A.End)
defaultSystemAction K.PageUp           = Just (ASpecial A.Page_Up)
defaultSystemAction K.PageDown         = Just (ASpecial A.Page_Down)
defaultSystemAction K.Escape           = Just (ASpecial A.Escape)
defaultSystemAction K.CursorLeft       = Just (ASpecial A.CursorLeft)
defaultSystemAction K.CursorRight      = Just (ASpecial A.CursorRight)
defaultSystemAction K.CursorUp         = Just (ASpecial A.CursorUp)
defaultSystemAction K.CursorDown       = Just (ASpecial A.CursorDown)
defaultSystemAction K.ScreenLock       = Just (ASpecial A.XF86ScreenSaver)
defaultSystemAction K.Stop             = Just (ASpecial A.Cancel)
defaultSystemAction K.Cancel           = Just (ASpecial A.Cancel)
defaultSystemAction K.Save             = Just (ASpecial A.XF86Save)
defaultSystemAction K.MediaPlayCD      = Just (ASpecial A.XF86AudioPlay)
defaultSystemAction K.MediaPlay        = Just (ASpecial A.XF86AudioPlay)
defaultSystemAction K.MediaPause       = Just (ASpecial A.XF86AudioPause)
defaultSystemAction K.MediaPlayPause   = Just (ASpecial A.XF86AudioPlay) -- shift: XF86AudioPause
defaultSystemAction K.MediaStop        = Just (ASpecial A.XF86AudioStop)
defaultSystemAction K.MediaEject       = Just (ASpecial A.XF86Eject)
defaultSystemAction K.MediaPrevious    = Just (ASpecial A.XF86AudioPrev)
defaultSystemAction K.MediaNext        = Just (ASpecial A.XF86AudioNext)
defaultSystemAction K.MediaRewind      = Just (ASpecial A.XF86AudioRewind)
defaultSystemAction K.MediaForward     = Just (ASpecial A.XF86AudioForward)
defaultSystemAction K.VolumeMute       = Just (ASpecial A.XF86AudioMute)
defaultSystemAction K.VolumeDown       = Just (ASpecial A.XF86AudioLowerVolume)
defaultSystemAction K.VolumeUp         = Just (ASpecial A.XF86AudioRaiseVolume)
defaultSystemAction K.BrowserBack      = Just (ASpecial A.XF86Back)
defaultSystemAction K.BrowserForward   = Just (ASpecial A.XF86Forward)
defaultSystemAction K.BrowserRefresh   = Just (ASpecial A.XF86Refresh)
defaultSystemAction K.BrowserSearch    = Just (ASpecial A.XF86Search)
defaultSystemAction K.BrowserFavorites = Just (ASpecial A.XF86Favorites)
defaultSystemAction K.BrowserHomePage  = Just (ASpecial A.XF86HomePage)
defaultSystemAction K.Calculator       = Just (ASpecial A.XF86Calculator)
defaultSystemAction K.MediaPlayer      = Just (ASpecial A.XF86AudioMedia)
defaultSystemAction K.Browser          = Just (ASpecial A.XF86WWW)
defaultSystemAction K.Email1           = Just (ASpecial A.XF86Mail)
defaultSystemAction K.Email2           = Just (ASpecial A.XF86Mail)
defaultSystemAction K.Find             = Just (ASpecial A.Find)
defaultSystemAction K.Explorer         = Just (ASpecial A.XF86Explorer)
defaultSystemAction K.MyComputer       = Just (ASpecial A.XF86MyComputer)
defaultSystemAction K.Documents        = Just (ASpecial A.XF86Documents)
defaultSystemAction K.Help             = Just (ASpecial A.Help)
defaultSystemAction K.Launch1          = Just (ASpecial A.XF86Launch1)
defaultSystemAction K.Launch2          = Just (ASpecial A.XF86Launch2)
defaultSystemAction K.Launch3          = Just (ASpecial A.XF86Launch3)
defaultSystemAction K.Launch4          = Just (ASpecial A.XF86Launch4)
defaultSystemAction K.Power            = Just (ASpecial A.XF86PowerOff)
defaultSystemAction K.Suspend          = Just (ASpecial A.XF86Suspend)
defaultSystemAction K.Sleep            = Just (ASpecial A.XF86Sleep)
defaultSystemAction K.WakeUp           = Just (ASpecial A.XF86WakeUp)
defaultSystemAction K.BrightnessDown   = Just (ASpecial A.XF86MonBrightnessDown)
defaultSystemAction K.BrightnessUp     = Just (ASpecial A.XF86MonBrightnessUp)
defaultSystemAction K.KP0              = Just (ASpecial A.KP_0)
defaultSystemAction K.KP1              = Just (ASpecial A.KP_1)
defaultSystemAction K.KP2              = Just (ASpecial A.KP_2)
defaultSystemAction K.KP3              = Just (ASpecial A.KP_3)
defaultSystemAction K.KP4              = Just (ASpecial A.KP_4)
defaultSystemAction K.KP5              = Just (ASpecial A.KP_5)
defaultSystemAction K.KP6              = Just (ASpecial A.KP_6)
defaultSystemAction K.KP7              = Just (ASpecial A.KP_7)
defaultSystemAction K.KP8              = Just (ASpecial A.KP_8)
defaultSystemAction K.KP9              = Just (ASpecial A.KP_9)
defaultSystemAction K.KPDecimal        = Just (ASpecial A.KP_Decimal)
defaultSystemAction K.KPComma          = Just (ASpecial A.KP_Decimal)
defaultSystemAction K.KPJPComma        = Just (ASpecial A.KP_Separator)
defaultSystemAction K.KPPlusMinus      = Just (AChar '±')
defaultSystemAction K.KPDivide         = Just (ASpecial A.KP_Divide)
defaultSystemAction K.KPMultiply       = Just (ASpecial A.KP_Multiply)
defaultSystemAction K.KPSubtract       = Just (ASpecial A.KP_Subtract)
defaultSystemAction K.KPAdd            = Just (ASpecial A.KP_Add)
defaultSystemAction K.KPEnter          = Just (ASpecial A.KP_Enter)
defaultSystemAction K.KPEquals         = Just (ASpecial A.KP_Equal)
defaultSystemAction K.Tabulator        = Just (ASpecial A.Tab)
defaultSystemAction K.Return           = Just (ASpecial A.Return)
defaultSystemAction K.Backspace        = Just (ASpecial A.BackSpace)
defaultSystemAction K.Delete           = Just (ASpecial A.Delete)
defaultSystemAction K.Insert           = Just (ASpecial A.Insert)
defaultSystemAction K.Undo             = Just (ASpecial A.Undo)
defaultSystemAction K.Redo             = Just (ASpecial A.Redo)
defaultSystemAction K.Cut              = Just (ASpecial A.XF86Cut)
defaultSystemAction K.Copy             = Just (ASpecial A.XF86Copy)
defaultSystemAction K.Paste            = Just (ASpecial A.XF86Paste)
defaultSystemAction K.Menu             = Just (ASpecial A.Menu)
defaultSystemAction K.Muhenkan         = Just (ASpecial A.Muhenkan)
defaultSystemAction K.Henkan           = Just (ASpecial A.Henkan)
defaultSystemAction K.Katakana         = Just (ASpecial A.Katakana)
defaultSystemAction K.Hiragana         = Just (ASpecial A.Hiragana)
defaultSystemAction K.HiraganaKatakana = Just (ASpecial A.Hiragana_Katakana)
defaultSystemAction K.Hangeul          = Just (ASpecial A.Hangul)
defaultSystemAction K.Hanja            = Just (ASpecial A.Hangul_Hanja)
-- Modifiers
defaultSystemAction K.LShift     = Just (AModifier LShift)
defaultSystemAction K.RShift     = Just (AModifier RShift)
defaultSystemAction K.CapsLock   = Just (AModifier CapsLock)
defaultSystemAction K.LControl   = Just (AModifier LControl)
defaultSystemAction K.RControl   = Just (AModifier RControl)
defaultSystemAction K.LAlternate = Just (AModifier LAlternate)
defaultSystemAction K.RAlternate = Just (AModifier RAlternate)
defaultSystemAction K.NumLock    = Just (AModifier NumericLock)
defaultSystemAction K.LSuper     = Just (AModifier LSuper)
defaultSystemAction K.RSuper     = Just (AModifier RSuper)
defaultSystemAction _            = Nothing


--- Blank layout --------------------------------------------------------------

multiOsBlankActionMap :: MultiOsAutoActionsKeyMap
multiOsBlankActionMap = mkMultiOsBlankActionMap DefaultCapsLockBehaviour isoLevel1 isoLevel2

mkMultiOsBlankActionMap :: CapsLockBehaviour -> Level -> Level -> MultiOsAutoActionsKeyMap
mkMultiOsBlankActionMap opt l1 l2 = Map.fromList . mapMaybe go $ enumFromTo minBound maxBound
  where
    go key = case (findA key osIndependent_am, findA key linux_am, findA key windows_am) of
      (AutoUndefinedActions, AutoUndefinedActions, AutoUndefinedActions) -> Nothing
      (i, l, w) -> Just (key, MultiOsActions $ MultiOs i l w)
    findA = Map.findWithDefault AutoUndefinedActions
    osIndependent_am = mkOsIndependentBlankActionMap opt l1 l2
    linux_am   = alter (mkLinuxSpecificBlankActionMap_update   opt l1 l2) osIndependent_am
    windows_am = alter (mkWindowsSpecificBlankActionMapUpdate opt l1 l2) osIndependent_am

osIndependentBlankActionMap :: AutoActionsKeyMap
osIndependentBlankActionMap = mkOsIndependentBlankActionMap DefaultCapsLockBehaviour isoLevel1 isoLevel2

-- | OS-independent blank layout
mkOsIndependentBlankActionMap :: CapsLockBehaviour -> Level -> Level -> AutoActionsKeyMap
mkOsIndependentBlankActionMap opt l1 l2 = Map.unionWith (+>) system misc
  where
    system :: AutoActionsKeyMap
    system
      = Map.fromList
      . mapMaybe (\key -> (key,) . AutoActions auto_modifiersOptions . SingleAction <$> defaultSystemAction key)
      $ K.keyGroupNonAlphanumeric \\ (Map.keys misc <> asian_keys)
    misc :: AutoActionsKeyMap
    misc =
      [ K.KP0       ⇒ [l1 .= A.KP_0] !> auto_modifiersOptions
      , K.KP1       ⇒ [l1 .= A.KP_1] !> auto_modifiersOptions
      , K.KP2       ⇒ [l1 .= A.KP_2] !> auto_modifiersOptions
      , K.KP3       ⇒ [l1 .= A.KP_3] !> auto_modifiersOptions
      , K.KP4       ⇒ [l1 .= A.KP_4] !> auto_modifiersOptions
      , K.KP5       ⇒ [l1 .= A.KP_5] !> auto_modifiersOptions
      , K.KP6       ⇒ [l1 .= A.KP_6] !> auto_modifiersOptions
      , K.KP7       ⇒ [l1 .= A.KP_7] !> auto_modifiersOptions
      , K.KP8       ⇒ [l1 .= A.KP_8] !> auto_modifiersOptions
      , K.KP9       ⇒ [l1 .= A.KP_9] !> auto_modifiersOptions
      , K.KPDecimal ⇒ [l1 .= A.KP_Decimal] !> auto_modifiersOptions
      , K.KPComma   ⇒ [l1 .= A.KP_Decimal] !> auto_modifiersOptions
      , K.Tabulator ⇒ [l1 .= A.Tab, l2 .= A.Iso_Left_Tab] !> auto_modifiersOptions
      , K.Backspace ⇒ [l1 .= A.BackSpace, l2 .= A.BackSpace] !> auto_modifiersOptions
      ]
    asian_keys = K.KPJPComma : range (K.Muhenkan, K.Hanja)
    auto_modifiersOptions = AutoWith opt

linuxBlankActionMap :: AutoActionsKeyMap
linuxBlankActionMap
  = alter
    (mkLinuxSpecificBlankActionMap_update DefaultCapsLockBehaviour isoLevel1 isoLevel2)
    osIndependentBlankActionMap

-- [TODO] Complete the map
mkLinuxSpecificBlankActionMap_update :: CapsLockBehaviour -> Level -> Level -> AutoActionsKeyMapUpdate
mkLinuxSpecificBlankActionMap_update _opt _l1 _l2 =
  [ K.PrintScreen ⇨ InsertOrReplace [isoLevel1 .= A.Print, alternate .= A.Sys_Req]
  , K.Pause       ⇨ InsertOrReplace [isoLevel1 .= A.Pause      , control .= A.Break]
  ]

windowsBlankActionMap :: AutoActionsKeyMap
windowsBlankActionMap
  = alter
    (mkWindowsSpecificBlankActionMapUpdate DefaultCapsLockBehaviour isoLevel1 isoLevel2)
    osIndependentBlankActionMap

-- [TODO] Complete the map
mkWindowsSpecificBlankActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> AutoActionsKeyMapUpdate
mkWindowsSpecificBlankActionMapUpdate opt l1 l2 =
  [ -- Restrict singletons to some levels
    K.Escape      ⇨ InsertOrReplace ([l1 .= A.Escape, l2 .= A.Escape, control .= A.Escape]!> auto_modifiersOptions)
  , K.Return      ⇨ InsertOrReplace ([l1 .= A.Return, l2 .= A.Return, control .= '\n']!> auto_modifiersOptions)
  , K.Backspace   ⇨ InsertOrReplace ([l1 .= A.BackSpace, l2 .= A.BackSpace, control .= '\x007f']!> auto_modifiersOptions)
  , K.Space       ⇨ InsertOrReplace ([l1 .= ' ', l2 .= ' ', control .= ' ']!> auto_modifiersOptions)
  , K.Tabulator   ⇨ InsertOrReplace ([l1 .= A.Tab, l2 .= A.Tab]!> auto_modifiersOptions)
  -- Numpad
  , K.KPDecimal   ⇨ InsertOrReplace ([l1 .= A.KP_Decimal, l2 .= A.KP_Decimal] !> auto_modifiersOptions)
  , K.KPComma     ⇨ InsertOrReplace ([l1 .= A.KP_Decimal, l2 .= A.KP_Decimal] !> auto_modifiersOptions)
  , K.KPDivide    ⇨ InsertOrReplace ([l1 .= A.KP_Divide, l2 .= A.KP_Divide] !> auto_modifiersOptions)
  , K.KPMultiply  ⇨ InsertOrReplace ([l1 .= A.KP_Multiply, l2 .= A.KP_Multiply] !> auto_modifiersOptions)
  , K.KPSubtract  ⇨ InsertOrReplace ([l1 .= A.KP_Subtract, l2 .= A.KP_Subtract] !> auto_modifiersOptions)
  , K.KPAdd       ⇨ InsertOrReplace ([l1 .= A.KP_Add, l2 .= A.KP_Add] !> auto_modifiersOptions)
  , K.KPEquals    ⇨ InsertOrReplace ([l1 .= A.KP_Equal, l2 .= A.KP_Equal] !> auto_modifiersOptions)
  ]
  where auto_modifiersOptions = AutoWith opt


--- US layout -----------------------------------------------------------------

multiOsUsActionMap :: MultiOsAutoActionsKeyMap
multiOsUsActionMap = make_multiOsUsActionMap DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4

make_multiOsUsActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsAutoActionsKeyMap
make_multiOsUsActionMap opt l1 l2 l3 l4 = Map.fromList . mapMaybe go $ enumFromTo minBound maxBound
  where
    go key = case (findA key osIndependent_am, findA key linux_am, findA key windows_am) of
      (AutoUndefinedActions, AutoUndefinedActions, AutoUndefinedActions) -> Nothing
      (i, l, w) -> Just (key, MultiOsActions $ MultiOs i l w)
    findA = Map.findWithDefault AutoUndefinedActions
    osIndependent_am = makeOsIndependentUsActionMap opt l1 l2
    linux_am   = alter (makeLinuxSpecificUsActionMapUpdate   opt l1 l2 l3 l4) osIndependent_am
    windows_am = alter (mkWindowsSpecificUsActionMapUpdate opt l1 l2      ) osIndependent_am

-- | OS-independent action map for US layout
osIndependentUsActionMap :: AutoActionsKeyMap
osIndependentUsActionMap = makeOsIndependentUsActionMap DefaultCapsLockBehaviour isoLevel1 isoLevel2

-- | OS-independent definition of the US layout
makeOsIndependentUsActionMap :: CapsLockBehaviour -> Level -> Level -> AutoActionsKeyMap
makeOsIndependentUsActionMap opt l1 l2 = Map.unionsWith (+>) ams
  where
    ams :: [AutoActionsKeyMap]
    ams = [mkOsIndependentBlankActionMap opt l1 l2, letters, punctuation]
    letters :: AutoActionsKeyMap
    letters = mkMap2 keyLetterToChar K.keyGroupLetters
    punctuation :: AutoActionsKeyMap
    punctuation =
      [ -- 4th row
        K.Tilde     ⇒ [l1 .= '`', l2 .= '~'] !> auto_modifiersOptions
      , K.N1        ⇒ [l1 .= '1', l2 .= '!'] !> auto_modifiersOptions
      , K.N2        ⇒ [l1 .= '2', l2 .= '@'] !> auto_modifiersOptions
      , K.N3        ⇒ [l1 .= '3', l2 .= '#'] !> auto_modifiersOptions
      , K.N4        ⇒ [l1 .= '4', l2 .= '%'] !> auto_modifiersOptions
      , K.N5        ⇒ [l1 .= '5', l2 .= '$'] !> auto_modifiersOptions
      , K.N6        ⇒ [l1 .= '6', l2 .= '^'] !> auto_modifiersOptions
      , K.N7        ⇒ [l1 .= '7', l2 .= '&'] !> auto_modifiersOptions
      , K.N8        ⇒ [l1 .= '8', l2 .= '*'] !> auto_modifiersOptions
      , K.N9        ⇒ [l1 .= '9', l2 .= '('] !> auto_modifiersOptions
      , K.N0        ⇒ [l1 .= '0', l2 .= ')'] !> auto_modifiersOptions
      , K.Minus     ⇒ [l1 .= '-', l2 .= '_'] !> auto_modifiersOptions
      , K.Plus      ⇒ [l1 .= '=', l2 .= '+'] !> auto_modifiersOptions
      -- 3rd row
      , K.LBracket  ⇒ [l1 .= '[', l2 .= '{'] !> auto_modifiersOptions
      , K.RBracket  ⇒ [l1 .= ']', l2 .= '}'] !> auto_modifiersOptions
      , K.Backslash ⇒ [l1 .= '\\', l2 .= '|'] !> auto_modifiersOptions
      -- 2nd row
      , K.Semicolon ⇒ [l1 .= ';', l2 .= ':'] !> auto_modifiersOptions
      , K.Quote     ⇒ [l1 .= '\'', l2 .= '"'] !> auto_modifiersOptions
      -- 1st row
      , K.Comma     ⇒ [l1 .= ',', l2 .= '<'] !> auto_modifiersOptions
      , K.Period    ⇒ [l1 .= '.', l2 .= '>'] !> auto_modifiersOptions
      , K.Slash     ⇒ [l1 .= '/', l2 .= '?'] !> auto_modifiersOptions
      -- Modifiers row
      , K.Space     ⇒ AutoActions auto_modifiersOptions (singleton ' ')
      ]

    keyLetterToChar :: K.Key -> Maybe (Char, Char)
    keyLetterToChar key = (toLower &&& id) <$> K.keyLetterToChar key

    mkMap2 :: (K.Key -> Maybe (Char, Char)) -> [K.Key] -> AutoActionsKeyMap
    mkMap2 f = Map.fromList . mapMaybe (mkEntry2 f)

    mkEntry2 :: (K.Key -> Maybe (Char, Char)) -> K.Key -> Maybe (K.Key, AutoActions)
    mkEntry2 f key = (key,) . mkActions2 <$> f key

    mkActions2 :: (Char, Char) -> AutoActions
    mkActions2 (c1, c2) = AutoActions auto_modifiersOptions (mkActions mempty mempty [(mempty, AChar c1), (l2, AChar c2)])

    auto_modifiersOptions :: AutoModifiersOptions
    auto_modifiersOptions = AutoWith opt

-- | Linux US layout
linuxUsActionMap :: AutoActionsKeyMap
linuxUsActionMap
  = alter
    (makeLinuxSpecificUsActionMapUpdate DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4)
    osIndependentUsActionMap

-- [TODO] Complete the map
-- | Linux US layout
makeLinuxSpecificUsActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> AutoActionsKeyMapUpdate
makeLinuxSpecificUsActionMapUpdate opt l1 l2 _l3 _l4 =
  mkLinuxSpecificBlankActionMap_update opt l1 l2

-- | Windows US layout
windowsUsActionMap :: AutoActionsKeyMap
windowsUsActionMap
  = alter
    (mkWindowsSpecificUsActionMapUpdate DefaultCapsLockBehaviour isoLevel1 isoLevel2)
    osIndependentUsActionMap

-- [TODO] Complete the map
-- | Windows US layout
mkWindowsSpecificUsActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> AutoActionsKeyMapUpdate
mkWindowsSpecificUsActionMapUpdate =
  mkWindowsSpecificBlankActionMapUpdate


--- Utils ---------------------------------------------------------------------

(⇒) :: K.Key -> AutoActions -> (K.Key, AutoActions)
k ⇒ as = (k, as)
infixr 1 ⇒

(⇨) :: K.Key -> AutoActionsAlteration -> (K.Key, AutoActionsAlteration)
k ⇨ as = (k, as)
infixr 1 ⇨

(.=) :: (IsAction a) => Level -> a -> [(Level, Action)]
l .= a = [(l, toAction a)]

checkDefaultActions :: K.Key -> OsSpecific -> RawActions -> RawActions
checkDefaultActions k os as
  | defaultActions k os == as' = ADefault as'
  | otherwise                   = ACustom as'
  where as' = untagCustomisableActions as

checkMultiOsDefaultActions :: K.Key -> MultiOsRawActions -> MultiOsRawActions
checkMultiOsDefaultActions k = mapMultiOsActionsWithOs (checkDefaultActions k)
