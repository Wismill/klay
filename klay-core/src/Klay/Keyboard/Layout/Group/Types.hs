{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Group.Types
  ( -- * Group
    Group(..)
  , GroupMapping

  , SingleOsGroup
  , GroupLevels
  , SingleOsGroupMapping

  , MultiOsGroup
  , MultiOsGroupLevels
  , MultiOsGroupMapping

  , MultiOsRawGroup
  , KeyMultiOsRawGroup

  , RawGroup
  , KeyRawGroup

  , ResolvedGroup
  , KeyResolvedGroup

  , FinalGroup
  , KeyFinalGroup

  , LayerGroup

  , DualFunctionsGroupLayout

    -- * Group collection
  , Groups(..)
  , GroupIndex(..)
  , GroupedGroups
  , GroupsMappings

  , MultiOsGroupedGroups
  , MultiOsGroupsMappings
  , SingleOsGroupsMappings

  , MultiOsRawGroups

  , RawGroups

  , ResolvedGroups

  , FinalGroups
  ) where

import Data.Map.Strict qualified as Map
import Data.Bifunctor (Bifunctor(..))
import Data.Bifoldable (Bifoldable(..))
import Data.Bitraversable (Bitraversable(..))
import Control.DeepSeq (NFData(..), NFData1(..)) --, rwhnf)
import GHC.Generics (Generic, Generic1)
import GHC.Exts (IsList(..))

import Data.Aeson.Types
  (ToJSON(..), ToJSON1(..), FromJSON(..), FromJSON1(..))
import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as Aeson

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.ButtonAction (ButtonActionsMap)
import Klay.Keyboard.Layout.Action
  ( Action, Actions, ActionsKeyMap, Level
  , MultiOsRawActions, MultiOsRawActionsKeyMap
  , RawActions, RawActionsKeyMap
  , ResolvedActions, ResolvedActionsKeyMap )
import Klay.OS (MultiOsId)
import Klay.Utils (jsonOptions)
import Klay.Utils.UserInput (RawLText)


--- Group ---------------------------------------------------------------------

-- | A group as defined in ISO/IEC&#xA0;9995-1, section&#xA0;5.1.
data Group a b = Group
  { _groupName :: !RawLText
  -- ^ Name of the group
  , _groupLevels :: !a
  -- ^ Names of the supported levels
  , _groupMapping :: !b
  -- ^ Mappings between keys, levels and actions (keymap)
  } deriving (Generic1, Generic, NFData, NFData1, Functor, Foldable, Traversable, Eq, Show)

instance Bifunctor Group where
  bimap f g (Group n ls am) = Group n (f ls) (g am)

instance Bifoldable Group where
  bifoldr f g acc (Group _ ls am) = f ls (g am acc)

instance Bitraversable Group where
  bitraverse f g (Group n ls am) = Group n <$> f ls <*> g am

instance ToJSON KeyMultiOsRawGroup where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 6))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 6))

instance FromJSON KeyMultiOsRawGroup where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 6))

type MultiOsGroupLevels = MultiOsId GroupLevels
type GroupLevels        = Map.Map Level RawLText

type MultiOsGroup  = Group MultiOsGroupLevels
type SingleOsGroup = Group GroupLevels

type GroupMapping      ls k a = Group ls (Map.Map k a)
type MultiOsGroupMapping  k a = GroupMapping MultiOsGroupLevels k a
type SingleOsGroupMapping k a = GroupMapping GroupLevels        k a

type MultiOsRawGroup k   = MultiOsGroupMapping k MultiOsRawActions
type KeyMultiOsRawGroup  = MultiOsRawGroup Key
type RawGroup k          = SingleOsGroupMapping k RawActions
type KeyRawGroup         = RawGroup Key
type ResolvedGroup k     = SingleOsGroupMapping k ResolvedActions
type KeyResolvedGroup    = ResolvedGroup Key
type FinalGroup k        = SingleOsGroupMapping k Actions
type KeyFinalGroup       = FinalGroup Key
type LayerGroup k        = SingleOsGroupMapping k Action

type DualFunctionsGroupLayout = ButtonActionsMap MultiOsRawActions


--- Groups --------------------------------------------------------------------

-- | A collection of layout groups
data Groups g
  = OneGroup !g            -- ^ A collection of 1 group
  | TwoGroups !g !g        -- ^ A collection of 2 groups
  | ThreeGroups !g !g !g   -- ^ A collection of 3 groups
  | FourGroups !g !g !g !g -- ^ A collection of 4 groups
  deriving (Generic1, Generic, NFData1, NFData, Functor, Foldable, Traversable, Eq, Show)

instance IsList (Groups g) where
  type Item (Groups g) = g
  fromList []               = error "fromList: Empty list"
  fromList [g1]             = OneGroup    g1
  fromList [g1, g2]         = TwoGroups   g1 g2
  fromList [g1, g2, g3]     = ThreeGroups g1 g2 g3
  fromList [g1, g2, g3, g4] = FourGroups  g1 g2 g3 g4
  fromList _                = error "fromList: Too many groups"

  toList (OneGroup    g1)          = [g1]
  toList (TwoGroups   g1 g2)       = [g1, g2]
  toList (ThreeGroups g1 g2 g3)    = [g1, g2, g3]
  toList (FourGroups  g1 g2 g3 g4) = [g1, g2, g3, g4]

instance ToJSON1 Groups where
  liftToJSON f _ (OneGroup    g1         ) = f g1
  liftToJSON _ f (TwoGroups   g1 g2      ) = f [g1, g2]
  liftToJSON _ f (ThreeGroups g1 g2 g3   ) = f [g1, g2, g3]
  liftToJSON _ f (FourGroups  g1 g2 g3 g4) = f [g1, g2, g3, g4]

  liftToEncoding f _ (OneGroup    g1         ) = f g1
  liftToEncoding _ f (TwoGroups   g1 g2      ) = f [g1, g2]
  liftToEncoding _ f (ThreeGroups g1 g2 g3   ) = f [g1, g2, g3]
  liftToEncoding _ f (FourGroups  g1 g2 g3 g4) = f [g1, g2, g3, g4]

instance FromJSON1 Groups where
  -- liftParseJSON = Aeson.genericLiftParseJSON (jsonOptions id)
  liftParseJSON f g = \case
    v@(Aeson.Array _) -> g v >>= \case
      []            -> fail "empty groups"
      [g1]          -> pure $ OneGroup    g1
      [g1,g2]       -> pure $ TwoGroups   g1 g2
      [g1,g2,g3]    -> pure $ ThreeGroups g1 g2 g3
      [g1,g2,g3,g4] -> pure $ FourGroups  g1 g2 g3 g4
      _             -> fail "too many groups (max: 4)"
    v@(Aeson.Object _) -> OneGroup <$> f v
    v -> Aeson.unexpected v

instance ToJSON MultiOsRawGroups where
  toJSON     = Aeson.toJSON1
  toEncoding = Aeson.toEncoding1

instance FromJSON MultiOsRawGroups where
  parseJSON = Aeson.parseJSON1

-- | An index of a group in 'Groups'.
data GroupIndex
  = FirstGroup  -- ^ First group
  | SecondGroup -- ^ Second group
  | ThirdGroup  -- ^ Third group
  | FourthGroup -- ^ Fourth group
  deriving (Generic, NFData, Eq, Ord, Enum, Bounded, Show)

type MultiOsGroupedGroups a = Groups (MultiOsGroup a)
type GroupedGroups        a = Groups (SingleOsGroup a)

type GroupsMappings      ls k a = Groups (GroupMapping ls k a)
type MultiOsGroupsMappings  k a = GroupsMappings MultiOsGroupLevels k a
type SingleOsGroupsMappings k a = GroupsMappings GroupLevels        k a

type MultiOsRawGroups = MultiOsGroupedGroups MultiOsRawActionsKeyMap
type RawGroups        = GroupedGroups RawActionsKeyMap
type ResolvedGroups   = GroupedGroups ResolvedActionsKeyMap
type FinalGroups      = GroupedGroups ActionsKeyMap
