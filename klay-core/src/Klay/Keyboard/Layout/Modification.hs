{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}

module Klay.Keyboard.Layout.Modification
  ( -- * Layout modification
    IsLayoutModification(..)
  , Modification(..)
    -- * Metadata modification
  , LayoutMetadataModification(..)
    -- * Dua functions modification
  , DuaFunctionModification
    ( ..
    , DualAddTapN
    , DualAddTapNR
    , DualAddTapH
    , DualAddTapHN
    , DualAddTapHNR
    , DualAddHoldN
    , DualAddHoldNR
    , DualAddHoldH
    , DualAddHoldHN
    , DualAddHoldHNR
    , DualAddTapHold
    )
    -- * Options modification
  , IsOptionModification(..)
  , WindowsOptionsModification(..)
    -- * Dead keys modification
  , IsDeadKeysModification(..)
  , DeadKeysModification(..)
    -- * Groups modification
  , GroupsModification(..)
    -- * Group modification
  , IsGroupModification(..)
  , GroupModification(..)
  , RawGroupsModification
  , MultiOsRawGroupsModification
  , MultiOsRawGroupsModification'
  , MultiOsRawGroupModification
  , MultiOsRawGroupModification'
  , DiscardedLevels
  , LevelRef
  , Combine(..)
  , LevelErrors(..)
  , DiscardedLevel(..)
  , DiscardedReason(..)
  , mapGroupLevelsResolved
    -- * Actions mapping modification
  , IsActionsModification(..)
  , ActionMapModification(..)
  , SimpleActionMapModification
  , MultiOsRawActionMapModification
  , MultiOsRawActionMapModification'
  , VirtualKeysDefinitionModification
    -- * Utils
  , permuteMapKeys
  , permuteMapValues
  , permutationToMap
  , permutationToReverseMap
  , relativePermutationMap
  ) where


import Prelude hiding (lookup)
import Data.Map.Strict qualified as Map
-- import Data.Map.Merge.Strict qualified as Map
import Data.Containers.ListUtils (nubOrd)
-- import Data.Function ((&))
import Data.Functor (($>))
-- import Data.Functor.Classes (Eq1, Show1)
import Data.Bifunctor (Bifunctor(..))
import Data.Foldable (foldl')
import Data.Maybe (fromMaybe)
import Control.Arrow ((&&&))
import Control.Monad.State.Strict
import GHC.Generics (Generic(..))

-- import Data.Align (Semialign(..))
-- import Data.These (These(..))

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.ButtonAction
  (ButtonAction(..), Delay, Next(..), Release(..))
import Klay.Keyboard.Layout
  ( Layout(..), MultiOsRawLayout, MultiOsRawLayout, LayoutMetadata(..), ButtonActionsDefinition
  , LayoutOptions, WindowsOptions(..) )
import Klay.Keyboard.Layout.Action
  ( Action(..), Actions, ActionsAlteration, Level
  , mapActionsLevels, mbimapActions
  , RawActions, RawActionsAlteration
  , MultiOsRawActions, MultiOsRawActionsAlteration, MultiOsRawActionsAlteration'
  , mapMultiOsActions, checkMultiOsDefaultActions )
import Klay.Keyboard.Layout.Action.DeadKey
  ( DeadKey, DeadKeyMapping, RawDeadKeyDefinitions, DeadKeyDefinitions(..)
  , updateRawDeadKeyDefinitions, mkDeadKeyDefinitions)
import Klay.Keyboard.Layout.Group.Types
  (Group(..), SingleOsGroup, MultiOsGroup, GroupIndex(..))
import Klay.Keyboard.Layout.Group
  ( ResolvedGroup, MultiOsGroupMapping, GroupLevels
  , laxReplaceGroup, onGroup, addGroup, removeGroup)
import Klay.Keyboard.Layout.Modifier (Modifier, ModifiersCombo)
import Klay.Keyboard.Layout.VirtualKey (VirtualKey)
import Klay.OS (OsSpecific(..), MultiOs(..), MultiOsId, mapWithOs, onOs, onOss, toMultiOs)
import Klay.Utils (foldlMWithKey')
import Data.Alterable (Alterable(..))
import Klay.Utils.UserInput


-- | Class representing a modification of a layout.
class IsLayoutModification m l where
  lmodify :: m -> l -> l

instance {-# OVERLAPPABLE #-} (IsLayoutModification m l) => IsLayoutModification [m] l where
  lmodify ms l = foldl' (flip lmodify) l ms

-- | Non-specialised modification.
data Modification l where
  Mod :: (IsLayoutModification m l) => !m -> Modification l

instance IsLayoutModification (Modification l) l where
  lmodify (Mod m) l = lmodify m l

-- | Modification of metadata of a layout
data LayoutMetadataModification
  = ReplaceLayoutName !RawLText
  | ReplaceMetadata !LayoutMetadata
  deriving (Eq, Show)

instance IsLayoutModification LayoutMetadataModification (Layout gs) where
  lmodify (ReplaceMetadata md) l = l{_metadata=md}
  lmodify (ReplaceLayoutName t) l@Layout{_metadata=md} = l{_metadata=md{_name=t}}

-- | Modification of the dual functions
data DuaFunctionModification
  = DualAddTap !Key !Key !(Maybe Delay) !Next !Release -- ^ Add or replace a tap action.
  | DualAddHold !Key !Key !(Maybe Delay) !Next !Release -- ^ Add or replace a hold key action.
  | DualAdd !Key !Key !Key !(Maybe Delay) !Next !Release -- ^ Add or replace tap/hold key actions.
  | DualRemove !Key
  | DualUpdate !ButtonActionsDefinition
  deriving (Eq, Show)

pattern DualAddTapN :: Key -> Key -> DuaFunctionModification
pattern DualAddTapN k t = DualAddTap k t Nothing Next NoRelease

pattern DualAddTapNR :: Key -> Key -> DuaFunctionModification
pattern DualAddTapNR k t = DualAddTap k t Nothing Next Release

pattern DualAddTapH :: Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddTapH k t d = DualAddTap k t (Just d) NoNext NoRelease

pattern DualAddTapHN :: Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddTapHN k t d = DualAddTap k t (Just d) Next NoRelease

pattern DualAddTapHNR :: Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddTapHNR k t d = DualAddTap k t (Just d) Next Release

pattern DualAddHoldN :: Key -> Key -> DuaFunctionModification
pattern DualAddHoldN k h = DualAddHold k h Nothing Next NoRelease

pattern DualAddHoldNR :: Key -> Key -> DuaFunctionModification
pattern DualAddHoldNR k h = DualAddHold k h Nothing Next Release

pattern DualAddHoldH :: Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddHoldH k h d = DualAddHold k h (Just d) NoNext NoRelease

pattern DualAddHoldHN :: Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddHoldHN k h d = DualAddHold k h (Just d) Next NoRelease

pattern DualAddHoldHNR :: Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddHoldHNR k h d = DualAddHold k h (Just d) Next Release

pattern DualAddTapHold :: Key -> Key -> Key -> Delay -> DuaFunctionModification
pattern DualAddTapHold k t h d = DualAdd k t h (Just d) NoNext NoRelease

instance IsLayoutModification DuaFunctionModification (Layout gs) where
  lmodify (DualAddTap k k' delay n r) l@Layout{_dualFunctionKeys=ds} =
    l{ _dualFunctionKeys=Map.alter (\case
           Nothing -> Just (DualKey k' k delay n r)
           Just (NormalKey k'') -> Just (DualKey k' k'' delay n r)
           Just (DualKey _ k'' _ _ _) -> Just (DualKey k' k'' delay n r)
         ) k ds
    }
  lmodify (DualAddHold k k' delay n r) l@Layout{_dualFunctionKeys=ds} =
    l{ _dualFunctionKeys=Map.alter (\case
           Nothing -> Just (DualKey k k' delay n r)
           Just (NormalKey k'') -> Just (DualKey k'' k' delay n r)
           Just (DualKey k'' _ _ _ _) -> Just (DualKey k'' k' delay n r)
         ) k ds
    }
  lmodify (DualAdd k t h d n r) l@Layout{_dualFunctionKeys=ds} =
    l{ _dualFunctionKeys=Map.insert k (DualKey t h d n r) ds}
  lmodify (DualRemove k) l@Layout{_dualFunctionKeys=ds} =
    l{_dualFunctionKeys=Map.delete k ds}
  lmodify (DualUpdate ds') l@Layout{_dualFunctionKeys=ds} =
    l{_dualFunctionKeys=ds' <> ds}


class IsOptionModification m where
  omodify :: m -> LayoutOptions -> LayoutOptions

--  Modification of the Windows layout options
data WindowsOptionsModification
  = WinAltGr !Bool
  | WinIsoLevel3IsAltGr !Bool
  | VirtualKeysDefinition !VirtualKeysDefinitionModification
  deriving (Eq, Show)

instance IsOptionModification WindowsOptionsModification where
  omodify (WinAltGr b) = modifyWinOptions \opts -> opts{_wAltgr=b}
  omodify (WinIsoLevel3IsAltGr b) = modifyWinOptions \opts -> opts{_wIsoLevel3IsAltGr=b}
  omodify (VirtualKeysDefinition vkd) = modifyWinOptions \opts -> opts{_wVirtualKeyDefinition=amodify vkd . _wVirtualKeyDefinition $ opts}

modifyWinOptions :: (WindowsOptions -> WindowsOptions) -> LayoutOptions -> LayoutOptions
modifyWinOptions f opts = opts{_windows = f $ _windows opts}

instance IsLayoutModification WindowsOptionsModification (Layout gs) where
  lmodify m l = l{_options = omodify m $ _options l}


--- Dead keys -----------------------------------------------------------------

-- | Modification of dead keys
data DeadKeysModification
  = DKReplace !(DeadKeyMapping DeadKey)
  -- ^ Replace dead keys with a substitution map
  | DKDefExtend !RawDeadKeyDefinitions
  -- ^ Extend the dead keys definitions
  | DKDefReplace !RawDeadKeyDefinitions
  -- ^ Reset the dead keys definitions
  deriving (Eq, Show)

class IsDeadKeysModification m dkd where
  dk_modify :: m -> dkd -> dkd

instance IsDeadKeysModification DeadKeysModification RawDeadKeyDefinitions where
  dk_modify (DKReplace dks) = updateRawDeadKeyDefinitions dks
  dk_modify (DKDefExtend dkd) = (<> dkd)
  dk_modify (DKDefReplace dkd) = const dkd

instance IsDeadKeysModification DeadKeysModification DeadKeyDefinitions where
  dk_modify m = mkDeadKeyDefinitions . dk_modify m . _dkRawDefinitions

instance IsLayoutModification DeadKeysModification (Layout gs) where
  lmodify m l = l{_deadKeys=dk_modify m $ _deadKeys l}


--- Groups --------------------------------------------------------------------

type RawGroupsModification         = GroupsModification SingleOsGroup Key RawActions        RawActionsAlteration
type TMultiOsRawGroupsModification = GroupsModification MultiOsGroup  Key MultiOsRawActions
type MultiOsRawGroupsModification  = GroupsModification MultiOsGroup  Key MultiOsRawActions MultiOsRawActionsAlteration
type MultiOsRawGroupsModification' = GroupsModification MultiOsGroup  Key MultiOsRawActions MultiOsRawActionsAlteration'

-- | Modification of the groups of a layout
data GroupsModification g vk a d
  = GroupMod !GroupIndex !(GroupModification vk a d)
  | GroupMods !GroupIndex ![GroupModification vk a d]
  | GroupsMod !(GroupModification vk a d)
  | GroupsMods ![GroupModification vk a d]
  | AddGroup !(g (Map.Map vk a))
  -- ^ Append a new group
  -- AddGroupLayout !(KeyMapping g vk a)
  -- -- ^ Append a new group layout
  | RemoveGroup !GroupIndex
  -- ^ Remove a group at a given index
  | ReplaceGroup !GroupIndex !(g (Map.Map vk a))
  -- ^ Replace a group with a new one
  -- ReplaceGroupLayout !GroupIndex !(KeyMapping g vk a)
  -- -- ^ Replace a group with a new group layout

deriving instance (Eq a, Eq vk, Eq d, Eq (g (Map.Map vk a))) => Eq (GroupsModification g vk a d)
deriving instance (Show a, Show vk, Show d, Show (g (Map.Map vk a))) => Show (GroupsModification g vk a d)

-- instance IsLayoutModification RawGroupsModification MultiOsRawLayout where
--   lmodify (GroupMod k m) l = l{_groups=map_key_mapping_group k (gmodify m) $ _groups l}
--   lmodify (GroupMods k ms) l = lmodify (GroupMod k <$> ms :: [RawGroupsModification]) l
--   lmodify (AddGroup g) l = l{_groups=map_key_mapping_groups (\gs -> fromMaybe gs $ addGroup g gs) (_groups l)}
--   lmodify (AddGroupLayout gl) l =
--     let g = adapt_group_layout (on_key_map id . _groups $ l) gl
--     in lmodify (AddGroup g) l
--   lmodify (RemoveGroup k) l = l{_groups=map_key_mapping_groups (\gs -> fromMaybe gs $ removeGroup k gs) (_groups l)}
--   lmodify (ReplaceGroup k g) l = l{_groups=map_key_mapping_groups (\gs -> laxReplaceGroup k g gs) (_groups l)}
--   lmodify (ReplaceGroupLayout k gl) l =
--     let g = adapt_group_layout (on_key_map id . _groups $ l) gl
--     in lmodify (ReplaceGroup k g) l

instance (IsRawGroupModification Key d) => IsLayoutModification (TMultiOsRawGroupsModification d) MultiOsRawLayout where
  lmodify (GroupMod k m) l = l{_groups=onGroup k (gmodify m) $ _groups l}
  lmodify (GroupMods k ms) l = lmodify (GroupMod k <$> ms :: [TMultiOsRawGroupsModification d]) l
  lmodify (GroupsMod m) l = l{_groups=gmodify m <$> _groups l}
  lmodify (GroupsMods ms) l = lmodify (GroupsMod <$> ms :: [TMultiOsRawGroupsModification d]) l
  lmodify (AddGroup g) l = l{_groups=let gs = _groups l in fromMaybe gs $ addGroup g gs}
  -- lmodify (AddGroupLayout gl) l =
  --   let g = adapt_group_layout (on_key_map id . _groups $ l) gl
  --   in lmodify (AddGroup g :: TMultiOsRawGroupsModification d) l
  lmodify (RemoveGroup k) l = l{_groups=let gs = _groups l in fromMaybe gs $ removeGroup k gs}
  lmodify (ReplaceGroup k g) l = l{_groups=let gs = _groups l in laxReplaceGroup k g gs}
  -- lmodify (ReplaceGroupLayout k gl) l =
  --   let g = adapt_group_layout (on_key_map id . _groups $ l) gl
  --   in lmodify (ReplaceGroup k g :: TMultiOsRawGroupsModification d) l

-- adapt_group_layout
--   :: (IsGroupModification (g (Map.Map Key MultiOsRawActions)) MultiOsRawGroupModification)
--   => VirtualKeysDefinition
--   -> KeyMapping g Key MultiOsRawActions
--   -> g MultiOsRawActionsKeyMap
-- adapt_group_layout ref_vkd gl = on_vk_maps (gmodify vk_mod) gl
--   where vkd = on_key_map id gl
--         vk_mod :: MultiOsRawGroupModification
--         vk_mod = KeyMapMod . KeyRemap $ relativePermutationMap ref_vkd vkd

-- instance (IsKeyModification (KeyMapModification a Key) Key) => IsLayoutModification (KeyMapModification a Key) MultiOsRawLayout where
--   lmodify m l = l{_groups=kmModify (KeyMapMod m :: RawKeyMappingModification) $ _groups l}


--- Group modifications -------------------------------------------------------

class IsGroupModification g m where
  gmodify :: m -> g -> g

instance {-# OVERLAPPABLE #-} (IsGroupModification g m) => IsGroupModification g [m] where
  gmodify ms l = foldl' (flip gmodify) l ms

type IsRawGroupModification  k d    = (Ord k, IsGroupModification (MultiOsGroupMapping k MultiOsRawActions) (GroupModification k MultiOsRawActions d))
type TMultiOsRawGroupModification k = GroupModification k MultiOsRawActions
type MultiOsRawGroupModification    = TMultiOsRawGroupModification Key MultiOsRawActionsAlteration
type MultiOsRawGroupModification'   = TMultiOsRawGroupModification Key MultiOsRawActionsAlteration'

-- | Modification of a single group
data GroupModification k a d
  = ReplaceGroupName !RawLText
  | AddLevels !GroupLevels -- [TODO] use Combine?
  | AddOsLevels !OsSpecific !GroupLevels -- [TODO] use Combine?
  | UpdateLevels !(Map.Map Level (Maybe Level)) !Combine
  | KeyMapMod !(ActionMapModification k a d)
  | KeyMapMods ![ActionMapModification k a d]
  deriving (Eq, Show)

-- instance (Ord vk) => IsGroupModification (SingleOsGroupMapping vk RawActions) (TMultiOsRawGroupModification vk) where
--   gmodify (ReplaceGroupName name) g = g{_groupName = name}
--   gmodify (AddLevels _ ls) g = g{_groupLevels=_groupLevels g <> ls}
--   gmodify (UpdateLevels f c) g = g{_groupLevels=ls, _groupMapping=gm}
--     where
--       (ls, mods) = fst . flip runState Nothing . modifyGroupLevelsDefinition f c $ _groupLevels g
--       gm = modifyActionMapLevels mods $ _groupMapping g
--       modifyActionMapLevels
--         :: LevelsUpdate
--         -> Map.Map vk RawActions
--         -> Map.Map vk RawActions
--       modifyActionMapLevels mods = Map.map (bimap (second go) go)
--         where go = mapActionsLevels $ flip Map.lookup mods
--   gmodify (KeyMapMod (SingleOsVkMapMod _ _)) g = g
--   gmodify (KeyMapMod (MultiOsVkMapMod m)) g =
--     g{_groupMapping = amodify m $ _groupMapping g}
--   gmodify (KeyMapMod (MultiOsVkMapUpdate am)) g =
--     let MultiOs am' _ = am
--     in g{_groupMapping = amodify (KeyMapUpdate am' :: ActionMapModification vk RawActions RawActionsAlteration) $ _groupMapping g}
--   gmodify (KeyMapMods ms) g = foldl' (\acc m -> gmodify (KeyMapMod m) acc) g ms

type LevelsUpdate = Map.Map Level Level
type MultiOsLevelsUpdate = MultiOsId LevelsUpdate

instance (IsMultiOsRawActionsModification k d) => IsGroupModification (MultiOsGroupMapping k MultiOsRawActions) (TMultiOsRawGroupModification k d) where
  gmodify (ReplaceGroupName name) g = g{_groupName = name}
  gmodify (AddLevels ls) g = g{_groupLevels=mapWithOs (const (ls <>)) $ _groupLevels g}
  gmodify (AddOsLevels os ls) g = g{_groupLevels=onOs os (<> ls) $ _groupLevels g}
  gmodify (UpdateLevels ml c) g = g{_groupLevels=ls, _groupMapping=gm}
    where
      go :: GroupLevels -> (GroupLevels, LevelsUpdate)
      go = fst . flip runState Nothing . modifyGroupLevelsDefinition f c
      f l = case Map.lookup l ml of
        Nothing -> Right l
        Just m  -> maybe (Left FilteredLevel) Right m
      mods :: MultiOsLevelsUpdate
      (ls, mods) = (onOss (toMultiOs fst) &&& onOss (toMultiOs snd)) . onOss (toMultiOs go)
                 $ _groupLevels g
      gm :: Map.Map k MultiOsRawActions
      gm = modifyActionMapLevels mods $ _groupMapping g
  gmodify (KeyMapMod m) g =
    g{_groupMapping = amodify m $ _groupMapping g}
  gmodify (KeyMapMods ms) g = foldl' (\acc m -> gmodify (KeyMapMod m) acc) g ms

-- | Modification of levels definition
modifyGroupLevelsDefinition
  :: (Level -> Either DiscardedLevel Level)
  -> Combine
  -> GroupLevels
  -> State (Maybe LevelErrors) (GroupLevels, LevelsUpdate)
modifyGroupLevelsDefinition f c = foldlMWithKey' go mempty
  where
    go :: (GroupLevels, LevelsUpdate) -> Level -> RawLText -> State (Maybe LevelErrors) (GroupLevels, LevelsUpdate)
    go (ls, m) l n = case f l of
      Left FilteredLevel -> report_discarded_level (l, n) Filtered $> (ls, m)
      Left (InvalidModifiers ms) -> report_discarded_level (l, n) (Invalid ms) $> (ls, m)
      Right l' -> case Map.lookup l' ls of
        Nothing -> pure (Map.insert l' n ls, Map.insert l l' m)
        Just n' -> if n /= n'
          then case c of
            KeepPrevious -> report_discarded_level (l', n) (ReplacedBy (l', n')) $> (ls, m)
            KeepNew -> report_discarded_level (l', n') (ReplacedBy (l', n)) $> (Map.insert l' n ls, Map.insert l l' m)
          else pure (ls, Map.insert l l' m)
    report_discarded_level l r = modify (<> Just mempty{_discardedLevels=Map.singleton l r})

modifyActionMapLevels
  :: MultiOsLevelsUpdate
  -> Map.Map vk MultiOsRawActions
  -> Map.Map vk MultiOsRawActions
modifyActionMapLevels m = Map.map (mapMultiOsActions fixLevels)
  where
    fixLevels :: MultiOsId (RawActions -> RawActions)
    fixLevels = onOss (toMultiOs go) m
    go :: LevelsUpdate -> RawActions -> RawActions
    go dl = let f = mapActionsLevels $ flip Map.lookup dl
            in bimap (second f) (second f)

-- | Levels and modifiers modification
mapGroupLevelsResolved
  :: (Level -> Either DiscardedLevel Level)
  -> (Modifier -> Maybe Modifier)
  -> Combine
  -> ResolvedGroup vk
  -> (ResolvedGroup vk, Maybe LevelErrors)
mapGroupLevelsResolved processLevel processModifier c g =
  (g{_groupLevels=ls1, _groupMapping=gm'}, errors)
  where
    ls0 = _groupLevels g
    ((ls1, _mods), errors) = runState (modifyGroupLevelsDefinition processLevel c ls0) Nothing
    processLevel' = either (const Nothing) Just . processLevel
    process_action _ (AModifier m) = AModifier <$> processModifier m
    process_action _ a             = Just a
    go = mbimapActions processLevel' process_action
    gm' = bimap go go <$> _groupMapping g

type DiscardedLevels = Map.Map LevelRef (DiscardedReason LevelRef ModifiersCombo)

type LevelRef = (Level, RawLText)

-- | Errors reported by 'modifyActionMapLevels'.
newtype LevelErrors = LevelErrors
  { _discardedLevels :: DiscardedLevels
  } deriving (Eq, Show)

instance Semigroup LevelErrors where
  LevelErrors a1 <> LevelErrors a2 = LevelErrors (a1 <> a2)

instance Monoid LevelErrors where
  mempty = LevelErrors mempty

-- | Describe the way to combine two entries at the same level.
data Combine
  = KeepPrevious
  | KeepNew
  deriving (Eq, Show)

data DiscardedLevel
  = FilteredLevel
  | InvalidModifiers !ModifiersCombo
  deriving (Eq, Show)

-- | Describe the reason of the discarding of an element.
data DiscardedReason a b
  = Filtered
  | ReplacedBy a
  | Invalid b
  deriving (Generic, Functor, Eq, Show)

instance Bifunctor DiscardedReason where
  bimap _ _ Filtered       = Filtered
  bimap f _ (ReplacedBy a) = ReplacedBy (f a)
  bimap _ g (Invalid b)    = Invalid (g b)


--- Actions mapping modification ------------------------------------------

class IsActionsModification m k a where
  amodify :: m -> Map.Map k a -> Map.Map k a

instance {-# OVERLAPPABLE #-} (IsActionsModification m k a) => IsActionsModification [m] k a where
  amodify ms am = foldl' (flip amodify) am ms

type IsMultiOsRawActionsModification vk d = (Ord vk, IsActionsModification (ActionMapModification vk MultiOsRawActions d) vk MultiOsRawActions)
type SimpleActionMapModification       = ActionMapModification Key Actions           ActionsAlteration
type MultiOsRawActionMapModification   = ActionMapModification Key MultiOsRawActions MultiOsRawActionsAlteration
type MultiOsRawActionMapModification'  = ActionMapModification Key MultiOsRawActions MultiOsRawActionsAlteration'
type VirtualKeysDefinitionModification = ActionMapModification Key VirtualKey VirtualKey

{-| Modification of the actions.

Note: an incorrect permutation has no effect.
-}
data ActionMapModification k a d
  = KeySwap !k !k
  -- ^ Swap two keys
  | KeyPermutation ![k]
  -- ^ Permute a list of keys
  | KeyPermutations ![[k]]
  -- ^ Process a list of permutations
  | KeyRemap !(Map.Map k k)
  -- ^ Relative remapping
  | KeyReplace !k !a
  -- ^ Replace a single key
  | KeyClone !k !k
  -- ^ Clone the mapping of the first key into the second key
  | KeyRemove !k
  -- ^ Remove the key from the mapping
  | KeyMapUpdate !(Map.Map k d)
  -- ^ Update the map with a new map
  deriving (Eq, Show)

instance {-# OVERLAPPABLE #-} (Ord k, Alterable (Map.Map k a) (Map.Map k d)) => IsActionsModification (ActionMapModification k a d) k a where
  amodify = defaultAmodify

instance IsActionsModification MultiOsRawActionMapModification Key MultiOsRawActions where
  amodify m = Map.mapWithKey checkMultiOsDefaultActions . defaultAmodify m

defaultAmodify :: (Ord k, Alterable (Map.Map k a) (Map.Map k d)) => ActionMapModification k a d -> Map.Map k a -> Map.Map k a
defaultAmodify (KeySwap k1 k2)     km = fromMaybe km $ permuteMapKeys [k1, k2] km
defaultAmodify (KeyPermutation ks) km = fromMaybe km $ permuteMapKeys ks       km
defaultAmodify (KeyPermutations kss) km =
  fromMaybe km $ foldl' (\mkm ks -> mkm >>= permuteMapKeys ks) (Just km) kss
defaultAmodify (KeyRemap m) km =
  let m' = m <> Map.mapWithKey const km -- Ensure we map all the keys
  in Map.compose km m'
defaultAmodify (KeyReplace k as) km = Map.insert k as km
defaultAmodify (KeyClone k1 k2) km = Map.alter (const $ Map.lookup k1 km) k2 km
defaultAmodify (KeyRemove k) km = Map.delete k km
defaultAmodify (KeyMapUpdate km') km = alter km' km

instance IsLayoutModification MultiOsRawActionMapModification MultiOsRawLayout where
  lmodify m = lmodify (GroupsMod (KeyMapMod m) :: MultiOsRawGroupsModification)


--- Utils ---------------------------------------------------------------------

-- | Permute 'Map.Map' /keys/ using canonical representation of the permutation, if valid.
permuteMapKeys :: (Ord k) => [k] -> Map.Map k v -> Maybe (Map.Map k v)
permuteMapKeys []  m = Just m
permuteMapKeys [_] m = Just m
permuteMapKeys ks  m =
  let ks' = Map.keys m
      km' = Map.fromList $ zip ks' ks'
      mkm = permutationToMap ks
  in Map.compose m . (<> km') <$> mkm

-- | Permute 'Map.Map' /values/ using canonical representation of the permutation, if valid.
permuteMapValues :: (Ord v) => [v] -> Map.Map k v -> Maybe (Map.Map k v)
permuteMapValues []  m = Just m
permuteMapValues [_] m = Just m
permuteMapValues vs  m =
  let vs' = Map.elems m
      vm' = Map.fromList $ zip vs' vs'
      mvm = permutationToMap vs
  in flip Map.compose m . (<> vm') <$> mvm

permutationToMap :: (Ord k) => [k] -> Maybe (Map.Map k k)
permutationToMap [] = Just mempty
permutationToMap ks0@(k:ks)
  | ks0 == nubOrd ks0 = Just $ Map.fromList $ zip ks0 (ks <> [k])
  | otherwise         = Nothing

permutationToReverseMap :: (Ord k) => [k] -> Maybe (Map.Map k k)
permutationToReverseMap [] = Just mempty
permutationToReverseMap ks0@(k:ks)
  | ks0 == nubOrd ks0 = Just $ Map.fromList $ zip (ks <> [k]) ks0
  | otherwise         = Nothing

relativePermutationMap
  :: (Ord k, Ord v)
  => Map.Map k v -- ^ Reference mapping
  -> Map.Map k v -- ^ Permutation to adapt
  -> Map.Map v v -- ^ Relative permutation
relativePermutationMap m1 = Map.foldrWithKey go mempty
  where go k v acc = maybe acc (\v' -> Map.insert v' v acc) $ Map.lookup k m1
