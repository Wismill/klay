module Klay.Keyboard.Layout.VirtualKey.Types
  ( VirtualKey
    ( ..
    , VK_HANGEUL
    , VK_HANGUL
    , VK_KANJI
    , VK_OEM_NEC_EQUAL
    , VK_DBE_ALPHANUMERIC
    , VK_DBE_KATAKANA
    , VK_DBE_HIRAGANA
    , VK_DBE_SBCSCHAR
    , VK_DBE_DBCSCHAR
    , VK_DBE_ROMAN
    , VK_DBE_NOROMAN
    , VK_DBE_ENTERWORDREGISTERMODE
    , VK_DBE_ENTERIMECONFIGMODE
    , VK_DBE_FLUSHSTRING
    , VK_DBE_CODEINPUT
    , VK_DBE_NOCODEINPUT
    , VK_DBE_DETERMINESTRING
    , VK_DBE_ENTERDLGCONVERSIONMODE )
  , VirtualKeysDefinition
  ) where

-- [TODO] Re-enable Generic deriving when the following issues are solved:
-- • https://gitlab.haskell.org/ghc/ghc/-/issues/5642
-- • https://gitlab.haskell.org/ghc/ghc/-/issues/16577

import Data.Ix (Ix(..))
import Data.Map qualified as Map
import Data.Text qualified as T
import Control.DeepSeq (NFData(..))
-- import GHC.Generics (Hashable(..))

import TextShow (TextShow(..), FromStringShow(..))
-- import TextShow.Generic (FromGeneric(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..)) --, genericShrink)
import Test.QuickCheck.Gen (chooseEnum)
import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as Aeson
import Data.Aeson.Types (ToJSON(..), ToJSONKey(..), FromJSON(..), FromJSONKey(..))

import Data.Alterable (Magma(..))
import Klay.Keyboard.Hardware.Key (Key(..))
-- import Klay.Utils (jsonOptions, jsonKeyOptions)

{- A /Windows/ virtual key is an abstract mapping to physical keys.

Virtual keys may be connected to hardware that provides either those keys and buttons
or an interface that simulates their physical behaviour (such as an onscreen touch keyboard).

Sources:
- @winuser.h@
- [Virtual-Key Codes](https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes)
- [KbdEdit](http://www.kbdedit.com/manual/low_level_vk_list.html)
-}
data VirtualKey
  = VK_LBUTTON             -- ^ @0x01@ Left mouse button
  | VK_RBUTTON             -- ^ @0x02@ Right mouse button
  | VK_CANCEL              -- ^ @0x03@ Encode @Break@ obtained from @Ctrl + Pause@
  | VK_MBUTTON             -- ^ @0x04@ Middle mouse button (three-button mouse)
  | VK_XBUTTON1            -- ^ @0x05@ X1 mouse button
  | VK_XBUTTON2            -- ^ @0x06@ X2 mouse button

  --                             0x07  Undefined
  | VK_BACK                -- ^ @0x08@ BACKSPACE key
  | VK_TAB                 -- ^ @0x09@ TAB key

  --                             0x0A-0x0B  Undefined
  | VK_CLEAR               -- ^ @0x0C@ obtained from numpad 5 with NumLock off.
  | VK_RETURN              -- ^ @0x0D@ ENTER key

  --                             0x0E-0x0F  Undefined
  | VK_SHIFT               -- ^ @0x10@
  | VK_CONTROL             -- ^ @0x11@
  | VK_MENU                -- ^ @0x12@
  | VK_PAUSE               -- ^ @0x13@
  | VK_CAPITAL             -- ^ @0x14@ CAPS LOCK key
  | VK_KANA                -- ^ @0x15@ IME: Kana mode
  | VK_IME_ON              -- ^ @0x16@ IME: On
  | VK_JUNJA               -- ^ @0x17@ IME: Junja mode
  | VK_FINAL               -- ^ @0x18@ IME: final mode
  | VK_HANJA               -- ^ @0x19@ IME: Hanja mode
  | VK_IME_OFF             -- ^ @0x1A@ IME: Off
  | VK_ESCAPE              -- ^ @0x1B@
  | VK_CONVERT             -- ^ @0x1C@ IME: convert
  | VK_NONCONVERT          -- ^ @0x1D@ IME: nonconvert
  | VK_ACCEPT              -- ^ @0x1E@ IME: accept
  | VK_MODECHANGE          -- ^ @0x1F@ IME: mode change request
  | VK_SPACE               -- ^ @0x20@ SPACEBAR
  | VK_PRIOR               -- ^ @0x21@ PAGE UP key
  | VK_NEXT                -- ^ @0x22@ PAGE DOWN key
  | VK_END                 -- ^ @0x23@ END key
  | VK_HOME                -- ^ @0x24@ Home key
  | VK_LEFT                -- ^ @0x25@ LEFT ARROW key
  | VK_UP                  -- ^ @0x26@ UP ARROW key
  | VK_RIGHT               -- ^ @0x27@ RIGHT ARROW key
  | VK_DOWN                -- ^ @0x28@ DOWN ARROW key
  | VK_SELECT              -- ^ @0x29@
  | VK_PRINT               -- ^ @0x2A@ OEM specific in Windows 3.1 SDK
  | VK_EXECUTE             -- ^ @0x2B@
  | VK_SNAPSHOT            -- ^ @0x2C@ PRINT SCREEN key
  | VK_INSERT              -- ^ @0x2D@
  | VK_DELETE              -- ^ @0x2E@
  | VK_HELP                -- ^ @0x2F@
  | VK_0                   -- ^ @0x30@ Number row 0
  | VK_1                   -- ^ @0x31@ Number row 1
  | VK_2                   -- ^ @0x32@ Number row 2
  | VK_3                   -- ^ @0x33@ Number row 3
  | VK_4                   -- ^ @0x34@ Number row 4
  | VK_5                   -- ^ @0x35@ Number row 5
  | VK_6                   -- ^ @0x36@ Number row 6
  | VK_7                   -- ^ @0x37@ Number row 7
  | VK_8                   -- ^ @0x38@ Number row 8
  | VK_9                   -- ^ @0x39@ Number row 9

  --                            0x3A-0x40  Undefined
  | VK_A                   -- ^ @0x41@ Letter A
  | VK_B                   -- ^ @0x42@ Letter B
  | VK_C                   -- ^ @0x43@ Letter C
  | VK_D                   -- ^ @0x44@ Letter D
  | VK_E                   -- ^ @0x45@ Letter E
  | VK_F                   -- ^ @0x46@ Letter F
  | VK_G                   -- ^ @0x47@ Letter G
  | VK_H                   -- ^ @0x48@ Letter H
  | VK_I                   -- ^ @0x49@ Letter I
  | VK_J                   -- ^ @0x4A@ Letter J
  | VK_K                   -- ^ @0x4B@ Letter K
  | VK_L                   -- ^ @0x4C@ Letter L
  | VK_M                   -- ^ @0x4D@ Letter M
  | VK_N                   -- ^ @0x4E@ Letter N
  | VK_O                   -- ^ @0x4F@ Letter O
  | VK_P                   -- ^ @0x50@ Letter P
  | VK_Q                   -- ^ @0x51@ Letter Q
  | VK_R                   -- ^ @0x52@ Letter R
  | VK_S                   -- ^ @0x53@ Letter S
  | VK_T                   -- ^ @0x54@ Letter T
  | VK_U                   -- ^ @0x55@ Letter U
  | VK_V                   -- ^ @0x56@ Letter V
  | VK_W                   -- ^ @0x57@ Letter W
  | VK_X                   -- ^ @0x58@ Letter X
  | VK_Y                   -- ^ @0x59@ Letter Y
  | VK_Z                   -- ^ @0x5A@ Letter Z
  | VK_LWIN                -- ^ @0x5B@ Left Windows key
  | VK_RWIN                -- ^ @0x5C@ Right Windows key
  | VK_APPS                -- ^ @0x5D@ Applications key

  --                             0x5E Unassigned
  | VK_SLEEP               -- ^ @0x5F@
  | VK_NUMPAD0             -- ^ @0x60@ Numpad 0
  | VK_NUMPAD1             -- ^ @0x61@ Numpad 1
  | VK_NUMPAD2             -- ^ @0x62@ Numpad 2
  | VK_NUMPAD3             -- ^ @0x63@ Numpad 3
  | VK_NUMPAD4             -- ^ @0x64@ Numpad 4
  | VK_NUMPAD5             -- ^ @0x65@ Numpad 5
  | VK_NUMPAD6             -- ^ @0x66@ Numpad 6
  | VK_NUMPAD7             -- ^ @0x67@ Numpad 7
  | VK_NUMPAD8             -- ^ @0x68@ Numpad 8
  | VK_NUMPAD9             -- ^ @0x69@ Numpad 9
  | VK_MULTIPLY            -- ^ @0x6A@ Numpad multiply @*@
  | VK_ADD                 -- ^ @0x6B@ Numpad add @+@
  | VK_SEPARATOR           -- ^ @0x6C@ Numpad ???
  | VK_SUBTRACT            -- ^ @0x6D@ Numpad subtract @-@
  | VK_DECIMAL             -- ^ @0x6E@ Numpad decimal separator
  | VK_DIVIDE              -- ^ @0x6F@ Numpad divide @\/@
  | VK_F1                  -- ^ @0x70@ Function key F1
  | VK_F2                  -- ^ @0x71@ Function key F2
  | VK_F3                  -- ^ @0x72@ Function key F3
  | VK_F4                  -- ^ @0x73@ Function key F4
  | VK_F5                  -- ^ @0x74@ Function key F5
  | VK_F6                  -- ^ @0x75@ Function key F6
  | VK_F7                  -- ^ @0x76@ Function key F7
  | VK_F8                  -- ^ @0x77@ Function key F8
  | VK_F9                  -- ^ @0x78@ Function key F9
  | VK_F10                 -- ^ @0x79@ Function key F10
  | VK_F11                 -- ^ @0x7A@ Function key F11
  | VK_F12                 -- ^ @0x7B@ Function key F12
  | VK_F13                 -- ^ @0x7C@ Function key F13
  | VK_F14                 -- ^ @0x7D@ Function key F14
  | VK_F15                 -- ^ @0x7E@ Function key F15
  | VK_F16                 -- ^ @0x7F@ Function key F16
  | VK_F17                 -- ^ @0x80@ Function key F17
  | VK_F18                 -- ^ @0x81@ Function key F18
  | VK_F19                 -- ^ @0x82@ Function key F19
  | VK_F20                 -- ^ @0x83@ Function key F20
  | VK_F21                 -- ^ @0x84@ Function key F21
  | VK_F22                 -- ^ @0x85@ Function key F22
  | VK_F23                 -- ^ @0x86@ Function key F23
  | VK_F24                 -- ^ @0x87@ Function key F24
  | VK_NAVIGATION_VIEW     -- ^ @0x88@
  | VK_NAVIGATION_MENU     -- ^ @0x89@
  | VK_NAVIGATION_UP       -- ^ @0x8A@
  | VK_NAVIGATION_DOWN     -- ^ @0x8B@
  | VK_NAVIGATION_LEFT     -- ^ @0x8C@
  | VK_NAVIGATION_RIGHT    -- ^ @0x8D@
  | VK_NAVIGATION_ACCEPT   -- ^ @0x8E@
  | VK_NAVIGATION_CANCEL   -- ^ @0x8F@
  | VK_NUMLOCK             -- ^ @0x90@ NUM LOCK key
  | VK_SCROLL              -- ^ @0x91@ SCROLL LOCK key

  -- Fujitsu/OASYS keyboards
  | VK_OEM_FJ_JISHO        -- ^ @0x92@ Fujitsu\/OASYS keyboard, \"Dictionary\" key
  | VK_OEM_FJ_MASSHOU      -- ^ @0x93@ Fujitsu\/OASYS keyboard, \"Unregister word\" key
  | VK_OEM_FJ_TOUROKU      -- ^ @0x94@ Fujitsu\/OASYS keyboard, \"Register word\" key
  | VK_OEM_FJ_LOYA         -- ^ @0x95@ Fujitsu\/OASYS keyboard, \"Left OYAYUBI\" key
  | VK_OEM_FJ_ROYA         -- ^ @0x96@ Fujitsu\/OASYS keyboard, \"Right OYAYUBI\" key

  --                             0x97-0x9F  Unassigned
  | VK_LSHIFT              -- ^ @0xA0@ Left SHIFT key
  | VK_RSHIFT              -- ^ @0xA1@ Right SHIFT key
  | VK_LCONTROL            -- ^ @0xA2@ Left CONTROL key
  | VK_RCONTROL            -- ^ @0xA3@ Right CONTROL key
  | VK_LMENU               -- ^ @0xA4@ Left ALT key
  | VK_RMENU               -- ^ @0xA5@ Right ALT key
  | VK_BROWSER_BACK        -- ^ @0xA6@
  | VK_BROWSER_FORWARD     -- ^ @0xA7@
  | VK_BROWSER_REFRESH     -- ^ @0xA8@
  | VK_BROWSER_STOP        -- ^ @0xA9@
  | VK_BROWSER_SEARCH      -- ^ @0xAA@
  | VK_BROWSER_FAVORITES   -- ^ @0xAB@
  | VK_BROWSER_HOME        -- ^ @0xAC@
  | VK_VOLUME_MUTE         -- ^ @0xAD@
  | VK_VOLUME_DOWN         -- ^ @0xAE@
  | VK_VOLUME_UP           -- ^ @0xAF@
  | VK_MEDIA_NEXT_TRACK    -- ^ @0xB0@
  | VK_MEDIA_PREV_TRACK    -- ^ @0xB1@
  | VK_MEDIA_STOP          -- ^ @0xB2@
  | VK_MEDIA_PLAY_PAUSE    -- ^ @0xB3@
  | VK_LAUNCH_MAIL         -- ^ @0xB4@
  | VK_LAUNCH_MEDIA_SELECT -- ^ @0xB5@
  | VK_LAUNCH_APP1         -- ^ @0xB6@
  | VK_LAUNCH_APP2         -- ^ @0xB7@

  --                             0xB8-0xB9  Unassigned
  | VK_OEM_1               -- ^ @0xBA@ US label: @; :@
  | VK_OEM_PLUS            -- ^ @0xBB@ For any country\/region, the @+@ key
  | VK_OEM_COMMA           -- ^ @0xBC@ For any country\/region, the @,@ key
  | VK_OEM_MINUS           -- ^ @0xBD@ For any country\/region, the @-@ key
  | VK_OEM_PERIOD          -- ^ @0xBE@ For any country\/region, the @.@ key
  | VK_OEM_2               -- ^ @0xBF@ US label: @\/ ?@
  | VK_OEM_3               -- ^ @0xC0@ US label: @` ~@
  | VK_ABNT_C1             -- ^ @0xC1@
  | VK_ABNT_C2             -- ^ @0xC2@
  | VK_GAMEPAD_A           -- ^ @0xC3@
  | VK_GAMEPAD_B           -- ^ @0xC4@
  | VK_GAMEPAD_X           -- ^ @0xC5@
  | VK_GAMEPAD_Y           -- ^ @0xC6@
  | VK_GAMEPAD_RIGHT_SHOULDER -- ^ @0xC7@
  | VK_GAMEPAD_LEFT_SHOULDER  -- ^ @0xC8@
  | VK_GAMEPAD_LEFT_TRIGGER   -- ^ @0xC9@
  | VK_GAMEPAD_RIGHT_TRIGGER  -- ^ @0xCA@
  | VK_GAMEPAD_DPAD_UP        -- ^ @0xCB@
  | VK_GAMEPAD_DPAD_DOWN      -- ^ @0xCC@
  | VK_GAMEPAD_DPAD_LEFT      -- ^ @0xCD@
  | VK_GAMEPAD_DPAD_RIGHT     -- ^ @0xCE@
  | VK_GAMEPAD_MENU           -- ^ @0xCF@
  | VK_GAMEPAD_VIEW           -- ^ @0xD0@
  | VK_GAMEPAD_LEFT_THUMBSTICK_BUTTON  -- ^ @0xD1@
  | VK_GAMEPAD_RIGHT_THUMBSTICK_BUTTON -- ^ @0xD2@
  | VK_GAMEPAD_LEFT_THUMBSTICK_UP      -- ^ @0xD3@
  | VK_GAMEPAD_LEFT_THUMBSTICK_DOWN    -- ^ @0xD4@
  | VK_GAMEPAD_LEFT_THUMBSTICK_RIGHT   -- ^ @0xD5@
  | VK_GAMEPAD_LEFT_THUMBSTICK_LEFT    -- ^ @0xD6@
  | VK_GAMEPAD_RIGHT_THUMBSTICK_UP     -- ^ @0xD7@
  | VK_GAMEPAD_RIGHT_THUMBSTICK_DOWN   -- ^ @0xD8@
  | VK_GAMEPAD_RIGHT_THUMBSTICK_RIGHT  -- ^ @0xD9@
  | VK_GAMEPAD_RIGHT_THUMBSTICK_LEFT   -- ^ @0xDA@
  | VK_OEM_4               -- ^ @0xDB@ US label: @[ {@
  | VK_OEM_5               -- ^ @0xDC@ US label: @\ |@
  | VK_OEM_6               -- ^ @0xDD@ US label: @] }@
  | VK_OEM_7               -- ^ @0xDE@ US label: @' "@
  | VK_OEM_8               -- ^ @0xDF@

  --                             0xE0       OEM specific
  | VK_OEM_AX              -- ^ @0xE1@  \"AX\" key on Japanese AX keyboard
  | VK_OEM_102             -- ^ @0xE2@  \"<>\" or \"\|\" on RT 102-key keyboard
  | VK_ICO_HELP            -- ^ @0xE3@  Help key on ICO
  | VK_ICO_00              -- ^ @0xE4@  00 key on ICO
  | VK_PROCESSKEY          -- ^ @0xE5@ IME PROCESS key
                           -- Tells the application that the IME has processed a virtual key;
                           -- to retrieve the value of the virtual key, applications can call ImmGetVirtualKey
  | VK_ICO_CLEAR           -- ^ @0xE6@
  | VK_PACKET              -- ^ @0xE7@ Used to pass Unicode characters as if they were keystrokes.

  --                             0xE8       Unassigned
  -- Nokia/Ericsson definitions
  | VK_OEM_RESET           -- ^ @0xE9@
  | VK_OEM_JUMP            -- ^ @0xEA@
  | VK_OEM_PA1             -- ^ @0xEB@ Used to encode Klay's Numeric modifier
  | VK_OEM_PA2             -- ^ @0xEC@ Used to encode Klay's IsoLevel3 modifier
  | VK_OEM_PA3             -- ^ @0xED@ Used to encode Klay's IsoLevel5 modifier
  | VK_OEM_WSCTRL          -- ^ @0xEE@
  | VK_OEM_CUSEL           -- ^ @0xEF@
  | VK_OEM_ATTN            -- ^ @0xF0@
  | VK_OEM_FINISH          -- ^ @0xF1@
  | VK_OEM_COPY            -- ^ @0xF2@
  | VK_OEM_AUTO            -- ^ @0xF3@
  | VK_OEM_ENLW            -- ^ @0xF4@
  | VK_OEM_BACKTAB         -- ^ @0xF5@
  | VK_ATTN                -- ^ @0xF6@
  | VK_CRSEL               -- ^ @0xF7@
  | VK_EXSEL               -- ^ @0xF8@
  | VK_EREOF               -- ^ @0xF9@
  | VK_PLAY                -- ^ @0xFA@
  | VK_ZOOM                -- ^ @0xFB@
  | VK_NONAME              -- ^ @0xFC@
  | VK_PA1                 -- ^ @0xFD@
  | VK_OEM_CLEAR           -- ^ @0xFE@

  --                             0xFF       Unassigned
  -- deriving (Generic, NFData, Bounded, Enum, Ix, Eq, Ord, Show)
  -- deriving TextShow via (FromGeneric VirtualKey)
  deriving (Bounded, Enum, Ix, Eq, Ord, Show, Read)
  deriving TextShow via (FromStringShow VirtualKey)

-- | Alias for 'VK_KANA' (Changes the mode to hangeul)
pattern VK_HANGEUL :: VirtualKey
pattern VK_HANGEUL = VK_KANA

-- | Alias for 'VK_KANA' (Deprecated in favor of 'VK_HANGUL'. See: South Korea's standard Romanization)
pattern VK_HANGUL :: VirtualKey
pattern VK_HANGUL = VK_KANA

-- | Alias for 'VK_HANJA' (Changes the mode to hanja)
pattern VK_KANJI :: VirtualKey
pattern VK_KANJI = VK_HANJA

-- | Alias for 'VK_OEM_FJ_JISHO' (NEC PC-9800 keyboard, \"=\" key on numpad)
pattern VK_OEM_NEC_EQUAL :: VirtualKey
pattern VK_OEM_NEC_EQUAL = VK_OEM_FJ_JISHO

-- IME-specific keys (see: [Microsoft doc](https://docs.microsoft.com/en-us/previous-versions/cc194848(v=msdn.10)?redirectedfrom=MSDN)
-- When the IME program is active, it traps all keyboard events, including the virtual keys listed below.
-- Unless you are writing an IME package or bypassing the IME module to create your own application-specific input mechanisms
-- (definitely not recommended!), you don't need to worry about adding code to respond to these virtual keys.
--
-- Note: DBE means "double-byte enabled"

-- | @0x0f0@ IME: Changes the mode to alphanumeric
pattern VK_DBE_ALPHANUMERIC :: VirtualKey
pattern VK_DBE_ALPHANUMERIC = VK_OEM_ATTN

-- | @0x0f1@ IME: Changes the mode to katakana
pattern VK_DBE_KATAKANA :: VirtualKey
pattern VK_DBE_KATAKANA = VK_OEM_FINISH

-- | @0x0f2@ IME: Changes the mode to hiragana
pattern VK_DBE_HIRAGANA :: VirtualKey
pattern VK_DBE_HIRAGANA = VK_OEM_COPY

-- | @0x0f3@ IME: Changes the mode to single-byte characters
pattern VK_DBE_SBCSCHAR :: VirtualKey
pattern VK_DBE_SBCSCHAR = VK_OEM_AUTO

-- | @0x0f4@ IME: Changes the mode to double-byte characters
pattern VK_DBE_DBCSCHAR :: VirtualKey
pattern VK_DBE_DBCSCHAR = VK_OEM_ENLW

-- | @0x0f5@ IME: Changes the mode to Roman characters
pattern VK_DBE_ROMAN :: VirtualKey
pattern VK_DBE_ROMAN = VK_OEM_BACKTAB

-- | @0x0f6@ IME: Changes the mode to non-Roman characters
pattern VK_DBE_NOROMAN :: VirtualKey
pattern VK_DBE_NOROMAN = VK_ATTN

-- | @0x0f7@ IME: Activates the word registration dialog box
pattern VK_DBE_ENTERWORDREGISTERMODE :: VirtualKey
pattern VK_DBE_ENTERWORDREGISTERMODE = VK_CRSEL

-- | @0x0f8@ IME: Activates a dialog box for setting up an IME environment
pattern VK_DBE_ENTERIMECONFIGMODE :: VirtualKey
pattern VK_DBE_ENTERIMECONFIGMODE = VK_EXSEL

-- | @0x0f9@ IME: Deletes the undetermined string without determining it
pattern VK_DBE_FLUSHSTRING :: VirtualKey
pattern VK_DBE_FLUSHSTRING = VK_EREOF

-- | @0x0fa@ IME: Changes the mode to code input
pattern VK_DBE_CODEINPUT :: VirtualKey
pattern VK_DBE_CODEINPUT = VK_PLAY

-- | @0x0fb@ IME: Changes the mode to non-code input
pattern VK_DBE_NOCODEINPUT :: VirtualKey
pattern VK_DBE_NOCODEINPUT = VK_ZOOM

-- | @0x0fc@ IME: ???
pattern VK_DBE_DETERMINESTRING :: VirtualKey
pattern VK_DBE_DETERMINESTRING = VK_NONAME

-- | @0x0fd@ IME: ???
pattern VK_DBE_ENTERDLGCONVERSIONMODE :: VirtualKey
pattern VK_DBE_ENTERDLGCONVERSIONMODE = VK_PA1

instance Magma VirtualKey

instance NFData VirtualKey where
  rnf = (`seq` ())

instance ToJSON VirtualKey where
  -- toJSON     = Aeson.genericToJSON (jsonOptions (drop 3))
  -- toEncoding = Aeson.genericToEncoding (jsonOptions (drop 3))
  toJSON     = Aeson.String . serializeVirtualKey
  toEncoding = Aeson.toEncoding . serializeVirtualKey

instance FromJSON VirtualKey where
  -- parseJSON = Aeson.genericParseJSON (jsonOptions (drop 3))
  parseJSON = Aeson.withText "virtual key" $
    either fail pure . parseVirtualKey

instance ToJSONKey VirtualKey where
  -- toJSONKey = Aeson.genericToJSONKey (jsonKeyOptions (drop 3))
  toJSONKey = Aeson.toJSONKeyText serializeVirtualKey

instance FromJSONKey VirtualKey where
  -- fromJSONKey = Aeson.genericFromJSONKey (jsonKeyOptions (drop 3))
  fromJSONKey = Aeson.FromJSONKeyTextParser $
    either fail pure . parseVirtualKey

instance Arbitrary VirtualKey where
  arbitrary = chooseEnum (minBound, maxBound)
  -- shrink = genericShrink

type VirtualKeysDefinition = Map.Map Key VirtualKey

serializeVirtualKey :: VirtualKey -> T.Text
serializeVirtualKey = T.drop 3 . showt

parseVirtualKey :: T.Text -> Either String VirtualKey
parseVirtualKey t =
  let s = "VK_" <> T.unpack t
  in case reads s of
    [(vk, "")] -> Right vk
    _          -> Left $ "Invalid virtual key: " <> s
