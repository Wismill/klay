{-# LANGUAGE OverloadedLists           #-}

module Klay.Keyboard.Layout.Modifier.Types
  ( -- * Modifier description
    Modifier
      ( ..
      , LShift, RShift, ShiftLatch, ShiftLock
      , CapsSet, CapsLock
      , LControl, RControl
      , LAlternate, RAlternate
      , LSuper, RSuper
      , NumericSet, NumericLatch, NumericLock
      , IsoLevel3Set, IsoLevel3Latch, IsoLevel3Lock
      , IsoLevel5Set, IsoLevel5Latch, IsoLevel5Lock
      )
  , ModifierBit(..)
  , ModifierVariant(..)
  , ModifierEffect(..)
  , serializeModifier
  , serializeModifierBit
  , modifierP
  , modifierBitP
  , modifierVariantP
  , modifierEffectP
    -- * Combination of modifiers
  , ModifiersCombo
  , ModifiersNECombo
  , ModifiersCombos
  , ModifiersMask
  , ModifiersField(.., NoModifier)
  , ModifiersFields
  , modifiersComboP
  , modifiersNEComboP
  , allModifiers
  , modifierToMask
  , modifiersToMask
  , maskToModifiers
  , isActiveModifierBit
  , isActiveModifier
    -- * Modifiers options
  , ModifiersOption(.., MTarget)
  , ModifiersOptions(.., NoModifiersOptions)
  , ModifiersOptionsDelta
  , ModifiersOptionsAlteration
  , ModifiersOptionsAlterations
  , AutoModifiersOptions(.., DefaultAutoModifiersOptions)
  , AutoModifiersOptionsMode(..)
  , CapsLockBehaviour(.., DefaultCapsLockBehaviour)
  , CapsLockBehaviourAlteration
  , normaliseModifiersOptions
  , modifiersOptions
  , modifiersOptionsToList
  ) where

import Data.Semigroup (Semigroup(..))
import Data.Void (Void)
import Data.Maybe (fromMaybe)
import Data.Bits (Bits(..))
import Data.List ((\\))
import Data.List.NonEmpty qualified as NE
import Data.Set qualified as Set
import Data.Set.NonEmpty qualified as NESet
import Klay.Utils.Orphans.Set.NonEmpty ()
import Data.IntMap.Strict qualified as IMap
import Data.Char (toLower)
import Data.Text qualified as T
import Data.Functor (($>))
import Data.Bifunctor (Bifunctor(..))
import Data.Foldable (foldl')
import Data.Hashable (Hashable)
import Control.Applicative ()
import Control.DeepSeq (NFData(..))
import GHC.Exts (IsList(..))
import GHC.Generics (Generic(..))

import TextShow (TextShow(..), FromStringShow(..))
import TextShow.Generic (FromGeneric(..))
import Test.QuickCheck.Arbitrary (Arbitrary(..), genericShrink)
import Test.QuickCheck.Gen (resize, chooseInt, chooseEnum, frequency, elements)
import Data.Witherable (WitherableWithIndex(..))
import Data.Aeson.Types (ToJSON(..), ToJSONKey(..), FromJSON(..), FromJSONKey(..))
import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as Aeson
import Data.Aeson.Encoding qualified as Aeson
import Text.Megaparsec
import Text.Megaparsec.Char

import Data.Alterable
  (Delta(..), Alterable(..), Alteration(..), Magma(..), Differentiable(..), insertOrUpdate1d)
import Klay.Utils (jsonOptions, toJsonOptionsNewtype)

-- | A fully configured modifier
data Modifier = Modifier
  { _mBit :: !ModifiersNECombo    -- ^ Modifier bits (see 'ModifierBit')
  , _mVariant :: !ModifierVariant -- ^ Variant: 'L'eft, 'N'eutral or 'R'ight
  , _mEffect :: !ModifierEffect   -- ^ Effect: 'Set', 'Latch' or 'Lock'
  }
  deriving (Generic, NFData, Hashable, Eq, Ord, Show)
  deriving TextShow via (FromStringShow Modifier)

-- [NOTE] Noisy because of https://gitlab.haskell.org/ghc/ghc/-/issues/14380
pattern LShift, RShift, ShiftLatch, ShiftLock, CapsSet, CapsLock :: Modifier
-- | Left 'Shift'
pattern LShift <- Modifier [Shift] L Set where
  LShift = Modifier [Shift] L Set
-- | Right 'Shift'
pattern RShift <- Modifier [Shift] R Set where
  RShift = Modifier [Shift] R Set
-- | 'Shift' 'Latch'
pattern ShiftLatch <- Modifier [Shift] N Latch where
  ShiftLatch = Modifier [Shift] N Latch
-- | 'Shift' 'Lock'
pattern ShiftLock <- Modifier [Shift] N Lock where
  ShiftLock = Modifier [Shift] N Lock
-- | 'Capitals' 'Set'
pattern CapsSet <- Modifier [Capitals] N Set where
  CapsSet = Modifier [Capitals] N Set
-- | 'Capitals' 'Lock' (usual @CapsLock@)
pattern CapsLock <- Modifier [Capitals] N Lock where
  CapsLock = Modifier [Capitals] N Lock

pattern LControl, RControl, LAlternate, RAlternate, LSuper, RSuper :: Modifier
-- | Left 'Control'
pattern LControl <- Modifier [Control] L Set where
  LControl = Modifier [Control] L Set
-- | Right 'Control'
pattern RControl <- Modifier [Control] R Set where
  RControl = Modifier [Control] R Set
-- | Left 'Alternate'
pattern LAlternate <- Modifier [Alternate] L Set where
  LAlternate = Modifier [Alternate] L Set
-- | Right 'Alternate'
pattern RAlternate <- Modifier [Alternate] R Set where
  RAlternate = Modifier [Alternate] R Set
-- | Left 'Super'
pattern LSuper <- Modifier [Super] L Set where
  LSuper = Modifier [Super] L Set
-- | Right 'Super'
pattern RSuper <- Modifier [Super] R Set where
  RSuper = Modifier [Super] R Set

pattern NumericSet, NumericLatch, NumericLock :: Modifier
-- | 'Numeric' 'Set'
pattern NumericSet <- Modifier [Numeric] N Set where
  NumericSet = Modifier [Numeric] N Set
-- | 'Numeric' 'Latch'
pattern NumericLatch <- Modifier [Numeric] N Latch where
  NumericLatch = Modifier [Numeric] N Latch
-- | 'Numeric' 'Lock' (usual @NumLock@)
pattern NumericLock <- Modifier [Numeric] N Lock where
  NumericLock = Modifier [Numeric] N Lock

pattern IsoLevel3Set, IsoLevel3Latch, IsoLevel3Lock :: Modifier
-- | 'IsoLevel3' 'Set'
pattern IsoLevel3Set <- Modifier [IsoLevel3] N Set where
  IsoLevel3Set = Modifier [IsoLevel3] N Set
-- | 'IsoLevel3' 'Latch'
pattern IsoLevel3Latch <- Modifier [IsoLevel3] N Latch where
  IsoLevel3Latch = Modifier [IsoLevel3] N Latch
-- | 'IsoLevel3' 'Lock'
pattern IsoLevel3Lock <- Modifier [IsoLevel3] N Lock where
  IsoLevel3Lock = Modifier [IsoLevel3] N Lock

pattern IsoLevel5Set, IsoLevel5Latch, IsoLevel5Lock :: Modifier
-- | 'IsoLevel5' 'Set'
pattern IsoLevel5Set <- Modifier [IsoLevel5] N Set where
  IsoLevel5Set = Modifier [IsoLevel5] N Set
-- | 'IsoLevel5' 'Latch'
pattern IsoLevel5Latch <- Modifier [IsoLevel5] N Latch where
  IsoLevel5Latch = Modifier [IsoLevel5] N Latch
-- | 'IsoLevel5' 'Lock'
pattern IsoLevel5Lock <- Modifier [IsoLevel5] N Lock where
  IsoLevel5Lock = Modifier [IsoLevel5] N Lock

instance ToJSON Modifier where
  toJSON     = Aeson.genericToJSON (jsonOptions (fmap toLower . drop 2))
  toEncoding = Aeson.genericToEncoding (jsonOptions (fmap toLower . drop 2))

instance FromJSON Modifier where
  parseJSON = Aeson.genericParseJSON (jsonOptions (fmap toLower . drop 2))

instance ToJSONKey Modifier where
  toJSONKey = Aeson.toJSONKeyText serializeModifier

instance FromJSONKey Modifier where
  fromJSONKey = Aeson.FromJSONKeyTextParser
              $ either fail pure . parseModifier

instance Arbitrary Modifier where
  arbitrary = Modifier <$> arbitrary <*> arbitrary <*> arbitrary
  shrink = genericShrink

serializeModifier :: Modifier -> T.Text
serializeModifier (Modifier bs v e) = case bs of
  [b] -> mconcat [v', serializeModifierBit b, e']
  _   -> mconcat [v', ":", serializeModifiersCombo bs, e']
  where v' = case v of
             N -> ""
             L -> "l"
             R -> "r"
        e' = case e of
             Set   -> ""
             Latch -> ":latch"
             Lock  -> ":lock"

parseModifier :: T.Text -> Either String Modifier
parseModifier = first errorBundlePretty
                   . parse (modifierP <* eof) mempty

type Parser = Parsec Void T.Text

modifierP :: Parser Modifier
modifierP = do
  v <- try modifierVariantP <|> pure N
  _ <- optional (char ':')
  bs <- modifiersNEComboP
  e <- (char ':' *> modifierEffectP) <|> pure Set
  pure $ Modifier bs v e

{-|
Modifier bits used to define the levels in the implementation.

Currently following the limitation of XKB to 8 bits for keyboard level state.

__Notes:__

* It is not clear how to deal with @Meta@ and @Hyper@ on Linux
  in conjunction with 'IsoLevel3' & 'IsolLevel5'. So deactivate them for now.
* On Windows @NumLock@ it is not a conventional modifier.
  Therefore 'Numeric' will be mapped to a custom bit.
-}
data ModifierBit
  = Shift     -- ^ ISO level /2/: the usual @Shift@
  | Capitals  -- ^ The usual @CapsLock@
  | Control   -- ^ The usual @Control@
  | Alternate -- ^ The usual @Alt@ on Linux and Windows, @Option@ on Mac.
  | Super     -- ^ @Super@ on Linux, the @Windows key@ on Windows and @Command@ on Mac.
  | Numeric   -- ^ The usual @NumLock@.
  | IsoLevel3 -- ^ ISO level /3/, usually labelled @Altgr@
  | IsoLevel5 -- ^ ISO level /5/ (non conventional)
  deriving (Generic, NFData, Hashable, Eq, Ord, Enum, Bounded, Show)
  deriving TextShow via (FromGeneric ModifierBit)

instance ToJSON ModifierBit where
  toJSON     = Aeson.String . serializeModifierBit
  toEncoding = Aeson.text   . serializeModifierBit

instance FromJSON ModifierBit where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance Arbitrary ModifierBit where
  arbitrary = chooseEnum (minBound, maxBound)

serializeModifierBit :: ModifierBit -> T.Text
serializeModifierBit Shift     = "shift"
serializeModifierBit Capitals  = "caps"
serializeModifierBit Control   = "ctrl"
serializeModifierBit Alternate = "alt"
serializeModifierBit Super     = "super"
serializeModifierBit Numeric   = "numeric"
serializeModifierBit IsoLevel3 = "level3"
serializeModifierBit IsoLevel5 = "level5"

modifierBitP :: Parser ModifierBit
modifierBitP = choice (
  [ string "shift" $> Shift
  , string "capitals" $> Capitals
  , string "caps" $> Capitals
  , string "control" $> Control
  , string "ctrl" $> Control
  , string "alternate" $> Alternate
  , string "alt" $> Alternate
  , string "super" $> Super
  , string "numeric" $> Numeric
  , string "num" $> Numeric
  , string "iso_level_3" $> IsoLevel3
  , string "iso3" $> IsoLevel3
  , string "level3" $> IsoLevel3
  , string "iso_level_5" $> IsoLevel5
  , string "iso5" $> IsoLevel5
  , string "level5" $> IsoLevel5
  ] :: [Parser ModifierBit]) <?> "modifier bit"

-- | Denotes the usual position of the modifier key on a keyboard.
data ModifierVariant
  = L -- ^ Left variant
  | N -- ^ Neutral variant
  | R -- ^ Right variant
  deriving (Generic, NFData, Hashable, Eq, Ord, Enum, Bounded, Show)
  deriving TextShow via (FromGeneric ModifierVariant)

instance ToJSON ModifierVariant where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON ModifierVariant where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance Arbitrary ModifierVariant where
  arbitrary = chooseEnum (minBound, maxBound)

modifierVariantP :: Parser ModifierVariant
modifierVariantP = choice (
  [ char 'l' <* notFollowedBy (string "evel") $> L
  , char 'r' $> R
  , char 'n' <* notFollowedBy (string "um")$> N
  ] :: [Parser ModifierVariant])
  <?> "modifier variant"

-- | Effect of a modifier
data ModifierEffect
  = Set   -- ^ The modifier is active as long as its key is pressed.
  | Latch -- ^ The modifier remains active until the next non-modifier key is pressed.
  | Lock  -- ^ The modifier remains active until it is explicitly deactivated.
  deriving (Generic, NFData, Hashable, Eq, Ord, Enum, Bounded, Show, Read)
  deriving TextShow via (FromGeneric ModifierEffect)

instance ToJSON ModifierEffect where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON ModifierEffect where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance Arbitrary ModifierEffect where
  arbitrary = chooseEnum (minBound, maxBound)

modifierEffectP :: Parser ModifierEffect
modifierEffectP = choice (
  [ string "set"   $> Set
  , string "latch" $> Latch
  , string "lock"  $> Lock
  ] :: [Parser ModifierEffect])
  <?> "modifier effect"

--- Modifiers Combinations ----------------------------------------------------

-- | A combination of 'ModifierBit's
type ModifiersCombo = Set.Set ModifierBit
-- | A non-empty collection of 'ModifierBit's
type ModifiersNECombo = NESet.NESet ModifierBit

-- | A combination of 'ModifiersCombo'
type ModifiersCombos = Set.Set ModifiersCombo

serializeModifiersCombo :: ModifiersNECombo -> T.Text
serializeModifiersCombo
  = sconcat
  . NE.intersperse "+"
  . fmap serializeModifierBit
  . NESet.toList

modifiersComboP :: Parser ModifiersCombo
modifiersComboP = Set.fromList
  <$> modifierBitP `sepBy` (oSpace *> sep *> oSpace)
  <?> "modifier combo"
  where oSpace = many (char ' ')
        sep = choice (
          [ char '+'
          , char '|'
          , char ','
          ] :: [Parser Char])

modifiersNEComboP :: Parser ModifiersNECombo
modifiersNEComboP = modifiersComboP >>=
  NESet.withNonEmpty (fail "empty set of modifier bits") pure

-- | The combination of all 'ModifierBit's.
allModifiers :: ModifiersCombo
allModifiers = Set.fromList $ enumFromTo minBound maxBound

-- | Alias when 'ModifiersField' is used to test a level.
type ModifiersMask = ModifiersField
-- | A set of 'ModifiersField'
type ModifiersFields = Set.Set ModifiersField

-- | A bit field representing a combination of 'ModifierBit's.
newtype ModifiersField = ModifiersField {_mField :: Int}
  deriving stock (Generic, Show)
  deriving newtype (NFData, Eq, Ord, Bits)
  deriving TextShow via (FromGeneric ModifiersField)

-- | Empty modifiers field
pattern NoModifier :: ModifiersField
pattern NoModifier = ModifiersField 0

instance Semigroup ModifiersField where
  (<>) = (.|.)

instance Monoid ModifiersField where
  mempty = ModifiersField 0

instance Bounded ModifiersField where
  minBound = mempty
  maxBound = modifiersToMask allModifiers

-- [FIXME] check this instance
instance Enum ModifiersField where
  toEnum k | k < 0 = error $ "Negative modifiers field: " <> show k
           | k > _mField maxBound = error $ "Modifiers field too big: " <> show k
           | otherwise = ModifiersField k

  fromEnum (ModifiersField m) = m

  enumFrom     x   = enumFromTo     x maxBound
  enumFromThen x y = enumFromThenTo x y bound
    where
      bound | fromEnum y >= fromEnum x = maxBound
            | otherwise                = minBound

instance IsList ModifiersField where
  type (Item ModifiersField) = ModifierBit
  fromList = foldl' (.|.) mempty . fmap modifierToMask
  toList m = filter (isActiveModifierBit m) $ enumFromTo minBound maxBound

instance ToJSON ModifiersField where
  toJSON = Aeson.String . serializeModifiersField
  toEncoding = toEncoding . serializeModifiersField

instance FromJSON ModifiersField where
  parseJSON = Aeson.withText "modifiers" $
    either fail pure . parseModifiersField

instance ToJSONKey ModifiersField where
  toJSONKey = Aeson.toJSONKeyText serializeModifiersField

instance FromJSONKey ModifiersField where
  fromJSONKey = Aeson.FromJSONKeyTextParser $
    either fail pure . parseModifiersField

instance Arbitrary ModifiersField where
  arbitrary = modifiersToMask <$> arbitrary

serializeModifiersField :: ModifiersField -> T.Text
serializeModifiersField f =
  let ms = maskToModifiers f
  in NESet.withNonEmpty "base" serializeModifiersCombo ms

parseModifiersField :: T.Text -> Either String ModifiersField
parseModifiersField "base" = Right mempty
parseModifiersField t
  = bimap errorBundlePretty modifiersToMask
  $ parse (modifiersComboP <* eof) mempty t

allModifiersFields :: [ModifiersField]
allModifiersFields = enumFromTo minBound maxBound

-- | Convert a 'ModifierBit' to its corresponding mask.
modifierToMask :: ModifierBit -> ModifiersField
modifierToMask = ModifiersField . shiftL 1 . fromEnum

-- | A combination of 'ModifierBit's to their corresponding mask.
modifiersToMask :: ModifiersCombo -> ModifiersField
modifiersToMask = Set.foldl' (.|.) mempty . Set.map modifierToMask

-- | Get the corresponding combination of 'ModifierBit's from a 'ModifierField'.
maskToModifiers :: ModifiersField -> ModifiersCombo
maskToModifiers l = Set.filter (isActiveModifierBit l) allModifiers

-- | Test if a modifier correspond to one of the given modifiers bit.
isActiveModifierBit
  :: ModifiersField -- ^ Modifiers of the level to test
  -> ModifierBit    -- ^ A modifier to test
  -> Bool
isActiveModifierBit ms = testBit ms . fromEnum

-- | Test if a modifier correspond to one of the given modifiers.
isActiveModifier
  :: (Foldable f)
  => ModifiersField -- ^ Modifiers of the level to test
  -> f ModifierBit  -- ^ A modifier to test
  -> Bool
isActiveModifier ms = all (testBit ms . fromEnum)


--- Modifiers Options ---------------------------------------------------------

-- | Option to control the behaviour of a 'ModifiersField'.
data ModifiersOption = ModifiersOption
  { _mTarget :: !ModifiersField
  -- ^ Conversion to a reference modifier combo [TODO] doc
  , _mPreserve :: !ModifiersField
  -- ^ Modifier bits that are preserved [TODO] doc
  } deriving (Generic, NFData, Eq, Ord, Show)
  deriving TextShow via (FromStringShow ModifiersOption)

pattern MTarget :: ModifiersField -> ModifiersOption
pattern MTarget m = ModifiersOption m NoModifier

instance Differentiable ModifiersOption ModifiersOption

instance Magma ModifiersOption

instance Arbitrary ModifiersOption where
  arbitrary = do
    ms <- chooseEnum (minBound, maxBound)
    ms' <- chooseEnum (minBound, maxBound)
    pure $ ModifiersOption ms (ms .&. ms')

instance ToJSON ModifiersOption where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 2))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 2))

instance FromJSON ModifiersOption where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 2))

type ModifiersOptionAlteration = Alteration ModifiersOption ModifiersOption

-- | Options to control the behaviour of some modifiers.
newtype ModifiersOptions = ModifiersOptions (IMap.IntMap ModifiersOption)
  deriving stock (Generic, Eq, Ord, Show)
  deriving newtype (NFData)
  deriving TextShow via (FromStringShow ModifiersOptions)

pattern NoModifiersOptions :: ModifiersOptions
pattern NoModifiersOptions <- ModifiersOptions (IMap.null -> True) where
  NoModifiersOptions = ModifiersOptions mempty

-- | Right-biased: keep the right element except if it is 'NoModifiersOptions'.
instance Magma ModifiersOptions where
  mo +> NoModifiersOptions = mo
  _  +> mo                 = mo

-- | Right-biased, no normalisation to preserve associativity
instance Semigroup ModifiersOptions where
  ModifiersOptions mo1 <> ModifiersOptions mo2 =
    ModifiersOptions (IMap.unionWith (+>) mo1 mo2)

instance Monoid ModifiersOptions where
  mempty = ModifiersOptions mempty

type ModifiersOptionsDelta       = Delta      ModifiersOptions ModifiersOptionsAlterations
type ModifiersOptionsAlteration  = Alteration ModifiersOptions ModifiersOptionsAlterations
type ModifiersOptionsAlterations = IMap.IntMap ModifiersOptionAlteration

instance Differentiable ModifiersOptions ModifiersOptions

instance Differentiable ModifiersOptions ModifiersOptionsDelta where
  diff NoModifiersOptions NoModifiersOptions = Nothing
  diff NoModifiersOptions mo                 = Just (Inserted mo)
  diff mo                 NoModifiersOptions = Just (Deleted mo)
  diff (ModifiersOptions mo1) (ModifiersOptions mo2) =
    Updated <$> diff mo1 mo2

instance Differentiable ModifiersOptions ModifiersOptionsAlterations where
  diff (ModifiersOptions a) (ModifiersOptions b) = diff a b

instance Differentiable ModifiersOptions ModifiersOptionsAlteration where
  diff NoModifiersOptions NoModifiersOptions = Nothing
  diff _                  NoModifiersOptions = Just Delete
  diff (ModifiersOptions mo1) (ModifiersOptions mo2) =
    insertOrUpdate1d <$> diff mo1 mo2

instance Alterable ModifiersOptions ModifiersOptionsDelta where
  alter (Inserted mo) _  = mo
  alter (Updated u)   mo = fromMaybe mempty $ normaliseModifiersOptions (alter u mo)
  alter (Deleted _)   _  = NoModifiersOptions

instance Alterable ModifiersOptions ModifiersOptionsAlterations where
  alter u (ModifiersOptions mo) = fromMaybe mempty $ normaliseModifiersOptions (ModifiersOptions (alter u mo))

instance Alterable ModifiersOptions ModifiersOptionsAlteration where
  alter (InsertOrIgnore mo)   NoModifiersOptions = mo
  alter (InsertOrIgnore _)    mo                 = mo
  alter (InsertOrReplace mo)  _                  = mo
  alter (InsertOrUpdate mo _) NoModifiersOptions = mo
  alter (InsertOrUpdate _ u)  mo                 = fromMaybe mempty $ normaliseModifiersOptions (alter u mo)
  alter (Update u)            mo                 = fromMaybe mempty $ normaliseModifiersOptions (alter u mo)
  alter Delete                _                  = NoModifiersOptions

instance IsList ModifiersOptions where
  type (Item ModifiersOptions) = (ModifiersField, ModifiersOption)
  fromList = fromMaybe mempty . modifiersOptions
  toList = modifiersOptionsToList

instance ToJSON ModifiersOptions where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON ModifiersOptions where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

instance Arbitrary ModifiersOptions where
  arbitrary = do
    -- Choose levels to redirect
    n <- chooseInt (0, _mField maxBound - 1)
    redirected <- Set.toList <$> resize n arbitrary
    -- Compute possible targets
    let targets = allModifiersFields \\ redirected
    -- Ensure that targets are final targets (not redirected themselves)
    let f l = do t <- elements targets
                 p <- elements allModifiersFields
                 pure (_mField l, ModifiersOption t (l .&. p))
    ModifiersOptions . fromList <$> traverse f redirected
  shrink = genericShrink

-- [TODO] Use a data type that avoid incorrectness by construction
-- | Check and normalise 'ModifiersOptions'
normaliseModifiersOptions :: ModifiersOptions -> Maybe ModifiersOptions
normaliseModifiersOptions (ModifiersOptions mo) = ModifiersOptions <$> iwither f mo
  where
    f l (ModifiersOption r@(ModifiersField l') p)
      -- Level redirects to itself without preserving modifiers
      | l == l' && p == mempty = Just Nothing
      -- Redirection is not final: redirected itself
      | maybe False ((/= l') . _mField . _mTarget) (IMap.lookup l' mo) = Nothing
      -- Normalise '_mPreserve' field
      | otherwise              = Just (Just (ModifiersOption r (ModifiersField l .&. p)))

-- | Smart constructor for 'ModifiersOptions'
modifiersOptions :: [(ModifiersField, ModifiersOption)] -> Maybe ModifiersOptions
modifiersOptions = normaliseModifiersOptions . ModifiersOptions . IMap.fromList . fmap (first _mField)

modifiersOptionsToList :: ModifiersOptions -> [(ModifiersField, ModifiersOption)]
modifiersOptionsToList (ModifiersOptions mo) = first toEnum <$> IMap.toList mo

-- | Configuration of the automated derivation of 'ModifiersOptions' using
--   the levels and corresponding symbols of a key action map.
data AutoModifiersOptions
  = Auto
  -- ^ Automatically derive 'ModifiersOptions' with 'DefaultCapsLockBehaviour'.
  --   It is overriden if combined with another 'AutoModifiersOptions'.
  | AutoWith !CapsLockBehaviour
  -- ^ Automatically derive 'ModifiersOptions' with a given 'CapsLockBehaviour'.
  | Manual
  -- ^ No automated derivation. Keep the 'ModifiersOptions' as parametered in the actions.
  deriving (Generic, NFData, Show)
  deriving TextShow via (FromGeneric AutoModifiersOptions)

instance Eq AutoModifiersOptions where
  Auto == Auto                        = True
  Auto == DefaultAutoModifiersOptions = True
  DefaultAutoModifiersOptions == Auto = True
  AutoWith opt1 == AutoWith opt2      = opt1 == opt2
  Manual == Manual                    = True
  _ == _                              = False

instance Ord AutoModifiersOptions where
  Auto `compare` Auto                        = EQ
  Auto `compare` DefaultAutoModifiersOptions = EQ
  Auto `compare` _                           = LT
  DefaultAutoModifiersOptions `compare` Auto = EQ
  AutoWith _    `compare` Auto               = GT
  AutoWith opt1 `compare` AutoWith opt2      = opt1 `compare` opt2
  AutoWith _    `compare` _                  = LT
  Manual `compare` Manual                    = EQ
  Manual `compare` _                         = GT

-- | 'Auto' is neutral, 'Manual' absorbs, else right-biased.
instance Magma AutoModifiersOptions where
  Auto   +> a      = a
  a      +> Auto   = a
  Manual +> _      = Manual
  _      +> Manual = Manual
  _      +> a      = a

instance Semigroup AutoModifiersOptions where
  (<>) = (+>)

instance Monoid AutoModifiersOptions where
  mempty = Auto

instance Differentiable AutoModifiersOptions AutoModifiersOptions

instance ToJSON AutoModifiersOptions where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON AutoModifiersOptions where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance Arbitrary AutoModifiersOptions where
  arbitrary = frequency
    [ (1, pure Auto)
    , (fromEnum (maxBound :: CapsLockBehaviour) + 1, AutoWith <$> arbitrary)
    , (1, pure Manual)
    ]
  shrink = genericShrink

-- | Default 'CapsLockBehaviour'
pattern DefaultCapsLockBehaviour :: CapsLockBehaviour
pattern DefaultCapsLockBehaviour = CapsIsShiftCancel

-- | Default 'AutoModifiersOptions'
pattern DefaultAutoModifiersOptions :: AutoModifiersOptions
pattern DefaultAutoModifiersOptions = AutoWith DefaultCapsLockBehaviour

-- | Configure the mode of resolution of 'AutoModifiersOptions'.
data AutoModifiersOptionsMode
  = Strict -- ^ Apply strict rules.
  | Lax    -- ^ Apply lax rules as XKB.
  deriving (Eq, Show)

-- | Describe the behaviour of 'Capitals'.
data CapsLockBehaviour
  = CapsIsShiftCancel
  -- ^ 'Capitals' act as 'Shift' with the combo, but the addition of 'Shift' cancels it:
  -- @combo + Capitals = combo + Shift@ but @combo + Capitals + Shift = combo@
  | CapsIsShiftNoCancel
  -- ^ 'Capitals' act as 'Shift' with the combo; the addition of 'Shift' has no impact.
  -- @combo + Capitals = combo + Shift = combo + Capitals + Shift@
  | CapsIsNotShift
  -- ^ 'Capitals' acts as an independent modifier.
  deriving (Generic, NFData, Eq, Ord, Enum, Bounded, Show)
  deriving TextShow via (FromGeneric CapsLockBehaviour)

type CapsLockBehaviourAlteration = Alteration CapsLockBehaviour CapsLockBehaviour

instance Differentiable CapsLockBehaviour CapsLockBehaviour

instance Magma CapsLockBehaviour

instance ToJSON CapsLockBehaviour where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON CapsLockBehaviour where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance Arbitrary CapsLockBehaviour where
  arbitrary = chooseEnum (minBound, maxBound)
