{-|
Description: Model of a keyboard layout
-}

module Klay.Keyboard.Layout.Internal.Layout
  ( Layout(..)
  , MultiOsRawLayout
  , RawLayout
  , ResolvedLayout
  , LinuxLayout
  , LayoutMetadata(..)

  , SystemName(..)
  , LayoutLocales
  , Locales(..)
  , Version(..)

  , LayoutOptions
  , OsIndependentOptions(..)
  , LinuxOptions(..)
  , WindowsOptions(..)
  , KeyNames
  ) where

import Numeric.Natural
import Data.Typeable
import Data.Set (Set)
import Data.String (IsString(..))
-- import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Time.Calendar (Day)
-- import Data.Time.Format (parseTimeOrError, defaultTimeLocale)
import GHC.Generics (Generic)
import Control.Applicative (Alternative(..))
import Control.DeepSeq (NFData(..))

import Data.Versions qualified as V
import Klay.Utils.Orphans.Versions ()
import Data.BCP47 (BCP47) -- mkLanguage
-- import Data.LanguageCodes (ISO639_1(EN))
import Text.Megaparsec (errorBundlePretty)
import Data.Aeson qualified as Aeson
import Data.Aeson.Encoding qualified as Aeson
import Data.Aeson.Types (ToJSON(..), FromJSON(..))

import Klay.Keyboard.Hardware.Key.Types (KeyNames)
import Klay.Keyboard.Hardware.ButtonAction
import Klay.Keyboard.Layout.Action (CustomActionLabels)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.VirtualKey.Types (VirtualKeysDefinition)
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.OS
import Klay.Utils (jsonOptions, toJsonOptionsNewtype)
import Klay.Utils.UserInput


{-|
A keyboard layout is defined by:

  * Its metadata 'LayoutMetadata'
  * A mapping between the physical keys and the virtual keys 'ButtonActionsDefinition'. See also "Klay.Keyboard.Layout.VirtualKey".
  * The definitions of the dead keys '_deadKeys'
  * The various action mappings organised in groups '_groups'.
  * Some options
-}
data Layout gs = Layout
  { _metadata :: !LayoutMetadata
  -- ^ Information about the layout, such as: (system) name, version, author, etc.
  , _dualFunctionKeys :: !ButtonActionsDefinition
  -- ^ A Mapping of the raw scan codes to support dual-function keys.
  , _deadKeys :: !DeadKeyDefinitions
  -- ^ The definitions of the dead keys.
  , _groups :: !gs
  -- ^ The mappings between virtual keys and actions, grouped in 'Group'.
  , _options :: !LayoutOptions
  -- ^ Options for the layout
  } deriving (Generic, NFData, Typeable, Eq, Show)

instance (ToJSON gs) => ToJSON (Layout gs) where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 1))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 1))

instance (FromJSON gs) => FromJSON (Layout gs) where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 1))

-- | A raw multi-OS layout: actions not resolved
type MultiOsRawLayout = Layout MultiOsRawGroups
-- | A raw single-OS layout: actions not resolved
type RawLayout        = Layout RawGroups
-- | A single-OS layout with its actions resolved
type ResolvedLayout   = Layout ResolvedGroups
-- | A Linux layout
type LinuxLayout      = ResolvedLayout


--- Options -------------------------------------------------------------------

-- | Options of a layout
type LayoutOptions = MultiOs OsIndependentOptions LinuxOptions WindowsOptions

-- | OS-independent options for a layout
newtype OsIndependentOptions = OsIndependentOptions
  { _iActionLabels :: CustomActionLabels
    -- ^ Custom labels of the actions.
  } deriving stock (Generic, Show)
  deriving newtype (Eq)
  deriving anyclass (NFData)

instance ToJSON OsIndependentOptions where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 2))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 2))

instance FromJSON OsIndependentOptions where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 2))

-- | Linux-specific options for a layout
newtype LinuxOptions = LinuxOptions
  { _lActionLabels :: CustomActionLabels
    -- ^ Custom labels of the actions.
  } deriving stock (Generic, Show)
  deriving newtype (Eq)
  deriving anyclass (NFData)

instance ToJSON LinuxOptions where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 2))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 2))

instance FromJSON LinuxOptions where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 2))

-- | Windows-specific options for a layout
data WindowsOptions = WindowsOptions
  { _wActionLabels :: CustomActionLabels
    -- ^ Custom labels of the actions.
  , _wKeyboardType :: Raw Natural
    -- ^ Keyboard type
  , _wKeysNames :: !KeyNames
  -- ^ The names of the virtual keys.
  , _wVirtualKeyDefinition :: VirtualKeysDefinition
    -- ^ Explicit mapping 'Key' -> 'VirtualKey'.
    -- It is automatically deduced but the user may force part of this mapping.
  , _wAltgr :: Bool
    -- ^ If 'True', then treat right @Alt@ as @Ctrl + Alt@
  , _wIsoLevel3IsAltGr :: Bool
    -- ^ If 'True', then convert @Iso Level 3@ to @altGr@.
  , _wShiftDeactivatesCaps :: Bool
    -- ^ If 'True', then @Shift@ deactivates @CapsLock@.
    --   Note: this option is also configurable after installation in regional settings.
  , _wLrmRlm :: Bool
    -- ^ If 'True', then @LShift + Backspace@ inserts @U+200E@ and  @RShift + Backspace@ inserts @U+200F@.
  } deriving (Generic, NFData, Eq, Show)

instance ToJSON WindowsOptions where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 2))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 2))

instance FromJSON WindowsOptions where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 2))

--- Metadata ------------------------------------------------------------------

-- | Metadata of a keyboard layout
data LayoutMetadata = LayoutMetadata
  { _name :: RawLText              -- ^ Name of the keyboard
  , _systemName :: RawLText        -- ^ Simplified name of the layout for the operating system
  , _description :: Maybe RawLText -- ^ Description of the layout
  , _locales :: LayoutLocales      -- ^ Locales of the layout
  , _version :: Maybe Version      -- ^ Version of the layout
  , _publicationDate :: Maybe Day  -- ^ Date of publication
  , _author :: Maybe RawLText      -- ^ Author of the layout
  , _license :: Maybe RawLText     -- ^ License
  } deriving (Generic, NFData, Eq, Show)
  -- [TODO] Contact, license, etc.

-- Note: right-biased
instance Semigroup LayoutMetadata where
  (LayoutMetadata n1 s1 d1 lo1 v1 p1 a1 li1) <> (LayoutMetadata n2 s2 d2 lo2 v2 p2 a2 li2) =
    LayoutMetadata (n1 <> n2) (s1 <> s2) (d2 <|> d1) (lo1 <> lo2) (v2 <|> v1) (p2 <|> p1) (a1 <> a2) (li2 <|> li1)

instance Monoid LayoutMetadata where
  mempty = LayoutMetadata
    { _name = mempty
    , _systemName = mempty
    , _description = empty
    , _locales = mempty
    , _version = empty
    , _publicationDate = empty
    , _author = empty
    , _license = empty }

instance ToJSON LayoutMetadata where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 1))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 1))

instance FromJSON LayoutMetadata where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 1))

-- | Simplified name of the layout for the operating system
newtype SystemName = SystemName { getSystemName :: TL.Text }
  deriving stock (Generic, Show, Eq)
  deriving newtype (NFData, IsString)

instance ToJSON SystemName where
  toJSON     = Aeson.genericToJSON toJsonOptionsNewtype
  toEncoding = Aeson.genericToEncoding toJsonOptionsNewtype

instance FromJSON SystemName where
  parseJSON = Aeson.genericParseJSON toJsonOptionsNewtype

-- | Locales of the layout
type LayoutLocales = Locales (Set BCP47)

-- | A container for locale-dependent things
data Locales a = Locales
  { _locPrimary :: a   -- ^ Primary locales
  , _locSecondary :: a -- ^ Secondary locales
  , _locOther :: a     -- ^ Other locales
  } deriving stock (Generic, Eq, Functor, Foldable, Show)

instance NFData (Locales a) where
  rnf (Locales m s o) = m `seq` s `seq` o `seq` ()

instance (Semigroup a) => Semigroup (Locales a) where
  (Locales m1 s1 o1) <> (Locales m2 s2 o2) = Locales (m1 <> m2) (s1 <> s2) (o1 <> o2)

instance (Monoid a) => Monoid (Locales a) where
  mempty = Locales mempty mempty mempty

instance (ToJSON a) => ToJSON (Locales a) where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 4))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 4))

instance (FromJSON a) => FromJSON (Locales a) where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 4))

newtype Version = Version { unVersion :: V.Version }
  deriving stock (Generic)
  deriving newtype (NFData, Eq, Ord, Show)

instance IsString Version where
  fromString
    = either (error . errorBundlePretty) Version
    . V.version
    . fromString

instance ToJSON Version where
  toJSON     = Aeson.String . V.prettyVer . unVersion
  toEncoding = Aeson.text   . V.prettyVer . unVersion

instance FromJSON Version where
  parseJSON = Aeson.withText "version" $
    either (fail . errorBundlePretty) (pure . Version) . V.version
