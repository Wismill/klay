{-# LANGUAGE FlexibleInstances     #-}

module Klay.Keyboard.Layout.Action
  ( -- * Action
    Action(..)
  , IsAction(..)
  , SpecialAction(..)
  , ActionUpgrade
  , IsActionUpgrade(..)
  , ActionAlteration
  , IsActionAlteration(..)
  , MultiOsAction
  , LongPressActions(..)
  , toMonogram
  , specialActionToMonogram
  , isDefinedAction
  , isActiveModifier
  , prettyActionB

    -- ** Action label
  , ActionLabeler
  , CustomActionLabels
  , actionLabel
  , specialActionLabel
  , mkActionLabeler

    -- * Actions

    -- ** Normal Actions
  , IsActions(..)
  , Actions(..)
  , actionsOptions

  , ActionsAlteration
  , ActionsAlterations(..)
  , ModifiersOptionsAlteration
  , DefaultActionAlteration
  , LevelsActionsAlteration

  , MultiOsActions
  , MultiOsActionsAlteration
  , MultiOsActionsAlteration'
  , MultiOsActionsAlterations

    -- ** Resolved Actions
  , ResolvedActions
  , ResolvedAction
  , ResolvedActionsAlteration
  , ResolvedActionsAlterations
  , ResolvedActionsDelta
  , ResolvedActionsChanges

  , MultiOsResolvedActions

    -- ** Auto Actions
  , TAutoActions(..)
  , AutoActions
  , AutoActionsAlteration
  , AutoActionsAlterations

  , MultiOsAutoActions

    -- ** Raw Actions
  , CustomActions(..)
  , RawActions
  , RawActionsAlteration
  , RawActionsAlterations
  , RawActionsDelta
  , RawActionsChanges

  , IsMultiOsActions(..)
  , TMultiOsActions(..)
  , MultiOsRawActions
  , MultiOsRawActionsAlteration
  , MultiOsRawActionsAlteration'
  , MultiOsRawActionsAlterations
  , multiOsActions
  , multiOsActionsWithDefault
  , toMultiOsRawActionsMap
  , toMultiOsRawActionsAlteration
  , toMultiOsRawActionsAlterationMap
  , toOsRawActionsAlteration
  , toOsRawActionsAlterationMap
  , mapMultiOsActions
  , mapMultiOsActionsWithOs
  , insertOrUpdate1d
  , forceCustomActions
  , forceMultiosCustomActions

    -- ** Levels action map
  , Level
  , LevelsActions
  , NELevelsActions

  , fromActionsAlterations
  , toActionsAlterations
  , untagCustomisableActions

  , normaliseActions
  , mapActions
  , mapActionsLevels
  , mbimapActions
  , foldlActions
  , foldlImplicitActions
  , foldrActions
  , foldlActionsM
  , foldrActionsM
  , traverseActions
  , traverseMaybeActions
  , bitraverseMaybeActions

  , multiOsRawActionsToResolvedActions
  , multiOsRawActionsToRawActions
  , rawActionsToActions

    -- * Actions map
  , MultiOsRawActionsMap
  , MultiOsRawActionsKeyMap
  , MultiOsRawActionsKeyMapAlteration
  , MultiOsRawActionsKeyMapAlteration'
  , MultiOsRawActionsKeyMapAlterations
  , MultiOsRawActionsKeyMapDelta

  , MultiOsAutoActionsMap
  , MultiOsAutoActionsKeyMap

  , MultiOsActionsMap
  , MultiOsActionsKeyMap

  , RawActionsMap
  , RawActionsKeyMap
  , RawActionsKeyMapAlteration
  , RawActionsKeyMapDelta

  , AutoActionsMap
  , AutoActionsMapUpdate
  , AutoActionsKeyMap
  , AutoActionsKeyMapUpdate

  , ResolvedActionsMap
  , ResolvedActionsKeyMap
  , ResolvedActionsKeyMapAlteration
  , ResolvedActionsKeyMapDelta

  , ActionsMap
  , ActionsKeyMap
  , ActionsKeyMapAlteration
  , ActionsKeyMapAlterations

  , ActionsMapUpdate
  , ActionsKeyMapUpdate

  , Layers
  , KeyLayers

  , Layer
  , KeyLayer

    -- * Default actions
  , defaultActions
  , mDefaultActions
  , defaultSystemAction
  , resolveModifiersOptions
  , resolveModifiersOptionsLax
  , autoDiscardModifiersOptions
  , noResolveModifiersOptions
  , checkDefaultActions
  , checkMultiOsDefaultActions
    -- ** Blank Layout
  , multiOsBlankActionMap
  , mkMultiOsBlankActionMap
  , osIndependentBlankActionMap
  , mkOsIndependentBlankActionMap
  , linuxBlankActionMap
  , mkLinuxSpecificBlankActionMap_update
  , windowsBlankActionMap
  , mkWindowsSpecificBlankActionMapUpdate
    -- ** US Layout
  , multiOsUsActionMap
  , make_multiOsUsActionMap
  , osIndependentUsActionMap
  , makeOsIndependentUsActionMap
  , makeLinuxSpecificUsActionMapUpdate
  , mkWindowsSpecificUsActionMapUpdate

    -- ** Creation of an action map
  , osDefault
  , osDefaults
  , pattern DefaultCapsLockBehaviour
  , pattern DefaultAutoModifiersOptions
  , custom
  , customAuto
  , customManual
  , singleton
  , rawSingleton
  , rawUndefinedActions
  , rawNoActions
  , multiOsRawSingleton
  , mkActions
  , mkActionsAlterations
  , (.=)
  , (⏵)
  , (+>)
  , (⇒)
  , (⇨)

  -- ** Miscellaneous
  , actionsList
  , implicitActionsList
  , actionsByLevel
  , implicitActionsByLevel
  , actionsByMaybeLevel
  , actionAtLevel
  , implicitActionAtLevel
  , definedLevels
  , getLayers
  , getLayer
  , getImplicitLayer
  , getBaseLayer
  , actionAtKeyLevel
  ) where

import Klay.Keyboard.Layout.Action.Action
import Klay.Keyboard.Layout.Action.Actions
import Klay.Keyboard.Layout.Action.Auto.ModifiersOptions
import Klay.Keyboard.Layout.Action.Default
import Klay.Keyboard.Layout.Action.Map
import Klay.Keyboard.Layout.Action.MultiOs
import Klay.Keyboard.Layout.Action.Raw
import Klay.Keyboard.Layout.Action.Special
import Klay.Keyboard.Layout.Action.Label
