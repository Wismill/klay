{-# LANGUAGE OverloadedLists       #-}

module Klay.Keyboard.Layout.Symbols
  ( ReachableResults(..)
  , BaseResults
  , DeadKeyResults
  , reachableCharacters
  , reachableSymbols
  , groupReachableSymbols
  , groupByUnicodeBlock
  , groupByUnicodeScript
  , onlyChars
  , onlyDeadKeys
    -- * Re-export
  , KeySequence
  , KeyModCombo(..)
  ) where

import Data.Text.Lazy qualified as TL
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Foldable
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.List.NonEmpty (NonEmpty)

import Klay.Keyboard.Hardware.Key (KeySequence, KeyModCombo(..))
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier (maskToModifiers)
import Klay.OS (OsSpecific)
import Klay.Utils.Unicode.Blocks
import Klay.Utils.Unicode.Scripts
import Klay.Utils.UserInput (RawText(..))

type BaseResults = Map Result (Set KeySequence)
type DeadKeyResults = Map Result (Map DeadKey (Set (NonEmpty Symbol)))

-- | 'Result'ing characters, strings and dead keys reachable with a layout
data ReachableResults = ReachableResults
  { _rByDirectInput :: BaseResults -- ^ Symbols reachable /without/ dead keys
  , _rByDeadKey :: DeadKeyResults  -- ^ Symbols reachable /with/ dead keys
  } deriving (Eq)

instance Semigroup ReachableResults where
  (ReachableResults bs1 ds1) <> (ReachableResults bs2 ds2) = ReachableResults (Map.unionWith (<>) bs1 bs2) (Map.unionWith (<>) ds1 ds2)

instance Monoid ReachableResults where
  mempty = ReachableResults mempty mempty

-- | Get all the supported characters of a layout
reachableCharacters :: OsSpecific -> MultiOsRawLayout -> Set Char
reachableCharacters os l =
  let (ReachableResults bs ds) = reachableSymbols os l
  in foldl' only_chars mempty (Map.keys bs <> Map.keys ds)
  where
    only_chars acc (RChar c) = Set.insert c acc
    only_chars acc _         = acc

-- | Get all the supported results of a layout
reachableSymbols :: OsSpecific -> MultiOsRawLayout -> ReachableResults
reachableSymbols os Layout{_groups=gs, _deadKeys=dksDef} =
  -- foldl_groups_layouts_with_index
  foldl' (\acc g -> acc <> groupReachableSymbols dksDef g) mempty . resolveFinalGroups os $ gs

-- | Get the reachable characters\/character sequences\/dead keys within a group
groupReachableSymbols :: DeadKeyDefinitions -> KeyFinalGroup -> ReachableResults
groupReachableSymbols dksDef g = ReachableResults
  { _rByDirectInput = base_results
  , _rByDeadKey = dk_results
  }
  where
    base_results :: BaseResults
    base_results = foldlGroupMappingActionsWithKey process_action mempty g

    dk_results :: DeadKeyResults
    dk_results = processDeadKeys base_results

    process_action :: BaseResults -> Key -> ModifiersOptions -> Level -> Action -> BaseResults
    process_action acc vk _ l a = maybe acc (\r -> Map.insertWith (<>) r s acc) mr
      where
        s = [[KeyModCombo (maskToModifiers l) vk]]
        mr = case a of
          AChar c     -> Just $ RChar c
          AText t     -> Just $ RText (TL.toStrict . getRawText $ t) -- [FIXME]
          ADeadKey dk -> Just $ RDeadKey dk
          _           -> Nothing -- [TODO] "SpecialAction" may produce characters on some platforms

    dkm = _dkMergedDefinitions $ flattenDeadKeyDefinitions FlattenThenKeepChainedDK dksDef

    processDeadKeys :: BaseResults -> DeadKeyResults
    processDeadKeys rs = foldl' (processDeadKeys' rs) mempty (Map.keys rs)

    processDeadKeys' :: BaseResults -> DeadKeyResults -> Result -> DeadKeyResults
    processDeadKeys' bs acc (RDeadKey dk) = case Map.lookup dk dkm of
      Nothing -> acc
      Just combos -> foldl' process_combo acc combos
      where
        process_combo acc' (DeadKeyCombo symbols r) =
          if all isReachable symbols
            then Map.insertWith (<>) r [(dk, [symbols])] acc'
            else acc'
        isReachable (SChar c) = Map.member (RChar c) bs
        isReachable (SDeadKey dk') = Map.member (RDeadKey dk') bs
    processDeadKeys' _ acc _ = acc -- Not a dead key

-- | Group characters mapping by Unicode block
groupByUnicodeBlock :: Map Char a -> Map UnicodeBlock (Map Char a)
groupByUnicodeBlock = Map.foldlWithKey' mkBlocks mempty
  where
    mkBlocks acc c a = Map.insertWith Map.union (unicodeBlock c) [(c, a)] acc

-- | Group characters mapping by Unicode script
groupByUnicodeScript :: Map Char a -> Map UnicodeScript (Map Char a)
groupByUnicodeScript = Map.foldlWithKey' mkScripts mempty
  where
    mkScripts acc c a = Map.insertWith Map.union (unicodeScript c) [(c, a)] acc

-- | Keep only chars
onlyChars :: Map Result a -> Map Char a
onlyChars = filterResults f
  where f (RChar c) = Just c
        f _         = Nothing

-- | Keep only dead keys
onlyDeadKeys :: Map Result a -> Map DeadKey a
onlyDeadKeys = filterResults f
  where f (RDeadKey dk) = Just dk
        f _             = Nothing

filterResults :: Ord r => (Result -> Maybe r) -> Map Result a -> Map r a
filterResults f = Map.foldlWithKey' go mempty
  where
    go acc k v = case f k of
      Nothing -> acc
      Just k' -> Map.insert k' v acc
