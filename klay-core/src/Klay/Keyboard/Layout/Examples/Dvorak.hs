{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Dvorak
  ( layout
  , group
  , permutations
  , permute
  , mkGroup
  , mkActionMap
  ) where

import Data.Maybe (fromMaybe)
import Data.Map qualified as Map

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (MultiOsRawActionsKeyMap)
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Group
  ( KeyMultiOsRawGroup, GroupIndex(FirstGroup)
  )
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify, permutationToMap )


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods = [ Mod metadata_mod, Mod groups_mod]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata US.layout)
      { _name = "Dvorak"
      , _systemName = "Dvorak" }
    groups_mod :: MultiOsRawGroupsModification
    groups_mod = GroupMods FirstGroup groupMods

actionMapMod :: MultiOsRawActionMapModification
actionMapMod = KeyPermutations permutations

permutations :: [[K.Key]]
permutations =
  [ [ K.Q, K.Quote, K.Minus, K.LBracket, K.Slash, K.Z
    , K.Semicolon, K.S, K.O, K.R, K.P, K.L, K.N, K.B, K.X ]
  , [ K.W, K.Comma]
  , [ K.E, K.Period, K.V, K.K, K.T, K.Y, K.F
    , K.U, K.G, K.I, K.C, K.J, K.H, K.D ]
  , [ K.Equals, K.RBracket ]
  ]

permutationMap :: Map.Map K.Key K.Key
permutationMap = fromMaybe mempty (foldMap permutationToMap permutations)

permute :: K.Key -> K.Key
permute key = Map.findWithDefault key key permutationMap

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

groupMods :: [MultiOsRawGroupModification]
groupMods =
  [ ReplaceGroupName "Dvorak"
  , KeyMapMod actionMapMod
  ]

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = gmodify groupMods $ US.mkGroup opt l1 l2 l3 l4

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 = amodify actionMapMod $ US.mkActionMap opt l1 l2 l3 l4
