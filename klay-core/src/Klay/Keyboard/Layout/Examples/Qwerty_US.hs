{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Qwerty_US
  ( layout
  , group
  , mkGroup
  , actionMap
  , mkActionMap
  ) where

import Data.Map.Strict qualified as Map

import Country.Identifier (unitedStatesOfAmerica)
import Data.BCP47 (mkLocalized)
import Data.LanguageCodes (ISO639_1(EN))

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
  (MultiOsRawLayout, Layout(..), LayoutMetadata(..), Locales(..), defaultOptions)
import Klay.Keyboard.Layout.Action
  ( MultiOsRawActions, MultiOsRawActionsKeyMap, MultiOsAutoActions
  , CustomActions(..), Level
  , make_multiOsUsActionMap
  , (⇒), (.=) )
import Klay.Keyboard.Layout.Group
  (Groups(OneGroup), KeyMultiOsRawGroup, mkRawGroup)
import Klay.Keyboard.Layout.Level qualified as L
import Klay.Keyboard.Layout.Modifier
  ( CapsLockBehaviour(..)
  , control, isoLevel1, isoLevel2, isoLevel3, isoLevel4)
import Klay.OS
  (MultiOs(..), pattern TLinux, pattern TWindows)
import Data.Alterable (Magma(..))


layout :: MultiOsRawLayout
layout = Layout
  { _metadata = mempty
    { _name = "US"
    , _systemName = "us_klay"
    , _locales = mempty{_locPrimary=[mkLocalized EN unitedStatesOfAmerica]} }
  , _dualFunctionKeys = mempty
  , _deadKeys = mempty
  , _groups = OneGroup group
  , _options = defaultOptions
  }

group :: KeyMultiOsRawGroup
group = mkGroup DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = mkRawGroup "English" levels ams
  where
    levels = MultiOs
      { _osIndependent = osIndependentLevels
      , _linux = linuxLevels
      , _windows = windowsLevels
      }
    osIndependentLevels =
      [ (l1, L.defaultAlphanumericLower)
      , (l2, L.defaultAlphanumericUpper)
      ]
    linuxLevels = osIndependentLevels
      -- <> [ (control, L.defaultControlLevel)
      --    , (alternate, L.defaultAlternateLevel) ]
    windowsLevels = osIndependentLevels
      <> [ (control, L.defaultControlLevel) ]
    ams = mkActionMap opt l1 l2 l3 l4

actionMap :: MultiOsRawActionsKeyMap
actionMap = mkActionMap DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 =
  Map.mapWithKey go (make_multiOsUsActionMap opt l1 l2 l3 l4) +> xtra
  where
    is_default_config
      = opt == DefaultCapsLockBehaviour
      && l1 == isoLevel1
      && l2 == isoLevel2
      && l3 == isoLevel3
      && l4 == isoLevel4
    go :: K.Key -> MultiOsAutoActions -> MultiOsRawActions
    go | is_default_config = const (fmap ADefault)
       | otherwise = \key -> fmap if K.isAlphanumericIso105 key then ACustom else ADefault
    xtra :: MultiOsRawActionsKeyMap
    xtra =
      [ K.Iso102    ⇒ [ TLinux   [l1 .= '<' , l2 .= '>', l3 .= '|', l4 .= '¦']
                      , TWindows [l1 .= '\\', l2 .= '|', control .= '\x001c'] ]
      -- Control sequences
      , K.LBracket  ⇒ [TWindows [control .= '\x001b']] -- Escape
      , K.RBracket  ⇒ [TWindows [control .= '\x001d']] -- Information Separator Three
      , K.Backslash ⇒ [TWindows [control .= '\x001c']] -- Information Separator Four
      ]
