{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Latin.Type4
  ( layout
  , Latin.deadKeysDefinition
  , group
  , mkGroup
  , mkActionMap
  , usActionMapUpdate
  , usMultiOsActionMapUpdate
  ) where

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  ( toMultiOsRawActionsAlterationMap
  , MultiOsRawActionsKeyMap, MultiOsRawActionsKeyMapAlteration, RawActionsKeyMapAlteration
  , (⏵), (+>))
import Klay.Keyboard.Layout.Group (KeyMultiOsRawGroup, GroupIndex(FirstGroup))
import Klay.Keyboard.Layout.Examples.Latin qualified as Latin
import Klay.Keyboard.Layout.Modifier (shift, isoLevel3)
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )
import Klay.Keyboard.Hardware.Key qualified as K

layout :: MultiOsRawLayout
layout = lmodify mods Latin.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods = [Mod metadata_mod, Mod groups_mod]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata Latin.layout)
      { _name = "Latin (type 4)"
      , _systemName = "latin4" }
    groups_mod :: MultiOsRawGroupsModification
    groups_mod =
      let ms = groupMods DefaultCapsLockBehaviour mempty shift isoLevel3 (isoLevel3 <> shift)
      in GroupMods FirstGroup ms

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4
  = gmodify (groupMods opt l1 l2 l3 l4)
  $ Latin.mkGroup opt l1 l2 l3 l4

groupMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawGroupModification]
groupMods opt l1 l2 l3 l4 =
  [ ReplaceGroupName "Latin"
  , KeyMapMod $ keymapMods opt l1 l2 l3 l4
  ]

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4
  = amodify (keymapMods opt l1 l2 l3 l4)
    (Latin.mkActionMap opt l1 l2 l3 l4)

keymapMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionMapModification
keymapMods opt l1 l2 l3 l4 = KeyMapUpdate $ multiOsActionMapUpdate opt l1 l2 l3 l4

usMultiOsActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMapAlteration
usMultiOsActionMapUpdate opt l1 l2 l3 l4 =
  Latin.usMultiOsActionMapUpdate opt l1 l2 l3 l4 +> multiOsActionMapUpdate opt l1 l2 l3 l4

usActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> RawActionsKeyMapAlteration
usActionMapUpdate opt l1 l2 l3 l4 =
  Latin.usActionMapUpdate opt l1 l2 l3 l4 +> actionMapUpdate opt l1 l2 l3 l4

multiOsActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMapAlteration
multiOsActionMapUpdate opt l1 l2 l3 l4 =
  toMultiOsRawActionsAlterationMap $ actionMapUpdate opt l1 l2 l3 l4

actionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> RawActionsKeyMapAlteration
actionMapUpdate _opt l1 l2 l3 l4 =
  [ (K.N2, [l2 ⏵ '"', l3 ⏵ '@'])
  , (K.N6, [l2 ⏵ '&', l3 ⏵ '¬'])
  , (K.N7, [l2 ⏵ '/'])
  , (K.N8, [l2 ⏵ '('])
  , (K.N9, [l2 ⏵ ')'])
  , (K.N0, [l2 ⏵ '='])

  , (K.E, [l3 ⏵ '€', l4 ⏵ '¢'])

  , (K.Comma , [l2 ⏵ ';'])
  , (K.Period, [l2 ⏵ ':'])
  , (K.Slash , [l1 ⏵ '-', l2 ⏵ '_'])
  ]
