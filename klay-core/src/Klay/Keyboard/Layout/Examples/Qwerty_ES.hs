{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Qwerty_ES
  ( layout
  , deadKeysDefinition
  , group
  , vkUpdate
  , mkGroup
  , mkActionMap
  ) where

import Country.Identifier (spain)
import Data.BCP47 (mkLocalized)
import Data.LanguageCodes (ISO639_1(ES))

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  ( MultiOsRawActionsKeyMap, SpecialAction(KP_Separator)
  , (⇨), (⏵), rawSingleton, toOsRawActionsAlterationMap )
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Group
  (KeyMultiOsRawGroup, GroupIndex(FirstGroup))
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Latin.Type4 qualified as Latin4
import Klay.Keyboard.Layout.Level qualified as L
import Klay.Keyboard.Layout.Modifier
  ( control, isoLevel1, isoLevel2, isoLevel3, isoLevel4 )
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , WindowsOptionsModification(..), DeadKeysModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.OS (linux, windows)
import Data.Alterable (Magma(..), Alteration(..))

layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods =
      [ metadata_mod
      , Mod $ DKDefReplace deadKeysDefinition
      , Mod opt_mods
      , Mod groups_mod]
    metadata_mod = Mod (ReplaceMetadata metadata)
    metadata = (_metadata US.layout)
      { _name = "ES"
      , _systemName = "es_klay"
      , _locales = mempty{_locPrimary=[mkLocalized ES spain]} }
    opt_mods :: [WindowsOptionsModification]
    opt_mods =
      [ WinAltGr True
      -- , VirtualKeysDefinition (KeyPermutations permutations)
      , VirtualKeysDefinition (KeyMapUpdate vkUpdate)
      , WinIsoLevel3IsAltGr True
      ]
    groups_mod :: MultiOsRawGroupsModification
    groups_mod =
      let ms = groupMods DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4
      in GroupMods FirstGroup ms

deadKeysDefinition :: RawDeadKeyDefinitions
deadKeysDefinition = mconcat
  [ DK.diacriticsDef
  , DK.diacriticsExtraDef ]

vkUpdate :: VirtualKeysDefinition
vkUpdate =
  [ (K.Tilde    , VK.VK_OEM_5)
  , (K.Minus    , VK.VK_OEM_4)
  , (K.Plus     , VK.VK_OEM_6)
  , (K.LBracket , VK.VK_OEM_1)
  , (K.RBracket , VK.VK_OEM_PLUS)
  , (K.Semicolon, VK.VK_OEM_3)
  , (K.Backslash, VK.VK_OEM_2)
  , (K.Slash    , VK.VK_OEM_MINUS)
  ]

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4
  = gmodify (groupMods opt l1 l2 l3 l4)
  $ US.mkGroup opt l1 l2 l3 l4

groupMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawGroupModification]
groupMods opt l1 l2 l3 l4 =
  [ ReplaceGroupName "Español"
  , AddLevels [(l3, L.defaultIsoLevel3)]
  , AddOsLevels linux [(l4, L.defaultIsoLevel4)]
  , KeyMapMod (keymapMods opt l1 l2 l3 l4)
  ]

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4
  = amodify (keymapMods opt l1 l2 l3 l4)
  $ US.mkActionMap opt l1 l2 l3 l4

keymapMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionMapModification
keymapMods opt l1 l2 l3 l4 = KeyMapUpdate $
  [ K.Tilde ⇨ [l1 ⏵ 'º', l2 ⏵ 'ª', l3 ⏵ '\\']
  , K.N1    ⇨ [l2 ⏵ '!', l3 ⏵ '|']
  , K.N2    ⇨ [l2 ⏵ '"', l3 ⏵ '@']
  , K.N3    ⇨ [l2 ⏵ '·', l3 ⏵ '#']
  , K.N4    ⇨ [l2 ⏵ '$', l3 ⏵ DK.tildeAbove]
  , K.N5    ⇨ [l2 ⏵ '%', l3 ⏵ '€']
  , K.N6    ⇨ [l2 ⏵ '&', l3 ⏵ '¬']
  , K.N7    ⇨ [l2 ⏵ '/']
  , K.N8    ⇨ [l2 ⏵ '(']
  , K.N9    ⇨ [l2 ⏵ ')']
  , K.N0    ⇨ [l2 ⏵ '=']
  , K.Minus ⇨ [l1 ⏵ '\'', l2 ⏵ '?']
  , K.Plus  ⇨ [l1 ⏵ '¡', l2 ⏵ '¿']

  , K.E ⇨ [l3 ⏵ '€']
  , K.LBracket ⇨ [l1 ⏵ DK.graveAbove, l2 ⏵ DK.circumflexAbove, l3 ⏵ '[']
  , K.RBracket ⇨ [l1 ⏵ '+', l2 ⏵ '*', l3 ⏵ ']']

  , K.Semicolon ⇨ [l1 ⏵ 'ñ', l2 ⏵ 'Ñ']
  , K.Quote ⇨ [l1 ⏵ DK.acuteAbove, l2 ⏵ DK.diaeresisAbove, l3 ⏵ '{']
  , K.Backslash ⇨ [l1 ⏵ 'ç', l2 ⏵ 'Ç', l3 ⏵ '}']

  , K.Iso102 ⇨ [l1 ⏵ '<', l2 ⏵ '>']
  , K.Comma  ⇨ [l1 ⏵ ',', l2 ⏵ ';']
  , K.Period ⇨ [l1 ⏵ '.', l2 ⏵ ':']
  , K.Slash  ⇨ [l1 ⏵ '-', l2 ⏵ '_']

  , K.RAlternate ⇨ InsertOrReplace (rawSingleton (AModifier IsoLevel3Set))
  , K.KPDecimal  ⇨ [l1 ⏵ KP_Separator, l2 ⏵ KP_Separator]
  ] +>

  toOsRawActionsAlterationMap linux
    (   Latin4.usActionMapUpdate opt l1 l2 l3 l4
    +>  [ (K.Tilde    , [l1 ⏵ 'º', l2 ⏵ 'ª', l3 ⏵ '\\', l4 ⏵ '\\'])
        , (K.N1       , [l3 ⏵ '|'])
        , (K.N3       , [l2 ⏵ '·', l3 ⏵ '#'])
        , (K.N4       , [l3 ⏵ DK.tildeAbove])
        , (K.Minus    , [l1 ⏵ '\'', l2 ⏵ '?'])
        , (K.Plus     , [l1 ⏵ '¡', l2 ⏵ '¿', l3 ⏵ DK.tildeAbove, l4 ⏵ '~'])

        , (K.LBracket , [l1 ⏵ DK.graveAbove, l2 ⏵ DK.circumflexAbove, l3 ⏵ '[', l4 ⏵ DK.ringAbove])
        , (K.RBracket , [l1 ⏵ '+', l2 ⏵ '*', l3 ⏵ ']', l4 ⏵ DK.macronAbove])

        , (K.Semicolon, [l1 ⏵ 'ñ', l2 ⏵ 'Ñ', l3 ⏵ '~'])
        , (K.Quote    , [l1 ⏵ DK.acuteAbove, l2 ⏵ DK.diaeresisAbove, l3 ⏵ '{', l4 ⏵ '{'])
        , (K.Backslash, [l1 ⏵ 'ç', l2 ⏵ 'Ç', l3 ⏵ '}'])
        ]
    ) +>

  toOsRawActionsAlterationMap windows
    [ (K.Iso102, [l1 ⏵ '<', l2 ⏵ '>', control ⏵ '\x001c'])
    , (K.N4    , [l3 ⏵ DK.tildeAbove])
    , (K.N5    , [l3 ⏵ '€'])
    ]
