{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Qwertz_DE
  ( layout
  , deadKeysDefinition
  , permutation
  , ES.vkUpdate
  , group
  , mkGroup
  , mkActionMap
  ) where

import Data.BCP47 (mkLocalized)
import Data.LanguageCodes (ISO639_1(DE))
import Country.Identifier (germany)

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  ( ActionUpgrade, MultiOsRawActionsKeyMap, SpecialAction(KP_Separator)
  , (⇨), (⏵), rawSingleton, toOsRawActionsAlterationMap )
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Group ( KeyMultiOsRawGroup, GroupIndex(FirstGroup) )
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Latin.Type4 qualified as Latin4
import Klay.Keyboard.Layout.Level qualified as L
import Klay.Keyboard.Layout.Modifier
  (control, isoLevel1, isoLevel2, isoLevel3, isoLevel4, capitals)
  -- [TODO] fourLevelWithLock, makeFourLevelSemiAlphabetic)
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , WindowsOptionsModification(..), DeadKeysModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )
import Klay.OS (windows, linux)
import Data.Alterable (Alteration(..), Upgrade(Reset), Magma(..))


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods =
      [ metadata_mod
      , Mod $ DKDefReplace deadKeysDefinition
      , Mod opt_mods
      , groups_mod]
    metadata_mod = Mod (ReplaceMetadata metadata)
    metadata = (_metadata US.layout)
      { _name = "DE"
      , _systemName = "de_klay"
      , _locales = mempty{_locPrimary=[mkLocalized DE germany]} }
    opt_mods :: [WindowsOptionsModification]
    opt_mods =
      [ WinAltGr True
      , VirtualKeysDefinition (KeyMapUpdate ES.vkUpdate)
      , WinIsoLevel3IsAltGr True
      ]
    groups_mod =
      let ms = groupMods DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4
      in Mod (GroupMods FirstGroup ms :: MultiOsRawGroupsModification)

permutation :: [K.Key]
permutation = [K.Y, K.Z]

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

groupMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawGroupModification]
groupMods opt l1 l2 l3 l4 =
  [ ReplaceGroupName "Deutsch"
  , AddLevels [(l3, L.defaultIsoLevel3), (l4, L.defaultIsoLevel4)]
  , AddOsLevels linux [(l1 <> capitals, L.defaultAlphanumericCaps)]
  , KeyMapMods (keymapMods opt l1 l2 l3 l4)
  ]

keymapMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawActionMapModification]
keymapMods opt l1 l2 l3 l4 =
  [ KeyPermutation permutation
  , KeyMapUpdate $
    -- Reference: DIN 2137-1:2012-06
    [ K.Tilde ⇨ [l1 ⏵ DK.circumflexAbove, l2 ⏵ '°']
    , K.N1    ⇨ [l2 ⏵ '!']
    , K.N2    ⇨ [l2 ⏵ '"', l3 ⏵ '²']
    , K.N3    ⇨ [l2 ⏵ '§', l3 ⏵ '³']
    , K.N4    ⇨ [l2 ⏵ '$']
    , K.N5    ⇨ [l2 ⏵ '%']
    , K.N6    ⇨ [l2 ⏵ '&']
    , K.N7    ⇨ [l2 ⏵ '/', l3 ⏵ '{']
    , K.N8    ⇨ [l2 ⏵ '(', l3 ⏵ '[']
    , K.N9    ⇨ [l2 ⏵ ')', l3 ⏵ ']']
    , K.N0    ⇨ [l2 ⏵ '=', l3 ⏵ '}']
    , K.Minus ⇨ [l1 ⏵ 'ß', l2 ⏵ '?', l3 ⏵ '\\', l4 ⏵ 'ẞ']
    , K.Plus  ⇨ [l1 ⏵ DK.acuteAbove, l2 ⏵ DK.graveAbove]

    , K.Q        ⇨ [l3 ⏵ '@']
    , K.E        ⇨ [l3 ⏵ '€']
    , K.LBracket ⇨ [l1 ⏵ 'ü', l2 ⏵ 'Ü']
    , K.RBracket ⇨ [l1 ⏵ '+', l2 ⏵ '*', l3 ⏵ '~']

    , K.Semicolon ⇨ [l1 ⏵ 'ö', l2 ⏵ 'Ö']
    , K.Quote     ⇨ [l1 ⏵ 'ä', l2 ⏵ 'Ä']
    , K.Backslash ⇨ [l1 ⏵ '#', l2 ⏵ '\'', l3 ⏵ UndefinedAction]

    , K.Iso102 ⇨ [l1 ⏵ '<', l2 ⏵ '>', l3 ⏵ '|']
    , K.M      ⇨ [l3 ⏵ 'µ']
    , K.Comma  ⇨ [l1 ⏵ ',', l2 ⏵ ';']
    , K.Period ⇨ [l1 ⏵ '.', l2 ⏵ ':']
    , K.Slash  ⇨ [l1 ⏵ '-', l2 ⏵ '_']

    , K.RAlternate ⇨ InsertOrReplace (rawSingleton (AModifier IsoLevel3Set))
    , K.KPDecimal  ⇨ [l1 ⏵ KP_Separator, l2 ⏵ KP_Separator]
    ] +>

    toOsRawActionsAlterationMap linux
      (   Latin4.usActionMapUpdate opt l1 l2 l3 l4
      +>  [ (K.Tilde, [l1 ⏵ DK.circumflexAbove, l2 ⏵ '°', l3 ⏵ '′', l4 ⏵ '″'])
          , (K.N2   , [l3 ⏵ '²'])
          , (K.N3   , [l2 ⏵ '§'])
          , (K.N4   , [l4 ⏵ '¤'])
          , (K.Minus, [l3 ⏵ '\\', (l1 <> capitals) ⏵ 'ẞ']) -- !> fourLevelWithLock
          , (K.Plus , [l1 ⏵ DK.acuteAbove, l2 ⏵ DK.graveAbove, l3 ⏵ DK.cedilla, l4 ⏵ DK.ogonek])
          --
          , (K.E       , [l4 ⏵ '€'])
          , (K.Y       , [l3 ⏵ '←', l4 ⏵ '¥'])
          , (K.RBracket, [l3 ⏵ '~', l4 ⏵ '\x00af'])
          --
          , (K.S        , [l3 ⏵ 'ſ', l4 ⏵ 'ẞ'])
          , (K.J        , [l3 ⏵ DK.dotBelow, l4 ⏵ DK.dotAbove])
          , (K.Semicolon, [l3 ⏵ DK.doubledAcute, l4 ⏵ DK.dotBelow])
          , (K.Backslash, [l3 ⏵ '’', l4 ⏵ DK.breve])
          --
          , (K.Iso102, [l4 ⏵ DK.macronBelow])
          , (K.Z     , [l3 ⏵ '»', l4 ⏵ '›'])
          , (K.X     , [l3 ⏵ '«', l4 ⏵ '‹'])
          , (K.V     , [l3 ⏵ '„', l4 ⏵ '‚'])
          , (K.B     , [l3 ⏵ '“', l4 ⏵ '‘'])
          , (K.N     , [l3 ⏵ '”', l4 ⏵ '’'])
          , (K.Comma , [l3 ⏵ '·']) -- [FIXME]
          , (K.Period, [l3 ⏵ '…'])
          , (K.Slash , [l3 ⏵ '\x2013', l4 ⏵ '\x2014']) -- – —
          --
          -- [TODO] K.KPDecimal
          ]
      ) +>

    toOsRawActionsAlterationMap windows
      [ (K.Iso102, [l1 ⏵ '<', l2 ⏵ '>', control ⏵ (Reset :: ActionUpgrade)])
      ]
  ]

deadKeysDefinition :: RawDeadKeyDefinitions
deadKeysDefinition = ES.deadKeysDefinition

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = gmodify (groupMods opt l1 l2 l3 l4) $ US.mkGroup opt l1 l2 l3 l4

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 = amodify (keymapMods opt l1 l2 l3 l4) $ US.mkActionMap opt l1 l2 l3 l4
