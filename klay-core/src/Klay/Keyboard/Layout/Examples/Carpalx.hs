{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Carpalx
  ( layout
  , group
  , permutations
  , mkGroup
  , mkActionMap
  ) where


import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (MultiOsRawActionsKeyMap)
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Group
  ( KeyMultiOsRawGroup, GroupIndex(FirstGroup)
  )
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods = [ Mod metadata_mod, Mod groups_mod]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata US.layout)
      { _name = "Carpalx (QGMLWB)"
      , _systemName = "carpalx"
      , _description = Just "http://mkweb.bcgsc.ca/carpalx/"
      , _license = Just "CC BY-NC-SA 4.0" }
    groups_mod :: MultiOsRawGroupsModification
    groups_mod = GroupMods FirstGroup groupMods

permutations :: MultiOsRawActionMapModification
permutations = KeyPermutations
  [ [ K.W, K.G, K.R, K.L, K.O, K.V, K.F, K.N, K.K, K.E, K.M, K.P
    , K.Semicolon, K.H, K.I, K.U, K.Y, K.B, K.J, K.A, K.D, K.T ]
  ]

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

groupMods :: [MultiOsRawGroupModification]
groupMods =
  [ ReplaceGroupName "Carpalx"
  , KeyMapMod permutations
  ]

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = gmodify groupMods $ US.mkGroup opt l1 l2 l3 l4

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 = amodify permutations $ US.mkActionMap opt l1 l2 l3 l4
