{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Colemak
  ( layout
  , group
  , permutations
  , mkGroup
  , mkActionMap
  ) where


import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (MultiOsRawActionsKeyMap)
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Group
  ( KeyMultiOsRawGroup, GroupIndex(FirstGroup)
  )
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods = [ Mod metadata_mod, Mod groups_mod]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata US.layout)
      { _name = "Colemak"
      , _systemName = "colemak" }
    groups_mod :: MultiOsRawGroupsModification
    groups_mod = GroupMods FirstGroup groupMods

actionMapMod :: MultiOsRawActionMapModification
actionMapMod = KeyPermutations permutations

permutations :: [[K.Key]]
permutations =
  [ [K.E, K.F, K.T, K.G, K.D, K.S, K.R, K.P, K.Semicolon, K.O, K.Y, K.J, K.N, K.K]
  , [K.U, K.L, K.I]
  ]

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

groupMods :: [MultiOsRawGroupModification]
groupMods =
  [ ReplaceGroupName "Colemak"
  , KeyMapMod actionMapMod
  ]

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = gmodify groupMods $ US.mkGroup opt l1 l2 l3 l4

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 = amodify actionMapMod $ US.mkActionMap opt l1 l2 l3 l4
