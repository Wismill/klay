{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Latin
  ( layout
  , deadKeysDefinition
  , group
  , mkGroup
  , mkActionMap
  , usMultiOsActionMapUpdate
  , usActionMapUpdate
  ) where

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  ( MultiOsRawActionsKeyMap, MultiOsRawActionsKeyMapAlteration, RawActionsKeyMapAlteration
  , SpecialAction(KP_Separator)
  , toMultiOsRawActionsAlterationMap, rawSingleton, (⏵))
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Group
  (KeyMultiOsRawGroup, GroupIndex(FirstGroup))
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Modifier
  ( shift, isoLevel3, isoLevel4 )
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , DeadKeysModification(..), WindowsOptionsModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..)
  , lmodify, gmodify )
import Klay.Keyboard.Hardware.Key qualified as K
import Data.Alterable (Alterable(..), Alteration(..))

layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods:: [Modification MultiOsRawLayout]
    mods =
      [ metadata_mod
      , Mod opt_mods
      , Mod $ DKDefReplace deadKeysDefinition
      , Mod groups_mod
      ]
    metadata_mod = Mod (ReplaceMetadata metadata)
    metadata = (_metadata US.layout)
      { _name = "Latin (basic)"
      , _systemName = "latin_basic_custom"
      , _description = Just "Common latin layout (basic)"
      }
    opt_mods :: [WindowsOptionsModification]
    opt_mods =
      [ WinAltGr True
      , WinIsoLevel3IsAltGr True
      ]
    groups_mod :: MultiOsRawGroupsModification
    groups_mod =
      let ms = groupMods DefaultCapsLockBehaviour mempty shift isoLevel3 isoLevel4
      in GroupMods FirstGroup ms

deadKeysDefinition :: RawDeadKeyDefinitions
deadKeysDefinition = mconcat
  [ DK.diacriticsDef
  , DK.diacriticsExtraDef ]

group :: KeyMultiOsRawGroup
group = mkGroup DefaultCapsLockBehaviour mempty shift isoLevel3 isoLevel4

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = gmodify (groupMods opt l1 l2 l3 l4) $ US.mkGroup opt l1 l2 l3 l4

groupMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawGroupModification]
groupMods opt l1 l2 l3 l4 =
  [ ReplaceGroupName "English"
  -- [TODO] delete windows control level
  , KeyMapMod (KeyMapUpdate $ usMultiOsActionMapUpdate opt l1 l2 l3 l4)
  ]

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 =
  alter (usMultiOsActionMapUpdate opt l1 l2 l3 l4) (US.mkActionMap opt l1 l2 l3 l4)

usMultiOsActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMapAlteration
usMultiOsActionMapUpdate opt l1 l2 l3 l4 =
  toMultiOsRawActionsAlterationMap $ usActionMapUpdate opt l1 l2 l3 l4

usActionMapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> RawActionsKeyMapAlteration
usActionMapUpdate _opt l1 l2 l3 l4 =
  [ (K.Tilde     , [l3 ⏵ '¬', l4 ⏵ '¬'])
  , (K.N1        , [l3 ⏵ '¹', l4 ⏵ '¡'])
  , (K.N2        , [l3 ⏵ '²', l4 ⏵ '⅛'])
  , (K.N3        , [l3 ⏵ '³', l4 ⏵ '£'])
  , (K.N4        , [l3 ⏵ '¼', l4 ⏵ '$'])
  , (K.N5        , [l3 ⏵ '½', l4 ⏵ '⅜'])
  , (K.N6        , [l3 ⏵ '¾', l4 ⏵ '⅝'])
  , (K.N7        , [l3 ⏵ '{', l4 ⏵ '⅞'])
  , (K.N8        , [l3 ⏵ '[', l4 ⏵ '™'])
  , (K.N9        , [l3 ⏵ ']', l4 ⏵ '±'])
  , (K.N0        , [l3 ⏵ '}', l4 ⏵ '°'])
  , (K.Minus     , [l3 ⏵ '\\', l4 ⏵ '¿'])
  , (K.Plus      , [l3 ⏵ DK.cedilla, l4 ⏵ DK.ogonek])
  --
  , (K.Q         , [l3 ⏵ '@', l4 ⏵ 'Ω'])
  , (K.W         , [l3 ⏵ 'ł', l4 ⏵ 'Ł'])
  , (K.E         , [l3 ⏵ 'e', l4 ⏵ 'E'])
  , (K.R         , [l3 ⏵ '¶', l4 ⏵ '®'])
  , (K.T         , [l3 ⏵ 'ŧ', l4 ⏵ 'Ŧ'])
  , (K.Y         , [l3 ⏵ '←', l4 ⏵ '¥'])
  , (K.U         , [l3 ⏵ '↓', l4 ⏵ '↑'])
  , (K.I         , [l3 ⏵ '→', l4 ⏵ 'ı'])
  , (K.O         , [l3 ⏵ 'ø', l4 ⏵ 'Ø'])
  , (K.P         , [l3 ⏵ 'þ', l4 ⏵ 'Þ'])
  , (K.LBracket  , [l3 ⏵ DK.diaeresisAbove, l4 ⏵ DK.ringAbove])
  , (K.RBracket  , [l3 ⏵ DK.tildeAbove, l4 ⏵ DK.macronAbove])
  , (K.Backslash , [l3 ⏵ DK.graveAbove, l4 ⏵ DK.breveAbove])
  --
  , (K.A         , [l3 ⏵ 'æ', l4 ⏵ 'Æ'])
  , (K.S         , [l3 ⏵ 'ß', l4 ⏵ '§'])
  , (K.D         , [l3 ⏵ 'ð', l4 ⏵ 'Ð'])
  , (K.F         , [l3 ⏵ 'đ', l4 ⏵ 'ª'])
  , (K.G         , [l3 ⏵ 'ŋ', l4 ⏵ 'Ŋ'])
  , (K.H         , [l3 ⏵ 'ħ', l4 ⏵ 'Ħ'])
  , (K.J         , [l3 ⏵ NoAction, l4 ⏵ DK.horn]) -- [FIXME] DK.hook
  , (K.K         , [l3 ⏵ 'ĸ', l4 ⏵ '&'])
  , (K.L         , [l3 ⏵ 'ł', l4 ⏵ 'Ł'])
  , (K.Semicolon , [l3 ⏵ DK.acuteAbove, l4 ⏵ DK.doubledAcute])
  , (K.Quote     , [l3 ⏵ DK.circumflexAbove, l4 ⏵ DK.caronAbove])
  --
  , (K.Iso102    , [l1 ⏵ '<', l2 ⏵ '>', l3 ⏵ '|', l4 ⏵ '¦'])
  , (K.Z         , [l3 ⏵ '«', l4 ⏵ '<'])
  , (K.X         , [l3 ⏵ '»', l4 ⏵ '>'])
  , (K.C         , [l3 ⏵ '¢', l4 ⏵ '©'])
  , (K.V         , [l3 ⏵ '“', l4 ⏵ '‘'])
  , (K.B         , [l3 ⏵ '”', l4 ⏵ '’'])
  , (K.N         , [l3 ⏵ 'n', l4 ⏵ 'N'])
  , (K.M         , [l3 ⏵ 'µ', l4 ⏵ 'º'])
  , (K.Comma     , [l3 ⏵ '─', l4 ⏵ '×'])
  , (K.Period    , [l3 ⏵ '·', l4 ⏵ '÷'])
  , (K.Slash     , [l3 ⏵ DK.dotBelow, l4 ⏵ DK.dotAbove])

  , (K.RAlternate, InsertOrReplace (rawSingleton (AModifier IsoLevel3Set)))
  , (K.KPDecimal , [l1 ⏵ KP_Separator, l2 ⏵ KP_Separator])
  ]
