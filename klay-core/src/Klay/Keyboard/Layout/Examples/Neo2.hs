{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Neo2
  ( layout
  , group
  , mkGroup
  , permutation
  ) where

import Data.Map.Strict qualified as Map

import Country.Identifier (germany)
import Data.BCP47 (mkLocalized, mkLanguage)
import Data.LanguageCodes (ISO639_1(DE, EN))

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action qualified as A
import Klay.Keyboard.Layout.Action ((⏵))
import Klay.Keyboard.Layout.Action.DeadKey qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Compose.English qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Diacritics qualified as DK
import Klay.Keyboard.Layout.Action.DeadKey.Greek qualified as DK
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Group
  (KeyMultiOsRawGroup, GroupIndex(FirstGroup))
import Klay.Keyboard.Layout.Modifier
  ( control, isoLevel1, isoLevel2, isoLevel3, isoLevel4, isoLevel5, isoLevel6
  , eightLevelLevelFiveLock, eightLevelAlphabeticLevelFiveLock)
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , DeadKeysModification(..), WindowsOptionsModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify )
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.OS (pattern TLinux)
import Data.Alterable (Magma(..), Alteration(..), Upgrade(..), (!>))


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods:: [Modification MultiOsRawLayout]
    mods =
      [ Mod metadata_mod
      , Mod opt_mods
      , Mod $ DKDefReplace deadKeysDefinition
      , Mod groups_mod
      ]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata US.layout)
      { _name = "Neo2"
      , _systemName = "neo2"
      , _locales = mempty
        { _locPrimary=[mkLocalized DE germany]
        , _locSecondary=[mkLanguage EN]
        }
      }
    opt_mods :: [WindowsOptionsModification]
    opt_mods =
      [ WinAltGr False
      , VirtualKeysDefinition (KeyMapUpdate vkUpdate)
      , WinIsoLevel3IsAltGr False
      ]
    groups_mod :: MultiOsRawGroupsModification
    groups_mod =
      let ms = groupMods kleinbuchstaben großbuchstaben sonderzeichen tabellenkalkulation griechisch wissenschaft
      in GroupMods FirstGroup ms

deadKeysDefinition :: RawDeadKeyDefinitions
deadKeysDefinition = mconcat
  [ DK.diacriticsDef
  , DK.greekDef
  , DK.composeDef
  , [(drehen, [DK.char 'A' '∀'])] -- [TODO]
  ]

drehen :: DeadKey
drehen = DeadKey
  { _dkName = "Drehen"
  , _dkLabel = "↻"
  , _dkBaseChar = '↻'
  }

vkUpdate :: VirtualKeysDefinition
vkUpdate =
  [ (K.Tilde     , VK.VK_OEM_1)
  , (K.Plus      , VK.VK_OEM_2)
  , (K.LBracket  , VK.VK_OEM_3)
  , (K.RBracket  , VK.VK_OEM_4)
  , (K.CapsLock  , VK.VK_OEM_102)
  , (K.Backslash , VK.VK_OEM_102)
  , (K.Z         , VK.VK_OEM_5)
  , (K.X         , VK.VK_OEM_6)
  , (K.C         , VK.VK_OEM_7)
  , (K.Iso102    , VK.VK_OEM_8)
  , (K.RAlternate, VK.VK_OEM_8)
  ]

permutation :: [K.Key]
permutation =
  [ K.Q, K.X, K.Quote, K.Y, K.K, K.R, K.C, K.Slash, K.J, K.N, K.B, K.Z, K.Semicolon
  , K.D, K.A, K.U, K.H, K.S, K.I, K.G, K.O, K.F, K.E, K.L, K.T, K.W, K.V, K.P
  ]

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

groupMods :: Level -> Level -> Level -> Level -> Level -> Level -> [MultiOsRawGroupModification]
groupMods kleinbuchstaben' großbuchstaben' sonderzeichen' tabellenkalkulation' griechisch' wissenschaft' =
  [ ReplaceGroupName "Neo2"
  , AddLevels
    [ (kleinbuchstaben'    , "Klein Buchstaben")
    , (großbuchstaben'     , "Groß Buchstaben")
    , (sonderzeichen'      , "Sonder Zeichen")
    , (tabellenkalkulation', "Tabellen Kalkulation")
    , (griechisch'         , "Griechisch")
    , (wissenschaft'       , "Wissenschaft")
    ]
  , KeyMapMods keymapMods ]

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 l5 l6 = gmodify (groupMods l1 l2 l3 l4 l5 l6) $ US.mkGroup opt l1 l2 l3 l4

kleinbuchstaben, großbuchstaben, sonderzeichen, tabellenkalkulation, griechisch, wissenschaft :: Level
kleinbuchstaben     = isoLevel1
großbuchstaben      = isoLevel2
sonderzeichen       = isoLevel3
griechisch          = isoLevel4
tabellenkalkulation = isoLevel5
wissenschaft        = isoLevel6

-- [TODO] Numpad

keymapMods :: [MultiOsRawActionMapModification]
keymapMods =
  [ KeyMapUpdate usActionMapUpdate
  , KeyClone K.RAlternate K.Iso102
  , KeyClone K.Backslash K.CapsLock
  , KeyPermutation permutation
  ]

usActionMapUpdate :: A.MultiOsRawActionsKeyMapAlteration
usActionMapUpdate
  =  buchstabenAM
  +> sonderzeichenTabellenkalkulationAM
  +> griechischWissenschaftActionMap
  +> Map.map A.insertOrUpdate1d
      [ (K.LBracket , [control ⏵ (Reset :: A.ActionUpgrade) ])
      , (K.RBracket , [control ⏵ (Reset :: A.ActionUpgrade) ])
      , (K.Backslash, [control ⏵ (Reset :: A.ActionUpgrade) ])
      ]

buchstabenAM :: A.MultiOsRawActionsKeyMapAlteration
buchstabenAM = Map.map A.insertOrUpdate1d
  [ (K.Tilde   ,     [kleinbuchstaben ⏵ DK.circumflexAbove, großbuchstaben ⏵ DK.caronAbove])
  , (K.N1      ,     [großbuchstaben ⏵ '°'])
  , (K.N2      ,     [großbuchstaben ⏵ '§'])
  , (K.N3      ,     [großbuchstaben ⏵ 'ℓ'])
  , (K.N4      ,     [großbuchstaben ⏵ '»'])
  , (K.N5      ,     [großbuchstaben ⏵ '«'])
  , (K.N6      ,     [großbuchstaben ⏵ '$'])
  , (K.N7      ,     [großbuchstaben ⏵ '€'])
  , (K.N8      ,     [großbuchstaben ⏵ '„'])
  , (K.N9      ,     [großbuchstaben ⏵ '“'])
  , (K.N0      ,     [großbuchstaben ⏵ '”'])
  , (K.Minus   ,     [kleinbuchstaben ⏵ '-', großbuchstaben ⏵ '—'])
  , (K.Plus    ,     [kleinbuchstaben ⏵ DK.graveAbove, großbuchstaben ⏵ DK.cedillaBelow])

  , (K.LBracket,  [kleinbuchstaben ⏵ 'ß', großbuchstaben ⏵ 'ẞ'])
  , (K.RBracket,  [kleinbuchstaben ⏵ DK.acuteAbove, großbuchstaben ⏵ DK.tildeAbove])

  , (K.Backslash, InsertOrReplace (A.rawSingleton (AModifier IsoLevel3Set)))

  , (K.Semicolon, [kleinbuchstaben ⏵ 'ü', großbuchstaben ⏵ 'Ü'])
  , (K.Quote    , [kleinbuchstaben ⏵ 'ö', großbuchstaben ⏵ 'Ö'])
  , (K.Slash    , [kleinbuchstaben ⏵ 'ä', großbuchstaben ⏵ 'Ä'])

  , (K.Comma    , [kleinbuchstaben ⏵ ',', großbuchstaben ⏵ '–'])
  , (K.Period   , [kleinbuchstaben ⏵ '.', großbuchstaben ⏵ '•'])

  , (K.Space     , [kleinbuchstaben ⏵ ' ', großbuchstaben ⏵ ' '])
  , (K.RAlternate, InsertOrReplace (A.rawSingleton (AModifier IsoLevel5Set)))

  , (K.KPDecimal , [kleinbuchstaben ⏵ A.KP_Separator, großbuchstaben ⏵ A.KP_Separator])
  ] +>
  [ (K.Tabulator , [TLinux ([griechisch ⏵ IsoLevel5Lock] :: A.RawActionsAlteration)])
  ]

sonderzeichenTabellenkalkulationAM :: A.MultiOsRawActionsKeyMapAlteration
sonderzeichenTabellenkalkulationAM = Map.map A.insertOrUpdate1d
  [ (K.Tilde, [sonderzeichen ⏵ drehen, tabellenkalkulation ⏵ DK.dotAbove] !> eightLevelLevelFiveLock)
  , (K.N1   , [sonderzeichen ⏵ '¹', tabellenkalkulation ⏵ 'ª'] !> eightLevelLevelFiveLock)
  , (K.N2   , [sonderzeichen ⏵ '²', tabellenkalkulation ⏵ 'º'] !> eightLevelLevelFiveLock)
  , (K.N3   , [sonderzeichen ⏵ '³', tabellenkalkulation ⏵ '№'] !> eightLevelLevelFiveLock)
  , (K.N4   , [sonderzeichen ⏵ '›', tabellenkalkulation ⏵ NoAction] !> eightLevelLevelFiveLock)
  , (K.N5   , [sonderzeichen ⏵ '‹', tabellenkalkulation ⏵ '·'] !> eightLevelLevelFiveLock)
  , (K.N6   , [sonderzeichen ⏵ '¢', tabellenkalkulation ⏵ '£'] !> eightLevelLevelFiveLock)
  , (K.N7   , [sonderzeichen ⏵ '¥', tabellenkalkulation ⏵ '¤'] !> eightLevelLevelFiveLock)
  , (K.N8   , [sonderzeichen ⏵ '‚', tabellenkalkulation ⏵ A.Tab] !> eightLevelLevelFiveLock)
  , (K.N9   , [sonderzeichen ⏵ '‘', tabellenkalkulation ⏵ A.KP_Divide] !> eightLevelLevelFiveLock)
  , (K.N0   , [sonderzeichen ⏵ '’', tabellenkalkulation ⏵ A.KP_Multiply] !> eightLevelLevelFiveLock)
  , (K.Minus, [sonderzeichen ⏵ NoAction, tabellenkalkulation ⏵ A.KP_Subtract] !> eightLevelLevelFiveLock)
  , (K.Plus , [sonderzeichen ⏵ DK.ringAbove, tabellenkalkulation ⏵ DK.diaeresisAbove] !> eightLevelLevelFiveLock)

  , (K.Tabulator, [sonderzeichen ⏵ DK.compose] !> eightLevelLevelFiveLock)
  , (K.X        , [sonderzeichen ⏵ '…', tabellenkalkulation ⏵ A.Page_Up] !> eightLevelAlphabeticLevelFiveLock)
  , (K.V        , [sonderzeichen ⏵ '_', tabellenkalkulation ⏵ A.BackSpace] !> eightLevelAlphabeticLevelFiveLock)
  , (K.L        , [sonderzeichen ⏵ '[', tabellenkalkulation ⏵ A.CursorUp] !> eightLevelAlphabeticLevelFiveLock)
  , (K.C        , [sonderzeichen ⏵ ']', tabellenkalkulation ⏵ A.Delete] !> eightLevelAlphabeticLevelFiveLock)
  , (K.W        , [sonderzeichen ⏵ '^', tabellenkalkulation ⏵ A.Page_Down] !> eightLevelAlphabeticLevelFiveLock)
  , (K.K        , [sonderzeichen ⏵ '!', tabellenkalkulation ⏵ '¡'] !> eightLevelAlphabeticLevelFiveLock)
  , (K.H        , [sonderzeichen ⏵ '<', tabellenkalkulation ⏵ A.KP_7] !> eightLevelAlphabeticLevelFiveLock)
  , (K.G        , [sonderzeichen ⏵ '>', tabellenkalkulation ⏵ A.KP_8] !> eightLevelAlphabeticLevelFiveLock)
  , (K.F        , [sonderzeichen ⏵ '=', tabellenkalkulation ⏵ A.KP_9] !> eightLevelAlphabeticLevelFiveLock)
  , (K.Q        , [sonderzeichen ⏵ '&', tabellenkalkulation ⏵ A.KP_Add] !> eightLevelAlphabeticLevelFiveLock)
  , (K.LBracket , [sonderzeichen ⏵ 'ſ', tabellenkalkulation ⏵ '\x2212'] !> eightLevelAlphabeticLevelFiveLock)
  , (K.RBracket , [sonderzeichen ⏵ DK.longSolidus, tabellenkalkulation ⏵ DK.doubledAcute] !> eightLevelLevelFiveLock)

  , (K.U, [sonderzeichen ⏵ '\\', tabellenkalkulation ⏵ A.Home] !> eightLevelAlphabeticLevelFiveLock)
  , (K.I, [sonderzeichen ⏵ '/', tabellenkalkulation ⏵ A.CursorLeft] !> eightLevelAlphabeticLevelFiveLock)
  , (K.A, [sonderzeichen ⏵ '{', tabellenkalkulation ⏵ A.CursorDown] !> eightLevelAlphabeticLevelFiveLock)
  , (K.E, [sonderzeichen ⏵ '}', tabellenkalkulation ⏵ A.CursorRight] !> eightLevelAlphabeticLevelFiveLock)
  , (K.O, [sonderzeichen ⏵ '*', tabellenkalkulation ⏵ A.End] !> eightLevelAlphabeticLevelFiveLock)
  , (K.S, [sonderzeichen ⏵ '?', tabellenkalkulation ⏵ '¿'] !> eightLevelAlphabeticLevelFiveLock)
  , (K.N, [sonderzeichen ⏵ '(', tabellenkalkulation ⏵ A.KP_4] !> eightLevelAlphabeticLevelFiveLock)
  , (K.R, [sonderzeichen ⏵ ')', tabellenkalkulation ⏵ A.KP_5] !> eightLevelAlphabeticLevelFiveLock)
  , (K.T, [sonderzeichen ⏵ '-', tabellenkalkulation ⏵ A.KP_6] !> eightLevelAlphabeticLevelFiveLock)
  , (K.D, [sonderzeichen ⏵ ':', tabellenkalkulation ⏵ A.KP_Decimal] !> eightLevelAlphabeticLevelFiveLock)
  , (K.Y, [sonderzeichen ⏵ '@', tabellenkalkulation ⏵ '.'] !> eightLevelAlphabeticLevelFiveLock)

  , (K.Semicolon , [sonderzeichen ⏵ '#', tabellenkalkulation ⏵ A.Escape] !> eightLevelAlphabeticLevelFiveLock)
  , (K.Quote     , [sonderzeichen ⏵ '$', tabellenkalkulation ⏵ A.Tab] !> eightLevelAlphabeticLevelFiveLock)
  , (K.Slash     , [sonderzeichen ⏵ '|', tabellenkalkulation ⏵ A.Insert] !> eightLevelAlphabeticLevelFiveLock)
  , (K.P         , [sonderzeichen ⏵ '~', tabellenkalkulation ⏵ A.Return] !> eightLevelAlphabeticLevelFiveLock)
  , (K.Z         , [sonderzeichen ⏵ '`', tabellenkalkulation ⏵ A.Undo] !> eightLevelAlphabeticLevelFiveLock)
  , (K.B         , [sonderzeichen ⏵ '+', tabellenkalkulation ⏵ ':'] !> eightLevelAlphabeticLevelFiveLock)
  , (K.M         , [sonderzeichen ⏵ '%', tabellenkalkulation ⏵ A.KP_1] !> eightLevelAlphabeticLevelFiveLock)
  , (K.Comma     , [sonderzeichen ⏵ '"', tabellenkalkulation ⏵ A.KP_2] !> eightLevelLevelFiveLock)
  , (K.Period    , [sonderzeichen ⏵ '\'', tabellenkalkulation ⏵ A.KP_3] !> eightLevelLevelFiveLock)
  , (K.J         , [sonderzeichen ⏵ ';', tabellenkalkulation ⏵ ';'] !> eightLevelAlphabeticLevelFiveLock)

  , (K.Space, [sonderzeichen ⏵ ' ', tabellenkalkulation ⏵ A.KP_0] !> eightLevelLevelFiveLock)
  ]

griechischWissenschaftActionMap :: A.MultiOsRawActionsKeyMapAlteration
griechischWissenschaftActionMap = Map.map A.insertOrUpdate1d
  [ (K.Tilde   , [griechisch ⏵ NoAction, wissenschaft ⏵ DK.dotBelow])
  , (K.N1      , [griechisch ⏵ '₁', wissenschaft ⏵ '¬'])
  , (K.N2      , [griechisch ⏵ '₂', wissenschaft ⏵ '∨'])
  , (K.N3      , [griechisch ⏵ '₃', wissenschaft ⏵ '∧'])
  , (K.N4      , [griechisch ⏵ '♀', wissenschaft ⏵ '⊥' ])
  , (K.N5      , [griechisch ⏵ '♂', wissenschaft ⏵ '∡' ])
  , (K.N6      , [griechisch ⏵ '⚥', wissenschaft ⏵ '∥' ])
  , (K.N7      , [griechisch ⏵ 'ϰ', wissenschaft ⏵ '→'])
  , (K.N8      , [griechisch ⏵ '⟨', wissenschaft ⏵ '∞'])
  , (K.N9      , [griechisch ⏵ '⟩', wissenschaft ⏵ '∝'])
  , (K.N0      , [griechisch ⏵ '₀', wissenschaft ⏵ '∅'])
  , (K.Minus   , [griechisch ⏵ '\x2011', wissenschaft ⏵ '╌'])
  , (K.Plus    , [griechisch ⏵ DK.reversedCommaAbove, wissenschaft ⏵ DK.macronAbove])

  , (K.X       , [griechisch ⏵ 'ξ', wissenschaft ⏵ 'Ξ'])
  , (K.V       , [griechisch ⏵ NoAction, wissenschaft ⏵ '√'])
  , (K.L       , [griechisch ⏵ 'λ', wissenschaft ⏵ 'Λ'])
  , (K.C       , [griechisch ⏵ 'χ', wissenschaft ⏵ 'ℂ'])
  , (K.W       , [griechisch ⏵ 'ω', wissenschaft ⏵ 'Ω'])
  , (K.K       , [griechisch ⏵ 'κ', wissenschaft ⏵ '×'])
  , (K.H       , [griechisch ⏵ 'ψ', wissenschaft ⏵ 'Ψ'])
  , (K.G       , [griechisch ⏵ 'γ', wissenschaft ⏵ 'Γ'])
  , (K.F       , [griechisch ⏵ 'φ', wissenschaft ⏵ 'Φ'])
  , (K.Q       , [griechisch ⏵ 'ϕ', wissenschaft ⏵ 'ℚ'])
  , (K.LBracket, [griechisch ⏵ 'ς', wissenschaft ⏵ '∘'])
  , (K.RBracket, [griechisch ⏵ DK.horn, wissenschaft ⏵ DK.breveAbove])

  , (K.U, [griechisch ⏵ NoAction, wissenschaft ⏵ '⊂'])
  , (K.I, [griechisch ⏵ 'ι', wissenschaft ⏵ '∫'])
  , (K.A, [griechisch ⏵ 'α', wissenschaft ⏵ '∀'])
  , (K.E, [griechisch ⏵ 'ε', wissenschaft ⏵ '∃'])
  , (K.O, [griechisch ⏵ 'ο', wissenschaft ⏵ '∈'])
  , (K.S, [griechisch ⏵ 'σ', wissenschaft ⏵ 'Σ'])
  , (K.N, [griechisch ⏵ 'ν', wissenschaft ⏵ 'ℕ'])
  , (K.R, [griechisch ⏵ 'ρ', wissenschaft ⏵ 'ℝ'])
  , (K.T, [griechisch ⏵ 'τ', wissenschaft ⏵ '∂'])
  , (K.D, [griechisch ⏵ 'δ', wissenschaft ⏵ 'Δ'])
  , (K.Y, [griechisch ⏵ 'υ', wissenschaft ⏵ '∇'])

  , (K.Semicolon, [griechisch ⏵ NoAction, wissenschaft ⏵ '∪'])
  , (K.Quote    , [griechisch ⏵ 'ϵ', wissenschaft ⏵ '∩'])
  , (K.Slash    , [griechisch ⏵ 'η', wissenschaft ⏵ 'ℵ'])
  , (K.P        , [griechisch ⏵ 'π', wissenschaft ⏵ 'Π'])
  , (K.Z        , [griechisch ⏵ 'ζ', wissenschaft ⏵ 'ℤ'])
  , (K.B        , [griechisch ⏵ 'β', wissenschaft ⏵ '⇐'])
  , (K.M        , [griechisch ⏵ 'μ', wissenschaft ⏵ '⇔'])
  , (K.Comma    , [griechisch ⏵ 'ϱ', wissenschaft ⏵ '⇒'])
  , (K.Period   , [griechisch ⏵ 'ϑ', wissenschaft ⏵ '↦'])
  , (K.J        , [griechisch ⏵ 'θ', wissenschaft ⏵ 'Θ'])

  , (K.Space    ,     [griechisch ⏵ '\xA0', wissenschaft ⏵ '\x202f'])
  ]
