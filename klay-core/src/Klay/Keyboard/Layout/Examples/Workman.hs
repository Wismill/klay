{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists   #-}

module Klay.Keyboard.Layout.Examples.Workman
  ( layout
  , group
  , mkGroup
  , mkActionMap
  , permutations
  ) where


import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (MultiOsRawActionsKeyMap)
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Group
  ( KeyMultiOsRawGroup, GroupIndex(FirstGroup)
  )
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods = [ Mod metadata_mod, Mod groups_mod]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata US.layout)
      { _name = "Workman"
      , _systemName = "workman" }
    groups_mod :: MultiOsRawGroupsModification
    groups_mod = GroupMods FirstGroup groupMods

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

groupMods :: [MultiOsRawGroupModification]
groupMods =
  [ ReplaceGroupName "Workman"
  , KeyMapMod actionMapMod
  ]

actionMapMod :: MultiOsRawActionMapModification
actionMapMod = KeyPermutations permutations

permutations :: [[K.Key]]
permutations =
  [ [K.E, K.R, K.W, K.D, K.H, K.Y, K.J, K.N, K.K]
  , [K.T, K.B, K.V, K.C, K.M, K.L, K.O, K.P, K.Semicolon, K.I, K.U, K.F]
  ]

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4 = gmodify groupMods $ US.mkGroup opt l1 l2 l3 l4

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 = amodify actionMapMod $ US.mkActionMap opt l1 l2 l3 l4

