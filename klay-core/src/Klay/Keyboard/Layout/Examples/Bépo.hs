{-# LANGUAGE OverloadedLists #-}

module Klay.Keyboard.Layout.Examples.Bépo
  ( layout
  , deadKeysDefinition
  , group
  , vkUpdate
  , permutations
  , mkGroup
  , mkActionMap
  , keymapMods
  ) where

import Country.Identifier qualified as C
import Data.BCP47 (mkLocalized, mkLanguage)
import Data.LanguageCodes qualified as LC

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  ( Actions(..), ActionUpgrade, SpecialAction(KP_Separator)
  , MultiOsRawActionsKeyMap, RawActionsKeyMap
  , rawSingleton, custom
  , toMultiOsRawActionsAlterationMap, (.=), (⏵))
import Klay.Keyboard.Layout.Action.Auto.ArabicNumerals
    ( westernArabicNumeralsMap )
import Klay.Keyboard.Layout.Action.DeadKey.Bépo.All qualified as DK
import Klay.Keyboard.Layout.Group
  ( KeyMultiOsRawGroup, GroupIndex(FirstGroup)
  )
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Level qualified as L
import Klay.Keyboard.Layout.Modifier
  ( control, isoLevel1, isoLevel2, isoLevel3, isoLevel4
  , makeFourLevelSemiAlphabetic)
import Klay.Keyboard.Layout.Modification
  ( Modification(..), LayoutMetadataModification(..)
  , WindowsOptionsModification(..), DeadKeysModification(..)
  , GroupsModification(..), MultiOsRawGroupsModification
  , GroupModification(..), MultiOsRawGroupModification
  , ActionMapModification(..), MultiOsRawActionMapModification
  , lmodify, gmodify, amodify )
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.OS (pattern TWindows)
import Data.Alterable (Magma(..), Upgrade(..), (!>), insertOrUpdate0)


layout :: MultiOsRawLayout
layout = lmodify mods US.layout
  where
    mods :: [Modification MultiOsRawLayout]
    mods =
      [ Mod metadata_mod
      , Mod $ DKDefReplace deadKeysDefinition
      , Mod opt_mods
      , Mod groups_mod]
    metadata_mod = ReplaceMetadata metadata
    metadata = (_metadata US.layout)
      { _name = "Bépo"
      , _systemName = "bepo"
      , _description = Just "Français (bépo v1.1rc2)"
      , _locales = Locales
        { _locPrimary=[mkLocalized LC.FR C.france]
        , _locSecondary=
            [ mkLocalized LC.EN C.unitedKingdomOfGreatBritainAndNorthernIreland
            , mkLocalized LC.ES C.spain
            , mkLocalized LC.DE C.germany
            , mkLanguage LC.DA -- danois
            , mkLanguage LC.ET -- estonien
            , mkLanguage LC.FI -- finnois
            , mkLanguage LC.SV -- suédois
            , mkLanguage LC.HU -- hongrois
            , mkLanguage LC.GA -- irlandais
            , mkLanguage LC.IT -- italien
            , mkLanguage LC.LV -- letton
            , mkLanguage LC.LT -- lituanien
            , mkLanguage LC.MT -- maltais
            , mkLanguage LC.NL -- néerlandais
            , mkLanguage LC.PL -- polonais
            , mkLanguage LC.PT -- portugais
            , mkLanguage LC.RO -- roumain
            , mkLanguage LC.SK -- slovaque
            , mkLanguage LC.SL -- slovène
            , mkLanguage LC.CS -- tchèque
            , mkLanguage LC.HR -- croate
            --
            , mkLanguage LC.EO -- espéranto
            , mkLanguage LC.BR -- breton
            , mkLanguage LC.CA -- catalan
            ]
        , _locOther =
          [ mkLanguage LC.EL -- grec
          ]
        }
      , _version = Just "1.1.0-rc2"
      , _publicationDate = parsePublicationDate <$> Nothing
      , _license = Just "CC-SA-BY"
      , _author = Just "Disposition bépo — http://bepo.fr"
      }
    opt_mods :: [WindowsOptionsModification]
    opt_mods =
      [ WinAltGr True
      , VirtualKeysDefinition (KeyMapUpdate vkUpdate)
      , WinIsoLevel3IsAltGr True
      ]
    groups_mod :: MultiOsRawGroupsModification
    groups_mod =
      let ms = groupMods DefaultCapsLockBehaviour isoLevel1 isoLevel2 isoLevel3 isoLevel4
      in GroupMods FirstGroup ms


deadKeysDefinition :: RawDeadKeyDefinitions
deadKeysDefinition = DK.bépoDef

group :: KeyMultiOsRawGroup
group = firstGroupMapping layout

mkGroup :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> KeyMultiOsRawGroup
mkGroup opt l1 l2 l3 l4
  = gmodify (groupMods opt l1 l2 l3 l4)
  $ US.mkGroup opt l1 l2 l3 l4

groupMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawGroupModification]
groupMods opt l1 l2 l3 l4 =
  [ ReplaceGroupName "Bépo"
  , AddLevels
      [ (l1, L.defaultIsoLevel1)
      , (l2, L.defaultIsoLevel2)
      , (l3, L.defaultIsoLevel3)
      , (l4, L.defaultIsoLevel4) ]
  , KeyMapMods (keymapMods opt l1 l2 l3 l4) ]

vkUpdate :: VirtualKeysDefinition
vkUpdate =
  [ (K.Tilde, VK.VK_OEM_7)
  , (K.Minus, VK.VK_OEM_MINUS)
  , (K.W    , VK.VK_OEM_1)
  , (K.T    , VK.VK_OEM_2)
  , (K.Y    , VK.VK_OEM_4)
  , (K.Z    , VK.VK_OEM_6)
  , (K.N    , VK.VK_OEM_3)
  ]

permutations :: [[K.Key]]
permutations =
  [ [ K.Q, K.B, K.K, K.S, K.U, K.V, K.Period, K.H, K.C, K.X
    , K.Y, K.LBracket, K.Z, K.RBracket, K.W, K.Semicolon, K.N, K.Tilde
    , K.Quote, K.M ]
  , [ K.E, K.P, K.J, K.T, K.Slash, K.F]
  , [ K.R, K.O, K.L]
  , [ K.I, K.D]
  , [ K.G, K.Comma]
  ]

-- [FIXME] better update system
keymapMods :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> [MultiOsRawActionMapModification]
keymapMods opt l1 l2 l3 l4 =
  [ KeyMapUpdate $
    toMultiOsRawActionsAlterationMap (insertOrUpdate0 (custom UndefinedActions) <$> keymapUpdate opt l1 l2 l3 l4)
    -- Restrict singletons to some levels
    +> [ (K.Space    , [TWindows [control ⏵ (Reset :: ActionUpgrade)] ])
       , (K.Iso102   , [TWindows [control ⏵ (Reset :: ActionUpgrade)] ])
       , (K.LBracket , [TWindows [control ⏵ (Reset :: ActionUpgrade)] ])
       , (K.RBracket , [TWindows [control ⏵ (Reset :: ActionUpgrade)] ])
       , (K.Backslash, [TWindows [control ⏵ (Reset :: ActionUpgrade)] ])
       ]
  , KeyPermutations permutations
  ]

keymapUpdate :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> RawActionsKeyMap
keymapUpdate opt l1 l2 l3 l4
  = westernArabicNumeralsMap l2
  +>  [ (K.Quote    , [l1 .= '$', l2 .= '#', l3 .= '\x2013', l4 .= '¶'])
      , (K.N1       , [l1 .= '"', l3 .= '\x2014', l4 .= '„'] !> fourLevelSemiAlphabetic)
      , (K.N2       , [l1 .= '«', l3 .= '<', l4 .= '“'] !> fourLevelSemiAlphabetic)
      , (K.N3       , [l1 .= '»', l3 .= '>', l4 .= '”'] !> fourLevelSemiAlphabetic)
      , (K.N4       , [l1 .= '(', l3 .= '[', l4 .= '⩽'] !> fourLevelSemiAlphabetic)
      , (K.N5       , [l1 .= ')', l3 .= ']', l4 .= '⩾'] !> fourLevelSemiAlphabetic)
      , (K.N6       , [l1 .= '@', l3 .= '^', l4 .= NoAction] !> fourLevelSemiAlphabetic)
      , (K.N7       , [l1 .= '+', l3 .= '±', l4 .= '¬'] !> fourLevelSemiAlphabetic)
      , (K.N8       , [l1 .= '-', l3 .= '\x2212', l4 .= '¼'] !> fourLevelSemiAlphabetic)
      , (K.N9       , [l1 .= '/', l3 .= '÷', l4 .= '½'] !> fourLevelSemiAlphabetic)
      , (K.N0       , [l1 .= '*', l3 .= '×', l4 .= '¾'] !> fourLevelSemiAlphabetic)
      , (K.Minus    , [l1 .= '=', l2 .= '°', l3 .= '≠', l4 .= '′'])
      , (K.Plus     , [l1 .= '%', l2 .= '`', l3 .= '‰', l4 .= '″'])
      --
      , (K.B        , [l3 .= '|', l4 .= '_'])
      , (K.Semicolon, [l1 .= 'é', l2 .= 'É', l3 .= DK.acuteAbove, l4 .= NoAction])
      , (K.P        , [l3 .= '&', l4 .= '§'])
      , (K.O        , [l3 .= 'œ', l4 .= 'Œ'])
      , (K.Slash    , [l1 .= 'è', l2 .= 'È', l3 .= DK.graveAbove, l4 .= '`'])
      , (K.LBracket , [l1 .= DK.circumflexAbove, l2 .= '!', l3 .= '¡', l4 .= NoAction])
      , (K.V        , [l3 .= DK.hatchekAbove, l4 .= NoAction])
      , (K.D        , [l3 .= DK.bépoScience, l4 .= NoAction])
      , (K.L        , [l3 .= DK.longSolidus, l4 .= '£'])
      , (K.J        , [l3 .= NoAction, l4 .= NoAction])
      , (K.Z        , [l3 .= DK.longStroke, l4 .= NoAction])
      , (K.W        , [l3 .= NoAction, l4 .= NoAction])
      --
      , (K.A        , [l3 .= 'æ', l4 .= 'Æ'])
      , (K.U        , [l3 .= 'ù', l4 .= 'Ù'])
      , (K.I        , [l3 .= DK.diaeresisAbove, l4 .= DK.dotAbove])
      , (K.E        , [l3 .= '€', l4 .= DK.bépoCurrency])
      , (K.Comma    , [l1 .= ',', l2 .= ';', l3  .= '\'', l4 .= DK.commaBelow])
      , (K.C        , [l3 .= DK.cedillaBelow, l4 .= '©'])
      , (K.T        , [l3 .= DK.superscript, l4 .= '™'])
      , (K.S        , [l3 .= DK.bépoLatin, l4 .= 'ſ'])
      , (K.R        , [l3 .= DK.breveAbove, l4 .= '®'])
      , (K.N        , [l3 .= DK.tildeAbove, l4 .= NoAction])
      , (K.M        , [l3 .= DK.macronAbove, l4 .= NoAction])
      , (K.Backslash, [l1 .= 'ç', l2 .= 'Ç', l3 .= NoAction, l4 .= '🄯'])
      --
      , (K.Iso102   , [l1 .= 'ê', l2 .= 'Ê', l3 .= '/', l4 .= '^'])
      , (K.RBracket , [l1 .= 'à', l2 .= 'À', l3 .= '\\', l4 .= '‚'])
      , (K.Y        , [l3 .= '{', l4 .= '‘'])
      , (K.X        , [l3 .= '}', l4 .= '’'])
      , (K.Period   , [l1 .= '.', l2 .= ':', l3 .= '…', l4 .= '·'])
      , (K.K        , [l3 .= '~', l4 .= '\x2011'])
      , (K.Tilde    , [l1 .= '’', l2 .= '?', l3 .= '¿', l4 .= DK.crochetEnChef])
      , (K.Q        , [l3 .= DK.ringAbove, l4 .= DK.horn])
      , (K.G        , [l3 .= DK.greek, l4 .= '†'])
      , (K.H        , [l3 .= DK.dotBelow, l4 .= '‡'])
      , (K.F        , [l3 .= DK.ogonek, l4 .= NoAction])
      --
      , (K.Space    , [l1 .= '\x0020', l2 .= '\x202F', l3 .= '_', l4 .= '\x00A0'])
      , (K.KPDecimal, [l1 .= KP_Separator, l2 .= KP_Separator])

      , (K.RAlternate, rawSingleton (AModifier IsoLevel3Set))
      ]
  -- Fill blanks
  +>  [ (K.N6       , [l4 .= '☭'])
      , (K.V        , [l4 .= '☢'])
      , (K.D        , [l4 .= '☣'])
      , (K.J        , [l3 .= '☮', l4 .= '☯'])
      , (K.Z        , [l4 .= '☙'])
      , (K.W        , [l3 .= '⚜', l4 .= '♿'])
      , (K.Semicolon, [l4 .= '♥'])
      , (K.LBracket , [l4 .= '☠'])
      , (K.N        , [l4 .= '⚓'])
      , (K.M        , [l4 .= '⛽'])
      , (K.Backslash, [l3 .= '✈'])
      , (K.F        , [l4 .= '⛄'])
      ]
  where fourLevelSemiAlphabetic = makeFourLevelSemiAlphabetic opt

mkActionMap :: CapsLockBehaviour -> Level -> Level -> Level -> Level -> MultiOsRawActionsKeyMap
mkActionMap opt l1 l2 l3 l4 = amodify (keymapMods opt l1 l2 l3 l4) $ US.mkActionMap opt l1 l2 l3 l4
