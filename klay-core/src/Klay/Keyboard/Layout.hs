{-# LANGUAGE OverloadedLists #-}

{-|
Description: Model of a keyboard layout
-}

module Klay.Keyboard.Layout
  ( Layout(..)
  , MultiOsRawLayout
  , RawLayout
  , ResolvedLayout
  , LinuxLayout
  , LayoutMetadata(..)

  , SystemName(..)
  , LayoutLocales
  , Locales(..)
  , Version(..)

  , LayoutOptions
  , OsIndependentOptions(..)
  , LinuxOptions(..)
  , WindowsOptions(..)

  , Key
  , ButtonAction
  , ButtonActionsDefinition
  , KeyNames
  , VirtualKeysDefinition

  , Modifier(..)
  , ModifierBit(..)
  , ModifierVariant(..)
  , ModifierEffect(..)
  , ModifiersOptions
  , CapsLockBehaviour(..)

  , Group(..)
  , Groups(..)

  , Level

  , Action(..)
  , ActionsKeyMap

  , DeadKey(..)
  , DeadKeyCombo(..)
  , DeadKeyDefinitions
  , RawDeadKeyDefinition
  , RawDeadKeyDefinitions
  , MergedDeadKeyDefinitions

  , defaultOptions
  , defaultWindowsOptions
  , defaultVirtualKeysDefinition

  , parsePublicationDate
  , actionLabeler
  , resolveOsLayout
  , resolveLayout
  , groupsLayouts
  , finalGroupsLayouts
  , firstGroupLayout
  , firstGroupMapping
  , mainLayoutLocale
  , mMainLayoutLocale
  , mkLayoutId
  , mkLayoutId'
  , mkLayoutName
  , mkLayoutVersion
  , mkLayoutSemanticVersion
  , versionToSemVer
  , mkWindowsKeyboardType
  ) where

import Numeric.Natural
import Data.List.NonEmpty qualified as NE
import Data.String(IsString(..))
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Time.Calendar (Day)
import Data.Time.Format (parseTimeOrError, defaultTimeLocale)
import Data.Functor ((<&>))
import Control.Monad ((>=>))

import Data.Versions qualified as V
import Data.BCP47 qualified as BCP47
import Data.LanguageCodes qualified as ISO639_1

import Klay.Export.Windows.Common.KeyNames (defaultKeyNames)
import Klay.Keyboard.Hardware.Key (Key(..))
import Klay.Keyboard.Hardware.ButtonAction (ButtonAction, ButtonActionsDefinition)
import Klay.Keyboard.Layout.Action
  ( Action(..), ActionsKeyMap, Level
  , ActionLabeler, mkActionLabeler )
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Internal.Layout
import Klay.Keyboard.Layout.Modifier (Modifier(..), ModifierBit(..), ModifierEffect(..), ModifierVariant(..), ModifiersOptions, CapsLockBehaviour(..))
import Klay.Keyboard.Layout.VirtualKey (VirtualKeysDefinition, defaultVirtualKeysDefinition)
import Klay.Keyboard.Layout.Action.DeadKey (DeadKey(..), DeadKeyCombo(..), DeadKeyDefinitions, RawDeadKeyDefinition, RawDeadKeyDefinitions, MergedDeadKeyDefinitions)
import Klay.OS (MultiOs(..), OS(..), OsSpecific(..))
import Klay.Utils.UserInput


-- | Get the action labeler of the layout for a specific OS
actionLabeler :: OsSpecific -> LayoutOptions -> ActionLabeler
actionLabeler OsIndependent        = mkActionLabeler . _iActionLabels . _osIndependent
actionLabeler (OsSpecific Linux)   = mkActionLabeler . _lActionLabels . _linux
actionLabeler (OsSpecific Windows) = mkActionLabeler . _wActionLabels  . _windows

-- | Default layout options
defaultOptions :: LayoutOptions
defaultOptions = MultiOs
  { _osIndependent = OsIndependentOptions
    { _iActionLabels = mempty
    }
  , _linux = LinuxOptions
    { _lActionLabels = mempty
    }
  , _windows = defaultWindowsOptions
  }

-- | Default windows options
defaultWindowsOptions :: WindowsOptions
defaultWindowsOptions = WindowsOptions
  { _wActionLabels = mempty
  , _wKeysNames = defaultKeyNames
  , _wKeyboardType = Raw 4
  , _wVirtualKeyDefinition = mempty
  , _wAltgr = False
  , _wIsoLevel3IsAltGr = False
  , _wShiftDeactivatesCaps = False
  , _wLrmRlm = False
  }

-- | Convert a 'String' to a date.
parsePublicationDate :: String -> Day
parsePublicationDate = parseTimeOrError True defaultTimeLocale "%F"

resolveOsLayout :: OsSpecific -> MultiOsRawLayout -> RawLayout
resolveOsLayout os l = l{_groups = resolveOsGroups os . _groups $ l}

resolveLayout :: OsSpecific -> MultiOsRawLayout -> ResolvedLayout
resolveLayout os l = l{_groups = resolveGroups os . _groups $ l}

groupsLayouts :: OsSpecific -> MultiOsRawLayout -> ResolvedGroups
groupsLayouts os Layout{_groups=gs} = resolveGroups os gs

finalGroupsLayouts :: OsSpecific -> MultiOsRawLayout -> FinalGroups
finalGroupsLayouts os Layout{_groups=gs} = resolveFinalGroups os gs

firstGroupLayout :: OsSpecific -> MultiOsRawLayout -> KeyFinalGroup
firstGroupLayout os Layout{_groups=gs} = resolveGroupLayout os . firstGroup $ gs

firstGroupMapping :: Layout (GroupsMappings ls k a) -> GroupMapping ls k a
firstGroupMapping = firstGroup . _groups

-- | Get safe layout ID
mkLayoutId :: Layout gs -> SystemName
mkLayoutId = SystemName . TL.toLower . escapeSafeFilename . _systemName . _metadata

mkLayoutId' :: Layout gs -> FilePath
mkLayoutId' = TL.unpack . getSystemName . mkLayoutId

-- | Get safe layout name
mkLayoutName :: (IsString t) => Layout gs -> t
mkLayoutName = fromString . TL.unpack . escapeControlNewlines . _name . _metadata

-- | Get safe layout version
mkLayoutVersion :: (IsString t) => Layout gs -> Maybe t
mkLayoutVersion = fmap (fromString . T.unpack . V.prettyVer . unVersion) . _version . _metadata

-- | Get the semantic layout version, if defined and with correct syntax.
mkLayoutSemanticVersion :: (IsString t) => Layout gs -> Maybe t
mkLayoutSemanticVersion = fmap (fromString . T.unpack . V.prettySemVer) . (_version . _metadata >=> versionToSemVer . unVersion)

-- | Try to convert 'V.Version' to 'V.SemVer'
versionToSemVer :: V.Version -> Maybe V.SemVer
versionToSemVer (V.Version _ cs m r) =
  traverse vChunkToWord cs <&> NE.take 3 . (<> NE.repeat 0) <&> \case
    (major:minor:patch:_) -> V.SemVer major minor patch r m
    _                     -> error "impossible"
  where
    vChunkToWord = fmap concatWords . traverse vUnitToWord
    vUnitToWord (V.Digits d) = Just d
    vUnitToWord _            = Nothing
    concatWords = sum . fmap (uncurry (*)) . NE.zip [1..] . NE.reverse

-- | Get the main locale from metadata, if any
mMainLayoutLocale :: LayoutLocales -> Maybe BCP47.BCP47
mMainLayoutLocale ls
  | null pLocales = Nothing
  | otherwise = Just $ minimum pLocales -- [FIXME] better method?
  where
    pLocales = _locPrimary ls

-- | Get the main locale from metadata (default to English)
mainLayoutLocale :: LayoutLocales -> BCP47.BCP47
mainLayoutLocale ls
  | null pLocales = BCP47.mkLanguage ISO639_1.EN
  | otherwise = minimum pLocales -- [FIXME] better method?
  where
    pLocales = _locPrimary ls

{-| Get safe layout Windows type

Source: @kbd.h@
-}
mkWindowsKeyboardType :: Layout gs -> Either Natural Natural
mkWindowsKeyboardType layout
  | isValid   = Right win_type
  | otherwise = Left win_type
  where
    win_type = getRaw . _wKeyboardType . _windows . _options $ layout
    isValid =  (1 <= win_type && win_type <= 16)
            || (20 <= win_type && win_type <= 22)
            || (30 <= win_type && win_type <= 34)
            || win_type == 37 || win_type == 40 || win_type == 41
