module Klay.Typing.Method
  ( TypingConfiguration(..)
  , TypingMethod(..)
  ) where

import Data.Text qualified as T
import GHC.Generics
import Control.DeepSeq

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.KeyPosition (Row, Column)
import Klay.Keyboard.Layout (MultiOsRawLayout)
import Klay.Typing.Hand

data TypingConfiguration = TypingConfiguration
  { _keyboardLayout :: !MultiOsRawLayout
  , _typingMethod :: !TypingMethod
  } deriving (Generic, NFData)

data TypingMethod = TypingMethod
  { methodName :: !T.Text
    -- | Given a key, get the corresponding finger.
  , hfinger :: Key -> Maybe HandFinger
    -- | Unsafe version of `hfinger`.
  , unsafeHFinger :: Key -> HandFinger
    -- | Given a finger, get the corresponding home row.
  , homeRow :: Finger -> Row
    -- | Given a hand and a finger, get the corresponding home column.
  , homeColumn :: Hand -> Finger -> Column
  } deriving (Generic, NFData)
