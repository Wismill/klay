{-# LANGUAGE OverloadedLists   #-}

module Klay.Typing.Method.ISO.O0
  ( typingMethod
  , keyFingers
  ) where

import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.KeyPosition
import Klay.Typing.Hand
import Klay.Typing.Method

typingMethod :: TypingMethod
typingMethod = TypingMethod
  { methodName = "o0"
  , hfinger = hfinger'
  , unsafeHFinger = unsafeHFinger'
  , homeColumn = homeColumn'
  , homeRow = homeRow'
  }
  where
    {-# INLINE hfinger' #-}
    hfinger' = flip Map.lookup keyFingers
    {-# INLINE unsafeHFinger' #-}
    unsafeHFinger' = (Map.!) keyFingers

homeColumn' :: Hand -> Finger -> Column
homeColumn' LeftHand  ThumbFinger  = Column 3
homeColumn' LeftHand  IndexFinger  = Column 4
homeColumn' LeftHand  MiddleFinger = Column 3
homeColumn' LeftHand  RingFinger   = Column 2
homeColumn' LeftHand  LittleFinger = Column 1
homeColumn' RightHand ThumbFinger  = Column 3
homeColumn' RightHand IndexFinger  = Column 7
homeColumn' RightHand MiddleFinger = Column 8
homeColumn' RightHand RingFinger   = Column 9
homeColumn' RightHand LittleFinger = Column 10
homeColumn' AnyHand   ThumbFinger  = Column 3
homeColumn' h'        f'           = error $ mconcat ["Initial column not defined for:", show h', " ", show f']
{-# INLINE homeColumn' #-}

homeRow' :: Finger -> Row
homeRow' ThumbFinger = A
homeRow' _           = C
{-# INLINE homeRow' #-}

keyFingers :: Map Key HandFinger
keyFingers =
  [ (K.Escape, HandFinger LeftHand RingFinger)
  -- Numbers row
  , (K.Tilde    , HandFinger LeftHand LittleFinger)
  , (K.N1       , HandFinger LeftHand LittleFinger)
  , (K.N2       , HandFinger LeftHand LittleFinger)
  , (K.N3       , HandFinger LeftHand RingFinger)
  , (K.N4       , HandFinger LeftHand MiddleFinger)
  , (K.N5       , HandFinger LeftHand IndexFinger)
  , (K.N6       , HandFinger LeftHand IndexFinger)
  , (K.N7       , HandFinger RightHand IndexFinger)
  , (K.N8       , HandFinger RightHand IndexFinger)
  , (K.N9       , HandFinger RightHand MiddleFinger)
  , (K.N0       , HandFinger RightHand RingFinger)
  , (K.Minus    , HandFinger RightHand LittleFinger)
  , (K.Plus     , HandFinger RightHand LittleFinger)
  , (K.Backspace, HandFinger RightHand LittleFinger)
  -- Alphanumerical: row 1
  , (K.Tabulator, HandFinger LeftHand LittleFinger)
  , (K.Q        , HandFinger LeftHand LittleFinger)
  , (K.W        , HandFinger LeftHand RingFinger)
  , (K.E        , HandFinger LeftHand MiddleFinger)
  , (K.R        , HandFinger LeftHand IndexFinger)
  , (K.T        , HandFinger LeftHand IndexFinger)
  , (K.Y        , HandFinger RightHand IndexFinger)
  , (K.U        , HandFinger RightHand IndexFinger)
  , (K.I        , HandFinger RightHand MiddleFinger)
  , (K.O        , HandFinger RightHand RingFinger)
  , (K.P        , HandFinger RightHand LittleFinger)
  , (K.LBracket , HandFinger RightHand LittleFinger)
  , (K.RBracket , HandFinger RightHand LittleFinger)
  , (K.Return   , HandFinger RightHand LittleFinger)
  -- Alphanumerical: row 2
  , (K.CapsLock , HandFinger LeftHand LittleFinger)
  , (K.A        , HandFinger LeftHand LittleFinger)
  , (K.S        , HandFinger LeftHand RingFinger)
  , (K.D        , HandFinger LeftHand MiddleFinger)
  , (K.F        , HandFinger LeftHand IndexFinger)
  , (K.G        , HandFinger LeftHand IndexFinger)
  , (K.H        , HandFinger RightHand IndexFinger)
  , (K.J        , HandFinger RightHand IndexFinger)
  , (K.K        , HandFinger RightHand MiddleFinger)
  , (K.L        , HandFinger RightHand RingFinger)
  , (K.Semicolon, HandFinger RightHand LittleFinger)
  , (K.Quote    , HandFinger RightHand LittleFinger)
  , (K.Backslash, HandFinger RightHand LittleFinger)
  -- Alphanumerical: row 3
  , (K.LShift, HandFinger LeftHand LittleFinger)
  , (K.Iso102, HandFinger LeftHand LittleFinger)
  , (K.Z     , HandFinger LeftHand LittleFinger)
  , (K.X     , HandFinger LeftHand RingFinger)
  , (K.C     , HandFinger LeftHand MiddleFinger)
  , (K.V     , HandFinger LeftHand IndexFinger)
  , (K.B     , HandFinger LeftHand IndexFinger)
  , (K.N     , HandFinger RightHand IndexFinger)
  , (K.M     , HandFinger RightHand IndexFinger)
  , (K.Comma , HandFinger RightHand MiddleFinger)
  , (K.Period, HandFinger RightHand RingFinger)
  , (K.Slash , HandFinger RightHand LittleFinger)
  , (K.RShift, HandFinger RightHand LittleFinger)
  -- Control
  , (K.LControl  , HandFinger LeftHand LittleFinger)
  , (K.LSuper    , HandFinger LeftHand ThumbFinger)
  , (K.LAlternate, HandFinger LeftHand ThumbFinger)
  , (K.Space     , HandFinger AnyHand ThumbFinger)
  , (K.RAlternate, HandFinger RightHand ThumbFinger)
  , (K.RSuper    , HandFinger RightHand ThumbFinger)
  , (K.Menu      , HandFinger RightHand LittleFinger)
  , (K.RControl  , HandFinger RightHand LittleFinger)
  -- Cursor
  , (K.CursorLeft , HandFinger RightHand IndexFinger)
  , (K.CursorRight, HandFinger RightHand RingFinger)
  , (K.CursorUp   , HandFinger RightHand MiddleFinger)
  , (K.CursorDown , HandFinger RightHand MiddleFinger)
  -- Editing
  , (K.Delete  , HandFinger RightHand IndexFinger)
  , (K.Insert  , HandFinger RightHand IndexFinger)
  , (K.Home    , HandFinger RightHand IndexFinger)
  , (K.End     , HandFinger RightHand IndexFinger)
  , (K.PageUp  , HandFinger RightHand IndexFinger)
  , (K.PageDown, HandFinger RightHand IndexFinger)
  ]
