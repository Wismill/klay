{-# LANGUAGE OverloadedLists   #-}

module Klay.Typing.Method.TypeMatrix.Default
  ( typingMethod
  , keyFingers
  ) where


import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.KeyPosition
import Klay.Typing.Hand
import Klay.Typing.Method

typingMethod :: TypingMethod
typingMethod = TypingMethod
  { methodName = "TypeMatrix 2030 Default"
  , hfinger = hfinger'
  , unsafeHFinger = unsafeHFinger'
  , homeColumn = homeColumn'
  , homeRow = homeRow'
  }
  where
    {-# INLINE hfinger' #-}
    hfinger' = flip Map.lookup keyFingers
    {-# INLINE unsafeHFinger' #-}
    unsafeHFinger' = (Map.!) keyFingers

homeColumn' :: Hand -> Finger -> Column
homeColumn' LeftHand  ThumbFinger  = Column 3
homeColumn' LeftHand  IndexFinger  = Column 4
homeColumn' LeftHand  MiddleFinger = Column 3
homeColumn' LeftHand  RingFinger   = Column 2
homeColumn' LeftHand  LittleFinger = Column 1
homeColumn' RightHand ThumbFinger  = Column 3
homeColumn' RightHand IndexFinger  = Column 7
homeColumn' RightHand MiddleFinger = Column 8
homeColumn' RightHand RingFinger   = Column 9
homeColumn' RightHand LittleFinger = Column 10
homeColumn' AnyHand   ThumbFinger  = Column 3
homeColumn' h'        f'           = error $ mconcat ["Initial column not defined for:", show h', " ", show f']
{-# INLINE homeColumn' #-}

homeRow' :: Finger -> Row
homeRow' ThumbFinger = A
homeRow' _           = C
{-# INLINE homeRow' #-}

keyFingers :: Map Key HandFinger
keyFingers =
  [ (K.Escape, HandFinger LeftHand RingFinger)
  , (K.F1    , HandFinger LeftHand RingFinger)
  , (K.F2    , HandFinger LeftHand RingFinger)
  , (K.F3    , HandFinger LeftHand RingFinger)
  , (K.F4    , HandFinger LeftHand RingFinger)
  , (K.F5    , HandFinger LeftHand RingFinger)
  , (K.Delete, HandFinger AnyHand IndexFinger)
  , (K.F6    , HandFinger RightHand RingFinger)
  , (K.F7    , HandFinger RightHand RingFinger)
  , (K.F8    , HandFinger RightHand RingFinger)
  , (K.F9    , HandFinger RightHand RingFinger)
  , (K.F10   , HandFinger RightHand RingFinger)
  , (K.F11   , HandFinger RightHand RingFinger)
  , (K.F12   , HandFinger RightHand RingFinger)
  -- Numbers row
  , (K.Tilde     , HandFinger LeftHand LittleFinger)
  , (K.N1        , HandFinger LeftHand LittleFinger)
  , (K.N2        , HandFinger LeftHand RingFinger)
  , (K.N3        , HandFinger LeftHand MiddleFinger)
  , (K.N4        , HandFinger LeftHand IndexFinger)
  , (K.N5        , HandFinger LeftHand IndexFinger)
  , (K.Backspace , HandFinger AnyHand IndexFinger)
  , (K.N6        , HandFinger LeftHand IndexFinger)
  , (K.N7        , HandFinger RightHand IndexFinger)
  , (K.N8        , HandFinger RightHand MiddleFinger)
  , (K.N9        , HandFinger RightHand RingFinger)
  , (K.N0        , HandFinger RightHand LittleFinger)
  , (K.Minus     , HandFinger RightHand LittleFinger)
  , (K.Plus      , HandFinger RightHand LittleFinger)
  , (K.Yen       , HandFinger RightHand LittleFinger)
  , (K.Calculator, HandFinger RightHand LittleFinger)
  -- Alphanumerical: row 1
  , (K.Tabulator, HandFinger LeftHand LittleFinger)
  , (K.Q        , HandFinger LeftHand LittleFinger)
  , (K.W        , HandFinger LeftHand RingFinger)
  , (K.E        , HandFinger LeftHand MiddleFinger)
  , (K.R        , HandFinger LeftHand IndexFinger)
  , (K.T        , HandFinger LeftHand IndexFinger)
  , (K.Y        , HandFinger RightHand IndexFinger)
  , (K.U        , HandFinger RightHand IndexFinger)
  , (K.I        , HandFinger RightHand MiddleFinger)
  , (K.O        , HandFinger RightHand RingFinger)
  , (K.P        , HandFinger RightHand LittleFinger)
  , (K.LBracket , HandFinger RightHand LittleFinger)
  , (K.RBracket , HandFinger RightHand LittleFinger)
  , (K.Email1   , HandFinger RightHand LittleFinger)
  -- Alphanumerical: row 2
  , (K.A        ,  HandFinger LeftHand LittleFinger)
  , (K.S        ,  HandFinger LeftHand RingFinger)
  , (K.D        ,  HandFinger LeftHand MiddleFinger)
  , (K.F        ,  HandFinger LeftHand IndexFinger)
  , (K.G        ,  HandFinger LeftHand IndexFinger)
  , (K.Return   , HandFinger AnyHand IndexFinger)
  , (K.H        , HandFinger RightHand IndexFinger)
  , (K.J        , HandFinger RightHand IndexFinger)
  , (K.K        , HandFinger RightHand MiddleFinger)
  , (K.L        , HandFinger RightHand RingFinger)
  , (K.Semicolon, HandFinger RightHand LittleFinger)
  , (K.Quote    , HandFinger RightHand LittleFinger)
  , (K.CapsLock , HandFinger RightHand LittleFinger)
  -- Alphanumerical: row 3
  , (K.LShift          , HandFinger LeftHand LittleFinger)
  , (K.Iso102          , HandFinger LeftHand LittleFinger)
  , (K.MediaPlayPause  , HandFinger LeftHand LittleFinger)
  , (K.Z               , HandFinger LeftHand LittleFinger)
  , (K.X               , HandFinger LeftHand RingFinger)
  , (K.C               , HandFinger LeftHand MiddleFinger)
  , (K.V               , HandFinger LeftHand IndexFinger)
  , (K.B               , HandFinger LeftHand IndexFinger)
  , (K.N               , HandFinger RightHand IndexFinger)
  , (K.M               , HandFinger RightHand IndexFinger)
  , (K.Comma           , HandFinger RightHand MiddleFinger)
  , (K.Period          , HandFinger RightHand RingFinger)
  , (K.Slash           , HandFinger RightHand LittleFinger)
  , (K.Backslash       , HandFinger RightHand LittleFinger)
  , (K.JPBackSlash     , HandFinger RightHand LittleFinger)
  , (K.RShift          , HandFinger RightHand LittleFinger)
  , (K.HiraganaKatakana, HandFinger RightHand LittleFinger)
  , (K.Browser         , HandFinger RightHand LittleFinger)
  -- Control
  , (K.LControl  , HandFinger LeftHand LittleFinger)
  , (K.LSuper    , HandFinger LeftHand ThumbFinger)
  , (K.LAlternate, HandFinger LeftHand ThumbFinger)
  , (K.Muhenkan  , HandFinger LeftHand ThumbFinger)
  , (K.Space     , HandFinger AnyHand ThumbFinger)
  , (K.RAlternate, HandFinger RightHand ThumbFinger)
  , (K.RSuper    , HandFinger RightHand ThumbFinger)
  , (K.Henkan    , HandFinger RightHand ThumbFinger)
  , (K.Menu      , HandFinger RightHand LittleFinger)
  , (K.RControl  , HandFinger RightHand LittleFinger)
  -- Cursor
  , (K.Home       , HandFinger RightHand IndexFinger)
  , (K.CursorLeft , HandFinger RightHand IndexFinger)
  , (K.CursorRight, HandFinger RightHand RingFinger)
  , (K.End        , HandFinger RightHand RingFinger)
  , (K.CursorDown , HandFinger RightHand MiddleFinger)
  , (K.CursorUp   , HandFinger RightHand MiddleFinger)
  , (K.PageUp     , HandFinger RightHand LittleFinger)
  , (K.PageDown   , HandFinger RightHand LittleFinger)
  ]
