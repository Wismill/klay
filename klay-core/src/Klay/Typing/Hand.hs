module Klay.Typing.Hand
  ( HandFinger(..)
  , Hand(..)
  , Finger(..)
  ) where

import GHC.Generics
import Control.DeepSeq

-- | A human hand
data Hand
  = LeftHand  -- ^ Left hand
  | AnyHand   -- ^ Any hand
  | RightHand -- ^ Right hand
  deriving (Eq, Ord, Enum, Bounded, Show, Generic, NFData)

-- | A human finger on a specific hand
data HandFinger = HandFinger
  { _hand :: !Hand     -- ^ Hand
  , _finger :: !Finger -- ^ Finger
  } deriving (Eq, Ord, Bounded, Show, Generic, NFData)

instance Enum HandFinger where
  toEnum n = let (h, f) = quotRem n fingersCount
             in HandFinger (toEnum h) (toEnum f)
  fromEnum (HandFinger h f) = fingersCount * fromEnum h + fromEnum f

fingersCount :: Int
fingersCount = 5

-- | A human finger (both sides)
data Finger
  = ThumbFinger
  | IndexFinger
  | MiddleFinger
  | RingFinger
  | LittleFinger
  deriving (Eq, Ord, Enum, Bounded, Show, Generic, NFData)
