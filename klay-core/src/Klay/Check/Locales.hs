module Klay.Check.Locales
  ( LocalesCheckResult
  , LocaleCheckResult
  , Exemplars(..)
  , ExemplarsResult(..)
  , checkLocales
  , checkMissingCharacters
  ) where


import Data.Char (toUpper)
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Foldable
import GHC.Generics (Generic)

import Data.BCP47 (BCP47)
import Data.BCP47 qualified as BCP47
import Data.Aeson (ToJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Symbols (reachableCharacters)
import Klay.Utils.CLDR (Exemplars(..))
import Klay.Utils.CLDR qualified as CLDR
import Klay.OS (OsSpecific(OsIndependent))

type LocalesCheckResult = Locales (Map BCP47 LocaleCheckResult)
type LocaleCheckResult = Exemplars (Maybe (ExemplarsResult (Set Char)))
type LocaleUnsupportedChars = Locales (Map BCP47 (Exemplars (Maybe (Set Char))))

-- | Container for exemplar check results
data ExemplarsResult a = ExemplarsResult
  { _exSupported :: a
  -- ^ Supported examplars
  , _exNotSupported :: a
  -- ^ Unsupported exemplars
  , _exCoverage :: Double
  -- ^ Coverage: percentage of supported exemplar with respect to the tested exemplars.
  } deriving (Generic, Eq, Functor, Show)

instance Semigroup (ExemplarsResult (Set Char)) where
  (ExemplarsResult s1 n1 _) <> (ExemplarsResult s2 n2 _) = ExemplarsResult s n c
    where s = s1 <> s2
          n = (n1 <> n2) Set.\\ s
          ls = fromIntegral . length $ s
          ln = fromIntegral . length $ n
          c = ls / (ln + ls)

instance Monoid (ExemplarsResult (Set Char)) where
  mempty = ExemplarsResult mempty mempty 0

instance (ToJSON a) => ToJSON (ExemplarsResult a) where
    toEncoding = Aeson.genericToEncoding Aeson.defaultOptions

{-|
Check the 'Exemplars' of each locales defined in the layout.

The exemplar character sets contain the commonly used letters for a given modern form of a language.
See [the CLDR page](http://cldr.unicode.org/translation/-core-data/exemplars) about exemplars.
-}
checkLocales :: MultiOsRawLayout -> LocalesCheckResult
checkLocales layout = foldl' checkLocales' mempty <$> locales
  where
    locales = _locales . _metadata $ layout
    layout_chars = reachableCharacters OsIndependent layout
    checkLocales' :: Map BCP47 LocaleCheckResult -> BCP47 -> Map BCP47 LocaleCheckResult
    checkLocales' rs l = Map.insert l (checkLocale l) rs
    checkLocale :: BCP47 -> LocaleCheckResult
    checkLocale l = case CLDR.getExemplars (BCP47.toText l) of
      Nothing -> error "[ERROR] checkLocale: Locale not found" -- [FIXME] better error handling
      Just cs -> fmap check_chars <$> cs
    check_chars :: Set Char -> ExemplarsResult (Set Char)
    check_chars cs = ExemplarsResult
      { _exSupported = supported
      , _exNotSupported = notSupported
      , _exCoverage = fromIntegral (length supported) / fromIntegral (length cs')
      }
      where supported = Set.filter (`Set.member` layout_chars) cs'
            notSupported = Set.filter (`Set.notMember` layout_chars) cs'
            cs' = cs <> Set.map toUpper cs

-- | Given a layout, get the unsupported characters of the 'Exemplar's, by locale defined in the layout.
checkMissingCharacters :: MultiOsRawLayout -> Maybe LocaleUnsupportedChars
checkMissingCharacters layout
  | all null r = Nothing
  | otherwise  = Just r
  where
    r = check <$> checkLocales layout
    check ls = Map.foldrWithKey' check' mempty ls
    check' l rs acc | rs' == mempty = acc | otherwise = Map.insert l rs' acc
      where
        rs' = f . fmap _exNotSupported <$> rs
        f Nothing = Nothing
        f (Just cs) | null cs = Nothing | otherwise = Just cs
