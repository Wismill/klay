{-# LANGUAGE OverloadedLists       #-}

module Klay.Export.Kmonad
  ( generateKmonad
  ) where

import Data.Maybe (fromMaybe)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.List (intersperse, stripPrefix)
import Data.Char (toLower)
import Data.String (fromString)
import Data.Text.Lazy.Builder qualified as TB
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.IO.Utf8 qualified as Utf8
import System.FilePath
import System.Directory
  (createDirectoryIfMissing, copyFile, getPermissions, setPermissions, setOwnerExecutable)

import Data.Aeson
import Text.Megaparsec qualified as Mega
import Text.Mustache qualified as Mustache

import Paths_klay_core (getDataFileName)
import Klay.Keyboard.Hardware.ButtonAction (ButtonAction(..), Next(..), Release(..))
import Klay.Keyboard.Hardware.Key (keyToKeycodeMap)
import Klay.Keyboard.Layout
import Klay.Export.Linux.XKB.Header (createHeader)
import Klay.Utils.UserInput

-- | Generator for Kmonad <https://github.com/david-janssen/kmonad>
generateKmonad :: FilePath -> MultiOsRawLayout -> IO ()
generateKmonad destination_dir layout = do
  createDirectoryStructure destination_dir
  header <- createHeader layout (";;" :: TL.Text)
  generateConfig destination_dir "kmonad/template.kbd" template_path (make_vars header)
  generateConfig destination_dir "kmonad/launch.sh" "launch" script_vars
  copyScript destination_dir "kmonad/switch.sh" "switch"
  where
    template_path = replaceExtension system_name ".kbd.template"
    system_name = mkLayoutId' layout
    layout_name = _systemName . _metadata $ layout
    make_vars header = object
      [ "header" .= header
      , "layout" .= escapeControlNewlines layout_name
      , "aliases" .= aliases
      , "source" .= mapping_src
      , "layer1" .= mapping_base ]
    script_vars = object ["layout" .= system_name]
    (aliases, mapping_src, mapping_base) = getKmonadMappings layout

generateConfig :: FilePath -> FilePath -> FilePath -> Value -> IO ()
generateConfig destination_dir template_file destinationFile vars = do
  template_file' <- getDataFileName template_file
  templateContent <- TL.toStrict <$> Utf8.readFile template_file'
  case Mustache.compileMustacheText (fromString template_file) templateContent of
    Left bundle -> do
      putStrLn $ "[ERROR] Kmonad: Cannot compile Mustache template: " <> template_file
      putStrLn (Mega.errorBundlePretty bundle)
    Right template -> do
      let content = Mustache.renderMustache template vars
      let destinationFile' = destination_dir </> destinationFile
      Utf8.writeFile destinationFile' content

copyScript :: FilePath -> FilePath -> FilePath -> IO ()
copyScript destination_dir source_file destinationFile = do
  source_file' <- getDataFileName source_file
  let destinationFile' = destination_dir </> destinationFile
  copyFile source_file' destinationFile'
  perms <- getPermissions destinationFile'
  setPermissions destinationFile' (setOwnerExecutable True perms)

-- | Get the mapping for Kmonad
getKmonadMappings :: MultiOsRawLayout -> (TL.Text, TL.Text, TL.Text)
getKmonadMappings layout = (mk aliases, mk mapping_src, mk mapping_base)
  where
    (aliases, mapping_src, mapping_base) = getKmonadActions layout
    mk = TB.toLazyText . mconcat . intersperse "\n"

-- | Get Kmonad mapping from the virtual keys definitions
getKmonadActions :: MultiOsRawLayout -> ([TB.Builder], [TB.Builder], [TB.Builder])
getKmonadActions layout = foldr mkActions mempty (enumFromTo minBound maxBound)
  where
    dual_functions = _dualFunctionKeys layout
    mkActions :: Key -> ([TB.Builder], [TB.Builder], [TB.Builder]) -> ([TB.Builder], [TB.Builder], [TB.Builder])
    mkActions key = mkActions' key $ Map.lookup key dual_functions
    mkActions' :: Key -> Maybe (ButtonAction Key) -> ([TB.Builder], [TB.Builder], [TB.Builder]) -> ([TB.Builder], [TB.Builder], [TB.Builder])
    mkActions' _ Nothing acc = acc
    mkActions' key (Just ka) (as, ss, bs) = (maybe as (:as) a, s:ss, b:bs)
      where
        (a, s, b) = case ka of
          NormalKey _ -> mkNormal key
          DualKey t h Nothing _ r -> mkTapNextKey key t h r
          DualKey t h (Just d) NoNext Release -> mkTapHoldKey key t h d NoNext NoRelease
          DualKey t h (Just d) n r -> mkTapHoldKey key t h d n r
    indent = "  "
    mkNormal key =
      ( Nothing
      , indent <> key'
      , indent <> key' )
      where
        key' = toKmonadKey key
    mkTapHoldKey k t h d n r =
      ( Just $ mconcat [indent, k', " (tap-hold", next, release, " ", TB.fromString $ show d, " ", toKmonadKey t, " ", toKmonadKey h, ")"]
      , indent <> k'
      , indent <> a )
      where
        next = case n of
          Next   -> "-next"
          NoNext -> mempty
        release = case r of
          Release   -> "-release"
          NoRelease -> mempty
        k' = toKmonadKey k
        a = "@" <> k'
    mkTapNextKey k t h r =
      ( Just $ mconcat [indent, k', " (tap-next", release, " ", toKmonadKey t, " ", toKmonadKey h, ")"]
      , indent <> k'
      , indent <> a )
      where
        release = case r of
          Release   -> "-release"
          NoRelease -> mempty
        k' = toKmonadKey k
        a = "@" <> k'

-- | Convert a key to its Kmonad equivalent
toKmonadKey :: Key -> TB.Builder
toKmonadKey key = Map.findWithDefault err key kmonadKeyMap
  where err = error $ "[ERROR] Kmonad: key not supported: " <> show key

{-|
Map of the keys to their Kmonad equivalent.

Kmonad key codes are the [Linux input event codes](https://github.com/david-janssen/kmonad/blob/master/src/KMonad/Keyboard/Keycode.hs)
without the @KEY_@ prefix.
-}
kmonadKeyMap :: Map Key TB.Builder
kmonadKeyMap = Map.map toKmonadKeycode keyToKeycodeMap
  where
    toKmonadKeycode code =
      let code_str = show code
      in TB.fromString . fmap toLower . fromMaybe code_str . stripPrefix "KEY_" $ code_str

-- | Create a directory structure required by Kmonad.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure = createDirectoryIfMissing True
