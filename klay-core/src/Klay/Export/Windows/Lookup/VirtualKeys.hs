module Klay.Export.Windows.Lookup.VirtualKeys
  ( toWinVkPrefixed
  , toWinVkPrefixedQuoted
  , toWinVkUnprefixed
  , toWinVkUnprefixedQuoted
  , parseVk
  , nativeActionVk
  , nativeNonPunctuationActionVk
  , nativeSpecialActionVk
  , isRemappableVk
  , isReservedVk
  ) where

import Prelude hiding (Left, Right)
import Data.Ix (Ix(..))
import Data.Text qualified as T
import Data.Text.Lazy.Builder qualified as TB
import Data.HashMap.Strict qualified as HMap
import Control.Arrow ((&&&))

import TextShow (showt)

import Klay.Export.Windows.Lookup.Characters (nativeCharVk, nonPunctuationNativeCharVk)
import Klay.Export.Windows.Lookup.Modifiers (nativeModifierVk)
import Klay.Keyboard.Layout.Action (Action(..), SpecialAction(..))
import Klay.Keyboard.Layout.VirtualKey qualified as VK

-- | Convert a virtual key into its Windows symbol, with the "VK_" prefix
toWinVkPrefixed :: VK.VirtualKey -> TB.Builder
toWinVkPrefixed vk = let t = showt vk in case T.uncons (T.drop 3 t) of
  Just (c, "") -> TB.singleton c
  _            -> TB.fromText t

-- | Convert a virtual key into its Windows symbol, with the "VK_" prefix and quoted for single char
toWinVkPrefixedQuoted :: VK.VirtualKey -> TB.Builder
toWinVkPrefixedQuoted vk = let t = showt vk in case T.uncons (T.drop 3 t) of
  Just (c, "") -> mconcat ["'", TB.singleton c, "'"]
  _            -> TB.fromText t

-- | Convert a virtual key into its Windows symbol, without the "VK_" prefix
toWinVkUnprefixed :: VK.VirtualKey -> TB.Builder
toWinVkUnprefixed = TB.fromText . T.drop 3 . showt

-- | Convert a virtual key into its Windows symbol, without the "VK_" prefix and quoted for single char
toWinVkUnprefixedQuoted :: VK.VirtualKey -> TB.Builder
toWinVkUnprefixedQuoted vk = let t = T.drop 3 (showt vk) in case T.uncons t of
  Just (c, "") -> mconcat ["'", TB.singleton c, "'"]
  _            -> TB.fromText t

-- [TODO] Complete the list
-- | Indicate if a virtual key can be remapped in Windows
isRemappableVk :: VK.VirtualKey -> Bool
isRemappableVk VK.VK_CANCEL  = True
isRemappableVk VK.VK_ESCAPE  = True
isRemappableVk VK.VK_SPACE   = True
isRemappableVk VK.VK_SELECT  = True
isRemappableVk VK.VK_EXECUTE = True
isRemappableVk vk
  =  inRange (VK.VK_BACK, VK.VK_RETURN) vk
  || inRange (VK.VK_0, VK.VK_9) vk
  || inRange (VK.VK_A, VK.VK_Z) vk
  || inRange (VK.VK_NUMPAD0, VK.VK_DIVIDE) vk
  || inRange (VK.VK_OEM_FJ_MASSHOU, VK.VK_OEM_FJ_ROYA) vk
  || inRange (VK.VK_OEM_1, VK.VK_ABNT_C2) vk
  || inRange (VK.VK_OEM_4, VK.VK_ICO_HELP) vk
  || inRange (VK.VK_PROCESSKEY, maxBound) vk

-- | Indicate if a virtual key is reserved for a special use in Windows or in Klay
isReservedVk :: VK.VirtualKey -> Bool
isReservedVk VK.VK_OEM_PA1 = True -- Encodes Numeric modifier
isReservedVk VK.VK_OEM_PA2 = True -- Encodes IsoLevel3 modifier
isReservedVk VK.VK_OEM_PA3 = True -- Encodes IsoLevel5 modifier
isReservedVk vk            = not (isRemappableVk vk)

nativeActionVk :: Action -> Maybe VK.VirtualKey
nativeActionVk (AChar c)     = nativeCharVk c
nativeActionVk (AModifier m) = nativeModifierVk m
nativeActionVk (ASpecial a)  = nativeSpecialActionVk a
nativeActionVk _             = Nothing

nativeNonPunctuationActionVk :: Action -> Maybe VK.VirtualKey
nativeNonPunctuationActionVk (AChar c)     = nonPunctuationNativeCharVk c
nativeNonPunctuationActionVk (AModifier m) = nativeModifierVk m
nativeNonPunctuationActionVk (ASpecial a)  = nativeSpecialActionVk a
nativeNonPunctuationActionVk _             = Nothing

-- [TODO] This is more or less restricted to type 4 keyboards. Explain why
nativeSpecialActionVk :: SpecialAction -> Maybe VK.VirtualKey
nativeSpecialActionVk BackSpace   = Just VK.VK_BACK
nativeSpecialActionVk Tab         = Just VK.VK_TAB
nativeSpecialActionVk Return      = Just VK.VK_RETURN
nativeSpecialActionVk Pause       = Just VK.VK_PAUSE
nativeSpecialActionVk Scroll_Lock = Just VK.VK_SCROLL
-- nativeSpecialActionVk Sys_Req = Just VK.VK_
nativeSpecialActionVk Escape = Just VK.VK_ESCAPE
nativeSpecialActionVk Delete = Just VK.VK_DELETE
nativeSpecialActionVk Muhenkan  = Just VK.VK_NONCONVERT
nativeSpecialActionVk Henkan    = Just VK.VK_CONVERT
nativeSpecialActionVk Hiragana_Katakana = Just VK.VK_KANA
nativeSpecialActionVk Home   = Just VK.VK_HOME
nativeSpecialActionVk Left   = Just VK.VK_LEFT
nativeSpecialActionVk Up     = Just VK.VK_UP
nativeSpecialActionVk Right  = Just VK.VK_RIGHT
nativeSpecialActionVk Down   = Just VK.VK_DOWN
nativeSpecialActionVk Prior  = Just VK.VK_PRIOR
nativeSpecialActionVk Next   = Just VK.VK_NEXT
nativeSpecialActionVk End    = Just VK.VK_END
nativeSpecialActionVk Print  = Just VK.VK_SNAPSHOT
nativeSpecialActionVk Insert = Just VK.VK_INSERT
nativeSpecialActionVk Menu   = Just VK.VK_APPS
-- Not native: Find
nativeSpecialActionVk Help  = Just VK.VK_HELP
nativeSpecialActionVk Break = Just VK.VK_CANCEL
-- Note: Windows uses a special mechanism for KPEnter
-- nativeSpecialActionVk KP_Enter = Just VK.VK_RETURN
nativeSpecialActionVk KP_Home  = Just VK.VK_NUMPAD7
nativeSpecialActionVk KP_Left  = Just VK.VK_NUMPAD4
nativeSpecialActionVk KP_Up    = Just VK.VK_NUMPAD8
nativeSpecialActionVk KP_Right = Just VK.VK_NUMPAD6
nativeSpecialActionVk KP_Down  = Just VK.VK_NUMPAD2
nativeSpecialActionVk KP_Prior = Just VK.VK_NUMPAD9
nativeSpecialActionVk KP_Next  = Just VK.VK_NUMPAD3
nativeSpecialActionVk KP_End   = Just VK.VK_NUMPAD1
-- Not native: KP_Begin
nativeSpecialActionVk KP_Insert = Just VK.VK_NUMPAD0
nativeSpecialActionVk KP_Delete = Just VK.VK_DECIMAL
-- Not native: KP_Equal
nativeSpecialActionVk KP_0 = Just VK.VK_NUMPAD0
nativeSpecialActionVk KP_1 = Just VK.VK_NUMPAD1
nativeSpecialActionVk KP_2 = Just VK.VK_NUMPAD2
nativeSpecialActionVk KP_3 = Just VK.VK_NUMPAD3
nativeSpecialActionVk KP_4 = Just VK.VK_NUMPAD4
nativeSpecialActionVk KP_5 = Just VK.VK_NUMPAD5
nativeSpecialActionVk KP_6 = Just VK.VK_NUMPAD6
nativeSpecialActionVk KP_7 = Just VK.VK_NUMPAD7
nativeSpecialActionVk KP_8 = Just VK.VK_NUMPAD8
nativeSpecialActionVk KP_9 = Just VK.VK_NUMPAD9
nativeSpecialActionVk KP_Multiply = Just VK.VK_MULTIPLY
nativeSpecialActionVk KP_Add = Just VK.VK_ADD
-- Special case: see Export.Windows.Common.Layout.hs
-- nativeSpecialActionVk KP_Separator = Just VK.VK_SEPARATOR
nativeSpecialActionVk KP_Subtract = Just VK.VK_SUBTRACT
-- Special case: see Export.Windows.Common.Layout.hs
-- nativeSpecialActionVk KP_Decimal = Just VK.VK_DECIMAL
nativeSpecialActionVk KP_Divide = Just VK.VK_DIVIDE
nativeSpecialActionVk F1  = Just VK.VK_F1
nativeSpecialActionVk F2  = Just VK.VK_F2
nativeSpecialActionVk F3  = Just VK.VK_F3
nativeSpecialActionVk F4  = Just VK.VK_F4
nativeSpecialActionVk F5  = Just VK.VK_F5
nativeSpecialActionVk F6  = Just VK.VK_F6
nativeSpecialActionVk F7  = Just VK.VK_F7
nativeSpecialActionVk F8  = Just VK.VK_F8
nativeSpecialActionVk F9  = Just VK.VK_F9
nativeSpecialActionVk F10 = Just VK.VK_F10
nativeSpecialActionVk F11 = Just VK.VK_F11
nativeSpecialActionVk F12 = Just VK.VK_F12
nativeSpecialActionVk F13 = Just VK.VK_F13
nativeSpecialActionVk F14 = Just VK.VK_F14
nativeSpecialActionVk F15 = Just VK.VK_F15
nativeSpecialActionVk F16 = Just VK.VK_F16
nativeSpecialActionVk F17 = Just VK.VK_F17
nativeSpecialActionVk F18 = Just VK.VK_F18
nativeSpecialActionVk F19 = Just VK.VK_F19
nativeSpecialActionVk F20 = Just VK.VK_F20
nativeSpecialActionVk F21 = Just VK.VK_F21
nativeSpecialActionVk F22 = Just VK.VK_F22
nativeSpecialActionVk F23 = Just VK.VK_F23
nativeSpecialActionVk F24 = Just VK.VK_F24
nativeSpecialActionVk XF86AudioLowerVolume = Just VK.VK_VOLUME_DOWN
nativeSpecialActionVk XF86AudioMute = Just VK.VK_VOLUME_MUTE
nativeSpecialActionVk XF86AudioRaiseVolume = Just VK.VK_VOLUME_UP
nativeSpecialActionVk XF86AudioPlay = Just VK.VK_MEDIA_PLAY_PAUSE
nativeSpecialActionVk XF86AudioStop = Just VK.VK_MEDIA_STOP
nativeSpecialActionVk XF86AudioPrev = Just VK.VK_MEDIA_PREV_TRACK
nativeSpecialActionVk XF86AudioNext = Just VK.VK_MEDIA_NEXT_TRACK
nativeSpecialActionVk XF86HomePage = Just VK.VK_BROWSER_HOME
nativeSpecialActionVk XF86Mail = Just VK.VK_LAUNCH_MAIL
nativeSpecialActionVk XF86Search = Just VK.VK_BROWSER_SEARCH
-- nativeSpecialActionVk XF86Calculator = Just VK.VK_
nativeSpecialActionVk XF86Back = Just VK.VK_BROWSER_BACK
nativeSpecialActionVk XF86Forward = Just VK.VK_BROWSER_FORWARD
nativeSpecialActionVk XF86Stop = Just VK.VK_BROWSER_STOP
nativeSpecialActionVk XF86Refresh = Just VK.VK_BROWSER_REFRESH
-- nativeSpecialActionVk XF86PowerOff = Just VK.VK_
-- nativeSpecialActionVk XF86WWW = Just VK.VK_
nativeSpecialActionVk XF86Favorites = Just VK.VK_BROWSER_FAVORITES
nativeSpecialActionVk XF86AudioMedia = Just VK.VK_LAUNCH_MEDIA_SELECT
-- nativeSpecialActionVk XF86Documents = Just VK.VK_
-- nativeSpecialActionVk XF86Explorer = Just VK.VK_
nativeSpecialActionVk Hangul = Just VK.VK_HANGEUL
nativeSpecialActionVk Hangul_Hanja = Just VK.VK_HANJA
-- [TODO]
-- nativeSpecialActionVk Kanji = Just VK.
-- nativeSpecialActionVk Romaji = Just VK.
-- nativeSpecialActionVk KanaLock = Just VK.
-- nativeSpecialActionVk KanaShift = Just VK.
nativeSpecialActionVk _ = Nothing

virtualKeysMap :: HMap.HashMap T.Text VK.VirtualKey
virtualKeysMap = HMap.fromList . fmap go $ enumFromTo minBound maxBound
  where go = T.drop 3 . showt &&& id

parseVk :: T.Text -> Maybe VK.VirtualKey
parseVk = (`HMap.lookup` virtualKeysMap)
