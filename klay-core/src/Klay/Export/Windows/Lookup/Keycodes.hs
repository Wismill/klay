module Klay.Export.Windows.Lookup.Keycodes
  ( toWinKeycode
  , winKeyCodesMap
  , winKeyCodes
  ) where

import Data.String (IsString(..))
import Data.Map.Strict qualified as Map

import Klay.Keyboard.Hardware.Key (Key(..))

-- [NOTE] These codes are the constants for scancodes defined in `kbd.h` and their locale variant.
-- [NOTE] The VK codes are defined in `winuser.h`

toWinKeycode :: (IsString s) => Key -> Maybe s
toWinKeycode = fmap fromString . flip Map.lookup winKeyCodesMap

winKeyCodesMap :: Map.Map Key String
winKeyCodesMap = Map.fromList winKeyCodes


{-| Windows scan codes

These are valid for type 4 keyboards.
Some scan codes may be valid for type 8 [TODO].

The keys @PrtSc\/SysRq@ and @Pause\/Break@ are special.
The former produces scancode @e0 2a e0 37@ when no modifier key is pressed simultaneously,
@e0 37@ together with Shift or Ctrl, but @54@ together with Alt.
The latter produces scancode sequence @e1 1d 45 e1 9d c5@ when pressed (without modifier).
However, together with Ctrl, one gets @e0 46 e0 c6@.

Sources:

- [USB HID to PS\/2 Scan Code Translation Table](https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-rawkeyboard)
- [Kbd project](https://kbd-project.org/docs/scancodes/scancodes-1.html)
- [Accentuez mon nom](http://accentuez.mon.nom.free.fr/Scancode.php)
- [Qemu](https://github.com/qemu/keycodemapdb/blob/master/data/keymaps.csv)
-}
winKeyCodes :: [(Key, String)]
winKeyCodes =
  -- ================== Section: Editing-and-function =========================
  -- Zone: Editing function ---------------------------------------------------
  -- System keys
  [ (PrintScreen, "X37")
  , (ScrollLock,  "T46")
  , (Pause,       "Y1D") -- [TODO] check Pause
  -- Function keys
  , (F1,  "T3B")
  , (F2,  "T3C")
  , (F3,  "T3D")
  , (F4,  "T3E")
  , (F5,  "T3F")
  , (F6,  "T40")
  , (F7,  "T41")
  , (F8,  "T42")
  , (F9,  "T43")
  , (F10, "T44")
  , (F11, "T57")
  , (F12, "T58")
  , (F13, "T64")
  , (F14, "T65")
  , (F15, "T66")
  , (F16, "T67")
  , (F17, "T68")
  , (F18, "T69")
  , (F19, "T6A")
  , (F20, "T6B")
  , (F21, "T6C")
  , (F22, "T6D")
  , (F23, "T6E")
  , (F24, "T76")
  -- Movements
  , (Home, "X47")
  , (End, "X4F")
  , (PageUp, "X49")
  , (PageDown, "X51")
  -- Miscellaneous
  , (Escape, "T01")
  -- Zone: Cursor key ---------------------------------------------------------
  , (CursorUp, "X48")
  , (CursorDown, "X50")
  , (CursorLeft, "X4B")
  , (CursorRight, "X4D")
  -- Zone: Optional -----------------------------------------------------------
  -- [TODO] ScreenLock
  , (Stop, "X68")
  -- [TODO] Cancel
  -- [TODO] Save
  -- Media control
  -- [TODO] MediaPlayCD | MediaPlay
  -- [TODO] , (MediaPause, "X22")
  , (MediaPlayPause, "X22")
  , (MediaStop, "X24")
  -- , (MediaEject, "X2C")
  , (MediaPrevious, "X10")
  , (MediaNext, "X19")
  -- , (MediaRewind, "X18")
  -- , (MediaForward, "X34")
  , (VolumeMute, "X20")
  , (VolumeDown, "X2E")
  , (VolumeUp, "X30")

  -- Browser control
  , (BrowserBack, "X6A")
  , (BrowserForward, "X69")
  , (BrowserRefresh, "X67")
  , (BrowserSearch, "X65")
  , (BrowserFavorites, "X66")
  , (BrowserHomePage, "X32")
  -- BrowserStop X68 = Stop

  -- Applications
  , (Calculator, "X21")
  , (MediaPlayer, "X6D")
  -- , (Browser, "X02")
  , (Email1, "X6C")
  -- , (Email2, "X3F")
  -- , (Search, "X65")
  -- , (Find, "X41")
  -- , (Explorer, "T67")
  , (MyComputer, "X6B")
  -- , (Documents, "X70")
  -- , (Help, "X75")
  -- , (Launch1, "X6B") = MyComputer
  -- , (Launch2, "X21") = Calculator
  -- , (Launch3, "X2B")
  -- , (Launch4, "X2C")
  -- Power keys
  , (Power, "X5E")
  -- , (Suspend, "X25")
  , (Sleep, "X5F")
  , (WakeUp, "X63")
  -- [TODO] BrightnessDown | BrightnessUp
  -- ================== Section: numeric keypad =============================
  -- Numeric: first row
  , (KPDivide,   "X35")
  , (KPMultiply, "T37")
  , (KPSubtract, "T4A")
  -- Numeric: second row
  , (KP7,        "T47")
  , (KP8,        "T48")
  , (KP9,        "T49")
  -- Numeric: third row
  , (KP4,        "T4B")
  , (KP5,        "T4C")
  , (KP6,        "T4D")
  , (KPAdd,      "T4E")
  -- Numeric: fourth row
  , (KP1,        "T4F")
  , (KP2,        "T50")
  , (KP3,        "T51")
  -- Numeric: fifth row
  , (KP0,        "T52")
  , (KPDecimal,  "T53")
  , (KPEnter,    "X1C")
  , (KPEquals,   "T59")
  -- , (KPPlusMinus, "X4E") -- LK411 keyboard. Labelled "—" [NOTE] Correct, but need to include in C template
  , (KPJPComma,  "T5C")
  , (KPComma,    "T7E")
  -- ================== Section: alphanumeric =================================
  -- Zone: function -----------------------------------------------------------
  -- Edition
  , (Tabulator,  "T0F")
  , (Return, "T1C")
  , (Backspace, "T0E")
  , (Insert, "X52")
  , (Delete, "X53")
  -- [TODO] Undo | Redo | Cut | Copy | Paste
  -- Miscellaneous
  , (Menu, "X5D")
  -- International keys: Japanese
  , (Muhenkan, "T7B")
  , (Henkan, "T79")
  , (Katakana, "T78")
  , (Hiragana, "T77")
  , (HiraganaKatakana, "T70")
  -- Zenkaku/Hankaku T76 = F24

  -- International keys: Korean
  , (Hangeul, "XF2")
  , (Hanja, "XF1")

  -- Modifiers
  , (LShift,     "T2A")
  , (RShift,     "T36")
  , (CapsLock,   "T3A")
  , (LControl,   "T1D")
  , (RControl,   "X1D")
  , (LAlternate, "T38")
  , (RAlternate, "X38")
  , (LSuper,     "X5B")
  , (RSuper,     "X5C")
  , (NumLock,    "T45")
  -- Zone: alphanumeric -------------------------------------------------------
  , (Space,      "T39")
  -- Alphanumeric section: first row
  , (Tilde,      "T29")
  , (N1,         "T02")
  , (N2,         "T03")
  , (N3,         "T04")
  , (N4,         "T05")
  , (N5,         "T06")
  , (N6,         "T07")
  , (N7,         "T08")
  , (N8,         "T09")
  , (N9,         "T0A")
  , (N0,         "T0B")
  , (Minus,      "T0C")
  , (Plus,       "T0D")
  , (Backspace,  "T0E")
  -- Alphanumeric section: second row
  , (Q,          "T10")
  , (W,          "T11")
  , (E,          "T12")
  , (R,          "T13")
  , (T,          "T14")
  , (Y,          "T15")
  , (U,          "T16")
  , (I,          "T17")
  , (O,          "T18")
  , (P,          "T19")
  , (LBracket,   "T1A")
  , (RBracket,   "T1B")
  -- Alphanumeric section: third row
  , (A,          "T1E")
  , (S,          "T1F")
  , (D,          "T20")
  , (F,          "T21")
  , (G,          "T22")
  , (H,          "T23")
  , (J,          "T24")
  , (K,          "T25")
  , (L,          "T26")
  , (Semicolon,  "T27")
  , (Quote,      "T28")
  , (Backslash,  "T2B")
  -- Alphanumeric section: fourth row
  , (Iso102,     "T56")
  , (Z,          "T2C")
  , (X,          "T2D")
  , (C,          "T2E")
  , (V,          "T2F")
  , (B,          "T30")
  , (N,          "T31")
  , (M,          "T32")
  , (Comma,      "T33")
  , (Period,     "T34")
  , (Slash,      "T35")
  -- [FIXME] Add missing keys
  , (Yen,         "T7D")
  , (Ro,          "T73")
  ]
