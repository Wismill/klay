{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Lookup.Characters
  ( WindowsLigature(..)
  , WindowsChar(..)
  , toWinSymbol
  , toWcharUnprefixed
  , toWcharUnprefixed'
  , toWcharUnprefixedAll
  , toWinDkSymbol
  , toQuotedWinSymbolLong
  , toQuotedWinSymbolShort
  , quoteWcharLong
  , quoteWcharShort
  , quoteWstring
  , toHex
  , toHex'
  , toWinEncoding
  , toWinEncoding'
  , fromWinEncoding
  , nonPunctuationNativeCharVk
  , nativeCharVk
  ) where


import Data.Word (Word8)
import Data.Char (ord, isControl, isAscii, isAlphaNum, isSpace, isMark) -- isPrint
import Data.Text qualified as T
import Data.Text.Encoding qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Text.Lazy.Encoding qualified as TL
import Data.ByteString qualified as BS
import Data.ByteString.Lazy qualified as BSL
import Control.Monad ((>=>))

import Klay.Keyboard.Hardware.Key (charToKey, nonPunctuationCharToKey)
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.Utils.Unicode (showHexB, padL)


newtype WindowsLigature = WindowsLigature [Word8]
  deriving newtype (Eq, Show)

data WindowsChar
  = WChar !Char      -- Not escaped
  | WEscaped !Char   -- Escaped with backslash
  | WHex !TB.Builder -- Escaped as hexadecimal
  deriving (Eq, Show)

toQuotedWinSymbolLong :: Char -> Either Char TB.Builder
toQuotedWinSymbolLong = fmap quoteWcharLong . toWinSymbol

toQuotedWinSymbolShort :: Char -> Either Char TB.Builder
toQuotedWinSymbolShort = fmap quoteWcharShort . toWinSymbol

quoteWcharLong :: WindowsChar -> TB.Builder
quoteWcharLong (WChar c)    = mconcat ["L'", TB.singleton c, "'"]
quoteWcharLong (WEscaped e) = mconcat ["L'", escapeChar e, "'"]
quoteWcharLong (WHex h)     = mconcat ["0x", h]

quoteWcharShort :: WindowsChar -> TB.Builder
quoteWcharShort (WChar c)    = mconcat ["u'", TB.singleton c, "'"]
quoteWcharShort (WEscaped e) = mconcat ["u'", escapeChar e, "'"]
quoteWcharShort (WHex h)     = mconcat ["0x", h]

quoteWstring :: WindowsChar -> TB.Builder
quoteWstring (WChar c)    = mconcat ["L\"", TB.singleton c, "\""]
quoteWstring (WEscaped e) = mconcat ["L\"", escapeChar e, "\""]
quoteWstring (WHex h)     = mconcat ["L\"\\u", h, "\""]

toWinSymbol :: Char -> Either Char WindowsChar
toWinSymbol '\0' = Right (WEscaped '\0')
toWinSymbol '\t' = Right (WEscaped '\t')
toWinSymbol '\n' = Right (WEscaped '\n')
toWinSymbol '\r' = Right (WEscaped '\r')
toWinSymbol '\'' = Right (WEscaped '\'')
toWinSymbol '\\' = Right (WEscaped '\\')
toWinSymbol ' '  = Right (WChar ' ')
toWinSymbol c
  | isNotBMP  = Left c -- Non-BMP
  | isSpecial = Right (WHex . toHex . ord $ c)
  | isAscii c = Right (WChar c)
  | otherwise = Right (WHex . toHex . ord $ c)
  where
    isSpecial = isSpace c || isMark c || isControl c
    isNotBMP = c > '\xFFFF'

toWcharUnprefixed :: Char -> Either Char TB.Builder
toWcharUnprefixed c
  | isNotBMP                  = Left c -- Non-BMP
  | isAscii c && isAlphaNum c = Right (TB.singleton c)
  | otherwise                 = Right (toHex . ord $ c)
  where
    isNotBMP = c > '\xFFFF'

toWcharUnprefixed' :: WindowsChar -> TB.Builder
toWcharUnprefixed' (WEscaped c) = toHex . ord $ c
toWcharUnprefixed' (WHex h)     = h
toWcharUnprefixed' (WChar c)
  | isAscii c && isAlphaNum c = TB.singleton c
  | otherwise                 = toHex . ord $ c

toWcharUnprefixedAll :: Char -> TB.Builder
toWcharUnprefixedAll c
  | isAscii c && isAlphaNum c = TB.singleton c
  | otherwise                 = toHex . ord $ c

toWinDkSymbol :: Char -> Either Char WindowsChar
toWinDkSymbol c
  | ord c <= 0x7fff = toWinSymbol c
  | otherwise       = Left c

escapeChar :: Char -> TB.Builder
escapeChar '\0' = "\\0"
escapeChar '\t' = "\\t"
escapeChar '\n' = "\\n"
escapeChar '\r' = "\\r"
escapeChar '\'' = "\\'"
escapeChar '\\' = "\\\\"
escapeChar c    = TB.singleton c

toHex :: Int -> TB.Builder
toHex = padL 4 '0' . showHexB

toHex' :: WindowsChar -> TB.Builder
toHex' (WEscaped c) = toHex . ord $ c
toHex' (WHex h)     = h
toHex' (WChar c)    = toHex . ord $ c

toWinEncoding :: Char -> [TB.Builder]
toWinEncoding = fmap (("0x" <>) . toHex . fromIntegral) . BSL.unpack . TL.encodeUtf16LE . TL.singleton

toWinEncoding' :: Char -> [TB.Builder]
toWinEncoding' = fmap (toHex . fromIntegral) . BSL.unpack . TL.encodeUtf16LE . TL.singleton

fromWinEncoding :: [Word8] -> T.Text
fromWinEncoding = T.decodeUtf16LE . BS.pack

nonPunctuationNativeCharVk :: Char -> Maybe VK.VirtualKey
nonPunctuationNativeCharVk = nonPunctuationCharToKey >=> VK.keyNativeVirtualKey4

nativeCharVk :: Char -> Maybe VK.VirtualKey
nativeCharVk = charToKey >=> VK.keyNativeVirtualKey4
