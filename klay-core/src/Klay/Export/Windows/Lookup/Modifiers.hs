{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Lookup.Modifiers
  ( nativeModifierVk
  , toWinModifierBit
  , toModifierBit
  ) where

import Data.Bits (shift)
import Data.Text.Lazy.Builder qualified as TB

import Klay.Keyboard.Layout.Modifier (Modifier(..), ModifierBit(..), ModifierVariant(..), ModifierEffect(..))
import Klay.Keyboard.Layout.VirtualKey qualified as VK


nativeModifierVk :: Modifier -> Maybe VK.VirtualKey
nativeModifierVk (Modifier [Shift]              L Set)  = Just VK.VK_LSHIFT
nativeModifierVk (Modifier [Shift]              R Set)  = Just VK.VK_RSHIFT
nativeModifierVk (Modifier [Shift]              N Set)  = Just VK.VK_SHIFT
nativeModifierVk (Modifier [Capitals]           N Lock) = Just VK.VK_CAPITAL
nativeModifierVk (Modifier [Control]            L Set)  = Just VK.VK_LCONTROL
nativeModifierVk (Modifier [Control]            R Set)  = Just VK.VK_RCONTROL
nativeModifierVk (Modifier [Control]            N Set)  = Just VK.VK_CONTROL
nativeModifierVk (Modifier [Alternate]          L Set)  = Just VK.VK_LMENU
nativeModifierVk (Modifier [Alternate]          R Set)  = Just VK.VK_RMENU
nativeModifierVk (Modifier [Alternate]          N Set)  = Just VK.VK_MENU
nativeModifierVk (Modifier [Control, Alternate] R Set)  = Just VK.VK_RMENU
nativeModifierVk (Modifier [Super]              L Set)  = Just VK.VK_LWIN
nativeModifierVk (Modifier [Super]              R Set)  = Just VK.VK_RWIN
nativeModifierVk (Modifier [Numeric]            _ Set)  = Just VK.VK_OEM_PA1
nativeModifierVk (Modifier [Numeric]            N Lock) = Just VK.VK_NUMLOCK -- [TODO] GROUP OR NUMERIC
nativeModifierVk (Modifier [IsoLevel3]          _ Set)  = Just VK.VK_OEM_PA3
nativeModifierVk (Modifier [IsoLevel5]          _ Set)  = Just VK.VK_OEM_PA3
nativeModifierVk _                                      = Nothing

{-| Windows modifiers bits mapping.

Sources: @kbd.h@

Some mappings are widely accepted and must not be changed to avoid bugs in applications:

- 'Shift', 'Control' on their respective core modifiers
- @[TODO]@

Other are conventional and should not be changed to maintain consistency with defaults in XKB:

- 'Caps' on Lock
- 'Numeric' on Mod2
- 'IsoLevel3' on Mod5
-}
toWinModifierBit :: ModifierBit -> Maybe TB.Builder
toWinModifierBit Shift     = Just "KBDSHIFT"        -- 1 << 0
toWinModifierBit Capitals  = Nothing
toWinModifierBit Control   = Just "KBDCTRL"         -- 1 << 1
toWinModifierBit Alternate = Just "KBDALT"          -- 1 << 2
toWinModifierBit Super     = Nothing
toWinModifierBit Numeric   = Just "KBD_NUMERIC"     -- 1 << 3, originally: KBDKANA
toWinModifierBit IsoLevel3 = Just "KBD_ISO_LEVEL_3" -- 1 << 4, originally: KBDROYA
toWinModifierBit IsoLevel5 = Just "KBD_ISO_LEVEL_5" -- 1 << 5, originally: KBDLOYA

toModifierBit :: ModifierBit -> Maybe Int
toModifierBit Shift     = Just (1 `shift` 0) -- System fixed value
toModifierBit Capitals  = Nothing
toModifierBit Control   = Just (1 `shift` 1) -- System fixed value
toModifierBit Alternate = Just (1 `shift` 2) -- System fixed value
toModifierBit Super     = Nothing
toModifierBit Numeric   = Just (1 `shift` 3) -- Custom value
toModifierBit IsoLevel3 = Just (1 `shift` 4) -- Custom value
toModifierBit IsoLevel5 = Just (1 `shift` 5) -- Custom value

