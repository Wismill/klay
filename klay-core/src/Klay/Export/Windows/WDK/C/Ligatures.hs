{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.WDK.C.Ligatures
  ( mkALigature
  ) where

import Data.List (intersperse)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map

import TextShow (showb)

import Klay.Export.Windows.Common.Modifiers (WLevel(..))
import Klay.Export.Windows.Common.Symbols (CharSequenceMapping, LigatureRef)
import Klay.Export.Windows.Lookup.Characters (toWinSymbol, toWinEncoding, quoteWcharLong)
import Klay.Export.Windows.WDK.C.Modifiers (paddingVk)
import Klay.Export.Windows.WDK.C.Symbols (paddingLevel)
import Klay.Utils.Unicode (padR)
import Klay.Utils.UserInput (rawTextToString)

-- | Create the table @aLigature@ that defines the ligatures.
mkALigature :: CharSequenceMapping -> (Set.Set LigatureRef, TL.Text, TL.Text)
mkALigature ls | null ls = (mempty, mempty, "  0, 0, NULL // No ligatures")
mkALigature ls = (valid_ligatures, aLigature, kbdTables_ligatures)
  where
    valid_ligatures = Map.keysSet ls -- [TODO] check correctness
    aLigature = TB.toLazyText $ mconcat
      [ "static ALLOC_SECTION_LDATA LIGATURE", max_ligature_length', " aLigature[] = {\n"
      , "  // Virtual key, Level index, Ligature sequence\n"
      , ligatures
      , "  { ", mconcat . intersperse ", " . replicate (max_ligature_length + 2) $ "0", " }\n"
      , "};\n" ]
    kbdTables_ligatures = TB.toLazyText . mconcat $
      [ "  ", max_ligature_length', ", // Size of the longuest sequence\n"
      , "  sizeof(aLigature[0]), // Size of one entry in `aLigature`\n"
      , "  (PLIGATURE1)aLigature, // Pointer to `aLigature` table"]
    ligatures = mconcat . fmap mkLigatureEntry . Map.toList $ ls'
    ls' = mkLigature <$> ls
    mkLigatureEntry ((vk, wlevel), liga) = mconcat
      [ "  { ", paddingVk . TB.fromLazyText $ vk, ", " -- [FIXME] missing VK
      , paddingLevel . showb . getWLevel $ wlevel, ", "
      , mconcat . intersperse ", " . take max_ligature_length $ liga <> repeat (paddingWchar "WCH_NONE"), " },\n" ]
    mkLigature = mconcat . fmap mkWinChar . rawTextToString
    mkWinChar = fmap paddingWchar . either toWinEncoding ((:[]) . quoteWcharLong) . toWinSymbol
    paddingWchar = padR 8 ' '
    max_ligature_length = maximum . fmap length $ ls'
    max_ligature_length' = showb max_ligature_length
