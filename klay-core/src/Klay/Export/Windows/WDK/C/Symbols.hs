{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.WDK.C.Symbols
  ( -- * Types
    SymbolState(..)
  , DeadKeyBaseCharMapping
  , CharSequenceMapping
  , WdkId(..)
  , WdkIdTL
    -- * Content generation
  , prepareSymbols
  , finalizeSymbols
    -- * Error handling
  , SymbolErrors(..)
    -- * Utils
  , escapeIdentifier
  , escapeGroupId
  , paddingLevel
  ) where

import Data.Maybe (isNothing, fromMaybe)
import Numeric.Natural
import Data.List (intersperse, sort, genericLength, dropWhileEnd)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set qualified as Set
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Functor (($>))
import Data.Foldable (foldlM)
import Control.Monad.State.Strict

import TextShow (showb)

import Klay.Export.Windows.Common.Layout
import Klay.Export.Windows.Common.Modifiers
  ( WLevel(..), groupLayoutWLevels, fixLevelIndex, levelIndexThreshold
  , hasCapsLock, hasAltGrCapsLock, checkModifierVk)
import Klay.Export.Windows.Common.Symbols
  ( SymbolErrors(..), SymbolState(..), DeadKeyBaseCharMapping, CharSequenceMapping, LigatureRef
  , WAction(..), WdkId(..), WdkIdTL
  , actionsByWLevel, addReachableChar, addReachableDk
  , addNonRemappableVk, addUnsupportedAction
  , escapeGroupId, escapeIdentifier)
import Klay.Export.Windows.Lookup.Characters (quoteWcharLong, toQuotedWinSymbolLong)
import Klay.Export.Windows.Lookup.VirtualKeys (toWinVkPrefixedQuoted, isReservedVk)
import Klay.Export.Windows.WDK.C.Modifiers (paddingVk)
import Klay.Keyboard.Layout (WindowsOptions(..), DeadKey)
import Klay.Keyboard.Layout.Action
  ( Action(..), Actions(..), ResolvedActions, Level, CustomActions(..)
  , untagCustomisableActions, specialActionToMonogram, (.=))
import Klay.Keyboard.Layout.Action.DeadKey (showDeadKeyName)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier
  (ModifiersOptions, modifiersOptionsToList, noModifier, shift, control)
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.Utils.Unicode (padR, padL, showUnicodEB, showInvisibleChar)
import Klay.Utils.UserInput (fromChar, escapeControlNewlines)


type S = State SymbolState

{-| An accumulator to build @aVkToWch@s tables.

The numpad keys must come last so that VkKeyScan interprets number characters
as coming from the main section of the kbd before considering the numpad.
-}
data VkToWcharTables e a = VkToWcharTables
  { _wCharErrors :: e
  -- ^ Errors messages that will be included to the C source file
  , _wCharNumPad :: a
  -- ^ Numpad key actions
  , _wCharOther :: a
  -- ^ Other keys actions
  } deriving (Functor, Eq, Show)

instance (Semigroup e, Semigroup a) => Semigroup (VkToWcharTables e a) where
  (VkToWcharTables a1 b1 c1) <> (VkToWcharTables a2 b2 c2) = VkToWcharTables (a1 <> a2) (b1 <> b2) (c1 <> c2)

instance (Monoid e, Monoid a) => Monoid (VkToWcharTables e a) where
  mempty = VkToWcharTables mempty mempty mempty

type VkToWcharTablesEntries = VkToWcharTables [InvalidEntry] (Map Natural [ValidEntry])

-- | An invalid entry for a 'VirtualKey'
data InvalidEntry = InvalidVk
  { _invalidVk :: TL.Text
  , _invalidReason :: InvalidVkReason
  } deriving (Eq, Ord, Show)

-- | Reason of the invalid 'VirtualKey'
data InvalidVkReason
  = UnsupportedVk
  | NonMappableVk
  | UnMappedVk
  deriving (Eq, Ord, Show)

-- | A valid entry for a 'VirtualKey'
data ValidEntry = ValidEntry
  { _validVk :: TL.Text
  , _options :: ModifiersOptions
  , _actions :: [ActionEntry]
  } deriving (Eq, Show)

type ActionEntry = (WAction, TB.Builder)

instance Ord ValidEntry where
  ValidEntry k1 _ _ `compare` ValidEntry k2 _ _ = compare k1 k2

-- | Preprocess actions
prepareSymbols
  :: WindowsOptions
  -> [VK.VirtualKey]
  -> WdkGroup
  -> (VkToWcharTablesEntries, Set.Set Char, Set.Set DeadKey, CharSequenceMapping, Maybe SymbolErrors)
prepareSymbols opts vks g = (symbols, chars, dks, ligatures, errors)
  where
    -- (symbols, (SymbolState dks ligatures errors)) = runState preprocessGroup' mempty
    (symbols, SymbolState chars dks ligatures errors) = runState (preprocessGroup' >>= preprocessExtra) mempty
    preprocessGroup' = foldrGroupMappingWithKeyM preprocessVk' mempty g
    preprocessExtra s = foldlM (\acc (vk, as) -> preprocessVk wlevels opts vk as acc) s extraEntries
    -- [TODO] move this to common prepare layout?
    extraEntries :: [(VK.VirtualKey, ResolvedActions)]
    extraEntries =
      [ (VK.VK_CANCEL, ADefault [noModifier .= '\x0003', shift .= '\x0003', control .= '\x0003'])
      ]
    wlevels = groupLayoutWLevels g
    vks' = Set.fromList vks
    preprocessVk' vk as acc
      | Set.member vk vks' = preprocessVk wlevels opts vk as acc
      | otherwise          = pure acc

preprocessVk
  :: [(WLevel, Level)]
  -> WindowsOptions
  -> VK.VirtualKey
  -> ResolvedActions
  -> VkToWcharTablesEntries
  -> S VkToWcharTablesEntries
-- OS default for non-remappable key: skip
preprocessVk _ _ vk0 (ADefault _) acc
  | isReservedVk vk0 = pure acc
-- Mappable virtual key
preprocessVk wlevels _ vk0 as0 acc =
  let win_vk = TB.toLazyText $ toWinVkPrefixedQuoted vk0
  in if isReservedVk vk0 || isSpecialSingleton (untagCustomisableActions as0)
    -- Only process remappable keys
    then nonRemappableVk vk0 win_vk as0
    else processVk vk0 win_vk . untagCustomisableActions $ as0
  where
    nonRemappableVk :: VK.VirtualKey -> TL.Text -> ResolvedActions -> S VkToWcharTablesEntries
    nonRemappableVk _  _      (ADefault _) = pure acc -- Skip OS default mapping
    nonRemappableVk vk win_vk (ACustom as)
      | checkModifierVk vk as = pure acc -- Skip supported modifier
      | otherwise =  addNonRemappableVk vk
                  *> addInvalid (InvalidVk win_vk NonMappableVk)

    -- We need this in order to manage non-standard VK mappings
    isSpecialSingleton :: Actions -> Bool
    isSpecialSingleton (SingleAction ( AModifier _)) = True
    isSpecialSingleton _                             = False

    processVk :: VK.VirtualKey -> TL.Text -> Actions -> S VkToWcharTablesEntries
    processVk vk win_vk as = mkVkActions win_vk as >>= \(mopts, as') ->
      addValid vk (ValidEntry win_vk mopts as')

    addInvalid :: InvalidEntry -> S VkToWcharTablesEntries
    addInvalid e = pure acc{_wCharErrors=e : _wCharErrors acc}

    addValid :: VK.VirtualKey -> ValidEntry -> S VkToWcharTablesEntries
    addValid vk e = pure $ addEntry vk e acc

    mkVkActions :: TL.Text -> Actions -> S (ModifiersOptions, [ActionEntry])
    mkVkActions win_vk = traverse mkVkActions' . actionsByWLevel wlevels
      where
        mkVkActions' = traverse \(wl, a) -> (, prettyActionB a) <$> mkVkAction win_vk wl a
        prettyActionB (AChar c) = mconcat ["U+", padL 4 '0' $ showUnicodEB c, " ", showInvisibleChar c]
        prettyActionB (AText t) = mconcat ["\"", TB.fromLazyText . escapeControlNewlines $ t, "\""]
        prettyActionB (ADeadKey dk) = mconcat ["‹", TB.fromString $ showDeadKeyName dk, "›"]
        prettyActionB (AModifier m) = mconcat ["‹", showb m, "›"] -- [FIXME]
        prettyActionB (ASpecial a) = showb a
        prettyActionB NoAction = noSymbol
        prettyActionB UndefinedAction = noSymbol

    mkVkAction :: TL.Text -> WLevel -> Action -> S WAction
    mkVkAction vk iLevel (AChar c) = case toQuotedWinSymbolLong c of
      Right s -> addReachableChar c $> WChar s
      Left c' -> addReachableChar c *> mkVkAction vk iLevel (AText . fromChar $ c')
    mkVkAction vk iLevel (AText liga) = do
      let liga_ref = (vk, iLevel)
      modify \s -> s{_charSequenceMapping=Map.insert liga_ref liga $ _charSequenceMapping s}
      pure $ WLigature liga_ref liga
    mkVkAction _ _ (ADeadKey dk) =
      addReachableDk dk $> WDeadKey dk
    mkVkAction _ _ (AModifier m) =
      addUnsupportedAction (Left m) $> WNoAction
    mkVkAction vk iLevel (ASpecial a) = case specialActionToMonogram a of
      Nothing -> addUnsupportedAction (Right a) $> WNoAction
      Just c  -> mkVkAction vk iLevel (AChar c)
    mkVkAction _ _ NoAction         = pure WNoAction
    mkVkAction _ _ UndefinedAction  = pure WNoAction

addEntry :: VK.VirtualKey -> ValidEntry -> VkToWcharTablesEntries -> VkToWcharTablesEntries
addEntry vk e acc
  | n == 0    = acc -- error $ mconcat ["addEntry: empty entry at ", show vk]
  | is_numpad = acc{_wCharNumPad=Map.alter update n $ _wCharNumPad acc}
  | otherwise = acc{_wCharOther=Map.alter update n $ _wCharOther acc}
  where
    is_numpad = VK.VK_NUMPAD0 <= vk && vk <= VK.VK_NUMPAD9
    as = dropWhileEnd ((== WNoAction) . fst) . _actions $ e
    e' = e{_actions = as}
    n = genericLength as
    update Nothing   = Just [e']
    update (Just es) = Just (e':es)

noSymbol :: TB.Builder
noSymbol = "�"

-- | Generate the various @aVkToWch@s tables
finalizeSymbols
  :: DeadKeyBaseCharMapping
  -> Set.Set LigatureRef
  -> VkToWcharTablesEntries
  -> (TL.Text, TL.Text)
finalizeSymbols dkm ls symbols =
  ( mkAVkToWcharTable symbols
  , mkAVkToWchs dkm ls symbols )

-- | Generate the content of the @aVkToWcharTable@
mkAVkToWcharTable :: VkToWcharTablesEntries -> TL.Text
mkAVkToWcharTable = TB.toLazyText . mconcat . intersperse "\n" . make_entries
  where
    make_entries acc = mconcat
      [ fmap (go "Other") (Map.keys . _wCharOther $ acc)
      , msg
      , fmap (go "Numpad") (Map.keys . _wCharNumPad $ acc)
      ]
    msg = [ "  // The numpad keys must come last so that VkKeyScan interprets number characters"
          , "  // as coming from the main section of the kbd before considering the numpad." ]
    go suffix n = mconcat [indent, "{ (PVK_TO_WCHARS1)aVkToWch", suffix, paddingLevel n', ", ", paddingLevel n', ", sizeof(aVkToWch", suffix, n', "[0]) },"]
      where n' = showb . getWLevel . fixLevelIndex $ n

-- | Generate the  @aVkToWch@ tables
mkAVkToWchs
  :: DeadKeyBaseCharMapping
  -> Set.Set LigatureRef
  -> VkToWcharTablesEntries
  -> TL.Text
mkAVkToWchs dkm ligatures symbols = TB.toLazyText . mconcat $
  [ "// Errors encountered while creating aVkToWch tables:\n"
  , errors, "\n"
  , aVkToWchs ]
  where
    errors = foldr mkErrors mempty . _wCharErrors $ symbols

    mkErrors (InvalidVk vk UnsupportedVk) acc =
      mconcat ["// [ERROR] Unsupported virtual key: ", TB.fromLazyText vk, "\n", acc]
    mkErrors (InvalidVk vk NonMappableVk) acc =
      mconcat ["// [ERROR] Non remappable virtual key: ", TB.fromLazyText vk, "\n", acc]
    mkErrors (InvalidVk vk UnMappedVk) acc =
      mconcat ["// [WARNING] Virtual key ", TB.fromLazyText vk, " has a virtual key definition but is not mapped to any actions.\n", acc]

    aVkToWchs
      =  (Map.foldrWithKey' (go "Other") mempty . _wCharOther $ symbols)
      <> (Map.foldrWithKey' (go "Numpad") mempty . _wCharNumPad $ symbols)
    go suffix n entries acc = mkAVkToWch suffix n dkm ligatures entries <> acc

-- | Generate one specific @aVkToWch@ table
mkAVkToWch
  :: TB.Builder
  -> Natural
  -> DeadKeyBaseCharMapping
  -> Set.Set LigatureRef
  -> [ValidEntry]
  -> TB.Builder
mkAVkToWch suffix n dkm ligatures entries = mconcat
  [ "static ALLOC_SECTION_LDATA VK_TO_WCHARS", n_text, " aVkToWch", suffix, n_text, "[] = {\n"
  , indent, "// Virtual key, Modifier options, ", levels, "\n"
  , symbols
  -- Add mandatory last line filled with 0
  , indent, "{", mconcat . intersperse ", " $ replicate (fromIntegral n' + 2) "0", "}\n"
  , "};\n\n" ]
  where
    symbols = mconcat . fmap mkEntry . sort $ entries
    levels = mconcat . intersperse ", " . fix_column15 . fmap mkLevelComment $ [(0::Natural)..fromIntegral n - 1]

    fix_column15 xs
      | length xs <= levelIndexThreshold = xs
      | otherwise = let (xs1, xs2) = splitAt levelIndexThreshold xs in mconcat [xs1, ["Reserved"], xs2]
    mkLevelComment k = paddingWchar . mconcat $ ["Level ", showb k]

    n' = getWLevel . fixLevelIndex $ n
    n_text = showb n'

    mkEntry :: ValidEntry -> TB.Builder
    mkEntry (ValidEntry win_vk opts as) =
      let (cs, as1, as2) = unzip3 . fmap mkAction $ as
      in mkComment cs <> mkRow1 (TB.fromLazyText win_vk) opts as1 <> mkRow2 as2

    mkComment :: [TB.Builder] -> TB.Builder
    mkComment cs = "  /*                                " <> concat' cs <> " */\n"
    mkRow1 :: TB.Builder -> ModifiersOptions -> [TB.Builder] -> TB.Builder
    mkRow1 win_vk opts as = mconcat [indent, "{", paddingVk win_vk, ", ", paddingLock (makeLocks opts), ", ", concat' as, "},\n"]
    mkRow2 :: [Maybe TB.Builder] -> TB.Builder
    mkRow2 as | all isNothing as = mempty
    mkRow2 as =
      let as' = fromMaybe wchNone <$> as
      in mconcat [indent, "{", paddingVk "0xff", ", ", paddingLock "0", ", ", concat' as', "},\n"]
    concat' = mconcat . intersperse ", " . fmap paddingWchar

    makeLocks :: ModifiersOptions -> TB.Builder
    makeLocks opts
      | null flags = "0"
      | otherwise = mconcat . intersperse " | " $ flags
      where
        flags = foldr go mempty . modifiersOptionsToList $ opts
        -- [TODO] warning if option not supported
        go (m, opt) acc
          | hasCapsLock      m opt = "CAPLOK" : acc
          | hasAltGrCapsLock m opt = "CAPLOKALTGR" : acc
        -- [TODO] handle all the remaining options properly (SGCAPS, KANALOK)
        go _ acc = acc
    paddingLock = padR 16 ' '

    mkAction :: ActionEntry -> (TB.Builder, TB.Builder, Maybe TB.Builder)
    mkAction (WNoAction, _) = (noSymbol, wchNone, Nothing)
    mkAction (WChar c, cmt) = (cmt, c, Nothing)
    mkAction (WLigature liga_ref _, cmt)
      | Set.member liga_ref ligatures = (cmt, "WCH_LGTR", Nothing)
      | otherwise = (noSymbol, wchNone, Nothing)
    mkAction (WDeadKey dk, cmt) = case Map.lookup dk dkm of
      Nothing -> (noSymbol, wchNone, Nothing)
      Just dk_ref -> (cmt, "WCH_DEAD", Just . quoteWcharLong $ dk_ref)
    wchNone = "WCH_NONE"

-- | Indent used in this section
indent :: TB.Builder
indent = "  "

-- | Padding for a level index
paddingLevel :: TB.Builder -> TB.Builder
paddingLevel = padR 2 ' '

-- | Padding for a @WCHAR@
paddingWchar :: TB.Builder -> TB.Builder
paddingWchar = padR 8 ' '
