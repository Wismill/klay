{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.WDK.C.DeadKeys
  ( DeadKeyError(..)
  , mkADeadKey
  , mkAKeyNamesDead
  , safeDeadKeyChars
  ) where


import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Map.Strict qualified as Map

import Klay.Export.Windows.Common.DeadKeys
  (ChainedDeadKeyProcessing(..), MkDkContent, MkDkMapping, MkEntry, MkPartialEntry, mkDkContent, processDkMapping, safeDeadKeyChars, paddingSymbol)
import Klay.Export.Windows.Lookup.Characters (WindowsChar, quoteWcharLong, quoteWstring, toQuotedWinSymbolLong)
import Klay.Export.Windows.WDK.C.Symbols (DeadKeyBaseCharMapping)
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.UserInput (RawText(..), escapeLine, escapeControlNewlines)
import Klay.Utils.Unicode (padR)

-- | Generate the content of the table @aDeadKey@
mkADeadKey :: MkDkContent
mkADeadKey = mkDkContent processDkMappingC CondenseChainedDeadKeys

processDkMappingC :: MkDkMapping
processDkMappingC = processDkMapping mkDkHeader mkEntry mkPartialEntry

mkDkHeader :: DeadKey -> WindowsChar -> TB.Builder
mkDkHeader dk _ = mconcat ["// Dead key: ", make_comment dk, "\n"]
  where
    -- [TODO] improve this ugly series of compositions
    make_comment = TB.fromLazyText . fix_rsolidus . escapeControlNewlines . RawText . TL.pack . showDeadKeyName
    fix_rsolidus t = case TL.unsnoc t of
      Just (t', '\\') -> t' <> "U+002F"
      _               -> t

-- [TODO] Add comment in mkPartialEntry
mkEntry :: MkEntry
mkEntry dk_char entryP comment = mconcat
  ["  DEADTRANS(", paddingSymbol . quoteWcharLong $ dk_char, ", ", entryP, "),\t// "
  , comment, "\n"]

mkPartialEntry :: MkPartialEntry
mkPartialEntry lookupDk s r = case (mkSymbol s, mkResult r) of
  (Nothing, Nothing) -> Left $ DeadKeyComboError [s] (Just r)
  (Just _, Nothing) -> Left $ DeadKeyComboError mempty (Just r)
  (Nothing, Just _) -> Left $ DeadKeyComboError [s] Nothing
  (Just s', Just (r', flag)) -> Right (mconcat [paddingSymbol s', ", ", paddingSymbol r', ", ", flag], comment)
  where
    mkSymbol :: Symbol -> Maybe TB.Builder
    mkSymbol (SChar c) = mkChar toQuotedWinSymbolLong c
    mkSymbol (SDeadKey dk') = quoteWcharLong <$> lookupDk dk'
    mkResult :: Result -> Maybe (TB.Builder, TB.Builder)
    mkResult (RText _) = Nothing
    mkResult (RChar c) = (,"NORMAL_CHARACTER") <$> mkChar toQuotedWinSymbolLong c
    mkResult (RDeadKey dk') = (,"CHAINED_DEAD_KEY") . quoteWcharLong <$> lookupDk dk'
    mkChar :: (Char -> Either Char TB.Builder) -> Char -> Maybe TB.Builder
    mkChar convert c = case convert c of
      Left _   -> Nothing
      Right c' -> Just c'
    comment = TB.fromString . mconcat $ [prettySymbol s, " -> ", prettyResult r]

-- | Create the content of @aKeyNamesDead@, the table of the dead keys names
mkAKeyNamesDead :: DeadKeyBaseCharMapping -> TL.Text
mkAKeyNamesDead = TB.toLazyText . Map.foldrWithKey' go mempty
  where
    go dk dk_ref acc = mconcat ["  ", make_dk_ref dk_ref, "   L\"", make_dk_name dk, "\",\n", acc]
    make_dk_ref = padR 6 ' '  . quoteWstring
    make_dk_name = TB.fromLazyText . escapeLine . _dkName
