{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.WDK.C.Modifiers
  ( ModifierErrors(..)
  , mkAVkToBits
  , mkLevelsDefinitionWdk
  , levelIndexThreshold
  , levelsIndexEntriesThreshold
  , paddingVk
  ) where


import Data.List (intersperse)
import Data.Maybe (catMaybes)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Functor (($>))
import Data.Bifunctor (first)
import Control.Monad.State.Strict

import Data.These (These(..))
import Data.Align (alignWith)
import TextShow (showb)

import Klay.Export.Windows.Common.Layout (WdkGroup)
import Klay.Export.Windows.Common.Modifiers
  ( WLevel(..), ModifierErrors(..), mkLevelsDefinition
  , addUnsupportedModifierBit, addUnsupportedModifier
  , addNonRemappableVk
  , levelIndexThreshold, levelsIndexEntriesThreshold, getModifier)
import Klay.Export.Windows.Lookup.Modifiers (toWinModifierBit)
import Klay.Export.Windows.Lookup.VirtualKeys (toWinVkPrefixedQuoted)
import Klay.Keyboard.Layout.Action (ResolvedActions, untagCustomisableActions)
import Klay.Keyboard.Layout.Group (Group(..))
import Klay.Keyboard.Layout.Modifier (Modifier(..), ModifierBit(..), ModifierEffect(..), ModifiersCombo)
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.Utils (foldlMWithKey')
import Data.Set.NonEmpty qualified as NESet
import Klay.Utils.UserInput (escapeLine)
import Klay.Utils.Unicode (padR)

type S = State (Maybe ModifierErrors)

type RawModifierBitMapping = Map.Map VK.VirtualKey (TL.Text, ModifiersCombo)
type ModifierBitMapping = Map.Map TL.Text ModifiersCombo

-- [FIXME] Check max number of modifier VKs
{-| Create the content of the table @aVkToBits@.

This table defines the modifiers bits associated to some virtual keys.
The possible bits are:

+------------------------+------------+--------------------------------+
| Windows modifier bit   | Value      | Usual associated virtual key   |
+========================+============+================================+
| @KBDBASE@ (/none/)     | @0b000000@ | Any key /not/ in @aVkToBits@   |
+------------------------+------------+--------------------------------+
| @KBDSHIFT@ ('Shift')   | @0b000001@ | @VK_SHIFT@ ('VK.WShift')       |
+------------------------+------------+--------------------------------+
| @KBDCTRL@ ('Control')  | @0b000010@ | @VK_CONTROL@ ('VK.WControl')   |
+------------------------+------------+--------------------------------+
| @KBDALT@ ('Alternate') | @0b000100@ | @VK_MENU@ ('VK.WAlt')          |
+------------------------+------------+--------------------------------+
| @KBDKANA@              | @0b001000@ |                                |
+------------------------+------------+--------------------------------+
| @KBDROYA@              | @0b010000@ |                                |
+------------------------+------------+--------------------------------+
| @KBDLOYA@              | @0b100000@ |                                |
+------------------------+------------+--------------------------------+

These bits may be combined using bitwise @OR@ operation.
-}
mkAVkToBits
  :: WdkGroup
  -> (TL.Text, Maybe ModifierErrors)
mkAVkToBits g = first TB.toLazyText $ runState (mapping >>= Map.foldrWithKey' make_entries (pure mempty)) mempty
  where
    make_entries :: TL.Text -> Set.Set ModifierBit -> S TB.Builder -> S TB.Builder
    make_entries vk ms acc = do
      bits <- mkBits ms
      acc' <- acc
      pure $ mconcat ["  { ", paddingVk (TB.fromLazyText vk), ", ", padR 30 ' ' bits, " },\n", acc']

    mkBits = fmap (mconcat . intersperse " | " . catMaybes) . traverse toWinModifierBit' . Set.toList

    toWinModifierBit' :: ModifierBit -> S (Maybe TB.Builder)
    toWinModifierBit' m = case toWinModifierBit m of
      Nothing -> addUnsupportedModifierBit m $> Nothing
      m'      -> pure m'

    -- [TODO] the following is a bit hacky
    mapping :: S ModifierBitMapping
    mapping = (foldlMWithKey' go1 mempty . _groupMapping) >=> simplify >=> finalize $ g

    finalize :: RawModifierBitMapping -> S ModifierBitMapping
    finalize = pure . Map.fromList . Map.elems

    simplify :: RawModifierBitMapping -> S RawModifierBitMapping
    simplify m = foldlMWithKey' go2 m
      [ (VK.VK_SHIFT,     (VK.VK_LSHIFT  , VK.VK_RSHIFT))
      , (VK.VK_CONTROL,   (VK.VK_LCONTROL, VK.VK_RCONTROL))
      , (VK.VK_MENU,      (VK.VK_LMENU   , VK.VK_RMENU))]

    go2 :: RawModifierBitMapping -> VK.VirtualKey -> (VK.VirtualKey, VK.VirtualKey) -> S RawModifierBitMapping
    go2 acc vk (vk1, vk2) = case alignWith f (Map.lookup vk1 acc) (Map.lookup vk2 acc) of
      (Just (True, ms)) -> if vk /= VK.VK_MENU || (vk == VK.VK_MENU && ms == [Alternate])
        then add_modifier vk ms . Map.delete vk1 . Map.delete vk2 $ acc
        else pure acc
      _ -> pure acc
      where f (This (_, ms)) = (True, ms)
            f (That (_, ms)) = (True, ms)
            f (These (_, ms1) (_, ms2)) = (ms1 == ms2, ms1)

    go1 :: RawModifierBitMapping -> VK.VirtualKey -> ResolvedActions -> S RawModifierBitMapping
    go1 acc vk as =
      let as' = untagCustomisableActions as
      in case getModifier vk as' of
        Nothing -> pure acc -- Not a modifier, skip
        Just (Left _) -> addNonRemappableVk vk $> acc
        Just (Right m) -> case m of
          Modifier [Capitals] _ _    -> pure acc -- skip
          Modifier [Super]    _ _    -> pure acc -- skip
          Modifier [Numeric]  _ Lock -> pure acc -- skip -- [TODO] GROUP OR NUMERIC
          Modifier bs         _ Set  -> add_modifier vk (NESet.toSet bs) acc -- ok
          -- [TODO] KanaLock?
          _ -> addUnsupportedModifier m $> acc -- error

    add_modifier vk bs acc =
      let t = toWinVkPrefixedQuoted vk
      in pure $ Map.insert vk (TB.toLazyText t, bs) acc


{-| Create the content of the table @CharModifiers@.

This table converts values from the @aVkToBits@ table into column indexes for the @aVkToWch@ tables.

Somes issues:

* @SHFT_INVALID@ indicates that no index is associated to a modifier bits combination.
  It has the value @15@ so this cannot be used as a column index.
  We insert a dummy column at this index to fix this.
* There is an issue if this table has more that 256 entries, so it is truncated if required.
-}
mkLevelsDefinitionWdk :: WdkGroup -> (Maybe ModifierErrors, Int, TL.Text)
mkLevelsDefinitionWdk = mkLevelsDefinition mkHeader mkSymbolIndexEntry ",\n"
  where
    mkHeader n = mconcat ["  ", paddingIndex "// Level index", padR (n + 3) ' ' " -> Modifiers pressed", " -> Level name\n"]
    mkSymbolIndexEntry Nothing  _  comment = mkEntry "SHFT_INVALID" comment "(No valid level)"
    mkSymbolIndexEntry (Just (l, n)) _ comment =
      let levelIndex = showb . getWLevel $ l
          levelName  = TB.fromLazyText . escapeLine $ n
      in mkEntry levelIndex comment levelName
    mkEntry idx comment lname = mconcat [indent, paddingIndex idx, " /* ", comment, " -> ", padR 24 ' ' lname, " */"]
    indent = "    "
    paddingIndex = padR 12 ' '

-- | Add padding for prefixed virtual key (@VK_*@).
paddingVk :: TB.Builder -> TB.Builder
paddingVk = padR 13 ' '
