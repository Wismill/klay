{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.WDK.C.KeyNames
  ( mkKeyNames
  ) where

import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Bifunctor (bimap)

import Klay.Export.Windows.Common.KeyNames qualified as K

mkKeyNames :: K.KeyNames -> (TL.Text, TL.Text)
mkKeyNames
  = bimap mkNames mkNames
  . K.mkKeyNames
  where
    mkNames = TB.toLazyText . foldr mkName mempty
    mkName (key, name) acc = mconcat
      [ "  0x"
      , TB.fromLazyText key
      , ", L\""
      , TB.fromLazyText name
      , "\",\n"
      , acc
      ]
