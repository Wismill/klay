module Klay.Export.Windows.WDK.Project
  ( generateProject
  , generateProjectFilters
  ) where


import Data.Text.Lazy qualified as TL

import Data.Aeson (KeyValue(..), object)

import Klay.Keyboard.Layout
import Klay.Utils.Mustache qualified as U


-- [FIXME] content
generateProject :: Layout gs -> IO (Maybe TL.Text)
generateProject layout =
  U.generateFileContentFromTemplate "WDK project file" template_file vars
  where
    system_name = mkLayoutId' layout
    template_file = "windows/wdk/template.vcxproj"
    vars = object -- [TODO] header with metadata
      [ "keyboard" .= system_name ]

-- [FIXME] content
generateProjectFilters :: Layout gs -> IO (Maybe TL.Text)
generateProjectFilters layout =
  U.generateFileContentFromTemplate "WDK project filters file" template_file vars
  where
    system_name = mkLayoutId' layout
    template_file = "windows/wdk/template.vcxproj.Filters"
    vars = object -- [TODO] header with metadata
      [ "keyboard" .= system_name ]
