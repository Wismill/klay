module Klay.Export.Windows.WDK.Header
  ( HeaderErrors(..)
  , generateHeader
  ) where

import Data.Maybe (fromMaybe)
import Data.Bits (countTrailingZeros)
import Numeric.Natural
import Data.List (intersperse, sortBy)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Bifunctor (Bifunctor(..))
import Control.Applicative (Alternative(..))

import Data.Aeson (KeyValue(..), object)
import TextShow (showb)

import Klay.Export.Windows.Common.Layout
import Klay.Export.Windows.Lookup.Keycodes (toWinKeycode)
import Klay.Export.Windows.Lookup.Modifiers (toModifierBit)
import Klay.Export.Windows.Lookup.VirtualKeys (toWinVkUnprefixedQuoted)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.VirtualKey
import Klay.OS (MultiOs(..))
import Klay.Utils.Mustache qualified as Mustache

data HeaderErrors = HeaderErrors
  { _hUnsupportedKeys :: Set.Set Key
  -- ^ Keys not supported in WDK
  , _hUnsupportedVK :: Set.Set VirtualKey
  -- ^ Virtual keys not supported in WDK
  , _hUnsupportedKbdType :: Maybe Natural
  -- ^ Unuspported @KBDTYPE@
  } deriving (Eq, Show)

instance Semigroup HeaderErrors where
  (HeaderErrors k1 vk1 t1) <> (HeaderErrors k2 vk2 t2) = HeaderErrors (k1 <> k2) (vk1 <> vk2) (t1 <|> t2)

instance Monoid HeaderErrors where
  mempty = HeaderErrors mempty mempty empty

-- [TODO] Limit TYPEDEF_VK_TO_WCHARS and TYPEDEF_LIGATURE declaration to the strict minimum
-- | Generate the header content
generateHeader :: Bool -> TL.Text -> WdkLayout -> IO ([VirtualKey], Maybe TL.Text, Maybe HeaderErrors)
generateHeader is_wdk header layout = do
  (vks,,errors) <$> Mustache.generateFileContentFromTemplate "WDK C header" template_file vars
  where
    template_file = "windows/wdk/template.h"
    vars = object
      [ "header" .= (header :: TL.Text)
      , "kbd_type" .= kbd_type
      , "is_wdk" .= is_wdk
      , "numeric_shift" .= toModifierBitShift Numeric
      , "iso_level_3_shift" .= toModifierBitShift IsoLevel3
      , "iso_level_5_shift" .= toModifierBitShift IsoLevel5
      , "type_def_vk_to_wchars" .= mkTypedefVkToWchars 11 32
      , "type_def_ligature" .= mkTypedefLigature 6 16
      , "virtual_key_map" .= virtual_keyMap ]
    toModifierBitShift = countTrailingZeros . fromMaybe 0 . toModifierBit
    errors = vk_errors <> kbd_type_error
    (vk_errors, vks, virtual_keyMap) = mkVirtualKeyMap layout
    (kbd_type, kbd_type_error) = case mkWindowsKeyboardType layout of
      Left t  -> (4, Just mempty{_hUnsupportedKbdType=Just t})
      Right t -> (t, Nothing)

mkTypedefVkToWchars :: Int -> Int -> TL.Text
mkTypedefVkToWchars start end =
  TB.toLazyText . mconcat . intersperse "\n" $
    [ mconcat ["TYPEDEF_VK_TO_WCHARS(", n', ") //  VK_TO_WCHARS", n', ", *PVK_TO_WCHARS", n', ";"]
    | n <- [start..end], let n' = showb n]

mkTypedefLigature :: Int -> Int -> TL.Text
mkTypedefLigature start end =
  TB.toLazyText . mconcat . intersperse "\n" $ ["TYPEDEF_LIGATURE(" <> showb n <> ")" | n <- [start..end]]

mkVirtualKeyMap :: WdkLayout -> (Maybe HeaderErrors, [VirtualKey], TL.Text)
mkVirtualKeyMap
  = second (TB.toLazyText . mconcat . fmap snd . sortBy compareKeyEntries)
  . Map.foldlWithKey' go mempty
  . _wVirtualKeyDefinition
  . _windows
  . _options
  where
    go :: (Maybe HeaderErrors, [VirtualKey], [(TL.Text, TB.Builder)])
       -> Key
       -> VirtualKey
       -> (Maybe HeaderErrors, [VirtualKey], [(TL.Text, TB.Builder)])
    go acc key vk
      | keyNativeVirtualKey4 key == Just vk = noremap_vk acc vk
      | otherwise                     = remap_vk acc key vk

    remap_vk acc key vk = either (add_error acc) (add_vk_text acc vk) (virtualKeyMacro key vk)
    noremap_vk acc vk = first (vk:) acc
    add_error (err, vks, acc) (err', t) = (err <> Just err', vks, (mempty, t) : acc)
    add_vk_text acc vk t = bimap (vk:) (t:) acc
    -- Order key by their non-extended code
    compareKeyEntries (key1, _) (key2, _) = case compare key1' key2' of
      EQ -> compare t1 t2
      c  -> c
      where (t1, key1') = fromMaybe ('\0', mempty) $ TL.uncons key1
            (t2, key2') = fromMaybe ('\0', mempty) $ TL.uncons key2

virtualKeyMacro :: Key -> VirtualKey -> Either (HeaderErrors, TB.Builder) (TL.Text, TB.Builder)
virtualKeyMacro key vk = case toWinKeycode key of
  Nothing -> Left
    ( mempty{_hUnsupportedKeys=Set.singleton key}
    , mconcat ["// [ERROR] Unsupported key: ", showb key, "\n"] )
  Just win_key ->
    let win_vk = toWinVkUnprefixedQuoted vk
        macro = mconcat
          [ "// Key ", showb key, " -> ", showb vk, "\n"
          , "#undef  ", win_key, "\n"
          , "#define ", win_key, " _EQ(", win_vk, ")\n" ]
    in Right (TB.toLazyText win_key, macro)
