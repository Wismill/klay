module Klay.Export.Windows.WDK.C
  ( generateC
  ) where


import Data.List (intersperse)
import Data.Maybe (catMaybes)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB

import Data.Aeson (KeyValue(..), object)

import Klay.Export.Windows.Common.Layout
import Klay.Export.Windows.WDK.C.DeadKeys (mkADeadKey, mkAKeyNamesDead, safeDeadKeyChars)
import Klay.Export.Windows.WDK.C.KeyNames (mkKeyNames)
import Klay.Export.Windows.WDK.C.Ligatures (mkALigature)
import Klay.Export.Windows.WDK.C.Modifiers (ModifierErrors, mkAVkToBits, mkLevelsDefinitionWdk)
import Klay.Export.Windows.WDK.C.Symbols (SymbolErrors, prepareSymbols, finalizeSymbols)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey (DeadKeyError)
import Klay.Keyboard.Layout.VirtualKey (VirtualKey(..))
import Klay.OS (MultiOs(..))
import Klay.Utils.Mustache qualified as Mustache


-- [FIXME] What about the other groups?!
-- | Generate the C file content
generateC :: TL.Text -> WdkLayout -> [VirtualKey] -> IO (Maybe TL.Text, Maybe ModifierErrors, Maybe SymbolErrors, Maybe DeadKeyError)
generateC header layout vks = do
  generateGroupC system_name options dkd header vks . firstGroupMapping $ layout
  where
    system_name = mkLayoutId' layout
    options = _windows . _options $ layout
    dkd = _deadKeys layout

generateGroupC
  :: FilePath
  -> WindowsOptions
  -> DeadKeyDefinitions
  -> TL.Text
  -> [VirtualKey]
  -> WdkGroup
  -> IO (Maybe TL.Text, Maybe ModifierErrors, Maybe SymbolErrors, Maybe DeadKeyError)
generateGroupC system_name options dkd header vks g =
  (,mModifierErrors,mSymbolErrors,mdkErrors) <$> Mustache.generateFileContentFromTemplate "WDK C header" template_file vars
  where
    template_file = "windows/wdk/template.c"
    vars = object
      [ "header" .= (header :: TL.Text)
      , "kbd_file" .= system_name
      , "aVkToBits" .= aVkToBits
      , "level_total_number" .= level_total_number
      , "level_definitions" .= levelDefinitions
      , "aVkToWchs" .= aVkToWchs
      , "aVkToWcharTable" .= aVkToWcharTable
      , "aKeyNames" .= make_aKeyNames
      , "aKeyNamesExt" .= make_aKeyNamesExt
      , "aKeyNamesDead" .= aKeyNamesDead
      , "aDeadKey" .= aDeadKey
      , "aLigature" .= aLigature
      , "locale_options" .= localeOptions
      , "KbdTables_ligatures" .= kbdTables_ligatures ]
    -- Modifiers & levels
    mModifierErrors = mModifierErrors1 <> mModifierErrors2
    (aVkToBits, mModifierErrors1) = mkAVkToBits g
    (mModifierErrors2, level_total_number, levelDefinitions) = mkLevelsDefinitionWdk g
    -- Symbols
    (symbols, chars, dks, ligatures, mSymbolErrors) = prepareSymbols options vks g
    (aVkToWcharTable, aVkToWchs) = finalizeSymbols dkm valid_ligatures symbols
    -- Key names
    (make_aKeyNames, make_aKeyNamesExt) = mkKeyNames (_wKeysNames options)
    -- Dead keys
    (aDeadKey, dkm, _, mdkErrors) = mkADeadKey dkd dks chars safeDeadKeyChars
    aKeyNamesDead = mkAKeyNamesDead dkm
    -- Ligatures
    (valid_ligatures, aLigature, kbdTables_ligatures) = mkALigature ligatures
    -- System
    localeOptions
      | null opts = "0"
      | otherwise = TB.toLazyText . mconcat $ intersperse " | " opts
      where
        opts = catMaybes [altGr, shift_lock, lrm_rlm]
        altGr = if _wAltgr options then Just "KLLF_ALTGR" else Nothing
        shift_lock = if _wShiftDeactivatesCaps options then Just "KLLF_SHIFTLOCK" else Nothing
        lrm_rlm =  if _wLrmRlm options then Just "KLLF_LRM_RLM" else Nothing


