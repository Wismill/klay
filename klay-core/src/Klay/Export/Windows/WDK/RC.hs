module Klay.Export.Windows.WDK.RC
  ( generateRc
  , generateDef
  ) where

import Data.Maybe (fromMaybe)
import Data.Char (toUpper)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.List (intersperse)
import Data.List.NonEmpty qualified as NE
import Data.Functor ((<&>))
import Control.Monad ((>=>))

import Data.Aeson (KeyValue(..), object)
import Data.BCP47 qualified as BCP47
import TextShow (showt)
import Data.Version (showVersion)
import Data.Versions qualified as V

import Klay.Import.Windows.MSKLC (defaultEmptyField)
import Klay.Keyboard.Layout
import Klay.Utils.UserInput (escapeLine)
import Klay.Utils.Mustache qualified as U
import Klay.Export.Windows.Lookup.Locales
  (getWinLocale, getLanguageName, localeEN)
import Paths_klay_core qualified as Klay

-- [TODO] Check a more recent syntax of this file with Neo-Layout
generateRc :: Layout gs -> IO (Maybe TL.Text)
generateRc layout =
  U.generateFileContentFromTemplate "WDK RC file" template_file vars
  where
    system_name = mkLayoutId' layout
    metadata = _metadata layout
    escapeLine' = maybe defaultEmptyField escapeLine
    template_file = "windows/wdk/template.rc"
    vars = object -- [TODO] header with metadata
      [ "file_version" .= version
      , "file_version_str" .= version_str
      , "product_version" .= version
      , "product_version_str" .= version_str
      , "company_name" .= (escapeLine' . _author $ metadata)
      , "description" .= (escapeLine' . _description $ metadata)
      , "internal_name" .= (escapeLine . _name $ metadata)
      , "product_name" .= (escapeLine' . _description $ metadata)
      , "release_information" .= ("Created with Klay " <> showVersion Klay.version)
      , "license" .= (escapeLine' . _license $ metadata)
      , "original_filename" .= system_name
      , "locale" .= localeName
      , "locale_string" .= getLanguageName winLocale
      ]
    version = fromMaybe defaultVersion . (_version >=> toWindowsVersion) $ metadata
    version_str = maybe defaultVersion (V.prettyVer . unVersion) $ _version metadata
    defaultVersion = "0, 0, 0, 1"
    locale = mainLayoutLocale . _locales $ metadata
    localeName = BCP47.toText locale
    winLocale = fromMaybe localeEN $ getWinLocale (T.unpack localeName)

generateDef :: Layout gs -> IO (Maybe TL.Text)
generateDef layout =
  U.generateFileContentFromTemplate "WDK DEF file" template_file vars
  where
    system_name = fmap toUpper . mkLayoutId' $ layout
    template_file = "windows/wdk/template.def"
    vars = object -- [TODO] header with metadata
      [ "kbd_name" .= system_name ]

toWindowsVersion :: Version -> Maybe T.Text
toWindowsVersion (Version (V.Version _ cs _ _))
  =   traverse vChunkToWord cs
  <&> mconcat . intersperse ", " . fmap showt . NE.take 4 . (<> NE.repeat 0)
  where
    vChunkToWord = fmap concatWords . traverse vUnitToWord
    vUnitToWord (V.Digits d) = Just d
    vUnitToWord _            = Nothing
    concatWords = sum . fmap (uncurry (*)) . NE.zip (NE.fromList [1..]) . NE.reverse
