{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.WDK
  ( generateWdk
  , generateWdk'
  , prepareLayout
  , printErrors
  ) where

import Data.Text.Lazy qualified as TL
import Data.Maybe (fromMaybe)
import Data.Map.Strict qualified as Map
import Control.Monad (void, forM_, when, unless)
import Control.Monad.State.Strict (runState)

import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal

import Klay.App.Common
import Klay.Export.Linux.XKB.Header (createHeader)
import Klay.Export.Windows.Common.Layout qualified as L
import Klay.Export.Windows.Common.Modifiers (ModifierErrors(..), WdkId(..), WdkIdTL)
import Klay.Export.Windows.Common.Symbols (SymbolErrors(..))
import Klay.Export.Windows.IO (makePath, createDirectoryStructure, mwrite, write_utf_8)
import Klay.Export.Windows.Lookup.Keycodes (winKeyCodesMap)
import Klay.Export.Windows.WDK.Header (HeaderErrors(..), generateHeader)
import Klay.Export.Windows.WDK.C (generateC)
import Klay.Export.Windows.WDK.Project (generateProject, generateProjectFilters)
import Klay.Export.Windows.WDK.RC (generateRc, generateDef)
import Klay.App.Check (printDeadKeysErrors)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey (DeadKeyError(..))
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.Modification (DiscardedReason(..))

generateWdk :: FilePath -> MultiOsRawLayout -> IO ()
generateWdk = generateWdk' True "WDK" Nothing

generateWdk' :: Bool -> TL.Text -> Maybe ModifierErrors -> FilePath -> MultiOsRawLayout -> IO ()
generateWdk' is_wdk gname mErrors1 destination_dir layout = do
  createDirectoryStructure destination_dir
  header <- createHeader layout "//"
  (vks, headerContent, hErrors) <- generateHeader is_wdk header layout'
  mwrite_utf_8 "header file" (makePath' ".h") headerContent
  (cContent, mErrors3, sErrors2, mdkErrors) <- generateC header layout' vks
  mwrite_utf_8 "C file" (makePath' ".c") cContent
  generateRc layout' >>= mwrite_utf_8 "RC file" (makePath' ".rc")
  generateDef layout' >>= mwrite_utf_8 "DEF file" (makePath' ".def")
  when is_wdk do
    generateProject layout' >>= mwrite_utf_8 "Project file" (makePath' ".vcxproj")
    generateProjectFilters layout' >>= mwrite_utf_8 "Project filters file" (makePath' ".vcxproj.Filters")
  -- Errors
  let mErrors = mErrors1 <> mErrors2 <> mErrors3
  let sErrors = sErrors1 <> sErrors2
  printErrors gname hErrors mErrors sErrors mdkErrors
  where
    -- mwrite_utf_16 = mwrite write_utf_16_le -- [FIXME] some files may require utf16-le?
    mwrite_utf_8 = mwrite write_utf_8
    system_name = mkLayoutId' layout
    makePath' = makePath destination_dir system_name
    (layout', (mErrors2, sErrors1)) = prepareLayout layout

-- | Adapt the layout for Windows
prepareLayout :: MultiOsRawLayout -> (L.WdkLayout, (Maybe ModifierErrors, Maybe SymbolErrors))
prepareLayout l = runState
  (L.prepareLayout supportedBits supportedCombos checkKey checkVk l >>= go)
  (Nothing, Nothing)
  where
    go l' = pure l'{_groups = fmap (Map.mapKeys snd) <$> _groups l'}

    supportedBits = [Shift, Capitals, Control, Alternate, IsoLevel3, IsoLevel5]

    supportedCombos =
      [ M.isoLevel1
      , M.isoLevel2
      , M.isoLevel2Caps
      , M.isoLevel2ShiftCaps
      , M.isoLevel3
      , M.isoLevel4
      , M.isoLevel4Caps
      , M.isoLevel4ShiftCaps
      , M.isoLevel5
      , M.isoLevel6
      , M.isoLevel7
      , M.isoLevel8
      -- [TODO] complete the list
      , M.control
      , M.control <> M.shift
      , M.altGr
      , M.altGrShift
      , M.altGrCaps
      ]

    checkKey = flip Map.member winKeyCodesMap
    checkVk = const True

-- | Print errors occured using a Windows generator.
printErrors :: TL.Text -> Maybe HeaderErrors -> Maybe ModifierErrors -> Maybe SymbolErrors -> Maybe DeadKeyError -> IO ()
printErrors _ Nothing Nothing Nothing Nothing = pure ()
printErrors gname mHeaderErrors mModifierErrors mSymbolsErrors mDkErrors = do
  let HeaderErrors
        { _hUnsupportedKeys=unsupported_keys1
        , _hUnsupportedVK=unsupported_vks1
        , _hUnsupportedKbdType=unsupported_kbd_type
        } = fromMaybe mempty mHeaderErrors
  let ModifierErrors
        { _mUnsupportedModifierBits=unsupported_modifier_bits
        , _mUnsupportedModifiersCombos=unsupported_modifiers_combos
        , _mUnsupportedModifiers=unsupported_modifiers
        , _mUnsupportedVK=unsupported_vks2
        , _mNonRemappableVK=non_remappable_vks1
        , _mUnsupportedLevels=unsupportedLevels
        , _mTooMuchLevels=tooManyLevels
        } = fromMaybe mempty mModifierErrors
  let SymbolErrors
        { _sUnsupportedKeys=unsupported_keys2
        , _sUnsupportedVks = unsupported_vks3
        , _sNonRemappableVks = non_remappable_vks2
        , _sUnMappedVks = unmapped_vks
        , _sDiscardedActions = discarded_actions
        -- , _sDiscardedActions [TODO]
        , _sUnsupportedActions = unsupported_actions
        , _sUnsupportedLigatures = unsupported_ligatures
        , _sUnsupportedDks = unsupported_dks
        } = fromMaybe mempty mSymbolsErrors
  let unsupported_keys = unsupported_keys1 <> unsupported_keys2
  let dkErrors = fromMaybe mempty mDkErrors <> mempty{_dkUnsupported=unsupported_dks}
  let unsupported_vks = mconcat
                      [ unsupported_vks1
                      , unsupported_vks2
                      , unsupported_vks3 ]
  let non_remappable_vks = non_remappable_vks1 <> non_remappable_vks2
  putDoc $ mkError (mconcat ["[WARNING] [", gname, "] There were errors while generating the ", gname, " files:"]) <> line
  case unsupported_kbd_type of
    Nothing -> pure ()
    Just t  -> putDoc $ mkEmphasis ("• The Windows keyboard type is not supported: " <> (TL.pack . show $ t)) <> line
  unless (null unsupported_modifier_bits) do
    putDoc $ mkEmphasis (mconcat ["• The following modifiers bits are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_modifier_bits (putStrLn . showItem)
  unless (null unsupported_modifiers_combos) do
    putDoc $ mkEmphasis (mconcat ["• The following modifiers combos are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_modifiers_combos (putStrLn . showItem)
  unless (null unsupported_modifiers) do
    putDoc $ mkEmphasis (mconcat ["• The following modifiers are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_modifiers (putStrLn . showItem)
  unless (null unsupportedLevels) do
    putDoc $ mkEmphasis (mconcat ["• The following levels are not supported by the ", gname, " generator:"]) <> line
    void $ Map.traverseWithKey (\k v -> putStrLn $ showInvalidLevel k v) unsupportedLevels
  unless (null unsupported_keys) do
    putDoc $ mkEmphasis (mconcat ["• The following keys are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_keys (putStrLn . showItem)
  unless (null unsupported_vks) do
    putDoc $ mkEmphasis (mconcat ["• The following virtual keys are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_vks (putStrLn . showItem)
  unless (null non_remappable_vks) do
    putDoc $ mkEmphasis "• The following virtual keys cannot be remapped on Windows:" <> line
    forM_ non_remappable_vks (putStrLn . showItem)
  unless (null discarded_actions) do
    putDoc $ mkEmphasis "• The following actions were discarded:" <> line
    void $ Map.traverseWithKey (\k v -> putStrLn $ showDiscarded k v) discarded_actions
  unless (null unsupported_actions) do
    putDoc $ mkEmphasis (mconcat ["• The following special actions are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_actions (putStrLn . either showItem showItem)
  when tooManyLevels do
    putDoc $ mkEmphasis "• There are too many entries in the level index table. Some levels were discared." <> line
  unless (null unsupported_ligatures) do
    putDoc $ mkEmphasis (mconcat ["• The following character sequences are not supported by the ", gname, " generator:"]) <> line
    forM_ unsupported_ligatures (putStrLn . showItem)
  printDeadKeysErrors dkErrors
  unless (null unmapped_vks) do
    putDoc $ mkEmphasis "• The following dead virtual keys are mapped to key but have no actions defined in the corresponding group:" <> line
    forM_ unmapped_vks (putStrLn . showItemVK)
  where
    showItem :: Show a => a -> String
    showItem = ("  ⁃ " <>) . show
    showInvalidLevel k v = mconcat ["  ⁃ ", show k, ". Reason: ", show v]
    showDiscarded vk (as, r) = mconcat
      [ "  ⁃ ", show vk, ": ", show as, ".\n"
      , "    Reason: ", case r of
        Filtered -> "invalid virtual key"
        ReplacedBy as' -> "replaced by: " <> show as'
        Invalid key -> "invalid corresponding key: " <> show key
      ]
    showItemVK :: (Show b) => (WdkIdTL, b) -> String
    showItemVK (g, vk) = mconcat ["  ⁃ Group: “", TL.unpack . getWdkId $ g, "”, VK: ", show vk]
