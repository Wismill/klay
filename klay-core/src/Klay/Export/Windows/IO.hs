module Klay.Export.Windows.IO
  ( -- * File reading
    read_utf_16_le
  , read_utf_8
    -- * Files writing
  , mwrite
  , write_utf_16_le
  , write_utf_8
    -- * Paths
  , makePath
  , createDirectoryStructure
  ) where

import Data.Text qualified as T
import Data.Text.IO qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.IO qualified as TL
import System.IO (withFile, hSetEncoding, hSetNewlineMode, hPutChar, utf8, utf16le, IOMode(ReadMode, WriteMode), NewlineMode(..), Newline(..))
import System.FilePath ((</>), (<.>))
import System.Directory (createDirectoryIfMissing)


mwrite :: (FilePath -> TL.Text -> IO ()) -> String -> FilePath -> Maybe TL.Text -> IO ()
mwrite _     msg path Nothing = putStrLn . mconcat $ ["[ERROR] Cannot write ", msg, ": ", path]
mwrite write msg path (Just content) = do
  putStr . mconcat $ ["[INFO] Writing ", msg, ": ", path, "…"]
  write path content
  putStrLn " Done."

-- See: https://hackage.haskell.org/package/text-1.2.4.0/docs/Data-Text-Lazy-IO.html
-- | Write Windows files with UTF-16-LE encoding
write_utf_16_le :: FilePath -> TL.Text -> IO ()
write_utf_16_le path content = withFile path WriteMode \h -> do
  hSetEncoding h utf16le
  hSetNewlineMode h (NewlineMode CRLF CRLF)
  hPutChar h '\xFEFF' -- Add BOM
  TL.hPutStr h content

-- | Write Windows files with UTF-8 encoding
write_utf_8 :: FilePath -> TL.Text -> IO ()
write_utf_8 path content = withFile path WriteMode \handle -> do
  hSetEncoding handle utf8
  hSetNewlineMode handle (NewlineMode CRLF CRLF)
  TL.hPutStr handle content

read_utf_16_le :: FilePath -> IO T.Text
read_utf_16_le path = withFile path ReadMode \h -> do
  hSetEncoding h utf16le
  hSetNewlineMode h (NewlineMode CRLF CRLF)
  T.tail <$> T.hGetContents h -- Remove BOM

read_utf_8 :: FilePath -> IO T.Text
read_utf_8 path = withFile path ReadMode \h -> do
  hSetEncoding h utf8
  hSetNewlineMode h (NewlineMode CRLF CRLF)
  T.hGetContents h

makePath :: FilePath -> String -> String -> FilePath
makePath destination_dir system_name ext = destination_dir </> system_name <.> ext

-- | Create a directory structure.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure = createDirectoryIfMissing True
