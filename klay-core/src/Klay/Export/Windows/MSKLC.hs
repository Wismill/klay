{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.MSKLC
  ( MsKlcC(..)
  , generateMsklc
  , generateOnlyKlcFile
  , prepareLayout_
  ) where

import Data.Ix (Ix(..))
import Data.Text.Lazy qualified as TL
import Data.Set qualified as Set
import Data.Functor ((<&>))
import Control.Monad.State.Strict

import Data.Aeson (KeyValue(..), object)

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Export.Linux.XKB.Header (createHeader)
import Klay.Export.Windows.Common.Layout qualified as L
import Klay.Export.Windows.Common.Modifiers (ModifierErrors(..))
import Klay.Export.Windows.Common.Symbols (SymbolErrors(..))
import Klay.Export.Windows.IO (makePath, createDirectoryStructure, mwrite, write_utf_8, write_utf_16_le)
import Klay.Export.Windows.Lookup.VirtualKeys (isReservedVk)
import Klay.Export.Windows.MSKLC.KlcFile (generateGroupKlc)
import Klay.Export.Windows.WDK (generateWdk', printErrors)
import Klay.Keyboard.Layout.Action.DeadKey (DeadKeyError)
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.VirtualKey (VirtualKey(..))
import Klay.OS (MultiOs(..))
import Klay.Utils.Mustache qualified as Mustache

-- | MSKLC option to produce or not C source files
data MsKlcC = KlcWithoutC | KlcWithC
  deriving (Eq, Ord, Enum, Bounded, Show)

-- | Generate files for Microsoft Keyboard Layout Creator
generateMsklc :: FilePath -> MultiOsRawLayout -> MsKlcC -> IO ()
generateMsklc destination_dir layout c_sources = do
  createDirectoryStructure destination_dir
  -- [TODO] Warn the user that we process only the first group
  (klcContent, mErrors2, sErrors2, dkErrors) <- klc_file'
  mwrite write_utf_16_le "MSKLC file" (makePath' ".klc") klcContent
  let mErrors = mErrors1 <> mErrors2
  let sErrors = sErrors1 <> sErrors2
  if c_sources == KlcWithC
    then generateCSources mErrors destination_dir layout
    else printErrors "MSKLC" Nothing mErrors sErrors dkErrors
  where
    (layout', (mErrors1, sErrors1)) = runState (prepareLayout layout) (Nothing, Nothing)
    klc_file' = createHeader layout' "//"
            >>= generateKlcFile layout'
    system_name = mkLayoutId' layout
    makePath' = makePath destination_dir system_name

-- | Generate the @klc@ file itself, for testing.
generateOnlyKlcFile :: MultiOsRawLayout -> TL.Text -> IO (Maybe TL.Text, Maybe ModifierErrors, Maybe SymbolErrors, Maybe DeadKeyError)
generateOnlyKlcFile layout header =
  generateKlcFile layout' header <&>
  \(content, mErrors2, sErrors2, dkErrors) ->
    (content, mErrors1 <> mErrors2, sErrors1 <> sErrors2, dkErrors)
  where
    (layout', (mErrors1, sErrors1)) = runState (prepareLayout layout) (Nothing, Nothing)

type S = State (Maybe ModifierErrors, Maybe SymbolErrors)

-- | Ensure the layout is MSKLC-compatible
prepareLayout_ :: MultiOsRawLayout -> L.MsklcLayout
prepareLayout_ = flip evalState mempty . prepareLayout

-- | Ensure the layout is MSKLC-compatible
prepareLayout :: MultiOsRawLayout -> S L.MsklcLayout
prepareLayout = L.prepareLayout supportedBits supported_combos is_valid_key is_valid_vk
  where
    supportedBits = [Shift, Capitals, Control, Alternate]

    supported_combos =
      [ M.isoLevel1
      , M.isoLevel2
      , M.isoLevel2Caps
      , M.isoLevel2ShiftCaps
      , M.altGr
      , M.altGrShift
      , M.altGrCaps
      , M.control
      , M.control <> M.shift
      ]

    is_valid_key = (`Set.member` supported_keys)
    is_valid_vk VK_RMENU = True
    is_valid_vk vk = not . isReservedVk $ vk-- [TODO] restrict further

    supported_keys :: Set.Set Key
    supported_keys
      =  Set.fromList (range (K.FirstAlphanumericKey, maxBound))
      <> [K.KPDecimal, K.RAlternate]

generateCSources :: Maybe ModifierErrors -> FilePath -> MultiOsRawLayout -> IO ()
generateCSources modifierErrors destination_dir layout = do
  generateWdk' False "MSKLC" modifierErrors destination_dir layout
  header <- createHeader layout "#"
  generateScripts system_name header >>= mwrite write_utf_8 "MSKLC scripts" (makePath' "build" ".ps1")
  where
    system_name = mkLayoutId' layout
    makePath' = makePath destination_dir

-- | Generate a script to compile manually the @klc@ file.
generateScripts :: String -> TL.Text -> IO (Maybe TL.Text)
generateScripts system_name header = do
  Mustache.generateFileContentFromTemplate "MSKLC Powershell script" template_file vars
  where
    template_file = "windows/msklc/template.ps1"
    vars = object
      [ "base_file" .= system_name
      , "header" .= header ]

-- | Generate the @klc@ file itself, with no layout preprocessing.
generateKlcFile :: L.MsklcLayout -> TL.Text -> IO (Maybe TL.Text, Maybe ModifierErrors, Maybe SymbolErrors, Maybe DeadKeyError)
generateKlcFile layout header =
  generateGroupKlc system_name metadata options dkd header . firstGroupMapping $ layout
  where
    system_name = mkLayoutId' layout
    metadata = _metadata layout
    options = _windows . _options $ layout
    dkd = _deadKeys layout
