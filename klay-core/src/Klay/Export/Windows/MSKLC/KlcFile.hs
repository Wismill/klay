{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.MSKLC.KlcFile
  ( generateGroupKlc
  ) where

import Data.Char (isSpace)
import Data.Maybe (isNothing, fromMaybe)
import Data.List (intersperse)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Functor ((<&>))
import Data.Bifunctor (Bifunctor(..))
import Control.Arrow ((&&&))

import Data.Versions qualified as V
import Data.BCP47 qualified as BCP47
import Data.Aeson (KeyValue(..), object)
import TextShow (showb)

import Klay.Export.Windows.Common.DeadKeys (safeDeadKeyChars)
import Klay.Export.Windows.Common.Layout
import Klay.Export.Windows.Common.Modifiers
  (WLevel(..), ModifierErrors(..), mkLevelsDefinition, mkModifierBit, levelsByValidModifiersCombo)
import Klay.Export.Windows.Common.KeyNames qualified as K
import Klay.Export.Windows.Common.Symbols (SymbolErrors(..))
import Klay.Export.Windows.Lookup.Characters (toHex', toWinSymbol, toWinEncoding')
import Klay.Export.Windows.Lookup.Locales
  (WindowsLocale(..), getWinLocale, mkCustomLocaleId, localeEN)
import Klay.Export.Windows.MSKLC.DeadKeys qualified as DeadKeys
import Klay.Export.Windows.MSKLC.Symbols qualified as Symbols
import Klay.Import.Windows.MSKLC (defaultEmptyField)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey (DeadKeyError)
import Klay.Keyboard.Layout.Modifier (ModifiersCombo, maskToModifiers)
import Klay.Utils.Mustache qualified as Mustache
import Klay.Utils.Unicode (padR)
import Klay.Utils.UserInput (rawTextToString, escapeLine, escapeControlNewlines)


generateGroupKlc
  :: FilePath -- ^ Layout system name
  -> LayoutMetadata
  -> WindowsOptions
  -> DeadKeyDefinitions
  -> TL.Text -- ^ Header
  -> MsklcGroup
  -> IO (Maybe TL.Text, Maybe ModifierErrors, Maybe SymbolErrors, Maybe DeadKeyError)
generateGroupKlc system_name metadata options dkd header g
  =   (, modifierErrors, symbolErrors, dkErrors)
  <$> Mustache.generateFileContentFromTemplate "MSKCLC file" template_file vars
  where
    template_file = "windows/msklc/template.klc"
    is_valid_level = either
      (\ms -> not (isInvalidModifiersCombo ms) && not (isOptionalCombo ms))
      (not . isInvalidModifiersCombo)
    validLevels = levelsByValidModifiersCombo is_valid_level g
    validLevels' = fmap (\(wl, l, _) -> (wl, l)) <$> validLevels
    (shiftStates, modifierErrors) = makeModifiers g
    layoutHeader = mkSymbolsHeader (fmap (\(_, l, _) -> l) <$> validLevels)
    (symbols_entries, chars, dks, ligaturesMap, symbolErrors1) = Symbols.prepareSymbols options validLevels' g
    ligatures = mkLigatures (fmap (\(wl, _, _) -> wl) <$> validLevels) ligaturesMap
    valid_ligatures = Map.keysSet ligaturesMap -- [TODO] check
    (deadDeysDef, deadKeysNames, dkm, _dk_chars, dkErrors) =
      DeadKeys.mkDeadKeys dkd dks chars safeDeadKeyChars
    (symbols, symbolErrors2) = Symbols.finalizeSymbols dkm valid_ligatures symbols_entries
    symbolErrors = symbolErrors1 <> symbolErrors2
    -- Note: some fields are "semi" mandatory: although MSKLC can load files without them,
    -- it cannot compile them. So we provide a default value to avoid the error.
    escapeLine' = maybe defaultEmptyField escapeLine
    vars = object
        [ "header" .= header
        , "system_name" .= system_name
        , "keyboard_label" .= (escapeLine . _name $ metadata)
        , "copyright" .= (escapeLine' . _license $ metadata)
        , "company" .= (escapeLine' . _author $ metadata)
        , "locale_name" .= localeName
        , "locale_id" .= localeId
        , "version" .= version
        , "attributes" .= attributes
        , "shift_states" .= shiftStates
        , "layout_header" .= layoutHeader
        , "layout_mapping" .= symbols
        , "ligatures" .= ligatures
        , "dead_keys" .= deadDeysDef
        , "keys_names" .= keys_names
        , "keys_ext_names" .= keys_ext_names
        , "dead_keys_names" .= deadKeysNames
        , "descriptions" .= descriptions
        , "language_names" .= language_names ]
    -- System
    version = maybe mempty (V.prettyVer . unVersion) . _version $ metadata
    layoutLocale = mainLayoutLocale . _locales $ metadata
    winLocale = fromMaybe localeEN $ getWinLocale (T.unpack localeName)
    localeName = BCP47.toText layoutLocale
    localeId = mkCustomLocaleId winLocale
    language_names = mconcat [_wLanguageId localeEN, "\t", _wLanguage winLocale]
    descriptions =
      (fmap (TL.unpack . escapeLine) . _description) metadata <&> \d ->
      mconcat if winLocale /= localeEN
        then [ _wLanguageId winLocale,    "\t", d, "\n"
             , _wLanguageId localeEN, "\t", d ]
        else [ _wLanguageId localeEN, "\t", d ]
    attributes
      | _wAltgr options = "\nATTRIBUTES\nALTGR"
      | otherwise       = mempty :: TL.Text
    -- Key names
    (keys_names, keys_ext_names) = mkKeyNames (_wKeysNames options)

-- [FIXME] move the prepare layout part here ?
makeModifiers :: MsklcGroup -> (TL.Text, Maybe ModifierErrors)
makeModifiers g =
  let (modifierErrors, _, shiftStates) = mkLevelsDefinition mkHeader mkSymbolIndexEntry mempty g
  in (shiftStates, modifierErrors)
  where
    mkHeader n = mconcat ["// Index -> ", padR n ' ' "Modifiers pressed", "-> Level name\n"]
    mkSymbolIndexEntry ml ms comment
      -- Invalid entries
      | isInvalidModifiersCombo ms = mempty
      -- Optional entries
      | isNothing ml && isOptionalCombo ms = mempty
      -- Mandatory entries
      | otherwise =
        let levelIndex = showb $ mkModifierBit ms
            levelName = maybe "(No level defined)" (TB.fromLazyText . escapeLine . snd) ml
        in mconcat [padR 8 ' ' levelIndex, " // ", comment, " -> ", levelName, "\n"]

isInvalidModifiersCombo :: ModifiersCombo -> Bool
isInvalidModifiersCombo = (`Set.member` [[Alternate], [Shift, Alternate]])

isOptionalCombo :: ModifiersCombo -> Bool
isOptionalCombo = (== [Shift, Control])

mkSymbolsHeader :: [Maybe Level] -> TL.Text
mkSymbolsHeader levels = TB.toLazyText $ mconcat
  [ "// SC VK_         Caps", mconcat (fmap mkLevelName levels), "\n"
  , "// -- ----------- ----", mconcat (fmap (const " -----") levels)
  ]
  where
    mkLevelName = (" " <>) . Symbols.paddingWchar . maybe "none" (showb . mkModifierBit . maskToModifiers)

mkLigatures :: [Maybe WLevel] -> Symbols.CharSequenceMapping -> TL.Text
mkLigatures mLevels = TB.toLazyText . Map.foldrWithKey go mempty . fmap (mkComment &&& mkLigature)
  where
    go (vk, WLevel level) (comment, liga) acc = mconcat
      [ Symbols.paddingVk . TB.fromLazyText $ vk, "\t"
      , paddingLevel . showb $ levels !! level, "\t"
      , mconcat . intersperse "\t" $ liga
      , "\t// ", comment, "\n", acc ]
    mkComment = TB.fromLazyText . escapeControlNewlines
    mkLigature = mconcat . fmap mkWinChar . rawTextToString
    mkWinChar = fmap paddingWchar . either toWinEncoding' ((:[]) . toHex') . toWinSymbol
    paddingWchar = padR 4 ' '
    paddingLevel = padR 2 ' '
    -- Fix offset for undefined levels
    levels = reverse . fst $ foldl go' (mempty, 0) mLevels
      where go' (acc, offset) = \case
              Nothing -> (acc, offset + 1)
              Just wl -> (offset + getWLevel wl : acc, offset)

mkKeyNames :: K.KeyNames -> (TL.Text, TL.Text)
mkKeyNames
  = bimap mkNames mkNames
  . K.mkKeyNames
  where
    mkNames = TB.toLazyText . foldr mkName mempty
    mkName (key, name) =
      let key' = TB.fromLazyText key
          name'
            | TL.any isSpace name = mconcat ["\"", TB.fromLazyText name, "\""]
            | otherwise           = TB.fromLazyText name
      in (mconcat ["\n", key', "\t", name'] <>)
