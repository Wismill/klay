{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.MSKLC.Symbols
  ( SymbolState(..)
  , SymbolErrors(..)
  , DeadKeyBaseCharMapping
  , CharSequenceMapping
  , LigatureRef
  , WdkId(..)
  , WdkIdTL
  , prepareSymbols
  , finalizeSymbols
  , paddingVk
  , paddingWchar
  ) where

import Data.Bits ((.|.))
import Data.Char (isSpace)
import Data.List (intersperse, sort)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Functor (($>))
import Data.Foldable (foldl')
import Control.Monad.State.Strict

import TextShow (showb)

import Klay.Export.Windows.Common.Layout (MsklcGroup)
import Klay.Export.Windows.Common.Modifiers
  (WLevel, checkModifierVk, hasCapsLock, hasAltGrCapsLock)
import Klay.Export.Windows.Common.Symbols
  ( SymbolErrors(..), SymbolState(..), DeadKeyBaseCharMapping, CharSequenceMapping, LigatureRef
  , WAction(..), WdkId(..), WdkIdTL
  , addReachableChar, addReachableDk
  , actionsByMaybeWLevel, addCharSequence, addUnsupportedAction
  , addUnsupportedKey, addNonRemappableVk)
import Klay.Export.Windows.Lookup.Characters (toWcharUnprefixedAll, toWcharUnprefixed')
import Klay.Export.Windows.Lookup.Keycodes (toWinKeycode)
import Klay.Export.Windows.Lookup.VirtualKeys (toWinVkUnprefixed, isReservedVk)
import Klay.Keyboard.Layout (WindowsOptions(..), Key, DeadKey)
import Klay.Keyboard.Layout.Action
  ( Action(..), Actions, ResolvedActions, CustomActions(..), Level
  , untagCustomisableActions, specialActionToMonogram)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier (ModifiersOptions, modifiersOptionsToList)
import Klay.Keyboard.Layout.VirtualKey (VirtualKey(..))
import Klay.Utils.Unicode (padR)
import Klay.Utils.UserInput (RawText(..), RawLText, escapeControlNewlines)

type S = State SymbolState

-- | An entry for a pair ('Key', 'VirtualKey')
data Entry
  = InvalidKey String InvalidKeyReason
  | InvalidVk String TB.Builder InvalidVkReason
  | ValidEntry String TB.Builder ModifiersOptions [(Action, WAction)]
  | Skip
  deriving (Eq, Show)

data InvalidKeyReason
  = UnsupportedKey
  | NonMappableKey
  deriving (Eq, Show)

data InvalidVkReason
  = UnsupportedVk
  | NonMappableVk
  | UnMappedVk
  deriving (Eq, Ord, Show)

instance Ord Entry where
  InvalidKey k1 _     `compare` InvalidKey k2 _     = compare k1 k2
  InvalidKey _ _      `compare` _                   = LT
  _                   `compare` InvalidKey _ _      = GT
  InvalidVk k1 _ _    `compare` InvalidVk k2 _ _    = compare k1 k2
  InvalidVk k1 _ _    `compare` ValidEntry k2 _ _ _ = compare k1 k2
  ValidEntry k1 _ _ _ `compare` ValidEntry k2 _ _ _ = compare k1 k2
  ValidEntry k1 _ _ _ `compare` InvalidVk k2 _ _    = compare k1 k2
  Skip                `compare` Skip                = EQ
  Skip                `compare` _                   = GT
  _                   `compare` Skip                = LT

-- | Preprocess actions
prepareSymbols
  :: WindowsOptions
  -> [Maybe (WLevel, Level)]
  -> MsklcGroup
  -> ([Entry], Set.Set Char, Set.Set DeadKey, CharSequenceMapping, Maybe SymbolErrors)
prepareSymbols opts mWLevels g = (symbols, chars, dks, ligatures, errors)
  where
    (symbols, SymbolState chars dks ligatures errors) = runState preprocessGroup' mempty
    preprocessGroup' = sort <$> foldrGroupMappingWithKeyM (preprocessKeys opts mWLevels) mempty g

-- | Preprocess actions of a specific pair ('Key', 'VK.VirtualKey')
preprocessKeys
  :: WindowsOptions
  -> [Maybe (WLevel, Level)]
  -> (Key, VirtualKey)
  -> ResolvedActions
  -> [Entry]
  -> S [Entry]
-- OS default for non-remappable key: skip
preprocessKeys _ _ (_, vk0) (ADefault _) acc
  | isReservedVk vk0 = pure acc
preprocessKeys _ mWLevels (key0, vk0) as0 acc = (:acc) <$> case toWinKeycode key0 of
  -- No scan code defined
  Nothing -> unsupported_key key0 (show key0) UnsupportedKey
  -- Scan code starting with 'T'
  Just key_str@('T':_) -> let win_vk = toWinVkUnprefixed vk0
    in if not (isReservedVk vk0)
      -- Only process remappable keys
      then processVk key_str win_vk (untagCustomisableActions as0)
      else nonRemappableVk (show_key key0 key_str) win_vk vk0 as0
  -- Other scan code
  Just key_str -> case as0 of
    (ADefault _) -> pure Skip -- Skip OS default mapping
    (ACustom as) -> if checkModifierVk vk0 as
      then pure Skip
      else unsupported_key key0 (show_key key0 key_str) NonMappableKey
  where
    -- groupId = escapeGroupId g
    show_key key key_str = mconcat [key_str, " (", show key, ")"]

    unsupported_key :: Key -> String -> InvalidKeyReason -> S Entry
    unsupported_key key key_str cause = addUnsupportedKey key $> InvalidKey key_str cause

    nonRemappableVk :: String -> TB.Builder -> VirtualKey -> ResolvedActions -> S Entry
    nonRemappableVk _   _      _  (ADefault _) = pure Skip -- Skip OS default mapping
    nonRemappableVk key vk_str vk (ACustom as)
      | checkModifierVk vk as = pure Skip -- Skip supported modifier
      | otherwise                =  addNonRemappableVk vk
                                 $> InvalidVk key vk_str NonMappableVk

    processVk :: String -> TB.Builder -> Actions -> S Entry
    processVk key_str win_vk as = mkVkActions (TB.toLazyText win_vk) as >>= \(mopts, as') ->
      pure $ ValidEntry key_str win_vk mopts as'

    mkVkActions :: TL.Text -> Actions -> S (ModifiersOptions, [(Action, WAction)])
    mkVkActions vk_str as = traverse mkVkActions' (actionsByMaybeWLevel mWLevels as)
      where
        mkVkActions' = traverse mkVkAction'
        mkVkAction' (Nothing, a) = pure (a, WNoAction)
        mkVkAction' (Just l, a)  = (a,) <$> mkVkAction vk_str l a

    mkVkAction :: TL.Text -> WLevel -> Action -> S WAction
    mkVkAction _ _ (AChar c) =
      addReachableChar c $> (WChar . toWcharUnprefixedAll $ c)
    mkVkAction vk level (AText liga) = do
      (`WLigature` liga) <$> addCharSequence vk level liga
    mkVkAction _ _ (ADeadKey dk) =
      addReachableDk dk $> WDeadKey dk
    mkVkAction _ _ (AModifier m) =
      addUnsupportedAction (Left m) $> WNoAction
    mkVkAction vk level (ASpecial a) = case specialActionToMonogram a of
      Nothing -> addUnsupportedAction (Right a) $> WNoAction
      Just c  -> mkVkAction vk level (AChar c)
    mkVkAction _ _ NoAction         = pure WNoAction
    mkVkAction _ _ UndefinedAction  = pure WNoAction

-- | Generate data related to the @LAYOUT@ section
finalizeSymbols
  :: DeadKeyBaseCharMapping
  -> Set.Set LigatureRef
  -> [Entry]
  -> (TL.Text, Maybe SymbolErrors)
finalizeSymbols dkm ls actions =
  let (lazy_symbols, errors) = runState (traverse go actions) mempty
  in (TB.toLazyText . mconcat $ lazy_symbols, errors)
  where
    go (InvalidKey key cause) = make_invalid_key key cause
    go (InvalidVk key win_vk reason) = make_invalid_vk reason key win_vk
    go (ValidEntry key win_vk opts as)
      -- There is a bug in kbdutool: if a key has all his entries defined
      -- to -1, then it uses some default value (Qwerty?) that may clash with VK remapping.
      -- As a workaround, set a value for the base level.
      | all ((== WNoAction) . snd) as =
        let as' = (AChar '�', WChar "fffd") : drop 1 as
        in go (ValidEntry key win_vk opts as')
      | otherwise = makeRow key win_vk opts <$> traverse make_valid as
    go Skip = pure mempty
    make_invalid_key :: String -> InvalidKeyReason -> State (Maybe SymbolErrors) TB.Builder
    make_invalid_key key UnsupportedKey = pure $
      mconcat ["// [ERROR] Unsupported key: ", TB.fromString key, "\n"]
    make_invalid_key key NonMappableKey = pure $
      mconcat ["// [ERROR] Non mappable key: ", TB.fromString key, "\n"]
    make_invalid_vk UnsupportedVk key vk = pure $
      mconcat ["// [ERROR] Key: ", TB.fromString key, "; Unsupported virtual key: ", vk, "\n"]
    make_invalid_vk NonMappableVk key win_vk = pure $
      mconcat ["// [ERROR] Key: ", TB.fromString key, "; Non remappable virtual key: ", win_vk, "\n"]
    make_invalid_vk UnMappedVk key win_vk = pure $
      mconcat ["// [WARNING] Key: ", TB.fromString key, "; Virtual key ", win_vk, " has no actions.\n"]
    make_valid :: (Action, WAction) -> State (Maybe SymbolErrors) (TB.Builder, TB.Builder)
    make_valid (_, WNoAction) = pure (wchNone, empty_symbol)
    make_valid (_, WLigature liga_ref liga)
      | Set.member liga_ref ls = pure ("%%", escape_liga liga)
      | otherwise = addUnsupportedLigature liga $> (wchNone, empty_symbol)
    make_valid (AChar c', WChar c)
      | isSpace c' = pure (c, "U+" <> c)
      | otherwise  = pure (c, escapeChar c')
    make_valid (_, WChar c) = pure (c, "U+" <> c)
    make_valid (_, WDeadKey dk) = case Map.lookup dk dkm of
      Nothing -> addUnsupportedDk dk $> (wchNone, empty_symbol)
      Just dk_ref -> pure (toWcharUnprefixed' dk_ref <> "@", mconcat ["‹", showb dk, "›"])
    wchNone = "-1"
    empty_symbol = "�"
    makeRow key vk opts as = mconcat
      [ TB.fromString . drop 1 $ key, "    "
      , paddingVk vk, " "
      , makeLocks opts, "    "
      , makeSymbols as1, " // "
      , makeComment as2, "\n" ]
      where (as1, as2) = unzip as
    makeSymbols = mconcat . intersperse " " . fmap paddingWchar
    makeComment = mconcat . intersperse " "
    escapeChar = TB.fromLazyText . escapeControlNewlines . RawText . TL.singleton
    escape_liga = TB.fromLazyText . escapeControlNewlines
    makeLocks opts = showb flags
      where
        flags = foldl' go' (0 :: Int) $ modifiersOptionsToList opts
        -- [TODO] warning if option not supported
        go' acc (m, opt)
          | hasCapsLock      m opt = 0x01 .|. acc -- CAPLOK
          | hasAltGrCapsLock m opt = 0x04 .|. acc -- CAPLOKALTGR
        -- [TODO] handle all the remaining options properly (SGCAPS, KANALOK)
        -- SGCAPS: `SGCap`
        -- KANALOK: 0x08
        go' acc _ = acc

addUnsupportedLigature :: RawLText -> State (Maybe SymbolErrors) ()
addUnsupportedLigature l = modify (Just mempty{_sUnsupportedLigatures=[l]} <>)

addUnsupportedDk :: DeadKey -> State (Maybe SymbolErrors) ()
addUnsupportedDk dk = modify (Just mempty{_sUnsupportedDks=[dk]} <>)

-- | Add padding for a wchar with a possible flag @@@
paddingWchar :: TB.Builder -> TB.Builder
paddingWchar = padR 5 ' '

-- | Add padding for an unprefixed virtual key identifier.
paddingVk :: TB.Builder -> TB.Builder
paddingVk = padR 11 ' '
