{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.MSKLC.DeadKeys
  ( DeadKeyError(..)
  , mkDeadKeys
  , mkDeadKeysNames
  ) where

import Data.Set qualified as Set
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Map.Strict qualified as Map

import Klay.Export.Windows.Common.DeadKeys
  (ChainedDeadKeyProcessing(..), MkDkMapping, MkEntry, MkPartialEntry, mkDkContent, processDkMapping)
import Klay.Export.Windows.Lookup.Characters (WindowsChar, toWinSymbol, toWcharUnprefixed', toHex')
import Klay.Export.Windows.WDK.C.Symbols (DeadKeyBaseCharMapping)
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.UserInput (escapeLine)
import Klay.Utils.Unicode (padR)

-- | Generate the dead keys definitions, names and encoding
mkDeadKeys
  :: DeadKeyDefinitions
  -> Set.Set DeadKey
  -> Set.Set Char
  -> String
  -> (TL.Text, TL.Text, DeadKeyBaseCharMapping, String, Maybe DeadKeyError)
mkDeadKeys vkd dks cs dk_chars =
  let (dkDefs, dkm, dk_chars', errors) =
        mkDkContent processDkMapping' RemoveChainedDeadKeys vkd dks cs dk_chars
      dk_names = mkDeadKeysNames dkm
  in (dkDefs, dk_names, dkm, dk_chars', errors)

processDkMapping' :: MkDkMapping
processDkMapping' = processDkMapping mkDkHeader mkEntry mkPartialEntry

mkDkHeader :: DeadKey -> WindowsChar -> TB.Builder
mkDkHeader dk dk_ref =
  mconcat ["DEADKEY ", toWcharUnprefixed' dk_ref, "\t// ", make_comment dk, "\n\n"]
  where
    make_comment = TB.fromLazyText . fix_rsolidus . TL.pack . prettyDeadKey
    fix_rsolidus t = case TL.unsnoc t of
      Just (t', '\\') -> t' <> "U+002F"
      _               -> t

mkEntry :: MkEntry
mkEntry _ entryP comment = mconcat [entryP, "\t// ", comment, "\n"]

mkPartialEntry :: MkPartialEntry
mkPartialEntry lookupDk s r = case (mkSymbol s, mkResult r) of
  (Nothing, Nothing) -> Left $ DeadKeyComboError [s] (Just r)
  (Just _, Nothing) -> Left $ DeadKeyComboError mempty (Just r)
  (Nothing, Just _) -> Left $ DeadKeyComboError [s] Nothing
  (Just s', Just r') -> Right (mconcat [s', "\t", r'], comment)
  where
    mkSymbol :: Symbol -> Maybe TB.Builder
    mkSymbol (SChar c) = mkChar c
    mkSymbol (SDeadKey dk') = toWcharUnprefixed' <$> lookupDk dk'
    mkResult :: Result -> Maybe TB.Builder
    mkResult (RChar c) = mkChar c
    mkResult _         = Nothing -- [NOTE] Chained dead keys not supported
    mkChar :: Char -> Maybe TB.Builder
    mkChar c = case toWinSymbol c of
      Left _   -> Nothing
      Right c' -> Just (toHex' c')
    comment = TB.fromString . mconcat $ [prettySymbol s, " -> ", prettyResult r]

-- | Create the content of @aKeyNamesDead@, the table of the dead keys names
mkDeadKeysNames :: DeadKeyBaseCharMapping -> TL.Text
mkDeadKeysNames dkm
  | null dkm = mempty
  | otherwise = "\nKEYNAME_DEAD\n\n" <> TB.toLazyText (Map.foldrWithKey go mempty dkm)
  where
    go dk dk_ref acc = mconcat [make_dk_ref dk_ref, "\t\"", make_dk_name dk, "\"\n", acc]
    make_dk_ref = padR 4 ' '  . toHex'
    make_dk_name = TB.fromLazyText . escapeLine . _dkName
