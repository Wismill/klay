{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Common.DeadKeys
  ( DeadKeyError(..)
  , DeadKeyState(..)
  , ChainedDeadKeyProcessing(..)
  , MkDkContent
  , MkDkMapping
  , MkEntry
  , MkPartialEntry
  , LookupDk
  , mkDkContent
  , processDkMapping
  , safeDeadKeyChars
  , paddingSymbol
  ) where


import Data.List.NonEmpty qualified as NE
import Data.Char (GeneralCategory(..), generalCategory)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Foldable (foldrM, traverse_)
import Data.Functor (($>))
import Data.Bifunctor (Bifunctor(..))
import Control.Monad.State.Strict

import Klay.Export.Windows.Lookup.Characters (WindowsChar, toWinSymbol)
import Klay.Export.Windows.WDK.C.Symbols (DeadKeyBaseCharMapping)
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Utils.UserInput (RawText(..))
import Klay.Utils.Unicode (padR)

type S = State DeadKeyState

data DeadKeyState = DeadKeyState
  { _errors :: Maybe DeadKeyError
  , _dkDefinitions :: MergedDeadKeyDefinitions
  , _deadKeysMapping :: DeadKeyBaseCharMapping
  , _dkSTempDeadKeys :: Map.Map (DeadKey, Symbol) DeadKey
  , _availableDkChars :: String
  }

data ChainedDeadKeyProcessing
  = CondenseChainedDeadKeys
  | RemoveChainedDeadKeys

type MkDkContent
  =  DeadKeyDefinitions
  -> Set.Set DeadKey
  -> Set.Set Char
  -> String
  -> (TL.Text, DeadKeyBaseCharMapping, String, Maybe DeadKeyError)
type MkDkMapping
  =  DeadKeyBaseCharMapping
  -> MergedDeadKeyDefinitions
  -> DeadKey
  -> TB.Builder
  -> S TB.Builder
type MkDeadKeyHeader = DeadKey -> WindowsChar -> TB.Builder
type MkEntry = WindowsChar -> TB.Builder -> TB.Builder -> TB.Builder
type MkPartialEntry = LookupDk -> Symbol -> Result -> Either DeadKeyComboError (TB.Builder, TB.Builder)
type LookupDk = DeadKey -> Maybe WindowsChar

{-| Characters to use for dead key encoding

__Note:__ Due to the following C macro `MAKELONG`, we are limitated to the values U+0000..U+7fff to encode the dead keys

@
  ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
@
-}
safeDeadKeyChars :: String
safeDeadKeyChars = filter (isSafeCategory . generalCategory) ['\x0000'.. '\x7fff']
  where isSafeCategory NotAssigned = True
        isSafeCategory PrivateUse = True
        isSafeCategory _ = False

-- | Generate the dead keys definitions
mkDkContent :: MkDkMapping -> ChainedDeadKeyProcessing -> MkDkContent
mkDkContent mkDkMapping mode dkd0 dks0 chars0 dk_chars0 =
  (TB.toLazyText lazyContent, dkEncoding, dk_chars', errors)
  where
    dkd1 = cleanupDeadKeyDefinitions (Just chars0) dks0 dkd0

    dkEncoding :: DeadKeyBaseCharMapping
    lazyContent :: TB.Builder
    errors :: Maybe DeadKeyError
    ((dkEncoding, lazyContent), DeadKeyState errors _ _ _ dk_chars') =
      flip runState (DeadKeyState Nothing (_dkMergedDefinitions dkd0) mempty mempty dk_chars0) do
        DeadKeyDefinitions{_dkMergedDefinitions=dkd2, _dkErrors=resolveErrors} <- case mode of
          CondenseChainedDeadKeys -> unflattenDeadKeyDefinitions mergeDk dkd1
          RemoveChainedDeadKeys   -> pure . cleanupDeadKeyDefinitions (Just chars0) dks0
                                  $ removeChainedDeadKeys dkd1
        when (resolveErrors /= mempty) do
          modify \s@DeadKeyState{_errors=err} -> s{_dkDefinitions=dkd2, _errors=Just resolveErrors<>err}
        traverse_ processUnmapped (Map.keys dkd2)
        dkm' <- _deadKeysMapping <$> get
        (dkm',) <$> foldrM (mkDkMapping dkm' dkd2) mempty (Map.keysSet dkd2)

    mergeDk :: DeadKey -> Symbol -> S (Maybe DeadKey)
    mergeDk dk symbol = do
      s@(DeadKeyState _errors _dkd dkm dks cs0) <- get
      case Map.lookup (dk, symbol) dks of
        Just dk' -> pure . Just $ dk' -- already created this dk
        Nothing -> let {
          retry_with cs = case cs of
            -- Cannot encode a new DK: no symbols left
            -- [NOTE] The error will be also reported when reaching the combo with 2 or more symbols
            [] -> addUnsupportedDk (make_dk '\0' symbol) $> Nothing
            -- Some symbols available
            c:cs' -> case toWinSymbol c of
              Left _ -> retry_with cs' -- retry
              Right dk_ref -> if dk_ref `elem` dkm -- [FIXME] costly check -- ok
                then retry_with cs' -- already used, retry
                else do
                  let dk' = make_dk c symbol
                  put s
                    { _deadKeysMapping=Map.insert dk' dk_ref dkm
                    , _dkSTempDeadKeys=Map.insert (dk, symbol) dk' dks
                    , _availableDkChars=cs' }
                  pure . Just $ dk'
          } in retry_with cs0
      where
        make_dk bc (SChar c) = DeadKey
          { _dkName = mconcat [_dkName dk, " + ", RawText . TL.singleton $ c]
          , _dkLabel = mconcat [_dkLabel dk, " + ", RawText . TL.singleton $ c]
          , _dkBaseChar = bc }
        make_dk bc (SDeadKey dk') = DeadKey
          { _dkName = mconcat [_dkName dk, " + ", _dkName dk']
          , _dkLabel = mconcat [_dkLabel dk, " + ", _dkLabel dk']
          , _dkBaseChar = bc }

    -- | Encode a dead key
    processUnmapped :: DeadKey -> S ()
    processUnmapped dk@DeadKey{_dkBaseChar=bc} = do
      DeadKeyState{_deadKeysMapping=dkm} <- get
      -- Proceed only if the dead key not already encoded
      when (Map.notMember dk dkm) do
        -- Use the provided base char if valid or create a new one
        either (const newbaseChar) addNewDkCode (toWinSymbol bc)
      where
        addNewDkCode :: WindowsChar -> S ()
        addNewDkCode dk_ref
          = modify (\s -> s{_deadKeysMapping=Map.insert dk dk_ref (_deadKeysMapping s)})
        -- [TODO] We should try to find a better solution than some random characters
        newbaseChar :: S ()
        newbaseChar = nextBaseChar >>= \case
          -- Cannot create new dead key: no Unicode non-character left
          Nothing -> addUnsupportedDk dk
          -- Use the next special character to create a new dead key
          Just c  -> case toWinSymbol c of
            Left _       -> processUnmapped dk -- Unsupported character. Skip and retry
            -- [FIXME] We should check that there is no collisions with the DK encodings!
            Right dk_ref -> addNewDkCode dk_ref
        nextBaseChar :: S (Maybe Char)
        nextBaseChar = do
          s@DeadKeyState{_availableDkChars=cs} <- get
          case cs of
            []    -> pure Nothing
            c:cs' -> put s{_availableDkChars=cs'} $> Just c


-- | Prepend the definitions of a dead key to the other definitions
processDkMapping
  :: MkDeadKeyHeader
  -> MkEntry
  -> MkPartialEntry
  -> DeadKeyBaseCharMapping
  -> MergedDeadKeyDefinitions
  -> DeadKey
  -> TB.Builder
  -> S TB.Builder
processDkMapping mkDeadKeyHeader mkEntry mkPartialEntry dkm dkd dk acc = case Map.lookup dk dkd of
  Nothing -> addUnsupportedDk dk $> acc -- No definition found, ignore dead key
  Just rawCombos -> case lookupDk dk of
    Nothing  -> addUnsupportedDk dk $> mempty -- Dead key not supported, ignore it
    Just dk_ref -> do
      mCombos <- mkDeadKeyMapping dk_ref rawCombos
      case mCombos of
        Nothing -> pure acc -- Something went wrong, ignore dead key
        Just combos -> pure $ mconcat ["\n", mkDeadKeyHeader dk dk_ref, combos, acc]
  where
    lookupDk :: DeadKey -> Maybe WindowsChar
    lookupDk dk' = Map.lookup dk' dkm

    -- Create the entries for a dead key and combinations of following symbols.
    -- There may be several entries for a combo if a chained dead key is encountered.
    -- [NOTE] We limit the recursion of chained dead keys by keeping trace
    --        of the root dead key and previous intermediate dead keys in "forbidden_dk".
    mkDeadKeyMapping :: WindowsChar -> Set DeadKeyCombo -> S (Maybe TB.Builder)
    mkDeadKeyMapping dk_char mapping = do
      combos <- mkDeadKeyMapping' mapping [dk]
      pure if null combos
        then Nothing
        else Just . mconcat . fmap (uncurry $ mkEntry dk_char) $ combos

    -- Almost identical as the previous function. Will be used for recursion to deal will chained dead keys.
    mkDeadKeyMapping' :: Set DeadKeyCombo -> Set DeadKey -> S [(TB.Builder, TB.Builder)]
    mkDeadKeyMapping' combos forbidden_dk = do
      (combos_str, found_default_combo) <- foldrM go (mempty, False) combos'
      -- Ensure there is a default combo
      pure $ if found_default_combo || null combos_str
        then combos_str
        else
          -- Use the base char if not a special character
          let bc = _dkBaseChar dk
              bc' = if bc `elem` safeDeadKeyChars then ' ' else bc
              e = mkPartialEntry lookupDk (SChar ' ') (RChar bc')
               <> mkPartialEntry lookupDk (SChar ' ') (RChar ' ') -- default if previous failed
          in case e of
             Left _ -> combos_str
             Right extra_combo_str -> combos_str <> [extra_combo_str]
      where
        go = mkDeadKeyPartialEntries forbidden_dk . unWrappedCombo
        combos' = Set.map DeadKeyCombo' combos

    -- Create entries for a root dead dead key and a combination of symbols
    mkDeadKeyPartialEntries :: Set DeadKey -> DeadKeyCombo -> ([(TB.Builder, TB.Builder)], Bool) -> S ([(TB.Builder, TB.Builder)], Bool)
    -- Combo result: a chained dead key
    mkDeadKeyPartialEntries forbidden_dk combo@(DeadKeyCombo (s NE.:| []) r@(RDeadKey dk')) acc'
      | Set.member dk' forbidden_dk = pure acc' -- DK already processed
      | otherwise = mkDeadKeyPartialEntries' combo s r acc'
    -- Combo: 1 symbol only
    mkDeadKeyPartialEntries _ combo@(DeadKeyCombo (s NE.:| []) r) acc' =
      mkDeadKeyPartialEntries' combo s r acc'
    -- Combo: 2 symbols or more
    mkDeadKeyPartialEntries _ combo@(DeadKeyCombo ss _) acc' =
      addInvalidCombo dk combo mempty{_dkcomboInvalidSymbols=Set.fromList . NE.toList $ ss} $> acc'

    mkDeadKeyPartialEntries' :: DeadKeyCombo -> Symbol -> Result -> ([(TB.Builder, TB.Builder)], Bool) -> S ([(TB.Builder, TB.Builder)], Bool)
    mkDeadKeyPartialEntries' combo s r acc' = case mkPartialEntry lookupDk s r of
      Left err -> addInvalidCombo dk combo err $> acc'
      Right pe -> pure $ if s == SChar ' '
        then bimap (pe:) (const True) acc'
        else first (pe:) acc'

paddingSymbol :: TB.Builder -> TB.Builder
paddingSymbol = padR 6 ' '

-- Error handling -------------------------------------------------------------

-- [FIXME] should report the invalid symbol(s)
-- | Invalid combo
addInvalidCombo :: DeadKey -> DeadKeyCombo -> DeadKeyComboError -> S ()
addInvalidCombo dk combo err = modify \s ->
  s{_errors=_errors s <> Just mempty{_dkCombosErrors=[(dk, [(combo, err)])]}}

-- | Last resource. This should not happen, 'resolveMergedDeadKeyDefinitions' should take care of this
addUnsupportedDk :: DeadKey -> S ()
addUnsupportedDk dk = modify \s@DeadKeyState{_errors=errs} ->
  s{_errors=errs <> Just mempty{_dkUnsupported=[dk]}}

-- | A wrapper with a special 'Ord' instance to have DK + space process last
newtype DeadKeyCombo' = DeadKeyCombo' {unWrappedCombo :: DeadKeyCombo} deriving (Eq)

instance Ord DeadKeyCombo' where
  (DeadKeyCombo' c1@(DeadKeyCombo s1 _)) `compare` (DeadKeyCombo' c2@(DeadKeyCombo s2 _)) =
    case (NE.head s1, NE.head s2) of
      (SChar ' ', SChar ' ') -> compare c1 c2
      (SChar ' ', _) -> GT
      (_, SChar ' ') -> LT
      (_, _) -> compare c1 c2

