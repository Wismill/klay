{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Common.Layout
  ( WdkLayout
  , WdkGroups
  , WdkGroup
  , WdkActionsMap
  , MsklcLayout
  , MsklcGroups
  , MsklcGroup
  , MsklcActionsMap
  , prepareLayout
  , fixLevels
  , fixKeyMap
  , mkPartialVirtualKeyDefinition
  , simplifyVirtualKeysDefinition
  ) where

import Data.Functor (($>), (<&>))
import Data.Foldable (traverse_, foldrM)
import Data.Bifunctor (Bifunctor(..))
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Map.Merge.Strict qualified as Map
import Control.Monad.State.Strict

import Klay.Export.Windows.Common.Modifiers (ModifierErrors(..), mkALevel)
import Klay.Export.Windows.Common.Symbols (SymbolErrors(..))
import Klay.Export.Windows.Lookup.VirtualKeys
  (nativeActionVk, nativeNonPunctuationActionVk, isReservedVk)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
  ( MultiOsRawLayout, ResolvedLayout, Layout(..)
  , WindowsOptions(..), VirtualKeysDefinition
  , Key, Level, resolveLayout)
import Klay.Keyboard.Layout.Action
  ( Actions(..), TAutoActions(..)
  , ResolvedActions, CustomActions(..)
  , ResolvedActionsMap, ActionsKeyMap
  , untagCustomisableActions, getLayers, toMonogram
  , defaultActions,  )
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier
  ( Modifier(..), ModifierBit(..), ModifierVariant(..), ModifiersFields
  , maskToModifiers, splitMask
  , isoLevel1, isoLevel2, isoLevel3, altGr)
import Klay.Keyboard.Layout.Modification
  ( LevelErrors(..), DiscardedLevel(..), DiscardedReason(..), Combine(..)
  , WindowsOptionsModification(..), ActionMapModification(..)
  , mapGroupLevelsResolved, lmodify)
import Klay.Keyboard.Layout.VirtualKey (VirtualKey)
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.OS (MultiOs(..), windows)
import Klay.Utils (foldlMWithKey')

type WdkLayout        = Layout WdkGroups
type MsklcLayout      = Layout MsklcGroups
type WdkGroups        = Groups WdkGroup
type MsklcGroups      = Groups MsklcGroup
type WdkGroup         = ResolvedGroup VirtualKey
type MsklcGroup       = ResolvedGroup (Key, VirtualKey)
type WdkActionsMap    = ResolvedActionsMap VirtualKey
type MsklcActionsMap  = ResolvedActionsMap (Key, VirtualKey)

type S = State (Maybe ModifierErrors, Maybe SymbolErrors)

-- | Ensure the layout is Windows-compatible
prepareLayout
  :: Set.Set ModifierBit
  -- ^ Supported modifiers bits
  -> ModifiersFields
  -- ^ Supported modifiers combos
  -> (Key -> Bool)
  -- ^ Predicate for valid key
  -> (VK.VirtualKey -> Bool)
  -- ^ Predicate for valid virtual key
  -> MultiOsRawLayout
  -- ^ Raw layout to process
  -> S MsklcLayout
prepareLayout supportedBits supported_combos is_valid_key is_valid_vk
  =   resolveLayout'
  >=> fixLevels supportedBits supported_combos
  >=> fixKeyMap is_valid_key is_valid_vk
  where
    resolveLayout' = pure . resolveLayout windows


-- | Modify the levels according to the layout options and the supported modifiers.
fixLevels :: Set.Set ModifierBit -> ModifiersFields -> ResolvedLayout -> S ResolvedLayout
fixLevels supportedBits supported_combos layout =
  modify (first (<> mErrors)) $> layout{_groups=gs}
  where
    (gs, mErrors) = runState
      (traverse
        (fixGroupLevels supportedBits supported_combos convertLevel convertModifier)
        (_groups layout))
      Nothing
    MultiOs{_windows=wopts} = _options layout
    convertLevel = _wIsoLevel3IsAltGr wopts
    convertModifier = _wAltgr wopts

-- | Convert or remove modifiers then process corresponding levels of a group.
fixGroupLevels
  :: Set.Set ModifierBit -- ^ Supported modifiers bits
  -> ModifiersFields     -- ^ Supported modifiers combos
  -> Bool                -- ^ Convert 'IsoLevel3' level
  -> Bool                -- ^ Convert 'IsoLevel3' modifiers
  -> KeyResolvedGroup    -- ^ Group to modify
  -> State (Maybe ModifierErrors) KeyResolvedGroup
fixGroupLevels supportedBits supported_modifiers convertLevel convertModifier g
  = traverse_ reportErrors merrors $> g'
  where
    (g', merrors) = mapGroupLevelsResolved processLevel processModifier KeepPrevious g

    processLevel :: Level -> Either DiscardedLevel Level
    processLevel l
      | Set.member l' supported_modifiers = Right l'
      | otherwise                         = Left (mkInvalid l)
      where
        mkInvalid = InvalidModifiers . maskToModifiers
        -- Check IsoLevel3
        l' | convertLevel = either id (<> altGr) (splitMask isoLevel3 l) -- replace by altGr
           | otherwise     = l -- keep unchanged

    processModifier :: Modifier -> Maybe Modifier
    processModifier m@Modifier{_mBit=bs}
      -- Note: Set to [Alternate] rather than [Control, Alternate] because we use Windows special flag
      | bs == [IsoLevel3] && convertLevel && convertModifier = Just m{_mBit=[Alternate], _mVariant=R}
      | otherwise = Just m

    reportErrors :: LevelErrors -> State (Maybe ModifierErrors) ()
    reportErrors errors = modify (<> Just mempty
      { _mUnsupportedLevels=unsupportedLevels
      , _mUnsupportedModifierBits=unsupportedBits
      , _mUnsupportedModifiersCombos=unsupported_combos
      })
      where unsupportedBits = foldr (Set.union . Set.filter (`Set.notMember` supportedBits)) mempty unsupported_combos
            (unsupportedLevels, unsupported_combos) =
              flip runState mempty . traverse go . Map.mapKeys mkLevel . _discardedLevels $ errors
            go Filtered = pure Filtered
            go (ReplacedBy l) = pure $ ReplacedBy (mkLevel l)
            go (Invalid ms) = modify (Set.insert ms) $> Invalid ms
            mkLevel (l, n) = mkALevel g l n


-- | Transform to Windows VK, remap key to new VK
-- when original actions are only achievable with dedicated VK
-- and filter supported Keys and VK.
fixKeyMap
  :: (Key -> Bool)
  -- ^ Predicate for valid key
  -> (VK.VirtualKey -> Bool)
  -- ^ Predicate for valid virtual key
  -> ResolvedLayout
  -> S MsklcLayout
fixKeyMap is_valid_key is_valid_vk l =
  -- [FIXME] proper process that also update the VK definition
  -- [FIXME] use several groups
  mkPartialVirtualKeyDefinition vkd0 (go1 $ _groups l) >>= filter_keys
  where
    vkd0 = _wVirtualKeyDefinition . _windows . _options $ l

    go1 = Map.map untagCustomisableActions . firstGroupActionsMap

    filter_keys :: VirtualKeysDefinition -> S MsklcLayout
    filter_keys vkd = traverse (go2 vkd) (_groups l) <&> \gs ->
      lmodify (VirtualKeysDefinition (KeyMapUpdate vkd)) l{_groups=gs}

    go2 :: VirtualKeysDefinition -> KeyResolvedGroup -> S MsklcGroup
    go2 vkd g = foldlMWithKey' (go3 vkd) mempty (_groupMapping g) <&> \am ->
      g{_groupMapping=Map.fromList am}

    go3
      :: VirtualKeysDefinition
      -> [((Key, VirtualKey), ResolvedActions)]
      -> Key
      -> ResolvedActions
      -> S [((Key, VirtualKey), ResolvedActions)]
    -- Default actions: skip if failure
    go3 vkd acc key as@(ADefault _) = pure if is_valid_key key
      then case Map.lookup key vkd of
        Nothing -> acc
        Just vk -> if is_valid_vk vk
          then ((key, vk), as) : acc
          else acc
      else acc
    -- Custom actions: error if failure
    go3 vkd acc key as@(ACustom _) = if is_valid_key key
      then case Map.lookup key vkd of
        Nothing -> report_discarded_key key as Filtered $> acc
        Just vk -> if is_valid_vk vk
          then pure $ ((key, vk), as) : acc
          else report_invalid_vk vk *> report_discarded_key key as Filtered $> acc
      else report_invalid_key key $> acc

    report_invalid_key key = modify (second (<> Just mempty{_sUnsupportedKeys=[key]}))
    report_invalid_vk  vk  = modify (second (<> Just mempty{_sUnsupportedVks=[vk]}))
    report_discarded_key key as r = case untagCustomisableActions as of
      UndefinedActions -> pure ()
      NoActions        -> pure ()
      as'              ->
        modify (second (<> Just mempty{_sDiscardedActions=[(key, (as', r))]}))

-- | Create a virtual keys definitions from an initial definition
--   and deduce the missing definitions from an action map
mkPartialVirtualKeyDefinition
  :: VirtualKeysDefinition -- ^ Initial definition to complete
  -> ActionsKeyMap         -- ^ Actions map used to deduce definitions
  -> S VirtualKeysDefinition
mkPartialVirtualKeyDefinition vkd0 am =
  let -- All existing keys
      all_keys = Set.fromList (enumFromTo minBound maxBound)
      -- All existing VKs
      all_vks  = Set.fromList (enumFromTo minBound maxBound)
      -- Available remapable keys
      available_vks0 = Set.filter (not . isReservedVk) $ all_vks Set.\\ Set.fromList (Map.elems vkd0)
      -- Initialisation
      stage0_result = (vkd0, available_vks0)
      -- Stage 1: Deduce VKs from actions only from first two levels, excluding punctuation
      --          This first step avoids to match punctuation on numeric row
      stage1_result = Map.foldl' (Map.foldlWithKey' stage1) stage0_result layers12
      -- Stage 2: Deduce remaining VKs from actions from remaing layers
      stage2_result = Map.foldl' (Map.foldlWithKey' stage2) stage1_result layers
      -- Stage 3: Complete the map with default VKs
      missing_keys = all_keys Set.\\ Map.keysSet (fst stage2_result)
      stage3_result = foldrM stage3 stage2_result missing_keys
  in fst <$> stage3_result
  where
    layers
      = getLayers
      . Map.withoutKeys am
      $ Map.keysSet vkd0
    layers12 = Map.restrictKeys layers [isoLevel1, isoLevel2]

    -- Deduce candidates from actions
    stage12 check_action acc@(vkd, available_vks) key a = case Map.lookup key vkd of
      Just _  -> acc
      Nothing -> case check_action key a of
        -- No VK deduced: wait for next stage
        Nothing -> acc
        -- VK deduced
        Just vk -> if
          -- Deduced reserved vk: just add it to the mapping
          | isReservedVk vk -> (Map.insert key vk vkd, available_vks)
          -- Deduced available vk: add it to the mapping and remove from available keys
          | Set.member vk available_vks ->
            (Map.insert key vk vkd, Set.delete vk available_vks)
          -- Deduced non availble vk: change nothing and wait for next stage
          | otherwise -> acc

    -- Stage 1
    stage1 = stage12 \key a ->
      let is_sep = let a' = toMonogram a in a' == Just '.' || a' == Just ','
      in if
      -- Special case: decimal separators
      | key == K.KPDecimal && is_sep -> Just VK.VK_DECIMAL
      | key == K.KPComma   && is_sep -> Just VK.VK_ABNT_C2
      | key == K.KPJPComma && is_sep -> Just VK.VK_SEPARATOR
      -- Other cases
      | otherwise -> nativeNonPunctuationActionVk a

    -- Stage 2
    stage2 = stage12 (const nativeActionVk)

    -- Stage 3
    stage3 key acc@(vkd, available) = case VK.keyNativeVirtualKey4 key of -- Default VK
      Just vk -> if
        -- Reserved vk: just add it to the mapping
        | isReservedVk vk       -> pure (Map.insert key vk vkd, available)
        -- Deduced available vk: add it to the mapping and remove from available keys
        | Set.member vk available -> pure (Map.insert key vk vkd, Set.delete vk available)
        -- Deduced non available vk: try with a random remappable vk
        | otherwise               -> next_available_vk
      Nothing -> case Map.lookup key am of
        Nothing               -> pure acc -- Ignore: no actions mapped
        Just UndefinedActions -> pure acc -- Ignore: no actions mapped
        Just as -> if
          | K.isAlphanumeric key -> next_available_vk
          | as == _aActions (defaultActions key windows) -> pure acc -- Ignore: system key
          | otherwise -> addUnsupportedKey $> acc
      where
        addUnsupportedKey = modify (second (<> Just mempty{_sUnsupportedKeys=[key]}))
        next_available_vk = case Set.maxView available of
          Just (vk', available') -> pure (Map.insert key vk' vkd, available')
          Nothing                -> addUnsupportedKey $> acc


-- | Simplify a 'VirtualKeysDefinition' by removing its trivial mappings.
simplifyVirtualKeysDefinition
  :: ActionsKeyMap                -- ^ Actions map of the first group
  -> VirtualKeysDefinition -- ^ Definitions to simplify
  -> VirtualKeysDefinition
simplifyVirtualKeysDefinition am0 vkd0 =
  Map.merge Map.preserveMissing Map.dropMissing (Map.zipWithMaybeMatched f1) vkd0 vkd1
  where
    am = Map.restrictKeys am0 (Map.keysSet vkd0)
    vkd1 = evalState (mkPartialVirtualKeyDefinition mempty am) mempty
    f1 _ vk1 vk2
      | vk1 == vk2 = Nothing  -- Trivial mapping
      | otherwise  = Just vk1 -- Non-trivial
