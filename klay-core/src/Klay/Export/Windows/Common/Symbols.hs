{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Common.Symbols
  ( -- * Types
    SymbolState(..)
  , DeadKeyBaseCharMapping
  , CharSequenceMapping
  , LigatureRef
  , WAction(..)
  , WdkId(..)
  , WdkIdTL
  , addReachableChar
  , addReachableDk
    -- * Content generation
  , actionsByWLevel
  , actionsByMaybeWLevel
  , addCharSequence
    -- * Error handling
  , SymbolErrors(..)
  , addUnsupportedKey
  , addUnsupportedVk
  , addNonRemappableVk
  , addUnmappedVk
  , addUnsupportedAction
    -- * Utils
  , escapeIdentifier
  , escapeGroupId
  ) where


import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Control.Monad.State.Strict

import Klay.Export.Windows.Common.Modifiers (WLevel, WdkId(..), WdkIdTL)
import Klay.Export.Windows.Lookup.Characters (WindowsChar)
import Klay.Keyboard.Layout hiding (Layout(..))
import Klay.Keyboard.Layout.Action (SpecialAction, Actions, actionsByLevel, actionsByMaybeLevel)
import Klay.Keyboard.Layout.Modification (DiscardedReason)
import Klay.Keyboard.Layout.VirtualKey qualified as VK
import Klay.Utils.UserInput
import Data.Bifunctor

type S = State SymbolState

-- | An encoding table of the dead keys
type DeadKeyBaseCharMapping = Map DeadKey WindowsChar
-- | A catalogue of the ligatures
type CharSequenceMapping = Map LigatureRef RawLText
-- | A key for a ligature entry: represents ('VirtualKey', level index)
type LigatureRef = (TL.Text, WLevel)

-- | An internal state for processing symbols
data SymbolState = SymbolState
  { _chars :: Set Char
  -- ^ Reachable characters
  , _deadKeys :: Set DeadKey
  -- ^ Reachable dead keys
  , _charSequenceMapping :: CharSequenceMapping
  -- ^ Ligatures found
  , _errors :: Maybe SymbolErrors
  -- ^ Errors encountered while processing
  } deriving (Eq)

instance Semigroup SymbolState where
  (SymbolState cs1 dks1 ls1 e1) <> (SymbolState cs2 dks2 ls2 e2) =
    SymbolState (cs1 <> cs2) (dks1 <> dks2) (ls1 <> ls2) (e1 <> e2)

instance Monoid SymbolState where
  mempty = SymbolState mempty mempty mempty mempty

addReachableChar :: Char -> S ()
addReachableChar c = modify \s -> s{_chars=Set.insert c $ _chars s}

addReachableDk :: DeadKey -> S ()
addReachableDk dk = modify \s -> s{_deadKeys=Set.insert dk $ _deadKeys s}

data SymbolErrors = SymbolErrors
  { _sUnsupportedKeys :: Set Key
  -- ^ Keys not supported in WDK
  , _sUnsupportedVks :: Set VK.VirtualKey
  -- ^ Virtual keys not supported in WDK
  , _sNonRemappableVks :: Set VK.VirtualKey
  -- ^ Virtual keys not remappable
  , _sDiscardedActions :: Map Key (Actions, DiscardedReason Actions Key)
  -- ^ Discarded virtual key because its corresponding key is invalid
  , _sUnsupportedActions :: Set (Either Modifier SpecialAction)
  -- ^ Special action not supported in WDK
  , _sUnsupportedLigatures :: Set RawLText
  -- ^ Ligatures not supported in WDK
  , _sUnsupportedDks :: Set DeadKey
  -- ^ Dead keys not supported in WDK
  , _sUnMappedVks :: Set (WdkIdTL, VK.VirtualKey)
  -- ^ Virtual keys mapped to a key but with no action defined
  } deriving (Show, Eq)

instance Semigroup SymbolErrors where
  (SymbolErrors a1 b1 c1 d1 e1 f1 g1 h1) <> (SymbolErrors a2 b2 c2 d2 e2 f2 g2 h2) =
    SymbolErrors (a1 <> a2) (b1 <> b2) (c1 <> c2) (d1 <> d2) (e1 <> e2) (f1 <> f2) (g1 <> g2) (h1 <> h2)

instance Monoid SymbolErrors where
  mempty = SymbolErrors mempty mempty mempty mempty mempty mempty mempty mempty

actionsByWLevel :: [(WLevel, Level)] -> Actions -> (ModifiersOptions, [(WLevel, Action)])
actionsByWLevel wlevels as = go <$> actionsByLevel levels as
  where levels = snd <$> wlevels
        go = fmap (bimap fst snd) . zip wlevels

actionsByMaybeWLevel :: [Maybe (WLevel, Level)] -> Actions -> (ModifiersOptions, [(Maybe WLevel, Action)])
actionsByMaybeWLevel mwlevels as = go <$> actionsByMaybeLevel mlevels as
  where mlevels = fmap snd <$> mwlevels
        go = fmap (bimap (fst <$>) snd) . zip mwlevels

addUnsupportedKey :: Key -> S ()
addUnsupportedKey k = modify (mempty{_errors=Just mempty{_sUnsupportedKeys=[k]}} <>)

addUnsupportedVk :: VK.VirtualKey -> S ()
addUnsupportedVk vk = modify (mempty{_errors=Just mempty{_sUnsupportedVks=[vk]}} <>)

addNonRemappableVk :: VK.VirtualKey -> S ()
addNonRemappableVk vk = modify (mempty{_errors=Just mempty{_sNonRemappableVks=[vk]}} <>)

addUnmappedVk :: WdkIdTL -> VK.VirtualKey -> S ()
addUnmappedVk gname vk = modify (mempty{_errors=Just mempty{_sUnMappedVks=[(gname, vk)]}} <>)

addUnsupportedAction :: Either Modifier SpecialAction -> S ()
addUnsupportedAction a = modify (mempty{_errors=Just mempty{_sUnsupportedActions=[a]}} <>)

addCharSequence :: TL.Text -> WLevel -> RawLText -> S LigatureRef
addCharSequence vk iLevel ligature = do
  let liga_ref = (vk, iLevel)
  modify \s -> s{_charSequenceMapping=Map.insert liga_ref ligature $ _charSequenceMapping s}
  pure liga_ref

-- | A supported Windows action
data WAction
  = WNoAction
  | WLigature !LigatureRef !RawLText
  | WChar !TB.Builder
  | WDeadKey !DeadKey
  deriving (Eq, Show)

-- | Escape a name of a layout component: name, variant, group, type, etc.
escapeIdentifier :: RawLText -> WdkId TL.Text
escapeIdentifier = WdkId . escapeSafeFilename

escapeGroupId :: Group a b -> WdkId TL.Text
escapeGroupId = fmap TL.toLower . escapeIdentifier . _groupName
