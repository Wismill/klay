{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Common.KeyNames
  ( KeyNames
  , defaultKeyNames
  , mkKeyNames
  , keyFromKeyNameRef
  ) where

import Data.Maybe (mapMaybe)
import Data.List (sort, partition)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Map.Strict qualified as Map
import Data.Bifunctor (Bifunctor(..))
import Data.Bitraversable (Bitraversable(..))

import Klay.Import.Windows.Lookup.Keycodes (parseKey)
import Klay.Export.Windows.Lookup.Keycodes (toWinKeycode)
import Klay.Keyboard.Hardware.Key.Types
import Klay.Utils.UserInput (RawLText, escapeLine)

-- | Default English names
defaultKeyNames :: KeyNames
defaultKeyNames =
  [ (PrintScreen, "Prnt Scrn")
  , (ScrollLock, "Scroll Lock")
  , (Pause, "Pause")
  , (F1, "F1")
  , (F2, "F2")
  , (F3, "F3")
  , (F4, "F4")
  , (F5, "F5")
  , (F6, "F6")
  , (F7, "F7")
  , (F8, "F8")
  , (F9, "F9")
  , (F10, "F10")
  , (F11, "F11")
  , (F12, "F12")
  , (F13, "F13")
  , (F14, "F14")
  , (F15, "F15")
  , (F16, "F16")
  , (F17, "F17")
  , (F18, "F18")
  , (F19, "F19")
  , (F20, "F20")
  , (F21, "F21")
  , (F22, "F22")
  , (F23, "F23")
  , (F24, "F24")
  , (Home, "Home")
  , (End, "End")
  , (PageUp, "Page Up")
  , (PageDown, "Page Down")
  , (Escape, "Esc")
  , (CursorLeft, "Left")
  , (CursorRight, "Right")
  , (CursorUp, "Up")
  , (CursorDown, "Down")
  -- , (ScreenLock, "ScreenLock")
  , (KP0, "Num 0")
  , (KP1, "Num 1")
  , (KP2, "Num 2")
  , (KP3, "Num 3")
  , (KP4, "Num 4")
  , (KP5, "Num 5")
  , (KP6, "Num 6")
  , (KP7, "Num 7")
  , (KP8, "Num 8")
  , (KP9, "Num 9")
  , (KPDivide, "Num /")
  , (KPMultiply, "Num *")
  , (KPSubtract, "Num -")
  , (KPAdd, "Num +")
  , (KPEnter, "Num Enter")
  , (Tabulator, "Tab")
  , (Return, "Enter")
  , (Backspace, "Backspace")
  , (Delete, "Delete")
  , (Insert, "Insert")
  , (Menu, "Application")
  , (LShift, "Left Shift")
  , (RShift, "Right Shift")
  , (CapsLock, "Caps Lock")
  , (LControl, "Left Ctrl")
  , (RControl, "Right Ctrl")
  , (LAlternate, "Left Alt")
  , (RAlternate, "Right Alt")
  , (LSuper, "Left Windows")
  , (RSuper, "Right Windows")
  , (NumLock, "Num Lock")
  , (Space, "Space")
  ]

-- | Extra names for Windows. They are not mapped to our keys
extraKeyNames :: [(TL.Text, RawLText)]
extraKeyNames =
  [ ("T53", "Num Del")
  , ("T54", "Sys Req")
  , ("X54", "<00>")
  , ("X46", "Break")
  , ("X56", "Help")
  ]

-- | Create
mkKeyNames :: KeyNames -> ([(TL.Text, TL.Text)], [(TL.Text, TL.Text)])
mkKeyNames
  = bimap mkNames mkNames
  . partition (TL.isPrefixOf "T" . fst)
  . sort
  . (<> extraKeyNames)
  . mapMaybe (bitraverse toWinKeycode' pure)
  . Map.toList
  where
    -- Note: for some reason, "Pause" and "NumLock" have different codes here
    toWinKeycode' Pause = Just "T45"
    toWinKeycode' NumLock = Just "X45"
    toWinKeycode' key = toWinKeycode key
    mkNames = fmap (bimap mkName escapeLine)
    mkName = TL.toLower . TL.drop 1

-- | Fix for old MSKLC mapping
keyFromKeyNameRef :: T.Text -> Maybe (Either T.Text Key)
keyFromKeyNameRef "T45" = Just (Right Pause)
keyFromKeyNameRef "X45" = Just (Right NumLock)
keyFromKeyNameRef "T53" = Just (Left "T53")
keyFromKeyNameRef "T54" = Just (Left "T54")
keyFromKeyNameRef "X54" = Just (Left "X54")
keyFromKeyNameRef "T7C" = Just (Right F13)
keyFromKeyNameRef "T7D" = Just (Right F14)
keyFromKeyNameRef "T7E" = Just (Right F15)
keyFromKeyNameRef "T7F" = Just (Right F16)
keyFromKeyNameRef "T80" = Just (Right F17)
keyFromKeyNameRef "T81" = Just (Right F18)
keyFromKeyNameRef "T82" = Just (Right F19)
keyFromKeyNameRef "T83" = Just (Right F20)
keyFromKeyNameRef "T84" = Just (Right F21)
keyFromKeyNameRef "T85" = Just (Right F22)
keyFromKeyNameRef "T86" = Just (Right F23)
keyFromKeyNameRef "T87" = Just (Right F24)
keyFromKeyNameRef "X46" = Just (Left "X46")
keyFromKeyNameRef "X56" = Just (Left "X56")
keyFromKeyNameRef key_ref = Right <$> parseKey key_ref
