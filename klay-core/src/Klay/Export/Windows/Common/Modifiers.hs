{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Windows.Common.Modifiers
  ( -- * Level definitions
    WLevel(..)
  , MkSymbolIndexEntry
  , groupLayoutWLevels
  , mkLevelsDefinition
  , mkLevelIndex
  , mkLevelIndex'
  , fixLevelIndex
  , levelIndexThreshold
  , levelsIndexEntriesThreshold
  , allLevelsByModifiers
  , levelsByValidModifiersCombo
  , allGroupModifiers
  , mkModifierBit
  , checkModifierVk
  , getModifier
  , hasCapsLock
  , hasAltGrCapsLock
    -- * Error handling
  , ModifierErrors(..)
  , ALevel(..)
  , mkALevel
  , addUnsupportedModifierBit
  , addUnsupportedModifiersCombo
  , addUnsupportedModifier
  , addUnsupportedVk
  , addNonRemappableVk
  -- , add_unsupported_level
    -- * Utils
  , WdkId(..)
  , WdkIdTL
  ) where

import Data.Bits ((.|.))
import Data.List (intersperse, dropWhileEnd)
import Data.Maybe (fromMaybe, isJust, isNothing, mapMaybe)
import Data.String (IsString(..))
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Bifunctor(Bifunctor(..))
import Control.Arrow ((&&&), (***))
import Control.Monad.State.Strict

import TextShow (showb)
import Klay.Export.Windows.Lookup.VirtualKeys (isRemappableVk)
import Klay.Export.Windows.Lookup.Modifiers (toModifierBit, nativeModifierVk)
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.Actions (Actions(..))
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.Modification (DiscardedReason)
import Klay.Keyboard.Layout.VirtualKey (VirtualKey(..))
import Klay.Utils.UserInput (RawLText, escapeControlNewlines)

newtype WLevel = WLevel {getWLevel :: Int}
  deriving newtype (Eq, Ord, Show)

type WdkIdTL = WdkId TL.Text

-- | An identifier used in WDK, such as: group name, type name, variant, etc.
newtype WdkId t = WdkId { getWdkId :: t }
  deriving stock (Show, Functor)
  deriving newtype (Eq, Ord, Semigroup, Monoid, IsString)

-- | A container for the errors related to modifiers and levels
data ModifierErrors = ModifierErrors
  { _mUnsupportedModifierBits :: Set.Set ModifierBit
    -- ^ Unsupported modifiers bits
  , _mUnsupportedModifiersCombos :: Set.Set M.ModifiersCombo
    -- ^ Unsupported modifiers combos
  , _mUnsupportedModifiers :: Set.Set Modifier
    -- ^ Unsupported modifiers
  , _mUnsupportedVK :: Set.Set VirtualKey
    -- ^ Unsupported virtual keys
  , _mNonRemappableVK :: Set.Set VirtualKey
    -- ^ Non-remappable virtual keys
  , _mUnsupportedLevels :: Map.Map ALevel (DiscardedReason ALevel M.ModifiersCombo)
    -- ^ Levels discarded because of unsupported modifiers
  , _mTooMuchLevels :: Bool
    -- ^ Indicate if the table @CharModifiers@ is truncated with discared level indexes
  } deriving (Eq, Show)

instance Semigroup ModifierErrors where
  (ModifierErrors b1 c1 m1 vk1 nvk1 ls1 l1) <> (ModifierErrors b2 c2 m2 vk2 nvk2 ls2 l2) =
    ModifierErrors (b1 <> b2) (c1 <> c2) (m1 <> m2) (vk1 <> vk2) (nvk1 <> nvk2) (ls1 <> ls2) (l1 || l2)

instance Monoid ModifierErrors where
  mempty = ModifierErrors mempty mempty mempty mempty mempty mempty False

-- | A representation of a level
data ALevel = ALevel
  { _lGroup :: RawLText  -- ^ Group name
  , _lModifiers :: Level -- ^ Level modifiers
  , _lName :: RawLText   -- ^ Level name
  } deriving (Eq, Ord)

instance Show ALevel where
  show (ALevel g l n) = TL.unpack . escapeControlNewlines $ mconcat
    ["Group: ", g, ", Level: ", n, " (", fromString . M.modifiersFieldLabel $ l, ")"]

type S = State (Maybe ModifierErrors)

addUnsupportedModifierBit :: ModifierBit -> S ()
addUnsupportedModifierBit m = modify \s -> s <> Just mempty{_mUnsupportedModifierBits=[m]}

addUnsupportedModifiersCombo :: M.ModifiersCombo -> S ()
addUnsupportedModifiersCombo m = modify \s -> s <> Just mempty{_mUnsupportedModifiersCombos=[m]}

addUnsupportedModifier :: Modifier -> S ()
addUnsupportedModifier m = modify \s -> s <> Just mempty{_mUnsupportedModifiers=[m]}

addUnsupportedVk :: VirtualKey -> S ()
addUnsupportedVk vk = modify \s -> s <> Just mempty{_mUnsupportedVK=[vk]}

addNonRemappableVk :: VirtualKey -> S ()
addNonRemappableVk vk = modify \s -> s <> Just mempty{_mNonRemappableVK=[vk]}

mkALevel :: SingleOsGroup a -> Level -> RawLText -> ALevel
mkALevel g = ALevel (_groupName g)

-- type ModifierBitMapping = Map.Map (Either TL.Text TL.Text) (Set.Set Modifier)
type MkHeader = Int -> TB.Builder
type MkSymbolIndexEntry = Maybe (WLevel, RawLText) -> M.ModifiersCombo -> TB.Builder -> TB.Builder

{-| Create the content of the table @CharModifiers@.

This table converts values from the modifier bits into column indexes for the symbol tables.

Somes issues:

* @SHFT_INVALID@ indicates that no index is associated to a modifier bits combination.
  It has the value @15@ so this cannot be used as a column index.
  We insert a dummy column at this index to fix this.
* There is an issue if this table has more that 256 entries, so it is truncated if required.
-}
mkLevelsDefinition
  :: MkHeader
  -> MkSymbolIndexEntry
  -> TB.Builder
  -> ResolvedGroup k
  -> (Maybe ModifierErrors, Int, TL.Text)
mkLevelsDefinition mkHeader mkSymbolIndexEntry separator g = (errors, levelDefinitionsCount, levelDefinitions)
  where
    levelDefinitionsCount = length combos
    levelDefinitions = TB.toLazyText . (headerComment <>) . mconcat . intersperse separator $ combos
    headerComment =
      let modifiersLength = sum . fmap ((+1) . length . show) $ modifiers
      in mkHeader modifiersLength

    -- Process combos that do not exceed the threshold
    (combos, errors) = fmap mkLevel *** check $ splitAt levelsIndexEntriesThreshold rawCombos
    rawCombos = first (fmap \(wl, _, n) -> (wl, n)) <$> allLevelsByModifiers g
    modifiers = reverse . Set.toList . allGroupModifiers $ g
    -- Check if there is any relevant level that is discarded
    check cs
      | any (isJust . fst) cs = Just mempty{_mTooMuchLevels=True}
      | otherwise             = Nothing

    mkLevel (mlevel, ms) = mkSymbolIndexEntry mlevel (mkModifiersCombo ms) (mkComboComment ms)
    mkComboComment = mconcat . intersperse " " . fmap showb' . reverse
    showb' (Left m)  = let m' = show m in TB.fromString (replicate (length m') ' ')
    showb' (Right m) = showb m

groupLayoutWLevels :: ResolvedGroup k -> [(WLevel, Level)]
groupLayoutWLevels = zip (fixLevelIndex <$> [(0::Int)..]) . groupLevels

-- | Get all levels and modifier states ordered by the active modifiers
allLevelsByModifiers :: ResolvedGroup k -> [(Maybe (WLevel, Level, RawLText), [Either ModifierBit ModifierBit])]
allLevelsByModifiers g =
  dropWhileEnd (isNothing . fst) . fmap (getLevel &&& id) . mapM (\m -> [Left m, Right m]) $ modifiers
  where
    modifiers = reverse . Set.toList . allGroupModifiers $ g
    getLevel ms =
      let l = M.modifiersToMask . mkModifiersCombo $ ms
      in (,l,) <$> mkLevelIndex levels l <*> Map.lookup l levels
    levels = _groupLevels g

-- | Get all levels ordered by their modifiers combo
levelsByValidModifiersCombo :: (Either M.ModifiersCombo M.ModifiersCombo -> Bool) -> ResolvedGroup vk -> [Maybe (WLevel, Level, RawLText)]
levelsByValidModifiersCombo p = mapMaybe f . allLevelsByModifiers
  where f (e, ms) = if p (mkModifiersCombo' e ms) then Just e else Nothing
        mkModifiersCombo' Nothing  = Left . mkModifiersCombo
        mkModifiersCombo' (Just _) = Right . mkModifiersCombo

mkModifiersCombo :: [Either ModifierBit ModifierBit] -> M.ModifiersCombo
mkModifiersCombo = Set.fromList . mapMaybe f
  where f (Left _)  = Nothing
        f (Right m) = Just m

-- | Ensure to use all necessary modifiers
allGroupModifiers :: ResolvedGroup vk -> Set.Set ModifierBit
allGroupModifiers g =
  let ms = groupModifiers g
      extra | Set.member IsoLevel5 ms = [Numeric, IsoLevel3]
            | Set.member IsoLevel3 ms = [Numeric]
            | otherwise               = mempty
  in mconcat [ ms, [Shift, Control, Alternate], extra ]

-- | Convert a level into its Windows index.
mkLevelIndex :: GroupLevels -> Level -> Maybe WLevel
mkLevelIndex ls = fmap fixLevelIndex . flip Map.lookupIndex ls

-- | Same as 'mkLevelIndex', but ouputs 'TB.Builder'
mkLevelIndex' :: GroupLevels -> Level -> Maybe TB.Builder
mkLevelIndex' ls = fmap (showb . getWLevel) . mkLevelIndex ls

-- | Fix a raw level index by avoiding the value 'levelIndexThreshold'.
fixLevelIndex :: Integral n => n -> WLevel
fixLevelIndex l
  | l < levelIndexThreshold = WLevel $ fromIntegral l
  | otherwise               = WLevel $ fromIntegral l + 1

-- | The value of @SHFT_INVALID@.
levelIndexThreshold :: Integral n => n
levelIndexThreshold = 0x0F -- 15

-- | For some reason, there is an issue if the count of level index entries is superior to 256.
levelsIndexEntriesThreshold :: Int
levelsIndexEntriesThreshold = 255

-- | Make /Windows/ modifier bit
mkModifierBit :: M.ModifiersCombo -> Int
mkModifierBit = foldr ((.|.) . fromMaybe 0 . toModifierBit) 0

checkModifierVk :: VirtualKey -> Actions -> Bool
checkModifierVk vk = \case
  UndefinedActions           -> True
  NoActions                  -> True
  SingleAction (AModifier m) -> maybe False (\vk' -> vk==vk' || isRemappableVk vk') $ nativeModifierVk m
  SingleAction _             -> False
  Actions{}                  -> False

getModifier :: VirtualKey -> Actions -> Maybe (Either Modifier Modifier)
getModifier vk = \case
  UndefinedActions   -> Nothing
  NoActions          -> Nothing
  SingleAction (AModifier m) -> case nativeModifierVk m of
    Nothing -> Nothing
    Just vk' -> if vk == vk' || isRemappableVk vk'
      then Just (Right m) else Just (Left m)
  SingleAction _ -> Nothing
  Actions{}      -> Nothing

hasCapsLock :: Level -> M.ModifiersOption -> Bool
hasCapsLock l opt = l == M.isoLevel2Caps && M._mTarget opt == M.isoLevel2

hasAltGrCapsLock :: Level -> M.ModifiersOption -> Bool
hasAltGrCapsLock l opt = l == M.altGrCaps && M._mTarget opt == M.altGrShift
