module Klay.Export.CSV
  ( CSVFormat(..)
  , SimpleCharacterEntry(..)
  , DetailedCharacterEntry(..)
  , generateCsv
  , generateGroupCsvContent
  ) where


import Data.Semigroup (sconcat)
import Data.Maybe (fromMaybe)
import Data.ByteString.Lazy qualified as BL
import Data.Text qualified as T
import Data.Foldable
import Data.List (intersperse, nub, sort)
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NE
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import GHC.Generics (Generic)
import System.FilePath ((</>))
import System.Directory (createDirectoryIfMissing)

import Data.Csv

import Klay.Keyboard.Hardware.Key (sequenceDifficulty)
import Klay.Keyboard.Layout (Layout(..), MultiOsRawLayout, finalGroupsLayouts)
import Klay.Keyboard.Layout.Action.Action (Action(..))
import Klay.Keyboard.Layout.Symbols (KeySequence, KeyModCombo(..), ReachableResults(..), groupReachableSymbols)
import Klay.Keyboard.Layout.Action.DeadKey (DeadKey, DeadKeyDefinitions, Symbol(..), Result(..), showDeadKeyName)
import Klay.Keyboard.Layout.Modifier (Modifier(..))
import Klay.Keyboard.Layout.Group
import Klay.OS (OsSpecific)
import Klay.Utils.Unicode (showUnicode, isShowableChar)
import Klay.Utils.Unicode.Blocks
import Klay.Utils.Unicode.Scripts
import Klay.Utils.UserInput (escapeFilename')


-- | CSV formats
data CSVFormat
  = CSVFormat
  deriving (Eq, Ord, Enum, Bounded, Show)

generateCsv :: CSVFormat -> FilePath -> OsSpecific -> MultiOsRawLayout -> IO ()
generateCsv _ csv_path os layout
  = do
    createDirectoryStructure csv_path
    (all_simple_entries, all_detailed_entries) <- foldlGroupsLayoutsWithIndexM generate_group_csv' (mempty, mempty) groups
    let simple_entries = nub . sort . mconcat $ all_simple_entries
    let detailed_entries = nub . sort . mconcat $ all_detailed_entries
    generate (mkPath "summary") simple_entries
    generate (mkPath "detailed") detailed_entries
  where
    groups = finalGroupsLayouts os layout

    dkd :: DeadKeyDefinitions
    dkd = _deadKeys layout

    generate_group_csv' :: ([[SimpleCharacterEntry]], [[DetailedCharacterEntry]]) -> GroupIndex -> KeyFinalGroup -> IO ([[SimpleCharacterEntry]], [[DetailedCharacterEntry]])
    generate_group_csv' (simple_entries, detailed_entries) k g = do
      let (gsimple_entries, gdetailed_entries) = generateGroupCsvContent dkd g
      generate (mkGroupPath k g "summary") gsimple_entries
      generate (mkGroupPath k g "detailed") gdetailed_entries
      pure (gsimple_entries:simple_entries, gdetailed_entries:detailed_entries)

    generate :: (DefaultOrdered a, ToNamedRecord a) => FilePath -> [a] -> IO ()
    generate path = BL.writeFile path . encodeDefaultOrderedByNameWith csvOptions

    mkGroupPath :: GroupIndex -> KeyFinalGroup -> String -> FilePath
    mkGroupPath k g path =
      let gname = escapeFilename' . _groupName $ g
      in csv_path </> mconcat [show . groupIndexToInt $ k, " – ", gname, " – ", path, ".csv"]

    mkPath :: String -> FilePath
    mkPath path = csv_path </> mconcat ["0 – All groups – ", path, ".csv"]

    csvOptions = defaultEncodeOptions{encQuoting=QuoteAll}

generateGroupCsvContent
  :: DeadKeyDefinitions
  -> KeyFinalGroup
  -> ([SimpleCharacterEntry], [DetailedCharacterEntry])
generateGroupCsvContent dkd g = (simple_entries, detailed_entries)
  where
    simple_entries = mkSimpleCharEntry <$> foldr' only_chars mempty (Map.keys bcs <> Map.keys dcs)
    detailed_entries = mconcat $bc_entries <> dkc_entries

    ReachableResults{_rByDirectInput=bcs, _rByDeadKey=dcs} = groupReachableSymbols dkd g

    only_chars (RChar c) acc = c : acc
    only_chars _         acc = acc

    bc_entries = fst <$> Map.elems bcs'
    dkc_entries = Map.foldrWithKey' (mkDKEntries bcs'') mempty dcs

    base_actions = groupBaseLayer g

    bcs' :: Map Result ([DetailedCharacterEntry], KeySequence)
    bcs' = Map.mapWithKey mkDirectEntries bcs
    -- Best sequences
    bcs'' :: Map Result KeySequence
    bcs'' = snd <$> bcs'

    mkSimpleCharEntry :: Char -> SimpleCharacterEntry
    mkSimpleCharEntry c =
      let code_point = showUnicode c
          c' = mkChar c
          block = unicodeBlock c
          script = unicodeScript c
      in SimpleCharacterEntry c' code_point block script

    mkDirectEntries :: Result -> Set KeySequence -> ([DetailedCharacterEntry], KeySequence)
    mkDirectEntries r ss =
      let (acc', _, best_key_seq) = foldl' mkDirectCharEntries' (mempty, 1e9, undefined) ss
      in (acc', best_key_seq)
      where
        (r', code_point, block, script, type') = case r of
          RChar c -> (prettyChar c, Just $ showUnicode c, Just $ unicodeBlock c, Just $ unicodeScript c, "Character")
          RText t -> (Just . T.unpack $ t, Nothing, Nothing, Nothing, "Text")
          RDeadKey dk -> (Just . showDeadKeyName $ dk, Nothing, Nothing, Nothing, "Dead Key")
        mkDirectCharEntries' (acc', best_score, best_key_seq) key_seq =
          let difficulty = sequenceDifficulty key_seq
              sequence_str = mkVirtualKeySequence key_seq
              entry = DetailedCharacterEntry type' r' code_point block script Nothing difficulty sequence_str
              best_score' = min difficulty best_score
              best_key_seq' = if best_score > difficulty then key_seq else best_key_seq
          in (entry : acc', best_score', best_key_seq')

    mkDKEntries :: Map Result KeySequence -> Result -> Map DeadKey (Set (NonEmpty Symbol)) -> [[DetailedCharacterEntry]] -> [[DetailedCharacterEntry]]
    mkDKEntries bs r dks acc = Map.foldrWithKey' mkEntries mempty dks : acc
      where
        (r', code_point, block, script, type') = case r of
          RChar c -> (prettyChar c, Just $ showUnicode c, Just $ unicodeBlock c, Just $ unicodeScript c, "Character")
          RText t -> (Just . T.unpack $ t, Nothing, Nothing, Nothing, "Text")
          RDeadKey dk -> (Just . showDeadKeyName $ dk, Nothing, Nothing, Nothing, "Chained Dead Key")
        mkEntries dk combos acc' = foldr' (mkEntry dk) acc' combos
        mkEntry dk combo acc' =
          let key_sequence = mkComboVirtualKeySequence (NE.cons (SDeadKey dk) combo)
              difficulty = sequenceDifficulty key_sequence
              key_sequence_str = mkVirtualKeySequence key_sequence
          in DetailedCharacterEntry type' r' code_point block script (Just . showDeadKeyName $ dk) difficulty key_sequence_str : acc'
        mkComboVirtualKeySequence combo = sconcat $ mkSymbol <$> combo
        mkSymbol (SChar c) = case Map.lookup (RChar c) bs of
          Nothing      -> error "mkDKCharEntries: char not found"
          Just key_seq -> key_seq
        mkSymbol (SDeadKey dk) = case Map.lookup (RDeadKey dk) bs of
          Nothing      -> error "mkDKCharEntries: dk not found"
          Just key_seq -> key_seq

    mkVirtualKeySequence :: KeySequence -> String
    mkVirtualKeySequence combos = mconcat $ foldr' mkCombo mempty combos
      where
        mkCombo :: KeyModCombo -> [String] -> [String]
        mkCombo KeyModCombo{_kModifiers=ms, _kKey=vk} acc
          | null ms = vk' : sep : acc
          | otherwise = ms' : " + " : vk' : sep : acc
          where
            sep | null acc = "" | otherwise = " ; "
            ms' :: String
            ms' = mconcat . intersperse " + " . fmap show . Set.toList $ ms
            vk' :: String
            vk' = case Map.lookup vk base_actions of
              Nothing -> error "[ERROR] mkVirtualKeySequence: no action mapped" -- [FIXME] Impossible case
              Just a -> case a of
                AChar c -> mconcat ["‹",mkCharOrCodePoint c, "›"]
                ADeadKey dk -> mconcat ["‹DK:", showDeadKeyName dk, "›"]
                ASpecial a' -> mconcat ["‹", show a', "›"]
                AModifier (Modifier m v e) -> mconcat ["‹", show v, show m, ":", show e, "›"] -- [TODO] improve?
                NoAction -> mconcat ["‹VK:", show vk, "›"] -- [TODO] Improve?
                UndefinedAction -> mconcat ["‹VK:", show vk, "›"] -- [TODO] Improve?
                a' -> error . mconcat $ ["[ERROR] mkVirtualKeySequence: unsupported action for vk ", show vk, ": ", show a']

    mkChar :: Char -> Maybe Char
    mkChar c
      | isShowableChar c = Just c
      | otherwise        = Nothing

    prettyChar :: Char -> Maybe String
    prettyChar c
      | isShowableChar c = Just [c]
      | otherwise        = Nothing

    mkCharOrCodePoint :: Char -> String
    mkCharOrCodePoint c = fromMaybe (showUnicode c) (prettyChar c)


data SimpleCharacterEntry = SimpleCharacterEntry
  { _character :: Maybe Char
  , _codePoint :: !String
  , _block :: !UnicodeBlock
  , _script :: !UnicodeScript
  } deriving (Eq, Ord, Generic, Show)

instance FromNamedRecord SimpleCharacterEntry
instance ToNamedRecord SimpleCharacterEntry
instance DefaultOrdered SimpleCharacterEntry

data DetailedCharacterEntry = DetailedCharacterEntry
  { _type :: String
  , _symbol :: Maybe String
  , _codePoint :: Maybe String
  , _block :: Maybe UnicodeBlock
  , _script :: Maybe UnicodeScript
  , _deadKey :: Maybe String
  , _keySequenceDifficulty :: Int
  , _keySequence :: String -- [TODO] use Text
  } deriving (Eq, Ord, Generic, Show)

instance ToNamedRecord DetailedCharacterEntry where
  toNamedRecord DetailedCharacterEntry
    { _type=t
    , _symbol=c
    , _codePoint=cp
    , _block=b
    , _script=s
    , _deadKey=dk
    , _keySequenceDifficulty=d
    , _keySequence=ks} = namedRecord
      [ "Type" .= t
      , "Character or Dead Key" .= c
      , "Code Point" .= cp
      , "Block" .= b
      , "Script" .= s
      , "Dead Key Used" .= dk
      , "Key Sequence Difficulty" .= d
      , "Key Sequence" .= ks
      ]

instance FromNamedRecord DetailedCharacterEntry where
    parseNamedRecord m = DetailedCharacterEntry
                     <$> m .: "Type"
                     <*> m .: "Character or Dead Key"
                     <*> m .: "Code Point"
                     <*> m .: "Block"
                     <*> m .: "Script"
                     <*> m .: "Dead Key Used"
                     <*> m .: "Key Sequence Difficulty"
                     <*> m .: "Key Sequence"

instance DefaultOrdered DetailedCharacterEntry where
  headerOrder _ = header [ "Type", "Character or Dead Key", "Code Point", "Block", "Script", "Dead Key Used", "Key Sequence Difficulty", "Key Sequence"]


-- | Create a directory structure required by CSV.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure = createDirectoryIfMissing True
