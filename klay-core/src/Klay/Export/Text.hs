module Klay.Export.Text
  ( TextFormat(..)
  , LayoutSize(..)
  , allTextFormats
  , generateText
  , generateConsole
  , generateCommonMarkFile
  , displayBigGroups
  , displayBigGroup
  -- , mkDisplayBigGroup
  -- , mkDisplayBigLayers
  , displayMiniGroups
  -- , mkDisplayMiniGroupLayer
  -- , mkDisplayMiniLayer
  ) where


import Data.Foldable (traverse_)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.IO qualified as TLIO

import TextShow (showtl)
import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal

import Klay.App.Common
import Klay.Export.Text.Common
import Klay.Export.Text.CommonMark (generateCommonMarkFile)
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Layout
import Klay.OS (OsSpecific)
import Klay.Utils.UserInput (escapeLine)

-- | Text format
data TextFormat
  = Console !LayoutSize -- ^ ASCII art
  | CommonMark -- ^ Documentation in Common Mark format
  deriving (Eq, Ord, Show, Read)

data LayoutSize
  = Mini
  | Big
  deriving (Eq, Ord, Enum, Bounded, Show, Read)

allTextFormats :: [TextFormat]
allTextFormats = [Console Mini, Console Big, CommonMark]

generateText :: FilePath -> TextFormat -> OsSpecific -> H.Geometry -> MultiOsRawLayout -> IO ()
generateText _    (Console s) = generateConsole s
generateText path CommonMark  = generateCommonMarkFile path

generateConsole :: LayoutSize -> OsSpecific -> H.Geometry -> MultiOsRawLayout -> IO ()
generateConsole s os geo layout@Layout{_metadata=m} = do
  putDoc $ mkTitle (mconcat ["*** Layout: ", escapeLine . _name $ m, " (", showtl os, ") ***"]) <> line
  case s of
    Mini -> maybe err (traverse_ display_mini) (displayMiniGroups os geo layout)
    Big  -> maybe err (traverse_ display_big ) (displayBigGroups  os geo layout)
  where
    err = putDoc . mkError . mconcat $ ["[ERROR] Unsupported geometry: ", showtl geo, "\n"]

    display_big :: (TL.Text, TL.Text) -> IO ()
    display_big (gn, ls) = do
      putDoc . mkField . mconcat $ ["# Group: ", gn]
      putDoc line
      TLIO.putStrLn ls

    display_mini :: (TL.Text, [(TL.Text, TL.Text)]) -> IO ()
    display_mini (gn, ls) = traverse_ (display_mini' gn) ls

    display_mini' :: TL.Text -> (TL.Text, TL.Text) -> IO ()
    display_mini' gn (ln, l) = do
      putDoc . mkField . mconcat $ ["# Group: ", gn, "; ", "Level: ", ln]
      putDoc line
      TLIO.putStrLn l
