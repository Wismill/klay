{-# LANGUAGE DuplicateRecordFields #-}

{-|
Description: Generator for XKB
-}

module Klay.Export.Linux.XKB
  ( XkbType
  , XkbContent(..)
  , SymbolError
  , XComposeError
  , DeadKeyBaseCharMapping
  , CharSequenceMapping
  , generateXkb
  , getXkbContent
  , createSymbols
  , createTypes
  , createCompose
  ) where

import Data.Maybe (fromMaybe)
import Control.Monad (forM_, unless)
import System.FilePath
import System.Directory (createDirectoryIfMissing)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.IO.Utf8 qualified as Utf8
import GHC.Generics (Generic(..))
import Control.DeepSeq (NFData(..))

import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.Terminal

import Klay.App.Common
import Klay.App.Check (printDeadKeysErrors)
import Klay.Keyboard.Layout
import Klay.Export.Linux.XKB.Compat
import Klay.Export.Linux.XKB.Header (createHeader)
import Klay.Export.Linux.XKB.Scripts
import Klay.Export.Linux.XKB.Symbols
import Klay.Export.Linux.XKB.Types
import Klay.Export.Linux.XKB.XCompose
import Klay.OS (linux)
import Klay.Utils.Unicode (safeDeadKeyChars)

{-
XKB vocabulary:

key codes
    A translation of the scan codes from the keyboard into a suitable symbolic form.

key symbols
    A translation of symbolic key codes into actual symbols, such as an A or an E.

compatibility map
    A specification of what actions various special-purpose keys produce.

type
    A specification of what various shift combinations produce.

-}

-- | Generator for XKB on Linux.
generateXkb :: FilePath -> MultiOsRawLayout -> IO ()
generateXkb path raw_layout = do
  createDirectoryStructure path
  header_xkb <- createHeader layout "//"
  xkbContent <- getXkbContent layout
  Utf8.writeFile compat_path (header_xkb <> xkbCompat xkbContent)
  Utf8.writeFile symbols_path (header_xkb <> xkbSymbols xkbContent)
  Utf8.writeFile types_path (header_xkb <> xkbTypes xkbContent)
  header_xcompose <- createHeader layout "#"
  Utf8.writeFile xcompose_path (header_xcompose <> xkbCompose xkbContent)
  generateScripts path layout -- [TODO] add header with version and date
  printError (xkbSymbolsErrors xkbContent) (xkbComposeErrors xkbContent)
  where
    layout = resolveLayout linux raw_layout
    system_name = mkLayoutId' layout
    compat_path = path </> "compat" </> system_name
    symbols_path = path </> "symbols" </> system_name
    types_path = path </> "types" </> system_name
    xcompose_path = path </> "compose" </> system_name

data XkbContent = XkbContent
  { xkbCompat :: TL.Text
  , xkbTypes :: TL.Text
  , xkbSymbols :: TL.Text
  , xkbSymbolsErrors :: Maybe SymbolError
  , xkbCompose :: TL.Text
  , xkbComposeErrors :: Maybe XComposeError
  } deriving (Generic, NFData, Show)

getXkbContent :: LinuxLayout -> IO XkbContent
getXkbContent layout = do
  compatContent <- getCompat layout
  pure XkbContent
    { xkbCompat = compatContent
    , xkbTypes = typesContent
    , xkbSymbols = symbolsContent
    , xkbSymbolsErrors = symbolsErrors
    , xkbCompose = xcomposeContent
    , xkbComposeErrors = xcomposeErrors
    }
  where
    (types, use_native, typesContent) = createTypes layout
    (symbolsContent, deadKeys, deadKeysDefinitions, charSequences, dead_key_chars, symbolsErrors) = createSymbols layout types use_native safeDeadKeyChars
    (xcomposeContent, xcomposeErrors) = createCompose deadKeys deadKeysDefinitions charSequences dead_key_chars

printError :: Maybe SymbolError -> Maybe XComposeError -> IO ()
printError Nothing Nothing = pure ()
printError symbolsErrors composeErrors = do
  let SymbolError{ _unsupportedKeys=ks
                 , _unsupportedDeadKeys=dks
                 , _unsupportedLigatures=ls
                 , _unsupportedActions=as
                 , _unsupportedModifiers=ms
                 } = fromMaybe mempty symbolsErrors
  let dkErrors = fromMaybe mempty composeErrors <> mempty{_dkUnsupported=dks}
  putDoc $ mkError "[WARNING] [XKB] There were errors while generating the XKB files:" <> line
  unless (null ks) do
    putDoc $ mkEmphasis "• The following keys are not supported by the XKB generator:" <> line
    forM_ ks (putStrLn . showItem)
  unless (null as) do
    putDoc $ mkEmphasis "• The following special actions are not supported by the XKB generator:" <> line
    forM_ as (putStrLn . showItem)
  unless (null ms) do
    putDoc $ mkEmphasis "• The following modifiers are not supported by the XKB generator:" <> line
    forM_ ms (putStrLn . showItem)
  unless (null ls) do
    putDoc $ mkEmphasis "• The following character sequences are not supported by the XKB generator:" <> line
    forM_ ls (putStrLn . showItem)
  printDeadKeysErrors dkErrors
  where
    showItem :: Show a => a -> String
    showItem = ("  ⁃ " <>) . show

-- | Create a directory structure required by XKB.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure path = do
  createDirectoryIfMissing True $ path </> "compose"
  createDirectoryIfMissing True $ path </> "compat"
  createDirectoryIfMissing True $ path </> "rules"
  createDirectoryIfMissing True $ path </> "symbols"
  createDirectoryIfMissing True $ path </> "scripts"
  createDirectoryIfMissing True $ path </> "types"
