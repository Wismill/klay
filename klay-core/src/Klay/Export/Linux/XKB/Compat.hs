module Klay.Export.Linux.XKB.Compat
  ( getCompat
  ) where

import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.IO.Utf8 qualified as Utf8

import Klay.Keyboard.Layout
import Paths_klay_core (getDataFileName)


-- | Generate "compat" files content
getCompat :: LinuxLayout -> IO TL.Text
getCompat _layout =
  getDataFileName "xkb/compat/missing_from_xkb.xkb" >>= Utf8.readFile
