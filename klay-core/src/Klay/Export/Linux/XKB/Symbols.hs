{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Linux.XKB.Symbols
  ( createSymbols
  , DeadKeyBaseCharMapping
  , CharSequenceMapping
  , SymbolState(..)
  , SymbolError(..)
  ) where


import Data.Maybe (catMaybes, fromJust)
import Data.List (intersperse, nub, sort, dropWhileEnd)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Functor (($>), (<&>))
import Data.Foldable (foldl', foldrM)
import Control.Applicative (Alternative(..))
import Control.Monad.State.Strict
import GHC.Generics (Generic(..))
import Control.DeepSeq (NFData(..))

import TextShow (showb)

import Klay.Export.Linux.XKB.Common
import Klay.Export.Linux.XKB.Lookup
import Klay.Export.Linux.XKB.Lookup.Keycodes.EvDev (toXkbKeyCode)
import Klay.Export.Linux.XKB.Types
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action
  (Actions(..), SpecialAction, ResolvedActions, CustomActions(..), actionsList)
import Klay.Keyboard.Layout.Group
import Data.Customisable (Customisable(..))
import Klay.Utils.Unicode (padL, showUnicodEB, showInvisibleChar)
import Klay.Utils.UserInput (RawLText, escapeControlNewlines)


type S = State SymbolState
type DeadKeyBaseCharMapping = Map DeadKey TB.Builder
type CharSequenceMapping = Map RawLText TB.Builder

createSymbols :: LinuxLayout -> [Map ModifiersOptions CustomXkbType] -> Bool -> String -> (TL.Text, DeadKeyBaseCharMapping, DeadKeyDefinitions, CharSequenceMapping, String, Maybe SymbolError)
createSymbols layout types can_use_native dk_chars =
  let (symbols, SymbolState dks ls dk_chars' es) = runState processLayout mempty{_availableSpecialChars=dk_chars}
      dksDef = _deadKeys layout
  in (symbols, dks, dksDef, ls, dk_chars', es)
  where
    groups :: ResolvedGroups
    groups = _groups layout

    processLayout :: S TL.Text
    processLayout =
      TB.toLazyText . ("default " <>) . mconcat . fst <$> foldrM processGroups (mempty, reverse types) groups

    processGroups :: KeyResolvedGroup -> ([TB.Builder], [Map ModifiersOptions CustomXkbType]) -> S ([TB.Builder], [Map ModifiersOptions CustomXkbType])
    processGroups _ acc@(_, []) = pure acc
    processGroups g (acc, t:ts) = (,ts) . (:acc) <$> processGroup g t can_use_native

processGroup :: KeyResolvedGroup -> Map ModifiersOptions CustomXkbType -> Bool -> S TB.Builder
processGroup g types can_use_native = do
  (all_actions, modifiers_keymap) <- foldrGroupMappingWithKeyM mkActions mempty g
  let modifiers_remapping = Map.foldrWithKey' mkModifierRemap mempty modifiers_keymap
  let sorted_actions = snd . foldl' mkKeyGroups (Nothing, mempty) . sort $ all_actions
  pure . mconcat $
    [ "partial alphanumeric_keys modifier_keys\n"
    , "xkb_symbols \"", groupId', "\" {\n"
    , "  name[Group1] = \"", group_label', "\";\n"
    , "  // Mapping for normal keys\n"
    , sorted_actions, "\n"
    , "  // Remap modifiers\n"
    , modifiers_remapping, "\n"
    , "};\n" ]
  where
    groupId = escapeGroupId g
    groupId' = TB.fromLazyText . getXkbId $ groupId
    group_label' = escapeLabel' . _groupName $ g
    levels = groupLevels g

    mkKeyGroups :: (Maybe XkbIdTL, TB.Builder) -> (XkbIdTL, Key, TB.Builder) -> (Maybe XkbIdTL, TB.Builder)
    mkKeyGroups (mt, es) (t, _, e) = case mt of
      Nothing -> (Just t, mconcat [es, mkTypeEntry t, e])
      Just t' -> if t' == t
        then (Just t, es <> e)
        else (Just t, mconcat [es, mkTypeEntry t, e])
      where mkTypeEntry (XkbId t')
              -- [NOTE] The group index "[1]" in "type[1]" is mandatory to obtain the proper merge behaviour.
              | TL.null t' = "\n  // Errors\n"
              | otherwise = mconcat ["\n  key.type[1] = \"", TB.fromLazyText t', "\";\n"]

    mkActions :: Key -> ResolvedActions -> ([(XkbIdTL, Key, TB.Builder)], Map ModifierBit (Set Key)) -> S ([(XkbIdTL, Key, TB.Builder)], Map ModifierBit (Set Key))
    mkActions _   (ADefault _) acc = pure acc -- Default value: skip
    mkActions key (ACustom as) acc = mkActions' key as acc

    mkActions' :: Key -> Actions -> ([(XkbIdTL, Key, TB.Builder)], Map ModifierBit (Set Key)) -> S ([(XkbIdTL, Key, TB.Builder)], Map ModifierBit (Set Key))
    mkActions' key actions (xs, mks) = do
      mkGroupActions actions >>= \(t, as, cs, ms) -> do
        -- Nothing -> addUnMappedVK groupId vk $> ((mempty, key, errorVKHasNoAction):xs, mks)
        let mks' = foldl' (\acc' m -> Map.insertWith (<>) m [key] acc') mks ms
        case toXkbKeyCode key of
          -- [TODO] Document "override" https://web.archive.org/web/20190720032715/http://pascal.tsu.ru/en/xkb/gram-common.html
          Nothing      -> addUnsupportedK key $> ((mempty, key, errorUnsupportedKey):xs, mks')
          Just keycode -> pure
            ( ( t, key, mconcat
              [ "  //                     ", cs, "\n"
              , "  override key ", keycode, " { [", as, "] };\n"
              ]):xs
            , mks')
      where
        -- errorVKHasNoAction  = mconcat ["  // [WARNING] Key ", showb key, " has a virtual key definition but is not mapped to any actions.\n"]
        errorUnsupportedKey = mconcat ["  // [ERROR] Unsupported key: ", showb key, "\n"]

    mkGroupActions :: Actions -> S (XkbIdTL, TB.Builder, TB.Builder, Set ModifierBit)
    mkGroupActions (SingleAction a) = do
      (action, c, mm) <- mkAction' a
      let ms = maybe mempty Set.singleton mm
      pure (XkbId "ONE_LEVEL", action, c, ms)
    mkGroupActions as@(Actions opts _ _) = do
      let err = error $ "[ERROR] mkGroupActions: " <> show opts -- [FIXME] partial function
          t = Map.findWithDefault err opts types
      let (tid, levels') = getType t opts as
      let as' = actionsList levels' as
      (actions, comments, ms) <- unzip3 <$> forM as' mkAction'
      let actions' = mconcat . intersperse ", " $ actions
      let comments' = mconcat . intersperse ", " $ comments
      let ms' = Set.fromList . catMaybes $ ms
      pure (tid, actions', comments', ms')

    getType :: CustomXkbType -> ModifiersOptions -> Actions -> (XkbIdTL, [Level])
    getType (Default (t, ls)) _ _ = (t, ls)
    getType (Custom t) opts as = -- try to use native type after simplifaction
      case tryNativeType opts as of
        Nothing       -> (_typeId t, levels)
        Just (t', ls) -> (t', ls)

    tryNativeType :: ModifiersOptions -> Actions -> Maybe (XkbIdTL, [Level])
    tryNativeType opts as =
      let as' = actionsList levels as
          ls1 = fmap fst
              . filter ((/= UndefinedAction) . snd)
              . zip levels
              $ as'
          ls2 = flip drop levels
              . length
              . dropWhileEnd (== UndefinedAction)
              $ as'
      in getNativeType opts ls1 <|> getNativeType opts ls2

    getNativeType :: ModifiersOptions -> [Level] -> Maybe (XkbIdTL, [Level])
    getNativeType opts ls = do
      t <- getPredefinedType opts ls
      case t of
        (t', ls', Nothing) -> Just (t', ls')
        (t', ls', Just _)  -> if can_use_native
          then Just (t', ls')
          else Nothing


    mkAction' :: Action -> S (TB.Builder, TB.Builder, Maybe ModifierBit)
    mkAction' a = mkAction a >>= \(a', c, m) -> pure (padKeysym a', padKeysym c, m)

    padKeysym :: TB.Builder -> TB.Builder
    padKeysym = padL 25 ' '

    mkAction :: Action -> S (TB.Builder, TB.Builder, Maybe ModifierBit)
    mkAction (AChar c) = pure
      ( toXkbSymbol c
      , mconcat ["U+", padL 4 '0' $ showUnicodEB c, " ", showInvisibleChar c]
      , Nothing )
    mkAction (AText l) =
      lookupCharSequence l <&> \case
        Nothing     -> ("NoSymbol", empty_symbol, Nothing)
        Just dk_xkb -> (dk_xkb, showb . escapeControlNewlines $ l, Nothing)
    mkAction (ADeadKey dk) =
      lookupDeadKey dk <&> \case
        Nothing     -> ("NoSymbol", empty_symbol, Nothing)
        Just dk_xkb -> (dk_xkb, mconcat ["‹", showb dk, "›"], Nothing)
    mkAction (AModifier m@(Modifier [b] v e)) = case toModifierSymbol b v e of
      Nothing -> addUnsupportedModifier m $> ("NoSymbol", empty_symbol, Nothing)
      Just t  -> case e of
        Lock -> pure (t, mconcat ["‹", showb b, " ", showb v, " ", showb e, "›"], Nothing) -- [FIXME] Do not add modifier_map if "Lock"
        _    -> pure (t, mconcat ["‹", showb b, " ", showb v, " ", showb e, "›"], Just b)
    -- [TODO] support multiple modifier bits
    mkAction (AModifier m) = addUnsupportedModifier m $> ("NoSymbol", empty_symbol, Nothing)
    mkAction (ASpecial a) = case toXkbAction a of
      Nothing -> addUnsupportedSAction a $> ("NoSymbol", empty_symbol, Nothing)
      Just t  -> pure (t, t, Nothing)
    mkAction NoAction        = pure ("VoidSymbol", empty_symbol, Nothing)
    mkAction UndefinedAction = pure ("NoSymbol", empty_symbol, Nothing)

    empty_symbol = "�"

    -- [TODO] Use modifier_map None { … } to clear remapped modifiers
    mkModifierRemap :: ModifierBit -> Set Key -> TB.Builder -> TB.Builder
    mkModifierRemap m keys acc =
      let keys' = mconcat . intersperse ", " . fmap (fromJust . toXkbKeyCode) . Set.toList $ keys
      in mconcat ["  modifier_map ", toRealModifier m, " { ", keys', " }; // ", showb m, "\n", acc]

data SymbolState = SymbolState
  { _deadKeysMapping :: DeadKeyBaseCharMapping
  , _charSequenceMapping :: CharSequenceMapping
  , _availableSpecialChars :: String
  , _errors :: Maybe SymbolError
  } deriving (Eq)

instance Semigroup SymbolState where
  (SymbolState dks1 ls1 cs1 e1) <> (SymbolState dks2 ls2 cs2 e2) = SymbolState (dks1 <> dks2) (ls1 <> ls2) (nub $ cs1 <> cs2) (e1 <> e2)

instance Monoid SymbolState where
  mempty = SymbolState mempty mempty mempty mempty

lookupDeadKey :: DeadKey -> S (Maybe TB.Builder)
lookupDeadKey dk = do
  s@SymbolState{_deadKeysMapping=dks} <- get
  case Map.lookup dk dks of
    Just dk_xkb -> pure (Just dk_xkb)         -- Dead key already encountered, return its XKB code
    Nothing     -> case toXkbDeadKey dk of -- Dead key not encountered previously, try predefined dead keys from XKB
      Just dk_xkb -> do
        let dks' = Map.insert dk dk_xkb dks -- Add native XKB dead key
        put s{_deadKeysMapping=dks'}
        pure (Just dk_xkb)
      Nothing -> createDeadKeyCode dk -- Try to create our own dead key

createDeadKeyCode :: DeadKey -> S (Maybe TB.Builder)
createDeadKeyCode dk = do
  s@SymbolState{_deadKeysMapping=dks, _availableSpecialChars=cs} <- get
  case cs of
    []    -> addUnsupportedDK dk $> Nothing -- Cannot create new dead key: no non-character left
    c:cs' -> do -- Use the next special character to create a new dead key
      let dk_xkb = toXkbSymbol c
      let dks' = Map.insert dk dk_xkb dks
      put s{_deadKeysMapping=dks', _availableSpecialChars=cs'}
      pure (Just dk_xkb)

lookupCharSequence :: RawLText -> S (Maybe TB.Builder)
lookupCharSequence l = do
  s@SymbolState{_charSequenceMapping=ls, _availableSpecialChars=cs} <- get
  case Map.lookup l ls of
    Just dk_xkb -> pure (Just dk_xkb) -- AText already encountered, return its XKB code
    Nothing     -> case cs of -- AText not encountered previously, try to create our own compose sequence
      [] -> addUnsupportedLigature l $> Nothing -- Cannot create new compose character: no non-character left
      c:cs' -> do
        let dk_xkb = toXkbSymbol c
        let ls' = Map.insert l dk_xkb ls
        put s{_charSequenceMapping=ls', _availableSpecialChars=cs'}
        pure (Just dk_xkb)


data SymbolError = SymbolError
  { _unsupportedKeys :: Set Key
  -- ^ Keys not supported in XKB
  , _unsupportedModifiers :: Set Modifier
  -- ^ Modifiers not supported in XKB
  , _unsupportedActions :: Set SpecialAction
  -- ^ Special action not supported in XKB
  , _unsupportedLigatures :: Set RawLText
  -- ^ Ligatures not supported in XKB
  , _unsupportedDeadKeys :: Set DeadKey
  -- ^ Dead keys not supported in XKB
  } deriving (Show, Eq, Ord, Generic, NFData)

instance Semigroup SymbolError where
  (SymbolError a1 b1 c1 d1 e1) <> (SymbolError a2 b2 c2 d2 e2) =
    SymbolError (a1 <> a2) (b1 <> b2) (c1 <> c2) (d1 <> d2) (e1 <> e2)

instance Monoid SymbolError where
  mempty = SymbolError mempty mempty mempty mempty mempty

addUnsupportedK :: Key -> S ()
addUnsupportedK k = modify (mempty{_errors=Just mempty{_unsupportedKeys=[k]}} <>)

addUnsupportedModifier :: Modifier -> S ()
addUnsupportedModifier m = modify (mempty{_errors=Just mempty{_unsupportedModifiers=[m]}} <>)

addUnsupportedSAction :: SpecialAction -> S ()
addUnsupportedSAction a = modify (mempty{_errors=Just mempty{_unsupportedActions=[a]}} <>)

addUnsupportedLigature :: RawLText -> S ()
addUnsupportedLigature l = modify (mempty{_errors=Just mempty{_unsupportedLigatures=[l]}} <>)

addUnsupportedDK :: DeadKey -> S ()
addUnsupportedDK dk = modify (mempty{_errors=Just mempty{_unsupportedDeadKeys=[dk]}} <>)
