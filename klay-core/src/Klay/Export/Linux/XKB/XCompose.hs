{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Linux.XKB.XCompose
  ( createCompose
  , XComposeError
  , DeadKeyError(..)
  ) where


import Data.List (intersperse)
import Data.Char (isSpace, isControl)
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Function ((&))
import Data.Functor (($>), (<&>))
import Data.Foldable (foldrM, foldl')
import Control.Monad.State.Strict

import TextShow (showb)

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action.DeadKey
import Klay.Export.Linux.XKB.Lookup.Characters (toUnicode, toXkbSymbol)
import Klay.Export.Linux.XKB.Lookup.DeadKeys (toXkbDeadKey)
import Klay.Export.Linux.XKB.Symbols (DeadKeyBaseCharMapping, CharSequenceMapping)
import Klay.Utils.Unicode (showUnicodeB)
import Klay.Utils.UserInput (RawLText, rawTextToString)


type XComposeError = DeadKeyError
type S = State (Maybe DeadKeyError)

-- | Create XCompose file content and reports errors, if any.
createCompose :: DeadKeyBaseCharMapping -> DeadKeyDefinitions -> CharSequenceMapping -> String -> (TL.Text, Maybe DeadKeyError)
createCompose dksMap dksDef0 ls dk_chars = (content, errors)
  where
    content = TB.toLazyText . mconcat . intersperse "\n" $
      [ "# Import the default Compose file for the current locale"
      , "include \"%L\""
      , ""
      , "# Character sequences"
      , charSequences
      , deadKeys ]
    -- Character sequences ----------------------------------------------------
    charSequences = Map.foldrWithKey mkCharSequencesDef mempty ls

    mkCharSequencesDef :: RawLText -> TB.Builder -> TB.Builder -> TB.Builder
    mkCharSequencesDef l xkb_code defs = mconcat [mkSymbol xkb_code, ": \"", mconcat . fmap escape . rawTextToString $ l, "\"\n", defs]

    -- Dead keys --------------------------------------------------------------
    dksDef' :: MergedDeadKeyDefinitions
    resolveErrors :: DeadKeyError
    DeadKeyDefinitions{_dkMergedDefinitions=dksDef', _dkErrors=resolveErrors}
      = cleanupDeadKeyDefinitions Nothing (Map.keysSet dksMap) dksDef0
      & flattenDeadKeyDefinitions FlattenThenRemoveChainedDK

    deadKeys :: TB.Builder
    errors, other_errors :: Maybe DeadKeyError
    (deadKeys, other_errors) = runState (foldrM mkDeadKeyDef mempty (Map.keys dksDef')) mempty

    errors = resolveErrors' <> notMapped <> other_errors
      where
        resolveErrors'
          | resolveErrors == mempty = Nothing
          | otherwise                = Just resolveErrors
        notMapped
          | null dks_notMapped = Nothing
          | otherwise = Just mempty{_dkUnmappable=dks_notMapped}

    dksMap' :: DeadKeyBaseCharMapping
    (dksMap', _, dks_notMapped) = foldl' processUnmapped (dksMap, dk_chars, mempty) (Map.keys dksDef')

    processUnmapped :: (DeadKeyBaseCharMapping, String, Set DeadKey) -> DeadKey -> (DeadKeyBaseCharMapping, String, Set DeadKey)
    processUnmapped acc@(m, cs, notMapped) dk
      | Map.member dk m = acc                     -- Dead key already mapped
      | otherwise = case toXkbDeadKey dk of    -- Try predefined dead keys from XKB
          -- Add native XKB dead key
          Just dk_xkb -> (Map.insert dk dk_xkb m, cs, notMapped)
          -- Or try to create our own dead key
          Nothing -> case cs of
            -- Cannot create new code
            []    -> (m, cs, Set.insert dk notMapped)
            -- Create a new code and remove it from available codes
            c:cs' -> (Map.insert dk (toXkbSymbol c) m, cs', notMapped)

    lookupDK :: DeadKey -> Maybe TB.Builder
    lookupDK dk = Map.lookup dk dksMap'

    -- Prepend the definitions of a dead key to the other definitions
    mkDeadKeyDef :: DeadKey -> TB.Builder -> S TB.Builder
    mkDeadKeyDef dk defs =
      case Map.lookup dk dksDef' of
        Nothing -> addUnsupportedDk dk $> defs -- No definition found, ignore dead key
        Just mapping -> do
          mCombos <- mkDeadKeyMapping mapping dk
          case mCombos of
            Nothing     -> pure defs -- Something went wrong, ignore dead key
            Just combos -> pure $ mconcat ["\n# Dead key: ", showb dk, "\n", combos, defs]

    -- Create the entries for a dead key and combinations of following symbols.
    -- There may be several Compose entries for a combo if a chained dead key is encountered.
    -- [NOTE] We limit the recursion of chained dead keys by keeping trace
    --        of the root dead key and previous intermediate dead keys in "forbidden_dk".
    mkDeadKeyMapping :: Set DeadKeyCombo -> DeadKey -> S (Maybe TB.Builder)
    mkDeadKeyMapping mapping dk = case lookupDK dk of
      Nothing  -> addUnsupportedDk dk $> Nothing -- Dead key not supported in XKB, ignore dead key
      Just dk' -> do
        combos <- mkDeadKeyMapping' mapping [dk]
        pure . Just $ Set.foldr' (\m a -> mconcat [mkSymbol dk', m, a]) mempty combos

    -- Almost identical as the previous function. Will be used for recursion to deal will chained dead keys.
    mkDeadKeyMapping' :: Set DeadKeyCombo -> Set DeadKey -> S (Set TB.Builder)
    mkDeadKeyMapping' mapping forbidden_dk = foldrM (mkDeadKeyEntries forbidden_dk) mempty mapping

    -- Create entries for a root dead dead key and a combination of symbols
    mkDeadKeyEntries :: Set DeadKey -> DeadKeyCombo -> Set TB.Builder -> S (Set TB.Builder)
    -- Combo result: a single character
    mkDeadKeyEntries _ (DeadKeyCombo combo (RChar result)) s = do
      mPartialEntry <- foldrM mkComboEntry (Just mempty) combo
      pure $ case mPartialEntry of
        Nothing -> s -- Something went wrong, ignore entry
        Just partialEntry ->
          let entry = mconcat [partialEntry, ": \"", escape result, "\" ", toUnicode result, "\n"]
          in Set.insert entry s
    -- Combo result: multiple characters
    mkDeadKeyEntries _ (DeadKeyCombo combo (RText result)) s =
      foldrM mkComboEntry (Just mempty) combo <&> \case
        Nothing -> s -- Something went wrong, ignore entry
        Just partialEntry ->
          let entry = mconcat [partialEntry, ": \"", mconcat . fmap escape . T.unpack $ result, "\"\n"]
          in Set.insert entry s
    -- Combo result: a chained dead key
    mkDeadKeyEntries _forbidden_dk (DeadKeyCombo _combo (RDeadKey _dk')) _s = error "The impossible happened: chained dead key in XCompose"

    -- Create a single (partial) combo entry
    mkComboEntry :: Symbol -> Maybe TB.Builder -> S (Maybe TB.Builder)
    mkComboEntry _             Nothing  = pure Nothing
    mkComboEntry (SChar c)     (Just e) = pure . Just $ mkChar c <> e
    mkComboEntry (SDeadKey dk) (Just e) = case mkDeadKey dk of
      Nothing  -> addUnsupportedDk dk $> Nothing -- Dead key not supported in XKB, ignore entry
      Just dk' -> pure . Just $ dk' <> e -- Append the dead key to the other symbols

    mkDeadKey = fmap mkSymbol . lookupDK
    mkChar = mkSymbol . toXkbSymbol
    mkSymbol s = mconcat ["<", s, "> "]

    escape :: Char -> TB.Builder
    escape '"'  = "\\\""
    escape '\\' = "\\\\"
    escape c
      | isSpace c || isControl c = "\\0x" <> showUnicodeB c
      | otherwise                = TB.singleton c


-- Error handling -------------------------------------------------------------

-- Last resource. This should not happen, 'resolveMergedDeadKeyDefinitions' should take care of this
addUnsupportedDk :: DeadKey -> S ()
addUnsupportedDk dk = modify (Just mempty{_dkUnsupported=[dk]} <>)

