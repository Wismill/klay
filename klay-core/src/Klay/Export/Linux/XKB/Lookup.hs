module Klay.Export.Linux.XKB.Lookup
  ( toModifierSymbol, toRealModifier
  , toXkbSymbol
  , toXkbDeadKey, xkbDeadKeysMap
  , toXkbAction
  ) where

import Data.Text.Lazy.Builder qualified as TB

import Klay.Export.Linux.XKB.Lookup.Characters (toXkbSymbol)
import Klay.Export.Linux.XKB.Lookup.DeadKeys (toXkbDeadKey, xkbDeadKeysMap)
import Klay.Export.Linux.XKB.Lookup.Modifiers (toModifierSymbol, toRealModifier)
import Klay.Import.Linux.Lookup.Keysyms (mActionToKeysym)
import Klay.Keyboard.Layout.Action (Action(ASpecial), SpecialAction)

-- | Convert an action to its XKB identifier.
toXkbAction :: SpecialAction -> Maybe TB.Builder
toXkbAction = fmap TB.fromText . mActionToKeysym . ASpecial

