{-# LANGUAGE OverloadedLists   #-}

module Klay.Export.Linux.XKB.Scripts
  ( generateScripts
  ) where

import Data.List.NonEmpty qualified as NE
import Data.Semigroup
import Data.Foldable (traverse_)
import System.FilePath
import System.Directory

import Data.Aeson

import Klay.Keyboard.Layout (Layout(..), LinuxLayout, LayoutMetadata(..), SystemName(..), mkLayoutId)
import Klay.Keyboard.Layout.Group
import Klay.Export.Linux.XKB.Common
import Klay.Utils.UserInput
import Paths_klay_core (getDataFileName)


-- [TODO] Add header

-- | Generate installation scripts for XKB files.
generateScripts :: FilePath -> LinuxLayout -> IO ()
generateScripts path layout = do
  createDirectoryStructure path
  traverse_ (generateScript path) vars
  traverse_ (copyScript path) filesToCopy
  where
    vars :: [(FilePath, Value)]
    vars =
      [ ("xkb/switch.sh", object
          [ "layout" .= layoutId
          , "symbols" .= symbols
          , "layout-description" .= layoutDescription
          , "variants" .= variants
          , "variants-descriptions" .= variantsDescriptions
          , "rules" .= layoutId ]
        )
      , ( "xkb/debug.sh", object
          [ "symbols" .= symbols
          , "variants" .= variants
          ]
        )
      , ( "xkb/scripts/klay.py", object
          [ "layout" .= layoutId ]
        )
      ]
    filesToCopy :: [FilePath]
    filesToCopy = [ ]
    layoutId = getSystemName . mkLayoutId $ layout
    layoutDescription = escapeLine . _name . _metadata $ layout
    groups = _groups layout
    symbols = sconcat . NE.intersperse "," . groupsToList . fmap (const layoutId) $ groups
    variants =  sconcat . NE.intersperse "," . groupsToList . fmap (getXkbId . escapeGroupId) $ groups
    variantsDescriptions =  sconcat . NE.intersperse "\\n" . groupsToList . fmap (escapeLine . _groupName) $ groups

generateScript :: FilePath -> (FilePath, Value) -> IO ()
generateScript destination_dir (template_file, vars) = do
  mdestinationFile <- generateFileFromTemplate destination_dir template_file template_file vars
  case mdestinationFile of
    Left destinationFile -> putStrLn $ "[ERROR] XKB: The following script was not created: " <> destinationFile
    Right destinationFile -> do
      perms <- getPermissions destinationFile
      setPermissions destinationFile (setOwnerExecutable True perms)

copyScript :: FilePath -> FilePath -> IO ()
copyScript destination_dir source_file = do
  source_file' <- getDataFileName source_file
  let destinationFile = makeDestinationPath destination_dir source_file
  copyFile source_file' destinationFile
  perms <- getPermissions destinationFile
  setPermissions destinationFile (setOwnerExecutable True perms)

-- | Create a directory structure required by XKB.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure path =
  createDirectoryIfMissing True $ path </> "scripts"
