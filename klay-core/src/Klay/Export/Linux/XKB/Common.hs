module Klay.Export.Linux.XKB.Common
  ( XkbId(..), XkbIdTL
  , XkbLabel(..)
  , escapeText
  , escapeGroupId
  , escapeIdentifier, escapeIdentifier'
  , escapeLabel
  , escapeLabel'
    -- * Template generation
  , generateFileFromTemplate
  , generateFileContentFromTemplate
  , makeDestinationPath
  ) where

import Data.String (IsString(..))
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Text.Lazy.IO.Utf8 qualified as Utf8
import System.FilePath
import GHC.Generics (Generic(..))
import Control.DeepSeq (NFData(..))

import Data.Aeson (Value)

import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Group (SingleOsGroup)
import Klay.Utils.Mustache qualified as U
import Klay.Utils.UserInput (RawLText, escapeSafeFilename, escapeLine)

type XkbIdTL = XkbId TL.Text

-- | An identifier used in XKB, such as: group name, type name, variant, etc.
newtype XkbId t = XkbId { getXkbId :: t }
  deriving stock (Generic, Functor, Show)
  deriving newtype (Eq, Ord, Semigroup, Monoid, IsString, NFData)

-- | A label used in XKB, such as: group name, type name, variant, etc.
newtype XkbLabel = XkbLabel { getXkbLabel :: TB.Builder }
  deriving newtype (Eq, Semigroup, Monoid, IsString, Show)

-- | Escape name and description
escapeText :: RawLText -> TB.Builder
escapeText = TB.fromLazyText . escapeLine

-- | Escape a name of a layout component: name, variant, group, type, etc.
escapeIdentifier :: RawLText -> XkbId TL.Text
escapeIdentifier = XkbId . escapeSafeFilename

escapeIdentifier' :: RawLText -> TB.Builder
escapeIdentifier' = TB.fromLazyText . escapeSafeFilename

escapeGroupId :: SingleOsGroup l -> XkbId TL.Text
escapeGroupId = fmap TL.toLower . escapeIdentifier . _groupName

escapeLabel :: RawLText -> XkbLabel
escapeLabel = XkbLabel . escapeLabel'

escapeLabel' :: RawLText -> TB.Builder
escapeLabel' = TB.fromLazyText . escapeLine

-- | Render a template file with the given values.
generateFileContentFromTemplate
  :: FilePath -- ^ Relative path of the template
  -> Value    -- ^ Values to render the template
  -> IO (Maybe TL.Text)
generateFileContentFromTemplate = U.generateFileContentFromTemplate "XKB rules"

-- | Render a template file with the given values and save it using @UTF-8@ encoding.
generateFileFromTemplate
  :: FilePath -- ^ Parent destination path
  -> FilePath -- ^ Relative path of the template
  -> FilePath -- ^ Relative destination path of the resulting file
  -> Value    -- ^ Values to render the template
  -> IO (Either FilePath FilePath)
generateFileFromTemplate destination_dir template_file destinationFile vars = do
  mcontent <- generateFileContentFromTemplate template_file vars
  let destinationFile' = makeDestinationPath destination_dir destinationFile
  case mcontent of
    Nothing -> pure (Left destinationFile')
    Just content -> do
      Utf8.writeFile destinationFile' content
      pure (Right destinationFile')

{-| Create the destination path from a parent destination and a relative path to a template.

>>> makeDestinationPath "/tmp" "xkb/scripts/functions.sh"
/tmp/scripts/functions.sh
-}
makeDestinationPath
  :: FilePath -- ^ Destination parent path
  -> FilePath -- ^ /Relative/ path of the template
  -> FilePath -- ^ Resulting destination path
makeDestinationPath destination_dir = (destination_dir </>) . joinPath . tail . splitPath
