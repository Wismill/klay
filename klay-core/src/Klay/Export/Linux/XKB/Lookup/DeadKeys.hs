module Klay.Export.Linux.XKB.Lookup.DeadKeys
  ( toXkbDeadKey
  , xkbDeadKeysMap
  ) where

import Data.Maybe (mapMaybe)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Text.Lazy.Builder qualified as TB

import Klay.Keyboard.Layout.Action.Action (Action(..))
import Klay.Keyboard.Layout.Action.DeadKey (DeadKey(..))
import Klay.Import.Linux.Lookup.Keysyms (keysyms)


toXkbDeadKey :: DeadKey -> Maybe TB.Builder
toXkbDeadKey DeadKey{_dkBaseChar=c} = Map.lookup c xkbDeadKeysMap

xkbDeadKeysMap :: Map Char TB.Builder
xkbDeadKeysMap = Map.fromList . mapMaybe mkEntry $ keysyms
  where
    mkEntry (_, identifier, ADeadKey dk, True) = Just (_dkBaseChar dk, TB.fromText identifier)
    mkEntry _                                  = Nothing
