module Klay.Export.Linux.XKB.Lookup.Modifiers
  ( toModifierSymbol
  , toVirtualModifier
  , toRealModifier
  ) where


import Data.Text.Lazy.Builder qualified as TB

import Klay.Keyboard.Layout.Modifier (ModifierBit(..), ModifierVariant(..), ModifierEffect(..))


toModifierSymbol :: ModifierBit -> ModifierVariant -> ModifierEffect -> Maybe TB.Builder
toModifierSymbol Control   L Set   = Just "Control_L"
toModifierSymbol Control   R Set   = Just "Control_R"
toModifierSymbol Control   _ Set   = Just "Control_R"
toModifierSymbol Shift     L Set   = Just "Shift_L"
toModifierSymbol Shift     R Set   = Just "Shift_R"
toModifierSymbol Shift     _ Set   = Just "Shift_R"
toModifierSymbol Shift     _ Latch = Just "ISO_Level2_Latch"
toModifierSymbol Shift     _ Lock  = Just "Shift_Lock"
toModifierSymbol Capitals  _ Set   = Just "ISO_Lock"
toModifierSymbol Capitals  _ Latch = Just "F31"
toModifierSymbol Capitals  _ Lock  = Just "F32" -- [NOTE] Alternative Lock to avoid XKB setting "modifier_map" to Lock
-- toModifierSymbol Capitals  _ Lock  = Just "Caps_Lock"
toModifierSymbol Alternate L Set   = Just "Alt_L"
toModifierSymbol Alternate R Set   = Just "Alt_R"
toModifierSymbol Alternate _ Set   = Just "Alt_R"
toModifierSymbol Super     L Set   = Just "Super_L"
toModifierSymbol Super     R Set   = Just "Super_R"
toModifierSymbol Super     _ Set   = Just "Super_R"
toModifierSymbol Numeric   _ Set   = Just "F33"
toModifierSymbol Numeric   _ Latch = Just "F34"
toModifierSymbol Numeric   _ Lock  = Just "F35"
toModifierSymbol IsoLevel3 _ Set   = Just "ISO_Level3_Shift"
toModifierSymbol IsoLevel3 _ Latch = Just "ISO_Level3_Latch"
toModifierSymbol IsoLevel3 _ Lock  = Just "ISO_Level3_Lock"
toModifierSymbol IsoLevel5 _ Set   = Just "ISO_Level5_Shift"
toModifierSymbol IsoLevel5 _ Latch = Just "ISO_Level5_Latch"
toModifierSymbol IsoLevel5 _ Lock  = Just "ISO_Level5_Lock"
toModifierSymbol _         _ _     = Nothing
-- [TODO] What about L and R variants for latch and locks?

{-|
XKB modifiers bits mapping.

Sources:

- @\/usr\/share\/X11\/xkb\/symbols\/pc@
- @\/usr\/share\/X11\/xkb\/symbols\/altwin@

Some mappings are widely accepted and must not be changed to avoid bugs in applications:

- Shift, Control on their respective core modifiers
- Alt\/Meta on Mod1
- Super\/Meta on Mod4

Other are conventional and should not be changed to maintain consistency with defaults in XKB:

- Capitals on Lock
- Numeric on Mod2
- IsoLevel3 on Mod5
- IsoLevel5 on Mod3
-}
toRealModifier :: ModifierBit -> TB.Builder
toRealModifier Shift     = "Shift"
toRealModifier Capitals  = "Lock"
toRealModifier Control   = "Control"
toRealModifier Alternate = "Mod1"
toRealModifier Super     = "Mod4"
toRealModifier Numeric   = "Mod2"
toRealModifier IsoLevel3 = "Mod5"
toRealModifier IsoLevel5 = "Mod3"


-- [TODO] Rework to use only native modifiers?
toVirtualModifier :: ModifierBit -> TB.Builder
toVirtualModifier Alternate = "Alt"
toVirtualModifier Super     = "Super"
toVirtualModifier Numeric   = "Numeric"
toVirtualModifier IsoLevel3 = "LevelThree"
toVirtualModifier IsoLevel5 = "LevelFive"
toVirtualModifier m         = toRealModifier m
