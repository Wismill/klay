{-# LANGUAGE OverloadedLists   #-}

module Klay.Export.Linux.XKB.Lookup.Keycodes.EvDev
  ( toXkbKeyCode
  , xkbKeycodeMapping
  ) where


import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Text.Lazy.Builder qualified as TB

import TextShow (TextShow(showb))

import Klay.Keyboard.Hardware.Key (Key)
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Utils.Unicode (padL)


-- | Convert a key to its XKB keycode. See 'xkbKeycodeMapping'.
toXkbKeyCode :: Key -> Maybe TB.Builder
toXkbKeyCode key = fmap (("<" <>) . (<> ">")) . Map.lookup key $ xkbKeycodeMapping

{- |
Mapping to XKB keycodes as defined in @/usr/share/X11/xkb/keycodes/evdev@.

The [Xorg keycodes](https://cgit.freedesktop.org/xorg/driver/xf86-input-evdev/tree/src/evdev.c)
are 8 larger than their [Linux keycodes](https://github.com/torvalds/linux/blob/master/include/uapi/linux/input-event-codes.h) equivalent.
(see [Arch forum](https://wiki.archlinux.org/index.php/Keyboard_input)).
-}
xkbKeycodeMapping :: Map Key TB.Builder
xkbKeycodeMapping = mconcat
  [ editAndFunction
  , alphanumeric
  , numericKeypad
  , applications
  , media
  , browser
  , power
  , misc
  , japanese -- [TODO] Move these to another keyboard
  ]

-- Section: Editing-and-function -----------------------------------------------
editAndFunction :: Map Key TB.Builder
editAndFunction = mconcat
  [ functionKeys
  , movementKeys
  , edition
  , modifiers
  , inputMethod
  ]

misc :: Map Key TB.Builder
misc =
  [ (K.Escape, "ESC")
  , (K.Iso102, "LSGT")
  , (K.PrintScreen, "PRSC")
  , (K.ScrollLock, "SCLK")
  , (K.Pause, "PAUS")
    --
  , (K.Backslash, "BKSL")
  , (K.Backspace, "BKSP")
    --
  , (K.Tabulator, "TAB")
  , (K.Return, "RTRN")
    --
  , (K.Menu, "MENU")
    --
  , (K.ScreenLock, "I160")
  , (K.Stop, "STOP")
  , (K.Cancel, "I231")
  , (K.Save, "I242")
  ]

functionKeys :: Map Key TB.Builder
functionKeys = mkRow "FK" 1 (enumFromTo K.F1 K.F24)

movementKeys :: Map Key TB.Builder
movementKeys =
  [ (K.CursorLeft, "LEFT")
  , (K.CursorRight, "RGHT")
  , (K.CursorUp, "UP")
  , (K.CursorDown, "DOWN")
  , (K.PageUp, "PGUP")
  , (K.PageDown, "PGDN")
  , (K.Home, "HOME")
  , (K.End, "END") ]

edition :: Map Key TB.Builder
edition =
  [ (K.Insert, "INS")
  , (K.Delete, "DELE")
  , (K.Undo, "I158") -- [TODO] check Undo
  , (K.Redo, "I190") -- [TODO] check Redo
  , (K.Cut, "CUT") -- [TODO] check Cut
  , (K.Copy, "COPY") -- [TODO] check Copy
  , (K.Paste, "PAST") -- [TODO] check Paste
  ]

modifiers :: Map Key TB.Builder
modifiers =
  [ (K.CapsLock, "CAPS")
  , (K.LShift, "LFSH")
  , (K.RShift, "RTSH")
  , (K.LControl, "LCTL")
  , (K.RControl, "RCTL")
  , (K.LSuper, "LWIN")
  , (K.RSuper, "RWIN")
  , (K.LAlternate, "LALT")
  , (K.RAlternate, "RALT") ]

inputMethod :: Map Key TB.Builder
inputMethod =
  [ (K.Muhenkan, "MUHE")
  , (K.Henkan, "HENK")
  , (K.Katakana, "KATA")
  , (K.Hiragana, "HIRA")
  , (K.HiraganaKatakana, "HKTG")
  , (K.Hangeul, "HNGL")
  , (K.Hanja, "HJCV") ]

-- Section: alphanumeric ------------------------------------------------------
alphanumeric :: Map Key TB.Builder
alphanumeric = mconcat
  [ [(K.Tilde, "TLDE")]
  , mkRow "AE" 1 (enumFromTo K.N1 K.N9 <> [K.N0, K.Minus, K.Plus])
  , mkRow "AD" 1 [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
  , mkRow "AC" 1 [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote, K.Backslash]
  , mkRow "AB" 1 [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash]
  , [(K.Iso102, "LSGT"), (K.Space, "SPCE")]
  ]

-- [TODO] Move to a proper file
japanese :: Map Key TB.Builder
japanese =
  [ (K.JPBackSlash, "AB11")
  , (K.Yen, "AE13")
  ]

-- Section: numeric -----------------------------------------------------------
numericKeypad :: Map Key TB.Builder
numericKeypad =
  [ (K.NumLock, "NMLK")
  , (K.KP0, "KP0")
  , (K.KP1, "KP1")
  , (K.KP2, "KP2")
  , (K.KP3, "KP3")
  , (K.KP4, "KP4")
  , (K.KP5, "KP5")
  , (K.KP6, "KP6")
  , (K.KP7, "KP7")
  , (K.KP8, "KP8")
  , (K.KP9, "KP9")
  , (K.KPPlusMinus, "I126")
  , (K.KPAdd, "KPAD")
  , (K.KPSubtract, "KPSU")
  , (K.KPMultiply, "KPMU")
  , (K.KPDivide, "KPDV")
  , (K.KPDecimal, "KPDL")
  , (K.KPComma, "I129")
  , (K.KPJPComma, "JPCM")
  , (K.KPEquals, "KPEQ")
  , (K.KPEnter, "KPEN") ]

-- Miscellaneous --------------------------------------------------------------
applications :: Map Key TB.Builder
applications = -- [TODO] Check keycodes for applications
  [ (K.Calculator, "I148")
  , (K.MediaPlayer, "I234")
  , (K.Email1, "I163")
  , (K.Email2, "I223")
  , (K.Find, "FIND")
  , (K.Explorer, "I152")
  , (K.Browser, "I158")
  , (K.Documents, "I243")
  , (K.MyComputer, "I165")
  , (K.Help, "HELP")
  , (K.Launch1, "I158")
  , (K.Launch2, "I159")
  , (K.Launch3, "I210")
  , (K.Launch4, "I211") ]

media :: Map Key TB.Builder
media =
  [ (K.MediaPlay, "I215")
  , (K.MediaPlayCD, "I208")
  , (K.MediaPause, "I209")
  , (K.MediaPlayPause, "I172")
  , (K.MediaPrevious, "I173")
  , (K.MediaNext, "I171")
  , (K.MediaStop, "I174")
  , (K.MediaRewind, "I176")
  , (K.MediaForward, "I216")
  , (K.MediaEject, "I169")
  , (K.VolumeMute, "MUTE")
  , (K.VolumeDown, "VOL-")
  , (K.VolumeUp, "VOL+") ]

browser :: Map Key TB.Builder
browser =
  [ (K.BrowserBack, "I166")
  , (K.BrowserForward, "I167")
  , (K.BrowserRefresh, "I181")
  , (K.BrowserSearch, "I225")
  , (K.BrowserFavorites, "I164")
  , (K.BrowserHomePage, "I180") ]

power :: Map Key TB.Builder
power =
  [ (K.Power, "POWR")
  , (K.Sleep, "I150")
  , (K.Suspend, "I213")
  , (K.WakeUp, "I151")
  , (K.BrightnessDown, "I237")
  , (K.BrightnessUp, "I238") ]

mkRow :: TB.Builder -> Int -> [Key] -> Map Key TB.Builder
mkRow prefix k0 = Map.fromList . fmap mkKey . zip [k0..]
  where mkKey (k, key) = (key, (prefix <>) . padL 2 '0' . showb $ k)
