module Klay.Export.Linux.XKB.Lookup.Characters
  ( toUnicode
  , toXkbSymbol
  ) where


import Prelude hiding (Left, Right)
import Data.Text.Lazy.Builder qualified as TB

import Klay.Keyboard.Layout.Action.DeadKey (Symbol(SChar))
import Klay.Import.Linux.Lookup.Keysyms (mSymbolToKeysym)
import Klay.Utils.Unicode (showUnicodEB, padL)

{- Sources:

- @\/usr\/include\/X11\/keysymdef.h@
- [X11 protocol](https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html#keysym_encoding)
-}

toUnicode :: Char -> TB.Builder
toUnicode = ("U" <>) . padL 4 '0' . showUnicodEB

toXkbSymbol :: Char -> TB.Builder
toXkbSymbol c
  = maybe (toUnicode c) TB.fromText $ mSymbolToKeysym (SChar c)
