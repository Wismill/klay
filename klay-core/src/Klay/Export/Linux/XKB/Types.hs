{-# LANGUAGE OverloadedLists   #-}

module Klay.Export.Linux.XKB.Types
  ( XkbType(..)
  , CustomXkbType
  , createTypes
  , getPredefinedType
  ) where

import Data.Maybe (isJust)
import Data.List (intersperse)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.Functor (($>))
import Data.Bifunctor (Bifunctor(..))
import Data.Foldable (foldrM)
import Control.Monad.State.Strict

import Data.Hashable (Hashable(..))
import TextShow (showb)

import Klay.Export.Linux.XKB.Common (XkbId(..), XkbIdTL, XkbLabel(..), escapeIdentifier, escapeLabel)
import Klay.Export.Linux.XKB.Lookup.Modifiers (toVirtualModifier)
import Klay.Keyboard.Layout (Layout(..), LinuxLayout, LayoutMetadata(..))
import Klay.Keyboard.Layout.Action (Actions(..), untagCustomisableActions)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Level (Level)
import Klay.Keyboard.Layout.Modifier
import Data.Customisable (Customisable(..))
import Klay.Utils.UserInput (fromTShow)

-- [TODO] Use standard modifiers instead of virtual ones?
-- [TODO] Add led indicators?

-- | Create the content of the XKB \"type\" file corresponding to a layout.
createTypes :: LinuxLayout -> ([Map ModifiersOptions CustomXkbType], Bool, TL.Text)
createTypes layout = (types,can_use_native,) . TB.toLazyText . mconcat $
  [ "default xkb_types \"basic\" {\n"
  , "  virtual_modifiers ", virtual_modifiers, ";\n"
  , definitions
  , "};\n" ]
  where
    groups = fmap (fmap (fmap untagCustomisableActions)) . _groups $ layout
    layout_name = escapeIdentifier . _systemName . _metadata $ layout

    virtual_modifiers = mkVkModifiers $ foldr mkVkModifier mempty groups
      where
        -- Note: remove core modifiers [Shift, Capitals, Control] from virtual modifiers
        mkVkModifiers = mconcat . intersperse ", " . fmap toVirtualModifier
                      . Set.toAscList . flip Set.difference [Shift, Capitals, Control] . mconcat
        mkVkModifier g ms = groupModifiers g : ms

    types :: [Map ModifiersOptions CustomXkbType]
    types = fmap go <$> raw_types
      where
        go (Just (t, ls, DoNotUseCapsOption), _) = Default (t, ls)
        go (Just (t, ls, UseCapsOption), _)
          | can_use_native                       = Default (t, ls)
        go (_, t)                                = Custom t

    can_use_native = isJust capsBehaviour

    capsBehaviour :: Maybe (Maybe CapsLockBehaviour)
    raw_types :: [RawCustomXkbTypeMap]
    (raw_types, capsBehaviour) = runState (foldrM mkTypes mempty groups) (Just Nothing)
      where
        mkTypes :: KeyFinalGroup -> [RawCustomXkbTypeMap] -> S [RawCustomXkbTypeMap]
        mkTypes g ts = (: ts) <$> createGroupTypes layout_name g

    definitions = mconcat . fmap (foldr makeTypeContent mempty) $ types

-- | A XKB type. [TODO] add link to the doc
data XkbType = XkbType
  { _typeId :: XkbIdTL
  -- ^ Identifier of the type
  , _typeModifiers :: Set ModifierBit
  -- ^ Set of all modifiers used to define the level
  , _typeLevelModifiers :: [(LevelId, [(ModifiersCombo, ModifiersCombo)])]
  -- ^ Map XKB level id -> modifiers
  , _typeLevelNames :: [(LevelId, XkbLabel)]
  -- ^ Map XKB level id -> level name
  } deriving (Eq, Show)

type CustomXkbType = Customisable (XkbIdTL, [Level]) XkbType
type RawCustomXkbTypeMap = Map ModifiersOptions RawCustomXkbType
type RawCustomXkbType = (Maybe (XkbIdTL, [Level], UseCapsOption), XkbType)
type S = State (Maybe (Maybe CapsLockBehaviour))

newtype LevelId = LevelId Int
  deriving newtype (Eq, Num, Enum, Show)

-- | Create all the types usable by a group corresponding to the variations of options.
createGroupTypes :: XkbIdTL -> KeyFinalGroup -> S RawCustomXkbTypeMap
createGroupTypes prefix g = fmap Map.fromList . traverse getType . Set.toList $ allOptionsCombinations
  where
    levels = groupLevels g :: [Level]

    allOptionsCombinations :: Set ModifiersOptions
    allOptionsCombinations = Map.foldr go [mempty] (_groupMapping g)

    go (SingleAction _)  acc = acc
    go (Actions mos _ _) acc = Set.insert mos acc

    getType :: ModifiersOptions -> S (ModifiersOptions, RawCustomXkbType)
    getType opts = (opts,) . (,createType prefix g opts) <$> getPredefinedTypeM opts levels

data UseCapsOption = UseCapsOption | DoNotUseCapsOption
  deriving (Eq, Show)

getPredefinedTypeM :: ModifiersOptions -> [Level] -> S (Maybe (XkbIdTL, [Level], UseCapsOption))
getPredefinedTypeM opts levels =
  let mt = getPredefinedType opts levels
  in case mt of
    Just (t, ls, Just c)  -> checkCaps c $> Just (t, ls, UseCapsOption)
    Just (t, ls, Nothing) -> pure         $  Just (t, ls, DoNotUseCapsOption)
    Nothing               -> pure            Nothing
  where
    checkCaps c = modify (>>= \case
      Nothing -> Just (Just c)
      Just c' -> if c' == c then Just (Just c) else Nothing)

{- | Get the predefined type with the given options

Based on:
  - [XKB FindAutomaticType](https://github.com/xkbcommon/libxkbcommon/blob/master/src/xkbcomp/symbols.c#L1291)
  - [XKB xkb_keysym_is_lower](https://github.com/xkbcommon/libxkbcommon/blob/master/src/keysym.c#L244)
-}
getPredefinedType :: ModifiersOptions -> [Level] -> Maybe (XkbIdTL, [Level], Maybe CapsLockBehaviour)
getPredefinedType opts ls
  | has1Level            = Just ("ONE_LEVEL"        , ls, Nothing)
  | has2LevelsControl   = Just ("PC_CONTROL_LEVEL2", ls, Nothing)
  | has2Levels_alternate = Just ("PC_ALT_LEVEL2"    , ls, Nothing)
  | has2Levels = if
    | opts == twoLevel                            -> Just ("TWO_LEVEL", ls, Nothing)
    | opts == alphabetic                           -> Just ("ALPHABETIC", ls, Just CapsIsShiftCancel)
    | opts == alphabeticNoCancel                 -> Just ("ALPHABETIC", ls, Just CapsIsShiftNoCancel)
    | opts == fourLevelAlphabetic                -> Just ("ALPHABETIC", ls, Just CapsIsShiftCancel)
    | opts == fourLevelAlphabeticNoCancel      -> Just ("ALPHABETIC", ls, Just CapsIsShiftNoCancel)
    | opts == fourLevelSemiAlphabetic           -> Just ("ALPHABETIC", ls, Just CapsIsShiftCancel)
    | opts == fourLevelSemiAlphabeticNoCancel -> Just ("ALPHABETIC", ls, Just CapsIsShiftNoCancel)
    | otherwise                                    -> Nothing
  | has3Levels || has4Levels = if
    | opts == fourLevel                           -> Just ("FOUR_LEVEL", ls, Nothing)
    | opts == fourLevelAlphabetic                -> Just ("FOUR_LEVEL_ALPHABETIC", ls, Just CapsIsShiftCancel)
    | opts == fourLevelAlphabeticNoCancel      -> Just ("FOUR_LEVEL_ALPHABETIC", ls, Just CapsIsShiftNoCancel)
    | opts == fourLevelSemiAlphabetic           -> Just ("FOUR_LEVEL_SEMIALPHABETIC", ls, Just CapsIsShiftCancel)
    | opts == fourLevelSemiAlphabeticNoCancel -> Just ("FOUR_LEVEL_SEMIALPHABETIC", ls, Just CapsIsShiftNoCancel)
    | opts == alphabetic                           -> Just ("FOUR_LEVEL_SEMIALPHABETIC", ls, Just CapsIsShiftCancel)
    | opts == alphabeticNoCancel                 -> Just ("FOUR_LEVEL_SEMIALPHABETIC", ls, Just CapsIsShiftNoCancel)
    | otherwise                                    -> Nothing
  | has4Levels && opts == separateCapsAndShiftAlphabetic = Just ("SEPARATE_CAPS_AND_SHIFT_ALPHABETIC", ls, Nothing)
  | (has4Levels || has4LevelsWithLock) && opts == fourLevelWithLock =
    Just ("FOUR_LEVEL_PLUS_LOCK", [isoLevel1, isoLevel2, isoLevel3, isoLevel4, capitals], Nothing)
  | has5Levels || has6Levels || has7Levels || has8Levels = if
    | opts == eightLevel                            -> Just ("EIGHT_LEVEL", eightLevels, Nothing)
    | opts == eightLevelAlphabetic                 -> Just ("EIGHT_LEVEL_ALPHABETIC", eightLevels, Nothing)
    -- opts == eightLevelAlphabeticNoCancel      -> Just ("EIGHT_LEVEL_ALPHABETIC", eightLevels, Nothing)
    | opts == eightLevelSemiAlphabetic            -> Just ("EIGHT_LEVEL_SEMIALPHABETIC", eightLevels, Nothing)
    -- opts == eightLevelSemiAlphabeticNoCancel -> Just ("EIGHT_LEVEL_SEMIALPHABETIC", eightLevels, Nothing)
    | opts == eightLevelLevelFiveLock            -> Just ("EIGHT_LEVEL_LEVEL_FIVE_LOCK", eightLevels, Nothing)
    | opts == eightLevelAlphabeticLevelFiveLock -> Just ("EIGHT_LEVEL_ALPHABETIC_LEVEL_FIVE_LOCK", eightLevels, Nothing)
    | opts == fourLevelAlphabeticNoCancel        -> Just ("EIGHT_LEVEL_ALPHABETIC", eightLevels, Nothing)
    | opts == fourLevelAlphabetic                  -> Just ("EIGHT_LEVEL_ALPHABETIC", eightLevels, Nothing)
    | opts == fourLevelSemiAlphabeticNoCancel   -> Just ("EIGHT_LEVEL_SEMIALPHABETIC", eightLevels, Nothing)
    | opts == fourLevelSemiAlphabetic             -> Just ("EIGHT_LEVEL_SEMIALPHABETIC", eightLevels, Nothing)
    | opts == alphabetic                             -> Just ("EIGHT_LEVEL_SEMIALPHABETIC", eightLevels, Nothing)
    | opts == alphabeticNoCancel                   -> Just ("EIGHT_LEVEL_SEMIALPHABETIC", eightLevels, Nothing)
    | otherwise                                      -> Nothing
  | otherwise = Nothing
  -- [TODO] CTRL+ALT
  where
    has1Level            = ls == [isoLevel1]
    has2LevelsControl   = ls == [isoLevel1, control]
    has2Levels_alternate = ls == [isoLevel1, alternate]

    has2Levels = ls == [isoLevel1, isoLevel2]
    has3Levels = ls == [isoLevel1, isoLevel2, isoLevel3]
    has4Levels = ls == [isoLevel1, isoLevel2, isoLevel3, isoLevel4]
    has4LevelsWithLock = ls == [isoLevel1, isoLevel2, capitals, isoLevel3, isoLevel4]
    has5Levels = ls == [isoLevel1, isoLevel2, isoLevel3, isoLevel4, isoLevel5]
    has6Levels = ls == [isoLevel1, isoLevel2, isoLevel3, isoLevel4, isoLevel5, isoLevel6]
    has7Levels = ls == [isoLevel1, isoLevel2, isoLevel3, isoLevel4, isoLevel5, isoLevel6, isoLevel7]
    has8Levels = ls == [isoLevel1, isoLevel2, isoLevel3, isoLevel4, isoLevel5, isoLevel6, isoLevel7, isoLevel8]
    eightLevels = [isoLevel1, isoLevel2, isoLevel3, isoLevel4, isoLevel5, isoLevel6, isoLevel7, isoLevel8]

-- | Create a single XKB type.
createType :: XkbId TL.Text -> KeyFinalGroup -> ModifiersOptions -> XkbType
createType prefix g opts = XkbType
  { _typeId = typeId
  , _typeModifiers = typeModifiers
  , _typeLevelModifiers = typeModifiersByLevel
  , _typeLevelNames = second snd <$> levels
  }
  where
    levels :: [(LevelId, ([(ModifiersCombo, ModifiersCombo)], XkbLabel))]
    levels = groupLayoutLevelsNames' opts g
    typeModifiersByLevel = second fst <$> levels
    typeModifiers = Set.unions . Set.fromList . mconcat . fmap (fmap fst . snd) $ typeModifiersByLevel
    groupId = escapeIdentifier . _groupName $ g
    typeId = TL.toUpper <$> mconcat
      [ prefix, "_", groupId, "_"
      -- [TODO] Improve naming
      , escapeIdentifier . fromTShow . hash . show . modifiersOptionsToList $ opts]

makeTypeContent :: CustomXkbType -> TB.Builder -> TB.Builder
makeTypeContent (Default _) = id
makeTypeContent (Custom t)  = (<>) $ mconcat
  [ "  type \"", typeId, "\" {\n"
  , "    modifiers = ", modifiers, ";\n"
  , "    // Levels definition\n"
  , levelsMapping
  , "    // Levels names\n"
  , levelsNames
  , "  };\n" ]
  where
    typeId = TB.fromLazyText . getXkbId . _typeId $ t
    modifiers = mkModifiers . _typeModifiers $ t
    mkModifiers = mconcat . intersperse "+" . fmap toVirtualModifier . Set.toAscList
    levelsMapping = foldr mkLevelMaps mempty . _typeLevelModifiers $ t
    mkLevelMaps :: (LevelId, [(ModifiersCombo, ModifiersCombo)]) -> TB.Builder -> TB.Builder
    mkLevelMaps (levelId, [])  = mkLevelMaps (levelId, [mempty]) -- Ensure we process base level with no modifier
    mkLevelMaps (levelId, lms) = (<>) $ foldr mkLevelMaps' mempty lms
      where
        levelId' = mkLevelId levelId
        mkLevelMaps' (ms, preserved) =
          let ms' = if null ms then "None" else mkModifiers ms
              mapping
                | null preserved = mconcat [ "    map[", ms', "] = ", levelId', ";\n" ]
                | otherwise      = mconcat [ "    map[", ms', "] = ", levelId', ";\n"
                                           , "    preserve[", ms', "] = ", mkModifiers preserved, ";\n" ]
          in (<>) mapping
    levelsNames = foldr mkLevelNames mempty . _typeLevelNames $ t
      where
        mkLevelNames (levelId, levelName) = (<>) $ mconcat
          [ "    level_name[", mkLevelId levelId, "] = \"", getXkbLabel levelName, "\";\n" ]
    -- [NOTE] Do not use "LevelN" notation, because N is limited to 8
    mkLevelId (LevelId i) = showb i

groupLayoutLevelsNames'
  :: ModifiersOptions
  -> KeyFinalGroup
  -> [(LevelId, ([(ModifiersCombo, ModifiersCombo)], XkbLabel))]
groupLayoutLevelsNames' opts
  = zip [1..]
  . fmap (bimap go escapeLabel)
  . filter f
  . Map.toList
  . _groupLevels
  where
    f (l, _) = toCanonicalMask opts l == l
    go m = let ms = processModifiersOptionsDetails opts m
           in bimap maskToModifiers maskToModifiers <$> ms
