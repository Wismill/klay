module Klay.Export.Windows
  ( WindowsFormat(..)
  , MsKlcC(..)
  , generateWdk
  , generateMsklc
  ) where

import Klay.Export.Windows.WDK (generateWdk)
import Klay.Export.Windows.MSKLC (MsKlcC(..), generateMsklc)

-- | Windows keyboard layout formats
data WindowsFormat
  = WDK           -- ^ Windows Driver Kit
  | MSKLC !MsKlcC -- ^ Microsoft Keyboard Layout Creator
  deriving (Eq, Ord, Show)

instance Bounded WindowsFormat where
  minBound = WDK
  maxBound = MSKLC maxBound

instance Enum WindowsFormat where
  toEnum 0 = minBound
  toEnum 1 = MSKLC minBound
  toEnum 2 = MSKLC maxBound
  toEnum n = error $ "Out of bound WindowsFormat toEnum: " <> show n

  fromEnum WDK = 0
  fromEnum (MSKLC opt) = 1 + fromEnum opt

  enumFrom     x   = enumFromTo     x maxBound
  enumFromThen x y = enumFromThenTo x y bound
    where
      bound | fromEnum y >= fromEnum x = maxBound
            | otherwise                = minBound
