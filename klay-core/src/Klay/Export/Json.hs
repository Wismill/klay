module Klay.Export.Json
  ( generateJsonFile
  , generateJson
  ) where

import System.FilePath ((</>), (<.>))
import Data.ByteString.Lazy qualified as BS
import System.Directory (createDirectoryIfMissing)

import Data.Aeson.Encode.Pretty qualified as Aeson

import Klay.Keyboard.Layout
import Klay.Utils.UserInput
import Klay.Export.Yaml (toYamlLayout)

generateJsonFile :: FilePath -> MultiOsRawLayout -> IO ()
generateJsonFile path layout
  =  createDirectoryStructure path
  *> BS.writeFile path' (generateJson layout)
  where path' = path </> system_name <.> "json"
        system_name = escapeFilename' . _systemName . _metadata $ layout

generateJson :: MultiOsRawLayout -> BS.ByteString
generateJson = Aeson.encodePretty' jsonOptions . toYamlLayout

jsonOptions :: Aeson.Config
jsonOptions = Aeson.defConfig
  { Aeson.confIndent = Aeson.Spaces 2
  }

-- | Create a directory structure.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure = createDirectoryIfMissing True
