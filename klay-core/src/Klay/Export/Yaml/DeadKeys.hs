module Klay.Export.Yaml.DeadKeys
  ( RawDeadKeyDefinitions(..)
  ) where

import Data.Void (Void)
import Data.Functor ((<&>))
import Control.Arrow ((&&&))
import GHC.Generics (Generic(..))

import Data.Semigroup
import Data.Text qualified as T
import Data.List.NonEmpty qualified as NE
import Data.HashMap.Strict qualified as HMap
import Data.Aeson.Types (ToJSON(..), FromJSON(..), (.=), (.:))
import Data.Aeson qualified as Aeson
import Text.Megaparsec
import Text.Megaparsec.Char

import Klay.Import.Linux.XCompose (symbolP, resultP')
import Klay.Keyboard.Layout qualified as L
import Klay.Keyboard.Layout.Action.DeadKey qualified as DK
import Klay.Keyboard.Layout.Action.Json
import Klay.Utils (jsonOptions)

newtype RawDeadKeyDefinitions = RawDeadKeyDefinitions
  { unRawDeadKeyDefinitions :: L.RawDeadKeyDefinitions }
  deriving (Generic, Eq)

instance ToJSON RawDeadKeyDefinitions where
  toJSON = toJSON
         . fmap (uncurry RawDeadKeyDefinition)
         . unRawDeadKeyDefinitions
  toEncoding = toEncoding
             . fmap (uncurry RawDeadKeyDefinition)
             . unRawDeadKeyDefinitions

instance FromJSON RawDeadKeyDefinitions where
  parseJSON
    = fmap (RawDeadKeyDefinitions . fmap (_dk &&& _dkDefinitions))
    . parseJSON

data RawDeadKeyDefinition = RawDeadKeyDefinition
  { _dk :: DK.DeadKey
  , _dkDefinitions :: [DK.DeadKeyCombo]
  } deriving (Generic)

instance ToJSON RawDeadKeyDefinition where
  toJSON (RawDeadKeyDefinition dk combos) = Aeson.object
    [ "dead key"  .= DK._dkName dk
    , "label"     .= DK._dkLabel dk
    , "base char" .= charToKeysym (DK._dkBaseChar dk)
    , "combos"    .= (DeadKeyCombo <$> combos)
    ]
  toEncoding (RawDeadKeyDefinition dk combos) = Aeson.pairs . mconcat $
    [ "dead key"  .= DK._dkName dk
    , "label"     .= DK._dkLabel dk
    , "base char" .= charToKeysym (DK._dkBaseChar dk)
    , "combos"    .= (DeadKeyCombo <$> combos)
    ]

instance FromJSON RawDeadKeyDefinition where
  parseJSON = Aeson.withObject "dead key definition" \o -> do
    n  <- o .: "dead key"
    l  <- o .: "label"
    s  <- o .: "base char"
    cs <- o .: "combos" <&> fmap unDeadKeyCombo
    case charFromKeysym s of
      Left e  -> fail e
      Right c -> pure $ RawDeadKeyDefinition (DK.DeadKey n l c) cs

-- We need this newtype in order to avoid reference cycle using keysyms
newtype DeadKey = DeadKey { unDeadKey :: DK.DeadKey } deriving (Generic)
instance ToJSON DeadKey where
  toJSON = Aeson.genericToJSON (jsonOptions (drop 2))
    { Aeson.unwrapUnaryRecords = True }
  toEncoding (DeadKey dk) = Aeson.pairs . mconcat $
    [ "name"      .= DK._dkName dk
    , "label"     .= DK._dkLabel dk
    , "base char" .= charToKeysym (DK._dkBaseChar dk)
    ]

instance FromJSON DeadKey where
  parseJSON = Aeson.withObject "dead key" \o -> do
    n <- o .: "name"
    l <- o .: "label"
    s <- o .: "base char"
    case charFromKeysym s of
      Left e  -> fail e
      Right c -> pure $ DeadKey (DK.DeadKey n l c)

newtype DeadKeyCombo = DeadKeyCombo {unDeadKeyCombo :: DK.DeadKeyCombo} deriving (Generic)

instance ToJSON DeadKeyCombo where
  toJSON (DeadKeyCombo (DK.DeadKeyCombo s r)) = Aeson.object
    [serializeDkSequence s .= Result r]
  toEncoding (DeadKeyCombo (DK.DeadKeyCombo s r)) = Aeson.pairs $
    serializeDkSequence s .= Result r

instance FromJSON DeadKeyCombo where
  parseJSON = Aeson.withObject "dead key combo" \o ->
    case HMap.toList o of
      []      -> fail "No dead key combo"
      [(s,r)] ->
        parseJSON r >>=
        either fail' go (parse (dkSequenceP <* eof) mempty s)
      _       -> fail "Invalid dead key combo: too many fields"
    where
      fail' e _ = fail (errorBundlePretty e)
      go s (Result r) = pure $ DeadKeyCombo (DK.DeadKeyCombo s r)

newtype Result = Result {unResult :: DK.Result} deriving (Generic)

instance ToJSON Result where
  toJSON (Result r) = case r of
    DK.RChar c     -> toJSON $ charToKeysym c
    DK.RText t     -> toJSON $ "text:" <> t
    DK.RDeadKey dk -> toJSON $ serializeDeadKey dk
  toEncoding (Result r) = case r of
    DK.RChar c     -> toEncoding $ charToKeysym c
    DK.RText t     -> toEncoding $ "text:" <> t
    DK.RDeadKey dk -> toEncoding $ serializeDeadKey dk

instance FromJSON Result where
  parseJSON = Aeson.withText "dead key combo result"
    $ either (fail . errorBundlePretty) pure
    . parse (resultP <* eof) mempty

serializeDkSequence :: NE.NonEmpty DK.Symbol -> T.Text
serializeDkSequence = sconcat . NE.intersperse " " . fmap serializeSymbol

serializeSymbol :: DK.Symbol -> T.Text
serializeSymbol (DK.SChar c)     = charToKeysym c
serializeSymbol (DK.SDeadKey dk) = serializeDeadKey dk

dkSequenceP :: Parsec Void T.Text (NE.NonEmpty DK.Symbol)
dkSequenceP
  =   symbolP' `sepBy` char ' '
  >>= maybe (fail "Empty list of symbols") pure . NE.nonEmpty
  <?> "dead key sequence"

symbolP' :: Parsec Void T.Text DK.Symbol
symbolP' = ((DK.SDeadKey <$> prefixedDeadKeyP)
      <|> (try symbolP <?> "keysym symbol")
      <|> (DK.SChar <$> anySingle))
      <?> "dead key symbol"

resultP :: Parsec Void T.Text Result
resultP = Result <$> choice
  [ DK.RText <$> prefixedTextP
  , DK.RDeadKey <$> prefixedDeadKeyP
  , try resultP' <?> "keysym result"
  , takeRest >>= \t -> case T.uncons t of
    Nothing      -> fail "empty result"
    Just (c, "") -> pure (DK.RChar c)
    _            -> pure (DK.RText t)
  ] <?> "dead key result"
