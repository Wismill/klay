module Klay.Export.Yaml.Layout
  ( toYamlLayout
  , Layout(..)
  , RawDeadKeyDefinitions(..)
  ) where

-- import Data.Void (Void)
import Data.Functor ((<&>))
-- import Data.Bifunctor
import Control.Monad ((>=>))
import Control.Arrow ((&&&))
import GHC.Generics (Generic(..))

-- import Data.Semigroup
-- import Data.Char (isPrint, isSpace, isMark)
-- import Data.Text qualified as T
-- import Data.Set qualified as Set
-- import Data.List.NonEmpty qualified as NE
import Data.Map.Strict qualified as Map
-- import Data.HashMap.Strict qualified as HMap
-- import Data.Map.NonEmpty qualified as NEMap
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson
-- import Text.Megaparsec
-- import Text.Megaparsec.Char

import Klay.Keyboard.Layout qualified as L
import Klay.Keyboard.Layout.Action.Action (Action(..))
import Klay.Keyboard.Layout.Action.DeadKey qualified as DK
import Klay.Keyboard.Layout.Group (Groups, itraverseGroups)
import Klay.Export.Yaml.DeadKeys
import Klay.Export.Yaml.Group qualified as G
import Klay.Export.Yaml.KeyMap qualified as KM
import Klay.OS
-- import Klay.Utils.UserInput
import Klay.Utils (foldrMWithKey', jsonOptions)

data Layout = Layout
  { _metadata :: L.LayoutMetadata
  , _dual_functions :: L.ButtonActionsDefinition
  , _dead_keys :: RawDeadKeyDefinitions
  , _options :: L.LayoutOptions
  , _groups :: Groups G.Group
  } deriving (Generic, Eq)

layoutJsonOptions :: Aeson.Options
layoutJsonOptions = jsonOptions $ fmap (\c -> case c of '_' -> ' '; _ -> c) . drop 1

instance ToJSON Layout where
  toJSON     = Aeson.genericToJSON layoutJsonOptions
  toEncoding = Aeson.genericToEncoding layoutJsonOptions

instance FromJSON Layout where
  parseJSON = Aeson.genericParseJSON layoutJsonOptions >=> post_process
    where
      post_process l = do
        let dkd = unRawDeadKeyDefinitions . _dead_keys $ l
        let dke = Map.fromList
                . fmap ((DK._dkBaseChar &&& id) . fst)
                $ dkd
        let fix_labels_ k x xs = (:xs) . (,x) <$> fix_action "Custom action labels" dke k
        let fix_labels = fmap Map.fromList
                       . foldrMWithKey' fix_labels_ mempty
        let oi opts = fix_labels (L._iActionLabels opts) >>= \labels -> pure opts{L._iActionLabels=labels}
        let ol opts = fix_labels (L._lActionLabels opts) >>= \labels -> pure opts{L._lActionLabels=labels}
        let ow opts = fix_labels (L._wActionLabels opts) >>= \labels -> pure opts{L._wActionLabels=labels}
        opts <- traverseMultiOs (MultiOs oi ol ow) (_options l)
        gs <- itraverseGroups (post_process_group dke) (_groups l)
        dkd' <- traverse (traverse $ traverse (fix_dk_combo dke)) dkd
        pure l{_options=opts, _groups=gs, _dead_keys=RawDeadKeyDefinitions dkd'}
      fix_dk_combo dke (DK.DeadKeyCombo s r) =
        DK.DeadKeyCombo <$> traverse (fix_symbol dke) s <*> fix_result dke r
      fix_symbol dke (DK.SDeadKey dk) = DK.SDeadKey <$> fix_dk "deadkey combos" dke dk
      fix_symbol _   s = pure s
      fix_result dke (DK.RDeadKey dk) = DK.RDeadKey <$> fix_dk "deadkey combos" dke dk
      fix_result _   r = pure r
      post_process_group dke gi g =
        traverse (fix_keymap_item dke gi) (G._keyMap g) >>= \km ->
          pure g{G._keyMap=km}
      fix_keymap_item dke gi (KM.SingletonKey k oss a) =
        KM.SingletonKey k oss <$> fix_action (mconcat [show gi, ", key ", show k]) dke a
      fix_keymap_item dke gi (KM.NormalKey k oss mo a as) =
        let go = fix_action (mconcat [show gi, ", key ", show k]) dke
        in KM.NormalKey k oss mo <$> traverse go a <*> traverse go as
      fix_keymap_item dke gi (KM.LayerGroup ks mo ls) =
        let process_layer l' =
              process_actions (KM._lActions l')
              <&> \as -> l'{KM._lActions = as}
            process_actions (KM.LayerActions as) =
              KM.LayerActions <$> traverse (traverse (fix_action (mconcat [show gi, ", layout"]) dke)) as
        in KM.LayerGroup ks mo <$> traverse process_layer ls
      fix_action loc dke (ADeadKey dk) = ADeadKey <$> fix_dk loc dke dk
      fix_action _   _   a = pure a
      fix_dk loc dke dk = case Map.lookup (DK._dkBaseChar dk) dke of
        Nothing -> fail . mconcat $
          [ loc
          , ": unknown dead key with base char '"
          , [DK._dkBaseChar dk]
          , "'" ]
        Just dk' -> pure dk'

toYamlLayout :: L.MultiOsRawLayout -> Layout
toYamlLayout layout = Layout
  { _metadata = L._metadata layout
  , _dual_functions = L._dualFunctionKeys layout
  , _dead_keys
    = RawDeadKeyDefinitions
    . DK._dkRawDefinitions
    . L._deadKeys
    $ layout
  , _options = L._options layout
  , _groups = G.toYamlGroup <$> L._groups layout
  }
