module Klay.Export.Yaml.Group
  ( Group(..)
  , toYamlGroup
  , fromYamlGroup
  ) where

import Data.Maybe
import Data.String (IsString(..))
-- import Data.Set qualified as Set
-- import Data.List.NonEmpty qualified as NE
-- import Data.Map.Strict qualified as Map
import Data.Map.NonEmpty qualified as NEMap
-- import Data.IntMap.NonEmpty qualified as NEIntMap
-- import Data.Text.Lazy qualified as TL
-- import Data.Bifunctor
import GHC.Generics (Generic(..))
-- import GHC.Exts (IsList(..))

-- import Data.Yaml qualified as Yaml
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Klay.Keyboard.Layout.Group qualified as G
import Klay.Export.Yaml.KeyMap
import Klay.OS
import Klay.Utils.UserInput
import Klay.Utils (jsonOptions)

data Group = Group
  { _name :: Maybe RawLText
  , _levels :: MultiOsGroupLevels
  , _keyMap :: KeyMap
  } deriving (Generic, Eq)

instance ToJSON Group where
  toJSON     = Aeson.genericToJSON (jsonOptions (drop 1))
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 1))

instance FromJSON Group where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 1))

instance ToJSON (G.Groups Group) where
  toJSON     = Aeson.toJSON1
  toEncoding = Aeson.toEncoding1

instance FromJSON (G.Groups Group) where
  parseJSON = Aeson.parseJSON1

type SingleOsLevels = LevelMap RawLText
data MultiOsGroupLevels
  = SingleOsLevels SingleOsLevels
  | MultiOsGroupLevels (MultiOsId SingleOsLevels)
  deriving (Generic, Eq)

groupLevelsJsonOptions :: Aeson.Options
groupLevelsJsonOptions = (jsonOptions id)
  { Aeson.sumEncoding = Aeson.UntaggedValue }

instance ToJSON MultiOsGroupLevels where
  toJSON     = Aeson.genericToJSON groupLevelsJsonOptions
  toEncoding = Aeson.genericToEncoding groupLevelsJsonOptions

instance FromJSON MultiOsGroupLevels where
  parseJSON = Aeson.genericParseJSON groupLevelsJsonOptions

toYamlGroup :: G.KeyMultiOsRawGroup -> Group
toYamlGroup g = Group
  { _name = Just (G._groupName g)
  , _levels = toYamlLevels (G._groupLevels g)
  , _keyMap = toYamlKeyMap (G._groupMapping g)
  }

toYamlLevels :: G.MultiOsGroupLevels -> MultiOsGroupLevels
toYamlLevels = MultiOsGroupLevels . mapWithOs (const go)
  -- [FIXME] may fail if no level defined.
  where go = NEMap.unsafeFromMap

fromYamlLevels :: MultiOsGroupLevels -> G.MultiOsGroupLevels
fromYamlLevels = \case
  SingleOsLevels ls      -> toMultiOs (go ls)
  MultiOsGroupLevels lss -> mapWithOs (const go) lss
  where go = NEMap.toMap

fromYamlGroup :: G.GroupIndex -> Group -> G.KeyMultiOsRawGroup
fromYamlGroup i g = G.Group
  { G._groupName = fromMaybe ("Group " <> fromString (show (fromEnum i))) (_name g)
  , G._groupLevels = fromYamlLevels (_levels g)
  , G._groupMapping = fromYamlKeymap (_keyMap g)
  }
