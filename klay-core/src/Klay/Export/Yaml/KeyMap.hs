{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Yaml.KeyMap
  ( LevelMap
  , KeyOptions(..)
  , NamedKeyOptions(..)
  , KeyMap
  , KeyMapItem(..)
  , Layer(..)
  , LayerActions(..)
  , OSs(..)
  , toYamlKeyMap
  , fromYamlKeymap
  , serializeDeadKey
  ) where

import Data.Maybe
import Data.Semigroup
import Data.Char (toLower)
import Data.Text qualified as T
-- import Data.Tuple (swap)
import Data.List (intersperse)
import Data.List.NonEmpty qualified as NE
-- import Data.Set qualified as Set
import Data.Set.NonEmpty qualified as NESet
import Data.Map qualified as Map
import Data.Map.NonEmpty qualified as NEMap
import Data.IntMap.NonEmpty qualified as NEIntMap
import Data.Bifunctor
import GHC.Generics (Generic(..))

import TextShow (showt)
import Data.Aeson qualified as Aeson
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson.Types qualified as Aeson
import Data.Aeson.Encoding qualified as Aeson
import Text.Megaparsec
import Text.Megaparsec.Char (space1)

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action qualified as A
import Klay.Keyboard.Layout.Action.Action qualified as A
import Klay.Keyboard.Layout.Action.Json (keyP)
import Klay.Keyboard.Layout.Action.DeadKey qualified as DK
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.Modifier.Types qualified as M
import Klay.OS
import Klay.Utils (jsonOptions, chunksOf, chunksOfNE)
import Data.Alterable ((<+))


type KeyMap = NE.NonEmpty KeyMapItem

data KeyMapItem
  = SingletonKey
    { _key :: K.Key
    , _os :: Maybe OSs
    , _action :: A.Action
    }
  | NormalKey
    { _key :: K.Key
    , _os :: Maybe OSs
    , _options :: Maybe KeyOptions
    , _default :: Maybe A.Action
    , _actions :: LevelMap A.Action
    }
  | LayerGroup
    { _keys :: Keys
    , _options :: Maybe KeyOptions
    , _layers :: NE.NonEmpty Layer
    }
  deriving (Generic, Eq)

instance ToJSON KeyMapItem where
  toJSON = Aeson.genericToJSON (jsonOptions (drop 1))
    { Aeson.sumEncoding = Aeson.UntaggedValue
    , Aeson.omitNothingFields = True }
  toEncoding = Aeson.genericToEncoding (jsonOptions (drop 1))
    { Aeson.sumEncoding = Aeson.UntaggedValue
    , Aeson.omitNothingFields = True }

instance FromJSON KeyMapItem where
  parseJSON = Aeson.genericParseJSON (jsonOptions (drop 1))
    { Aeson.sumEncoding = Aeson.UntaggedValue }

data Layer = Layer
  { _lLayer :: M.ModifiersField
  , _lOs :: Maybe OSs
  , _lActions :: LayerActions
  } deriving (Generic, Eq)

instance ToJSON Layer where
  toJSON     = Aeson.genericToJSON     (jsonOptions (fmap toLower . drop 2))
    { Aeson.omitNothingFields = True }
  toEncoding = Aeson.genericToEncoding (jsonOptions (fmap toLower . drop 2))
    { Aeson.omitNothingFields = True }

instance FromJSON Layer where
  parseJSON = Aeson.genericParseJSON (jsonOptions (fmap toLower . drop 2))

newtype Keys = Keys {unKeys :: NE.NonEmpty K.Key} deriving (Generic, Eq)

keysChunkSize :: Int
keysChunkSize = 12

instance ToJSON Keys where
  toJSON (Keys ks)
    = Aeson.String
    . mconcat
    . intersperse "\n"
    . fmap (mconcat . intersperse " " . fmap showt)
    . chunksOf keysChunkSize
    . NE.toList
    $ ks
  toEncoding (Keys ks)
    = Aeson.text
    . mconcat
    . intersperse "\n"
    . fmap (mconcat . intersperse " " . fmap showt)
    . chunksOf keysChunkSize
    . NE.toList
    $ ks

instance FromJSON Keys where
  parseJSON = Aeson.withText "keys" p
    where
      p = either fail (maybe (fail "empty list of keys") (pure . Keys))
        . bimap errorBundlePretty NE.nonEmpty
        . parse pKeys mempty
      pKeys = keyP `sepBy1` space1 <* eof

newtype LayerActions = LayerActions {unLayerActions :: NE.NonEmpty (Maybe A.Action)}
  deriving (Generic, Eq)

instance ToJSON LayerActions where
  toJSON (LayerActions as)
    = Aeson.String
    . sconcat
    . NE.intersperse "\n"
    . fmap (mconcat . intersperse " " . fmap A.serializeAction')
    . chunksOfNE keysChunkSize
    $ as
  toEncoding (LayerActions as)
    = Aeson.text
    . sconcat
    . NE.intersperse "\n"
    . fmap (mconcat . intersperse " " . fmap A.serializeAction')
    . chunksOfNE keysChunkSize
    $ as

instance FromJSON LayerActions where
  parseJSON = Aeson.withText "actions" $ either fail pure . p
    where
      p = either (Left . errorBundlePretty)
            ( maybe (Left "empty actions list") (Right . LayerActions)
            . NE.nonEmpty )
        . parse pLayout mempty
      pLayout = A.actionP' `sepEndBy1` space1 <* eof

newtype OSs = OSs { unOSs :: NE.NonEmpty OsSpecific}
  deriving stock (Generic, Show)
  deriving newtype (Eq, Semigroup)

instance ToJSON OSs where
  toJSON     (OSs [os]) = toJSON os
  toJSON     (OSs oss)  = toJSON oss
  toEncoding (OSs [os]) = toEncoding os
  toEncoding (OSs oss)  = toEncoding oss

instance FromJSON OSs where
  parseJSON = \case
    v@(Aeson.Array _)  -> OSs <$> parseJSON v
    v@(Aeson.String _) -> OSs . (NE.:|[]) <$> parseJSON v
    v                  -> Aeson.unexpected v

data KeyOptions
  = Auto M.CapsLockBehaviour
  | NamedKeyOptions NamedKeyOptions
  | DetailedKeyOptions (LevelMap M.ModifiersOption)
  deriving (Generic, Eq, Ord)

instance ToJSON KeyOptions where
  toJSON     = Aeson.genericToJSON     keyOptionsJsonOptions
  toEncoding = Aeson.genericToEncoding keyOptionsJsonOptions

instance FromJSON KeyOptions where
  parseJSON = Aeson.genericParseJSON keyOptionsJsonOptions

keyOptionsJsonOptions :: Aeson.Options
keyOptionsJsonOptions = (jsonOptions id)
  { Aeson.sumEncoding = Aeson.UntaggedValue }

data NamedKeyOptions
  = NoOptions
  | TwoLevelAlphabetic
  | TwoLevelAlphabeticNoCancel
  | FourLevelSemiAlphabetic
  | FourLevelSemiAlphabeticNoCancel
  | FourLevelReversedSemiAlphabetic
  | FourLevelAlphabetic
  | FourLevelAlphabeticNoCancel
  | FourLevelWithLock
  | SeparateCapsAndShiftAlphabetic
  | EightLevelSemiAlphabetic
  | EightLevelSemiAlphabeticNoCancel
  | EightLevelAlphabetic
  | EightLevelAlphabeticNoCancel
  | EightLevelFiveLock
  | EightLevelAlphabeticFiveLock
  deriving (Generic, Eq, Ord)

instance ToJSON NamedKeyOptions where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance FromJSON NamedKeyOptions where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

type Actions = Either A.Action MultiActionsParams
type MultiActionsParams = (Maybe KeyOptions, A.Action, LevelMap A.Action)

serializeDeadKey :: DK.DeadKey -> T.Text
serializeDeadKey dk = "dk:" <> T.singleton (DK._dkBaseChar dk)

type LevelMap a = NEMap.NEMap M.ModifiersField a

neIntmapToLevelMap :: NEIntMap.NEIntMap a -> LevelMap a
neIntmapToLevelMap
  = NEMap.fromList
  . fmap (first M.ModifiersField)
  . NEIntMap.toList

levelMapToNEIntmap :: LevelMap a -> NEIntMap.NEIntMap a
levelMapToNEIntmap
  = NEIntMap.fromList
  . NEMap.toList
  . NEMap.mapKeys M._mField

toYamlActions :: K.Key -> A.RawActions -> Maybe Actions
toYamlActions key = \case
  -- Keep default action only if alphanumeric and not null
  A.ADefault as -> if K.isAlphanumeric key && as /= mempty
    then go as
    else Nothing
  A.ACustom as -> go as
  where
    go (A.AutoActions _    (A.SingleAction a) ) = Just (Left a)
    go (A.AutoActions auto (A.Actions mo a as)) =
        let auto' = case auto of
              M.Auto         -> Nothing
              M.DefaultAutoModifiersOptions -> Nothing
              M.AutoWith opt -> Just (Auto opt)
              M.Manual       -> Just (toYamlModifiersOptions mo)
            as' = neIntmapToLevelMap as
        in Just (Right (auto', a, as'))

toYamlModifiersOptions :: M.ModifiersOptions -> KeyOptions
toYamlModifiersOptions mo@(M.ModifiersOptions opts) =
  NEIntMap.withNonEmpty (NamedKeyOptions NoOptions) go opts
  where
    go opts'
      | mo == M.alphabetic                             = NamedKeyOptions TwoLevelAlphabetic
      | mo == M.alphabeticNoCancel                   = NamedKeyOptions TwoLevelAlphabeticNoCancel
      | mo == M.fourLevelSemiAlphabetic             = NamedKeyOptions FourLevelSemiAlphabetic
      | mo == M.fourLevelSemiAlphabeticNoCancel   = NamedKeyOptions FourLevelSemiAlphabeticNoCancel
      | mo == M.fourLevelReverseSemiAlphabetic     = NamedKeyOptions FourLevelReversedSemiAlphabetic
      | mo == M.fourLevelWithLock                   = NamedKeyOptions FourLevelWithLock
      | mo == M.fourLevelAlphabetic                  = NamedKeyOptions FourLevelAlphabetic
      | mo == M.fourLevelAlphabeticNoCancel        = NamedKeyOptions FourLevelAlphabeticNoCancel
      | mo == M.separateCapsAndShiftAlphabetic     = NamedKeyOptions SeparateCapsAndShiftAlphabetic
      | mo == M.eightLevelSemiAlphabetic            = NamedKeyOptions EightLevelSemiAlphabetic
      | mo == M.eightLevelSemiAlphabeticNoCancel  = NamedKeyOptions EightLevelSemiAlphabeticNoCancel
      | mo == M.eightLevelAlphabetic                 = NamedKeyOptions EightLevelAlphabetic
      | mo == M.eightLevelAlphabeticNoCancel       = NamedKeyOptions EightLevelAlphabeticNoCancel
      | mo == M.eightLevelLevelFiveLock            = NamedKeyOptions EightLevelFiveLock
      | mo == M.eightLevelAlphabeticLevelFiveLock = NamedKeyOptions EightLevelAlphabeticFiveLock
      | otherwise = DetailedKeyOptions (neIntmapToLevelMap opts')

fromYamlModifiersOptions :: Maybe KeyOptions -> (M.AutoModifiersOptions, M.ModifiersOptions)
fromYamlModifiersOptions = \case
  Nothing                        -> (M.Auto, M.NoModifiersOptions)
  Just (Auto caps              ) -> (M.AutoWith caps, M.NoModifiersOptions)
  Just (NamedKeyOptions n      ) -> (M.Manual, resolve_namedOptions n)
  Just (DetailedKeyOptions opts) ->
    ( M.Manual
    , M.ModifiersOptions . NEIntMap.toMap $ levelMapToNEIntmap opts
    )
  where
    resolve_namedOptions NoOptions                        = mempty
    resolve_namedOptions TwoLevelAlphabetic               = M.alphabetic
    resolve_namedOptions TwoLevelAlphabeticNoCancel       = M.alphabeticNoCancel
    resolve_namedOptions FourLevelSemiAlphabetic          = M.fourLevelSemiAlphabetic
    resolve_namedOptions FourLevelSemiAlphabeticNoCancel  = M.fourLevelSemiAlphabeticNoCancel
    resolve_namedOptions FourLevelReversedSemiAlphabetic  = M.fourLevelReverseSemiAlphabetic
    resolve_namedOptions FourLevelWithLock                = M.fourLevelWithLock
    resolve_namedOptions FourLevelAlphabetic              = M.fourLevelAlphabetic
    resolve_namedOptions FourLevelAlphabeticNoCancel      = M.fourLevelAlphabeticNoCancel
    resolve_namedOptions SeparateCapsAndShiftAlphabetic   = M.separateCapsAndShiftAlphabetic
    resolve_namedOptions EightLevelSemiAlphabetic         = M.eightLevelSemiAlphabetic
    resolve_namedOptions EightLevelSemiAlphabeticNoCancel = M.eightLevelSemiAlphabeticNoCancel
    resolve_namedOptions EightLevelAlphabetic             = M.eightLevelAlphabetic
    resolve_namedOptions EightLevelAlphabeticNoCancel     = M.eightLevelAlphabeticNoCancel
    resolve_namedOptions EightLevelFiveLock               = M.eightLevelLevelFiveLock
    resolve_namedOptions EightLevelAlphabeticFiveLock     = M.eightLevelAlphabeticLevelFiveLock

fromYamlKeymap :: KeyMap -> A.MultiOsRawActionsKeyMap
fromYamlKeymap
  -- update the default (implicit) map with the explicit map
  = Map.mapWithKey A.checkMultiOsDefaultActions
  . Map.unionWith (\_ x -> x) defaultMap
  -- build explicit map
  . foldr go mempty . NE.toList
  where
    defaultMap = Map.map (fmap A.ADefault) A.multiOsUsActionMap
    go :: KeyMapItem -> A.MultiOsRawActionsKeyMap -> A.MultiOsRawActionsKeyMap
    go x acc = Map.unionWith (<+) xs acc
      where
        mkOs :: K.Key -> Maybe OSs -> A.RawActions -> A.MultiOsRawActions
        mkOs key mos
          = A.MultiOsActions
          . mapWithOs (A.checkDefaultActions key)
          . case mos of
            Nothing        -> toMultiOs
            Just (OSs oss) -> fromListWithDefault A.rawUndefinedActions
                            . (<$> NE.toList oss)
                            . flip (,)

        xs = case x of
          SingletonKey key mos a ->
            Map.singleton key $ mkOs key mos . A.ACustom $ A.AutoActions M.Auto (A.SingleAction a)
          NormalKey key mos mo ma as ->
            let (auto, mo') = fromYamlModifiersOptions mo
            in Map.singleton key $ mkOs key mos . A.ACustom . A.AutoActions auto $ mkActions mo' ma as
          LayerGroup keys mo layers ->
            let actions = foldr (mkLayer keys) mempty layers :: LayerKeyMap'
                (auto, mo') = fromYamlModifiersOptions mo
            in Map.map
               ( A.MultiOsActions
               . mapWithOs (\_ as ->
                 let as' = NEMap.mapMaybe (>>= f) as
                     f a = case a of A.UndefinedAction -> Nothing; _ -> Just a
                     as'' = NEMap.withNonEmpty mempty (mkActions mo' Nothing) as'
                 in A.ACustom (A.AutoActions auto as''))
               . NEMap.foldMapWithKey
                 (\l -> mapWithOs \_ a -> NEMap.singleton l a)
               )
               actions

        mkLayer :: Keys -> Layer -> LayerKeyMap' -> LayerKeyMap'
        mkLayer (Keys keys) (Layer l oss (LayerActions as)) =
          let oss' = NE.toList $ maybe allOsSpecific unOSs oss
              as' = Map.fromList
                  . zip (NE.toList keys)
                  . fmap ( NEMap.singleton l
                         . fromListWithDefault Nothing
                         . zip oss' . repeat )
                  . NE.toList
                  $ as
          in Map.unionWith (NEMap.unionWith (<>)) as'

        mkActions mo ma as = A.Actions mo (fromMaybe mempty ma) (levelMapToNEIntmap as)

-- [FIXME] how do we handle empty map?!
toYamlKeyMap :: A.MultiOsRawActionsKeyMap -> KeyMap
toYamlKeyMap
  = NE.fromList . mkKeyMapItems
  . Map.foldlWithKey' groupKeys (LayerBuilder mempty mempty mempty)
  where
    groupKeys :: LayerBuilder -> K.Key -> A.MultiOsRawActions -> LayerBuilder
    groupKeys acc key (A.MultiOsActions as)
      = NEMap.foldlWithKey (processKey key) acc . go . toNonEmptyMap $ as
      where
        isComplex :: Bool
        isComplex = fst $ foldlWithOs check (False, Nothing) as
        -- Check if the type of key or its default action is different
        -- among the OSs
        check (True , _) _ _   = (True, Nothing)
        check (False, t) _ as' = case untag as' of
          A.SingleAction _ -> case t of
            Just (Just _) -> (True, Nothing)
            _             -> (False, Just Nothing)
          A.Actions _ a _ -> case t of
            Just Nothing   -> (True, Nothing)
            Just (Just a') -> if a /= a'
              then (True, Nothing)
              else (False, Just (Just a))
            _ -> (False, Just (Just a))
        go | isComplex = NEMap.mapKeys Left
           | otherwise = NEMap.mapKeys Right
        untag = A._aActions . A.untagCustomisableActions
    processKey key acc os actions =
      case toYamlActions key actions of
        -- Discard default
        Nothing -> acc
        -- Single actions
        Just (Left a) -> add_single key (either id id os) a acc
        -- Multi actions
        Just (Right as0@(mo, a, as)) -> case os of
          Left os' -> add_non_trivial key os' as0 acc
          Right os' -> case a of
            A.UndefinedAction -> add_trivial key os' mo as acc
            _                 -> add_non_trivial key os' as0 acc

    allOsSpecific' = NESet.fromList allOsSpecific
    mkOSs oss = if oss == allOsSpecific'
      then Nothing
      else Just (OSs $ NESet.toList oss)

    mkKeyMapItems :: LayerBuilder -> [KeyMapItem]
    mkKeyMapItems as = mconcat
      [ Map.foldlWithKey mkSingleAction mempty (_singles as)
      , Map.foldlWithKey mkMultiActions mempty (_multi_non_trivial as)
      , Map.foldrWithKey' mkLayers mempty (_multi_trivial as)
      ]
    mkSingleAction :: [KeyMapItem] -> K.Key -> Map.Map A.Action (NESet.NESet OsSpecific) -> [KeyMapItem]
    mkSingleAction acc key = Map.foldrWithKey
      (\a oss acc' -> SingletonKey key (mkOSs oss) a : acc') acc

    -- [TODO] group oss
    mkLayers :: Maybe KeyOptions -> LayerKeyMap -> [KeyMapItem] -> [KeyMapItem]
    mkLayers mo as acc =
      let keys = Keys (NEMap.keys as)
          levels = NESet.toList $ NEMap.foldr' (NESet.union . NEMap.keysSet) (NESet.singleton mempty) as
          layers = sconcat $ mkOsLayers as <$> levels
          layerGroup = LayerGroup keys mo <$> NE.nonEmpty layers
      in maybe acc (:acc) layerGroup

    mkOsLayers :: LayerKeyMap -> M.ModifiersField -> [Layer]
    mkOsLayers as0 l =
      let os_layouts = NEMap.foldMapWithKey (mkLayouts l) as0
                     :: MultiOsId (NE.NonEmpty (Maybe A.Action))
          grouped_os_layouts = NEMap.mapMaybeWithKey
                               (\as oss -> if all isNothing as then Nothing else Just oss)
                             . NEMap.fromListWith (<>)
                             . fmap (\(os, as) -> (as, [os]))
                             . toNonEmpty $ os_layouts
      in mkLayer l <$> Map.toList grouped_os_layouts

    mkLayer l (as, oss) = Layer l (mkOSs oss) (LayerActions as)

    mkLayouts :: M.ModifiersField -> K.Key -> LayerLevelsMap -> MultiOsId (NE.NonEmpty (Maybe A.Action))
    mkLayouts l _ ls = mapWithOs (\_ a -> [a])
                     $ NEMap.findWithDefault mempty l ls

    mkMultiActions acc key = Map.foldrWithKey
      (\(mo, a, as) oss acc' -> NormalKey key (mkOSs oss) mo (Just a) as:acc') acc

    add_single key os a acc = acc
      { _singles=Map.insertWith (Map.unionWith (<>)) key [(a, [os])]
      $ _singles acc
      }
    add_non_trivial key os as acc = acc
      { _multi_non_trivial=Map.insertWith (Map.unionWith (<>)) key [(as, [os])]
      $ _multi_non_trivial acc
      }
    add_trivial :: K.Key -> OsSpecific -> Maybe KeyOptions -> LevelMap A.Action -> LayerBuilder -> LayerBuilder
    add_trivial key os mo as acc =
      let as' = NEMap.map
                  ( toMultiOsWithDefault Nothing os
                  . \a -> case a of A.UndefinedAction -> Nothing
                                    _                 -> Just a
                  )
                as :: LayerLevelsMap
      in acc
      { _multi_trivial=
        Map.unionWith (NEMap.unionWith (NEMap.unionWith (<>)))
        (Map.singleton mo (NEMap.singleton key as'))
        (_multi_trivial acc)
      }

data LayerBuilder = LayerBuilder
  { _singles :: Map.Map K.Key (Map.Map A.Action (NESet.NESet OsSpecific))
  , _multi_non_trivial :: Map.Map K.Key (Map.Map MultiActionsParams (NESet.NESet OsSpecific))
  , _multi_trivial :: LayerOptionsMap
  }

type LayerOptionsMap = Map.Map (Maybe KeyOptions) LayerKeyMap
type LayerKeyMap  = NEMap.NEMap K.Key LayerLevelsMap
type LayerKeyMap' = Map.Map     K.Key LayerLevelsMap
type LayerLevelsMap = LevelMap (MultiOsId (Maybe A.Action))
