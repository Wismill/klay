module Klay.Export.Yaml
  ( generateYamlFile
  , generateYaml
  , toYamlLayout
  , Layout(..)
  , RawDeadKeyDefinitions(..)
  ) where

import System.FilePath ((</>), (<.>))
import System.Directory (createDirectoryIfMissing)

import Data.Char (isPrint, isSpace, isMark)
import Data.Text qualified as T
import Data.ByteString qualified as BS
import Data.Yaml qualified as Yaml
import Text.Libyaml qualified as LibYaml

import Klay.Keyboard.Layout qualified as L
import Klay.Export.Yaml.DeadKeys
import Klay.Export.Yaml.Layout
import Klay.Utils.UserInput

generateYamlFile :: FilePath -> L.MultiOsRawLayout -> IO ()
generateYamlFile path layout
  =  createDirectoryStructure path
  *> Yaml.encodeFileWith yamlOptions path' layout'
  where path' = path </> system_name <.> "yaml"
        system_name = escapeFilename' . L._systemName . L._metadata $ layout
        layout' = toYamlLayout layout

generateYaml :: L.MultiOsRawLayout -> BS.ByteString
generateYaml = Yaml.encodeWith yamlOptions . toYamlLayout

yamlOptions :: Yaml.EncodeOptions
yamlOptions = Yaml.setStringStyle stringStyle Yaml.defaultEncodeOptions
  where
    stringStyle t = case Yaml.defaultStringStyle t of
      s@(LibYaml.NoTag, LibYaml.PlainNoTag) -> case T.uncons t of
        Just (c, "") -> if is_invisible_char c
          then (LibYaml.NoTag, LibYaml.SingleQuoted)
          else s
        Just _ -> if is_invisible_char (T.last t)
          then (LibYaml.NoTag, LibYaml.SingleQuoted)
          else s
        _ -> s
      s -> s
    is_invisible_char c = not (isPrint c) || isMark c || isSpace c

-- | Create a directory structure.
createDirectoryStructure :: FilePath -> IO ()
createDirectoryStructure = createDirectoryIfMissing True
