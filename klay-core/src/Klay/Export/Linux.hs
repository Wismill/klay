module Klay.Export.Linux
  ( LinuxFormat(..)
  , generateXkb
  ) where

import Klay.Export.Linux.XKB (generateXkb)

data LinuxFormat
  = XKB
  deriving (Eq, Ord, Enum, Bounded, Show)
