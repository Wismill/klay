module Klay.Export.Text.ANSI
  ( DisplayGroup
  , DisplayGroupLayers
  , DisplayGroupLayer
  , DisplayLayer
  , Position(..)
  , displayAnsiGroup
  , displayMiniAnsiLayer
  , displayAnsiLayers
  , selectIsoGroupLevels
  , center
  , padR
  , length'
  ) where

import Data.Maybe (fromMaybe)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.List (intersperse)
import Data.Char (isMark)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Control.Monad ((>=>))

import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (Actions(..), ActionLabeler, KeyLayer, actionAtLevel)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.OS (OsSpecific)


--- Mini ANSI -----------------------------------------------------------------

type DisplayGroupLayer = ActionLabeler -> Level -> KeyFinalGroup -> TL.Text
type DisplayLayer = ActionLabeler -> KeyLayer -> TL.Text

{-|
@
  ┌───┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬──────┐
  │ ` │1│2│3│4│5│6│7│8│9│0│-│=│   ⌫  │
  ├───┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬─────┤
  │  ↹ │q│w│e│r│t│y│u│i│o│p│[│]│  \  │
  ├────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴─────┤
  │  ⮸  │a│s│d│f│g│h│j│k│l│;│'│  ⏎   │
  ├───┬─┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴──────┤
  │ ⇧ │ \│z│x│c│v│b│n│m│,│.│/│   ⇧   │
  ├───┼──┼─┴┬┴─┴─┴─┴─┴─┴┬┴─┼─┴┬──┬───┤
  │ ⎈ │ ◆│ ⎇│     ␣     │ ⎇│ ◆│ ▤│ ⎈ │
  └───┴──┴──┴───────────┴──┴──┴──┴───┘
@
-}
displayMiniAnsiLayer :: ActionLabeler -> KeyLayer -> TL.Text
displayMiniAnsiLayer labeler layer = TB.toLazyText $ mconcat
  [ "┌───┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬──────┐\n"
  , "│", grave, "│", rowE, "│", backspace, "│\n"
  , "├───┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬─────┤\n"
  , "│", tab, "│", rowD, "│", backslash, "│\n"
  , "├────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴─────┤\n"
  , "│", caps, "│", rowC, "│", return', "│\n"
  , "├──────┬─┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴──────┤\n"
  , "│", lshift, "│", rowB, "│", rshift, "│\n"
  , "├───┬──┼─┴┬┴─┴─┴─┴─┴─┴┬┴─┼─┴┬──┬───┤\n"
  , "│", lctrl, "│", lsuper, "│", lalt, "│", space, "│", ralt, "│", rsuper, "│", menu, "│", rctrl, "│\n"
  , "└───┴──┴──┴───────────┴──┴──┴──┴───┘"
  ]
  where
    rowE = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    rowD = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    rowC = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote]
    rowB = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash]
    grave = center' 3 . actionLabel $ K.Grave
    backspace = center' 6 . actionLabel $ K.Backspace
    tab = center' 4 . actionLabel $ K.Tabulator
    backslash = center' 5 . actionLabel $ K.Backslash
    return' = center' 6 . actionLabel $ K.Return
    caps = center' 5 . actionLabel $ K.CapsLock
    lshift = center' 6 . actionLabel $ K.LShift
    rshift = center' 7 . actionLabel $ K.RShift
    lctrl = center' 3 . actionLabel $ K.LControl
    lsuper = center' 2 . actionLabel $ K.LSuper
    lalt = center' 2 . actionLabel $ K.LAlternate
    space = center' 11 . actionLabel $ K.Space
    ralt = center' 2 . actionLabel $ K.RAlternate
    rsuper = center' 2 . actionLabel $ K.RSuper
    menu = center' 2 . actionLabel $ K.Menu
    rctrl = center' 3 . actionLabel $ K.RControl
    actionLabel = fromMaybe " " . ((`Map.lookup` layer) >=> labeler)
    center' n = center n ' '


--- Big -----------------------------------------------------------------------

type DisplayGroup = OsSpecific -> ActionLabeler -> KeyFinalGroup -> TL.Text
type DisplayGroupLayers = OsSpecific -> ([Level], [Level]) -> ActionLabeler -> ActionsKeyMap -> TL.Text

displayAnsiGroup :: DisplayGroup
displayAnsiGroup os labeler g =
  let levels = selectIsoGroupLevels g
  in displayAnsiLayers os levels labeler (_groupMapping g)

selectIsoGroupLevels :: KeyFinalGroup -> ([Level], [Level])
selectIsoGroupLevels g
  | hasLevel5 = ( [M.isoLevel2, M.isoLevel4, M.isoLevel6]
                , [M.isoLevel1, M.isoLevel3, M.isoLevel5] )
  | hasLevel3 = ( [M.isoLevel2, M.isoLevel4]
                , [M.isoLevel1, M.isoLevel3] )
  | otherwise = ( [M.isoLevel2]
                , [M.isoLevel1] )
  where
    ls = Map.keysSet (_groupLevels g)
    hasLevel3 = Set.member M.isoLevel3 ls
    hasLevel5 = Set.member M.isoLevel5 ls

data Position = Top | Bottom

displayAnsiLayers :: DisplayGroupLayers
displayAnsiLayers _ levels labeler am = TB.toLazyText $ mconcat
  [ "┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────────┐\n"
  , "│", rowE Top, "│\n"
  , "│", rowE Bottom, "│\n"
  , "├─────┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──────┤\n"
  , "│", rowD Top, "│\n"
  , "│", rowD Bottom, "│\n"
  , "├────────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴──────┤\n"
  , "│", rowC Top, "│\n"
  , "│", rowC Bottom, "│\n"
  , "├─────────┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴───────────┤\n"
  , "│", rowB Top, "│\n"
  , "│", rowB Bottom, "│\n"
  , "├──────┬─────┴┬────┴─┬───┴─────┴─────┴─────┴─────┴─────┴────┬┴─────┼─────┴┬──────┬──────┤\n"
  , "│", rowA Top, "│\n"
  , "│", rowA Bottom, "│\n"
  , "└──────┴──────┴──────┴──────────────────────────────────────┴──────┴──────┴──────┴──────┘"
  ]
  where
    (levelsTop, levelsBottom) = levels
    labels pos key = case Map.lookup key am of
      Nothing -> mempty
      Just (SingleAction a) -> case pos of
        Top    -> label a
        Bottom -> mempty
      Just as -> mconcat . intersperse " " . fmap (`labelL` as) $ case pos of
        Top    -> levelsTop
        Bottom -> levelsBottom
    labelL l = label . (`actionAtLevel` l)
    label = fromMaybe " " . labeler
    rowE pos = mconcat [rowE' pos, "│", backspace pos]
    rowE' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Grave, K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    rowD pos = mconcat [tab pos, "│", rowD' pos, "│", backslash pos]
    rowD' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    rowC pos = mconcat [caps pos, "│", rowC' pos, "│", return' pos]
    rowC' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote]
    rowB pos = mconcat [lshift pos, "│", rowB' pos, "│", rshift pos]
    rowB' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash]
    rowA pos = mconcat [lctrl pos, "│", lsuper pos, "│", lalt pos, "│", space pos, "│", ralt pos, "│", menu pos, "│", rsuper pos, "│", rctrl pos]
    backspace pos = center' 9 . labels pos $ K.Backspace
    tab pos = center' 8 . labels pos $ K.Tabulator
    backslash pos = center' 6 . labels pos $ K.Backslash
    return' pos = center' 11 . labels pos $ K.Return
    caps pos = center' 9 . labels pos $ K.CapsLock
    lshift pos = center' 12 . labels pos $ K.LShift
    rshift pos = center' 14 . labels pos $ K.RShift
    lctrl pos = center' 6 . labels pos $ K.LControl
    lsuper pos = center' 6 . labels pos $ K.LSuper
    lalt pos = center' 6 . labels pos $ K.LAlternate
    space pos = center' 38 . labels pos $ K.Space
    ralt pos = center' 6 . labels pos $ K.RAlternate
    rsuper pos = center' 6 . labels pos $ K.RSuper
    menu pos = center' 6 . labels pos $ K.Menu
    rctrl pos = center' 6 . labels pos $ K.RControl
    center' n = center n ' '


--- Utils ---------------------------------------------------------------------


padR :: Int -> Char -> String -> TB.Builder
padR n c t =
  let len = length' t
      d | len > n   = 0
        | otherwise = n - len
  in TB.fromString $ t <> replicate d c

center :: Int -> Char -> String -> TB.Builder
center n c t =
  let len = length' t
      (d, m) | len > n   = (0, 0)
            | otherwise = divMod (n - len) 2
  in TB.fromString $ mconcat [replicate d c, t, replicate (d + m) c]

length' :: String -> Int
length' = length . filter (not . isMark)
