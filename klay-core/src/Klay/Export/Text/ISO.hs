module Klay.Export.Text.ISO
  ( displayActionMap
  , displayGroups
  , displayGroup
  , displayGroupLevel
  , display

  , displayMiniIsoGroups
  , displayMiniIsoGroupLayer
  , displayMiniIsoLayer

  , displayIsoGroups
  , displayIsoGroup
  , displayIsoLayers
  , selectIsoGroupLevels
  ) where

import Data.Maybe (fromMaybe)
import Data.Set qualified as Set
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.List (sortBy, groupBy, intersperse)
import Data.Foldable (foldl')
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.String (fromString)
import Control.Applicative (liftA2)
import Control.Monad ((>=>))
import Control.Arrow ((&&&))

import Klay.Export.Text.ANSI
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.ButtonAction (ButtonAction(..), ButtonActionsMap, composeKeyMap)
import Klay.Keyboard.Hardware.KeyPosition
import Klay.Keyboard.Hardware.KeyPosition.ISO qualified as ISO
import Klay.Keyboard.Hardware.Divisions.ISO qualified as ISO
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (Actions(..), ActionLabeler, KeyLayer, actionAtLevel)
import Klay.Keyboard.Layout.Group
import Klay.OS (OsSpecific)
import Klay.Utils.UserInput (RawLText, escapeLine)

displayGroups
  :: OsSpecific
  -> MultiOsRawLayout
  -> Groups (TL.Text, [(TL.Text, TL.Text)])
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayGroups os layout@Layout{_groups=gs} = do
  (groupName &&& displayG) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    label = actionLabeler os . _options $ layout
    ka = _dualFunctionKeys layout

    displayG :: KeyFinalGroup -> [(TL.Text, TL.Text)]
    displayG g = displayL g <$> groupLevelsNames g

    displayL :: KeyFinalGroup -> (Level, RawLText) -> (TL.Text, TL.Text)
    displayL g (l, n) = (escapeLine n, displayGroupLevel label ka l g)

displayGroup :: ActionLabeler -> ButtonActionsMap Key -> KeyFinalGroup -> TL.Text
displayGroup actionLabel ka = displayGroupLevel actionLabel ka minBound

displayGroupLevel :: ActionLabeler -> ButtonActionsMap Key -> Level -> KeyFinalGroup -> TL.Text
displayGroupLevel actionLabel ka l = displayActionMap actionLabel . (`composeKeyMap` ka) . groupImplicitLayer l

displayActionMap :: ActionLabeler -> ButtonActionsMap Action -> TL.Text
displayActionMap actionLabel = display . fmap show_key
  where
    show_key :: ButtonAction Action -> String
    show_key (NormalKey a) = fromMaybe mempty $ actionLabel a
    show_key (DualKey a1 a2 _ _ _) =
      fromMaybe mempty $ actionLabel a1 <>> Just "/" <>> actionLabel a2
    (<>>) = liftA2 (<>)

display :: Map Key String -> TL.Text
display m = TB.toLazyText . mconcat . intersperse "\n" $ fmap display_row rows
  where
    rows :: [[(Key, KeyPosition)]]
    rows = reverse . groupBy (\(_, pos1) (_, pos2) -> sameRow pos1 pos2) $ keyPositions
    display_row :: [(Key, KeyPosition)] -> TB.Builder
    display_row r = mconcat . intersperse "\n" $ [ts <> "─┐", ms, bs <> "─┘"]
      where
        (ts, ms, bs) = snd $ foldl' add_key (Nothing, (mempty, mempty, mempty)) r
    add_key :: (Maybe Int, (TB.Builder, TB.Builder, TB.Builder)) -> (Key, KeyPosition) -> (Maybe Int, (TB.Builder, TB.Builder, TB.Builder))
    add_key (Nothing, (ts, ms, bs)) (key, KeyPosition _ (Column c)) =
      ( Just c,
        ( mconcat [ts, space (4 * (c - leftmost_column) + 1), "┌──"]
        , mconcat [ms, space (4 * (c - leftmost_column) + 1), "| ", show_key key, " |"]
        , mconcat [bs, space (4 * (c - leftmost_column) + 1), "└──"]
        )
      )
    add_key (Just c', (ts, ms, bs)) (key, KeyPosition _ (Column c))
      | c == succ c' =
        ( Just c,
          ( ts <> "─┬──"
          , mconcat [ms, " ", show_key key, " |"]
          , bs <> "─┴──"
          )
        )
      | otherwise =
        ( Just c,
          ( mconcat [ts, "─┐", space (4 * (c - c') - 5), "┌──"]
          , mconcat [ms, space (4 * (c - c') - 5), "| ", show_key key, " |"]
          , mconcat [bs, "─┘", space (4 * (c - c') - 5), "└──"]
          )
        )
    space n = fromString (replicate n ' ')
    show_key :: Key -> TB.Builder
    show_key key = case Map.lookup key m of
      Nothing -> " "
      Just "" -> " "
      Just l  -> fromString l
    leftmost_column :: Int
    leftmost_column =
      let (Column c) = minimum . foldr f mempty $ keyPositions
      in c
      where f (_, KeyPosition _ c) cs = c:cs
    keyPositions :: [(Key, KeyPosition)]
    keyPositions = sortBy comparePos . fmap mkKeyPos . Set.toList $ keys
    comparePos :: (Key, KeyPosition) -> (Key, KeyPosition) -> Ordering
    comparePos (_, KeyPosition r1 c1) (_, KeyPosition r2 c2) = case compare r1 r2 of
      EQ -> compare c1 c2
      o  -> o
    keys :: Set.Set Key
    keys = H._alphanumericSection ISO.keyboardSections
    mkKeyPos :: Key -> (Key, KeyPosition)
    mkKeyPos key = (key, pos) -- [FIXME] partial function
      where
        pos = fromMaybe err . ISO.toKeyPosition $ key
        err = error $ "[ERROR] Key position not defined: " <> show key


--- Mini ----------------------------------------------------------------------

displayMiniIsoGroups
  :: OsSpecific
  -> MultiOsRawLayout
  -> Groups (TL.Text, [(TL.Text, TL.Text)])
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayMiniIsoGroups os layout@Layout{_groups=gs} = do
  (groupName &&& displayG) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    label = actionLabeler os . _options $ layout

    displayG :: KeyFinalGroup -> [(TL.Text, TL.Text)]
    displayG g = displayL g <$> groupLevelsNames g

    displayL :: KeyFinalGroup -> (Level, RawLText) -> (TL.Text, TL.Text)
    displayL g (l, n) = (escapeLine n, displayMiniIsoGroupLayer label l g)

displayMiniIsoGroupLayer :: ActionLabeler -> Level -> KeyFinalGroup -> TL.Text
displayMiniIsoGroupLayer labeler l = displayMiniIsoLayer labeler . groupImplicitLayer l

{-|
@
  ┌───┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬──────┐
  │ ` │1│2│3│4│5│6│7│8│9│0│-│=│   ⌫  │
  ├───┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬─────┤
  │  ↹ │q│w│e│r│t│y│u│i│o│p│[│]│  ⏎  │
  ├────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴─┐   │
  │  ⮸  │a│s│d│f│g│h│j│k│l│;│'│ \│   │
  ├───┬─┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴──┴───┤
  │ ⇧ │ \│z│x│c│v│b│n│m│,│.│/│   ⇧   │
  ├───┼──┼─┴┬┴─┴─┴─┴─┴─┴┬┴─┼─┴┬──┬───┤
  │ ⎈ │ ◆│ ⎇│     ␣     │ ⎇│ ◆│ ▤│ ⎈ │
  └───┴──┴──┴───────────┴──┴──┴──┴───┘
@
-}
displayMiniIsoLayer :: ActionLabeler -> KeyLayer -> TL.Text
displayMiniIsoLayer labeler layer = TB.toLazyText $ mconcat
  [ "┌───┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬──────┐\n"
  , "│", grave, "│", rowE, "│", backspace, "│\n"
  , "├───┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬─────┤\n"
  , "│", tab, "│", rowD, "│", return', "│\n"
  , "├────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴─┐   │\n"
  , "│", caps, "│", rowC, "│", backslash, "│   │\n"
  , "├───┬─┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴──┴───┤\n"
  , "│", lshift, "│", iso102, "│", rowB, "│", rshift, "│\n"
  , "├───┼──┼─┴┬┴─┴─┴─┴─┴─┴┬┴─┼─┴┬──┬───┤\n"
  , "│", lctrl, "│", lsuper, "│", lalt, "│", space, "│", ralt, "│", rsuper, "│", menu, "│", rctrl, "│\n"
  , "└───┴──┴──┴───────────┴──┴──┴──┴───┘"
  ]
  where
    rowE = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    rowD = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    rowC = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote]
    rowB = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash]
    grave = center' 3 . actionLabel $ K.Grave
    backspace = center' 6 . actionLabel $ K.Backspace
    tab = center' 4 . actionLabel $ K.Tabulator
    backslash = center' 2 . actionLabel $ K.Backslash
    return' = center' 5 . actionLabel $ K.Return
    caps = center' 5 . actionLabel $ K.CapsLock
    lshift = center' 3 . actionLabel $ K.LShift
    iso102 = center' 2 . actionLabel $ K.Iso102
    rshift = center' 7 . actionLabel $ K.RShift
    lctrl = center' 3 . actionLabel $ K.LControl
    lsuper = center' 2 . actionLabel $ K.LSuper
    lalt = center' 2 . actionLabel $ K.LAlternate
    space = center' 11 . actionLabel $ K.Space
    ralt = center' 2 . actionLabel $ K.RAlternate
    rsuper = center' 2 . actionLabel $ K.RSuper
    menu = center' 2 . actionLabel $ K.Menu
    rctrl = center' 3 . actionLabel $ K.RControl
    actionLabel = fromMaybe " " . ((`Map.lookup` layer) >=> labeler)
    center' n = center n ' '

--- Big -----------------------------------------------------------------------

displayIsoGroups
  :: OsSpecific
  -> MultiOsRawLayout
  -> Groups (TL.Text, TL.Text)
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayIsoGroups os layout@Layout{_groups=gs} =
  (groupName &&& displayIsoGroup os labeler) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    labeler = actionLabeler os . _options $ layout

displayIsoGroup :: DisplayGroup
displayIsoGroup os labeler g =
  let levels = selectIsoGroupLevels g
  in displayIsoLayers os levels labeler (_groupMapping g)

displayIsoLayers :: DisplayGroupLayers
displayIsoLayers _ levels labeler am = TB.toLazyText $ mconcat
  [ "┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────────┐\n"
  , "│", rowE Top, "│\n"
  , "│", rowE Bottom, "│\n"
  , "├─────┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──────┤\n"
  , "│", rowD Top, "│\n"
  , "│", rowD Bottom, "│\n"
  , "├────────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┐     │\n"
  , "│", rowC Top, "│     │\n"
  , "│", rowC Bottom, "│     │\n"
  , "├──────┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴─────┴─────┤\n"
  , "│", rowB Top, "│\n"
  , "│", rowB Bottom, "│\n"
  , "├──────┼─────┴┬────┴─┬───┴─────┴─────┴─────┴─────┴─────┴────┬┴─────┼─────┴┬──────┬──────┤\n"
  , "│", rowA Top, "│\n"
  , "│", rowA Bottom, "│\n"
  , "└──────┴──────┴──────┴──────────────────────────────────────┴──────┴──────┴──────┴──────┘"
  ]
  where
    (levelsTop, levelsBottom) = levels
    labels pos key = case Map.lookup key am of
      Nothing -> mempty
      Just (SingleAction a) -> case pos of
        Top    -> label a
        Bottom -> mempty
      Just as -> mconcat . intersperse " " . fmap (`labelL` as) $ case pos of
        Top    -> levelsTop
        Bottom -> levelsBottom
    labelL l = label . (`actionAtLevel` l)
    label = fromMaybe " " . labeler
    rowE pos = mconcat [rowE' pos, "│", backspace pos]
    rowE' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Grave, K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    rowD pos = mconcat [tab pos, "│", rowD' pos, "│", return' pos]
    rowD' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    rowC pos = mconcat [caps pos, "│", rowC' pos]
    rowC' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote, K.Backslash]
    rowB pos = mconcat [lshift pos, "│", rowB' pos, "│", rshift pos]
    rowB' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Iso102, K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash]
    rowA pos = mconcat [lctrl pos, "│", lsuper pos, "│", lalt pos, "│", space pos, "│", ralt pos, "│", menu pos, "│", rsuper pos, "│", rctrl pos]
    backspace pos = center' 9 . labels pos $ K.Backspace
    tab pos = center' 8 . labels pos $ K.Tabulator
    return' pos = center' 6 . labels pos $ K.Return
    caps pos = center' 9 . labels pos $ K.CapsLock
    lshift pos = center' 6 . labels pos $ K.LShift
    rshift pos = center' 14 . labels pos $ K.RShift
    lctrl pos = center' 6 . labels pos $ K.LControl
    lsuper pos = center' 6 . labels pos $ K.LSuper
    lalt pos = center' 6 . labels pos $ K.LAlternate
    space pos = center' 38 . labels pos $ K.Space
    ralt pos = center' 6 . labels pos $ K.RAlternate
    rsuper pos = center' 6 . labels pos $ K.RSuper
    menu pos = center' 6 . labels pos $ K.Menu
    rctrl pos = center' 6 . labels pos $ K.RControl
    center' n = center n ' '
