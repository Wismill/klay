module Klay.Export.Text.Common
  ( displayBigGroups
  , displayBigGroup
  , displayMiniGroups
  , mkDisplayMiniGroupLayer
  , keycapLegend
  , keycapLegendHtml
  , displayIsoGroupLegend
  , ISO.displayIsoGroup
  , ISO.displayMiniIsoGroupLayer
  , ISO.selectIsoGroupLevels
  ) where


import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Data.List (intersperse)
import Data.Map.Strict qualified as Map
import Data.Functor ((<&>))
import Control.Arrow ((&&&))

import Data.Semialign (padZip)

import Klay.Export.Text.ANSI (DisplayGroup, DisplayGroupLayers, DisplayGroupLayer, DisplayLayer)
import Klay.Export.Text.ANSI qualified as ANSI
import Klay.Export.Text.ABNT qualified as ABNT
import Klay.Export.Text.ISO qualified as ISO
import Klay.Export.Text.JIS qualified as JIS
import Klay.Export.Text.TypeMatrix qualified as TM
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (ActionLabeler)
import Klay.Keyboard.Layout.Group
import Klay.OS (OsSpecific)
import Klay.Utils.UserInput (RawLText, escapeLine)
import Klay.Utils.Html (escapeText)

displayBigGroups
  :: OsSpecific
  -> H.Geometry
  -> MultiOsRawLayout
  -> Maybe (Groups (TL.Text, TL.Text))
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayBigGroups os geo layout@Layout{_groups=gs} =
  mkDisplayBigGroup geo <&> \d ->
    (groupName &&& d os labeler) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    labeler = actionLabeler os . _options $ layout

displayBigGroup :: H.Geometry -> OsSpecific -> ActionLabeler -> KeyFinalGroup -> Maybe TL.Text
displayBigGroup geo os labeler g = mkDisplayBigLayers geo <&> \d ->
  let levels = ANSI.selectIsoGroupLevels g
  in d os levels labeler (_groupMapping g)

mkDisplayBigGroup :: H.Geometry -> Maybe DisplayGroup
mkDisplayBigGroup geo =
  case geo of
    H.ANSI        _ -> Just ANSI.displayAnsiGroup
    H.ABNT        _ -> Just ABNT.displayAbntGroup
    H.ISO         _ -> Just ISO.displayIsoGroup
    H.JIS         _ -> Just JIS.displayJisGroup
    H.TypeMatrix  m -> Just (TM.displayTypematrix2030Group m)

mkDisplayBigLayers :: H.Geometry -> Maybe DisplayGroupLayers
mkDisplayBigLayers geo =
  case geo of
    H.ANSI       _ -> Just ANSI.displayAnsiLayers
    H.ABNT       _ -> Just ABNT.displayAbntLayers
    H.ISO        _ -> Just ISO.displayIsoLayers
    H.JIS        _ -> Just JIS.displayJisLayers
    H.TypeMatrix m -> Just (TM.displayTypematrix2030Layers m)

displayMiniGroups
  :: OsSpecific
  -> H.Geometry
  -> MultiOsRawLayout
  -> Maybe (Groups (TL.Text, [(TL.Text, TL.Text)]))
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayMiniGroups os geo layout@Layout{_groups=gs} =
  mkDisplayMiniLayer geo <&> \d ->
    (groupName &&& displayG d) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    labeler = actionLabeler os . _options $ layout

    displayG :: DisplayLayer -> KeyFinalGroup -> [(TL.Text, TL.Text)]
    displayG f g = displayL f g <$> groupLevelsNames g

    displayL :: DisplayLayer -> KeyFinalGroup -> (Level, RawLText) -> (TL.Text, TL.Text)
    displayL f g (l, n) = (escapeLine n, f labeler . groupImplicitLayer l $ g)

mkDisplayMiniGroupLayer :: H.Geometry -> Maybe DisplayGroupLayer
mkDisplayMiniGroupLayer
  = fmap (\d labeler l -> d labeler . groupImplicitLayer l)
  . mkDisplayMiniLayer

mkDisplayMiniLayer :: H.Geometry -> Maybe DisplayLayer
mkDisplayMiniLayer (H.ANSI       _) = Just ANSI.displayMiniAnsiLayer
mkDisplayMiniLayer (H.ABNT       _) = Just ABNT.displayMiniAbntLayer
mkDisplayMiniLayer (H.ISO        _) = Just ISO.displayMiniIsoLayer
mkDisplayMiniLayer (H.JIS        _) = Just JIS.displayMiniJisLayer
mkDisplayMiniLayer (H.TypeMatrix m) = Just (TM.displayMiniTypematrix2030Layer m)

displayIsoGroupLegend :: KeyFinalGroup -> TL.Text
displayIsoGroupLegend g =
  TB.toLazyText (keycapLegendHtml (Just "Levels") True ts' bs')
  where
    (ts, bs) = ANSI.selectIsoGroupLevels g
    ts' = zip (show <$> [(2::Int),4..]) . fmap mkDescription $ ts
    bs' = zip (show <$> [(1::Int),3..]) . fmap mkDescription $ bs
    mkDescription l = TL.unpack . escapeLine
                  $ Map.findWithDefault mempty l (_groupLevels g)

keycapLegend :: [(String, String)] -> [(String, String)] -> TB.Builder
keycapLegend ts bs = mconcat
  [ "┌─────┐\n"
  , "│", mkRow ts, "│", row1, "\n"
  , "│", mkRow bs, "│", row2, "\n"
  , "└─────┘", row3
  ]
  where
    ts' = mkDescription . snd <$> take 3 ts
    bs' = mkDescription . snd <$> take 3 bs
    mkRow = ANSI.center 5 ' '
          . mconcat
          . intersperse " "
          . fmap fst
          . take 3
    mkDescription = ANSI.padR 24 ' '
    (row1, row2, row3) = case zip ts' bs' of
      [(l1,l2)] ->
        ( mempty
        , " 1. " <> l1
        , " 2. " <> l2 )
      [(l1,l2), (l3,l4)] ->
        ( " 1. " <> l1 <> " 4." <> l4
        , " 2. " <> l2
        , " 3. " <> l3 )
      [(l1,l2), (l3,l4), (l5,l6)] ->
        ( " 1. " <> l1 <> " 4. " <> l4
        , " 2. " <> l2 <> " 5. " <> l5
        , " 3. " <> l3 <> " 6. " <> l6 )
      _ -> mempty

keycapLegendHtml :: Maybe TB.Builder -> Bool -> [(String, String)] -> [(String, String)] -> TB.Builder
keycapLegendHtml caption isNumberedList ts bs = mconcat
  [ "<table>\n"
  , caption'
  , "  <thead>\n"
  , "    <tr>\n"
  , "      <th>Keycap</th>\n"
  , "      <th>Legend</th>\n"
  , "    </tr>\n"
  , "  </thead>\n"
  , "  <tbody>\n"
  , "    <tr>\n"
  , "      <td>\n"
  , "         <pre>\n"
  , "┌─────┐\n"
  , "│", mkRow ts, "│\n"
  , "│", mkRow bs, "│\n"
  , "└─────┘\n"
  , "         </pre>\n"
  , "      </td>\n"
  , "      <td>\n"
  , "        <", list_tag, ">\n"
  , xs
  , "        </", list_tag, ">\n"
  , "      </td>\n"
  , "    </tr>\n"
  , "  </tbody>\n"
  , "</table>"
  ]
  where
    caption' = case caption of
      Nothing -> mempty
      Just c  -> mconcat ["  <caption>", c, "</caption>\n"]
    list_tag | isNumberedList = "ol"
             | otherwise      = "ul"
    xs = mconcat
       . foldr (\case
          (Nothing, Nothing) -> id
          (Just b , Nothing) -> (mkItem b :)
          (Nothing, Just t ) -> (mkItem t :)
          (Just b , Just t ) -> (mkItem b :) . (mkItem t :)
          ) mempty
       $ padZip bs ts
    mkItem x = mconcat
      [ "          <li>"
      , TB.fromLazyText . escapeText . TL.pack . snd $ x
      , "</li>\n"]
    mkRow = ANSI.center 5 ' '
          . mconcat
          . intersperse " "
          . fmap (TL.unpack . escapeText . TL.pack . fst)
          . take 3
