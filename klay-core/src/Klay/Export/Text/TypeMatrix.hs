module Klay.Export.Text.TypeMatrix
  ( displayMiniTypematrix2030Layer
  , displayTypematrix2030Layers
  , displayTypematrix2030Group
  ) where

import Data.Maybe (fromMaybe)
import Data.Map.Strict qualified as Map
import Data.List (intersperse)
import Data.Functor ((<&>))
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB

import Klay.Export.Text.ANSI
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Hardware.Key.TypeMatrix2030 qualified as H
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action
  ( Actions(..), TAutoActions(..), ActionLabeler, KeyLayer
  , defaultActions, actionAtLevel)
import Klay.Keyboard.Layout.Group

displayMiniTypematrix2030Layer :: H.TypeMatrixMode -> ActionLabeler -> KeyLayer -> TL.Text
displayMiniTypematrix2030Layer mode labeler layer = TB.toLazyText $ mconcat
  [ "┌─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┐\n"
  , "│", row1, "│\n"
  , "├─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┤\n"
  , "│", row2, "│\n"
  , "├─┼─┼─┼─┼─┼─┼─┤ ├─┼─┼─┼─┼─┼─┼─┤\n"
  , "│", row3L, "│ │", row3R, "│\n"
  , "├─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┼─┤\n"
  , "│", row4, "│\n"
  , "│ ├─┼─┼─┼═┼─┤ ├─┼═┼─┼─┼─┼─┤ ├─┤\n"
  , "│ │", row5L, "│ │", row5R, "│ │", www, "│\n"
  , "├─┼─┼─┼─┼─┴─┴─┴─┴─┼─┼─┼─┼─┼─┼─┤\n"
  , "│", row6L, "│", space, "│", row6R, "│\n"
  , "├─┼─┴┬┴─┼─────────┼─┼─┼─┼─┤ ├─┤\n"
  , "│ │", row7L, "│         │", row7R, "│ │", pageDown, "│\n"
  , "└─┴──┴──┘         └─┴─┴─┴─┴─┴─┘"
  ]
  where
    labeler' key = case K.toBaseFunction (H.tm2030Key key mode) of
      K.OneKey (K.Mappable key') ->
        fromMaybe " " $ Map.lookup key' layer >>= labeler <&> labeler''
      K.OneKey H.Shuffle -> "\xE23C"
      K.OneKey H.Desktop -> "\xF044"
      _         -> " "
    labeler'' a | length' a > 1 = "␥"
                | otherwise     = a
    row1  = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.Escape, K.F1, K.F2, K.F3, K.F4, K.F5, K.Delete, K.F6, K.F7, K.F8, K.F9, K.F10, K.F11, K.F12, K.NumLock]
    row2  = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.Grave, K.N1, K.N2, K.N3, K.N4, K.N5, K.Backspace, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals, K.Calculator]
    row3L = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.Tabulator, K.Q, K.W, K.E, K.R, K.T]
    row3R = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket, K.Email1]
    row4  = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.LShift, K.A, K.S, K.D, K.F, K.G, K.Return, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote, K.RShift, K.CapsLock]
    row5L = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.Z, K.X, K.C, K.V, K.B]
    row5R = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.N, K.M, K.Comma, K.Period, K.Slash, K.Backslash]
    row6L = mconcat . intersperse "│" $ TB.fromString . labeler' <$> [H.Mappable K.LControl, H.Mappable K.MediaPlayPause, H.Mappable K.Menu, H.Shuffle]
    row6R = mconcat . intersperse "│" $ TB.fromString . labeler' <$> [H.Desktop, H.Mappable K.Home, H.Mappable K.CursorUp, H.Mappable K.End, H.Mappable K.RControl, H.Mappable K.PageUp]
    row7L = mconcat . intersperse "│" $ center' 2 . labeler' <$> [H.Mappable K.LSuper, H.Mappable K.LAlternate]
    row7R = mconcat . intersperse "│" $ TB.fromString . labeler' . H.Mappable <$> [K.RAlternate, K.CursorLeft, K.CursorDown, K.CursorRight]
    space = center' 9 . labeler' $ H.Mappable K.Space
    www = TB.fromString . labeler' $ H.Mappable K.Browser
    pageDown = TB.fromString . labeler' $ H.Mappable K.PageDown
    center' n = center n ' '

--- Big -----------------------------------------------------------------------

displayTypematrix2030Group :: H.TypeMatrixMode -> DisplayGroup
displayTypematrix2030Group m os labeler g =
  let levels = selectIsoGroupLevels g
  in displayTypematrix2030Layers m os levels labeler (_groupMapping g)

displayTypematrix2030Layers :: H.TypeMatrixMode -> DisplayGroupLayers
displayTypematrix2030Layers mode os levels labeler am = TB.toLazyText $ mconcat
  [ "╔═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╗\n"
  , "║", row1, "║\n"
  , "╚═════╩═════╩═════╩═════╩═════╩═════╣", delete Bottom, "╠═════╩═════╩═════╩═════╩═════╩═════╩═════╬═════╣\n"
  , "│", row2L Top   , "╠═════╣"            , row2R Top   , "║", calc Top   , "║\n"
  , "│", row2L Bottom, "║", backspace Top, "║", row2R Bottom, "║", calc Bottom, "║\n"
  , "╔═════╗─────┼─────┼─────┼─────┼─────║", backspace Bottom, "║─────┼─────┼─────┼─────┼─────┼─────┼─────╠═════╣\n"
  , "║", tab Top   , "║", row3L Top   , "║     ║", row3R Top   , "║", mail Top   , "║\n"
  , "║", tab Bottom, "║", row3L Bottom, "║     ║", row3R Bottom, "║", mail Bottom, "║\n"
  , "╠═════╣─────┼─────┼─────┼─────┼─────╠═════╣─────┼─────┼─────┼─────┼─────┼─────╔═════╬═════╣\n"
  , "║", lshift Top   , "║", row4L Top   , "║", return' Top   , "║", row4R Top   , "║", rshift Top   , "║", caps Top   , "║\n"
  , "║", lshift Bottom, "║", row4L Bottom, "║", return' Bottom, "║", row4R Bottom, "║", rshift Bottom, "║", caps Bottom, "║\n"
  , "║     ║─────┼─────┼─────┼─═══─┼─────║     ║─────┼─═══─┼─────┼─────┼─────┼─────║     ╠═════╣\n"
  , "║     ║", row5L Top   ,       "║     ║", row5R Top   ,            "║     ║", www Top   , "║\n"
  , "║     ║", row5L Bottom,       "║     ║", row5R Bottom,            "║     ║", www Bottom, "║\n"
  , "╠═════╬═════╦═════╦═════╦═════╧═════╩═════╩═════╧═════╦═════╦═════╦═════╦═════╬═════╬═════╣\n"
  , "║", row6L Top   , "║", space Top         , "║", row6R Top   , "║\n"
  , "║", row6L Bottom, "║                             ║", row6R Bottom, "║\n"
  , "╠═════╬═════╩══╦══╩═════╣", space Bottom,       "╠═════╬═════╬═════╬═════╣     ╠═════╣\n"
  , "║ Fn  ║", lsuper Top   , "║", lalt Top   , "║                             ║", row7R Top        , "║     ║", pageDown Top   , "║\n"
  , "║     ║", lsuper Bottom, "║", lalt Bottom, "╠═════════════════════════════╣", row7R Bottom, "║     ║", pageDown Bottom, "║\n"
  , "╚═════╩════════╩════════╝                             ╚═════╩═════╩═════╩═════╩═════╩═════╝\n"

  ]
  where
    (levelsTop, levelsBottom) = levels
    labels pos key = case K.toBaseFunction (H.tm2030Key key mode) of
      K.OneKey (K.Mappable key') ->
        labels' pos $ Map.findWithDefault (_aActions $ defaultActions key' os) key' am
      K.OneKey H.Shuffle -> case pos of
        Top    -> "shuf"
        Bottom -> mempty
      K.OneKey H.Desktop -> case pos of
        Top    -> "desk"
        Bottom -> mempty
      _ -> mempty
    labels' pos = \case
      SingleAction a -> case pos of
        Top    -> label a
        Bottom -> mempty
      as -> mconcat . intersperse " " . fmap (`labelL` as) $ case pos of
        Top    -> levelsTop
        Bottom -> levelsBottom
    labelL l = label . (`actionAtLevel` l)
    label a = fromMaybe " " . labeler $ a
    row1 = mconcat . intersperse "│" $ center' 5 . labels Top      . H.Mappable <$> [K.Escape, K.F1, K.F2, K.F3, K.F4, K.F5, K.Delete, K.F6, K.F7, K.F8, K.F9, K.F10, K.F11, K.F12, K.NumLock]
    row2L pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.Grave, K.N1, K.N2, K.N3, K.N4, K.N5]
    row2R pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    row3L pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.Q, K.W, K.E, K.R, K.T]
    row3R pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    row4L pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.A, K.S, K.D, K.F, K.G]
    row4R pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.H, K.J, K.K, K.L, K.Semicolon, K.Quote]
    row5L pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.Z, K.X, K.C, K.V, K.B]
    row5R pos = mconcat . intersperse "│" $ center' 5 . labels pos . K.Mappable <$> [K.N, K.M, K.Comma, K.Period, K.Slash, K.Backslash]
    row6L pos = mconcat . intersperse "║" $ center' 5 . labels pos <$> [H.Mappable K.LControl, H.Mappable K.MediaPlayPause, H.Mappable K.Menu, H.Shuffle]
    row6R pos = mconcat . intersperse "║" $ center' 5 . labels pos <$> [H.Desktop, H.Mappable K.Home, H.Mappable K.CursorUp, H.Mappable K.End, H.Mappable K.RControl, H.Mappable K.PageUp]
    row7R pos = mconcat . intersperse "║" $ center' 5 . labels pos . H.Mappable <$> [K.RAlternate, K.CursorLeft, K.CursorDown, K.CursorRight]
    backspace pos = center' 5 . labels pos $ H.Mappable K.Backspace
    tab pos = center' 5 . labels pos $ H.Mappable K.Tabulator
    return' pos = center' 5 . labels pos $ H.Mappable K.Return
    delete pos = center' 5 . labels pos $ H.Mappable K.Delete
    caps pos = center' 5 . labels pos $ H.Mappable K.CapsLock
    lshift pos = center' 5 . labels pos $ H.Mappable K.LShift
    rshift pos = center' 5 . labels pos $ H.Mappable K.RShift
    -- suffle pos = center' 4 . labels pos $ H.TM2030Shuffle
    -- desktop pos = center' 4 . labels pos $ H.TM2030Desktop
    lsuper pos = center' 8 . labels pos $ H.Mappable K.LSuper
    lalt pos = center' 8 . labels pos $ H.Mappable K.LAlternate
    space pos = center' 29 . labels pos $ H.Mappable K.Space
    calc pos = center' 5 . labels pos $ H.Mappable K.Calculator
    mail pos = center' 5 . labels pos $ H.Mappable K.Email1
    www pos = center' 5 . labels pos $ H.Mappable K.Browser
    pageDown pos = center' 5 . labels pos $ H.Mappable K.PageDown
    center' n = center n ' '
