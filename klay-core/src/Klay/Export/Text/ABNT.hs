module Klay.Export.Text.ABNT
  ( displayMiniAbntGroups
  , displayMiniAbntGroupLayer
  , displayMiniAbntLayer

  , displayAbntGroups
  , displayAbntGroup
  , displayAbntLayers
  ) where

import Data.Maybe (fromMaybe)
import Data.Map.Strict qualified as Map
import Data.List (intersperse)
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Control.Monad ((>=>))
import Control.Arrow ((&&&))

import Klay.Export.Text.ANSI
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (Actions(..), ActionLabeler, KeyLayer, actionAtLevel)
import Klay.Keyboard.Layout.Group
import Klay.OS (OsSpecific)
import Klay.Utils.UserInput (RawLText, escapeLine)


--- Mini ----------------------------------------------------------------------

displayMiniAbntGroups
  :: OsSpecific
  -> MultiOsRawLayout
  -> Groups (TL.Text, [(TL.Text, TL.Text)])
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayMiniAbntGroups os layout@Layout{_groups=gs} = do
  (groupName &&& displayG) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    label = actionLabeler os . _options $ layout

    displayG :: KeyFinalGroup -> [(TL.Text, TL.Text)]
    displayG g = displayL g <$> groupLevelsNames g

    displayL :: KeyFinalGroup -> (Level, RawLText) -> (TL.Text, TL.Text)
    displayL g (l, n) = (escapeLine n, displayMiniAbntGroupLayer label l g)

displayMiniAbntGroupLayer :: ActionLabeler -> Level -> KeyFinalGroup -> TL.Text
displayMiniAbntGroupLayer labeler l = displayMiniAbntLayer labeler . groupImplicitLayer l

{-|
@
  ┌───┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬────┐
  │ ` │1│2│3│4│5│6│7│8│9│0│-│=│ │ ⌫  │
  ├───┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴────┤
  │ ↹  │q│w│e│r│t│y│u│i│o│p│[│]│  ⏎  │
  ├────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴─┐   │
  │  ⮸  │a│s│d│f│g│h│j│k│l│;│'│\ │   │
  ├─────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴──┴───┤
  │  ⇧   │z│x│c│v│b│n│m│,│.│/│   ⇧   │
  ├──┬──┬┴─┼─┴┬┴─┴─┼─┴┬┴─┼─┴┬┴─┬──┬──┤
  │⎈ │◆ │⎇ │  │ ␣  │  │  │⎇ │◆ │▤ │⎈ │
  └──┴──┴──┴──┴────┴──┴──┴──┴──┴──┴──┘
@
-}
displayMiniAbntLayer :: ActionLabeler -> KeyLayer -> TL.Text
displayMiniAbntLayer labeler layer = TB.toLazyText $ mconcat
  [ "┌───┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬─┬──────┐\n"
  , "│", grave, "│", rowE, "│", backspace, "│\n"
  , "├───┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬─────┤\n"
  , "│", tab, "│", rowD, "│", return', "│\n"
  , "├────┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴─┐   │\n"
  , "│", caps, "│", rowC, "│", backslash, "│   │\n"
  , "├────┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬┴┬─┴───┤\n"
  , "│", lshift, "│", rowB, "│", rshift, "│\n"
  , "├──┬─┴┬┴─┼─┴┬┴─┴─┼─┴┬┴─┼─┴┬┴─┼──┬──┤\n"
  , "│", rowA1, "│", space, "│", rowA2, "│\n"
  , "└──┴──┴──┴──┴────┴──┴──┴──┴──┴──┴──┘"
  ]
  where
    rowE = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    rowD = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    rowC = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote]
    rowB = TB.fromString . mconcat . intersperse "│" $ actionLabel <$> [K.Iso102, K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash, K.Ro]
    rowA1 = mconcat . intersperse "│" $ center' 2 . actionLabel <$> [K.LControl, K.LSuper, K.LAlternate, K.Muhenkan]
    rowA2 = mconcat . intersperse "│" $ center' 2 . actionLabel <$> [K.Henkan, K.HiraganaKatakana, K.RAlternate, K.RSuper, K.Menu, K.RControl]
    grave = center' 3 . actionLabel $ K.Grave
    backspace = center' 6 . actionLabel $ K.Backspace
    tab = center' 4 . actionLabel $ K.Tabulator
    backslash = center' 2 . actionLabel $ K.Backslash
    return' = center' 5 . actionLabel $ K.Return
    caps = center' 5 . actionLabel $ K.CapsLock
    lshift = center' 4 . actionLabel $ K.LShift
    rshift = center' 5 . actionLabel $ K.RShift
    space = center' 4 . actionLabel $ K.Space
    actionLabel = fromMaybe " " . ((`Map.lookup` layer) >=> labeler)
    center' n = center n ' '

--- Big -----------------------------------------------------------------------

displayAbntGroups
  :: OsSpecific
  -> MultiOsRawLayout
  -> Groups (TL.Text, TL.Text)
  -- ^ Groups (Group name, \[(Level name, layer)\])
displayAbntGroups os layout@Layout{_groups=gs} =
  (groupName &&& displayAbntGroup os labeler) <$> resolveFinalGroups os gs
  where
    groupName = escapeLine . _groupName
    labeler = actionLabeler os . _options $ layout

displayAbntGroup :: DisplayGroup
displayAbntGroup os labeler g =
  let levels = selectIsoGroupLevels g
  in displayAbntLayers os levels labeler (_groupMapping g)

displayAbntLayers :: DisplayGroupLayers
displayAbntLayers _ levels labeler am = TB.toLazyText $ mconcat
  [ "┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬──────────┐\n"
  , "│", rowE Top, "│\n"
  , "│", rowE Bottom, "│\n"
  , "├─────┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬───────┤\n"
  , "│", rowD Top, "│\n"
  , "│", rowD Bottom, "│\n"
  , "├────────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┐      │\n"
  , "│", rowC Top, "│      │\n"
  , "│", rowC Bottom, "│      │\n"
  , "├──────┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──────┤\n"
  , "│", rowB Top, "│\n"
  , "│", rowB Bottom, "│\n"
  , "├──────┼─────┴┬────┴─┬───┴──┬──┴─────┴─────┴───┬─┴────┬┴─────┼─────┴┬────┴─┬───┴──┬──────┤\n"
  , "│", rowA Top, "│\n"
  , "│", rowA Bottom, "│\n"
  , "└──────┴──────┴──────┴──────┴──────────────────┴──────┴──────┴──────┴──────┴──────┴──────┘"
  ]
  where
    (levelsTop, levelsBottom) = levels
    labels pos key = case Map.lookup key am of
      Nothing -> mempty
      Just (SingleAction a) -> case pos of
        Top    -> label a
        Bottom -> mempty
      Just as -> mconcat . intersperse " " . fmap (`labelL` as) $ case pos of
        Top    -> levelsTop
        Bottom -> levelsBottom
    labelL l = label . (`actionAtLevel` l)
    label = fromMaybe " " . labeler
    rowE pos = mconcat [rowE' pos, "│", backspace pos]
    rowE' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Grave, K.N1, K.N2, K.N3, K.N4, K.N5, K.N6, K.N7, K.N8, K.N9, K.N0, K.Minus, K.Equals]
    rowD pos = mconcat [tab pos, "│", rowD' pos, "│", return' pos]
    rowD' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Q, K.W, K.E, K.R, K.T, K.Y, K.U, K.I, K.O, K.P, K.LBracket, K.RBracket]
    rowC pos = mconcat [caps pos, "│", rowC' pos]
    rowC' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.A, K.S, K.D, K.F, K.G, K.H, K.J, K.K, K.L, K.Semicolon, K.Quote, K.Backslash]
    rowB pos = mconcat [lshift pos, "│", rowB' pos, "│", rshift pos]
    rowB' pos = mconcat . intersperse "│" $ center' 5 . labels pos <$> [K.Iso102, K.Z, K.X, K.C, K.V, K.B, K.N, K.M, K.Comma, K.Period, K.Slash, K.Ro]
    rowA pos = mconcat [rowA1 pos, "│", space pos, "│", rowA2 pos]
    rowA1 pos = mconcat . intersperse "│" $ center' 6 . labels pos <$> [K.LControl, K.LSuper, K.LAlternate, K.Muhenkan]
    rowA2 pos = mconcat . intersperse "│" $ center' 6 . labels pos <$> [K.Henkan, K.HiraganaKatakana, K.RAlternate, K.RSuper, K.Menu, K.RControl]
    backspace pos = center' 10 . labels pos $ K.Backspace
    tab pos = center' 8 . labels pos $ K.Tabulator
    return' pos = center' 7 . labels pos $ K.Return
    caps pos = center' 9 . labels pos $ K.CapsLock
    lshift pos = center' 6 . labels pos $ K.LShift
    rshift pos = center' 9 . labels pos $ K.RShift
    space pos = center' 18 . labels pos $ K.Space
    center' n = center n ' '
