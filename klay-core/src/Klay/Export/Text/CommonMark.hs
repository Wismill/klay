{-# LANGUAGE OverloadedLists #-}

module Klay.Export.Text.CommonMark
  ( generateCommonMarkFile
  , generateCommonMark
  ) where

import Data.Semigroup (Semigroup(..))
import Data.Maybe (fromMaybe)
import Data.Set qualified as Set
import Data.Map.Strict qualified as Map
import Data.List (intersperse)
import Data.List.NonEmpty qualified as NE
import Data.Char (isAlphaNum, isPunctuation, isSymbol)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Data.Text.Lazy.Builder qualified as TB
import Control.Applicative (Alternative(..))
import System.FilePath
import System.Directory

import TextShow (showb, showtl)
import Data.Text.Lazy.IO.Utf8 qualified as Utf8
import Data.Time.Format.ISO8601 (iso8601Show)
import Data.BCP47 qualified as BCP47

import Klay.Export.Linux.XKB.Header (createHeader)
import Klay.Export.Text.Common
import Klay.Keyboard.Hardware qualified as H
import Klay.Keyboard.Hardware.Key.ISO qualified as ISO
import Klay.Keyboard.Layout
import Klay.Keyboard.Layout.Action (Actions(..), implicitActionsList)
import Klay.Keyboard.Layout.Group
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Keyboard.Layout.Symbols qualified as S
import Klay.OS (OsSpecific(..), serializeOsSpecific)
-- import Klay.Utils.CommonMark (escapeText)
import Klay.Utils.Unicode
import Klay.Utils.Unicode.Scripts (UnicodeScript(Common), unicodeScript)
import Klay.Utils.Unicode.Blocks (UnicodeBlockRange(..), unicodeBlockRange)
import Klay.Utils.UserInput (escapeLine, RawText(..))

generateCommonMarkFile :: FilePath -> OsSpecific -> H.Geometry -> MultiOsRawLayout -> IO ()
generateCommonMarkFile path os geo layout = do
  createDirectoryIfMissing True path
  header <- createHeader layout mempty
  let content = mconcat
              [ "<!--\n"
              , header
              , "-->\n\n"
              , generateCommonMark os geo layout ]
  Utf8.writeFile path' content
  where
    os' = case os of
      OsIndependent -> mempty
      _             -> ("-" <>) . T.unpack . serializeOsSpecific $ os
    geo' = "-" <> (fmap (\c -> if c == ' ' then '-' else c) . H.geometryToString $ geo)
    path' = path </> mkLayoutId' layout <> os' <> geo' <.> "md"

generateCommonMark :: OsSpecific -> H.Geometry -> MultiOsRawLayout -> TL.Text
generateCommonMark os geo layout = mconcat
  [ "# Keyboard layout “", lname, "” (OS: ", osName, ", ", geoName, ")\n\n"
  , "## Metadata\n\n"
  , "* Name: ", lname, "\n"
  , "* System name: ``", TL.pack . mkLayoutId' $ layout, "``\n"
  , "* Version: ", maybe "(none)" (("``" <>) . (<> "``"))(mkLayoutVersion layout), "\n"
  , "* Author: ", maybe "(none)" escapeLine . _author $ metadata, "\n"
  , "* License: ", maybe "(none)" escapeLine . _license $ metadata, "\n"
  -- , "* Date: ", TL.pack . iso8601Show $ t, "\n"
  , "* Publication Date: ", maybe "(none)" (TL.pack . iso8601Show) . _publicationDate $ metadata, "\n"
  , "* OS: ", osName, "\n"
  , "* Description: ", maybe "(none)" getRawText . _description $ metadata, "\n"
  , "* Locales:\n", locales, "\n\n"
  , "## Supported Characters\n\n"
  -- , charactersDoc (S.reachableSymbols os layout)
  , TB.toLazyText $ charactersDoc (S.reachableCharacters os layout)
  , "\n"
  , mkGroups groups
  ]
  where
    lname = mkLayoutName layout
    osName = TL.fromStrict . T.toTitle . serializeOsSpecific $ os
    geoName = TL.pack . H.geometryToString $ geo
    metadata = _metadata layout
    locales = TB.toLazyText
      let ls = _locales metadata
          mkLocales = mconcat
                    . intersperse ", "
                    . fmap (("``" <>) . (<> "``") . TB.fromText . BCP47.toText)
                    . Set.toList
      in mconcat
        [ "    * Primary: ", mkLocales (_locPrimary ls), "\n"
        , "    * Secondary: ", mkLocales (_locSecondary ls), "\n"
        , "    * Other: ", mkLocales (_locOther ls)
        ]
    labeler = actionLabeler os . _options $ layout
    groups = indexedGroups $ finalGroupsLayouts os layout
    mkGroups = sconcat
             . NE.intersperse "\n\n"
             . fmap processGroup
    processGroup (i, g) = mconcat
      [ "## Group ", showtl (groupIndexToInt i), ": "
      , escapeLine . _groupName $ g, "\n\n"
      , "### Global view\n\n"
      -- , "```\n"
      , displayIsoGroupLegend g, "\n"
      , "\n"
      -- , "```\n\n"
      , "```\n"
      , fromMaybe (displayIsoGroup os labeler g) (displayBigGroup geo os labeler g), "\n"
      , "```\n\n"
      , "### Detailed layers\n\n"
      , mkGroupLayers g
      , "### Detailed keys\n\n"
      , mkDetailedKeys g
      ]
    mkGroupLayers g = Map.foldMapWithKey (mkLayer g) (_groupLevels g)
    display_mini_group_layer = fromMaybe displayMiniIsoGroupLayer (mkDisplayMiniGroupLayer geo)
    mkLayer g l n = mconcat
      [ "#### Layer: ", escapeLine n, "\n\n"
      , "```\n"
      , display_mini_group_layer labeler l g, "\n"
      , "```\n\n"
      ]
    mkDetailedKeys g =
      let am = Map.mapKeys ISO.IsoKey (_groupMapping g)
      in Map.foldMapWithKey (mkDetailedKey (selectIsoGroupLevels g)) am
    mkDetailedKey (ts, bs) (ISO.IsoKey key) = TB.toLazyText . \case
      SingleAction a -> mkLegend (mkActionLegends [a]) []
      as             ->
        let ts' = implicitActionsList ts as
            bs' = implicitActionsList bs as
        in mkLegend (mkActionLegends ts') (mkActionLegends bs')
      where
        caption = mconcat ["Key “", showb key, "”"]
        mkLegend = keycapLegendHtml (Just caption) True
        mkActionLegends = fmap mkActionLegend
        mkActionLegend a = let a' = fromMaybe " " (labeler a) in (a',) case a of
          AChar c     -> mconcat [showUnicode c]
          AText _     -> mconcat ["Text: ", a']
          ADeadKey dk -> mconcat ["Dead key: ", TL.unpack . escapeLine $ _dkName dk]
          AModifier m -> mconcat ["Modifier: ", T.unpack . M.serializeModifier $ m]
          ASpecial s  -> mconcat ["OS action: ", show s]
          _           -> "No action"

-- charactersDoc :: S.ReachableResults -> TL.Text
charactersDoc :: Set.Set Char -> TB.Builder
charactersDoc symbols = mconcat
  -- [ "### Characters by direct input\n\n"
  -- , foldMap charactersByScript (S.groupByUnicodeScript bs')
  -- , "### Characters by dead key\n\n"
  -- , foldMap charactersByScript (S.groupByUnicodeScript bs')
  [ "### Characters\n\n"
  , "__Total:__ ", showb totalChars, " character", plural totalChars
  , " distributed over "
  , showb totalScripts, " script", plural totalScripts, ":\n\n"
  , Map.foldMapWithKey mkScript charactersByScript
  ]
  where
    totalChars = length symbols
    totalScripts = length charactersByScript
    -- S.ReachableResults{S._rByDirectInput=bs, S._rByDeadKey=ds} = symbols
    -- bs' = S.onlyChars bs
    -- ds' = S.onlyChars ds
    -- dks = S.onlyDeadKeys bs
    charactersByScript = foldr groupByUnicodeScript mempty symbols

    groupByUnicodeScript c =
      let script = unicodeScript c
      in Map.alter (\cs -> fmap (Set.insert c) cs <|> Just [c]) script

    groupByUnicodeBlock c =
      let block = unicodeBlockRange c
      in Map.alter (\cs -> fmap (Set.insert c) cs <|> Just [c]) block


    mkScript script chars = mconcat
      [ "- Script: __", showb script, "__\n"
      , case script of
        Common -> let blocks = foldr groupByUnicodeBlock mempty chars
                      blockCount = length blocks
                  in mconcat
                    [ "\n  ", showb charsCount, " character", plural charsCount
                    , " distributed over "
                    , showb blockCount, " block", plural blockCount, ":\n"
                    , Map.foldMapWithKey showCharsUnicodeByBlock blocks
                    ]
        _      -> "\n  " <> showChars chars
      ]
      where charsCount = length chars

    showCharsUnicodeByBlock blockRange chars = mconcat
      [ "    - Block: _", TB.fromString . _blockName $ blockRange, "_\n\n"
      , "      ", showChars chars ]

    showChars cs = mconcat
      [ showb charsCount, " character", plural charsCount, ": ``"
      , foldMap prettyChar cs
      , "``\n"
      ]
      where charsCount = length cs

    prettyChar c
      | isAlphaNum c || isPunctuation c || isSymbol c = TB.singleton c
      | otherwise = mconcat [" (", TB.fromString . showUnicode $ c, ") "]

    plural n | n > 1     = "s"
             | otherwise = ""
