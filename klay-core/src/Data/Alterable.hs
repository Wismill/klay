{-# LANGUAGE OverloadedLists      #-}

module Data.Alterable
  ( -- * Delta
    Delta(..)
    -- * Alteration
  , Alteration(..)
  , IsAlteration(..)
  , mAlterationToValue
    -- * Upgrade
  , Upgrade(..)
  , upgradeToValue
  , mUpgradeToValue
    -- * Differentiable
  , Differentiable(..)
  , defaultDiff
    -- * Alterable
  , Magma(..)
  , Alterable(..)
  , AlterableWithIndex(..)
  , (<!), (!>), (<+)
  , insertOrUpdate0
  , insertOrUpdate1
  , insertOrUpdate0d
  , insertOrUpdate1d
    -- * Incremental
  , Incremental
  ) where

import Data.Kind ( Type )
import Data.List.NonEmpty qualified as NE
import Data.IntMap.Strict qualified as IMap
import Data.IntMap.NonEmpty qualified as NEIntMap
import Data.Map.Strict qualified as Map
import Data.Function (on)
import Data.Foldable (foldl')
import Data.Bifunctor (Bifunctor(..))
import Data.Bifoldable (Bifoldable(..))
import Data.Bitraversable (Bitraversable(..))
import Control.DeepSeq ( NFData, NFData1 )
import GHC.Generics ( Generic, Generic1 )

import Data.These (These(..))
import Data.Align (Semialign(..))
import Data.Witherable (Filterable(..))
import Test.QuickCheck.Arbitrary
  (Arbitrary(..), Arbitrary1(..), Arbitrary2(..), genericShrink)
import Test.QuickCheck.Gen (oneof)


--- Delta ---------------------------------------------------------------------

data Delta a d
  = Inserted a
  -- ^ New element
  | Updated d
  -- ^ Update performed
  | Deleted a
  -- ^ Last element deleted
  deriving (Generic, Generic1, NFData, NFData1, Functor, Foldable, Traversable, Eq, Show)

instance Bifunctor Delta where
  bimap f _ (Inserted a) = Inserted (f a)
  bimap _ g (Updated d) = Updated (g d)
  bimap f _ (Deleted a) = Deleted (f a)

instance Bifoldable Delta where
  bifoldr f _ acc (Inserted a) = f a acc
  bifoldr _ g acc (Updated d)  = g d acc
  bifoldr f _ acc (Deleted a)  = f a acc

instance Bitraversable Delta where
  bitraverse f _ (Inserted a) = Inserted <$> f a
  bitraverse _ g (Updated d)  = Updated <$> g d
  bitraverse f _ (Deleted a)  = Deleted <$> f a

instance (Arbitrary a, Arbitrary d) => Arbitrary (Delta a d) where
  arbitrary = oneof
    [ Inserted <$> arbitrary
    , Updated <$> arbitrary
    , Deleted <$> arbitrary
    ]
  shrink = genericShrink

instance (Arbitrary a) => Arbitrary1 (Delta a) where
  liftArbitrary g = oneof
    [ Inserted <$> arbitrary
    , Updated <$> g
    , Deleted <$> arbitrary
    ]

instance Arbitrary2 Delta where
  liftArbitrary2 g1 g2 = oneof
    [ Inserted <$> g1
    , Updated <$> g2
    , Deleted <$> g1
    ]


--- Alteration ----------------------------------------------------------------

data Alteration a d
  = InsertOrIgnore a
  -- ^ Insert if no previous value
  | InsertOrReplace a
  -- ^ Insert or replace previous value
  | InsertOrUpdate a d
  -- ^ Insert a new value or update the previous one
  | Update d
  -- ^ Update a previous value if applicable
  | Delete
  -- ^ Delete
  deriving (Generic, Generic1, NFData, NFData1, Functor, Foldable, Traversable, Eq, Show)

insertOrUpdate0 :: (Differentiable a d) => a -> a -> Alteration a d
insertOrUpdate0 a0 a = maybe (InsertOrIgnore a) (InsertOrUpdate a) (diff a0 a)

insertOrUpdate1 :: (Monoid a, Differentiable a d) => a -> Alteration a d
insertOrUpdate1 = insertOrUpdate0 mempty

insertOrUpdate0d :: (Alterable a d) => a -> d -> Alteration a d
insertOrUpdate0d a0 d = InsertOrUpdate (alter d a0) d

insertOrUpdate1d :: (Monoid a, Alterable a d) => d -> Alteration a d
insertOrUpdate1d = insertOrUpdate0d mempty

class IsAlteration a b d where
  to_alteration :: a -> Alteration b d

instance IsAlteration (Alteration a d) a d where
  to_alteration = id

instance {-# OVERLAPPABLE #-} IsAlteration a a d where
  to_alteration = InsertOrReplace

-- instance {-# OVERLAPPABLE #-} (Monoid a, Alterable a d) => IsAlteration d a d where
--   to_alteration = insertOrUpdate1d

instance Bifunctor Alteration where
  bimap f _ (InsertOrIgnore a)   = InsertOrIgnore (f a)
  bimap f _ (InsertOrReplace a)  = InsertOrReplace (f a)
  bimap f g (InsertOrUpdate a d) = InsertOrUpdate (f a) (g d)
  bimap _ g (Update d)           = Update (g d)
  bimap _ _ Delete               = Delete

instance Bifoldable Alteration where
  bifoldr f _ acc (InsertOrIgnore a)   = f a acc
  bifoldr f _ acc (InsertOrReplace a)  = f a acc
  bifoldr f g acc (InsertOrUpdate a d) = g d (f a acc)
  bifoldr _ g acc (Update d)           = g d acc
  bifoldr _ _ acc Delete               = acc

instance Bitraversable Alteration where
  bitraverse f _ (InsertOrIgnore a)   = InsertOrIgnore <$> f a
  bitraverse f _ (InsertOrReplace a)  = InsertOrReplace <$> f a
  bitraverse f g (InsertOrUpdate a d) = InsertOrUpdate <$> f a <*> g d
  bitraverse _ g (Update d)           = Update <$> g d
  bitraverse _ _ Delete               = pure Delete

instance (Magma d, Alterable a d) => Magma (Alteration a d) where
  _                  +> InsertOrReplace a   = InsertOrReplace a
  _                  +> Delete              = Delete
  Update d           +> InsertOrIgnore a    = InsertOrUpdate a d
  Delete             +> InsertOrIgnore a    = InsertOrReplace a
  a                  +> InsertOrIgnore _    = a
  InsertOrIgnore a   +> InsertOrUpdate _ d  = InsertOrUpdate a d
  InsertOrReplace a  +> InsertOrUpdate _ d  = InsertOrReplace (alter d a)
  InsertOrUpdate a d +> InsertOrUpdate _ d' = InsertOrUpdate (alter d' a) (d +> d')
  Update d           +> InsertOrUpdate a d' = InsertOrUpdate a (d +> d')
  Delete             +> InsertOrUpdate a _  = InsertOrReplace a
  InsertOrIgnore a   +> Update d            = InsertOrUpdate (alter d a) d
  InsertOrReplace a  +> Update d            = InsertOrReplace (alter d a)
  InsertOrUpdate a d +> Update d'           = InsertOrUpdate (alter d' a) (d +> d')
  Update d           +> Update d'           = Update (d +> d')
  Delete             +> Update _            = Delete

-- [FIXME] this is not associative
-- -- | Note: Right-biased
-- instance (Alterable a d, Magma d) => Semigroup (Alteration a d) where
--   _                  <> InsertOrReplace a   = InsertOrReplace a
--   _                  <> Delete              = Delete
--   Update d           <> InsertOrIgnore a    = InsertOrUpdate a d
--   Delete             <> InsertOrIgnore a    = InsertOrReplace a
--   a                  <> InsertOrIgnore _    = a
--   InsertOrIgnore a   <> InsertOrUpdate _ d  = InsertOrUpdate a d
--   InsertOrReplace a  <> InsertOrUpdate _ d  = InsertOrReplace (alter d a)
--   InsertOrUpdate a d <> InsertOrUpdate _ d' = InsertOrUpdate (alter d' a) (alter d' d)
--   Update d           <> InsertOrUpdate a d' = InsertOrUpdate a (alter d' d)
--   Delete             <> InsertOrUpdate a _  = InsertOrReplace a
--   InsertOrIgnore a   <> Update d            = InsertOrUpdate (alter d a) d
--   InsertOrReplace a  <> Update d            = InsertOrReplace (alter d a)
--   InsertOrUpdate a d <> Update d'           = InsertOrUpdate (alter d' a) (alter d' d)
--   Update d           <> Update d'           = Update (alter d' d)
--   Delete             <> Update _            = Delete

-- instance (Alterable a d, Magma d, Monoid d) => Monoid (Alteration a d) where
--   mempty = Update mempty

instance (Arbitrary a, Arbitrary d) => Arbitrary (Alteration a d) where
  arbitrary = oneof
    [ InsertOrIgnore <$> arbitrary
    , InsertOrUpdate <$> arbitrary <*> arbitrary
    , InsertOrReplace <$> arbitrary
    , Update <$> arbitrary
    , pure Delete
    ]
  shrink = genericShrink

instance (Arbitrary a) => Arbitrary1 (Alteration a) where
  liftArbitrary g = oneof
    [ InsertOrIgnore <$> arbitrary
    , InsertOrUpdate <$> arbitrary <*> g
    , InsertOrReplace <$> arbitrary
    , Update <$> g
    , pure Delete
    ]
  liftShrink _ (InsertOrIgnore a)   = InsertOrIgnore <$> shrink a
  liftShrink f (InsertOrUpdate a u) = [InsertOrUpdate a' u' | a' <- shrink a, u' <- f u]
  liftShrink _ (InsertOrReplace a)  = InsertOrReplace <$> shrink a
  liftShrink f (Update u)           = Update <$> f u
  liftShrink _ Delete               = mempty

instance Arbitrary2 Alteration where
  liftArbitrary2 g1 g2 = oneof
    [ InsertOrIgnore <$> g1
    , InsertOrUpdate <$> g1 <*> g2
    , InsertOrReplace <$> g1
    , Update <$> g2
    , pure Delete
    ]
  liftShrink2 f _ (InsertOrIgnore a)   = InsertOrIgnore <$> f a
  liftShrink2 f g (InsertOrUpdate a u) = [InsertOrUpdate a' u' | a' <- f a, u' <- g u]
  liftShrink2 f _ (InsertOrReplace a)  = InsertOrReplace <$> f a
  liftShrink2 _ g (Update u)           = Update <$> g u
  liftShrink2 _ _ Delete               = mempty


mAlterationToValue :: (Monoid m, Alterable m d) => Alteration m d -> Maybe m
mAlterationToValue (InsertOrIgnore a)   = Just a
mAlterationToValue (InsertOrReplace a)  = Just a
mAlterationToValue (InsertOrUpdate a _) = Just a
mAlterationToValue (Update d)           = Just $ alter d mempty
mAlterationToValue Delete               = Nothing


--- Upgrade -------------------------------------------------------------------

data Upgrade a
  = Merge a
  -- ^ Merge with the new value
  | Replace a
  -- ^ Replace with a new value
  | Reset
  -- ^ Reset to a default value
  deriving (Generic, Generic1, NFData, NFData1, Functor, Foldable, Traversable, Eq, Show)

instance (Magma a) => Magma (Upgrade a) where
  Merge a   +> Merge b   = Merge (a +> b)
  Replace a +> Merge b   = Replace (a +> b)
  Reset     +> Merge b   = Replace b
  _         +> Replace a = Replace a
  _         +> Reset     = Reset

instance {-# OVERLAPPING #-} (Semigroup a) => Semigroup (Upgrade a) where
  Merge a   <> Merge b   = Merge (a <> b)
  Replace a <> Merge b   = Replace (a <> b)
  Reset     <> Merge b   = Replace b
  _         <> Replace a = Replace a
  _         <> Reset     = Reset

-- [FIXME] this is not a neutral element: Reset <> Merge mempty /= Reset
instance (Monoid a, Semigroup a) => Monoid (Upgrade a) where
  mempty = Merge mempty

instance (Arbitrary a) => Arbitrary (Upgrade a) where
  arbitrary = oneof
    [ Merge <$> arbitrary
    , Replace <$> arbitrary
    , pure Reset
    ]
  shrink = genericShrink

instance Arbitrary1 Upgrade where
  liftArbitrary g = oneof
    [ Merge <$> g
    , Replace <$> g
    , pure Reset
    ]
  liftShrink f (Merge a)   = Merge <$> f a
  liftShrink f (Replace a) = Replace <$> f a
  liftShrink _ Reset       = mempty

upgradeToValue :: (Monoid a) => Upgrade a -> a
upgradeToValue (Merge a)   = a
upgradeToValue (Replace a) = a
upgradeToValue Reset       = mempty

mUpgradeToValue :: Upgrade a -> Maybe a
mUpgradeToValue (Merge a)   = Just a
mUpgradeToValue (Replace a) = Just a
mUpgradeToValue Reset       = Nothing


--- Differentialble -----------------------------------------------------------

class Differentiable a d where
  diff :: a -> a -> Maybe d

  default diff :: (Eq a, d ~ a) => a -> a -> Maybe d
  diff a b
    | a /= b    = Just b
    | otherwise = Nothing

defaultDiff :: (Eq a) => a -> a -> Maybe a
defaultDiff a b
  | a /= b    = Just b
  | otherwise = Nothing

instance Differentiable Char    Char
instance Differentiable String  String
instance Differentiable Int     Int
instance Differentiable Integer Integer

instance (Differentiable a a) => Differentiable (Maybe a) a where
  diff Nothing  Nothing  = Nothing
  diff Nothing  a        = a
  diff a        Nothing  = a
  diff (Just a) (Just b) = diff a b

instance (Eq a, Eq d) => Differentiable (Delta a d) (Delta a d) where
  diff = defaultDiff -- [FIXME]

instance (Differentiable a a) => Differentiable (Upgrade a) (Upgrade a) where
  diff (Merge a)   (Merge b)   = Merge <$> diff a b
  diff (Replace a) (Merge b)   = Merge <$> diff a b
  diff Reset       (Merge b)   = Just $ Merge b
  diff (Merge a)   (Replace b) = Replace <$> diff a b
  diff (Replace a) (Replace b) = Replace <$> diff a b
  diff Reset       (Replace b) = Just $ Replace b
  diff _           Reset       = Just Reset

instance (Eq a, Eq d) => Differentiable (Alteration a d) (Alteration a d) where
  diff = defaultDiff -- [FIXME]

instance (Differentiable a d) => Differentiable (Maybe a) (Delta a d) where
  diff Nothing  Nothing  = Nothing
  diff (Just a) Nothing  = Just (Deleted a)
  diff Nothing  (Just b) = Just (Inserted b)
  diff (Just a) (Just b) = Updated <$> diff a b

instance (Monoid a, Differentiable a d) => Differentiable (Maybe a) (Alteration a d) where
  diff Nothing  Nothing  = Nothing
  diff (Just _) Nothing  = Just Delete
  diff Nothing  (Just b) = Just (insertOrUpdate1 b)
  diff (Just a) (Just b) = Update <$> diff a b

instance (Differentiable a a) => Differentiable (Maybe a) (Upgrade a) where
  diff Nothing  Nothing  = Nothing
  diff (Just _) Nothing  = Just Reset
  diff Nothing  (Just b) = Just (Replace b)
  diff (Just a) (Just b) = Merge <$> diff a b

instance (Differentiable a da, Differentiable b db) => Differentiable (a, b) (Maybe da, Maybe db) where
  diff (a, b) (a', b') = case (diff a a', diff b b') of
    (Nothing, Nothing) -> Nothing
    ds                 -> Just ds

instance (Differentiable a da, Differentiable b db, Differentiable c dc) => Differentiable (a, b, c) (Maybe da, Maybe db, Maybe dc) where
  diff (a, b, c) (a', b', c') = case (diff a a', diff b b', diff c c') of
    (Nothing, Nothing, Nothing) -> Nothing
    ds                          -> Just ds

-- [TODO] check [NOTE] Keep only the new elements
instance {-# OVERLAPPABLE #-} (Foldable f, Semialign f, Filterable f, Differentiable a a) => Differentiable (f a) (f a) where
  diff as bs | null d = Nothing | otherwise = Just d
    where
      d = catMaybes $ alignWith go as bs
      go (This a)    = Just a
      go (That b)    = Just b
      go (These a b) = diff a b

instance {-# OVERLAPPABLE #-} (Foldable f, Semialign f, Filterable f, Differentiable a d) => Differentiable (f a) (f (Delta a d)) where
  diff as bs | null d = Nothing | otherwise = Just d
    where
      d = catMaybes $ alignWith go as bs
      go (This a)    = Just (Deleted a)
      go (That b)    = Just (Inserted b)
      go (These a b) = Updated <$> diff a b

instance {-# OVERLAPPABLE #-} (Foldable f, Semialign f, Filterable f, Differentiable a d) => Differentiable (f a) (f (Alteration a d)) where
  diff as bs | null d = Nothing | otherwise = Just d
    where
      d = catMaybes $ alignWith go as bs
      go (This _)    = Just Delete
      go (That a)    = Just (InsertOrReplace a)
      go (These a b) = Update <$> diff a b

instance {-# OVERLAPPABLE #-} (Foldable f, Semialign f, Filterable f, Differentiable a a) => Differentiable (f a) (f (Upgrade a)) where
  diff as bs | null d = Nothing | otherwise = Just d
    where
      d = catMaybes $ alignWith go as bs
      go (This _)    = Just Reset
      go (That a)    = Just (Replace a)
      go (These a b) = Replace <$> diff a b

instance (Differentiable a a) => Differentiable (NEIntMap.NEIntMap a) (NEIntMap.NEIntMap a) where
  diff as1 as2 = on diff NEIntMap.toMap as1 as2 >>= NEIntMap.nonEmptyMap

instance (Differentiable a a) => Differentiable (IMap.IntMap a) (Upgrade (IMap.IntMap (Upgrade a))) where
  diff as1 as2
    | null as1  = if null as2
      then Nothing
      else Just $ Merge (IMap.map Replace as2)
    | null as2  = Just Reset
    | otherwise = Merge <$> diff as1 as2

instance (Differentiable a a) => Differentiable (NEIntMap.NEIntMap a) (Upgrade (NEIntMap.NEIntMap (Upgrade a))) where
  diff as1 as2 = on diff NEIntMap.toMap as1 as2 >>= traverse NEIntMap.nonEmptyMap

instance (Differentiable a a) => Differentiable (IMap.IntMap a) (Upgrade (IMap.IntMap (Alteration a a))) where
  diff as1 as2
    | null as1  = if null as2
      then Nothing
      else Just $ Merge (IMap.map InsertOrReplace as2)
    | null as2  = Just Reset
    | otherwise = Merge <$> diff as1 as2


--- Magma ---------------------------------------------------------------------

class Magma a where
  (+>) :: a -> a -> a
  (+>) = \_ x -> x

  -- | Left-to-right concatenation
  lconcat :: NE.NonEmpty a -> a
  lconcat (a NE.:| as) = go a as
    where go x1 []      = x1
          go x1 (x2:xs) = x1 +> go x2 xs

  -- | Right-to-left concatenation
  rconcat :: NE.NonEmpty a -> a
  rconcat (a NE.:| as) = go a as
    where go x1 []      = x1
          go x1 (x2:xs) = go (x1 +> x2) xs

(<+) :: (Magma a) => a -> a -> a
(<+) = flip (+>)

instance (Magma a) => Alterable a a where
  alter = (<+)

-- [TODO] more Magma instances
instance Magma Char
instance Magma Int
instance Magma Integer

instance {-# OVERLAPPABLE #-} (Semialign f, Magma a) => Magma (f a) where
  as +> bs = alignWith go as bs
    where go (This a) = a
          go (That b) = b
          go (These a b) = a +> b


--- Alterable -----------------------------------------------------------------

class Alterable a d where
  alter :: d -> a -> a

  -- Note: here because else it causes overlapping instances
  malter :: Maybe d -> a -> a
  malter Nothing  = id
  malter (Just d) = alter d

  falter :: (Foldable f) => f d -> a -> a
  falter ds a = foldl' (flip alter) a ds

(<!) :: (Alterable a d) => d -> a -> a
(<!) = alter

(!>) :: (Alterable a d) => a -> d -> a
(!>) = flip alter

class AlterableWithIndex i (f :: Type -> Type) | f -> i where
  ialter :: (Alterable a d) => i -> d -> f a -> f a

instance (Alterable a d) => Alterable (Maybe a) (Delta a d) where
  alter (Inserted a) _        = Just a
  alter (Updated d)  (Just a) = Just $ alter d a
  alter _            _        = Nothing

instance (Alterable a d) => Alterable (Maybe a) (Alteration a d) where
  alter (InsertOrIgnore a)   Nothing   = Just a
  alter (InsertOrIgnore _)   ma        = ma
  alter (InsertOrReplace a)  _         = Just a
  alter (InsertOrUpdate a _) Nothing   = Just a
  alter (InsertOrUpdate _ d) (Just a)  = Just $ alter d a
  alter (Update _)           Nothing   = Nothing
  alter (Update d)           (Just a)  = Just $ alter d a
  alter _                    _         = Nothing

instance (Alterable a a) => Alterable (Maybe a) (Upgrade a) where
  alter (Merge a)   (Just b) = Just $ alter a b
  alter (Replace a) _        = Just a
  alter _           _        = Nothing

instance {-# OVERLAPPABLE #-} (Semialign f, Filterable f, Alterable a d) => Alterable (f a) (f (Delta a d)) where
  alter d = catMaybes . alignWith go d
    where
      go (This (Inserted a))    = Just a
      go (This _)               = Nothing
      go (That a)               = Just a
      go (These (Inserted a) _) = Just a
      go (These (Updated u) a)  = Just (alter u a)
      go (These (Deleted _) _)  = Nothing

instance {-# OVERLAPPABLE #-} (Semialign f, Filterable f, Alterable a d) => Alterable (f a) (f (Alteration a d)) where
  alter d = catMaybes . alignWith go d
    where
      go (This (InsertOrIgnore a))       = Just a
      go (This (InsertOrReplace a))      = Just a
      go (This (InsertOrUpdate a _))     = Just a
      go (This _)                        = Nothing
      go (That a)                        = Just a
      go (These (InsertOrIgnore _) a)    = Just a
      go (These (InsertOrReplace a) _)   = Just a
      go (These (InsertOrUpdate _ u) a)  = Just (alter u a)
      go (These (Update u) a)            = Just (alter u a)
      go _                               = Nothing

instance {-# OVERLAPPABLE #-} (Semialign f, Filterable f, Magma a) => Alterable (f a) (f (Upgrade a)) where
  alter d = catMaybes . alignWith go d
    where
      go (This (Merge a))      = Just a
      go (This (Replace a))    = Just a
      go (This _)              = Nothing
      go (That a)              = Just a
      go (These (Merge a) b)   = Just (alter a b)
      go (These (Replace a) _) = Just a
      go (These Reset _)       = Nothing

instance (Monoid a, Magma a) => Alterable (IMap.IntMap a) (Upgrade (IMap.IntMap (Upgrade a))) where
  alter (Merge u)   a = alter u a
  alter (Replace u) _ = IMap.mapMaybe mUpgradeToValue u
  alter Reset       _ = mempty

instance (Monoid a, Magma a) => Alterable (IMap.IntMap a) (Upgrade (IMap.IntMap (Alteration a a))) where
  alter (Merge u)   a = alter u a
  alter (Replace u) _ = IMap.mapMaybe mAlterationToValue u
  alter Reset       _ = mempty

instance {-# OVERLAPPABLE #-} (Monoid a, Magma a) => Alterable a (Upgrade a) where
  alter (Merge a)   b = alter b a
  alter (Replace a) _ = a
  alter Reset       _ = mempty

instance (Ord k, Alterable v u) => Alterable (Map.Map k v) (k, Alteration v u) where
  alter (k, InsertOrIgnore v)   = Map.insertWith (\_ x -> x) k v
  alter (k, InsertOrUpdate v u) = Map.insertWith (const $ alter u) k v
  alter (k, InsertOrReplace v)  = Map.insert k v
  alter (k, Update u)           = Map.adjust (alter u) k
  alter (k, Delete)             = Map.delete k

instance (Ord k, Magma v) => Alterable (Map.Map k v) (k, Upgrade v) where
  alter (k, Merge v)   = Map.insertWith (<+) k v
  alter (k, Replace v) = Map.insert k v
  alter (k, Reset)     = Map.delete k

--- Incremental ---------------------------------------------------------------

type Incremental a d = (Differentiable a d, Alterable a d)
