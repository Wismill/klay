module Data.List.NonEmpty2
  ( NonEmpty2(..)
  , IsList(..)
  , head
  , tail
  ) where

import Prelude hiding (head, tail)
import Data.List.NonEmpty (NonEmpty(..))
import Control.DeepSeq ( NFData, NFData1 )
import GHC.Generics ( Generic, Generic1 )
import GHC.Exts (IsList(..))

-- | A list with at least two elements
data NonEmpty2 a = !a ::| !(NonEmpty a)
  deriving stock (Generic1, Generic, Functor, Foldable, Traversable, Eq, Ord, Show)
  deriving anyclass (NFData1, NFData)

instance IsList (NonEmpty2 a) where
  type (Item (NonEmpty2 a)) = a
  fromList [] = error "[ERROR] fromList: Empty list"
  fromList [_] = error "[ERROR] fromList: only one element"
  fromList (a1:a2:as) = a1 ::| (a2 :| as)
  toList (a1 ::| (a2 :| as)) = a1 : a2 : as

head :: NonEmpty2 a -> a
head (a ::| _) = a

tail :: NonEmpty2 a -> NonEmpty a
tail (_ ::| as) = as
