{-|
Description : A container for customisable data
-}

module Data.Customisable
  ( Customisable(..)
  , CustomisableChange(..)
  , CustomisableTypeChange(..)
  , untag
  , arbitraryDefault
  , arbitraryCustom
  ) where


import Data.Bifunctor (Bifunctor(..))
import Data.Bifoldable (Bifoldable(..))
import Data.Bitraversable (Bitraversable(..))
import Control.DeepSeq (NFData(..))
import GHC.Generics (Generic, Generic1)

import Test.QuickCheck.Arbitrary (Arbitrary(..), genericShrink)
import Test.QuickCheck.Gen (Gen, oneof)
import Data.Aeson.Types (ToJSON(..), FromJSON(..))
import Data.Aeson qualified as Aeson

import Data.Alterable (Magma(..), Alterable(..), Differentiable(..))
import Klay.Utils (jsonOptions)

-- | A value that is customisable.
data Customisable a b
  = Default !a
  -- ^ Default value.
  | Custom !b
  -- ^ Custom value.
  deriving ( Generic, Generic1, NFData
           , Functor, Foldable, Traversable
           , Eq, Ord, Show )

instance Bifunctor Customisable where
  bimap f _ (Default a) = Default (f a)
  bimap _ g (Custom b)  = Custom (g b)

instance Bifoldable Customisable where
  bifoldr f _ acc (Default a) = f a acc
  bifoldr _ g acc (Custom b)  = g b acc

instance Bitraversable Customisable where
  bitraverse f _ (Default a) = Default <$> f a
  bitraverse _ g (Custom b)  = Custom <$> g b

instance {-# OVERLAPPABLE #-} (Semigroup a) => Semigroup (Customisable a a) where
  Default a <> Default b = Default (a <> b)
  Default a <> Custom b  = Custom (a <> b)
  Custom a  <> Default b = Custom (a <> b)
  Custom a  <> Custom b  = Custom (a <> b)

instance {-# OVERLAPPABLE #-} (Monoid a) => Monoid (Customisable a a) where
  mempty = Custom mempty

-- | Note: this is not a valid 'Semigroup'
instance {-# OVERLAPPABLE #-} (Eq a, Magma a) => Magma (Customisable a a) where
  Default _ +> Default b = Default b -- Right-biased
  Default a +> Custom b  = if a == b then Default a else Custom (a +> b)
  Custom a  +> Default b = if a == b then Default b else Custom (a +> b)
  Custom a  +> Custom b  = Custom (a +> b)

instance (ToJSON a, ToJSON b) => ToJSON (Customisable a b) where
  toJSON     = Aeson.genericToJSON (jsonOptions id)
  toEncoding = Aeson.genericToEncoding (jsonOptions id)

instance (FromJSON a, FromJSON b) => FromJSON (Customisable a b) where
  parseJSON = Aeson.genericParseJSON (jsonOptions id)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Customisable a b) where
  arbitrary = oneof [arbitraryDefault, arbitraryCustom]
  shrink = genericShrink

arbitraryDefault :: (Arbitrary a) => Gen (Customisable a b)
arbitraryDefault = Default <$> arbitrary

arbitraryCustom :: (Arbitrary b) => Gen (Customisable a b)
arbitraryCustom = Custom <$> arbitrary

untag :: Customisable a a -> a
untag (Default a) = a
untag (Custom a)  = a


-- | Describe a change of 'Customisable' type.
data CustomisableTypeChange
  = ToDefault -- ^ Change from 'Custom' to 'Default'
  | ToCustom  -- ^ Change from 'Default' to 'Custom'
  deriving (Generic, NFData, Eq, Show)

-- | Describe a change on a 'Customisable' value.
data CustomisableChange a = CustomisableChange (Maybe CustomisableTypeChange) (Maybe a)
  deriving (Generic, Generic1, NFData, Functor, Foldable, Traversable, Eq, Show)

instance (Differentiable a d) => Differentiable (Customisable a a) (CustomisableChange d) where
  diff (Default a) (Default b) = CustomisableChange Nothing . Just <$> diff a b
  diff (Default a) (Custom  b) = Just $ CustomisableChange (Just ToCustom)  (diff a b)
  diff (Custom  a) (Default b) = Just $ CustomisableChange (Just ToDefault) (diff a b)
  diff (Custom  a) (Custom  b) = CustomisableChange Nothing . Just <$> diff a b

instance Alterable (Customisable a a) CustomisableTypeChange where
  alter ToDefault (Custom a)  = Default a
  alter ToDefault a           = a
  alter ToCustom  (Default a) = Custom a
  alter ToCustom  a           = a

instance (Alterable a d) => Alterable (Customisable a a) (CustomisableChange d) where
  alter (CustomisableChange m1 m2) a = malter m1 $ bimap (malter m2) (malter m2) a
