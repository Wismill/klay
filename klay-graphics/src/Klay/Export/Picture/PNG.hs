module Klay.Export.Picture.PNG
  ( generatePng
  ) where


import Data.Foldable (traverse_)
import System.Directory (createDirectoryIfMissing)
import Data.Text qualified as T
import Control.Arrow ((&&&))
import Shelly

import Klay.Export.Picture.SVG (getGroupFileNamesByGroup)
import Klay.Keyboard.Layout (Layout(..), MultiOsRawLayout)
import Klay.Keyboard.Layout.Group
  ( KeyFinalGroup, GroupIndex
  , itraverseGroupsLayouts_, resolveFinalGroups)
import Klay.OS (OsSpecific)

{-| PNG generator. It produces PNG converting SVG pictures with Inkscape.

__Warnings__

- It works only if the SVG files were generated previously.
- It requires Inkscape __1.0+__
-}
generatePng
  :: FilePath         -- ^ PNG output path
  -> FilePath         -- ^ SVG output path
  -> OsSpecific       -- ^ OS
  -> MultiOsRawLayout -- ^ Layout
  -> IO ()
generatePng pngPath svgPath os layout = do
  createDirectoryIfMissing True pngPath
  shelly . verbosely $ itraverseGroupsLayouts_ convertGroupSvg groups
  where
    groups = resolveFinalGroups os . _groups $ layout
    convertGroupSvg :: GroupIndex -> KeyFinalGroup -> Sh ()
    convertGroupSvg k = traverse_ convertSvg . uncurry zip .
      (getGroupFileNamesByGroup svgPath ".svg" k &&& getGroupFileNamesByGroup pngPath ".png" k)
    -- [TODO] ensure Inkscape 1.0 ?
    convertSvg (input_file, output_file) = run_ "inkscape"
      [ "--export-filename", T.pack output_file
      , "--export-type", "png"
      , "--export-width", T.pack . show $ defaultWidth
      , "--export-area-drawing"
      , "--export-area-snap"
      , T.pack input_file ]

-- | Default width of the picture
defaultWidth :: Int
defaultWidth = 900
