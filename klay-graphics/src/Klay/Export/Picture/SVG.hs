{-# LANGUAGE AllowAmbiguousTypes       #-}

{-|
Description: A SVG generator
-}

module Klay.Export.Picture.SVG
  ( StyleOptions(..)
  , generateSvg
  , getGroupFileNamesByGroup
  ) where


import Data.Foldable (traverse_)
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>))
import Data.Functor (void)

import Diagrams.Prelude hiding (Level, All, rmap)
import Diagrams.Backend.SVG

import Klay.Export.Picture.SVG.ABNT (generateSvgAbnt)
import Klay.Export.Picture.SVG.ANSI (GroupDiagram, generateSvgAnsi)
import Klay.Export.Picture.SVG.ISO (generateSvgIso)
import Klay.Export.Picture.SVG.JIS (generateSvgJis)
import Klay.Export.Picture.SVG.Stylesheet (StyleOptions(..))
import Klay.Export.Picture.SVG.TypeMatrix (generateSvgTypematrix)
import Klay.Keyboard.Hardware.Geometry (Geometry(..))
import Klay.Keyboard.Layout (Layout(..), MultiOsRawLayout, actionLabeler, finalGroupsLayouts)
import Klay.Keyboard.Layout.Group
import Klay.OS (OsSpecific)
import Klay.Typing.Method (TypingMethod)
import Klay.Utils.UserInput (escapeFilename')

-- | SVG generator
generateSvg :: FilePath -> OsSpecific -> Geometry -> TypingMethod -> StyleOptions -> MultiOsRawLayout -> IO ()
generateSvg path os geometry typingMethod config layout = do
  createDirectoryIfMissing True path
  itraverseGroupsLayouts_ (make_group_svg svg_generator) groups
  where
    svg_generator = case geometry of
      ANSI       f -> generateSvgAnsi f ka config labeler typingMethod
      ISO        f -> generateSvgIso f ka config labeler typingMethod
      ABNT       f -> generateSvgAbnt f ka config labeler typingMethod
      JIS        f -> generateSvgJis f ka config labeler typingMethod
      TypeMatrix m -> generateSvgTypematrix m ka config labeler typingMethod
      -- _            -> error $ "[ERROR] Geometry not implemented: " <> show geometry
    labeler = actionLabeler os . _options $ layout
    ka = _dualFunctionKeys layout
    groups = finalGroupsLayouts os layout
    make_group_svg :: (KeyFinalGroup -> [GroupDiagram SVG]) -> GroupIndex -> KeyFinalGroup -> IO ()
    make_group_svg f k g =
      let group_diagrams = f g
          paths = getGroupFileNamesByGroup path ".svg" k g
      in traverse_ myRenderSVG $ zip group_diagrams paths
      where myRenderSVG (d, fp) = void $ renderSVG fp (fromIntegral <$> mkSizeSpec2D (Just (900 :: Int)) Nothing) d

-- | Create the list of filenames of pictures corresponding to a 'Group'.
getGroupFileNamesByGroup :: FilePath -> String -> GroupIndex -> KeyFinalGroup -> [FilePath]
getGroupFileNamesByGroup path file_extension k g = f <$> levels
  where
    levels = groupIndexedLevelsNames g
    gname = _groupName g
    f (lindex, lname) = path </> mconcat
      [ show . groupIndexToInt $ k, "─"
      , escapeFilename' gname, "─"
      , show lindex, "─"
      , escapeFilename' lname, file_extension ]
