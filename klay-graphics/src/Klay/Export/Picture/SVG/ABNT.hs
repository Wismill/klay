{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Klay.Export.Picture.SVG.ABNT
  ( ANSI.row1, ISO.row2, ISO.row3, row4, ANSI.row5
  , generateSvgAbnt
  , generateSvgAbnt100percent
  , generateSvgAbnt80percent
  , generateSvgAbnt60percent
  ) where

import Klay.Export.Picture.SVG.ANSI qualified as ANSI
import Klay.Export.Picture.SVG.ISO qualified as ISO
import Klay.Export.Picture.SVG.JIS qualified as JIS
import Klay.Export.Picture.SVG.Stylesheet ( StyleOptions )
import Klay.Keyboard.Hardware.ButtonAction (ButtonActionsDefinition)
import Klay.Keyboard.Hardware.Geometry (FormFactor(..))
import Klay.Keyboard.Hardware.Key ( BaseHardwareAction(Mappable) )
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.Label (ActionLabeler)
import Klay.Keyboard.Layout.Group (KeyFinalGroup)
import Klay.Typing.Method ( TypingMethod )


generateSvgAbnt
  :: _
  => FormFactor
  -> ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgAbnt F100 = generateSvgAbnt100percent
generateSvgAbnt F80 = generateSvgAbnt80percent
generateSvgAbnt F70 = generateSvgAbnt70percent
generateSvgAbnt F60 = generateSvgAbnt60percent

generateSvgAbnt100percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgAbnt100percent
  = ANSI.generateGroupSvg
  $ ANSI.mkLevel100percent ANSI.NumPadWithExtraSeparator isoRows

generateSvgAbnt80percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgAbnt80percent = ANSI.generateGroupSvg $ ANSI.mkLevel80percent isoRows

generateSvgAbnt70percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgAbnt70percent = ANSI.generateGroupSvg $ ANSI.mkLevel70percent isoRows

generateSvgAbnt60percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgAbnt60percent = ANSI.generateGroupSvg $ ANSI.mkLevel60percent isoRows

isoRows :: _ => [[ ANSI.MkKey b ]]
isoRows = [ANSI.row1, ISO.row2, ISO.row3, row4, ANSI.row5]

--

row4 :: _ => [ ANSI.MkKey b ]
row4 =
  [ ISO.mkMediumShiftKey . Mappable $ K.LShift
  , ANSI.mkBaseKey . Mappable $ K.Iso102
  , ANSI.mkBaseKey . Mappable $ K.Z
  , ANSI.mkBaseKey . Mappable $ K.X
  , ANSI.mkBaseKey . Mappable $ K.C
  , ANSI.mkBaseKey . Mappable $ K.V
  , ANSI.mkBaseKey . Mappable $ K.B
  , ANSI.mkBaseKey . Mappable $ K.N
  , ANSI.mkBaseKey . Mappable $ K.M
  , ANSI.mkBaseKey . Mappable $ K.Comma
  , ANSI.mkBaseKey . Mappable $ K.Period
  , ANSI.mkBaseKey . Mappable $ K.Slash
  , ANSI.mkBaseKey . Mappable $ K.Ro
  , JIS.mkMediumShiftKey . Mappable $ K.RShift ]
