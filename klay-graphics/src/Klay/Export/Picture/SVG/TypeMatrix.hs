{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Klay.Export.Picture.SVG.TypeMatrix
  ( column0, column1, column2, column3, column4, column5, column6
  , generateSvgTypematrix
  ) where


import Diagrams.Prelude hiding (Level)

import Klay.Export.Picture.SVG.Stylesheet
    ( StyleOptions, baseKeyWidth, baseKeyHeight, horizontalSpace )
import Klay.Export.Picture.SVG.ANSI (MkKey, mkKeyBuilder)
import Klay.Export.Picture.SVG.ANSI qualified as ANSI
import Klay.Keyboard.Hardware.ButtonAction (ButtonActionsMap, ButtonActionsDefinition, composeKeyMap)
import Klay.Keyboard.Hardware.Key
    ( IsPhysicalKey, BaseHardwareAction(Mappable), HardwareAction )
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Hardware.Geometry (TypeMatrixMode(..))
import Klay.Keyboard.Hardware.Key.TypeMatrix2030 qualified as TM
import Klay.Keyboard.Layout.Action.Action (Action)
import Klay.Keyboard.Layout.Action.Label (ActionLabeler)
import Klay.Keyboard.Layout.Level (Level)
import Klay.Keyboard.Layout.Group (KeyFinalGroup, groupLayers)
import Klay.Typing.Method ( TypingMethod )


generateSvgTypematrix
  :: _
  => TypeMatrixMode
  -> ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgTypematrix mode ka config labeler tm =
  fmap (mkLevel config labeler mode tm . fmap (`composeKeyMap` ka)) . groupLayers

mkLevel
  :: _
  => StyleOptions
  -> ActionLabeler
  -> TypeMatrixMode
  -> TypingMethod
  -> (Level, ButtonActionsMap Action)
  -> ANSI.GroupDiagram b
mkLevel config labeler m tm (modifiers, am) = vsep horizontalSpace
  [ hsep horizontalSpace columns
  , hsep horizontalSpace
      [ vsep horizontalSpace . fmap ((# alignTL) . hsep horizontalSpace) $ [ keys_b1_row1', keys_b1_row2' ]
      , mkSpaceKey (TM.tm2030Key (Mappable K.Space) m) config mkKey # alignTL
      , vsep horizontalSpace . fmap ((# alignTL) . hsep horizontalSpace) $ [ keys_b2_row1', keys_b2_row2' ]
      , mkLargeKey (TM.tm2030Key (Mappable K.RControl) m) config mkKey # alignTL
      , vsep horizontalSpace
        [ mkBaseKey (TM.tm2030Key (Mappable K.PageUp)   m) config mkKey
        , mkBaseKey (TM.tm2030Key (Mappable K.PageDown) m) config mkKey
        ]
        # alignTL
      ]
    ]
  where
    mkKey = ANSI.mkKey labeler tm modifiers am
    columns = mkColumn <$>
      [ column0 m, column1 m, column2 m, column3 m, column4 m, column5 m, columnC
      , column6 m, column7 m, column8 m, column9 m, column10 m, column11 m, column12 m, column13 m]
    mkColumn = (# alignBL) . vsep horizontalSpace . fmap (\f -> f config mkKey)
    keys_b1_row1' = fmap (\f -> f config mkKey) (keys_b1_row1 m)
    keys_b1_row2' = fmap (\f -> f config mkKey) (keys_b1_row2 m)
    keys_b2_row1' = fmap (\f -> f config mkKey) (keys_b2_row1 m)
    keys_b2_row2' = fmap (\f -> f config mkKey) keys_b2_row2

mkBaseKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkBaseKey = mkKeyBuilder baseKeyWidth baseKeyHeight

mkMiniKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkMiniKey = mkKeyBuilder baseKeyWidth (baseKeyHeight * 2 / 3)

mkMediumHorizontalKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkMediumHorizontalKey = mkKeyBuilder (baseKeyWidth * 3 / 2 + horizontalSpace / 2) baseKeyHeight

mkMediumVerticalKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkMediumVerticalKey = mkKeyBuilder baseKeyWidth (baseKeyHeight * 5 / 3 + horizontalSpace)

mkLargeKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkLargeKey = mkKeyBuilder baseKeyWidth (baseKeyHeight * 2 + horizontalSpace)

mkSpaceKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkSpaceKey = mkKeyBuilder (baseKeyWidth * 5 + horizontalSpace * 4) (baseKeyHeight * 4 / 3)

ergot :: _ => ANSI.GroupDiagram b
ergot = roundedRect 0.3 0.03 0.03 # lw none # fc white # translate (r2 (0, -0.33))

column0 :: _ => TypeMatrixMode -> [ MkKey b ]
column0 m =
  [ mkMiniKey  (TM.tm2030Key (Mappable K.Escape) m)
  , mkBaseKey  (TM.tm2030Key (Mappable K.Tilde) m)
  , mkBaseKey  (TM.tm2030Key (Mappable K.Tabulator) m)
  , mkLargeKey (TM.tm2030Key (Mappable K.LShift) m)
  ]

column1 :: _ => TypeMatrixMode -> [ MkKey b ]
column1 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F1) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N1) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Q) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.A) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Z) m)
  ]

column2 :: _ => TypeMatrixMode -> [ MkKey b ]
column2 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F2) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N2) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.W) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.S) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.X) m)
  ]

column3 :: _ => TypeMatrixMode -> [ MkKey b ]
column3 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F3) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N3) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.E) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.D) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.C) m)
  ]

column4 :: _ => TypeMatrixMode -> [ MkKey b ]
column4 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F4) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N4) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.R) m)
  , \c s -> ergot <> mkBaseKey (TM.tm2030Key (Mappable K.F) m) c s
  , mkBaseKey (TM.tm2030Key (Mappable K.V) m)
  ]

column5 :: _ => TypeMatrixMode -> [ MkKey b ]
column5 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F5) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N5) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.T) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.G) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.B) m)
  ]

columnC :: _ => [ MkKey b ]
columnC =
  [ mkBaseKey (TM.tm2030Key (Mappable K.Delete) TM101)
  , mkMediumVerticalKey (TM.tm2030Key (Mappable K.Backspace) TM101)
  , mkLargeKey (TM.tm2030Key (Mappable K.Return) TM101)
  ]

column6 :: _ => TypeMatrixMode -> [ MkKey b ]
column6 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F6) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N6) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Y) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.H) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N) m)
  ]

column7 :: _ => TypeMatrixMode -> [ MkKey b ]
column7 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F7) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N7) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.U) m)
  , \c s -> ergot <> mkBaseKey (TM.tm2030Key (Mappable K.J) m) c s
  , mkBaseKey (TM.tm2030Key (Mappable K.M) m)
  ]

column8 :: _ => TypeMatrixMode -> [ MkKey b ]
column8 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F8) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N8) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.I) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.K) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Comma) m)
  ]

column9 :: _ => TypeMatrixMode -> [ MkKey b ]
column9 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F9) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N9) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.O) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.L) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Period) m)
  ]

column10 :: _ => TypeMatrixMode -> [ MkKey b ]
column10 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F10) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.N0 ) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.P) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Semicolon) m)
  , \c s -> ergot <> mkBaseKey (TM.tm2030Key (Mappable K.Slash) m) c s
  ]

column11 :: _ => TypeMatrixMode -> [ MkKey b ]
column11 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F11) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Minus) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.LBracket) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Quote) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Backslash) m)
  ]

column12 :: _ => TypeMatrixMode -> [ MkKey b ]
column12 m =
  [ mkMiniKey (TM.tm2030Key (Mappable K.F12) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Equals) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.RBracket) m)
  , mkLargeKey (TM.tm2030Key (Mappable K.RShift) m)
  ]

column13 :: _ => TypeMatrixMode -> [ MkKey b ]
column13 m =
  [ mkMiniKey (TM.tm2030Key TM.Num m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Calculator) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Email1) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.CapsLock) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Browser) m)
  ]

keys_b1_row1 :: _ => TypeMatrixMode -> [ MkKey b ]
keys_b1_row1 m =
  [ mkBaseKey (TM.tm2030Key (Mappable K.LControl) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.MediaPlayPause) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Menu) m)
  , mkBaseKey (TM.tm2030Key TM.Shuffle m)
  ]

keys_b1_row2 :: _ => TypeMatrixMode -> [ MkKey b ]
keys_b1_row2 m =
  [ mkBaseKey TM.FnKey
  , mkMediumHorizontalKey (TM.tm2030Key (Mappable K.LSuper) m)
  , mkMediumHorizontalKey (TM.tm2030Key (Mappable K.LAlternate) m)
  ]

keys_b2_row1 :: _ => TypeMatrixMode -> [ MkKey b ]
keys_b2_row1 m =
  [ mkBaseKey (TM.tm2030Key TM.Desktop m)
  , mkBaseKey (TM.tm2030Key (Mappable K.Home) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.CursorUp) m)
  , mkBaseKey (TM.tm2030Key (Mappable K.End) m)
  ]

keys_b2_row2 :: _ => [ MkKey b ]
keys_b2_row2 =
  [ mkBaseKey (TM.tm2030Key (Mappable K.RAlternate)  TM101)
  , mkBaseKey (TM.tm2030Key (Mappable K.CursorLeft)  TM101)
  , mkBaseKey (TM.tm2030Key (Mappable K.CursorDown)  TM101)
  , mkBaseKey (TM.tm2030Key (Mappable K.CursorRight) TM101)
  ]
