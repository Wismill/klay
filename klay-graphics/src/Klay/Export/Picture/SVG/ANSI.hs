{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Klay.Export.Picture.SVG.ANSI
  ( KeyStyler
  , MkKey
  , GroupDiagrams
  , GroupDiagram
  , NumPad(..)
  , row1, row2, row3, row4, row5, editingRows
  , generateSvgAnsi
  , generateSvgAnsi100percent
  , generateSvgAnsi80percent
  , generateSvgAnsi60percent
  , generateGroupSvg
  , mkLevel60percent, mkLevel70percent, mkLevel80percent, mkLevel100percent
  , mkKeyBuilder, mkBaseKey
  , mkMediumShiftKey, mkBackSpaceKey, mkKeypad0Key
  , mkSmallControlKey, mkMediumControlKey, mkLargeControlKey, mkXLargeControlKey
  , mkStyle, mkColour, mkKey
  , mkKeyWidth
  , ergot
  ) where


import Data.Maybe (fromMaybe)
import Data.Char (isMark)
import Data.List.NonEmpty2 qualified as NE2
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Diagrams.Prelude hiding (Level, All, fontSize, _fontSize)
import Data.Map.Strict qualified as Map

import Klay.Export.Picture.SVG.Stylesheet
import Klay.Keyboard.Hardware.ButtonAction (ButtonAction(..), ButtonActionsMap, ButtonActionsDefinition, composeKeyMap)
import Klay.Keyboard.Hardware.Geometry (FormFactor(..))
import Klay.Keyboard.Hardware.Key
  (Key, IsPhysicalKey, APhysicalKey(..), PhysicalKey, AHardwareAction(..), HardwareAction, BaseHardwareAction(..))
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.Action (Action(..), isActiveModifier)
import Klay.Keyboard.Layout.Action.Label (ActionLabeler)
import Klay.Keyboard.Layout.Group (KeyFinalGroup, groupImplicitLayers)
import Klay.Keyboard.Layout.Level (Level)
import Klay.Keyboard.Layout.Modifier qualified as M
import Klay.Typing.Hand
import Klay.Typing.Method
import Data.Set.NonEmpty qualified as NESet
import Klay.Utils.UserInput (getRawText)


type KeyStyler = PhysicalKey -> APhysicalKey (AHardwareAction (String, Maybe String, KeyStyle))
type MkKey b = StyleOptions  -> KeyStyler -> GroupDiagram b
type MkDiagram b
  =  StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> (Level, ButtonActionsMap Action)
  -> GroupDiagram b

type GroupDiagram b = QDiagram b V2 Double Any
type GroupDiagrams b = [GroupDiagram b]

generateSvgAnsi
  :: _
  => FormFactor
  -> ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [GroupDiagram b]
generateSvgAnsi F100 = generateSvgAnsi100percent
generateSvgAnsi F80  = generateSvgAnsi80percent
generateSvgAnsi F70  = generateSvgAnsi70percent
generateSvgAnsi F60  = generateSvgAnsi60percent

generateSvgAnsi100percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [GroupDiagram b]
generateSvgAnsi100percent = generateGroupSvg $ mkLevel100percent StandardNumPad ansiRows

generateSvgAnsi80percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [GroupDiagram b]
generateSvgAnsi80percent = generateGroupSvg $ mkLevel80percent ansiRows

generateSvgAnsi70percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [GroupDiagram b]
generateSvgAnsi70percent = generateGroupSvg $ mkLevel70percent ansiRows

generateSvgAnsi60percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [GroupDiagram b]
generateSvgAnsi60percent = generateGroupSvg $ mkLevel60percent ansiRows

ansiRows :: forall b. _ => [[MkKey b]]
ansiRows = [row1, row2, row3, row4, row5]

generateGroupSvg
  :: _
  => MkDiagram b
  -> ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [GroupDiagram b]
generateGroupSvg mkLevel ka config labeler tm =
  fmap (mkLevel config labeler tm . fmap (`composeKeyMap` ka)) . groupImplicitLayers


mkLevel60percent
  :: _
  => [[MkKey b]]
  -> MkDiagram b
mkLevel60percent rows config actionLabel tm (modifiers, am) = vsep horizontalSpace . fmap mkRow $ rows
  where
    mkRow = (# alignBL) . hsep horizontalSpace . fmap (\f -> f config (mkKey actionLabel tm modifiers am))

mkLevel70percent
  :: _
  => [[MkKey b]]
  -> MkDiagram b
mkLevel70percent rows config actionLabel tm (modifiers, am) = vsep sectionSpace [functionKeys, ansi60]
  where
    ansi60 = mkLevel60percent rows config actionLabel tm (modifiers, am)
    functionKeys = mkRow functionKeysRow
    mkRow = (# alignBL) . hsep horizontalSpace . fmap (\f -> f config (mkKey actionLabel tm modifiers am))

mkLevel80percent
  :: _
  => [[MkKey b]]
  -> MkDiagram b
mkLevel80percent rows config actionLabel tm (modifiers, am) = hsep sectionSpace [ansi70, editing_and_cursor]
  where
    ansi70 = mkLevel70percent rows config actionLabel tm (modifiers, am)
    editing_and_cursor = vsep sectionSpace [system_keys, editing_and_cursor']
    editing_and_cursor' = vsep (horizontalSpace * 2 + baseKeyHeight) [editing, cursor]
    system_keys = mkRow systemKeysRow
    editing = vsep horizontalSpace . fmap mkRow $ editingRows
    cursor = vsep horizontalSpace . fmap mkRow $ cursorRows
    mkRow = (# alignBL) . hsep horizontalSpace . fmap (\f -> f config (mkKey actionLabel tm modifiers am))

mkLevel100percent
  :: _
  => NumPad
  -> [[MkKey b]]
  -> MkDiagram b
mkLevel100percent numpad rows config actionLabel tm (modifiers, am) = hsep sectionSpace [ansi80, numericKeypad]
  where
    ansi80 = mkLevel80percent rows config actionLabel tm (modifiers, am)
    numericKeypad = vsep sectionSpace [mkRow [mkBlankBaseKey], numericKeypad']
    numericKeypad' = hsep horizontalSpace [numericKeypad1, numericKeypad2]
    numericKeypad1 = vsep horizontalSpace . fmap mkRow $ numericKeypadRows1
    numericKeypad2 = vsep horizontalSpace . fmap mkRow $ numericKeypadRows2 numpad
    mkRow = (# alignBL) . hsep horizontalSpace . fmap (\f -> f config (mkKey actionLabel tm modifiers am))

sectionSpace :: Double
sectionSpace = horizontalSpace * 4


mkKeyBuilder
  :: (IsPhysicalKey k HardwareAction, _)
  => Double
  -> Double
  -> k
  -> MkKey b
mkKeyBuilder w h key config styler
  =  label # center
  <> roundedRect w h 0.1 # fc c1 # lwG line_weight # lc c2 # dashingN d 0
  where
    (label, style) = case styler . K.mkPhysicalKey $ key of
      SingleKey l -> mkLabel l 1
      FnDualKey l1 l2 ->
        let (label1, s1) = mkLabel l1 0.9
            (label2, _s2) = mkLabel l2 0.5
            fontSize = _textSizeFactor s1 * baseFontSize
            separatorV = strutY (fontSize * 0.6)
        --in ((label2 ||| strutX (fontSize * 0.6)) === separatorV === (strutX (fontSize * 0.6) ||| label1), s1)
        in ((label2 # translateX (negate (fontSize * 0.5))) === separatorV === (label1 # translateX (fontSize * 0.24)), s1)
    mkLabel (OneKey l@(_, _, s)) f = (mkLabel' l f, s)
    mkLabel (KeyCombo l1 l2) f = (hsep (0.01 * _textSizeFactor s) (flip mkLabel' f <$> [l1, l, l2]), s)
      where
        (_, _, s) = l1
        l = ("+", Nothing, s{_textColour1=black})
    mkLabel (KeySequence ks) f = (hsep (0.01 * _textSizeFactor s) (flip mkLabel' f <$> ks'), s) -- [TODO] reduce text size
      where
        (_, _, s) = NE2.head ks
        ks' = NE2.toList ks
    mkLabel' (label1, mlabel2, s) f = case mlabel2 of
      Nothing     -> text' fontSize label1  # font fontName # fontSizeL fontSize # fc fc1
      Just label2 -> text' fontSize label1  # font fontName # fontSizeL fontSize # fc fc1
                 ||| text' fontSize "⦙"     # font fontName # fontSizeL fontSize # fc black
                 ||| text' fontSize label2  # font fontName # fontSizeL fontSize # fc fc2
      where
        fc1 = _textColour1 s
        fc2 = _textColour2 s
        fontSize = f * _textSizeFactor s * baseFontSize
    text' fontSize t = text t <> strutX (fromIntegral (length t) * fontSize * 0.51)
    c1 = _foregroundColour style
    c2 = _borderColour style
    d = _borderDashing style
    line_weight = _borderSize style
    fontName = _fontName config
    baseFontSize = _fontSize config

mkKeyWidth :: Double -> Double
mkKeyWidth w = w * (baseKeyWidth + horizontalSpace) - horizontalSpace

mkBlankKey :: _ => Double -> Double -> MkKey b
mkBlankKey w h _ _ = roundedRect w h 0.1 # lwG 0

mkBaseKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkBaseKey = mkKeyBuilder baseKeyWidth baseKeyWidth

mkBlankBaseKey :: _ => MkKey b
mkBlankBaseKey = mkBlankKey baseKeyWidth baseKeyWidth

mkBackSpaceKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkBackSpaceKey = mkKeyBuilder (mkKeyWidth 2) baseKeyWidth

mkSmallControlKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkSmallControlKey = mkKeyBuilder (mkKeyWidth 1.25) baseKeyWidth

mkMediumShiftKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkMediumShiftKey = mkKeyBuilder (mkKeyWidth 2.25) baseKeyWidth

mkMediumControlKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkMediumControlKey = mkKeyBuilder (mkKeyWidth 1.5) baseKeyWidth

mkLargeControlKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkLargeControlKey = mkKeyBuilder (mkKeyWidth 1.75) baseKeyWidth

mkXLargeControlKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkXLargeControlKey = mkKeyBuilder (mkKeyWidth 2.75) baseKeyWidth

mkFlatReturnKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkFlatReturnKey = mkMediumShiftKey

mkKeypad0Key :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkKeypad0Key = mkKeyBuilder (mkKeyWidth 2) baseKeyWidth

mkKeypadEnterKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkKeypadEnterKey = mkKeyBuilder baseKeyWidth (mkKeyWidth 2)

mkSpaceKey :: (IsPhysicalKey k HardwareAction, _) => k -> MkKey b
mkSpaceKey = mkKeyBuilder (mkKeyWidth 6) baseKeyWidth

ergot :: _ => GroupDiagram b
ergot = roundedRect 0.3 0.03 0.03 # lw none # fc white # translate (r2 (0, -0.33))

row1 :: _ => [ MkKey b ]
row1 =
  [ mkBaseKey . Mappable $ K.Tilde
  , mkBaseKey . Mappable $ K.N1
  , mkBaseKey . Mappable $ K.N2
  , mkBaseKey . Mappable $ K.N3
  , mkBaseKey . Mappable $ K.N4
  , mkBaseKey . Mappable $ K.N5
  , mkBaseKey . Mappable $ K.N6
  , mkBaseKey . Mappable $ K.N7
  , mkBaseKey . Mappable $ K.N8
  , mkBaseKey . Mappable $ K.N9
  , mkBaseKey . Mappable $ K.N0
  , mkBaseKey . Mappable $ K.Minus
  , mkBaseKey . Mappable $ K.Plus
  , mkBackSpaceKey . Mappable $ K.Backspace ]

row2 :: _ => [ MkKey b ]
row2 =
  [ mkMediumControlKey . Mappable $ K.Tabulator
  , mkBaseKey . Mappable $ K.Q
  , mkBaseKey . Mappable $ K.W
  , mkBaseKey . Mappable $ K.E
  , mkBaseKey . Mappable $ K.R
  , mkBaseKey . Mappable $ K.T
  , mkBaseKey . Mappable $ K.Y
  , mkBaseKey . Mappable $ K.U
  , mkBaseKey . Mappable $ K.I
  , mkBaseKey . Mappable $ K.O
  , mkBaseKey . Mappable $ K.P
  , mkBaseKey . Mappable $ K.LBracket
  , mkBaseKey . Mappable $ K.RBracket
  , mkMediumControlKey . Mappable $ K.Backslash ]

row3 :: _ => [ MkKey b ]
row3 =
  [ mkLargeControlKey . Mappable $ K.CapsLock
  , mkBaseKey . Mappable $ K.A
  , mkBaseKey . Mappable $ K.S
  , mkBaseKey . Mappable $ K.D
  , \c s -> ergot <> (mkBaseKey . Mappable $ K.F) c s
  , mkBaseKey . Mappable $ K.G
  , mkBaseKey . Mappable $ K.H
  , \c s -> ergot <> (mkBaseKey . Mappable $ K.J) c s
  , mkBaseKey . Mappable $ K.K
  , mkBaseKey . Mappable $ K.L
  , mkBaseKey . Mappable $ K.Semicolon
  , mkBaseKey . Mappable $ K.Quote
  , mkFlatReturnKey . Mappable $ K.Return ]

row4 :: _ => [ MkKey b ]
row4 =
  [ mkMediumShiftKey . Mappable $ K.LShift
  , mkBaseKey . Mappable $ K.Z
  , mkBaseKey . Mappable $ K.X
  , mkBaseKey . Mappable $ K.C
  , mkBaseKey . Mappable $ K.V
  , mkBaseKey . Mappable $ K.B
  , mkBaseKey . Mappable $ K.N
  , mkBaseKey . Mappable $ K.M
  , mkBaseKey . Mappable $ K.Comma
  , mkBaseKey . Mappable $ K.Period
  , mkBaseKey . Mappable $ K.Slash
  , mkXLargeControlKey . Mappable $ K.RShift ]

row5 :: _ => [ MkKey b ]
row5 =
  [ mkMediumControlKey . Mappable $ K.LControl
  , mkBaseKey . Mappable $ K.LSuper
  , mkMediumControlKey . Mappable $ K.LAlternate
  , mkSpaceKey . Mappable $ K.Space
  , mkMediumControlKey . Mappable $ K.RAlternate
  , mkBaseKey . Mappable $ K.RSuper
  , mkBaseKey . Mappable $ K.Menu
  , mkMediumControlKey . Mappable $ K.RControl ]

functionKeysRow :: _ => [ MkKey b ]
functionKeysRow =
  [ mkBaseKey . Mappable $ K.Escape
  , blank
  , mkBaseKey . Mappable $ K.F1
  , mkBaseKey . Mappable $ K.F2
  , mkBaseKey . Mappable $ K.F3
  , mkBaseKey . Mappable $ K.F4
  , blank
  , mkBaseKey . Mappable $ K.F5
  , mkBaseKey . Mappable $ K.F6
  , mkBaseKey . Mappable $ K.F7
  , mkBaseKey . Mappable $ K.F8
  , blank
  , mkBaseKey . Mappable $ K.F9
  , mkBaseKey . Mappable $ K.F10
  , mkBaseKey . Mappable $ K.F11
  , mkBaseKey . Mappable $ K.F12 ]
  where blank = mkBlankKey ((baseKeyWidth * 2 - horizontalSpace) / 3) baseKeyHeight

systemKeysRow :: _ => [ MkKey b ]
systemKeysRow =
  [ mkBaseKey . Mappable $ K.PrintScreen
  , mkBaseKey . Mappable $ K.ScrollLock
  , mkBaseKey . Mappable $ K.Pause
  ]

editingRows :: _ => [[MkKey b]]
editingRows =
  [ [ mkBaseKey . Mappable $ K.Insert
    , mkBaseKey . Mappable $ K.Home
    , mkBaseKey . Mappable $ K.PageUp
    ]
  , [ mkBaseKey . Mappable $ K.Delete
    , mkBaseKey . Mappable $ K.End
    , mkBaseKey . Mappable $ K.PageDown
    ]
  ]

cursorRows :: _ => [[MkKey b]]
cursorRows =
  [ [ mkBlankBaseKey
    , mkBaseKey . Mappable $ K.CursorUp
    ]
  , [ mkBaseKey . Mappable $ K.CursorLeft
    , mkBaseKey . Mappable $ K.CursorDown
    , mkBaseKey . Mappable $ K.CursorRight
    ]
  ]

numericKeypadRows1 :: _ => [[MkKey b]]
numericKeypadRows1 =
  [ [ mkBaseKey . Mappable $ K.NumLock
    , mkBaseKey . Mappable $ K.KPDivide
    , mkBaseKey . Mappable $ K.KPMultiply
    ]
  , [ mkBaseKey . Mappable $ K.KP7
    , mkBaseKey . Mappable $ K.KP8
    , mkBaseKey . Mappable $ K.KP9
    ]
  , [ mkBaseKey . Mappable $ K.KP4
    , mkBaseKey . Mappable $ K.KP5
    , mkBaseKey . Mappable $ K.KP6
    ]
  , [ mkBaseKey . Mappable $ K.KP1
    , mkBaseKey . Mappable $ K.KP2
    , mkBaseKey . Mappable $ K.KP3
    ]
  , [ mkKeypad0Key . Mappable $ K.KP0
    , mkBaseKey . Mappable $ K.KPDecimal
    ]
  ]

data NumPad = StandardNumPad | NumPadWithExtraSeparator

numericKeypadRows2 :: _ => NumPad -> [[MkKey b]]
numericKeypadRows2 StandardNumPad =
  [ [ mkBaseKey . Mappable $ K.KPSubtract ]
  , [ mkKeypadEnterKey . Mappable $ K.KPAdd ]
  , [ mkKeypadEnterKey . Mappable $ K.KPEnter ]
  ]
numericKeypadRows2 NumPadWithExtraSeparator =
  [ [ mkBaseKey . Mappable $ K.KPSubtract ]
  , [ mkBaseKey . Mappable $ K.KPAdd ]
  , [ mkBaseKey . Mappable $ K.KPComma ]
  , [ mkKeypadEnterKey . Mappable $ K.KPEnter ]
  ]

mkKey
  :: ActionLabeler -> TypingMethod -> Level
  -> ButtonActionsMap Action -> PhysicalKey
  -> APhysicalKey (AHardwareAction (String, Maybe String, KeyStyle))
mkKey actionLabel tm modifiers ka pk = fmap (mkKey' actionLabel tm modifiers ka) <$> pk

mkKey'
  :: ActionLabeler -> TypingMethod -> Level
  -> ButtonActionsMap Action -> BaseHardwareAction
  -> (String, Maybe String, KeyStyle)
mkKey' _            _  _         _  (NonMappable l) = (T.unpack l, Nothing, miscKeyStyle)
mkKey' actionLabel tm modifiers ka (Mappable key) = case Map.lookup key ka of
  Nothing -> (mempty, Nothing, defaultKeyStyle)
  Just (NormalKey a) ->
    let style = mkStyle colour a modifiers
    in (mkLabel a, Nothing, style)
  Just (DualKey a1 a2 _ _ _) ->
      let s1 = mkStyle colour a1 modifiers
          s2 = mkStyle colour a2 modifiers
          s | a2 == NoAction || a2 == UndefinedAction = s1
            | isActiveModifier modifiers a2 = s2{_textColour1=_textColour1 s1, _textColour2=_textColour1 s2, _borderDashing=[0.006, 0.006], _textSizeFactor=0.75}
            | otherwise = s1{_textColour2=_textColour1 s2, _borderDashing=[0.012, 0.006], _borderSize=defaultLineWeight*1.5, _textSizeFactor=0.75}
      in (mkLabel a1, Just (mkLabel a2), s)
  where
    colour = mkColour tm key
    mkLabel = fromMaybe mempty . actionLabel

mkStyle :: Colour Double -> Action -> Level -> KeyStyle
mkStyle c NoAction _ = defaultKeyStyle{_foregroundColour=mkNoActionKeyBackground c, _borderColour=mkBorderColour c}
mkStyle c UndefinedAction _ = defaultKeyStyle{_foregroundColour=mkUndefinedKeyBackground c, _borderColour=mkBorderColour c}
mkStyle c (AChar char) _
  | isMark char = diacriticsKeyStyle{_foregroundColour=c, _borderColour=mkBorderColour c}
  | otherwise = defaultKeyStyle{_foregroundColour=c, _borderColour=mkBorderColour c}
mkStyle c (AText t) _
  | len > 3 = defaultKeyStyle{_foregroundColour=c, _borderColour=mkBorderColour c, _textSizeFactor=0.45}
  | len > 2 = defaultKeyStyle{_foregroundColour=c, _borderColour=mkBorderColour c, _textSizeFactor=0.6}
  | otherwise             = defaultKeyStyle{_foregroundColour=c, _borderColour=mkBorderColour c}
  where len = TL.length . getRawText $ t
mkStyle c (ADeadKey _) _ = deadKeyStyle{_foregroundColour=c, _borderColour=mkBorderColour c}
mkStyle _ (ASpecial _) _ = controlKeyStyle
mkStyle _ (AModifier (M.Modifier bs _ _)) modifiers -- [TODO] Different style for lock?
  | M.isSystemModifier (NESet.toSet bs) =
    if M.isActiveModifier modifiers bs
      then mkKeyPressed controlKeyStyle
      else controlKeyStyle
  | otherwise =
    if M.isActiveModifier modifiers bs
      then mkKeyPressed modifierKeyStyle
      else modifierKeyStyle

-- [TODO] Check typing method is complete
mkColour :: TypingMethod -> Key -> Colour Double
mkColour tm key = maybe white (getColour . _finger) $ hfinger tm key
  where
    getColour ThumbFinger  = thumbFingerColour
    getColour IndexFinger  = indexFingerColour
    getColour MiddleFinger = middleFingerColour
    getColour RingFinger   = ringFingerColour
    getColour LittleFinger = littleFingerColour
