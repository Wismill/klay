module Klay.Export.Picture.SVG.Stylesheet
  ( KeyStyle(..)
  , StyleOptions(..)
  , defaultStyleOptions
  , mkBorderColour
  , mkKeyPressed
  , defaultKeyStyle, controlKeyStyle, modifierKeyStyle, deadKeyStyle, diacriticsKeyStyle
  , miscKeyStyle
  , myOrange, myRed, myGreen, myBlue
  , thumbFingerColour, indexFingerColour, middleFingerColour, ringFingerColour, littleFingerColour
  , deadKeyFontColor, modifierFontColor
  , undefinedKeyColor, mkUndefinedKeyBackground, mkNoActionKeyBackground
  , specialKeyColor
  , baseKeyWidth, baseKeyHeight
  , horizontalSpace, defaultLineWeight
  , defaultFontSize, defaultFontName
  ) where


import Diagrams.Prelude


data StyleOptions = StyleOptions
  { _fontName :: String
  , _fontSize :: Double
  } deriving (Eq, Show)

defaultStyleOptions :: StyleOptions
defaultStyleOptions = StyleOptions
  { _fontName = defaultFontName
  , _fontSize = defaultFontSize
  }

--- Key styles ----------------------------------------------------------------

data KeyStyle = KeyStyle
  { _foregroundColour :: Colour Double
  , _borderColour :: Colour Double
  , _borderSize :: Double
  , _borderDashing :: [Double]
  , _textColour1 :: Colour Double
  , _textColour2 :: Colour Double
  , _textSizeFactor :: Double
  } deriving (Eq, Show)

defaultKeyStyle :: KeyStyle
defaultKeyStyle = KeyStyle
  { _foregroundColour = lightgrey
  , _borderColour = black
  , _borderSize = defaultLineWeight
  , _borderDashing = mempty
  , _textColour1 = black
  , _textColour2 = black
  , _textSizeFactor = defaultFontSize
  }

controlKeyStyle :: KeyStyle
controlKeyStyle = defaultKeyStyle
  { _foregroundColour = specialKeyColor
  , _borderColour = mkBorderColour specialKeyColor
  }

miscKeyStyle :: KeyStyle
miscKeyStyle = defaultKeyStyle
  { _foregroundColour = miscKeyColor
  , _borderColour = mkBorderColour specialKeyColor
  }
  where
    miscKeyColor = blend 0.6 (specialKeyColor :: Colour Double) white

modifierKeyStyle :: KeyStyle
modifierKeyStyle = controlKeyStyle
  { _textColour1 = modifierFontColor
  }

deadKeyStyle, diacriticsKeyStyle :: KeyStyle
deadKeyStyle = defaultKeyStyle
  { _textColour1 = deadKeyFontColor
  }
diacriticsKeyStyle = defaultKeyStyle
  { _textColour1 = modifierFontColor
  }

mkKeyPressed :: KeyStyle -> KeyStyle
mkKeyPressed s = s
  { _borderSize = defaultLineWeight * 2.1
  , _borderColour = keyPressedBorderColor
  , _foregroundColour = keyPressedColor
  }

--- Colours -------------------------------------------------------------------

mkBorderColour :: Colour Double -> Colour Double
mkBorderColour = darken 0.18

mkUndefinedKeyBackground :: Colour Double -> Colour Double
mkUndefinedKeyBackground c = blend 0.60 c white

mkNoActionKeyBackground :: Colour Double -> Colour Double
mkNoActionKeyBackground c = blend 0.51 c white

keyPressedColor, keyPressedBorderColor :: Colour Double
keyPressedColor = darken 0.36 specialKeyColor
keyPressedBorderColor = black

myOrange, littleFingerColour :: (Ord a, Floating a) => Colour a
myOrange = sRGB24 0xf0 0xad 0x4e
littleFingerColour = myOrange

myRed, ringFingerColour :: (Ord a, Floating a) => Colour a
myRed = sRGB24 0xd9 0x53 0x4f
ringFingerColour = myRed

myGreen, middleFingerColour :: (Ord a, Floating a) => Colour a
myGreen = sRGB24 0x5c 0xb8 0x5c
middleFingerColour = myGreen

myBlue, indexFingerColour :: (Ord a, Floating a) => Colour a
myBlue = sRGB24 0x33 0x7a 0xb7
indexFingerColour = myBlue

myPurple, thumbFingerColour :: (Ord a, Floating a) => Colour a
myPurple = sRGB24 0x89 0x7d 0xd7
thumbFingerColour = myPurple

undefinedKeyColor :: (Ord a, Floating a) => Colour a
undefinedKeyColor = white

specialKeyColor :: (Ord a, Floating a) => Colour a
specialKeyColor = sRGB24 0x9f 0x9f 0x9f

deadKeyFontColor :: (Ord a, Floating a) => Colour a
deadKeyFontColor = sRGB24 0x66 0x00 0x00

modifierFontColor :: (Ord a, Floating a) => Colour a
modifierFontColor = sRGB24 0x00 0x33 0x99

--- Lines ---------------------------------------------------------------------

baseKeyWidth :: Double
baseKeyWidth = 1

baseKeyHeight :: Double
baseKeyHeight = baseKeyWidth

horizontalSpace :: Double
horizontalSpace = 0.12

defaultLineWeight :: Double
defaultLineWeight = 0.03

--- Font ----------------------------------------------------------------------

defaultFontSize :: Double
defaultFontSize = 0.72

defaultFontName :: String
defaultFontName = "DejaVu Sans Mono"
