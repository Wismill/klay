{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Klay.Export.Picture.SVG.ISO
  ( ANSI.row1, row2, row3, row4, ANSI.row5
  , generateSvgIso
  , generateSvgIso100percent
  , generateSvgIso80percent
  , generateSvgIso60percent
  , mkMediumShiftKey
  ) where

import Klay.Export.Picture.SVG.Stylesheet (baseKeyWidth, StyleOptions)
import Klay.Export.Picture.SVG.ANSI qualified as ANSI
import Klay.Keyboard.Hardware.ButtonAction (ButtonActionsDefinition)
import Klay.Keyboard.Hardware.Geometry (FormFactor(..))
import Klay.Keyboard.Hardware.Key
    ( IsPhysicalKey, BaseHardwareAction(Mappable), HardwareAction )
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.Label (ActionLabeler)
import Klay.Keyboard.Layout.Group (KeyFinalGroup)
import Klay.Typing.Method ( TypingMethod )


generateSvgIso
  :: _
  => FormFactor
  -> ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgIso F100 = generateSvgIso100percent
generateSvgIso F80  = generateSvgIso80percent
generateSvgIso F70  = generateSvgIso70percent
generateSvgIso F60  = generateSvgIso60percent

generateSvgIso100percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgIso100percent
  = ANSI.generateGroupSvg
  $ ANSI.mkLevel100percent ANSI.StandardNumPad isoRows

generateSvgIso80percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgIso80percent = ANSI.generateGroupSvg $ ANSI.mkLevel80percent isoRows

generateSvgIso70percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgIso70percent = ANSI.generateGroupSvg $ ANSI.mkLevel70percent isoRows

generateSvgIso60percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgIso60percent = ANSI.generateGroupSvg $ ANSI.mkLevel60percent isoRows

isoRows :: _ => [[ ANSI.MkKey b ]]
isoRows = [ANSI.row1, row2, row3, row4, ANSI.row5]

mkMediumShiftKey :: (IsPhysicalKey k HardwareAction, _) => k -> ANSI.MkKey b
mkMediumShiftKey = ANSI.mkKeyBuilder (ANSI.mkKeyWidth 1.25) baseKeyWidth

-- [TODO] proper Return key
mkIsoReturnKey :: (IsPhysicalKey k HardwareAction, _) => k -> ANSI.MkKey b
mkIsoReturnKey = ANSI.mkMediumControlKey

{- [TODO]
isoReturnKey :: _ => QDiagram b V2 Double Any
isoReturnKey
  = trailLike
  . (`at` p2 (w/2, abs rBR - h/2))
  . wrapTrail
  . glueLine
  $ seg (0, h - abs rTR - abs rBR)
  <> mkCorner 0 rTR
  <> seg (abs rTR + abs rTL - w, 0)
  <> mkCorner 1 rTL
  <> seg (0, abs rTL + abs rBL - h)
  <> mkCorner 2 rBL
  <> seg (w - abs rBL - abs rBR, 0)
  <> mkCorner 3 rBR
  where h = 2 :: Double
        w = 2 :: Double
        r0 = 0.3 :: Double
        seg = lineFromOffsets . (:[]) . r2
        diag = sqrt (w * w + h * h)
        -- to clamp corner radius, need to compare with other corners that share an
        -- edge. If the corners overlap then reduce the largest corner first, as far
        -- as 50% of the edge in question.
        rTL = r1
        rBL = r1
        rTR = r1
        rBR = r1
        r1 = clampDiag r0 . clampAdj h r0 . clampAdj w r0 $ r0
        -- prevent curves of adjacent corners from overlapping
        clampAdj len adj r  = if abs r > len/2
                                then sign r * max (len/2) (min (len - abs adj) (abs r))
                                else r
        -- prevent inward curves of diagonally opposite corners from intersecting
        clampDiag opp r     = if r < 0 && opp < 0 && abs r > diag / 2
                                then sign r * max (diag / 2) (min (abs r) (diag + opp))
                                else r
        sign n = if n < 0 then -1 else 1
        mkCorner k r | r == 0    = mempty
                     | r < 0     = doArc 3 (-1)
                     | otherwise = doArc 0 1
                     where
                       doArc d s =
                           arc' r (xDir & _theta <>~ ((k+d)/4 @@ turn)) (s/4 @@ turn)
-}

row2 :: _ => [ ANSI.MkKey b ]
row2 =
  [ ANSI.mkMediumControlKey . Mappable $ K.Tabulator
  , ANSI.mkBaseKey . Mappable $ K.Q
  , ANSI.mkBaseKey . Mappable $ K.W
  , ANSI.mkBaseKey . Mappable $ K.E
  , ANSI.mkBaseKey . Mappable $ K.R
  , ANSI.mkBaseKey . Mappable $ K.T
  , ANSI.mkBaseKey . Mappable $ K.Y
  , ANSI.mkBaseKey . Mappable $ K.U
  , ANSI.mkBaseKey . Mappable $ K.I
  , ANSI.mkBaseKey . Mappable $ K.O
  , ANSI.mkBaseKey . Mappable $ K.P
  , ANSI.mkBaseKey . Mappable $ K.LBracket
  , ANSI.mkBaseKey . Mappable $ K.RBracket
  , mkIsoReturnKey . Mappable $ K.Return ]

row3 :: _ => [ ANSI.MkKey b ]
row3 =
  [ ANSI.mkLargeControlKey . Mappable $ K.CapsLock
  , ANSI.mkBaseKey . Mappable $ K.A
  , ANSI.mkBaseKey . Mappable $ K.S
  , ANSI.mkBaseKey . Mappable $ K.D
  , \c s -> ANSI.ergot <> (ANSI.mkBaseKey . Mappable $ K.F) c s
  , ANSI.mkBaseKey . Mappable $ K.G
  , ANSI.mkBaseKey . Mappable $ K.H
  , \c s -> ANSI.ergot <> (ANSI.mkBaseKey . Mappable $ K.J) c s
  , ANSI.mkBaseKey . Mappable $ K.K
  , ANSI.mkBaseKey . Mappable $ K.L
  , ANSI.mkBaseKey . Mappable $ K.Semicolon
  , ANSI.mkBaseKey . Mappable $ K.Quote
  , ANSI.mkBaseKey . Mappable $ K.Backslash ]

row4 :: _ => [ ANSI.MkKey b ]
row4 =
  [ mkMediumShiftKey . Mappable $ K.LShift
  , ANSI.mkBaseKey . Mappable $ K.Iso102
  , ANSI.mkBaseKey . Mappable $ K.Z
  , ANSI.mkBaseKey . Mappable $ K.X
  , ANSI.mkBaseKey . Mappable $ K.C
  , ANSI.mkBaseKey . Mappable $ K.V
  , ANSI.mkBaseKey . Mappable $ K.B
  , ANSI.mkBaseKey . Mappable $ K.N
  , ANSI.mkBaseKey . Mappable $ K.M
  , ANSI.mkBaseKey . Mappable $ K.Comma
  , ANSI.mkBaseKey . Mappable $ K.Period
  , ANSI.mkBaseKey . Mappable $ K.Slash
  , ANSI.mkXLargeControlKey . Mappable $ K.RShift ]
