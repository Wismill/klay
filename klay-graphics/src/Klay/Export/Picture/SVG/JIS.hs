{-# LANGUAGE OverloadedLists           #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE PartialTypeSignatures     #-}
{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# OPTIONS_GHC -fno-warn-partial-type-signatures #-}

module Klay.Export.Picture.SVG.JIS
  ( row1, ISO.row2, ISO.row3, row4, row5
  , generateSvgJis
  , generateSvgJis100percent
  , generateSvgJis80percent
  , generateSvgJis60percent
  , mkMediumShiftKey
  ) where

import Klay.Export.Picture.SVG.Stylesheet
    ( baseKeyWidth, StyleOptions )
import Klay.Export.Picture.SVG.ANSI qualified as ANSI
import Klay.Export.Picture.SVG.ISO qualified as ISO
import Klay.Keyboard.Hardware.ButtonAction (ButtonActionsDefinition)
import Klay.Keyboard.Hardware.Geometry (FormFactor(..))
import Klay.Keyboard.Hardware.Key
    ( IsPhysicalKey, BaseHardwareAction(Mappable), HardwareAction )
import Klay.Keyboard.Hardware.Key qualified as K
import Klay.Keyboard.Layout.Action.Label (ActionLabeler)
import Klay.Keyboard.Layout.Group (KeyFinalGroup)
import Klay.Typing.Method


generateSvgJis
  :: _
  => FormFactor
  -> ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgJis F100 = generateSvgJis100percent
generateSvgJis F80 = generateSvgJis80percent
generateSvgJis F70 = generateSvgJis70percent
generateSvgJis F60 = generateSvgJis60percent

generateSvgJis100percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgJis100percent
  = ANSI.generateGroupSvg
  $ ANSI.mkLevel100percent ANSI.StandardNumPad isoRows

generateSvgJis80percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgJis80percent = ANSI.generateGroupSvg $ ANSI.mkLevel80percent isoRows

generateSvgJis70percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgJis70percent = ANSI.generateGroupSvg $ ANSI.mkLevel70percent isoRows

generateSvgJis60percent
  :: _
  => ButtonActionsDefinition
  -> StyleOptions
  -> ActionLabeler
  -> TypingMethod
  -> KeyFinalGroup
  -> [ANSI.GroupDiagram b]
generateSvgJis60percent = ANSI.generateGroupSvg $ ANSI.mkLevel60percent isoRows

isoRows :: _ => [[ ANSI.MkKey b ]]
isoRows = [row1, ISO.row2, ISO.row3, row4, row5]

--

mkMediumShiftKey :: (IsPhysicalKey k HardwareAction, _) => k -> ANSI.MkKey b
mkMediumShiftKey = ANSI.mkKeyBuilder (ANSI.mkKeyWidth 1.75) baseKeyWidth

-- mkMediumShiftKey :: (IsPhysicalKey k HardwareAction, _) => k -> KeyStyler -> GroupDiagram b
-- mkMediumShiftKey = mkKeyBuilder (baseKeyWidth * 2.75 - 2 * horizontalSpace) baseKeyWidth

mkSpaceKey :: (IsPhysicalKey k HardwareAction, _) => k -> ANSI.MkKey b
mkSpaceKey = ANSI.mkKeyBuilder (ANSI.mkKeyWidth 2.5) baseKeyWidth

row1 :: _ => [ ANSI.MkKey b ]
row1 =
  [ ANSI.mkBaseKey . Mappable $ K.Tilde
  , ANSI.mkBaseKey . Mappable $ K.N1
  , ANSI.mkBaseKey . Mappable $ K.N2
  , ANSI.mkBaseKey . Mappable $ K.N3
  , ANSI.mkBaseKey . Mappable $ K.N4
  , ANSI.mkBaseKey . Mappable $ K.N5
  , ANSI.mkBaseKey . Mappable $ K.N6
  , ANSI.mkBaseKey . Mappable $ K.N7
  , ANSI.mkBaseKey . Mappable $ K.N8
  , ANSI.mkBaseKey . Mappable $ K.N9
  , ANSI.mkBaseKey . Mappable $ K.N0
  , ANSI.mkBaseKey . Mappable $ K.Minus
  , ANSI.mkBaseKey . Mappable $ K.Plus
  , ANSI.mkBaseKey . Mappable $ K.Yen
  , ANSI.mkBaseKey . Mappable $ K.Backspace ]

row4 :: _ => [ ANSI.MkKey b ]
row4 =
  [ ANSI.mkMediumShiftKey . Mappable $ K.LShift
  , ANSI.mkBaseKey . Mappable $ K.Z
  , ANSI.mkBaseKey . Mappable $ K.X
  , ANSI.mkBaseKey . Mappable $ K.C
  , ANSI.mkBaseKey . Mappable $ K.V
  , ANSI.mkBaseKey . Mappable $ K.B
  , ANSI.mkBaseKey . Mappable $ K.N
  , ANSI.mkBaseKey . Mappable $ K.M
  , ANSI.mkBaseKey . Mappable $ K.Comma
  , ANSI.mkBaseKey . Mappable $ K.Period
  , ANSI.mkBaseKey . Mappable $ K.Slash
  , ANSI.mkBaseKey . Mappable $ K.Ro
  , mkMediumShiftKey . Mappable $ K.RShift ]

row5 :: _ => [ ANSI.MkKey b ]
row5 =
  [ ANSI.mkMediumControlKey . Mappable $ K.LControl
  , ANSI.mkSmallControlKey . Mappable $ K.LSuper
  , ANSI.mkSmallControlKey . Mappable $ K.LAlternate
  , ANSI.mkSmallControlKey . Mappable $ K.Muhenkan
  , mkSpaceKey . Mappable $ K.Space
  , ANSI.mkSmallControlKey . Mappable $ K.Henkan
  , ANSI.mkSmallControlKey . Mappable $ K.HiraganaKatakana
  , ANSI.mkSmallControlKey . Mappable $ K.RAlternate
  , ANSI.mkBaseKey . Mappable $ K.RSuper
  , ANSI.mkBaseKey . Mappable $ K.Menu
  , ANSI.mkMediumControlKey . Mappable $ K.RControl ]
