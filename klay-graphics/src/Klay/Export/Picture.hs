module Klay.Export.Picture
  ( PictureFormat(..)
  , StyleOptions(..)
  , generatePictures
  , generateSvg
  , generatePng
  , makePath
  ) where


import System.FilePath ((</>))

import Klay.Keyboard.Layout (MultiOsRawLayout)
import Klay.Keyboard.Hardware.Geometry (Geometry)
import Klay.Export.Picture.SVG
import Klay.Export.Picture.PNG
import Klay.OS (OsSpecific)
import Klay.Typing.Method (TypingMethod)


data PictureFormat
  = SVG -- [NOTE] it must come before PNG to ensure it is processed before.
  | PNG
  deriving (Eq, Ord, Enum, Bounded, Show)

generatePictures
  :: PictureFormat
  -> FilePath
  -> OsSpecific
  -> Geometry
  -> TypingMethod
  -> StyleOptions
  -> MultiOsRawLayout
  -> IO ()
generatePictures format path os g tm config = case format of
  SVG -> generateSvg (makePath path SVG) os g tm config
  PNG -> generatePng (makePath path PNG) (makePath path SVG) os

makePath :: FilePath -> PictureFormat -> FilePath
makePath path SVG = path </> "svg"
makePath path PNG = path </> "png"
