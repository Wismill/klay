{-# LANGUAGE OverloadedLists #-}

module Klay.App.Export.Pictures
  ( exportPictures
  , picturesFlags
  , styleConfigOption
  ) where


import Data.Foldable (traverse_)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import System.FilePath ((</>))

import Options.Applicative
import Data.Text.Prettyprint.Doc ( line )
import Data.Text.Prettyprint.Doc.Render.Terminal ( putDoc )

import Klay.App.Export ( mkLayoutName, mkOsPath, mkTitleT )
import Klay.Export.Picture (PictureFormat(..), generatePictures, makePath)
import Klay.Export.Picture.SVG.Stylesheet (StyleOptions(..), defaultFontName, defaultFontSize)
import Klay.Keyboard.Hardware.Geometry (Geometry(..))
import Klay.Keyboard.Layout ( MultiOsRawLayout )
import Klay.OS (OsSpecific)
import Klay.Typing.Method ( TypingMethod(..) )


exportPictures
  :: FilePath
  -> PictureFormat
  -> OsSpecific
  -> [Geometry]
  -> [TypingMethod]
  -> StyleOptions
  -> MultiOsRawLayout
  -> IO ()
exportPictures p f os gs tms config l =
  traverse_ generate (liftA2 (,) gs tms)
  where
    generate (g, tm) = do
      putDoc $ mkTitleT
        [ "[INFO] Export images files of layout “"
        , mkLayoutName l, "” to: ", TL.pack . makePath path $ f
        ] <> line
      generatePictures f path os g tm config l
      where path = p </> "pictures" </> mkOsPath os </> show g </> T.unpack (methodName tm)

picturesFlags :: (PictureFormat -> a) -> Parser [a]
picturesFlags f
  =   flag' [f PNG] (long "png" <> short 'p' <> help "Export PNG format. Requires Inkscape 1.0+.")
  <|> flag' [f SVG] (long "svg" <> short 's' <> help "Export SVG format")
  <|> flag' picture_formats (long "pictures" <> short 'P' <> help "Export all picture formats")
  where picture_formats = f <$> enumFromTo minBound maxBound

styleConfigOption :: Parser StyleOptions
styleConfigOption = StyleOptions
  <$> strOption
   ( long "font-name"
  <> metavar "FONT_NAME"
  <> value defaultFontName
  <> showDefault
  <> help "Font to use for SVG output" )
  <*> option auto
   ( long "font-size"
  <> metavar "FONT_SIZE"
  <> value defaultFontSize
  <> showDefault
  <> help "Font size to use for SVG output" )
