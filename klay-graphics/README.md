# Klay

`klay-graphics` is part of [__Klay__](https://gitlab.com/Wismill/klay/), a cross-platform command-line utility to design __keyboard layouts__.

# `klay-graphics`

This package `klay-graphics` provides the following features:

* Export a layout in the following formats:

    * __SVG__
    * __PNG__
