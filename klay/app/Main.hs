{-# LANGUAGE OverloadedLists #-}

module Main where

import Klay.App (defaultMain)
import Klay.Keyboard.Layout.Examples.Bépo qualified as Bépo
import Klay.Keyboard.Layout.Examples.Carpalx qualified as Carpalx
import Klay.Keyboard.Layout.Examples.Colemak qualified as Colemak
import Klay.Keyboard.Layout.Examples.Dvorak qualified as Dvorak
import Klay.Keyboard.Layout.Examples.Latin qualified as Latin
import Klay.Keyboard.Layout.Examples.Latin.Type4 qualified as Latin4
import Klay.Keyboard.Layout.Examples.Neo2 qualified as Neo2
import Klay.Keyboard.Layout.Examples.Qwertz_DE qualified as DE
import Klay.Keyboard.Layout.Examples.Qwerty_ES qualified as ES
import Klay.Keyboard.Layout.Examples.Qwerty_US qualified as US
import Klay.Keyboard.Layout.Examples.Workman qualified as Workman
import Klay.Typing.Method.ISO.O0 qualified as O0
import Klay.Typing.Method.TypeMatrix.Default qualified as TM

main :: IO ()
main = defaultMain
  [ US.layout
  , DE.layout
  , ES.layout
  , Carpalx.layout
  , Colemak.layout
  , Dvorak.layout
  , Workman.layout
  , Bépo.layout
  , Neo2.layout
  , Latin.layout
  , Latin4.layout
  ]
  [ O0.typingMethod, TM.typingMethod ]
