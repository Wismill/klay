# Klay

`klay` is part of [__Klay__](https://gitlab.com/Wismill/klay/), a cross-platform command-line utility to design __keyboard layouts__.

# `klay`

This package `klay` gather the features of the following package:

* [`klay-core`](https://gitlab.com/Wismill/klay/-/tree/master/klay-core)
* [`klay-graphics`](https://gitlab.com/Wismill/klay/-/tree/master/klay-graphics)
